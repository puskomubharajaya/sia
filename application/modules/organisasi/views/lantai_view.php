<script type="text/javascript" src="<?php echo base_url();?>assets/jquerymasked/src/jquery.maskedinput.js"></script>
<script type="text/javascript">
    function edit(edl) {
        $("#edit_lantai").load('<?php echo base_url()?>organisasi/lantai/load_lantai/'+edl);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Gedung <?php echo $gedung->gedung; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a class="btn btn-warning" href="<?php echo base_url();?>organisasi/Lokasi" ><i class="icon-chevron-left"></i> Kembali</a>
                    <a class="btn btn-primary" data-toggle="modal" href="#myModal" >Tambah Data</a><br><hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Lantai</th>
                                <th>Gedung</th>
                                <th width="118">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($w as $row) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->lantai;?></td>
                                <td><?php echo $row->gedung;?></td>
                                <td class="td-actions">
                                    <a class="btn btn-success btn-small" href="<?php echo base_url();?>organisasi/lantai/detil_lt/<?php echo $row->id_lantai;?>" ><i class="btn-icon-only icon-ok"> </i></a>
                                    <a onclick="edit(<?php echo $row->id_lantai;?>)" class="btn btn-primary btn-small" data-toggle="modal" href="#editModal" ><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>organisasi/lantai/del_lantai/<?php echo $row->id_lantai;?>"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_lantai">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Lantai</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>organisasi/lantai/save_lantai" method="post">
                <div class="modal-body" style="margin-left: -60px;">  
                    <script type="text/javascript">
                    $(function(){
                        $('#lt').mask('99',{placeholder:" "})
                    });
                    </script>
                    <div class="control-group" id="">
                        <label class="control-label">Lantai</label>
                        <div class="controls">
                            <input type="text" id="lt" class="span4" name="lantai" placeholder="Input Lantai (2 digit angka)" class="form-control" value="" required/>
                            
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Lokasi</label>
                        <div class="controls">
                            <input type="text" id="lt" class="span4" name="lants" class="form-control" value="<?php echo $gedung->gedung;?>" disabled/>
                            <input type="hidden" id="lt" class="span4" name="floor" class="form-control" value="<?php echo $this->uri->segment(4)?>" required/>
                        </div>
                    </div> 
                    <!-- <div class="control-group" id="">
                        <label class="control-label">Lokasi</label>
                        <div class="controls">
                            <select class="form-control" name='lants'>
                                <option>--Pilih Gedung--</option>
                                <?php foreach ($ge as $row) { ?>
                                    <option value="<?php echo $row->id_gedung;?>"><?php echo $row->gedung;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> -->              
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->