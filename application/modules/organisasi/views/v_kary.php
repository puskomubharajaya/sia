<script type="text/javascript">
    function edit(edg) {
        $("#edit_gedung").load('<?php echo base_url()?>organisasi/karyawan/load/'+edg);
    }
</script>

<script type="text/javascript">
    function detil(yuk) {
        $("#detil_kry").load('<?php echo base_url()?>organisasi/karyawan/detail/'+yuk);
    }
</script>

<script>
    $(function() {
        $( "#tgl" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            yearRange: "1950:2030"
        });
    });
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data Karyawan</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a class="btn btn-primary" data-toggle="modal" href="#myModal" >Tambah Data</a><br><hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Unit</th>
                                <th>Jabatan</th>
                                <th>Status</th>
                                <th width="118">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($query as $row) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->nip; ?></td>
                                <td><?php echo $row->nama;?></td>
                                <td><?php echo $row->unit;?></td>
                                <td><?php echo $row->jabatan;?></td>
                                <td>
                                    <?php if ($row->status == '1') {
                                        $stt = 'PHL';
                                    } else {
                                        $stt = 'TETAP';
                                    }
                                    echo $stt; ?>
                                </td>
                                <td class="td-actions">
                                    <a onclick="detil(<?php echo $row->id;?>)" class="btn btn-success btn-small" data-toggle="modal" href="#detilModal" ><i class="btn-icon-only icon-user"> </i></a>
                                    <a onclick="edit(<?php echo $row->id;?>)" class="btn btn-primary btn-small" data-toggle="modal" href="#editModal" ><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>organisasi/karyawan/delete/<?php echo $row->id;?>"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_gedung">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- edit modal -->
<div class="modal fade" id="detilModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="detil_kry">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Karyawan</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>organisasi/karyawan/add" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">NIP</label>
                        <div class="controls">
                            <input type="number" class="span4" name="nip" placeholder="Input NIP" class="form-control" value="" required/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Nama</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nama" placeholder="Input Nama" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Unit</label>
                        <div class="controls">
                            <input type="text" class="span4" name="unit" placeholder="Input Unit" class="form-control" value="" required/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Jabatan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="jabatan" placeholder="Input Jabatan" class="form-control" value="" required/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Status</label>
                        <div class="controls">
                            <select class="form-control span4" name='status' required>
                                <option disabled="" selected="">--Pilih Status--</option>
                                <option value="1">PHL</option>
                                <option value="2">Tetap</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">No. HP</label>
                        <div class="controls">
                            <input type="number" class="span4" name="hp" placeholder="Input Nomor HP" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">No. KTP</label>
                        <div class="controls">
                            <input type="number" class="span4" name="ktp" placeholder="Input No. KTP" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">E-mail</label>
                        <div class="controls">
                            <input type="email" class="span4" name="email" placeholder="Input E-mail" class="form-control" value="" required/>
                        </div>
                    </div><div class="control-group" id="">
                        <label class="control-label">Alamat</label>
                        <div class="controls">
                            <textarea type="text" class="span4" name="alamat" placeholder="" class="form-control" value="" required></textarea>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Tanggal Masuk</label>
                        <div class="controls">
                            <input type="text" id="tgl" class="span4" name="thms" placeholder="" class="form-control" value="" required/>
                        </div>
                    </div>  
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->