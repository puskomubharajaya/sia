<script type="text/javascript">
    function edit(edg) {
        $("#edit_gedung").load('<?php echo base_url()?>organisasi/lokasi/loaddata/'+edg);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Lokasi & Ruang</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a class="btn btn-primary" data-toggle="modal" href="#myModal" >Tambah Data</a><br><hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Gedung</th>
                                <th>Lokasi</th>
                                <th width="118">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($lokasi as $row) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->gedung;?></td>
                                <td><?php echo $row->lembaga;?></td>
                                <td class="td-actions">
                                    <a class="btn btn-success btn-small" href="<?php echo base_url();?>organisasi/lokasi/detail_gd/<?php echo $row->id_gedung;?>" ><i class="btn-icon-only icon-eye-open"> </i></a>
                                    <a onclick="edit(<?php echo $row->id_gedung;?>)" class="btn btn-primary btn-small" data-toggle="modal" href="#editModal" ><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>organisasi/lokasi/del_lokasi/<?php echo $row->id_gedung;?>"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_gedung">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Gedung</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>organisasi/lokasi/save_gedung" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Gedung</label>
                        <div class="controls">
                            <input type="text" class="span4" name="gedung" placeholder="Input Gedung" class="form-control" value="" required/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Lokasi</label>
                        <div class="controls">
                            <select class="form-control" name='loks'>
                                <option>--Pilih Lokasi--</option>
                                <?php foreach ($isakol as $row) { ?>
                                    <option value="<?php echo $row->kode_lembaga;?>"><?php echo $row->lembaga;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>              
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->