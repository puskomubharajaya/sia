            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Ruangan</h4>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $.post('<?php echo base_url()?>organisasi/ruang/load_ruang'+$(this).val(),{},function(get){
                        $("#ipt_ruang").html(get);
                    });
                });
            </script>
            <form class ='form-horizontal' action="<?php echo base_url();?>organisasi/ruang/update_ruang" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Kode Ruangan</label>
                        <div class="controls">
                            <input type="hidden" name="lantai" value="<?php echo $query->id_lantai;?>">
                            <input type="hidden" name="hd_ruang" value="<?php echo $query->id_ruangan;?>">
                            <input type="text" id="ipt_ruang" class="span4" name="kd_ruangs" placeholder="Input Kode Ruangan" class="form-control" value="<?php echo $query->kode_ruangan;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Ruangan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="ruangs" placeholder="Input Ruangan" class="form-control" value="<?php echo $query->ruangan;?>" required/>
                        </div>
                    </div> 
                    <!-- <div class="control-group" id="">
                        <label class="control-label">Lantai</label>
                        <div class="controls">
                            <select class="form-control" name='lantai'>
                                <option>--Pilih Lantai--</option>
                                <?php foreach ($lantais as $row) { ?>
                                    <option value="<?php echo $row->id_lantai;?>"><?php echo $row->lantai;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> -->
                    <div class="control-group" id="">
                        <label class="control-label">Kuota</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kuotas" placeholder="Input Kuota" class="form-control" value="<?php echo $query->kuota;?>" required/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Deskripsi</label>
                        <div class="controls">
                            <textarea class="span4" name="deskrips"><?php echo $query->deskripsi;?></textarea>
                        </div>
                    </div>             
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>