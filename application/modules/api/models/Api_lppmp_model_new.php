<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_lppmp_model_new extends CI_Model
{

    public function get_data_user($userid, $usertype)
    {
        switch ($usertype) {
            case 1:
                return $this->_data_dosen($userid);
                break;

            default:
                return $this->_data_administrator($userid);
                break;
        }
    }

    private function _data_dosen($userid)
    {
        $user = $this->db->query(
            "SELECT
                grp.group_id AS id_user_group,
                kry.nid,
                kry.nidn,
                kry.nama,
                kry.nik,
                bio.jabfung,
                bio.tpt_lahir,
                bio.tgl_lahir,
                kry.jns_kel,
                kry.jabatan_id,
                bio.tlp,
                bio.email
            FROM
                users usr
                JOIN tbl_karyawan kry
                ON kry.nid = usr.username
                LEFT JOIN tbl_biodata_dosen bio
                ON bio.nid = usr.username
                LEFT JOIN users_groups grp
                ON grp.user_id = usr.id
            WHERE usr.username = $userid
                AND usr.active = 1
                AND kry.nidn IS NOT NULL
                AND bio.jabfung IS NOT NULL"
        );
        return $user;
    }

    private function _data_administrator($userid)
    {
        $authorized_user = [5, 13, 10, 25];
        $admin = $this->db->query(
            "SELECT
            grp.group_id AS id_user_group,
            usr.username
        FROM
            users usr
        LEFT JOIN users_groups grp
            ON grp.user_id = usr.id
        WHERE usr.username = $userid
            AND usr.active = 1
            AND grp.group_id IN $authorized_user"
        );
        return $admin;
    }
}

/* End of file Api_bkd_model.php */
/* Location: ./application/modules/api/models/Api_bkd_model.php */