<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_bkd_model extends CI_Model
{

	public function get_data_user($userid, $usertype)
	{
		switch ($usertype) {
			case 1:
				return $this->_data_dosen($userid);
				break;

			case 3:
				return $this->_data_fakultas_prodi($userid);
				break;

			default:
				return $this->_data_reviewer($userid);
				break;
		}
	}

	private function _data_dosen($userid)
	{
		$user = $this->db->query("SELECT
									kry.nama,
									kry.nid,
									kry.nidn,
									kry.nupn,
									kry.jabatan_id AS homebase,
									kry.status,
									log.id_user_group,
									log.user_type,
									bio.alamat,
									bio.tlp,
									bio.email
								FROM tbl_karyawan kry
								JOIN tbl_user_login log ON kry.nid = log.userid
								LEFT JOIN tbl_biodata_dosen bio ON kry.nid = bio.nid
								WHERE kry.nid = '{$userid}'");
		return $user;
	}

	private function _data_fakultas_prodi($userid)
	{
		$this->db->select('a.userid, a.id_user_group, a.user_type, c.divisi');
		$this->db->from('tbl_user_login a');
		$this->db->join('tbl_divisi c', 'a.userid = c.kd_divisi');
		$this->db->where('a.username', $userid);
		$this->db->where('a.status', 1);
		return $this->db->get();
	}

	private function _data_reviewer($userid)
	{
		$this->db->select('a.userid, a.id_user_group, a.user_type, c.divisi');
		$this->db->from('tbl_user_login a');
		$this->db->join('tbl_divisi c', 'a.username = c.kd_divisi');
		$this->db->where('a.username', $userid);
		$this->db->where('a.status', 1);
		return $this->db->get();
	}

	public function get_teaching_list($userid, $year)
	{
		$teach_list = $this->db->query("SELECT	
											jdl.kd_jadwal,
											jdl.id_matakuliah,
											jdl.kd_matakuliah,
											jdl.kelas,
											jdl.hari,
											jdl.waktu_mulai,
											jdl.waktu_selesai,
											mk.nama_matakuliah,
											mk.sks_matakuliah
										FROM tbl_jadwal_matkul jdl
										JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
										WHERE jdl.kd_dosen = '{$userid}'
										AND jdl.kd_tahunajaran = '{$year}'");
		return $teach_list;
	}

	public function get_lecturer_position($nid)
	{
		$lecturer = $this->db->query("SELECT 
										kry.nama,
										kry.nid,
										kry.nidn,
										bio.jabfung,
										prd.prodi,
										fak.fakultas,
										fak.kd_fakultas
									FROM tbl_karyawan kry
									LEFT JOIN tbl_biodata_dosen bio ON kry.nid = bio.nid
									LEFT JOIN tbl_jurusan_prodi prd ON kry.jabatan_id = prd.kd_prodi
									JOIN tbl_fakultas fak ON prd.kd_fakultas = fak.kd_fakultas
									WHERE kry.nid = '{$nid}'")->row();
		return $lecturer;
	}

	public function get_lecture_list($query)
	{
		return $this->db->query($query);
	}

	public function get_dean($faculty)
	{
		$dean = $this->db->query("SELECT kry.`nidn`, kry.`nama` FROM tbl_karyawan kry
								JOIN tbl_fakultas fak ON kry.`nid` = fak.`dekan`
								WHERE fak.`kd_fakultas` = '{$faculty}'")->row();
		return $dean;
	}
}

/* End of file Api_bkd_model.php */
/* Location: ./application/modules/api/models/Api_bkd_model.php */