<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_voting_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_students(...$params)
	{
		
		$db2 = $this->load->database('siabaru', true);

		$sql = "SELECT COUNT(*) as total FROM tbl_verifikasi_krs WHERE tahunajaran = $params[0]";
		
		if (array_key_exists('type', $params[1])) {
			$val = $params[1]['kode'];
			if ($params[1]['type'] == '2') {
				$sql .= " AND kd_jurusan IN (SELECT kd_prodi FROM tbl_jurusan_prodi where kd_fakultas = $val)";
			}elseif ($params[1]['type'] == '3') {
				$sql .= " AND kd_jurusan = $val";

			}
		}

		$students = $db2->query($sql)->row()->total;

		return $students;
	}
}

/* End of file Api_votingg_model.php */
/* Location: ./application/models/Api_votingg_model.php */