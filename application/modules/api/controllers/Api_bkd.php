<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_bkd extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->input->get_request_header('x-bkd-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'App key not valid'];
			$this->_create_response(400, $response);
		}
		$this->load->model('api_bkd_model','bkdm');
	}

	/**
	 * Handle login from manual input
	 *
	 * @return void
	 */
	public function attemp_login()
	{
		$payload = file_get_contents('php://input');
		$decode_payload = json_decode($payload);

		$is_user_exist = $this->login_model->cekuser($decode_payload->username, $decode_payload->password);
		
		if ($is_user_exist->num_rows() > 0) {
			$is_authorized_user = $this->_is_authorized($is_user_exist->row()->id_user_group);

			if ($is_authorized_user) {
				$data_user = $this->bkdm->get_data_user($decode_payload->username,$is_user_exist->row()->user_type)->row();
				$session   = $this->_set_payload($data_user, $is_user_exist->row()->user_type);
				$response  = ['status' => 1, 'message' => 'success', 'data' => $session];
				$this->_create_response(200, $response);
			}
		}

		$response = ['status' => 3, 'message' => 'invalid login'];
		$this->_create_response(400, $response);
	}

	/**
	 * Handle login using cookie
	 *
	 * @return void
	 */
	public function cookie_login()
	{
		$payload        = file_get_contents('php://input');
		$decode_payload = json_decode($payload);
		$is_authorized  = $this->_is_authorized($decode_payload->usergroup);
		$data_user      = $this->bkdm->get_data_user($decode_payload->username, $decode_payload->usertype)->row();

		if ($data_user) {
			$session  = $this->_set_payload($data_user, $decode_payload->usertype);
			$response = ['status' => 1, 'message' => 'success', 'data' => $session];
			$this->_create_response(200, $response);
		}

		$response = ['status' => 3, 'message' => 'invalid login'];
		$this->_create_response(401, $response);
	}

	/**
	 * Set payload for each user type. Lecturer has more payload than other authorized user.
	 *
	 * @return void
	 */
	private function _set_payload($data, $usertype)
	{
		switch ($usertype) {
			case 1:
				return $this->_lecture_payload($data);
				break;
			
			default:
				return $this->_nonlecture_payload($data);
				break;
		}
	}

	/**
	 * Set payload for lecturer
	 * @param object $data
	 * @return void
	 */
	private function _lecture_payload($data)
	{
		$session = [
			'username' => $data->nama,
			'homebase' => $data->homebase,
			'userid' => $data->nid,
			'usertype' => $data->user_type,
			'nidn' => $data->nidn,
			'nupn' => $data->nupn,
			'group' => strlen($data->id_user_group) > 1 ? explode(',', $data->id_user_group)[0] : $data->id_user_group,
			'address' => $data->alamat,
			'phone' => substr(trim($data->tlp), 0, 13),
			'email' => $data->email
		];
		return $session;
	}

	/**
	 * Set payload for non lecturer
	 * @param object $data
	 * @return void
	 */
	private function _nonlecture_payload($data)
	{
		$session = [
			'username' => $data->divisi,
			'usertype' => $data->user_type,
			'userid' => $data->userid,
			'group' => $data->id_user_group,
		];
		return $session;
	}

	/**
	 * Check whether user is has authorization
	 * @param int $group
	 * @return void
	 */
	private function _is_authorized($group)
	{
		/**
		 * Group list:
		 * 6=dosen, 7=dosen PA, 8=prodi, 9=fakultas, 14=wr, 18=LPM, 21=GPM, 22=SPM, 23=REKTORAT, 24=Dekan, 25=LPPMP, 26=Ka. Prodi, 27= Wakil Dekan 1
		 */
		$groups = get_group($group);
		$authorized_user = [6,7,8,9,14,18,21,22,23,24,25, 26, 27];

		for ($i = 0; $i < count($groups); $i++) {
			if (in_array($groups[$i], $authorized_user)) {
				return TRUE;
			}
		}

		$response = ['status' => 8, 'message' => 'invalid permission'];
		$this->_create_response(403, $response);
	}

	/**
	 * Create response for all request
	 * @param int 	$status_code
	 * @param array $response
	 * @return void
	 */
	private function _create_response($status_code, $response)
	{
		$this->output
			->set_status_header($status_code)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

	/**
	 * Get list of lecturer teaching in active academic year
	 * 
	 * @return void
	 */
	public function teaching_list()
	{
		$accept_req   = file_get_contents('php://input');
		$decode_req   = json_decode($accept_req);

		$this->_is_number_of_property_valid($decode_req,2);
		$this->_is_property_exist($decode_req, ['userid','academic_year']);

		$get_teaching = $this->bkdm->get_teaching_list($decode_req->userid, $decode_req->academic_year)->result();

		count($get_teaching) > 0 ? $this->_success_teach_res($get_teaching) : $this->_fail_teach_res();
	}

	/**
	 * Create response teaching data for success request
	 * @param array $teaching
	 * @return void
	 */
	private function _success_teach_res($teaching)
	{
		foreach ($teaching as $teach) {
			$data[] = [
				'kode_mk' => $teach->kd_matakuliah,
				'nama_mk' => $teach->nama_matakuliah,
				'sks' => $teach->sks_matakuliah,
				'kelas' => $teach->kelas,
				'hari' => notohari($teach->hari),
				'jam_mulai' => $teach->waktu_mulai,
				'jam_selesai' => $teach->waktu_selesai
			];
		}
		$response = ['status' => 1, 'message' => 'success', 'data' => $data];
		$this->_create_response(200,$response);
	}

	/**
	 * Create response for fail teaching data 
	 * 
	 * @return void
	 */
	private function _fail_teach_res()
	{
		$response = ['status' => 23, 'message' => 'data is empty'];
		$this->_create_response(204,$response);
	}

	/**
	 * To check is object has valid property
	 * 
	 * @param object 	$class
	 * @param array 	$property
	 * @return bool
	 */
	private function _is_property_exist($class, $property)
	{
		$i = 0;
		foreach ($class as $key => $value) {
			if (!property_exists($class, $property[$i])) {
				$data[] = 'invalid parameter for '.strtoupper($key);
			}
			$i++;
		}

		if (isset($data)) {
			$response = ['status' => 4, 'message' => 'invalid parameter', 'desc' => $data];
			$this->_create_response(400, $response);
		}
		return;
	}

	/**
	 * Check the number of parameter should be sent, if invalid then reject
	 * 
	 * @param object 	$prop
	 * @param int 		$total_param
	 * @return void
	 */
	private function _is_number_of_property_valid($prop, $total_param)
	{
		if (count((array)$prop) != $total_param) {
			$response = ['status' => 4, 'message' => 'invalid parameter', 'desc' => 'number of parameter is invalid'];
			$this->_create_response(400, $response);
		}
		return;
	}

	/**
	 * Get active academic year
	 * 
	 * @return void
	 */
	public function active_year()
	{
		$payload = ['nama_tahun' => get_thnajar(getactyear()), 'kode_tahun' => getactyear()];
		$response = ['status' => 1, 'message' => 'success', 'data' => $payload];
		$this->_create_response(200, $response);
	}

	/**
	 * Get list of academic year
	 * 
	 * @return void
	 */
	public function year_list()
	{
		$years = $this->db->get('tbl_tahunakademik')->result();
		$response = ['status' => 1, 'message' => 'success', 'data' => $years];
		$this->_create_response(200, $response);
	}

	/**
	 * Get lecturer position data
	 * 
	 * @return void
	 */
	public function lecturer_data()
	{
		$accept_req   = file_get_contents('php://input');
		$decode_req   = json_decode($accept_req);

		$this->_is_number_of_property_valid($decode_req,1);
		$this->_is_property_exist($decode_req, ['userid']);

		$lecturer = $this->bkdm->get_lecturer_position($decode_req->userid);

		$response = ['status' => 200, 'message' => 'success', 'data' => $lecturer];
		$this->_create_response(200, $response);
	}
	
	/**
    * Get list lecturer  data
    * 
    * @return void
    */
   public function lecturer_list()
   {
           $accept_req   = file_get_contents('php://input');
           $decode_req   = json_decode($accept_req);

           $this->_is_number_of_property_valid($decode_req,1);
           $this->_is_property_exist($decode_req, ['filter']);

           $lecturer = $this->bkdm->get_lecture_list($decode_req->filter);

           $lectures = [];

           foreach ($lecturer->result() as $val) {
                   $lectures[] = $val->nid;
           }

           $response = ['status' => 200, 'message' => 'success', 'data' => $lectures];
           $this->_create_response(200, $response);
   }

   /**
    * Get dean name
    * 
    * @return string
    */
	public function get_dean()
	{
		$accept_req = file_get_contents('php://input');
		$decode_req = json_decode($accept_req);
		$dean = $this->bkdm->get_dean($decode_req->faculty);
		$response = ['status' => 200, 'message' => 'success', 'data' => ['nidn' => $dean->nidn, 'name' => $dean->nama]];
		$this->_create_response(200, $response);
	}
}

/* End of file Api_bkd.php */
/* Location: ./application/modules/api/controllers/Api_bkd.php */