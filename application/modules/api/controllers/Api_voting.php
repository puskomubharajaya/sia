<?php
defined('BASEPATH') or exit('No direct script access allowed');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Api_voting extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->input->get_request_header('x-voting-key') !== APPKEYS) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
		$this->load->library(['ion_auth', 'bcrypt']);
		$this->load->model('ion_auth_model');
	}

	public function index($value = '')
	{
		echo "Ini index ya";		
	}

	public function attemp_login()
	{
		$payload = file_get_contents('php://input');
		$decodePayload = json_decode($payload);

		$ok = $this->login_model->cekuser_new($decodePayload->username, $decodePayload->password);

		if ($ok) {
			// prepare user response
			$this->_prepare_response($this->ion_auth->user()->row());
		} else {
			$response = ['status' => 60, 'message' => 'user not found'];
			$this->output
				->set_status_header(404)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
	}

	private function _prepare_response(...$params)
	{

		$user_log = $this->login_model->data_user_new($params[0]->username)->row();

		$session['userid'] = $user_log->NIMHSMSMHS;
		$session['nama']  = $user_log->NMMHSMSMHS;
		$session['prodi'] = ['code' => $user_log->kd_prodi, 'name' => $user_log->prodi];
		$session['fakultas'] = ['code' => $user_log->kd_fakultas, 'name' => $user_log->fakultas];

		$response = ['status' => 1, 'message' => 'authentication success', 'data' => $session];
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}


	/**
	 * 
	 * Mendapatkan seluruh mahasiswa
	 * Bisa berdasarkan prodi dan fakultas atau tanpa keduanya
	 * @example query param ?type=int&kode=int
	 * type : 
	 * 		2 : fakultas
	 * 		3 : prodi
	 */
	public function amount_of_active_student()
	{
		$activeYear = getactyear();
		$this->load->model('api_voting_model', 'api_voting');
		
		$totalActiveStudent = $this->api_voting->get_students($activeYear, $_GET);
		
		$response = ['status' => 200, 'message' => 'get amount of active student success', 'result' => $totalActiveStudent];

		$this->_create_response(200, $response);
	}

	public function student_name($npm)
	{
		$studentName = $this->db->where('NIMHSMSMHS', $npm)->get('tbl_mahasiswa')->row()->NMMHSMSMHS;

		$response = ['status' => 1, 'message' => 'get student name success', 'result' => $studentName];
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}


	function get_user()
	{
		echo uri_string();

		$CI = &get_instance();
		$this->db2 = $CI->load->database('siabaru', TRUE);
		$qry = $this->db2->query("SELECT * FROM users limit 10");
		echo "<pre>";
		print_r($qry->result());

		// $db2 = $this->load->database('siabaru', true);

		// $list = $db2->get("users");

		// echo "<pre>";
		// print_r ($list);
		// echo "</pre>";
	}

	function fakultas()
	{
		$payload = file_get_contents('php://input');
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$db2 = $this->load->database('siabaru', TRUE);
			if ($payload == '') {
				$data = $db2->query("SELECT kd_fakultas as id, fakultas as name FROM tbl_fakultas;")->result();
			} else {
				$data = $db2->query("SELECT kd_fakultas as id, fakultas as name FROM tbl_fakultas WHERE kd_fakultas = " . $payload . ";")->row();
			}
			$status = 200;
			$response = ['status' => $status, 'message' => 'Success', 'data' => $data];
		} else {
			$status = 405;
			$response = ['status' => $status, 'message' => 'method not allowed'];
		}

		$this->output
			->set_status_header($status)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

	function prodi()
	{
		$payload = file_get_contents('php://input');
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$db2 = $this->load->database('siabaru', TRUE);
			if ($payload == '') {
				$data = $db2->query("SELECT kd_prodi as id, prodi as name FROM tbl_jurusan_prodi;")->result();
			} else {
				$data = $db2->query("SELECT kd_prodi as id, prodi as name FROM tbl_jurusan_prodi WHERE kd_prodi=" . $payload . ";")->row();
			}

			$status = 200;
			$response = ['status' => $status, 'message' => 'Success', 'data' => $data];
		} else {
			$status = 405;
			$response = ['status' => $status, 'message' => 'method not allowed'];
		}

		$this->output
			->set_status_header($status)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}

	public function mhs()
	{
		$db2     = $this->load->database('siabaru', true);
		$payload = file_get_contents('php://input');
		$db2->distinct();
		$db2->select("NIMHSMSMHS,NMMHSMSMHS,KDPSTMSMHS");
		$db2->from('tbl_mahasiswa');
		$db2->like('NMMHSMSMHS', $payload, 'both');
		$db2->or_like('NIMHSMSMHS', $payload, 'both');
		$sql     = $db2->get();
		$val     = array();

		foreach ($sql->result() as $row) {
			$val[] = array(
				"label"        => $row->NIMHSMSMHS . " - " . $row->NMMHSMSMHS,
				"value"        => $row->NIMHSMSMHS . " - " . $row->NMMHSMSMHS,
				"npm"          => $row->NIMHSMSMHS,
				"nama"         => $row->NMMHSMSMHS,
				"prodi"        => $row->KDPSTMSMHS,
				"prodiname"    => get_jur2($row->KDPSTMSMHS),
				"fakultas"     => get_kdfak_byprodi2($row->KDPSTMSMHS),
                "fakultasname" => get_fak2(get_kdfak_byprodi2($row->KDPSTMSMHS))

			);
		}

		$response = ['status' => 200, 'message' => 'success', 'data' => $val];
		$this->_create_response(200, $response);
	}
	/**
	 * Create response for all request
	 * @param int 	$status_code
	 * @param array $response
	 * @return void
	 */
	private function _create_response($status_code, $response)
	{
		$this->output
			->set_status_header($status_code)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();
		exit();
	}
}

/* End of file Api_voting.php */
/* Location: ./application/modules/api/controllers/Api-voting.php */