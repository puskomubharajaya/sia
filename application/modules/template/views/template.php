<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="utf-8">

  <?php $user = $this->session->userdata('sess_login');

  if ($user['user_type'] == 1) {

    $nama = $this->app_model->getdetail('tbl_karyawan','nid',$user['userid'],'nik','asc')->row(); 
    $name = $nama->nama;

  } elseif($user['user_type'] == 3) {

    $nama = $this->app_model->getdetail('tbl_divisi','kd_divisi',$user['userid'],'kd_divisi','asc')->row(); 
    $name = $nama->divisi;

  } elseif($user['user_type'] == 4) {

      $nama = $this->app_model->getdetail('tbl_karyawan_2','nip',$user['userid'],'nip','asc')->row(); $name = $nama->nama;

  } elseif($user['user_type'] == 5 || $user['user_type'] == 6 || $user['user_type'] == 7) {

    $nama = $this->app_model->getdetail('tbl_divisi','kd_divisi',$user['username'],'kd_divisi','asc')->row(); 
    $name = $nama->divisi;

  } else {

    $nama = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$user['userid'],'NIMHSMSMHS','asc')->row(); 
    $name = $nama->NMMHSMSMHS;

  } ?>

  <title>Siakad UBJ | <?= $name?></title>
  <link rel="shortcut icon" href="<?= base_url(); ?>assets/logo.ico"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <meta name="apple-mobile-web-app-capable" content="yes">

  <link href="<?= base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?= base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">

  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

  <link href="<?= base_url();?>assets/css/font-awesome.css" rel="stylesheet">

  <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet">

  <link href="<?= base_url();?>assets/css/pages/dashboard.css" rel="stylesheet">

  <link href="<?= base_url();?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

  <link href="<?= base_url();?>assets/css/pages/reports.css" rel="stylesheet">

  <link href="<?= base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css" rel="stylesheet">

  <script src="<?= base_url();?>assets/js/jquery-1.7.2.min.js"></script> 

  <script src="<?= base_url();?>assets/js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script> 

</head>

<body style="background: rgba(0, 0, 0, 0) url('https://www.toptal.com/designers/subtlepatterns/patterns/full-bloom.png') repeat scroll 0% 0%;">

<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container"> 
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </a>
      <a class="brand" href="#">
        <img src="<?= base_url();?>assets/logo.png" style="width:40px;float:left;margin-top:-8px;margin-bottom:-13px">
        <span style="margin-left:10px;line-height:-15px">SISTEM INFORMASI AKADEMIK UBHARA JAYA</span>
      </a>

      <div class="nav-collapse">

        <ul class="nav pull-right">

          <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:16px">

              <i class="icon-user"></i> <?= $name; ?> <b class="caret"></b></a>

            <ul class="dropdown-menu">

              <li><a href="<?= base_url();?>extra/account">Ganti Password</a></li>

              <li><a href="<?= base_url();?>auth/logout">Logout</a></li>

            </ul>

          </li>

        </ul>

      </div>

      <!--/.nav-collapse --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /navbar-inner --> 

</div>

<!-- /navbar -->

<div class="subnavbar">

  <div class="subnavbar-inner">

    <div class="container">

      <ul class="mainnav">

        <li class="active" ><a href="<?= base_url();?>home"><i class="icon-home"></i><span >Dashboard</span> </a>

        <?php $q = $this->role_model->getparentmenu()->result(); foreach ($q as $menu) { if (($menu->url) == '-') { ?>

        <li class="dropdown active"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">

        <?php } elseif (($menu->url) == '#') { ?>

        <li class="active" ><a href="<?= $menu->url; ?>">

        <?php } else { ?>

        <li class="active" ><a href="<?= base_url();?><?= $menu->url; ?>">

        <?php } ?>

          <i class="<?= $menu->icon; ?>"></i><span ><?= $menu->menu; ?></span> </a>

        <?php if (($menu->url) == '-') { ?>

            <ul class="dropdown-menu">

              <?php $qd = $this->role_model->getmenu($menu->id_menu)->result(); foreach ($qd as $row) { ?>

                <?php if (($row->url) == '#') { ?>

                  <li><a href="<?= $row->url; ?>">

                    <?php } else { ?>

                      <li><a href="<?= base_url();?><?= $row->url; ?>">

                    <?php } ?>

                    <?= $row->menu; ?></a></li>

                  <?php } ?>

                </ul>

              <?php } ?>

            </li>

        <?php } ?>

      </ul>

    </div>

    <!-- /container --> 

  </div>

  <!-- /subnavbar-inner --> 

</div>

<!-- /subnavbar -->

<div class="main">

  <div class="main-inner">

    <div class="container">

      <!-- load page -->

      <?php $this->load->view($page); ?>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

<!-- /main -->

<div class="footer">

  <div class="footer-inner">

    <div class="container">

      <div class="row">

        <div class="span12"> &copy; <?php date_default_timezone_set('Asia/Jakarta');echo date('Y'); ?> - <a target="_blank" href="http://ubharajaya.ac.id/">Universitas Bhayangkara Jakarta Raya</a>. <b><i>Last Login : <?php $cek = $this->app_model->getdetail('tbl_user_login','userid',$user['userid'],'userid','asc')->row()->last_login; echo $cek; ?></i></b></div>

        <!-- /span12 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /footer-inner --> 

</div>

<script src="<?= base_url();?>assets/js/bootstrap.js"></script>
<script src="<?= base_url();?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="<?= base_url();?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>

<script type="text/javascript">
  $(function() {

      $("#example0").dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true,
        "iDisplayLength": 100
      });

      $("#example1").dataTable();

      $("#example3").dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true
      });

      $("#example4").dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true
      });

      $("#example5").dataTable();
      $("#example6").dataTable();
      $("#example7").dataTable();
      
      $("#example8").dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true
      });

      $("#example9").dataTable({
        "bLengthChange": false,
        "bFilter": true,
        "bSort": false,
        "bAutoWidth": true
      });

      $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": true
      });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    var calendar = $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      selectable: true,
      selectHelper: true,
      select: function(start, end, allDay) {
        var title = prompt('Event Title:');
        if (title) {
          calendar.fullCalendar('renderEvent',
            {
              title: title,
              start: start,
              end: end,
              allDay: allDay
            },
            true // make the event "stick"
          );
        }
        calendar.fullCalendar('unselect');
      },
      editable: true,

      <?php $kalender = $this->app_model->getdata('tbl_kalender_dtl','mulai','asc')->result(); ?>

      events: [
        <?php foreach ($kalender as $value) { lastQ();?>
        {
          title: '<?php echo $value->kegiatan; ?>',
          start: '<?php echo $value->mulai; ?>',
          end: '<?php echo $value->akhir; ?>'
        },
        <?php } ?>
      ]
    });
  });
</script>



<style type="text/css" !important>
  @import url(http://fonts.googleapis.com/css?family=Old+Standard+TT:400,700);
  [data-notify="container"][class*="alert-pastel-"] {
    background-color: rgb(255, 255, 238) !important;
    border-width: 0px !important;
    border-left: 15px solid rgb(255, 240, 106) !important;
    border-radius: 0px !important;
    box-shadow: 0px 0px 5px rgba(51, 51, 51, 0.3) !important;
    font-family: 'Old Standard TT', serif !important;
    letter-spacing: 1px !important;
  }

  [data-notify="container"].alert-pastel-info {
    border-left-color: rgb(255, 179, 40) !important;
  }

  [data-notify="container"].alert-pastel-danger {
    border-left-color: rgb(255, 103, 76) !important;
  }

  [data-notify="container"][class*="alert-pastel-"] > [data-notify="title"] {
    color: rgb(80, 80, 57) !important;
    display: block !important;
    font-weight: 700 !important;
    margin-bottom: 5px !important;
  }
  
  [data-notify="container"][class*="alert-pastel-"] > [data-notify="message"] {
    font-weight: 400 !important;
  }
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93675871-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>