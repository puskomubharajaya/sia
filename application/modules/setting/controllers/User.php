<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(4)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['user'] = $this->app_model->getdetail('view_user','user_type !=','4','id_user','asc')->result();
		$data['usergroup'] = $this->app_model->getdata('tbl_user_group','id_user_group','asc')->result();
		$data['kary'] = $this->app_model->getdata('tbl_karyawan','nik','asc')->result();
		$data['page'] = 'setting/user_view';
		$this->load->view('template/template',$data);
	}

	function delete($id)
	{
		$delete = $this->app_model->deletedata('tbl_user_login','id_user',$id);
		if ($delete == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."setting/user';</script>";
		} else {
			echo "<script>alert('Gagal Hapus Data');history.go(-1);</script>";
		}
	}

	function save()
	{
		$data['username']= $this->input->post('username', TRUE);
		$data['userid']= $this->input->post('userid', TRUE);
		$data['status']= $this->input->post('status', TRUE);
		$data['id_user_group']= $this->input->post('usergroup', TRUE);
		$data['password_plain']= $this->input->post('password', TRUE);
		$data['user_type'] = $this->input->post('tipe', TRUE);
		$data['password']= sha1(md5($data['password_plain']).key);
		$insert = $this->app_model->insertdata('tbl_user_login',$data);
		if ($insert == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."setting/user';</script>";
		} else {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		}
	}

	function edit($id)
	{
		$data['usergroup'] = $this->app_model->getdata('tbl_user_group','id_user_group','asc')->result();
		$data['detail'] = $this->app_model->getdetail('tbl_user_login','id_user',$id,'id_user','asc')->row();
		$data['page'] = 'setting/user_edit';
		$this->load->view('template/template',$data);
	}

	function update($id)
	{
		$data['password_plain']= $this->input->post('password', TRUE);
		$data['password']= sha1(md5($data['password_plain']).key);
		$data['username']= $this->input->post('username', TRUE);
		$data['userid']= $this->input->post('userid', TRUE);
		$data['user_type'] = $this->input->post('tipe', TRUE);
		$data['status']= $this->input->post('status', TRUE);
		$data['id_user_group']= $this->input->post('usergroup', TRUE);
		$update = $this->app_model->updatedata('tbl_user_login','id_user',$id,$data);
		if ($update == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."setting/user';</script>";
		} else {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		}
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */