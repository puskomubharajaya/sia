<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jdl_pmb extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['rows'] = $this->db->get('tbl_jdl_pmb')->result();
		$data['kegiatan'] = $this->db->get('tbl_kegiatan_pmb')->result();

		$data['page'] = 'v_jdlpmb_list';
		$this->load->view('template/template', $data);
	}

	function jdl_save(){
		extract(PopulateForm());

		 if ($ahir < $mulai) {
	            echo "<script>alert('Cek Kembali Masa Kegiatan !!');history.go(-1);</script>";
	            exit();
	        }else{
	            $object = array(
								'gelombang' => $gel, 
								'kegiatan' => $keg, 
								'mulai' => $mulai,
								'ahir' => $ahir,
								'keterangan' => $ket,   
								);

	            //var_dump($object);die();

	            $q = $this->db->insert('tbl_jdl_pmb', $object);

	            if ($q) {
	                redirect(base_url('setting/jdl_pmb/','refresh'));
	            }else{
	                echo "<script>alert('Kesalahan Input Kegiatan !!');history.go(-1);</script>";
	                exit();  
	            }
	        }
	}

}

/* End of file Jdl_pmb.php */
/* Location: ./application/controllers/Jdl_pmb.php */