<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">FORM DATA</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>setting/usergroup/updatedata" method="post">
    <div class="modal-body" style="margin-left:-30px;">    
        <input type="hidden" name="id" value="<?php echo $getEdit->id_user_group; ?>">
        <div class="control-group" id="">
            <label class="control-label">User Group</label>
            <div class="controls">
                <input type="text" class="span4" name="textfield1" placeholder="Input User Group" class="form-control" value="<?php echo $getEdit->user_group; ?>" required/>
            </div>
        </div>
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Save changes"/>
    </div>
</form>