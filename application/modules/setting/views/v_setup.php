<!-- toggle switch -->
<link href="<?php echo base_url(); ?>assets/js/bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>

<!-- masked input -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.maskedinput.js') ?>"></script>

<script type="text/javascript">

	$(document).ready(function() {
     	$("#tgl, #tgl2, #tgl-3" ).datepicker({
          	changeMonth: true,
          	changeYear: true,
          	dateFormat: "yy-mm-dd"
      	});

      	jQuery(function($){
		   	$("#quota").mask("9999",{placeholder:" "});
		   	$("#yr").mask("9999",{placeholder:" "});
		});
   	});

	$(function () {
   		$(".oplos").change(function() {
			if ($(this).attr('checked')) {
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url() ?>setting/setup_gel/updateOC/'+ 1 + '/' + $(this).val(),
					success: function () {
						$('#oplos').attr('checked','checked');
					}
				});
			} else {
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url() ?>setting/setup_gel/updateOC/'+ 0 + '/' + $(this).val(),
					success: function () {
						$('#oplos').removeAttr('checked','checked');
					}
				});
			}
		});
   	});
   	

	function resetForm()
	{
		var frm = document.getElementById('#formInput');
	   	frm.reset();  // Reset all form data
	   	return false;
	}

	function edit(ed)
	{
		$('#contentEdit').load('<?php echo base_url('setting/setup_gel/loadEdit/'); ?>'+ed);
	}

	function delRow(id) {
		if (confirm('Anda ingin menghapus data ini ?')) {
			$.ajax({
	            type: 'POST',
	            url: '<?= base_url('setting/setup_gel/delRows/');?>'+id,
	            error: function (xhr, ajaxOptions, thrownError) {
	                return false;           
	            },
	            success: function () {
	                getTable();
	            }
	        });
		}
    }  

	function getTable() {
		$.post('<?php echo base_url(); ?>setting/setup_gel/loadTable/', function(data) {
			$('#tblBody').html(data);
        	// $("#tblBody").load('<?php echo base_url(); ?>setting/setup_gel/loadTable/');
    	});
    }

    $(function () {
		$('#sbm').click(function (e) {
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url(); ?>setting/setup_gel/add_wave',
				data: $('#formInput').serialize(),
				error: function (xhr, ajaxOption, thrownError) {
					return false;
				},
				beforeSend: function () {
					$('#onCenter').html("<center><img style='width:10%' src='<?php base_url('assets/img/loader.gif'); ?>'></center>");
				},
				success: function () {
					$('#myModal').modal('hide');
	                getTable();
				}
			});
			e.preventDefault();
		});
	});

</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Set Up Gelombang</h3>
			</div>
			<div class="widget-content">
				
				<fieldset>

					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#menu1">Gelombang Aktif</a></li>
					    <!-- <li><a data-toggle="tab" href="#home">Input Gelombang</a></li> -->
					    <li><a data-toggle="tab" href="#setup">Tutup Kuota Prodi</a></li>
					</ul>
					<div class="tab-content">

					 	<!-- tab I input dan atur rentang gelombang -->
					    <div id="menu1" class="tab-pane fade in active" id="onCenter">
					    	<form class="form-horizontal" action="<?php echo base_url(); ?>setting/setup_gel/update_gel" method="post" enctype="multipart/form-data">
						    	<div class="control-group">
									<label class="control-label">Gelombang Aktif</label>
									<div class="controls">
										<a class="btn btn-primary" href="javascript:;"><b><?php echo $qr->row()->gelombang; ?></b></a>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label">Gelombang</label>
									<div class="controls">
										<select class="form-control span4" name="tahun" required/>
											<option disabled="" selected="">--Pilih Gelombang--</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">Ekstra</option>
										</select>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label">Status</label>
									<div class="controls">
										<input type="text" class="span4" value="ON" readonly="">
									</div>
								</div>

								<div class="form-actions">
									<input class="btn btn-large btn-success" type="submit" value="Submit">
								</div>
							</form>
					    </div>

					    <!-- tab II atur gelombang aktif -->
					    <div id="home" class="tab-pane fade">
					    	<button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal"><i class="icon icon-plus"></i> Tambah Data</button>
					    	<hr>
					    	<table class="table table-stripped" id="example1">
					    		<thead>
					    			<tr>
					    				<th>Kode Gelombang</th>
					    				<th width="50">Gelombang</th>
					    				<th>Rentang Waktu</th>
					    				<th>Tanggal Ujian</th>
					    				<th>Kuota</th>
					    				<th style="text-align: center">Aksi</th>
					    			</tr>
					    		</thead>
					    		<tbody id="tblBody">
					    			<?php foreach ($qw->result() as $val) { ?>
										<tr>
											<td><?php echo $val->kd_gel; ?></td>
											<td><?php echo $val->gelombang; ?></td>
											<td><?php echo TanggalIndoRange($val->str_date,$val->end_date); ?></td>
											<td><?php echo TanggalIndo($val->exm_date); ?></td>
											<td><?php echo $val->kuota; ?></td>
											<td style="text-align: center">
												<button data-toggle="modal" data-target="#editModal" class="btn btn-primary" title="Edit data" onclick="edit(<?php echo $val->id_gel ?>)"><i class="icon icon-pencil"></i></button>
												<button class="btn btn-danger" onclick="delRow(<?php echo $val->id_gel ?>)" title="Hapus data"><i class="icon icon-remove"></i></button>
											</td>
										</tr>
									<?php } ?>
					    		</tbody>
					    	</table>
					    </div>

					    <!-- atur tutup buka prodi -->
					    <div id="setup" class="tab-pane fade">
					    	<table class="table table-stripped" id="example1">
					    		<thead>
					    			<tr>
					    				<th>Prodi</th>
					    				<th>Status</th>
					    				<th style="text-align: center">Aksi</th>
					    			</tr>
					    		</thead>
					    		<tbody id="tblBody">
					    			<?php foreach ($pr->result() as $v) { ?>
										<tr>
											<td><?php echo $v->prodi; ?></td>
											<td>
												<?php if ($v->status_kuota == 1) {
													echo "<i><u>Kuota Dibuka</i></u>";
												} else {
													echo "<i><u>Kuota Ditutup</i></u>";
												} ?>
											</td>
											<td style="text-align: center">
												<input data-toggle="toggle" data-size="mini" data-onstyle="success" data-offstyle="danger" class="oplos" type="checkbox" value="<?php echo $v->kd_prodi; ?>" <?php if ($v->status_kuota == 1) { echo "checked";}?>>
											</td>
										</tr>
									<?php } ?>
					    		</tbody>
					    	</table>
					    </div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
</div>


<!-- modal add start -->
<div id="myModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

    	<!-- Modal content-->
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	        	<h4 class="modal-title">Tambah Data Gelombang</h4>
	      	</div>
	      	<div class="modal-body">
	    		<form class="form-horizontal" id="formInput" action="" method="post">
			    	<div class="control-group">
						<label class="control-label">Gelombang</label>
						<div class="controls">
							<select class="form-control span3" name="gelombang" required/>
								<option disabled="" selected="">--Pilih Gelombang--</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">Ekstra</option>
							</select>
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Rentang waktu</label>
						<div class="controls">
							<input type="text" class="span2" name="strdate" id="tgl" placeholder="Tanggal Mulai">
							
							<input type="text" class="span2" name="enddate" id="tgl2" placeholder="Tanggal Akhir">
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Tanggal ujian</label>
						<div class="controls">
							<input type="text" class="span3" name="exmdate" id="tgl-3" value="" placeholder="Tanggal Mulai">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Kuota</label>
						<div class="controls">
							<input type="text" id="quota" class="span3" name="kuota" placeholder="Kuota Gelombang">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Tahun</label>
						<div class="controls">
							<input type="text" class="span3" id="yr" name="tahun" placeholder="Tahun PMB">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">Keterangan</label>
						<div class="controls">
							<textarea class="span3" name="not"></textarea>
						</div>
					</div>
				
		      	</div>
		      	<div class="modal-footer">
		      		<input class="btn btn-success" id="sbm" onclick="resetForm()" type="submit" value="Submit">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      	</div>
	    	</div>
    	</form>
  	</div>
</div>
<!-- modal end -->

<!-- modal edit -->
<div id="editModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

    	<!-- Modal content-->
    	<div class="modal-content" id="contentEdit">
	      	
    	</div>

  	</div>
</div>
<!-- end modal edit