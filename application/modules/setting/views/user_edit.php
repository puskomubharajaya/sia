<script type="text/javascript">
$(document).ready(function(){
    $(".auto-complete").autocomplete({
        source: function(request, response) {
                $.ajax({ url: "<?php echo base_url();?>setting/autocomplete/get_autocomplete_user/"+$("#tipe").val(),
                data: { term: $(".auto-complete").val().replace(/ /g,'')},
                dataType: "json",
                type: "POST",
                success: function(data){
                    response(data);
                }
            });
        },
        select: function (event, ui) {
            var id = ui.item.id;
            $("#id-complete").val(id);
        },
        minLength: 1
    });
});
</script>

<div class="row">
  <div class="span12">
    <div class="widget">
      <div class="widget-header">
        <i class="icon-star"></i>
        <h3>FORM EDIT</h3>
      </div> <!-- /widget-header -->
      <div class="widget-content">
        <b><center>Edit User</center></b><br>
        <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>setting/user/update/<?php echo $detail->id_user; ?>">
          <fieldset>
            <div class="control-group">                     
              <label class="control-label">Username</label>
              <div class="controls">
                <input type="text" class="span3" name="username" value="<?php echo $detail->username;?>"/>
              </div> <!-- /controls -->       
            </div> <!-- /control-group -->
            <div class="control-group">                     
              <label class="control-label">Password</label>
              <div class="controls">
                <input type="password" class="span3" name="password" value="<?php echo $detail->password_plain;?>"/>
              </div> <!-- /controls -->       
            </div> <!-- /control-group -->
            <div class="control-group" id="">
                <label class="control-label">Tipe</label>
                <div class="controls">
                    <select class="span4" name="tipe" class="form-control" id="tipe" required>
                        <option disabled selected> -- Pilih -- </option>
                        <option value="1">Karyawan</option>
                        <option value="2">Mahasiswa</option>
                    </select>
                </div>
            </div>
            <div class="control-group" id="">
                <label class="control-label">ID Pengguna</label>
                <div class="controls">
                    <input type="text" class="span4 auto-complete form-control" placeholder="Input No Identitas" required/>
                    <input type="hidden" id="id-complete" name="userid" required/>
                </div>
            </div>
            <div class="control-group" id="">
                <label class="control-label">User Group</label>
                <div class="controls">
                    <select class="span3" name="usergroup" class="form-control" required>
                        <option> -- Pilih -- </option>
                        <?php foreach ($usergroup as $row) { ?>
                         <option value="<?php echo $row->id_user_group;?>"> <?php echo $row->user_group;?> </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="control-group" id="">
              <label class="control-label">Status</label>
              <div class="controls">
                  <label class="radio inline">
                  <input type="radio" name="status" value="1">
                  Aktif
                </label>
                <label class="radio inline">
                  <input type="radio" name="status" value="0">
                  Tidak
                </label>
              </div>
            </div>
            <div class="form-actions" style="margin-bottom: -30px;">
              <input type="submit" class="btn btn-primary" value="Update"/> 
              <input type="reset" class="btn btn-warning" value="Cancel"/>
            </div> <!-- /form-actions -->
          </fieldset>
        </form>
      </div> <!-- /widget-content -->        
    </div> <!-- /widget -->
  </div>
</div>