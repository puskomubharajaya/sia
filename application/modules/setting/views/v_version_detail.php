<div class="modal-header">
	<h4 class="modal-title">Apa yang baru di versi <?php echo $versi->versi ?>?</h4>
</div>
<div class="modal-body">
	<table width="100%" class="table table-bordered table-striped border-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>User</th>
				<th>Log</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 1;
			foreach ($detail as $row) { ?>
				<tr>

					<td><?php echo $i ?></td>
					<td><?php echo get_group_user($row->user) ?></td>
					<td>
						<?php
							$n = 1;
							$pisah = explode(",", $row->vlog);
							foreach ($pisah as $satuan) {
								echo $n, ". ", $satuan, "<br>";

								$n++;
							}
							?>
					</td>
				</tr>
			<?php $i++;
			} ?>
		</tbody>
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>