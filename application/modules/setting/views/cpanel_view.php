<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>setting/cpanel/getdataedit/'+id);
}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Control Panel</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="50">No</th>
	                        	<th>Panel</th>
	                        	<th width="100">Status</th>
	                            <th width="40">Submit</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($getData as $row) { ?>
	                        <form action="<?php echo base_url(); ?>" method="post">
                            <tr>
	                        	<td><?php echo number_format($no); ?></td>
	                        	<td><?php echo $row->deskripsi; ?></td>
                                <?php if ($row->status == 1) { ?>
                                    <td>On</td>
                                <?php } else { ?>
                                    <td>Off</td>
                                <?php } ?>
	                        	<td class="td-actions">
									<!--button name="submit" class="btn btn-primary btn-small"><i class="btn-icon-only icon-save"> </i></button-->
									<a data-toggle="modal" class="btn btn-small btn-success" href="#editModal" onclick="edit(<?php echo $row->id;?>)"><i class="btn-icon-only icon-save"> </i></a>
								</td>
	                        </tr>
                            </form>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->