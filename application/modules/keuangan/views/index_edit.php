<script type="text/javascript">
                var format = function(num){
                    var str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
                    if(str.indexOf(".") > 0) {
                        parts = str.split(".");
                        str = parts[0];
                    }
                    str = str.split("").reverse();
                    for(var j = 0, len = str.length; j < len; j++) {
                        if(str[j] != ",") {
                            output.push(str[j]);
                            if(i%3 == 0 && j < (len - 1)) {
                                output.push(",");
                            }
                            i++;
                        }
                    }
                    formatted = output.reverse().join("");
                    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
                };

                $(function(){
                    $("#currency2").keyup(function(e){
                        $(this).val(format($(this).val()));
                    });
                });
</script>

<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM EDIT DATA INDEKS PEMBAYARAN</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>keuangan/indexpem/updatedata" method="post">
                <input type="hidden" name="jurusan" value="<?php echo $getEdit->kd_prodi; ?>"/>
                <input type="hidden" name="angkatan" value="<?php echo $getEdit->tahunajaran; ?>"/>
                <input type="hidden" name="kelas" value="<?php echo $getEdit->kelas; ?>"/>
                <input type="hidden" name="id" value="<?php echo $getEdit->id_index; ?>"/>
                <div class="modal-body" style="margin-left: -30px;">  
					<div class="control-group" id="">
                        <label class="control-label">Jenis Pembayaran</label>
                        <div class="controls">
                            <select name="jenis" class="span4 form-control" required>
                                <option disabled >Pilih Jenis Pembayaran</option>
                                <?php foreach ($jenis as $row) { ?>
                                <option value="<?php echo $row->kd_jenis; ?>" <?php if($row->kd_jenis == $getEdit->kd_jenis){ echo "selected";} ?> ><?php echo $row->jenis_pembayaran;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Jumlah</label>
                        <div class="controls">
                            <input type="text" name="jumlah" class="span3 form-control" id="currency2" style="text-align:right;" value="<?php echo number_format($getEdit->jumlah); ?>" required/>
                        </div>
                    </div>				
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>