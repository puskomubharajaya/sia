<?php
    header("Content-Type: application/xls");    
    header("Content-Disposition: attachment; filename=status_mahasiswa_".$tahunajar.".xls");  
    header("Pragma: no-cache"); 
    header("Expires: 0");
?>

<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr> 
            <th>No</th>
            <th>Prodi</th>
            <th>Fakultas</th>
            <th>Aktif</th>
            <th>Cuti</th>
            <th>Non Aktif</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($query as $row) { ?>
        <tr>
            <td><?= $no; ?></td>
            <td><?= $row->prodi; ?></td>
            <td><?= $row->fakultas; ?></td>
            <td><?= $row->aktif; ?></td>
            <td><?= $row->cuti; ?></td>
            <td><?= $row->nonaktif; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>