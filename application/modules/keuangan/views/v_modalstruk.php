<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 class="modal-title" id="myModalLabel">Detail Booking</h3>
</div>
<div class="modal-body">
	<ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Scan</a></li>
      <li><a data-toggle="tab" href="#menu1">Detail</a></li>
    </ul>

    <div class="tab-content">
      	<div id="home" class="tab-pane fade in active">
        	<div class="panel-body">
        		<center>
                    <?php if ($rows->paytipe == 3) {
                        echo '<h4>Calon mahasiswa ini mendaftar melalui pemasaran. Maka tidak ada foto/gambar struk pembayaran yang dapat ditampilkan.</h4>';
                    } else { ?>
					   <img style="width: 70%;" src="http://172.16.1.5:802<?php echo $rows->patch ?>" alt="">
                    <?php } ?>
				</center>
        	</div>
    	</div>
    	<div id="menu1" class="tab-pane fade">
        	<div class="panel-body">
        		<table class="table table-stipped">
        			<tr>
        				<td><b>Pengirim</b></td>
        				<td><?php echo getNamePmb($rows->userid); ?></td>
        			</tr>
        			<?php if ($rows->paytipe == '1') { ?>
        				<tr>
	        				<td><b>No. Rekening</b></td>
	        				<td><?php echo $rows->norek; ?></td>
	        			</tr>
	        			<tr>
	        				<td><b>Bank Asal</b></td>
	        				<td><?php echo getNameBank($rows->bank); ?></td>
	        			</tr>
        			<?php } ?>
        			<tr>
        				<td><b>Prodi</b></td>
        				<td><?php echo get_jur($dets); ?></td>
        			</tr>
        		</table>
        	</div>
    	</div>
    </div>
    
</div>