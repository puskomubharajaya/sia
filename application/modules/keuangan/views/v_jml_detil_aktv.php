<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-group"></i>
  				<h3>Detail Mahasiswa <?= $status ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?= base_url(); ?>keuangan/status_ujian/input_data" method="post">
                        <table>
                            <tr>
                                <td>Prodi</td>
                                <td>:</td>
                                <td><?= get_jur($prodi); ?></td>
                            </tr>
                            <tr>
                                <td>Tahun Akademik</td>
                                <td>:</td>
                                <td><?= $tahunajar; ?></td>
                            </tr>
                        </table>
                        <br>
                        <a class="btn btn-warning" href="<?= base_url(); ?>keuangan/report_jml/load">
                            <i class="icon-chevron-left"></i> Kembali
                        </a>
                        <a class="btn btn-success" href="<?= base_url(); ?>keuangan/report_jml/print_excel">
                            <i class="icon-print"></i> Print Excel
                        </a>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>SKS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($data as $row) { ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $row->NIMHSMSMHS; ?></td>
                                    <td><?= $row->NMMHSMSMHS; ?></td>
                                    <td>
                                        <a target="_blank" href="<?= base_url('keuangan/report_jml/view_krs_mhs/'.$row->kd_krs)?>">
                                        <?= $row->tot; ?></a>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>