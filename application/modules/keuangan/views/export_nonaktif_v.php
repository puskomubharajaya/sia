<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=Data_Mahasiswa_Nonaktif_".str_replace(' ', '_', get_jur($prodi))."_".$tahunajar.".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr> 
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($data as $row) { ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= $row->NIMHSMSMHS; ?></td>
            <td><?= $row->NMMHSMSMHS; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>