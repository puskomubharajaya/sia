<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-group"></i>
  				<h3>Detail Mahasiswa <?= $status ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a class="btn btn-warning" href="<?= base_url('keuangan/report_jml/load'); ?>">
                        <i class="icon-chevron-left"></i> Kembali
                    </a>
                    <a class="btn btn-success" href="<?= base_url('keuangan/report_jml/export_nonaktif/'.$prodi); ?>">
                        <i class="icon-print"></i> Print Excel
                    </a>
                    <br><br>
                    <table>
                        <tr>
                            <td>Prodi</td>
                            <td>:</td>
                            <td><?= get_jur($prodi); ?></td>
                        </tr>
                        <tr>
                            <td>Tahun Akademik</td>
                            <td>:</td>
                            <td><?= $tahunajaran; ?></td>
                        </tr>
                    </table>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>NPM</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telepon</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data as $row) { ?>
                            <tr>
                                <td><?= $no ?></td>
                                <td><?= $row->NIMHSMSMHS; ?></td>
                                <td><?= $row->NMMHSMSMHS; ?></td>
                                <td><?= $row->email; ?></td>
                                <td><?= $row->no_hp; ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>