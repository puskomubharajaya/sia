<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=MAHASISWA_BELUM_DAFTAR_ULANG_".$year.".xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table id="" class="table table-bordered table-striped">
    <thead>
    	<tr>
    		<th colspan="4" rowspan="2"><?php echo get_jur($prod); ?></th>
    	</tr>
    	<tr></tr>
        <tr> 
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Angkatan</th>
        </tr>
    </thead>
    <tbody>
    	<?php $no = 1; foreach ($load->result() as $key) { ?>
    		<tr>
	    		<td><?php echo $no; ?></td>
	    		<td><?php echo $key->NIMHSMSMHS; ?></td>
	    		<td><?php echo $key->NMMHSMSMHS; ?></td>
	    		<td><?php echo $key->TAHUNMSMHS; ?></td>
	    	</tr>
    	<?php $no++; } ?>
    </tbody>
</table>