<?php $this->dbreg = $this->load->database('regis', TRUE); ?>
<?php $sessgel = $this->session->userdata('wavereg'); ?>
<script type="text/javascript">
    function lookup(u,k) {
        $("#contentStruk").load('<?php echo base_url()?>keuangan/valid_regis/viewStruk/'+u+'/'+k);
    }
</script>

<script>
    function apdet(id) {
        //alert(id);
        $.ajax({
            type: 'POST',
            url: '<?= base_url('keuangan/valid_regis/updatestat/');?>'+id,
            error: function (xhr, ajaxOptions, thrownError) {
                return false;           
            },
            beforeSend: function() {
                $('.td-actions-'+id).html("<center><img style='width:50%' src='<?= base_url();?>/assets/img/ajax-loader.gif' /></center>");
            },
            success: function () {
                getRow(id);
            }
        });
    }

    function getRow(id) {
        $.post('<?= base_url('keuangan/valid_regis/loadRow/');?>'+id, function(data) {
            $('#row-act-'+id).html(data);
        });
    }
</script>
<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Daftar Registrasi Calon Mahasiswa Gelombang <?= $sessgel['gelombang'] ?></h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <a href="<?php echo base_url('keuangan/valid_regis/destroy'); ?>" class="btn btn-warning" title=""><i class="icon icon-chevron-left"></i> Kembali</a>
                    <hr>
                    <table id="example1" class="table table-striped">
                        <thead>
                            <tr> 
                                <th>Nama</th>
                                <th>ID Booking</th>
                                <th>Gelombang</th>
                                <th>Prodi</th>
                                <th>Metode Pendaftaran</th>
                                <th>Status Pembayaran</th>
                                <th width="80">Lihat Struk</th>
                                <th width="40">Validasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach ($regs->result() as $value) : 

                                // give color sign to be differentiator betweeen who has paid off and hasn't
                                if ($value->valid == 2) {
                                     $sty = "style=''";
                                 } elseif ($value->valid == 1) {
                                     $sty = "style='background:#FFD700'";
                                 } elseif ($value->valid == 0) {
                                     $sty = "style='background:#F08080'";
                                 }
                                 // end color flag

                            ?>
                            <tr id="row-act-<?php echo $value->key; ?>">

                                <td <?php echo $sty; ?> ><?php echo getNamePmb($value->userid); ?></td>
                                <td <?php echo $sty; ?> ><?php echo $value->key; ?></td>
                                <td <?php echo $sty; ?> ><?php echo substr($value->userid,2,1); ?></td>

                                <td <?php echo $sty; ?> ><?php echo get_jur($value->prodi); ?></td>

                                <!-- metode daftar -->
                                <?php if ($value->gate == 1) {
                                    $mtd = 'Online';
                                } else {
                                    $mtd = 'Datang ke tempat';
                                } ?>
                                <td <?= $sty ?> ><?= $mtd ?></td>

                                <!-- inisialisasi status -->
                                <?php if ($value->valid == 2) {
                                        $stsreg = 'Lunas Registrasi';
                                    } elseif ($value->valid == 1) {
                                        $stsreg = 'Menunggu Validasi BPAK';
                                    } elseif ($value->valid == 0) {
                                        $stsreg = 'Belum Lunas Registrasi';
                                    }
                                 ?>
                                <!-- inisialisasi status end -->

                                <td <?php echo $sty; ?> ><i><u><?php echo $stsreg; ?></u></i></td>

                                <!-- cek apakah sudah membayar/upload struk -->
                                <?php $hei = $this->dbreg->query("SELECT * from tbl_payment where key_booking = '".$value->key."'")->num_rows();
                                if ($value->valid < 1) { ?>
                                     <td <?php echo $sty; ?> >
                                        <a href="javascript:void(0);" class="btn btn-danger" onclick="alert('Bandit ini belum membayar!')"><i class="icon icon-remove"></i></a>
                                    </td>
                                <?php } else { ?>
                                    <td <?php echo $sty; ?> >
                                        <a href="#modalstruk" onclick="lookup(<?php echo $value->userid; ?>+'/'+<?php echo $value->key; ?>)" data-toggle="modal" title="lihat struk" class="btn btn-success"><i class="icon icon-eye-open"></i></a>
                                    </td>
                                <?php } ?>

                                <td class="td-actions-<?php echo $value->key; ?>" <?php echo $sty; ?> >
                                    <?php if ($value->valid == 2) { ?>
                                        <a href="javascript:void(0);" onclick="alert('Sudah Divalidasi!')" class="btn btn-success" title=""><i class="icon icon-ok"></i></a>
                                    <?php } else { ?>
                                        <button class="btn btn-danger" id="yes-btn" onclick="apdet('<?php echo $value->key; ?>')" title="validasi"><i class="icon icon-refresh"></i></button>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php $no++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- modal lihat struk -->
<div class="modal fade" id="modalstruk" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="contentStruk">
            
        </div>
    </div>
</div>