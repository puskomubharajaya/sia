<script type="text/javascript">
    function edit(idf) {
        $("#edit").load('<?php echo base_url()?>keuangan/indeks/edit/'+idf);
    }

</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Indeks Pembayaran</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <a data-toggle="modal" href="#myModal" class="btn btn-primary"> Tambah Data </a> <a href="#" class="btn btn-success"> Export Data </a><br><hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Kode</th>
                                <th>Program Studi</th>
                                <th>Fakultas</th>
                                <th>Kelas</th>
                                <th>Pembayaran</th>
                                <th>Jumlah</th>
                                <th>Tahun Ajaran</th>
                                <th width="80">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($getData as $row){?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->kode; ?></td>
                                <td><?php echo $row->prodi; ?></td>
                                <td><?php echo $row->fakultas; ?></td>
                                <?php switch ($row->kelas) {
                                    case 'PG':
                                        $kls = 'PAGI';
                                        break;
                                    case 'SR':
                                        $kls = 'SORE';
                                        break;
                                    case 'PK':
                                        $kls = 'P2K';
                                        break;
                                    
                                    default:
                                        $kls = 'SEMUA';
                                        break;
                                } ?>
                                <td><?php echo $kls; ?></td>
                                <td><?php echo $row->keterangan; ?></td>
                                <td><?php echo number_format($row->jumlah); ?></td>
                                <td><?php echo $row->ta; ?></td>
                                <td class="td-actions">
                                    <a onclick="edit(<?php echo $row->id_index; ?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url()?>keuangan/indeks/delete/<?php echo $row->id_index; ?>"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Form Data</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url()?>keuangan/indeks/save" method="post">
                    <script type="text/javascript">

                    var format = function(num){
                    var str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
                    if(str.indexOf(".") > 0) {
                        parts = str.split(".");
                        str = parts[0];
                    }
                    str = str.split("").reverse();
                    for(var j = 0, len = str.length; j < len; j++) {
                        if(str[j] != ",") {
                            output.push(str[j]);
                            if(i%3 == 0 && j < (len - 1)) {
                                output.push(",");
                            }
                            i++;
                        }
                    }
                    formatted = output.reverse().join("");
                    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
                };

                $(function(){
                    $("#currency").keyup(function(e){
                        $(this).val(format($(this).val()));
                    });
                });

                </script>
                <div class="modal-body" style="margin-left: -30px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Program Studi</label>
                        <div class="controls">
                            <select name="prodi" class="form-control span4" required>
                            <option disabled selected>Pilih Program Studi</option>
                            <?php foreach ($fakultas->result() as $key) {
                                echo "<option disabled><i><b> -- ".$key->fakultas." -- </b></i></option>";
                                $prodi = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$key->kd_fakultas,'kd_fakultas','asc')->result();
                                foreach ($prodi as $value) {
                                    echo "<option value='".$value->kd_prodi."'> ".$value->prodi." </option>";
                                }
                            } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Kelas</label>
                        <div class="controls">
                            <select name="kelas" class="form-control span4" required>
                                <option disabled selected>Pilih Kelas</option>
                                <option value="0">SEMUA</option>
                                <option value="PG">PAGI</option>
                                <option value="SR">SORE</option>
                                <option value="PK">P2K</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Kode Pembayaran</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kode" id='kode' placeholder="Input Kode" class="form-control" value="" style="text-transform:uppercase" required/>
                        </div>
                    </div>              
                    <div class="control-group" id="">
                        <label class="control-label">Pembayaran</label>
                        <div class="controls">
                            <input type="text" class="span4" name="keterangan" placeholder="Keterangan Pembayaran" class="form-control" value="" required/>
                        </div>
                    </div>
                     <div class="control-group" id="">
                        <label class="control-label">Jumlah</label>
                        <div class="controls">
                            <input style="text-align: right" type="text" id="currency" name="jumlah" placeholder="Jumlah Pembayaran" class="form-control span4" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Tahun Ajaran</label>
                        <div class="controls">
                            <select name="ta" class="form-control span4" required>
                                <option disabled selected>Pilih Tahun Ajaran</option>
                                <?php foreach ($ta->result() as $key2) {
                                    echo "<option value='".$key2->id_tahunajaran."'>".$key2->tahunajaran."</option>";
                                } ?>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->