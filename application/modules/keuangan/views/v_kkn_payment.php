<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<h3><i class="icon-money"></i> Pembayaran KKN</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
                    
                <div class="span11">
                    <form class="form-horizontal" action="<?= base_url('keuangan/kkn_payment/save_tahun_ajaran'); ?>" method="post">
                        <fieldset>
                            <div class="control-group">
                                <!-- set year range -->
                                <?php $year = date('Y'); $range = $year - 7; ?>
                                <label class="control-label" for="">Angkatan</label>
                                <div class="controls">
                                    <select name="angkatan" class="span6" required>
                                        <option disabled selected> -- Pilih Angkatan -- </option>
                                        <?php for ($i=$range; $i <= $year; $i++) { ?>
                                            <option value="<?= $i; ?>"><?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div> <!-- /controls -->               
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="">Prodi</label>
                                <div class="controls">
                                    <select name="prodi" class="span6" required>
                                        <option disabled selected> -- Pilih Prodi -- </option>
                                        <?php foreach ($prodi as $prodis) { ?>
                                            <option value="<?= $prodis->kd_prodi; ?>"><?= $prodis->prodi; ?></option>
                                        <?php } ?>
                                    </select>
                                </div> <!-- /controls -->               
                            </div>

                            <div class="control-group">                                         
                                <label class="control-label" for="">Tahun Akademik</label>
                                <div class="controls">
                                    <select name="tahunajaran" class="span6" required>
                                        <option disabled selected> -- Pilih Tahun Akademik -- </option>
                                        <?php foreach ($tahunajaran as $tahun_ajaran) { ?>
                                            <option value="<?= $tahun_ajaran->kode; ?>"><?= $tahun_ajaran->tahun_akademik; ?></option>
                                        <?php } ?>
                                    </select>
                                </div> <!-- /controls -->               
                            </div> <!-- /control-group -->
                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
                </div>
				
			</div>
		</div>
	</div>
</div>