<?php date_default_timezone_set('Asia/Jakarta') ; ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquerymasked/src/jquery.maskedinput.js"></script>
<script type="text/javascript">
    function edit(edl) {
        $("#edit_lantai").load('<?php echo base_url()?>organisasi/lantai/load_lantai/'+edl);
    }
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Sinkronisasi BRI</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a class="btn btn-primary" data-toggle="modal" href="#myModal" >Tambah Data</a><br><hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Sync Date</th>
                                <th>Transaction</th>
                                <th>File</th>
                                <th>User</th>
                                <!-- <th width="118">Aksi</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($p as $row) { ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $row->date_sync;?></td>
                                <td><?php echo $row->jumlah_transaksi; ?></td>
                                <td><a target="_blank" href="<?php echo base_url();?><?php echo $row->kode_file;?>"><?php echo $row->kode_file;?></a></td>
                                <?php $karyawan = $this->app_model->getdetail('tbl_karyawan','nik',$row->user,'nik','asc')->row(); ?>
                                <td><?php echo $karyawan->nama; ?></td>
                                <!-- <td class="td-actions">
                                    <a class="btn btn-success btn-small" href="" ><i class="btn-icon-only icon-ok"> </i></a>
                                    <a onclick="edit()" class="btn btn-primary btn-small" data-toggle="modal" href="#editModal" ><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>keuangan/delete"><i class="btn-icon-only icon-remove"> </i></a>
                                </td> -->
                            </tr>
                            <?php $no++; }?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_lantai">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>keuangan/upload_sinc/upload" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Sync Date</label>
                        <div class="controls">
                            <input type="text" id="lt" class="span4" name="tanggal" value="<?php echo date('Y-m-d');?>" class="form-control"  disabled/>
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">File</label>
                        <div class="controls">
                            <input type="file" class="span4" name="userfile">
                        </div>
                    </div> 
                    <div class="control-group" id="">
                        <label class="control-label">Deskripsi</label>
                        <div class="controls">
                            <textarea class="span4 form-control" name="desk"></textarea>
                        </div>
                    </div>              
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->