<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Validasi Semester Perbaikan/Khusus Mahasiswa</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>keuangan/status_sp/simpan_sesi">
                        <fieldset>
                      <script>
                              $(document).ready(function(){
                                $('#faks').change(function(){
                                  $.post('<?php echo base_url()?>keuangan/status_sp/get_jurusan/'+$(this).val(),{},function(get){
                                    $('#jurs').html(get);
                                  });
                                });
                              });
                              </script>
                              <div class="control-group">
                                <label class="control-label">Fakultas</label>
                                <div class="controls">
                                  <select class="form-control span6" name="fakultas" id="faks">
                                    <option>--Pilih Fakultas--</option>
                                    <?php foreach ($fakultas as $row) { ?>
                                    <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                              

                              <div class="control-group">
                                <label class="control-label">Prodi</label>
                                <div class="controls">
                                  <select class="form-control span6" name="jurusan" id="jurs">
                                    <option>--Pilih Jurusan--</option>
                                  </select>
                                </div>
                              </div>

                              <div class="control-group">
                                <label class="control-label">Semester</label>
                                <div class="controls">
                                  <select class="form-control span6" name="semester">
                                    <?php $improve = yearImprove(); ?>
                                      <optgroup label="Kelas Perbaikan">
									  <?php foreach ($ta as $tahun){?>
										<option value="<?php echo $tahun->kode+2 ?>"><?php echo $tahun->kode+2 ?></option>
									  <?php }?>
									  </optgroup>
                                      <optgroup label="Kelas Khusus"> 
									  <?php  $thn_skr = date('Y');
											for ($a=2015; $a<=$thn_skr; $a++) {?> 
										<option value="<?php echo $a ?>5"><?php echo $a ?>5</option>  
									  <?php }?> 
									  </optgroup>
                                  </select>
                                </div>
                              </div>
							  
                            <br/>
                              
                            <div class="form-actions">
                                <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                            </div> <!-- /form-actions -->
                        </fieldset>
                    </form>
          
        </div>
      </div>
    </div>
  </div>
</div>
