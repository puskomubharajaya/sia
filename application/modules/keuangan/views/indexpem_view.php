
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<h3>Data Index Pembayaran</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                      <li class="active">
                        <a href="#form1" data-toggle="tab">Form Index Pembayaran Per Program Studi</a>
                      </li>
                      <li><a href="#form2" data-toggle="tab">Form Index Pembayaran Universitas</a></li>
                    </ul>           
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="form1">
                            <div class="span11">
                                <script>
                                  $(document).ready(function(){
                                    $('#faks').change(function(){
                                      $.post('<?php echo base_url()?>keuangan/indexpem/get_jurusan/'+$(this).val(),{},function(get){
                                        $('#jurs').html(get);
                                      });
                                    });
                                  });
                                  </script>
                                <form class="form-horizontal" action="<?php echo base_url(); ?>keuangan/indexpem/save_seasson" method="post">
                                    <input type="hidden" name="id" value="2">
                                    <fieldset>
                                        <div class="control-group">                                         
                                            <label class="control-label" for="">Tahun Masuk</label>
                                            <div class="controls">
                                                <select name="tahunajaran" class="span6" required>
                                                    <option disabled selected> -- Pilih Tahun Masuk -- </option>
                                                    <?php $i = 2016 ; $batas = $i-8; for ($i=2016; $batas <= $i; $i--) { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div> <!-- /controls -->               
                                        </div> <!-- /control-group -->

                                        <div class="control-group">                                         
                                            <label class="control-label" for="">Fakultas</label>
                                            <div class="controls">
                                                <select name="fakultas" class="span6" id="faks" required>
                                                    <option disabled selected> -- Pilih Fakultas -- </option>
                                                    <?php foreach ($fakultas as $row) { ?>
                                                    <option value="<?php echo $row->kd_fakultas.'-'.$row->fakultas;?>"><?php echo $row->fakultas;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div> <!-- /controls -->               
                                        </div> <!-- /control-group -->

                                        <div class="control-group">                                         
                                            <label class="control-label" for="">Jurusan</label>
                                            <div class="controls">
                                                <select name="jurusan" class="span6" id="jurs" required>
                                                    <option disabled selected> -- Pilih Jurusan -- </option>
                                                </select>
                                            </div> <!-- /controls -->               
                                        </div> <!-- /control-group -->
                                            <!-- <div class="control-group">                                         
                                            <label class="control-label" for="">Kelas</label> -->
                                            <!-- <div class="controls">
                                                <select name="shift" class="span6" required>
                                                    <option disabled selected> -- Pilih kelas -- </option>
                                                    <option value="RP"> Reguler Pagi </option>
                                                    <option value="RS"> Reguler Sore </option>
                                                    <option value="NR"> Non-Reguler </option>
                                                </select>
                                            </div> <!-- /controls              
                                        </div> <!-- /control-group --> 
                                        
                                        <br />
                                          
                                        <div class="form-actions">
                                            <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                                        </div> <!-- /form-actions -->
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="form2">
                            <div class="span11">
                                <form class="form-horizontal" action="<?php echo base_url(); ?>keuangan/indexpem/save_seasson" method="post">
                                <input type="hidden" name="id" value="1">
                                    <fieldset>
                                        <div class="control-group">                                         
                                            <label class="control-label" for="">Tahun Masuk</label>
                                            <div class="controls">
                                                <select name="tahunajaran" class="span6" required>
                                                    <option disabled selected> -- Pilih Tahun Masuk -- </option>
                                                    <?php $i = 2016; $batas = $i-8; for ($i=2016; $batas <= $i; $i--) { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div> <!-- /controls -->               
                                        </div> <!-- /control-group -->
                                        <!-- <div class="control-group">                                         
                                            <label class="control-label" for="">Kelas</label>
                                            <div class="controls">
                                                <select name="shift" class="span6" required>
                                                    <option disabled selected> -- Pilih kelas -- </option>
                                                    <option value="RP"> Reguler Pagi </option>
                                                    <option value="RS"> Reguler Sore </option>
                                                    <option value="NR"> Non-Reguler </option>
                                                </select>
                                            </div> <!-- /controls                
                                        </div> --> <!-- /control-group -->
                                        <br />
                                        <div class="form-actions">
                                            <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                                        </div> <!-- /form-actions -->
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
				
			</div>
		</div>
	</div>
</div>