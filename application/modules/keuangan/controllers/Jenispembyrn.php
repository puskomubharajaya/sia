<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenispembyrn extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(57)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	
		$data['pembayaran']=$this->app_model->getdata('tbl_jenis_pembayaran','kd_jenis','asc');
		$data['page'] = 'keuangan/jenispembyrn_view';
		$this->load->view('template/template',$data);
	}

	function del_pembayaran($id)
	{
		$this->app_model->deletedata('tbl_jenis_pembayaran','id_jenis',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."keuangan/Jenispembyrn';</script>";
	}


	function save_pembayaran()
	{
		$data = array(
		'kd_jenis' 	=> $this->form_validation->set_rules('kode', 'Kode Fakultas', 'trim|required|xss_clean'),
		'jenis_pembayaran' 		=> $this->form_validation->set_rules('pembayaran', 'Kode Fakultas', 'trim|required|xss_clean')
		);

		//var_dump($data);die();
		if ($this->form_validation->run() == FALSE) {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		} else {
			$this->app_model->insertdata('tbl_jenis_pembayaran',$data);
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."keuangan/Jenispembyrn';</script>";
		}
	}


	function update_pembayaran()
	{
		$data = array(
			'kd_jenis'	=> $this->form_validation->set_rules('kode', 'Kode Fakultas', 'trim|required|xss_clean'),
			'jenis_pembayaran' => $this->form_validation->set_rules('pembayaran', 'Kode Fakultas', 'trim|required|xss_clean')
		);
		if ($this->form_validation->run() == FALSE) {
			echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		} else {
			$this->app_model->updatedata('tbl_jenis_pembayaran','id_jenis',$this->input->post('id_jenis'),$data);
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."keuangan/Jenispembyrn';</script>";
		}
	}

	function get_list_pembyrn($idf)
	{
		// $data['pembayaran']=$this->app_model->getdata('tbl_jenis_pembayaran','kd_jenis','asc');
		$data['list'] = $this->db->query("select * from tbl_jenis_pembayaran where id_jenis = '$idf'")->row();
		$this->load->view('keuangan/edit_pembayaran_view',$data);
	}	

}

/* End of file jenispembyrn.php */
/* Location: ./application/modules/keuangan/controllers/jenispembyrn.php */