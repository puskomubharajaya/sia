<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_jml extends CI_Controller {

	private $userid, $usergroup;

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login')) {
			$cekakses = $this->role_model->cekakses(117)->result();
			if (!$cekakses) {
				echo "<script>alert('Anda Tidak Berhak Mengakses!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('keuangan/report_model','report');
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
	}

	public function index()
	{
		$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = "v_report";
		$this->load->view('template/template', $data);
	}

	public function simpan_sesi()
	{
        $tahunajaran = $this->input->post('tahunajaran');  
        $this->session->set_userdata('tahunajaran_report_sts', $tahunajaran);
		redirect(base_url('keuangan/report_jml/load'));
	}

	public function load()
	{
		$this->session->unset_userdata('prodi_report_sts');
		$tahunajaran = $this->session->userdata('tahunajaran_report_sts');
		
		if ($this->usergroup == '8' || $this->usergroup == '19') {
			$data['query'] = $this->report->jml_mhs_per_sts($tahunajaran, $this->userid);	
		} else {
			$data['query'] = $this->report->all_mhs_per_sts($tahunajaran);
		}

		$data['tahunajar'] = get_thnajar($tahunajaran);
		$data['page']  = "v_detil_report";
		$this->load->view('template/template', $data);
	}

	public function detil_jml_aktv($prodi)
	{
		$this->session->set_userdata('prodi_report_sts', $prodi);
		$data['status']    = 'Aktif';
		$tahunajaran       = $this->session->userdata('tahunajaran_report_sts');
		$data['data']      = $this->report->jml_detil($prodi,$tahunajaran);
		$data['prodi']     = $prodi;
		$data['tahunajar'] = get_thnajar($tahunajaran);
		$data['page']      = "v_jml_detil_aktv";
		$this->load->view('template/template', $data);
	}

	public function view_krs_mhs($kd_krs)
	{
		$mhs = $this->db->get_where('tbl_mahasiswa',['NIMHSMSMHS' => substr($kd_krs, 0,12)])->row();
		$data['rows']  = $this->report->detail_krs($kd_krs, $this->session->userdata('prodi_report_sts'));
		$data['prodi'] = $this->session->userdata('prodi_report_sts');
		$data['nim']   = $mhs->NIMHSMSMHS;
		$data['nama']  = $mhs->NMMHSMSMHS;
		$data['page']  = 'v_krs_mhs';
		$this->load->view('template/template', $data);
	}

	public function detil_jml_cuti($prodi)
	{
		$data['status']    = 'Cuti';
		$tahunajaran       = $this->session->userdata('tahunajaran_report_sts');
		$data['data']      = $this->app_model->jml_cuti($prodi,$tahunajaran);
		$data['prodi']     = $prodi;
		$data['tahunajar'] = get_thnajar($tahunajaran);
		$data['page']      = "v_detil_mhs_cuti";
		$this->load->view('template/template', $data);
	}

	public function detil_jml_non($prodi)
	{
		$data['status']      = 'Non aktif';
		$tahunajaran         = $this->session->userdata('tahunajaran_report_sts');
		$data['data']        = $this->report->jml_nonaktif($prodi,$tahunajaran);
		$data['prodi']       = $prodi;
		$data['tahunajaran'] = get_thnajar($tahunajaran);
		$data['page']        = "v_detil_mhs_non";
		$this->load->view('template/template', $data);
	}

	public function print_excel()
	{
		$data['tahunajar'] = $this->session->userdata('tahunajaran_report_sts');
		$data['hasil'] = $this->report->jml_detil($this->session->userdata('prodi_report_sts'),$data['tahunajar']);
		$data['prodi'] = $this->session->userdata('prodi_report_sts');
		$this->load->view('excel_hasil', $data);
	}

	public function export_total_status()
	{
		$tahunajaran = $this->session->userdata('tahunajaran_report_sts');
		
		if ($this->usergroup == '8' || $this->usergroup == '19') {
			$data['query'] = $this->report->jml_mhs_per_sts($tahunajaran, $this->userid);	
		} else {
			$data['query'] = $this->report->all_mhs_per_sts($tahunajaran);
		}

		$data['tahunajar'] = $tahunajaran;
		$this->load->view('export_status_v', $data);
	}

	public function export_cuti($prodi)
	{
		$tahunajaran       = $this->session->userdata('tahunajaran_report_sts');
		$data['data']      = $this->app_model->jml_cuti($prodi,$tahunajaran);
		$data['prodi']     = $prodi;
		$data['tahunajar'] = $tahunajaran;
		$this->load->view('export_cuti', $data);
	}

	public function export_nonaktif($prodi)
	{
		$tahunajaran         = $this->session->userdata('tahunajaran_report_sts');
		$data['data']        = $this->report->jml_nonaktif($prodi,$tahunajaran);
		$data['prodi']       = $prodi;
		$data['tahunajar']   = $tahunajaran;
		$this->load->view('export_nonaktif_v', $data);
	}

}

/* End of file Report_jml.php */
/* Location: ./application/modules/keuangan/controllers/Report_jml.php */