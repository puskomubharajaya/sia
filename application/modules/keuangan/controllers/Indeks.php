<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indeks extends CI_Controller {

	public function index()
	{
		$data['getData'] = $this->app_model->getindekspembayaran(0)->result();
		$data['fakultas'] = $this->app_model->getdata('tbl_fakultas','kd_fakultas','asc');
		$data['ta'] = $this->app_model->getdata('tbl_tahunajaran','tahunajaran','asc');
		$data['page'] = 'keuangan/indeks_view';
		$this->load->view('template/template',$data);
	}

	function save()
	{
		$data['kode'] = strtoupper($this->input->post('kode', TRUE));
		$data['keterangan'] = $this->input->post('keterangan', TRUE);
		$data['kd_prodi'] = $this->input->post('prodi', TRUE);
		$data['kelas'] = $this->input->post('kelas', TRUE);
		$data['jumlah'] = str_replace(',', '', $this->input->post('jumlah', TRUE));
		$data['tahunajaran'] = $this->input->post('ta', TRUE);
		$table = 'tbl_index_pembayaran';
		$this->app_model->insertdata($table,$data);
		echo "<script>alert('Sukses');
				document.location.href='".base_url()."keuangan/indeks';</script>";
	}

	function edit($value)
	{
		$data['fakultas'] = $this->app_model->getdata('tbl_fakultas','kd_fakultas','asc');
		$data['ta'] = $this->app_model->getdata('tbl_tahunajaran','tahunajaran','asc');
		$data['getData'] = $this->app_model->getindekspembayaran($value)->row();
		$this->load->view('indeks_edit',$data);
	}

	function update()
	{
		$data['kode'] = strtoupper($this->input->post('kode', TRUE));
		$data['keterangan'] = $this->input->post('keterangan', TRUE);
		$data['kd_prodi'] = $this->input->post('prodi', TRUE);
		$data['kelas'] = $this->input->post('kelas', TRUE);
		$data['jumlah'] = str_replace(',', '', $this->input->post('jumlah', TRUE));
		$data['tahunajaran'] = $this->input->post('ta', TRUE);
		$table = 'tbl_index_pembayaran';
		$this->app_model->updatedata($table,'id_index',$this->input->post('id', TRUE),$data);
		echo "<script>alert('Sukses');
				document.location.href='".base_url()."keuangan/indeks';</script>";
	}

	function delete($id)
	{
		$table = 'tbl_index_pembayaran';
		$this->app_model->deletedata($table,'id_index',$id);
		echo "<script>alert('Sukses');
				document.location.href='".base_url()."keuangan/indeks';</script>";
	}

}

/* End of file Indeks.php */
/* Location: ./application/modules/keuangan/controllers/Indeks.php */