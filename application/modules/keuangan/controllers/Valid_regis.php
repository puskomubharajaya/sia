<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Valid_regis extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->dbreg = $this->load->database('regis', TRUE);
		if ($this->session->userdata('sess_login') !=  TRUE) {
			echo '<script>alert("Silahkan log-in kembali!");</script>';
			redirect(base_url('auth/logout'),'refresh');
		}
	}

	public function index() 
	{
		$data['page'] = "v_gel_regis";
		$this->load->view('template/template', $data);
	}

	function sess_reg() 
	{
		extract(PopulateForm());
		$array = array(
			'gelombang' => $gel,
			'tahun'	=> $year
		);
		$this->session->set_userdata('wavereg', $array);
		redirect(base_url('keuangan/valid_regis/load_reg_usr'), 'refresh');
	}

	function load_reg_usr() 
	{
		$sessreg = $this->session->userdata('wavereg');
		$thn = substr($sessreg['tahun'], 2,4);
		
		$data['db'] = $this->dbreg;
		$data['regid']= $sessreg['gelombang'];
		$data['regs'] = $this->dbreg->query("SELECT a.*, b.gate from tbl_booking a join tbl_regist b on a.userid = b.userid where substring(a.userid, 3,1) = '".$sessreg['gelombang']."' and valid IN (1,2) and substring(a.userid, 1,2) = '".$thn."'");
		$data['page'] = "v_valid_regis";
		$this->load->view('template/template', $data);
	}

	function reg_list($reg)
	{
		$regs = $this->dbreg->query("SELECT * from tbl_regist where substring(userid, 3,1) = '".$reg."'")->result();
		echo json_encode($data);
	}

	function destroy() {
		$this->session->unset_userdata('wavereg');
		redirect(base_url('keuangan/valid_regis'), 'refresh');
	}

	function updatestat($key) {
		$usr = $this->session->userdata('sess_login');
		$data = array(
			'valid' 		=> 2,
			'valid_admin' 	=> $usr['userid'],
			'valid_adm_date'=> date('Y-m-d')
		);
		$this->dbreg->where('key', $key);
		$this->dbreg->update('tbl_booking', $data);
	}

	function loadRow($id) {
		$getuk = $this->dbreg->where('key',$id)->get('tbl_booking')->row()->userid;
		$data['forKey'] = $id;

		$this->dbreg->where('userid', $getuk);
		$this->dbreg->where('key', $id);
		$data['forRow'] = $this->dbreg->get('tbl_booking')->row();

		// $data['forRow'] = $this->dbreg->query("SELECT * from tbl_booking where userid = '" . $getuk . "' and key")->row();
		$this->load->view('v_loadrow', $data);
	}

	function viewStruk($u,$b)
	{
		$data['rows'] = $this->dbreg->query("SELECT * from tbl_payment where userid = '".$u."' and key_booking = '".$b."'")->row();
		$data['dets'] = $this->dbreg->select('prodi')->from('tbl_booking')->where('key',$b)->get()->row()->prodi;
		$this->load->view('v_modalstruk', $data);
	}

}

/* End of file Valid_regis.php */
/* Location: ./application/modules/keuangan/controllers/Valid_regis.php */