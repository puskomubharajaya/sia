<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model {

	function all_mhs_per_sts($tahunajar)
	{
		$min_year = substr($tahunajar, 0,4) - 8;
		$data = $this->db->query("SELECT
									pr.kd_prodi,
									pr.prodi,
									fk.fakultas,

									(SELECT COUNT(npm_mahasiswa) FROM tbl_verifikasi_krs
									WHERE kd_jurusan = pr.kd_prodi
									AND tahunajaran = '$tahunajar') AS aktif,

									(SELECT COUNT(npm) FROM tbl_status_mahasiswa st
									JOIN tbl_mahasiswa mhs ON st.npm = mhs.NIMHSMSMHS
									WHERE mhs.KDPSTMSMHS = pr.kd_prodi
									AND st.status = 'C'
									AND st.tahunajaran = '$tahunajar'
									AND st.validate = 1) AS cuti,

									(SELECT COUNT(NIMHSMSMHS) 
                                    FROM tbl_mahasiswa 
                                    WHERE NIMHSMSMHS NOT IN (
                                        SELECT npm_mahasiswa FROM tbl_verifikasi_krs 
                                        WHERE tahunajaran = '$tahunajar'
                                    ) AND NIMHSMSMHS NOT IN (
                                        SELECT npm FROM tbl_status_mahasiswa 
                                        WHERE tahunajaran = '$tahunajar'
                                        AND validate = 1 
                                        AND tahunajaran = '$tahunajar'
                                    ) 
                                    AND KDPSTMSMHS = pr.kd_prodi
                                    AND BTSTUMSMHS >= '$tahunajar'
                                    AND STMHSMSMHS IN ('A', 'N')
                                    AND TAHUNMSMHS > '$min_year'
                                    AND SMAWLMSMHS <= '$tahunajar') AS nonaktif

								FROM tbl_jurusan_prodi pr 
								JOIN tbl_fakultas fk ON pr.kd_fakultas = fk.kd_fakultas
								GROUP BY pr.kd_prodi")->result();
		return $data;
	}

	function jml_mhs_per_sts($tahunajar, $kd_prodi)
	{
		$min_year = substr($tahunajar, 0,4) - 8;
		$data = $this->db->query("SELECT
									pr.kd_prodi,
									pr.prodi,
									fk.fakultas,

									(SELECT COUNT(npm_mahasiswa) FROM tbl_verifikasi_krs
									WHERE kd_jurusan = pr.kd_prodi
									AND tahunajaran = '$tahunajar') AS aktif,

									(SELECT COUNT(npm) FROM tbl_status_mahasiswa st
									JOIN tbl_mahasiswa mhs ON st.npm = mhs.NIMHSMSMHS
									WHERE mhs.KDPSTMSMHS = pr.kd_prodi
									AND st.status = 'C'
									AND st.tahunajaran = '$tahunajar'
									AND st.validate = 1) AS cuti,

									(SELECT COUNT(NIMHSMSMHS) 
                                    FROM tbl_mahasiswa 
                                    WHERE NIMHSMSMHS NOT IN (
                                        SELECT npm_mahasiswa FROM tbl_verifikasi_krs 
                                        WHERE tahunajaran = '$tahunajar'
                                    ) AND NIMHSMSMHS NOT IN (
                                        SELECT npm FROM tbl_status_mahasiswa 
                                        WHERE tahunajaran = '$tahunajar'
                                        AND validate = 1 
                                        AND tahunajaran = '$tahunajar'
                                    ) 
                                    AND KDPSTMSMHS = pr.kd_prodi
                                    AND BTSTUMSMHS >= '$tahunajar'
                                    AND STMHSMSMHS IN ('A', 'N')
                                    AND TAHUNMSMHS > '$min_year'
                                    AND SMAWLMSMHS <= '$tahunajar') AS nonaktif

								FROM tbl_jurusan_prodi pr 
								JOIN tbl_fakultas fk ON pr.kd_fakultas = fk.kd_fakultas
								WHERE pr.kd_prodi = '$kd_prodi'
								GROUP BY pr.kd_prodi")->result();
		return $data;
	}

	function jml_detil($prodi, $tahunakademik)
	{
		return $this->db->query("SELECT DISTINCT 
									d.`NIMHSMSMHS`,
									d.`NMMHSMSMHS`,
									a.`semester_krs`,
									a.`kd_krs`,
									SUM(c.`sks_matakuliah`) AS tot
								FROM tbl_krs a 
								JOIN tbl_matakuliah c ON a.`kd_matakuliah` = c.`kd_matakuliah`
								JOIN tbl_mahasiswa d ON a.`npm_mahasiswa` = d.`NIMHSMSMHS`
								WHERE a.kd_krs LIKE CONCAT (a.npm_mahasiswa,'$tahunakademik%') 
								AND d.KDPSTMSMHS = '$prodi'
								AND c.`kd_prodi` = '$prodi' 
								GROUP BY a.`kd_krs`")->result();
	}

	function detail_krs($kd_krs, $prodi)
	{
		$this->db->select('mk.kd_matakuliah,mk.nama_matakuliah,mk.sks_matakuliah');
		$this->db->from('tbl_krs krs');
		$this->db->join('tbl_matakuliah mk', 'krs.kd_matakuliah = mk.kd_matakuliah', 'left');
		$this->db->where('kd_prodi', $prodi);
		$this->db->where('kd_krs', $kd_krs);
		return $this->db->get()->result();
	}

	function jml_nonaktif($kd_prodi, $tahunajaran)
	{
		$begin = substr($tahunajaran, 0,4) - 8;
		$data = $this->db->query("SELECT 
									mhs.NIMHSMSMHS,
									mhs.NMMHSMSMHS,
									bio.email,
									bio.no_hp 
								FROM tbl_mahasiswa mhs
								LEFT JOIN tbl_bio_mhs bio ON mhs.NIMHSMSMHS = bio.npm
								WHERE mhs.NIMHSMSMHS NOT IN (
    								SELECT npm_mahasiswa FROM tbl_verifikasi_krs WHERE tahunajaran = '$tahunajaran'
								) AND mhs.NIMHSMSMHS NOT IN (
    								SELECT npm FROM tbl_status_mahasiswa WHERE tahunajaran = '$tahunajaran' 
    								AND validate = 1 
    								AND tahunajaran = '$tahunajaran'
								) 
								AND mhs.KDPSTMSMHS = '$kd_prodi' 
								AND mhs.BTSTUMSMHS >= '$tahunajaran' 
								AND mhs.STMHSMSMHS IN ('A','N') 
								AND mhs.TAHUNMSMHS > '$begin'
								AND mhs.SMAWLMSMHS <= '$tahunajaran' ")->result();
		return $data;
	}

}

/* End of file report_model.php */
/* Location: ./application/modules/keuangan/models/report_model.php */