<link href="<?php echo base_url();?>assets/css/plugins/bootstrap.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css">

<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui.js"></script>

<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">



<script type="text/javascript">

$(document).ready(function() {

	$('#alamat_ortu').hide();



	// $('#cek_alamat').change(function() {

	// 	$('#alamat_ortu').show();

	// });

$('#tanggal_lahir').datepicker({dateFormat: 'yy-mm-dd',

								yearRange: "1945:2015",

								changeMonth: true,

      							changeYear: true

      							});

$('#tgl_bayar').datepicker({dateFormat: 'yy-mm-dd'});

	

});

</script>



<div class="row">

	<div class="span12">      		  		

  		<div class="widget ">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Form Biodata</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content">

				<div class="tabbable">

							<ul class="nav nav-tabs">

							  <li class="active">

							    <a href="#formcontrols" data-toggle="tab">IDENTITAS DIRI</a>

							  </li>

							  <li>

							  	<a href="#jscontrols" data-toggle="tab">AKADEMIK</a>

							  </li>

							</ul>

							

							<br>

							<form id="edit-profile" class="form-horizontal" method="POST" action="<?php echo base_url();?>data/biodata/save_data">

								<div class="tab-content ">

									<div class="tab-pane active" id="formcontrols">

										

									<fieldset>

										<h3 style="text-align:center" >IDENTITAS DIRI</h3>

										<br>

										<input type="hidden" name="bio_type" value="<?php echo $cek; ?>">

										<?php if ($cek == 2){ ?>

										<div class="control-group">											

											<label class="control-label" for="npm">NPM</label>

											<div class="controls">

												<input type="text" class="span6 " name="npm" id="npm" value="<?php echo $nopok->NIMHSMSMHS ?>" readonly>

												<p class="help-block">Bila NPM tidak sesuai dengan yang tertera di KTM dapat menghubungi BAA</p>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->												

										<?php }else{ ?>

											<div class="control-group">											

											<label class="control-label" for="npm">NIK</label>

											<div class="controls">

												<input type="text" class="span6" name="nik" id="nik" value="<?php echo $nopok->nik; ?>" readonly>

												<p class="help-block">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<?php } ?>

										

										<div class="control-group">											

											<label class="control-label" for="nama">Nama Lengkap</label>

											<div class="controls">

												<input type="text" class="span6" placeholder="Isi dengan Nama Lengkap" id="nama" name="nama" value="<?php if($cek == 2) { echo $nopok->NMMHSMSMHS; }else{echo $nopok->nama ;}  ?>">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group" id="jekel">

					                        <label class="control-label">Jenis kelamin</label>

					                        <div class="controls">

					                            <input type="radio" name="jekel" value="1" required/> Laki-Laki

					                            <input type="radio" name="jekel" value="2" required/> Perempuan

					                        </div>

					                    </div>

										

										

										<div class="control-group">											

											<label class="control-label" for="tempat_lahir">Tempat/Tanggal Lahir</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Isi dengan Tempat lahir" id="tempat_lahir" nama="tempat_lahir" required>

												<input type="text" class="span2" placeholder="Tanggal Lahir" id="tanggal_lahir" name="tanggal_lahir" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<?php if ($cek == 2){ ?>

											<div class="control-group">											

												<label class="control-label" for="sekolah_asal">Nama dan Alamat Asal SMA/SMK</label>

												<div class="controls">

													<!-- <input type="text" class="span4" id="sekolah_asal" name="sekolah_asal" required> -->

													<textarea class="span3" placeholder="Nama Sekolah Asal" id="sekolah_asal" name="nama_sekolah_asal" required></textarea>

													<textarea class="span3" placeholder="Alamat Sekolah Asal" id="alamat_sekolah_asal" name="alamat_sekolah_asal" required></textarea>

												</div> <!-- /controls -->				

											</div> <!-- /control-group -->	

										<?php } ?>

										

										

										

										<br />

										<h3 style="text-align:center">IDENTITAS DIRI</h3>

										<br>

										

										<div class="control-group">											

											<label class="control-label" for="jalan">Jalan/Dusun</label>

											<div class="controls">

												<input type="text" placeholder="Isi dengan nama Jalan" class="span4" id="jalan" name="jalan" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										

										<div class="control-group">											

											<label class="control-label" for="rtrw">RT/RW</label>

											<div class="controls">

												<input type="text" class="span1" placeholder="RT" id="rt" name="rt" required>

												/

												<input type="text" class="span1" placeholder="RW" id="rw" name="rw" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kel">Kelurahan</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Kelurahan" id="kel" name="kel" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kec">Kecamatan</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Kecamatan" id="kec" name="kec" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kab">Kabupaten</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Kabupaten" id="kab" name="kab" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kota">Kota</label>

											<div class="controls">

												<input type="text" class="span4" id="kota" placeholder="Kota" name="kota" >

												<input type="text" class="span2" id="kdpos" placeholder="Kode POS" name="kdpos" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kab">Provinsi</label>

											<div class="controls">

												<select class="span4" name="provinsi" id="provinsi">

													<option>--Pilih Provinsi--</option>

													<?php foreach ($provinsi->result() as $key): ?>

														<option value="<?php echo $key->lokasi_ID; ?>"><?php echo $key->lokasi_nama; ?></option>

													<?php endforeach ?>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kota">Telepon</label>

											<div class="controls">

												<input type="text" class="span3" id="tlp" placeholder="No Telepon" name="tlp" required>

												<input type="text" class="span3" id="hp" placeholder="No HP" name="hp" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="email">Email</label>

											<div class="controls">

												<input type="text" class="span4" placeholder="Email" id="email" name="email" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<?php if ($cek == 2){ ?>

												<br/>

												<h3 style="text-align:center">ORANG TUA</h3>

												<br>

												

		                                        <div class="control-group">											

													<label class="control-label" for="kota">Nama</label>

													<div class="controls">

														<input type="text" class="span3" id="ayah" placeholder="Ayah" name="ayah" required>

														<input type="text" class="span3" id="ibu" placeholder="Ibu" name="ibu" required>

													</div> <!-- /controls -->				

												</div> <!-- /control-group -->

		                                        

		                                        <div class="control-group">											

													<label class="control-label">Alamat</label>

													<div class="controls">

			                                            <input type="radio" id="cek_alamat" name="cek_alamat" onclick="sepertiatas();" value="1" required/> Sama dengan diatas

								                        <input type="radio" id="cek_alamat" name="cek_alamat" onclick="hilang();" value="2" required/> Lain

		                                            

		                                          	</div><!-- /controls -->		

												</div> <!-- /control-group -->

		                                        

		                                        

		                                        <div class="control-group" id="alamat_ortu">											

													<label class="control-label" for="sekolah_asal">Alamat Orang Tua</label>

													<div class="controls">

														<!-- <input type="text" class="span4" id="sekolah_asal" name="sekolah_asal" required> -->

														<textarea class="span3" placeholder="Alamat orang tua" id="alamat_ortu" name="alamat_ortu"></textarea>

													</div> <!-- /controls -->				

												</div> <!-- /control-group -->

										<?php } ?>

										

                                        

											

										 <br />



										 <div class="form-actions">

											<a href="#jscontrols" data-toggle="tab"><button type="button" class="btn btn-primary">Next</button></a>

										</div> <!-- /form-actions -->

								

									</div>

									

									<div class="tab-pane " id="jscontrols">

										<?php if ($cek == 2){ ?>

											<h3>AKADEMIK</h3>

										<br>

										<script>

			                              $(document).ready(function(){

			                                $('#faks').change(function(){

			                                  $.post('<?php echo base_url()?>data/biodata/get_jurusan/'+$(this).val(),{},function(get){

			                                    $('#jurs').html(get);

			                                  });

			                                });

			                              });

			                              </script>



										<div class="control-group">											

											<label class="control-label" for="kab">Fakultas</label>

											<div class="controls">

												<select class="span4" nama="faks" id="faks">

													<option>--Pilih --</option>

													<?php foreach ($fakultas as $key): ?>

														<option value="<?php echo $key->kd_fakultas; ?>"><?php echo $key->fakultas; ?></option>

													<?php endforeach ?>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label" for="jurs">Program Studi</label>

											<div class="controls">

												<select class="span4" name="jurusan" id="jurs" >

													<option>--Pilih --</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label" for="kelas">Kelas</label>

											<div class="controls">

												<select class="span4" id="kelas" name='kelas'>

													<option>--Pilih --</option>

													<option value="R">Reguler</option>

													<option value="NR">Non Reguler</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label" for="kampus">Kampus</label>

											<div class="controls">

												<select class="span4" name="kampus" id="kampus">

													<option>--Pilih --</option>

													<?php foreach ($kampus as $isi): ?>

														<option value="<?php echo $isi->id_kampus; ?>"><?php echo $isi->nama_kampus; ?></option>

													<?php endforeach ?>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->





										<br>

										<br>



										<div class="control-group">											

											<label class="control-label" for="semester">Semester</label>

											<div class="controls">

												<select class="span4" name="semester" id="semester">

													<option>--Pilih--</option>

													<option valu="1">1</option>

													<option valu="2">2</option>

													<option valu="3">3</option>

													<option valu="4">4</option>

													<option valu="5">5</option>

													<option valu="6">6</option>

													<option valu="7">7</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<div class="control-group">											

											<label class="control-label" for="TA">Tahun Akademik</label>

											<div class="controls">

												<select class="span4" name="ta" id="TA">

													<option>--Pilih --</option>

													<?php foreach ($TA as $key): ?>

														<option value="<?php echo $key->id_tahunajaran; ?>"><?php echo $key->tahunajaran; ?></option>

													<?php endforeach ?>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<br/>

										<h3>UANG KULIAH</h3>

										<br>



										<div class="control-group">											

											<label class="control-label" for="email">Tanggal Pembayaran</label>

											<div class="controls">

												<input type="text" class="span2" placeholder="Tanggal Pembayaran" id="tgl_bayar" name="tgl_bayar" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="email">Jumlah</label>

											<div class="controls">

												<div class="input-prepend input-append">

                                                  <span class="add-on">Rp.</span>

                                                  <input class="span2" id="appendedPrependedInput" name="jumlah_tagihan" type="text">

                                                  

                                                </div>

                                              </div>	<!-- /controls -->

											</div> <!-- /controls -->				

										



										<div class="control-group">											

											<label class="control-label" for="kab">Cicilin Ke- </label>

											<div class="controls">

												<select class="span2" name="cicilan" id="cicilan">

													<option>--Bayak Cicilan --</option>

													<option value="1" >1</option>

													<option value="2" >2</option>

													<option value="3" >3</option>

													<option value="4" >4</option>

													<option value="5" >5</option>

													<option value="6" >6</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="email">Nomor Briva</label>

											<div class="controls">

												<input type="text" class="span3" placeholder="Isi dengan nomor BRIVA" id="no_briva" name="briva_no" required>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<?php }else{ ?>



										<script>

			                              $(document).ready(function(){

			                                $('#jabatan').change(function(){

			                                  $.post('<?php echo base_url()?>data/biodata/get_divisi/'+$(this).val(),{},function(get){

			                                    $('#divisi').html(get);

			                                  });

			                                });

			                              });

			                              </script>



										<div class="control-group">											

											<label class="control-label" for="jabatan">Jabatan</label>

											<div class="controls">

												<select class="span2" name="jbtn" id="jabatan">

													<option>-- Pilih Jabatan --</option>

													<?php foreach ($jabatan as $key) {

														echo "<option value=".$key->kd_divisi.">".$key->jabatan."</option>";

													} ?>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="kab">Divisi</label>

											<div class="controls">

												<select class="span2" name="divisi" id="divisi">

													<option>--</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<?php } ?>



										<div class="form-actions">

											<!-- <button type="submit" class="btn btn-primary">UNGGAH</button>

											<button type="submit" class="btn btn-primary">CETAK</button>  -->

											<button type="submit" class="btn btn-primary">SIMPAN</button>

										</div> <!-- /form-actions -->

										

										</fieldset>

									</div>

									</div>

								</div>

								</form>

						</div>

				

			</div>

		</div>

	</div>

</div>



<script type="text/javascript">

function sepertiatas(){

		$('#alamat_ortu').hide();

	}



	function hilang(){

		$('#alamat_ortu').show();

	}

</script>