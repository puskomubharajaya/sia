<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Update Data Dropout</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>data/Dropout/add" method="post" enctype="multipart/form-data">
    <div class="modal-body" style="margin-left: -60px;">
    	<div class="control-group" id="">
            <label class="control-label">No. SKEP DO</label>
            <div class="controls">
                <input type="text" class="span4" name="skep" placeholder="Input SKEP Drop Out" class="form-control" value="<?php echo $row->skep ?>" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Tanggal SKEP</label>
            <div class="controls">
                <input type="text" class="span2" name="tgl_skep" id="tgl_skep" placeholder="Masukan Tgl SKEP" value="<?php echo $row->tgl_skep ?>"  required>
            </div>
        </div>   
        <div class="control-group" id="">
            <label class="control-label">Mahasiswa</label>
            <div class="controls">
                <input type="text" class="span4" name="mhs" placeholder="Input Nama Mahasiswa" class="form-control" value="<?php echo $row->npm_mahasiswa ?>" required/>
                <input type="hidden" class="span4" name="npm" class="form-control" value="" required/>
            </div>
        </div>              
        <div class="control-group" id="">
            <label class="control-label">Alasan</label>
            <div class="controls">
                <select name="res" class="span2" class="form-control">
                	<option disabled>-- Pilih Alasan --</option>
                	<option value="1">Mengundurkan Diri</option>
                	<option value="2">Dikeluarkan</option>
                </select>
            </div>
        </div>
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>