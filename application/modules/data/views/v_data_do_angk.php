<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Data Dropout Angkatan <?php echo $this->session->userdata('angkatan'); ?></h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>NIM</th>
                                <th>Nama Mahasiswa</th>
                                <th>Angkatan</th>
                                <th>No. SKEP.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($rows as $row){?>
                            <tr>
                                <td><?php echo $row->npm_mahasiswa;?></td>
                                <td><?php echo get_nm_mhs($row->npm_mahasiswa);?></td>
                                <td><?php echo substr($row->npm_mahasiswa, 0,4);?></td>
                                <td><?php echo $row->skep;?></td>
                            </tr>
                            <?php $no++;} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>