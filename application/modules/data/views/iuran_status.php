 <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM DATA</h4>
            </div>
            <form class ='form-horizontal' action="#" method="post">
                <div class="modal-body">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#sem" data-toggle="tab">Semester 2</a></li>
                            <li><a href="#iuran" data-toggle="tab">Iuran</a></li>
                            <li><a href="#history" data-toggle="tab">Data</a></li>
                        </ul>           
                        <div class="tab-content">
                            <div class="tab-pane active" id="sem">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr> 
                                            <th>No</th>
                                            <th>Angsuran</th>
                                            <th>Biaya</th>
                                            <th width="40">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>Rp 300,000</td>
                                            <td class="td-actions">
                                                <input type="checkbox"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="iuran">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr> 
                                            <th>No</th>
                                            <th>Keterangan</th>
                                            <th>Biaya</th>
                                            <th width="40">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>SIdang Skripsi</td>
                                            <td>Rp 1,500,000</td>
                                            <td class="td-actions">
                                                <input type="checkbox"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="history">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr> 
                                            <th>No</th>
                                            <th>Semester</th>
                                            <th>Status</th>
                                            <th>Keterangan</th>
                                            <th width="40">Print</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>6/6</td>
                                            <td>Lunas</td>
                                            <td class="td-actions">
                                                <a class="btn btn-primary btn-small" href="#"><i class="btn-icon-only icon-print"> </i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>