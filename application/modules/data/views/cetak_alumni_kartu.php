<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">

	<style type="text/css">
		table {
			border-collapse: collapse;
		}

		.inborder {
			border: 1px solid black;
			text-align: left;
			padding-left: 8px;
			padding-right: 8px;
		}
	</style>
</head>

<body>
	<h3 align="center">KARTU ALUMNI LULUSAN TAHUN <?php if ($lulusan) {
														echo date('Y', strtotime($lulusan->tgl_yudisium));
													} else {
														echo '-';
													} ?></h3>
	<h3 align="center">UNIVERSITAS BHAYANGKARA JAKARTA RAYA</h3>
	<br>
	<table width="100%">
		<tr>
			<?php if ($bio) { ?>
				<?php if ($bio->foto) { ?>
					<th rowspan="10" width="170px"><img src="<?php echo base_url() ?>image/foto_alumni/<?php echo $bio->foto ?>" width="150px"></th>
				<?php } else { ?>
					<th rowspan="10" width="170px"><img src="<?php echo base_url() ?>image/foto_alumni/foto-default.png" width="150px"></th>
				<?php } ?>
			<?php } else { ?>
				<th rowspan="10" width="170px"><img src="<?php echo base_url() ?>image/foto_alumni/foto-default.png" width="150px"></th>
			<?php } ?>
			<th class="inborder" width="160px">Nama Lengkap</th>
			<td class="inborder" width="1px">:</td>
			<td class="inborder"><?php echo $mhs->NMMHSMSMHS ?></td>
		</tr>
		<tr>
			<th class="inborder">Tempat, Tgl Lahir</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php echo $mhs->TPLHRMSMHS ?>, <?php echo dateidn($mhs->TGLHRMSMHS) ?></td>
		</tr>
		<tr>
			<th class="inborder">NPM</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php echo $mhs->NIMHSMSMHS ?></td>
		</tr>
		<tr>
			<th class="inborder">Konsentrasi</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php echo get_jur($mhs->KDPSTMSMHS) ?></td>
		</tr>
		<tr>
			<th class="inborder">Alamat</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php if ($bio) {
										echo $bio->alamat;
									} else {
										echo '-';
									} ?></td>
		</tr>
		<tr>
			<th class="inborder">Nomor Telepon</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php if ($bio) {
										echo $bio->tlp;
									} else {
										echo '-';
									} ?></td>
		</tr>
		<tr>
			<th class="inborder">E Mail</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php if ($bio) {
										echo $bio->email;
									} else {
										echo '-';
									} ?></td>
		</tr>
		<tr>
			<th class="inborder">Tanggal Lulus</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php if ($lulusan) {
										echo dateidn($lulusan->tgl_yudisium);
									} else {
										echo '-';
									} ?></td>
		</tr>
		<tr>
			<th class="inborder">Judul Tesis/Skripsi</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php if ($lulusan) {
										echo $lulusan->jdl_skripsi;
									} else {
										echo '-';
									} ?></td>
		</tr>
		<tr>
			<th class="inborder">Pekerjaan</th>
			<td class="inborder">:</td>
			<td class="inborder"><?php if ($bio) {
										echo $bio->pekerjaan;
									} else {
										echo '-';
									} ?></td>
		</tr>
	</table>
	<br>
	<div align="center" style="font-weight: bold">MUTASI</div>
	<div><?php if ($bio) {
				echo $bio->mutasi;
			} else {
				echo '-';
			} ?></div>

</body>

</html>