

<style type="text/css">
.ui-autocomplete {
  z-index: 215000000 !important;
}
</style>

<script type="text/javascript">

  $(document).ready(function($) {

      $('input[name^=mhs]').autocomplete({

        source: '<?php echo base_url('data/kelas/load_mhs');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.mhs.value = ui.item.value;

        }

      });

$('#tgl_lulus').datepicker({
			dateFormat: "yy-mm-dd",

			yearRange: "2016:2020",

			changeMonth: true,

			changeYear: true

			});

$('#tgl_yudi,#mulai_bim,#ahir_bim').datepicker({
			dateFormat: "yy-mm-dd",

			yearRange: "2000:2020",

			changeMonth: true,

			changeYear: true

			});




  });





</script>


<div class="container">
	
	    <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Lulusan Form</h3>
	  				</div>
					
					<div class="widget-content">
						
						<form id="edit-profile" method="POST" action="<?php echo base_url(); ?>data/lulusan/save_edit" class="form-horizontal">
							<fieldset>

								<div class="control-group">											

									<label class="control-label">Tahun Akademik Lulus</label>

									<div class="controls">
										<!-- <input type="text" id='mhs' name="mhs" > -->
										<input type="text" class="span1" name="ta" id="ta" value="<?php echo $row->ta_lulus;?>"  readonly>
										<input type="hidden" class="span1" name="id" id="ta" value="<?php echo $row->id_lulusan;?>">

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">NPM*</label>

									<div class="controls">
										<!-- <input type="text" id='mhs' name="mhs" > -->
										<input type="text" class="span3" name="mhs" id="mhs" value="<?php echo $row->npm_mahasiswa;?>"  readonly>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->
								<div class="control-group">											

									<label class="control-label">Nama Mahasiswa</label>

									<div class="controls">
										<!-- <input type="text" id='mhs' name="mhs" > -->
										<input type="text" class="span3" value="<?php echo $row->NMMHSMSMHS;?>"  readonly>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Tanggal Lulus*</label>

									<div class="controls">

										<input type="text" class="span3" name="tgl_lulus" id="tgl_lulus"  value="<?php echo $row->tgl_lulus; ?>"  required>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Nomor S.K. Yudisium*</label>

									<div class="controls">

										<input type="text" class="span3" name="sk_yudi"  value="<?php echo $row->sk_yudisium; ?>"  required>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Tanggal S.K.*</label>

									<div class="controls">

										<input type="text" class="span3" name="tgl_yudi" id="tgl_yudi" value="<?php echo $row->tgl_yudisium; ?>"  required>

										<p class="help-block">*Masukan Tanggal yang Tertera Pada S.K. Yudisium</p>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Total SKS*</label>

									<div class="controls">

										<input type="text" class="span1" name="sks" value="<?php echo $row->sks;?>" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');"  required>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">IPK*</label>

									<div class="controls">

										<input type="text" class="span1" name="ipk" value="<?php echo $row->ipk; ?>" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');"  required>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Nomor Ijazah*</label>

									<div class="controls">

										<input type="text" class="span3" name="no_ijazah" value="<?php echo $row->no_ijazah; ?>"  required>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Judul Skripsi*</label>

									<div class="controls">

										<textarea  class="span6" name="jdl_skripsi" required><?php echo $row->jdl_skripsi; ?></textarea>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

								<div class="control-group">											

									<label class="control-label">Masa Bimbingan*</label>

									<div class="controls">

										<input type="text" class="span3" name="mulai_bim" id="mulai_bim"  value="<?php echo $row->mulai_bim; ?>"  required>
										<input type="text" class="span3" name="ahir_bim"  id="ahir_bim"  value="<?php echo $row->ahir_bim; ?>"  required>

									</div> <!-- /controls -->				

								</div> <!-- /control-group -->

			
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Save</button> 
									<a href="<?php echo base_url();?>data/lulusan/view_lulusan" class="btn btn-default">kembali</a>
								</div> <!-- /form-actions -->
							</fieldset>
						</form>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	    </div> <!-- /row -->
	
</div> <!-- /container -->
	   