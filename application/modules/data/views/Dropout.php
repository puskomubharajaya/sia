<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dropout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('setting_model');
        error_reporting(0);
        if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(147)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
        date_default_timezone_set('Asia/Jakarta');
	}

	public function form()
	{
		$data['page']='v_lulusan_form';
        $this->load->view('template/template', $data);
	}

    function index(){
        $this->session->unset_userdata('tahun');
        $this->session->unset_userdata('jurusan');

        $logged = $this->session->userdata('sess_login');
        $pecah = explode(',', $logged['id_user_group']);
        $jmlh = count($pecah);

        for ($i=0; $i < $jmlh; $i++) { 
            $grup[] = $pecah[$i];
        }
        
        if ((in_array(10, $grup))) { //baa
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_do_select_baa';

        } elseif ((in_array(9, $grup))) { // fakultas
            $data['jurusan']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$logged['userid'],'prodi','asc')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_do_select_fak';

        } elseif ((in_array(8, $grup))) { // prodi
            $data['jurusan']=$this->db->where('kd_prodi',$logged['userid'])->get('tbl_jurusan_prodi')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            //$data['page'] = 'data/v_do_select_prodi';
            $data['page'] = 'data/v_do_home';

        } elseif ((in_array(1, $grup))) { // admin
            $data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();

            $data['page'] = 'data/v_do_select_baa';
        }

        $this->load->view('template/template', $data);   
    }

    function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."_".$id."'>".$row->prodi. "</option>";
        }
        // $out .= "<option value='99_".$id."'>SEMUA PROGRAM STUDI</option>";
        $out .= "</select>";

        echo $out;
	}

    function view(){
        $jurusan = $this->input->post('jurusan');
        $pecah = explode('_', $jurusan);
        $tahun = $this->input->post('tahun');

        $this->session->set_userdata('tahun',$tahun);
        $this->session->set_userdata('jurusan',$pecah[0]);

        redirect(base_url().'data/Dropout/data_do','refresh');
    }

    function data_do(){

        if ($this->session->userdata('jurusan')) {
            $data['rows'] = $this->db->query("SELECT * FROM tbl_dropout do 
                                            JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = do.`npm_mahasiswa`
                                            WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' AND do.`tahunajaran` = ".$this->session->userdata('tahun')."")->result();

            $data['page']='v_do_view';
            $this->load->view('template/template', $data);    
        }else{
            redirect(base_url().'data/Dropout','refresh');
        }

        
    }

    function add(){
    	$logged = $this->session->userdata('sess_login');

    	$skep = $this->input->post('skep');
    	$tgl_skep = $this->input->post('tgl_skep');
    	$npm = $this->input->post('npm');
    	$alasan = $this->input->post('res');

    	//insert to tbl_dropout
    	$data_do = array(
    				'skep' => $skep,
    				'tgl_skep' => $tgl_skep,
    				'npm_mahasiswa' => $npm,
    				'alasan' => $alasan,
    				'tahunajaran' => $this->session->userdata('tahun'),
    				'audit_user' => $logged['userid'],
    				'audit_time' =>  date('Y-m-d H:i:s')
    				);

    	$this->db->insert('tbl_dropout', $data_do);

    	//insert to tbl_status_mahasiswa
    	$mhs_do = array('npm' => $npm, 'status' => 'K', 'tahunajaran' =>  $this->session->userdata('tahun'), 'tanggal' => date('Y-m-d'));
    	
    	$this->db->insert('tbl_status_mahasiswa', $mhs_do);

    	//update tbl_mahasiswa
    	$up_mhs = array('STMHSMSMHS' => 'K');
    	
    	$this->db->where('NIMHSMSMHS', $npm);
    	$this->db->update('tbl_mahasiswa', $up_mhs);

    	redirect(base_url('data/dropout/view'),'refresh');
    }

    function delete($id){
    	$this->db->where('npm_mahasiswa', $id);
    	$this->db->delete('tbl_dropout');

    	$this->db->where('npm', $id);
        $this->db->where('status', 'K');
    	$this->db->delete('tbl_status_mahasiswa');

    	redirect(base_url('data/dropout/view'),'refresh');
    }

    function edit_dropout($id){

    	$data['row']=$this->db->where('npm_mahasiswa', $id)
    				->get('tbl_dropout')->row();

    	$this->load->view('v_do_edit', $data);
    }


    function load_mhs_autocomplete(){

    $not_st = array('K','L');    

    $prodi = $this->session->userdata('jurusan');

    $this->db->distinct();

    $this->db->select("a.NIMHSMSMHS,a.NMMHSMSMHS");

    $this->db->from('tbl_mahasiswa a');

	$this->db->where_not_in('a.STMHSMSMHS',$not_st);
    
    // $this->db->where('a.KDPSTMSMHS', $prodi);
    
    $this->db->like('a.NMMHSMSMHS', $_GET['term'], 'both');

    $this->db->or_like('a.NIMHSMSMHS', $_GET['term'], 'both');

    $sql  = $this->db->get();

    $data = array();

    foreach ($sql->result() as $row) {

        $data[] = array(

                        'npm'           => $row->NIMHSMSMHS,

                        'value'         => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS

                        );
    }

    echo json_encode($data);

    }

}

/* End of file Dropout.php */
/* Location: ./application/controllers/Dropout.php */