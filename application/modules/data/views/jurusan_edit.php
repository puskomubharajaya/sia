            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Data Jurusan</h4>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $.post('<?php echo base_url()?>data/jurusan/view_edit_jurs'+$(this).val(),{},function(get){
                        $("#kode_jur").html(get);
                    });
                });
            </script> 
            <form class ='form-horizontal' action="<?php echo base_url();?>data/jurusan/update_jurusan" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
					<div class="control-group" id="">
                        <label class="control-label">Kode Jurusan</label>
                        <div class="controls">
                            <input type="hidden" name="id_jurusan" value="<?php echo $query->id_prodi;?>">
                            <input type="text" class="span4" id="kode_jur" name="kode_jur" placeholder="Input Kode" class="form-control" value="<?php echo $query->kd_prodi;?>" required/>
                        </div>
                    </div>				
                    <div class="control-group" id="">
                        <label class="control-label">Jurusan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="jurusan" placeholder="Input Jurusan" class="form-control" value="<?php echo $query->prodi;?>" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Fakultas</label>
                        <div class="controls">
                            <select class="form-control" name='fakuls'>
                            <option>--Pilih Fakultas--</option>
                            <?php foreach ($fakultas as $row) { ?>
                                <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>