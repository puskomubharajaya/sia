<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswapmb extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(89)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	function index()
	{
		//$data['yes'] = $this->db->query("SELECT * from tbl_calon_mhs a join tbl_jurusan_prodi b on a.`kd_prodi`=b.`kd_prodi` where a.`status`>='1'")->result();
		$data['page'] = 'data_cmhs';
		$this->load->view('template/template', $data);
	}
	
	function print_dt()
	{
		$data['cmb'] = $this->db->query('SELECT * from tbl_calon_mhs a join tbl_jurusan_prodi b on a.`kd_prodi`=b.`kd_prodi` where a.`status` >= "1"')->result();
		$this->load->view('print_dt', $data);
	}

	function data_cmhs()
	{
		$data['page'] = 'data_cmhs';
		$this->load->view('template/template', $data);
	}

	function simpan_sesi()
	{
		$jenjang = $this->input->post('jenjang');
		$jenis = $this->input->post('jenis');
		$gel = $this->input->post('gel');


        $this->session->set_userdata('cuss', $jenjang);

        $this->session->set_userdata('jenis', $jenis);   

		$this->session->set_userdata('gelombang', $gel);        
      
		redirect(base_url('datas/mahasiswapmb/load_data'));
	}

	function load_data()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ( (in_array(9, $grup))) {
			$hahay = $this->session->userdata('cuss');
			if ($hahay == 's1') {
				if ($this->session->userdata('gelombang') == 0) {
					$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' order by nomor_registrasi ASC')->result();
					$data['page'] = "v_list_s1";
					$this->load->view('template/template', $data);
				} else {
					$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
					$data['page'] = "v_list_s1";
					$this->load->view('template/template', $data);
				}
				
			} elseif ($hahay == 's2') {
				if ($this->session->userdata('gelombang') == 0) {
					$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' order by ID_registrasi ASC')->result();
					$data['page'] = "v_list_s2";
					$this->load->view('template/template', $data);
				} else {
					$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where b.kd_fakultas = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
					$data['page'] = "v_list_s2";
					$this->load->view('template/template', $data);
				}
				
			}
		} elseif ( (in_array(8, $grup))) {
			$hahay = $this->session->userdata('cuss');
			if ($hahay == 's1') {
				if ($this->session->userdata('gelombang') == 0) {
					$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.prodi = '.$logged['userid'].' order by nomor_registrasi ASC')->result();
					$data['page'] = "v_list_s1";
					$this->load->view('template/template', $data);
				} else {
					$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.prodi = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
					$data['page'] = "v_list_s1";
					$this->load->view('template/template', $data);
				}
				
			} elseif ($hahay == 's2') {
				if ($this->session->userdata('gelombang') == 0) {
					$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.opsi_prodi_s2 = '.$logged['userid'].' order by ID_registrasi ASC')->result();
					$data['page'] = "v_list_s2";
					$this->load->view('template/template', $data);
				} else {
					$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.opsi_prodi_s2 = '.$logged['userid'].' and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
					$data['page'] = "v_list_s2";
					$this->load->view('template/template', $data);
				}
				
			}
		} elseif ( (in_array(13, $grup))) {
			$hahay = $this->session->userdata('cuss');
			if ($hahay == 's1') {
				if ($this->session->userdata('gelombang') == 0) {
					if ($this->session->userdata('jenis') == 'KV') {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
														 where jenis_pmb = "KV" 
														 order by nomor_registrasi DESC')->result();
					}elseif ($this->session->userdata('jenis') == 'MB') {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
														 where jenis_pmb = "MB" 
														 order by nomor_registrasi DESC')->result();
					}elseif ($this->session->userdata('jenis') == 'RM') {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
														 where jenis_pmb = "RM" 
														 order by nomor_registrasi DESC')->result();
					} elseif ($this->session->userdata('jenis') == 'ALL') {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
														 order by nomor_registrasi DESC')->result();
					}
					
					$data['page'] = "v_list_s1";
					$this->load->view('template/template', $data);
				} else {
					if ($this->session->userdata('jenis') == 'KV') {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
														 where jenis_pmb = "KV" AND gelombang = '.$this->session->userdata('gelombang').'
														 order by nomor_registrasi DESC')->result();
					}elseif ($this->session->userdata('jenis') == 'MB') {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
														 where jenis_pmb = "MB" AND gelombang = '.$this->session->userdata('gelombang').'
														 order by nomor_registrasi DESC')->result();
					}elseif ($this->session->userdata('jenis') == 'RM') {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
														 where jenis_pmb = "RM" AND gelombang = '.$this->session->userdata('gelombang').'
														 order by nomor_registrasi DESC')->result();
					} elseif ($this->session->userdata('jenis') == 'ALL') {
						$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
														 where gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi DESC')->result();
					}

					$data['page'] = "v_list_s1";
					$this->load->view('template/template', $data);
				}
			} elseif ($hahay == 's2') {
				if ($this->session->userdata('gelombang') == 0) {
					if ($this->session->userdata('jenis') == 'KV') {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
														 where jenis_pmb = "KV" 
														 order by ID_registrasi DESC')->result();
					}elseif ($this->session->userdata('jenis') == 'MB') {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
														 where jenis_pmb = "MB" 
														 order by ID_registrasi DESC')->result();
					}elseif ($this->session->userdata('jenis') == 'RM') {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
														 where jenis_pmb = "RM" 
														 order by ID_registrasi DESC')->result();
					} elseif ($this->session->userdata('jenis') == 'ALL') {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
														order by ID_registrasi DESC')->result();
					}
					$data['page'] = "v_list_s2";
					$this->load->view('template/template', $data);
				} else {
					if ($this->session->userdata('jenis') == 'KV') {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
														 where jenis_pmb = "KV" AND gelombang = '.$this->session->userdata('gelombang').'
														 order by ID_registrasi DESC')->result();
					}elseif ($this->session->userdata('jenis') == 'MB') {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
														 where jenis_pmb = "MB" AND gelombang = '.$this->session->userdata('gelombang').'
														 order by ID_registrasi DESC')->result();
					}elseif ($this->session->userdata('jenis') == 'RM') {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
														 where jenis_pmb = "RM" AND gelombang = '.$this->session->userdata('gelombang').'
														 order by ID_registrasi DESC')->result();
					} elseif ($this->session->userdata('jenis') == 'ALL') {
						$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi
														where gelombang = '.$this->session->userdata('gelombang').'
														order by ID_registrasi DESC')->result();
					}

					$data['page'] = "v_list_s2";
					$this->load->view('template/template', $data);
				}
			}
		}
		
	}

	function printexcels2()
	{
		if ($this->input->post('fakultas') == 0) {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi order by ID_registrasi ASC')->result();
				$this->load->view('excel_mabas2', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
				$this->load->view('excel_mabas2', $data);
			}
			
		} else {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' order by ID_registrasi ASC')->result();
				$this->load->view('excel_mabas2', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
				$this->load->view('excel_mabas2', $data);
			}
			
		}
		
	}



	function printexcels1()
	{
		if ($this->input->post('fakultas') == 0) {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			}
		} else {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.gelombang = '.$this->session->userdata('gelombang').' order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			}
			
		}
	}

	function detils2($id)
	{
		$data['cek'] = $this->db->query('SELECT * from tbl_pmb_s2 where ID_registrasi = "'.$id.'"')->row();
		$data['page'] = "v_detil_mabas2";
		$this->load->view('template/template', $data);
	}

	function detils1($id)
	{
		$data['cek'] = $this->db->query('SELECT * from tbl_form_camaba where nomor_registrasi = "'.$id.'"')->row();

		$a = array('74101','61101');

		$this->db->select('*');
		$this->db->from('tbl_jurusan_prodi');
		$this->db->where_not_in('kd_prodi', $a);
		$data['rows'] = $this->db->get()->result();

		$data['page'] = "v_detil_mabas1";
		$this->load->view('template/template', $data);
	}

	function simpan_form(){


		$id = $this->input->post('kd_regis');
		//die($id);
		$datax = array(

				'jenis_pmb'		=>	$this->input->post('jenis'),
				'kampus'		=>	$this->input->post('kampus'),
				'kelas'			=>	$this->input->post('kelas'),
				'nama'			=>	$this->input->post('nama'),
				'kelamin'		=>	$this->input->post('jk'),
				'status_wn'		=>	$this->input->post('wn'),
				'kewaganegaraan'=>	$this->input->post('wn_txt'),
				'tempat_lahir'	=>	$this->input->post('tpt_lahir'),
				'tgl_lahir'		=>	$this->input->post('tgl_lahir'),
				'agama'			=>	$this->input->post('agama'),
				'status_nikah'	=>	$this->input->post('stsm'),
				'status_kerja'	=>	$this->input->post('stsb'),
				'perkerjaan'	=>	$this->input->post('stsb_txt'),
				'alamat'		=>	$this->input->post('alamat'),
				'kd_pos'		=>	$this->input->post('kdpos'),
				'tlp'			=>	$this->input->post('tlp'),

				'npm_lama_readmisi'		=>	$this->input->post('npm_readmisi'),
				'tahun_masuk_readmisi'	=>	$this->input->post('thmasuk_readmisi'),
				'smtr_keluar_readmisi'	=>	$this->input->post('smtr_readmisi'),

				'asal_sch_maba'			=>	$this->input->post('asal_sch'),
				'kota_sch_maba'			=>	$this->input->post('kota_sch'),
				'jur_maba'				=>	$this->input->post('jur_sch'),
				'lulus_maba'			=>	$this->input->post('lulus_sch'),
				'jenis_sch_maba'		=> 	$this->input->post('jenis_sch_maba'),

				'asal_pts_konversi'		=>	$this->input->post('asal_pts'),
				'kota_pts_konversi'		=>	$this->input->post('kota_pts'),
				'prodi_pts_konversi'	=>	$this->input->post('prodi_pts'),
				'npm_pts_konversi'		=>	$this->input->post('npm_pts'),
				'tahun_lulus_konversi'	=>	$this->input->post('lulus_pts'),
				'smtr_lulus_konversi'	=>	$this->input->post('smtr_pts'),


				
				'prodi'			=>	$this->input->post('prodi'),
				'nm_ayah'		=>	$this->input->post('nm_ayah'),
				'didik_ayah'	=>	$this->input->post('didik_ayah'),
				'workdad'		=>	$this->input->post('workdad'),
				'life_statdad'	=>	$this->input->post('life_statdad'),
				'nm_ibu'		=>	$this->input->post('nm_ibu'),
				'didik_ibu'		=>	$this->input->post('didik_ibu'),
				'workmom'		=>	$this->input->post('workmom'),
				'life_statmom'	=>	$this->input->post('life_statmom'),
				//'tanggal_regis'	=>	date('Y-m-d'),
				'penghasilan'	=>	$this->input->post('gaji'),
				'referensi'		=>	$this->input->post('refer')
				
			);

		$this->db->where('id_form', $id);
		$this->db->update('tbl_form_camaba', $datax);

		echo "<script>alert('Sukses');
		document.location.href='".base_url()."datas/mahasiswapmb/load_data';</script>";
	}

	function printexcelharis1()
	{
		if ($this->input->post('fakultas') == 0) {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.tanggal_regis = "'.$this->input->post('rekap_hari').'" order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi where a.gelombang = '.$this->session->userdata('gelombang').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'" order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			}
		} else {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'" order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.gelombang = '.$this->session->userdata('gelombang').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'" order by nomor_registrasi ASC')->result();
				$this->load->view('excel_mabas1', $data);
			}
			
		}
		
	}

	function printexcelharis2()
	{
		if ($this->input->post('fakultas') == 0) {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.tanggal_regis = "'.$this->input->post('rekap_hari').'"')->result();
				$this->load->view('excel_mabas2', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where a.gelombang = '.$this->session->userdata('gelombang').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'"')->result();
				$this->load->view('excel_mabas2', $data);
			}
		} else {
			if ($this->session->userdata('gelombang') == 0) {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'"')->result();
				$this->load->view('excel_mabas2', $data);
			} else {
				$data['look'] = $this->db->query('SELECT * from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi join tbl_fakultas c on b.kd_fakultas = c.kd_fakultas where c.kd_fakultas = '.$this->input->post('fakultas').' and a.gelombang = '.$this->session->userdata('gelombang').' and a.tanggal_regis = "'.$this->input->post('rekap_hari').'"')->result();
				$this->load->view('excel_mabas2', $data);
			}
			
		}
		
	}

	function update_s2()
	{
		$this->load->helper('inflector');
		$nama = underscore($_FILES['img']['name']);
		$config['upload_path'] = './upload/imgpmb/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $nama;
		$config['max_size'] = 1000000;
		$this->load->library('upload', $config);
		$patt = $config['upload_path'].basename($nama);

		$id = $this->input->post('ole');

		$namm = strtoupper($this->input->post('nama'));
		$ayah = strtoupper($this->input->post('nm_ayah'));
		$ibu = strtoupper($this->input->post('nm_ibu'));
		$kota = strtoupper($this->input->post('kota_asal'));

		$datas = array(
				'kampus'		=>	$this->input->post('kampus'),
				'kelas'			=>	'KR',
				'nama'			=>	$namm,
				'kelamin'		=>	$this->input->post('jk'),
				'negara'		=>	$this->input->post('wn'),
				'tempat_lahir'	=>	$this->input->post('tpt'),
				'tgl_lahir'		=>	$this->input->post('tgl'),
				'agama'			=>	$this->input->post('agama'),
				'status_nikah'	=>	$this->input->post('stsm'),
				'status_kerja'	=>	$this->input->post('stsb'),
				'alamat'		=>	$this->input->post('alamat'),
				'kd_pos'		=>	$this->input->post('kdpos'),
				'tlp'			=>	$this->input->post('tlp'),
				'npm_lama'		=>	$this->input->post('npm'),
				'thmsubj'		=>	$this->input->post('thmasuk'),
				'nm_univ'		=>	$this->input->post('nm_pt'),
				'prodi'			=>	$this->input->post('prodi_lm'),
				'th_lulus'		=>	$this->input->post('thlulus'),
				'kota_asl_univ'	=>	$kota,
				'npm_nim'		=>	$this->input->post('npmnim'),
				'opsi_prodi_s2'	=>	$this->input->post('prodi'),
				'pekerjaan'		=>	$this->input->post('pekerjaan'),
				'nm_ayah'		=>	$ayah,
				'nm_ibu'		=>	$ibu,
				'penghasilan'	=>	$this->input->post('gaji'),
				'informasi_from'=>	$this->input->post('infrom'),
				'foto'			=>	$patt,
				'negara_wna'	=>	$this->input->post('jns_wn'),
				'tempat_kerja'	=>	$this->input->post('tpt_kerja')
			);
		//var_dump($datas);exit();
		$this->app_model->updatedata('tbl_pmb_s2','ID_registrasi',$id,$datas);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."pmb/forms2';</script>";
	}

	function load_lengkap($id)
	{
		$data['mhspmb'] = $this->app_model->getdetail('tbl_form_camaba','id_form',$id,'id_form','asc')->row();
		$this->load->view('lengkap_view',$data);
	}

	function load_lengkap2($id)
	{
		$data['mhspmb'] = $this->app_model->getdetail('tbl_pmb_s2','id_s2',$id,'id_s2','asc')->row();
		$this->load->view('lengkap_view2',$data);
	}

	function save_lengkap(){
		$reg = $this->input->post('id', TRUE);
		$jumlah = count($this->input->post('lengkap', TRUE));
		//var_dump($reg.' - '.$jumlah);exit();
		$hasil = '';
		for ($i=0; $i < $jumlah; $i++) { 
			if ($hasil == '') {
				$hasil = $this->input->post('lengkap['.$i.']', TRUE).',';
			} else {
				$hasil = $hasil.$this->input->post('lengkap['.$i.']', TRUE).',';
			}	
		}
		$data['status_kelengkapan'] = $hasil;
		$this->app_model->updatedata('tbl_form_camaba','nomor_registrasi',$reg,$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."datas/mahasiswapmb/load_data';</script>";
		//var_dump($hasil);exit();
	}

	function save_lengkap2(){
		$reg = $this->input->post('id', TRUE);
		$jumlah = count($this->input->post('lengkap', TRUE));
		//var_dump($reg.' - '.$jumlah);exit();
		$hasil = '';
		for ($i=0; $i < $jumlah; $i++) { 
			if ($hasil == '') {
				$hasil = $this->input->post('lengkap['.$i.']', TRUE).',';
			} else {
				$hasil = $hasil.$this->input->post('lengkap['.$i.']', TRUE).',';
			}	
		}
		$data['status_kelengkapan'] = $hasil;
		$this->app_model->updatedata('tbl_pmb_s2','ID_registrasi',$reg,$data);
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."datas/mahasiswapmb/load_data';</script>";
		//var_dump($hasil);exit();
	}

	function printstatuss1()
	{
		if ($this->session->userdata('gelombang') == 0) {
			die('1');
			if ($this->session->userdata('jenis') == 'KV') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "KV" and status >1
												 order by nomor_registrasi ASC')->result();
			}elseif ($this->session->userdata('jenis') == 'MB') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "MB" and status >1
												 order by nomor_registrasi ASC')->result();
			}elseif ($this->session->userdata('jenis') == 'RM') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "RM" and status >1
												 order by nomor_registrasi ASC')->result();
			} elseif ($this->session->userdata('jenis') == 'ALL') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi 
												 WHERE status >1 								 
												 order by nomor_registrasi ASC')->result();
			}
		} else {
			
			if ($this->session->userdata('jenis') == 'KV') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "KV" and status >1 and a.gelombang = '.$this->session->userdata('gelombang').'
												 order by nomor_registrasi ASC')->result();
			}elseif ($this->session->userdata('jenis') == 'MB') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "MB" and status >1 and a.gelombang = '.$this->session->userdata('gelombang').'
												 order by nomor_registrasi ASC')->result();
			}elseif ($this->session->userdata('jenis') == 'RM') {
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi
												 where jenis_pmb = "RM" and status >1 and a.gelombang = '.$this->session->userdata('gelombang').'
												 order by nomor_registrasi ASC')->result();
			} elseif ($this->session->userdata('jenis') == 'ALL') {
				//die('3');
				$data['look'] = $this->db->query('SELECT nomor_registrasi as noreg,nama,status_kelengkapan,lulus_maba as lls from tbl_form_camaba a join tbl_jurusan_prodi b on a.prodi=b.kd_prodi 
												 where a.gelombang = '.$this->session->userdata('gelombang').' and status >1
												 order by nomor_registrasi ASC')->result();
				
			}
			
		}

		$this->load->library('excel');
		$this->load->view('welcome/phpexcel_status', $data);
	}

	function printstatuss2()
	{
		if ($this->session->userdata('gelombang') == 0) {
			$data['look'] = $this->db->query('SELECT ID_registrasi as noreg,nama,status_kelengkapan,th_lulus as lls from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi WHERE status >1 order by ID_registrasi ASC')->result();
		} else {
			$data['look'] = $this->db->query('SELECT ID_registrasi as noreg,nama,status_kelengkapan,th_lulus as lls from tbl_pmb_s2 a join tbl_jurusan_prodi b on a.opsi_prodi_s2=b.kd_prodi where status >1 and a.gelombang = '.$this->session->userdata('gelombang').' order by ID_registrasi ASC')->result();
		}
		$this->load->library('excel');
		$this->load->view('welcome/phpexcel_status', $data);
	}

}

/* End of file Mahasiswapmb.php */
/* Location: ./application/modules/datas/controllers/Mahasiswapmb.php */