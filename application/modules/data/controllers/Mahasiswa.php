<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//$id_menu = 32 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(32)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup))) {
			//$divisi = $this->app_model->get_jabatan_user($logged['userid'])->row();
			//$sess_prodi = '62201';
			$data['getData']=$this->app_model->getdetail('tbl_mahasiswa','KDPSTMSMHS',$logged['userid'],'NIMHSMSMHS','asc')->result();
			$data['page'] = 'data/mahasiswa_lihat';
		} else {
			$data['fakultas']=$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['page'] = 'data/mahasiswa_view';
		}
		$this->load->view('template/template',$data);
	}

	function view_mhs(){
		$sess_prodi = $this->input->post('jurusan', TRUE);
		$this->session->set_userdata('prodi',$sess_prodi);
		$data['getData']=$this->app_model->getdetail('tbl_mahasiswa','KDPSTMSMHS',$sess_prodi,'NIMHSMSMHS','asc')->result();
		$data['page'] = 'data/mahasiswa_lihat';
		$this->load->view('template/template',$data);
	}

	function get_jurusan($id){
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function printdata()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup))) {
			$data['getData']=$this->app_model->getdetailprintmhs('tbl_mahasiswa','KDPSTMSMHS',$logged['userid'],'TAHUNMSMHS',$this->session->userdata('tahun'),'NIMHSMSMHS','asc')->result();
		} else {
			$data['getData']=$this->app_model->getdetail('tbl_mahasiswa','KDPSTMSMHS',$this->session->userdata('prodi'),'NIMHSMSMHS','asc')->result();
		}
		$this->load->view('print_excel_mahasiswa', $data);
	}

}

/* End of file mahasiswa.php */
/* Location: ./application/modules/data/controllers/mahasiswa.php */