<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-calendar"></i>
  				<?php $tgl = strtotime(date('Y-m-d')); ?>
  				<h3>Daftar Jadwal Perkuliahan, <i><?= toIdnDay(date('l', $tgl)).' / '.TanggalIndo(date('Y-m-d')) ?></i>
  				</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>Dosen</th>
	                        	<th>Kelas</th>
	                        	<th>Status</th>
	                            <th>Detail Absen</th>
	                            <th>Review Pertemuan</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php $no=1; foreach ($listajar->result() as $val) { ?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
	                        	<td><?php echo get_nama_mk($val->kd_matakuliah,substr($val->kd_jadwal, 0,5)); ?></td>
	                        	<td><?php echo nama_dsn($val->kd_dosen); ?></td>
                                <td><?php echo $val->kelas; ?></td>

                                <?php if ($val->open == 1) {
                                	$stscls = "Sedang berlangsung";
                                } else {
                                	$stscls = "Perkuliahan belum dimulai";
                                } ?>

	                        	<td class="td-actions">
	                        		<?php echo $stscls; ?>
								</td>
								<td>
									<?php if ($val->open == 1) { ?>
	                                	
										<a class="btn btn-info" href="<?= base_url('absen/absenDosen/todaylist/'.$val->id_jadwal) ?>">
											<i class="icon icon-eye-open"></i>
										</a>

	                                <?php } else { ?>
	                                	
	                                	<a class="btn btn-danger" href="javascript:void(0);" onclick="alert('Hubungi prodi untuk memulai akses absensi kelas!')">
											<i class="icon icon-remove"></i>
										</a>

	                                <?php } ?>
								</td>
								<td>	                                	
									<a class="btn btn-success" href="<?= base_url('absen/absenDosen/review/'.$val->id_jadwal) ?>">
										<i class="icon icon-list"></i>
									</a>
								</td>
	                        </tr>
	                        <?php $no++;} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>