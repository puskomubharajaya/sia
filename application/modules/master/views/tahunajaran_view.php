<script type="text/javascript" src="<?php echo base_url();?>assets/jquerymasked/src/jquery.maskedinput.js"></script>
<script>
function edit(id){
$('#edit_tahun_ajar').load('<?php echo base_url();?>master/tahunajaran/view_edit/'+id);
}
</script>

<script type="text/javascript">
$(document).ready(function() {
    $("#tahun").mask("9999/9999");
});

</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Tahun Ajaran</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Tambah Data </a><br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Tahun Ajaran</th>
	                            <th width="120">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($getData as $key): ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $key->tahunajaran; ?></td>
                                    <td class="td-actions">
                                        <a onclick="edit(<?php echo $key->id_tahunajaran;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
                                        <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>master/tahunajaran/delete/<?php echo $key->id_tahunajaran; ?>"><i class="btn-icon-only icon-remove"> </i></a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
	                    </tbody> 
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Tahun Ajaran</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>master/tahunajaran/insert_TA" method="post">
                <div class="modal-body" style="margin-left: -60px;">  
					<div class="control-group" id="">
                        <label class="control-label">Tahun Ajar</label>
                        <div class="controls">
                            <input type="text" class="span4" id="tahun" name="tahun" placeholder="Input Tahun Ajar" class="form-control" value="" required/>
                        </div>
                    </div>				
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_tahun_ajar">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->