<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th>NPM</th>
			<th>NAMA</th>
			<th>SEMESTER</th>
			<th>KODE MK</th>
			<th>MK</th>
			<th>SKS</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach ($mhs as $value) { ?>
		<tr>
			<td><?php echo $value->NIMHSMSMHS; ?></td>
			<td><?php echo $value->NMMHSMSMHS; ?></td>
			<td><?php echo $value->semester_krs; ?></td>
			<td><?php echo $value->kd_matakuliah; ?></td>
			<td><?php echo $value->nama_matakuliah; ?></td>
			<td><?php echo $value->sks_matakuliah; ?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>