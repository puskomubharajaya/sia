<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataajar extends CI_Controller {
	private $userid, $usergroup;

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login')) {
			$this->userid = $this->session->userdata('sess_login')['userid'];
			$this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
			if ($this->usergroup != 10 && $this->usergroup != 19 && $this->usergroup != 8 && $this->usergroup != 18) {
				redirect('auth/logout','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$this->session->unset_userdata('tahunajaran');
		$this->session->unset_userdata('id_fakultas_prasyarat');
		$this->session->unset_userdata('nama_fakultas_prasyarat');
		$this->session->unset_userdata('id_jurusan_prasyarat');
		$this->session->unset_userdata('nama_jurusan_prasyarat');

		$prodi = $this->userid;
		$group = get_group($this->usergroup);

		if ( in_array(10, $group) || in_array(1, $group) || in_array(18, $group) ) {
			$data['fakultas']  = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page']      = 'akademik/ajar_select';
	        $this->load->view('template/template', $data);

	    } elseif (in_array(9, $group)) {
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['prodi']     = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$prodi,'kd_$this->userid')->result();
			$data['page']      ='akademik/ajar_select_fak';
	        $this->load->view('template/template', $data);    	

	    } elseif (in_array(8, $group) || in_array(19, $group)) {
			$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
			$data['page']      = 'akademik/ajar_select_dosen';
	        $this->load->view('template/template', $data);
		}
	}

}

/* End of file Dataajar.php */
/* Location: ./application/modules/akademik/controllers/Dataajar.php */