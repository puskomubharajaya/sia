<?php $log = $this->session->userdata('sess_login'); ?>
<!-- status mahaisswa ubj -->
<script type="text/javascript">
	$(function () {
		var chart = $('#stsmhs').highcharts({
			colors: ['#7cb5ec', '#55BF3B','#000000'],
		    chart: {
		        type: 'column'
		    },

		    title: {
		        text: 'Status Mahasiswa UBJ'
		    },

		    subtitle: {
		        text: 'Source : SIA UBHARAJAYA'
		    },

		    legend: {
		        align: 'right',
		        verticalAlign: 'middle',
		        layout: 'vertical'
		    },

		    xAxis: {
		        categories: [
		        	<?php $tahun = 2015; for ($i=$tahun; $i < date('Y'); $i++) { 
		        		for ($a=1; $a < 3; $a++) { 
		        			echo $i.$a.',';
		        		}
		        	} ?>
		        ],
		        labels: {
		            x: -10
		        }
		    },

		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Amount'
		        }
		    },

		    series: [{
		        name: 'Aktif',
		        data: [
		        	<?= $mhsact; ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }, {
		        name: 'Cuti',
		        data: [
		        	<?= $mhscut; ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }, {
		        name: 'Non Aktif',
		        data: [
		        	<?= $mhsnon; ?>
		        ],
		        dataLabels: {
		            enabled: true,
		            rotation: 0,
		            color: '#FFFFFF',
		            align: 'right',
		            format: '{point.y:.f}', // one decimal
		            y: 10, // 10 pixels down from the top
		            style: {
		                fontSize: '13px',
		                fontFamily: 'Verdana, sans-serif'
		            }
		        }
		    }],
		});

		$('#small').click(function () {
		    chart.setSize(400, 300);
		});

		$('#large').click(function () {
		    chart.setSize(600, 300);
		});
	});
</script>

<script src="<?php echo base_url(); ?>assets/hc/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/data.js"></script>
<script src="<?php echo base_url(); ?>assets/hc/js/modules/drilldown.js"></script>

<!-- <div id="stsmhs-pie" style="min-width: 1000px; max-width: 1000px; height: 400px; margin: 0 auto"></div>
<br>
<hr> -->
<div id="stsmhs" style="min-width: 1000px; height: 400px; max-width: 1000px; margin: 0 auto"></div>	
<?php 
	if ($log['id_user_group'] != 8 && $log['id_user_group'] != 9) { ?>
		<hr>
    	<center>
    		<h3>Jumlah Status Mahasiswa Per Prodi</h3>
    		<small>*pilih prodi pada <i>select box</i> untuk menampilkan statistik</small>
    	</center>
    	<br>
    	<script>
            $(document).ready(function(){
                $('#fk1').change(function(){
                    $.post('<?php echo base_url()?>infografis/ig02/sesi_jml_sts/'+$(this).val(),{},function(get){
                        $('#showsts').html(get);
                    });
                });
            }); 
        </script>
    	<div class="pull-right">
            <div class="form-group" style="float:left;margin-right">
                <select class="form-control" name="prod" id="fk1">
                    <option disabled="" selected="">-- Pilih Prodi --</option>
                    <?php foreach ($pro as $key) { ?>
                    <option value="<?php echo $key->kd_prodi; ?>"><?php echo $key->prodi; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <br>
        <div id="showsts" style="min-width: 300px; height: 400px; max-width: 1100px;"></div>
<?php } ?>