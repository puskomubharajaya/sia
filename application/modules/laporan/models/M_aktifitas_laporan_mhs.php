<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_aktifitas_laporan_mhs extends CI_Model {

	public function get_members($tahunakademik)
	{
		$data = $this->db->query("SELECT 
									ag.`npm`, 
									ak.`judul`, 
									ak.`lokasi`, 
									ak.`no_sk_tugas`, 
									ak.`tgl_sk_tugas`,
									jn.`nama` AS jenis,
									mhs.`NMMHSMSMHS`,
									mhs.`KDPSTMSMHS`
								FROM tbl_anggota_aktifitas ag
								JOIN tbl_aktifitas_mhs ak ON ag.`id_aktifitas` = ak.`id`
								JOIN tbl_jenis_aktifitas jn ON jn.`kode` = ak.`jenis`
								JOIN tbl_mahasiswa mhs ON ag.`npm` = mhs.`NIMHSMSMHS`
								WHERE ak.`tahunakademik` = '{$tahunakademik}'")->result();
		return $data;
	}	

}

/* End of file M_aktifitas_laporan_mhs.php */
/* Location: ./application/modules/laporan/models/M_aktifitas_laporan_mhs.php */