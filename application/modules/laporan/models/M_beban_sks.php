<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_beban_sks extends CI_Model {

	public function get_credit_weight($tahunakademik)
	{
		$data = $this->db->query("SELECT
									ky.`nid`,
									ky.`nidn`,
									ky.`nupn`,
									ky.`nama`,
									SUM(mk.`sks_matakuliah`) as total_sks,
									jd.`kd_tahunajaran` 
								FROM tbl_jadwal_matkul jd
								JOIN tbl_matakuliah mk ON jd.`id_matakuliah` = mk.`id_matakuliah` 
								JOIN tbl_karyawan ky ON jd.`kd_dosen` = ky.`nid` 
								WHERE jd.`kd_tahunajaran` = '{$tahunakademik}'
								AND (jd.`gabung` IS NULL OR jd.`gabung` = 0)
								AND mk.`mk_ta` IS NULL 
								AND jd.`kd_jadwal` IN 
									(SELECT kd_jadwal FROM tbl_krs 
									WHERE kd_krs LIKE CONCAT(npm_mahasiswa, '{$tahunakademik}%') )
								GROUP BY jd.`kd_dosen` ")->result();
		return $data;
	}	

	public function detail_mengajar($nid, $tahunakademik)
	{
		$data = $this->db->query("SELECT 
									mk.`kd_matakuliah`,
									mk.`nama_matakuliah`, 
									mk.`sks_matakuliah` , 
									jd.`kelas`, 
									jd.`hari`, 
									jd.`waktu_mulai`, 
									jd.`waktu_selesai` 
								FROM tbl_jadwal_matkul jd
								JOIN tbl_matakuliah mk ON jd.`id_matakuliah` = mk.`id_matakuliah` 
								WHERE jd.`kd_dosen` = '{$nid}'
								AND (jd.`gabung` IS NULL OR jd.`gabung` = 0)
								AND jd.`kd_tahunajaran` = '{$tahunakademik}'
								AND mk.`mk_ta` IS NULL 
								AND jd.`kd_jadwal` IN 
									(SELECT kd_jadwal FROM tbl_krs 
									WHERE kd_krs LIKE CONCAT(npm_mahasiswa, '{$tahunakademik}%') )")->result();
		return $data;
	}

}

/* End of file M_beban_sks.php */
/* Location: ./application/modules/laporan/models/M_beban_sks.php */