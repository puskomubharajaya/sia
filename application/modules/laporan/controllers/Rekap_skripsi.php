<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekap_skripsi extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		//$this->load->library('Cfpdf');
		/* if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(115)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		} */
	}

	function index()
	{
		$data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'rekap_skripsi_select';
		$this->load->view('template/template', $data);
	}

	function simpan_sesi()
	{

		$jurusan = $this->input->post('jurusan');

		$tahunajaran = $this->input->post('tahunajaran');
		//var_dump($jurusan);exit();
		$this->session->set_userdata('tahunajaran', $tahunajaran);

		$this->session->set_userdata('jurusan', $jurusan);

		redirect(base_url('laporan/rekap_skripsi/load_laporan'));
	}

	function load_laporan()
	{
		$this->load->model('model_laporan');
		$jurusan = $this->session->userdata('jurusan');
		$tahunajaran = $this->session->userdata('tahunajaran');
		$data['rekap'] = $this->model_laporan->getrekap($jurusan, $tahunajaran);
		//var_dump($data['kelas']);die();
		$data['page'] = 'laporan/v_rekap_skripsi';
		$this->load->view('template/template', $data);
	}
}

/* End of file LaporanSemester.php */
/* Location: ./application/modules/laporan/controllers/LaporanSemester.php */
