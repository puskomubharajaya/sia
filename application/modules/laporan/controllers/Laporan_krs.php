<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_krs extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '2048M');
		//$this->load->library('Cfpdf');
		//error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(105)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth', 'refresh');
		}
	}

	function index()
	{
		$data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
		$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'krs_select';
		$this->load->view('template', $data);
	}
	function save_session()
	{
		$tahun = $this->input->post('tahunajaran');
		$this->session->set_userdata('ta', $tahun);
		redirect(base_url() . 'laporan/laporan_krs/show_report', 'refresh');
	}
	public function show_report()
	{
		$current_year 	=  $this->session->userdata('ta');
		$year = substr($current_year, 0, 4);
		$code = substr($current_year, 4, 1);
		if ($code == 1) {
			$before_year = ($year - 1) . '2';
		} else {
			$before_year = $year . '1';
		}
		$log = $this->session->userdata('sess_login')['id_user_group'];
		$userid = $this->session->userdata('sess_login')['userid'];
		$this->load->model('model_laporan');
		$ini = $this->model_laporan->laporan_krs($before_year, $current_year, $log, $userid);

		$data['before_year'] = $before_year;
		$data['ini'] = $ini;
		$data['page'] = 'v_laporan_krs';
		$this->load->view('template', $data);
	}
	public function cetak()
	{
		$current_year 	=  $this->session->userdata('ta');
		$year = substr($current_year, 0, 4);
		$code = substr($current_year, 4, 1);
		if ($code == 1) {
			$before_year = ($year - 1) . '2';
		} else {
			$before_year = $year . '1';
		}
		$log = $this->session->userdata('sess_login')['id_user_group'];
		$userid = $this->session->userdata('sess_login')['userid'];
		$this->load->model('model_laporan');
		$ini = $this->model_laporan->laporan_krs($before_year, $current_year, $log, $userid);

		$data['before_year'] = $before_year;
		$data['userid'] = $userid;
		$data['log'] = $log;
		$data['ini'] = $ini;
		$this->load->view('print/excel_pengisiankrs', $data);
	}
}

/* End of file Statusmhs.php */
/* Location: ./application/modules/laporan/controllers/Statusmhs.php */
