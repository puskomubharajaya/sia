<div class="row">
  <div class="span6">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Rasio Dosen <?php echo $prodi->prodi; ?> Tahun Ajaran <?php echo $ta->tahun_akademik; ?></h3>
      	</div> <!-- /widget-header -->
      	<div class="widget-content">
        	<div class="span5">
        	<table border="1" text-align="center">
    			<tr>
    				<td width="250" align="center"><h3>Perbandingan Jumlah</h3></td>
    				<td width="250" align="center"><h3>Rasio</h3></td>
    			</tr>
    			<tr>
    				<td width="250" align="center"><h3><?php echo $dosen_tetap->juml_dsn; ?> : <?php echo $totalkrs->total; ?></h3></td>
    				<td width="250" align="center"><h3><?php echo $dosen_tetap->juml_dsn/$dosen_tetap->juml_dsn; ?> : <?php echo number_format($totalkrs->total/$dosen_tetap->juml_dsn); ?></h3></td>
    			</tr>
        	</table>
        	</div>
        </div>
   	   </div>
   </div>

   <div class="span6">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Jumlah Kelas <?php echo $prodi->prodi; ?> Tahun Ajaran <?php echo $ta->tahun_akademik; ?></h3>
      	</div> <!-- /widget-header -->
      	<div class="widget-content">
        	<div class="span5">
        		<table border="1" text-align="center">
    			<tr>
    				<?php foreach ($kelasin as $aa) { 
              if ($aa->waktu_kelas == 'PG') {
                echo '<td width="150" align="center"><h3>PAGI</h3></td>';
              }elseif ($aa->waktu_kelas == 'SR') {
                echo '<td width="150" align="center"><h3>SORE</h3></td>';
              }elseif ($aa->waktu_kelas == 'PK') {
                echo '<td width="150" align="center"><h3>P2K</h3></td>';
              } ?>
              
            <?php } ?>
          </tr>
          <tr>
            <?php $a = 0; foreach ($kelasin as $jmlh) { ?>
              <td width="150" align="center"><h3><?php echo $jmlh->jml; ?></h3></td>
            <?php } ?>
    			</tr>
        	</table>
        	</div>
        </div>
      </div>
    </div>

    <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Jumlah Mahasiswa Aktif <?php echo $prodi->prodi; ?> <?php echo $ta->tahun_akademik; ?></h3>
      	</div> <!-- /widget-header -->
      	<div class="widget-content">
        	<div class="span11">
        	<center>
        	<table border="1" text-align="center">
    			<tr>
          <?php foreach ($jumlahkrs as $tahun) { ?>
    				<td width="100" align="center"><h3><?php echo $tahun->TAHUNMSMHS; ?></h3></td>
          <?php } ?>
            <td width="100" align="center"><h3>Total</h3></td>
    			</tr>
    			<tr>
    				<?php $a = 0; foreach ($jumlahkrs as $jmlh) { ?>
            <td width="100" align="center"><h3><?php echo $jmlh->total; ?></h3></td>
          <?php $a = $a+$jmlh->total; } ?>
    				<td width="100" align="center"><h3><?php echo $a; ?></h3></td>
    			</tr>
        	</table>
        	</center>
        	</div>
        </div>
      </div>
    </div>

    <div class="span6">                
      <div class="widget ">
        <div class="widget-header">
          <h3>Jumlah Mahasiswa Cuti & Non Aktif <?php echo $prodi->prodi; ?> Tahun Ajaran <?php echo $ta->tahun_akademik; ?></h3>
      	</div> <!-- /widget-header -->
      	<div class="widget-content">
        	<div class="span5">
        	<table border="1" text-align="center">
    			<tr>
    				<td width="250" align="center"><h3>Cuti</h3></td>
    				<td width="250" align="center"><h3>Non Aktif</h3></td>
    			</tr>
    			<tr>
    				<td width="250" align="center"><h3><?php echo $cuti->jml; ?></h3></td>
    				<td width="250" align="center"><h3>-</h3></td>
    			</tr>
        	</table>
        	</div>
        </div>
   	   </div>
   </div>

   <div class="span6">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Jumlah Dosen <?php echo $prodi->prodi; ?> Tahun Ajaran <?php echo $ta->tahun_akademik; ?></h3>
      	</div> <!-- /widget-header -->
      	<div class="widget-content">
        	<div class="span5">
        	<table border="1" text-align="center">
    			<tr>
    				<td width="250" align="center"><h3>Dosen Tetap</h3></td>
    				<td width="250" align="center"><h3>Dosen Tidak Tetap</h3></td>
    			</tr>
    			<tr>
    				<td width="250" align="center"><h3><?php echo $dosen_tetap->juml_dsn; ?></h3></td>
    				<td width="250" align="center"><h3><?php echo $dosen_tidak->juml_dsn; ?></h3></td>
    			</tr>
        	</table>
        	</div>
        </div>
      </div>
    </div>

    <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>KHS Mahasiswa <?php echo $prodi->prodi; ?> Tahun Ajaran <?php echo $ta->tahun_akademik; ?></h3>
      	</div> <!-- /widget-header -->
      	<div class="widget-content">
        	<div class="span11">
        	<center>
        	<table border="1" text-align="center">
    			<tr>
    				<td width="200" align="center" colspan="2"><h3>IPS < 2.50 </h3></td>
    				<td width="200" align="center" colspan="2"><h3>2.49 > IPS > 3.00</h3></td>
    				<td width="200" align="center" colspan="2"><h3>IPS > 2.99</h3></td>
    				<td width="150" align="center"><h3>Total</h3></td>
    			</tr>
    			<tr>
    				<td width="100" align="center"><h3><?php echo $getips1->total; ?></h3></td>
    				<td width="100" align="center"><h3><?php echo number_format(($getips1->total/$a)*100,2); ?> %</h3></td>
    				<td width="100" align="center"><h3><?php echo $getips2->total; ?></h3></td>
    				<td width="100" align="center"><h3><?php echo number_format(($getips2->total/$a)*100,2); ?> %</h3></td>
    				<td width="100" align="center"><h3><?php echo $getips3->total; ?></h3></td>
    				<td width="100" align="center"><h3><?php echo number_format(($getips3->total/$a)*100,2); ?> %</h3></td>
    				<td width="150" align="center"><h3>100 %</h3></td>

    			</tr>
        	</table>
        	<hr>
        	<table border="1" text-align="center">
    			<tr>
          <?php foreach ($getabcde as $value) { ?>
            <td width="100" align="center"><h3><?php echo $value->NLAKHTRLNM; ?></h3></td>
          <?php } ?>
    				<td width="100" align="center"><h3>Total</h3></td>
    			</tr>
    			<tr>
    			<?php $b = 0; foreach ($getabcde as $value1) { ?>
            <td width="100" align="center"><h3><?php echo $value1->total; ?></h3></td>
          <?php $b = $b+$value1->total; } ?>
    				<td width="100" align="center"><h3><?php echo $b; ?></h3></td>
    			</tr>
        	</table>
        	</center>
        	</div>
        </div>
      </div>
    </div>
</div>