<script>
    function back(){
        window.location.href = "<?php echo base_url('laporan/ruangkosong/destroy_sess');?>";
    }
</script>

<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}

.ada_nilai{
    background-color: #D9534F;
}
</style>

<?php 
$waktu = $this->session->userdata('sess_room');
 ?>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Data Ruangan Tersedia</h3>
            </div> <!-- /widget-header -->

            

            <div class="widget-content">
                <div class="span11">
                    <fieldset>
                        <h4 style="text-align:center">Data Ruangan Tersedia untuk (<i><u><?php echo notohari($waktu['hari']) ?> - <?php echo $waktu['jam'].' - '.substr($jam_keluar,0,5) ?></u></i>) </h4>                        
                        <a href="javascript:;" onclick="back()" class="btn btn-warning" title=""><i class="icon-arrow-left"></i> Kembali</a>
                    </fieldset>
                    <hr>
                    <table id="example1" class="table">
                        <thead>
                            <tr> 

                                <th>Gedung</th>

                                <th>Lantai</th>

                                <th>Kode Ruang</th>

                                <th>Ruang</th>

                                <th>Kapasitas</th>

                                <th>Keterangan</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($kelas as $isi) { ?>
                                <tr>
                                    <td><?php echo $isi->gedung; ?></td>
                                    <td><?php echo $isi->lantai; ?></td>
                                    <td><?php echo $isi->kode_ruangan; ?></td>
                                    <td><?php echo $isi->ruangan; ?></td>
                                    <td><?php if(is_null($isi->kuota) || $isi->kuota === ''){echo " - ";}else{ echo substr($isi->kuota,0,2).' Kursi'; } ?></td>
                                    <td><?php echo $isi->deskripsi; ?></td>
                               </tr>
                            <?php $no++; } ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>
