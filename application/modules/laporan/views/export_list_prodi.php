<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=evaluasi_per_fakultas.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 2px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
<table>
	<thead>
		<th colspan="5">Data Evaluasi Per Fakultas Tahun Akademik <?= get_thajar($sesi); ?></th>
	</thead>
</table>
<table id="" class="table table-bordered table-striped">
	<thead>
        <tr> 
        	<th>No</th>
            <th>Fakultas</th>
            <th>NILAI AKUMULATIF</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($faculty as $value) { ?>
        <tr>
        	<td><?php echo number_format($no); ?></td>
        	<td><?php echo $value->fakultas; ?></td>
        	<?php 
        		$arr = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$value->kd_fakultas,'kd_prodi','asc')->result();

        		$acc = "";

        		foreach ($arr as $key) {
        			$acc = $acc.$key->kd_prodi.',';
        		}

        		$fac = $acc;

        		$sub = substr($fac, 0, -1);

                $this->db2 = $this->load->database('eval', TRUE);

                if ($sesi < '20171') {
                	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM 
                								tbl_pengisian_kuisioner 
                								WHERE substr(kd_jadwal, 1, 5) IN (".$sub.") 
                								AND tahunajaran = '".$sesi."'")->row()->akhir;
                } else {
                	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir FROM 
                								tbl_pengisian_kuisioner_".$sesi." 
                								WHERE substr(kd_jadwal, 1, 5) IN (".$sub.") 
                								AND tahunajaran = '".$sesi."'")->row()->akhir;	
                } 
        	?>

        	<!-- nilai rata-rata -->
        	<?php if ($sesi < '20172') {
        		$average = number_format(($rata2/20),2);
        	} else {
        		$average = number_format((($rata2/20)*10),2);
        	}
        	 ?>

        	<td><?php echo $average; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>