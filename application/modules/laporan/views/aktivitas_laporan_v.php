<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file"></i>
  				<h3>Laporan Aktifitas Mahasiswa <?= get_thnajar($tahunajar); ?></h3>
			</div> 

			<div class="widget-content">
				<div class="span11">
                    
                    <a href="<?= base_url('laporan/laporan_aktifitas_mhs/export_aktifitas/'.$tahunajar); ?>" class="btn btn-success">
                    	<i class="btn-icon-only icon-print"> </i> Print Excel
                    </a>
                    <hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NPM</th>
                                <th>NAMA</th>
                                <th>PRODI</th>
                                <th>JENIS</th>
                                <th>JUDUL</th>
                                <th>LOKASI</th>
                                <th>No. SK</th>
                                <th>Tanggal SK</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($data as $value) { ?>
	                        <tr>
	                        	<td><?= $no ?></td>
	                        	<td><?= $value->npm ?></td>
	                        	<td><?= $value->NMMHSMSMHS ?></td>
	                        	<td><?= get_jur($value->KDPSTMSMHS) ?></td>
	                        	<td><?= $value->jenis ?></td>
	                        	<td><?= $value->judul; ?></td>
	                        	<td><?= $value->lokasi ?></td>
	                        	<td><?= $value->no_sk_tugas ?></td>
	                        	<td><?= $value->tgl_sk_tugas ?></td>
	                        </tr>
                            <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>