<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<a 
  					href="<?= base_url('laporan/evaluasi') ?>" 
  					class="btn btn-default" 
  					style="margin-left: 10px" 
  					data-toggle="tooltip" 
  					title="kembali">
  					<i class="icon-chevron-left" style="margin-left: 0"></i>
  				</a>
  				<h3>Data Evaluasi Dosen <?= get_thnajar($tahunajaran) ?></h3>
			</div>
			
			<div class="widget-content">
				<div class="span11">
                    
                    <a href="<?= base_url(); ?>laporan/evaluasi/export_evl_dosen" class="btn btn-success">
                    	<i class="btn-icon-only icon-print"></i> Print Excel
                    </a>
                    <br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NID</th>
                                <th>NAMA</th>
                                <th>NILAI AKUMULATIF</th>
                                <th>TOTAL SKS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($getData as $value) { ?>
	                        <tr>
	                        	<td><?= number_format($no); ?></td>
	                        	<td><?= $value->kd_dosen; ?></td>
	                        	<td><?= nama_dsn($value->kd_dosen); ?></td>
	                        	<?php 
	                        		$selected_year = $this->session->userdata('year');
		                            $this->db2 = $this->load->database('eval', TRUE);
		                            if ($selected_year < '20171') {
		                            	$rata2 = $this->db2->query("SELECT AVG(hasil_input) AS akhir 
		                            								FROM tbl_pengisian_kuisioner 
		                            								WHERE nid = '{$value->kd_dosen}' 
		                            								AND kd_jadwal LIKE '{$prodi}%' 
		                            								AND tahunajaran = '{$selected_year}'"
		                            							)->row()->akhir;
		                            } else {
		                            	$rata2 = $this->db2->query("SELECT AVG(hasil_input) AS akhir 
		                            								FROM tbl_pengisian_kuisioner_{$selected_year} 
		                            								WHERE nid = '{$value->kd_dosen}' 
		                            								AND kd_jadwal LIKE '{$prodi}%' 
		                            								AND tahunajaran = '{$selected_year}'"
		                            							)->row()->akhir;	
		                            } 
	                        	?>

	                        	<!-- nilai rata-rata -->
	                        	<?php $average = $selected_year < '20172' ? number_format(($rata2/20),2) : ($rata2/20)*10; ?>
	                        	 
	                        	<td><?= number_format($average,2); ?></td>
	                        	<td><?= $value->sks; ?></td>
	                        	<td class="td-actions">
									<a 
										class="btn btn-primary btn-small" 
										target="_blank" 
										href="<?= base_url('laporan/evaluasi/view_detail/'.$value->kd_dosen); ?>">
										<i class="btn-icon-only icon-ok"></i>
									</a>
								</td>
	                        </tr>
                            <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>