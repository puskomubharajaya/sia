<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file"></i>
  				<h3>Beban mengajar dosen <?= get_thnajar($tahunajar); ?></h3>
			</div> 

			<div class="widget-content">
				<div class="span11">
                    
                    <a href="<?= base_url(); ?>laporan/beban_sks_dosen/print_credit_report" class="btn btn-success">
                    	<i class="btn-icon-only icon-print"> </i> Print Excel
                    </a>
                    <hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NIDN</th>
                                <th>NUPN</th>
                                <th>NAMA</th>
                                <th>SKS</th>
	                            <th width="40">Detail</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($data as $value) { ?>
	                        <tr>
	                        	<td><?= $no; ?></td>
	                        	<td><?= (!empty($value->nidn) OR !is_null($value->nidn)) ? $value->nidn : '-'; ?></td>
	                        	<td><?= (!empty($value->nupn) OR !is_null($value->nupn)) ? $value->nupn : '-'; ?></td>
	                        	<td><?= $value->nama; ?></td>
	                        	<td><?= $value->total_sks; ?></td>
	                        	<td class="td-actions">
									<a 
										class="btn btn-primary btn-small" 
										target="_blank" 
										href="<?= base_url('laporan/beban_sks_dosen/detail_mengajar/'.$value->nid); ?>" >
										<i class="btn-icon-only icon-eye-open"></i>
									</a>
								</td>
	                        </tr>
                            <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>