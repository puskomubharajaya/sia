<?php //var_dump($chart_pie);die(); ?>

<script src="<?php echo base_url();?>assets/js/jquery-1.10.2.js"></script>

        <script src="<?php echo base_url();?>assets/js/highcharts/js/highcharts.js"></script>
        <script src="<?php echo base_url();?>assets/js/highcharts/js/modules/exporting.js"></script>
        <style type="text/css">
            ${demo.css}
        </style>
        <script type="text/javascript">
       
$(function () {


    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Persentasi Mahasiswa Tahun Ajaran 2015/2016'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "Persen",
            colorByPoint: true,
            data: [{
            <?php 
                $baris = count($chart_pie);
                $no = 0; 
                foreach ($chart_pie as $isi) {
                    $no++;

                    if ($no == $baris) {
                        echo "name: '".$isi->prodi."', y: ".$isi->jml."}]";    
                    }else{
                        echo "name: '".$isi->prodi."', y: ".$isi->jml."}, {";
                    }
                    
                }
            ?>
            
        }]
    });
});

</script>

<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Kegiatan Perkuliahan</h3>

      </div> <!-- /widget-header -->      

      <div class="widget-content">

        <div class="span11">

        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>akademik/ajar/save_session">

                        <fieldset>

                      <script>

                              $(document).ready(function(){

                                $('#faks').change(function(){

                                  $.post('<?php echo base_url()?>perkuliahan/jdl_kuliah/get_jurusan/'+$(this).val(),{},function(get){

                                    $('#jurs').html(get);

                                  });

                                });

                              });

                              </script>

                              <div class="control-group">

                                <label class="control-label">Fakultas</label>

                                <div class="controls">

                                  <select class="form-control span6" name="fakultas" id="faks">

                                    <option>--Pilih Fakultas--</option>

                                    <?php foreach ($fakultas as $row) { ?>

                                    <option value="<?php echo $row->kd_fakultas.'-'.$row->fakultas;?>"><?php echo $row->fakultas;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div>

                              



                              <div class="control-group">

                                <label class="control-label">Program Studi</label>

                                <div class="controls">

                                  <select class="form-control span6" name="jurusan" id="jurs">

                                    <option>--Pilih Program Studi--</option>

                                  </select>

                                </div>

                              </div>



                              <div class="control-group">

                                <label class="control-label">Tahun Akademik</label>

                                <div class="controls">

                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">

                                    <option>--Pilih Tahun Akademik--</option>

                                    <?php foreach ($tahunajar as $row) { ?>

                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div>  

                            <br/>
                                <input type="submit" class="btn btn-large btn-success" value="Cari"/>
                        </fieldset>

                    </form>
                    <fieldset>
                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                    </fieldset>

        </div>

      </div>

    </div>

  </div>

</div>

