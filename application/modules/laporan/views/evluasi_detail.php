<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Evaluasi Dosen <?= $kry->nama.' '.get_thnajar($tahunajaran); ?></h3>
			</div> 

			<div class="widget-content">
				<div class="span11">
                    
                    <a href="<?= base_url(); ?>laporan/evaluasi/export_per_dosen" class="btn btn-success">
                    	<i class="btn-icon-only icon-print"> </i> Print Excel
                    </a>
                    <hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>KELAS</th>
                                <th>KODE MK</th>
                                <th>MATAKULIAH</th>
                                <th>NILAI</th>
                                <th>TOTAL INPUT</th>
                                <th>JUMLAH MHS</th>
                                <th>SKS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($getData as $value) { ?>
	                        <tr>
	                        	<td><?= number_format($no); ?></td>
	                        	<td><?= $value->kelas; ?></td>
	                        	<td><?= $value->kd_matakuliah; ?></td>
	                        	<td><?= $value->nama_matakuliah; ?></td>

	                        	<?php $this->db2 = $this->load->database('eval', TRUE);
		                            if ($tahunajaran < '20171') {
		                            	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir 
		                            								FROM tbl_pengisian_kuisioner 
		                            								WHERE kd_jadwal = '{$value->kd_jadwal}'"
		                            							)->row()->akhir;

			                            $jmlmhs = $this->db2->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir 
			                            							FROM tbl_pengisian_kuisioner 
			                            							WHERE kd_jadwal = '{$value->kd_jadwal}'"
			                            						)->row()->akhir;

			                            $jmlkrs = $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir 
			                            							FROM tbl_krs WHERE kd_jadwal = '{$value->kd_jadwal}'"
			                            						)->row()->akhir;
		                            } else {
		                            	$rata2 = $this->db2->query("SELECT AVG(hasil_input) as akhir 
		                            								FROM tbl_pengisian_kuisioner_{$tahunajaran} 
		                            								WHERE kd_jadwal = '{$value->kd_jadwal}'"
		                            							)->row()->akhir;

			                            $jmlmhs = $this->db2->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir 
			                            							FROM tbl_pengisian_kuisioner_{$tahunajaran} 
			                            							WHERE kd_jadwal = '{$value->kd_jadwal}'"
			                            						)->row()->akhir;

			                            $jmlkrs = $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir 
			                            							FROM tbl_krs WHERE kd_jadwal = '{$value->kd_jadwal}'"
			                            						)->row()->akhir;	
		                            } ?>

	                        	<!-- nilai rata-rata -->
	                        	<?php if ($tahunajaran < '20172') {
	                        		$average = number_format(($rata2/20),2);
	                        	} else {
	                        		$average = number_format((($rata2/20)*10),2);
	                        	} ?>

	                        	<td><?= $average; ?></td>
	                        	<td><?= number_format($jmlmhs); ?></td>
	                        	<td><?= number_format($jmlkrs); ?></td>
	                        	<td><?= $value->sks_matakuliah; ?></td>
	                        	<td class="td-actions">
									<a 
										class="btn btn-primary btn-small" 
										target="_blank" 
										href="<?= base_url('laporan/evaluasi/view_eval/'.$value->id_jadwal); ?>" >
										<i class="btn-icon-only icon-ok"></i>
									</a>
								</td>
	                        </tr>
                            <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>