<?php 
	$tgl1 = '2016';
	$tgl2 = date('Y')+1;
 ?>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Absensi PMB</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" target="blank" action="<?php echo base_url(); ?>laporan/absensipmb/load_data" method="post">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Tahun</label>
							<div class="controls">
								<select class="form-control span4" name="tahun" required>
									<option disabled="" selected="">--Pilih Tahun--</option>
									<?php for ($i = $tgl1; $i <= $tgl2 ; $i++) { ?>
										<option value="<?php echo substr($i, 2,4); ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Program</label>
							<div class="controls">
								<select class="form-control span4" name="program" required>
									<option disabled="" selected="">--Pilih Program--</option>
									<option value="S1">S1</option>
									<option value="S2">S2</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Jenis</label>
							<div class="controls">
								<select class="form-control span4" name="jenis" required>
									<option disabled="" selected="">--Pilih Jenis--</option>
									<option value="0">Semua Jenis</option>
									<option value="RM">Readmisi</option>
									<option value="MB">Mahasiswa Baru</option>
									<option value="KV">Konversi</option>
								</select>
							</div>
						</div>

						<script>

	                      $(document).ready(function(){

	                        $('#faks').change(function(){

	                          $.post('<?php echo base_url()?>laporan/absensipmb/get_jurusan/'+$(this).val(),{},function(get){

	                            $('#jurs').html(get);

	                          });

	                        });

	                      });

	                    </script>



						<div class="control-group">
							<label class="control-label">Pilih Fakultas</label>
							<div class="controls">
								<select class="form-control span4" name="fak" id="faks" required>
									<option disabled="" selected="">--Pilih Fakultas--</option>
									<?php foreach ($fakultas as $isi) { ?>
										<option value="<?php echo $isi->kd_fakultas ?>"><?php echo $isi->fakultas; ?></option>	
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Prodi</label>
							<div class="controls">
								<select class="form-control span4" name="jur" id="jurs" required>
									<option disabled="" selected="">--Pilih Prodi--</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Pilih Gelombang</label>
							<div class="controls">
								<select class="form-control span4" name="gelombang" required>
									<option disabled="" selected="">--Pilih Gelombang--</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">EKSTRA</option>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Download">
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

