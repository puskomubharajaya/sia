<div class="row">
    <div class="span12">
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Laporan Pengisian KRS</h3>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="span11">
                    <a href="<?php echo base_url(); ?>laporan/laporan_krs/cetak" class="btn btn-success"><i class="icon icon-print"></i> Cetak</a>
                    <br>
                    <hr>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Fakultas</th>
                                <th>Prodi</th>
                                <th>Jumlah Mahasiswa <?php echo $before_year ?></th>
                                <th>Revisi</th>
                                <th>Verifikasi</th>
                                <th>Terajukan</th>
                                <th>Belum Diajukan</th>
                                <th>Sudah isi KRS</th>
                                <th>Belum isi KRS</th>
                                <th>Persentase</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $i = 1;
                            $arrow = '';
                            foreach ($ini as $row => $rw) {
                                // $arrow .= $row . ',';
                                $arrfak = [];
                                $fakultas = '';
                                foreach ($rw as $val => $vals) {
                                    if (!in_array($row, $arrfak)) {
                                        $arrfak[] = $row;
                                        $no = '<td rowspan="' . count($rw) . '">' . $i . '</td>';
                                        $fakultas = '<td rowspan="' . count($rw) . '">' . $row . '</td>';
                                    } else {
                                        $no = '';
                                        $fakultas = '';
                                    }
                            ?>
                                    <tr>
                                        <?php echo $no; ?>
                                        <?php echo $fakultas ?>
                                        <td><?php echo $val ?></td>
                                        <td><?php echo $vals['jml'] ?></td>
                                        <td><?php echo $vals['revisi'] ?></td>
                                        <td><?php echo $vals['verifikasi'] ?></td>
                                        <td><?php echo $vals['terajukan'] ?></td>
                                        <td><?php echo $vals['belum_diajukan'] ?></td>
                                        <td><?php echo $vals['total_krsan'] ?></td>
                                        <td><?php echo $vals['belum_input'] ?></td>
                                        <td><?php echo $vals['persentase'] ?></td>
                                    </tr>
                            <?php
                                }
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>