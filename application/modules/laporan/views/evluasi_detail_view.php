<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Evaluasi Dosen <?= $this->session->userdata('nama_dosen').' Kelas '.$kdjadwal->kelas; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="tabbable">
		            <ul class="nav nav-tabs">
		              <li class="active"><a href="#banyak" data-toggle="tab">Penilaian</a></li>
		              <li><a href="#satuan" data-toggle="tab">Saran</a></li>
		            </ul>
	            	<div class="tab-content">
	                  <div class="tab-pane active" id="banyak">
	                  	<div class="span11">
		                    
		                    <a href="<?= base_url(); ?>laporan/evaluasi/export_hasil" class="btn btn-success"><i class="btn-icon-only icon-print"> </i> Print Excel</a>
		                    <br><hr>
							<table id="example1" class="table table-bordered table-striped">
			                	<thead>
			                        <tr> 
			                        	<th>No</th>
		                                <th>Parameter</th>
		                                <th>Nilai</th>
			                        </tr>
			                    </thead>
			                    <tbody>
		                            <?php $no = 1; foreach ($getData as $value) { ?>
			                        <tr>
			                        	<td><?= number_format($no); ?></td>
			                        	<td><?= $value->parameter; ?></td>

			                        	<!-- nilai rata-rata -->
							            <?php if ($this->session->userdata('ta') < '20172') {
							                $average = number_format(($value->nilai/20),2);
							            } else {
							                $average = number_format(($value->nilai*1.25),2);
							            } ?>

			                        	<td><?= $average; ?></td>
			                        </tr>
		                            <?php $no++; } ?>
			                    </tbody>
			               	</table>
						</div>
	                  </div>
	                  <div class="tab-pane" id="satuan">
	                  	<div class="span11">
		                    
		                    <a href="<?= base_url(); ?>laporan/evaluasi/export_saran" class="btn btn-success"><i class="btn-icon-only icon-print"> </i> Print Excel</a>
		                    <br><hr>
							<table id="example6" class="table table-bordered table-striped">
			                	<thead>
			                        <tr> 
			                        	<th>No</th>
		                                <th>Saran</th>
			                        </tr>
			                    </thead>
			                    <tbody>
		                            <?php $no = 1; foreach ($getData2 as $value) { ?>
			                        <tr>
			                        	<td><?= number_format($no); ?></td>
			                        	<td><?= $value->saran; ?></td>
			                        </tr>
		                            <?php $no++; } ?>
			                    </tbody>
			               	</table>
						</div>
	                  </div>
	                </div>
	            </div>				
			</div>
		</div>
	</div>
</div>