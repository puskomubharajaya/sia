<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_mhs_baru_all_program.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>
                    
<table>
	<thead>
        <tr> 
        	<th rowspan="2">No</th>
            <th rowspan="2">Prodi</th>
            <th rowspan="2">Angkatan</th>
            <th colspan="3">A</th>
            <th colspan="3" rowspan="2">C</th>
            <th colspan="3" rowspan="2">N</th>
        </tr>
        <tr>
            <th>PG</th>
            <th>SR</th>
            <th>P2K</th>
        </tr>
        

        

        
    </thead>
    <tbody>     
        <?php foreach  ($prodi as $isi) { ?>
        <tr>
        <?php $q=$this->db->query('SELECT COUNT(vkrs.`npm_mahasiswa`),vkrs.`kelas` FROM tbl_verifikasi_krs vkrs
                                JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = vkrs.`npm_mahasiswa`
                                WHERE vkrs.`tahunajaran` = "'.$thn.'" AND vkrs.`kd_jurusan` = '.$isi->kd_prodi.'
                                GROUP BY vkrs.`kelas` ORDER BY vkrs.`kelas` ASC')->result(); ?>

            <?php foreach ($q as $key) { ?>
                <td><?php echo $key->jml.'/'.$key->kelas; ?></td>  
            <?php } ?>
            <td></td>
            <td></td>
        </tr>    
        <?php } ?>
    </tbody>
</table>