<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Changelog extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo '<script>alert("Sesi Anda telah habis. Silahkan Log-in kembali!")</script>';
			redirect('auth', 'refresh');
		}
		error_reporting(0);
	}

	function index()
	{
		$data['page'] = 'extra/v_changelog';
		$data['versi'] = $this->app_model->getdata('tbl_version', 'id_versi', 'DESC')->result();
		$data['detail'] = $this->app_model->getdata('tbl_version_detail', 'versi_sia', 'DESC')->result();
		$this->load->view('template/template', $data);
	}
}

/* End of file account.php */
/* Location: ./application/controllers/changelog.php */
