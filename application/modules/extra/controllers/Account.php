<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') != TRUE) {
		 	redirect('auth','refresh');
		}
		error_reporting(0);
	}

	function index()
	{
		$data['page'] = 'extra/account';
		$this->load->view('template/template',$data);
	}

	function ganti_pass(){ #nomenu
		$this->load->view('extra/account_no_menu');	
	}

	function simpan(){#nomenu
		$user = $this->session->userdata('sess_login');
		$new = $this->input->post('new', TRUE);
		$old = $this->input->post('old', TRUE);

		if ($user['password_plain'] == $old) {

			$data['password_plain']	= $new;
			$data['password']		= sha1(md5($data['password_plain']).key);

			$update = $this->app_model->updatedata('tbl_user_login','userid',$user['userid'],$data);

			if ($update == TRUE) {
				$cekmail = $this->db->where('npm',$user['userid'])->get('tbl_bio_mhs',1)->row();

				if (is_null($cekmail->email) || $cekmail->email == '') {
					echo "<script>alert('Berhasil');document.location.href='".base_url()."extra/account/mail';</script>";
				}else{
					echo "<script>alert('Berhasil');document.location.href='".base_url()."home';</script>";
				}
			} else {
				echo "<script>alert('Gagal Edit Data');history.go(-1);</script>";
			}
		} else {
			echo "<script>alert('Password Lama Salah');history.go(-1);</script>";
		}
	}

	function mail(){#nomenu
		$this->load->view('extra/account_email');	
	}

	function save_mail(){
		$user = $this->session->userdata('sess_login');
		$mail = $this->input->post('email',TRUE);

		$object = array('email' => $mail);

		$update = $this->db->where('npm', $user['userid'])
						   ->update('tbl_bio_mhs', $object);

		if ($update == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."home';</script>";
		} else {
			echo "<script>alert('Gagal Edit Data');history.go(-1);</script>";
		}
	}


	function changepass()
	{
		$user = $this->session->userdata('sess_login');
		$new = $this->input->post('new', TRUE);
		$old = $this->input->post('old', TRUE);

		if ($user['password_plain'] == $old) {

			$data['password_plain'] = $new;
			$data['password'] = sha1(md5($data['password_plain']).key);

			$update = $this->db->where('userid', $user['userid'])
								->where('username', $user['username'])
								->update('tbl_user_login', $data);
			// $update = $this->app_model->updatedata('tbl_user_login','userid',$user['userid'],$data);
			if ($update == TRUE) {
				echo "<script>alert('Berhasil');document.location.href='".base_url()."auth/logout';</script>";
			} else {
				echo "<script>alert('Gagal Edit Data');history.go(-1);</script>";
			}
		} else {
			echo "<script>alert('Password Lama Salah');history.go(-1);</script>";
		}
		
	}

}

/* End of file account.php */
/* Location: ./application/controllers/account.php */