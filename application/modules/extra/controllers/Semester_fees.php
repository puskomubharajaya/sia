<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Semester_fees extends CI_Controller {

	protected $userid;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'),'refresh');
		}
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->load->model('Semester_fee_model', 'fee');
	}

	public function index()
	{
		$data['page'] = "semester_fee_v";
		$this->load->view('template/template', $data);
	}

	public function get_mahasiswa($angkatan=0, $kelas=0)
	{
		$data = $this->fee->get_mahasiswa($this->userid, $angkatan, $kelas);

		foreach ($data as $row) {
			$response[] = [
				'npm'   => $row->NIMHSMSMHS,
				'nama'  => $row->NMMHSMSMHS,
				'kelas' => $this->_set_kelas($row->kategori_kelas),
				'biaya' => is_null($row->biaya_semester) ? '-' : $row->biaya_semester
			];
		}

		echo json_encode($response);
	}

	private function _set_kelas($kelas)
	{
		if ($kelas == 'PG') {
			$return_class = 'A';
		} elseif ($kelas == 'SR') {
			$return_class = 'B';
		} else {
			$return_class = 'C';
		}
		return $return_class;
	}

	public function set_fee()
	{
		$angkatan = $this->input->post('angkatan');
		$kelas    = $this->input->post('kelas');
		$biaya    = $this->input->post('biaya');

		$set   = ['biaya_semester' => $biaya];
		$this->fee->set_fee($set, $angkatan, $this->userid, $kelas);
		return;
	}

}

/* End of file Semester_fees.php */
/* Location: ./application/modules/extra/controllers/Semester_fees.php */