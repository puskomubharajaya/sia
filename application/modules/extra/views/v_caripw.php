<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=npm]').autocomplete({

        source: '<?php echo base_url('extra/servis/load_mhs_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.npm.value = ui.item.value;

        }

    });

    $('input[name^=old]').autocomplete({

        source: '<?php echo base_url('extra/servis/load_mhs_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.old.value = ui.item.value;

        }

    });

    $('input[name^=nid]').autocomplete({

        source: '<?php echo base_url('extra/servis/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.nid0.value = ui.item.value;

        }

    });

});

</script>

<?php $user = $this->session->userdata('sess_login'); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-wrench"></i>
  				<h3>Service User</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#home">Cari Akun Mahasiswa</a></li>
					    <li><a data-toggle="tab" href="#menu0">Cari Akun Dosen</a></li>
					    <li><a data-toggle="tab" href="#menu1">Reset Password</a></li>
					</ul>
					<div class="tab-content">
					    <div id="home" class="tab-pane fade in active">
							<!-- <a href="<?php echo base_url(); ?>adminpuskom/board">Home</a> / <a href="<?php echo base_url(); ?>adminpuskom/cari_pw">Cari Akun</a> -->
							<b><center>Cari Password Akun Mahasiswa</center></b><br>
							<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>extra/servis/cari">
								<fieldset>
									<div class="control-group">											
										<label class="control-label">NPM : </label>
										<div class="controls">
											<input type="text" class="span3" id="npm" name="npm" placeholder="Masukan NPM" required>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									
									<div class="form-actions">
										<input type="submit" class="btn btn-primary" id="save" value="Cari Password"/>
									</div> <!-- /form-actions -->
								</fieldset>
							</form>
						</div>

						<div id="menu0" class="tab-pane fade">
							<!-- <a href="<?php echo base_url(); ?>adminpuskom/board">Home</a> / <a href="<?php echo base_url(); ?>adminpuskom/cari_pw">Cari Akun</a> -->
							<b><center>Cari Password Akun Dosen</center></b><br>
							<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>extra/servis/cari_dosen">
								<fieldset>
									<div class="control-group">											
										<label class="control-label">NID : </label>
										<div class="controls">
											<input type="text" class="span3" id="nid0" name="nid" placeholder="Masukan NID" required>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									
									<div class="form-actions">
										<input type="submit" class="btn btn-primary" id="save" value="Cari Password"/>
									</div> <!-- /form-actions -->
								</fieldset>
							</form>
						</div>

					    <div id="menu1" class="tab-pane fade">
									<!-- <a href="<?php echo base_url(); ?>adminpuskom/board">Home</a> / <a href="<?php echo base_url(); ?>adminpuskom/reset">reset password</a> -->
							<b><center>Reset Password Akun</center></b><br>
							<div class="alert alert-info">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<strong>Perhatian!</strong> Pasword yang telah di-reset akan berubah menjadi <strong>Ubharaj4y4</strong>
							</div>
							<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>extra/servis/ganti">
								<fieldset>
									<div class="control-group">											
										<label class="control-label">User ID : </label>
										<div class="controls">
											<input type="text" class="span3" id="old" name="old" placeholder="Masukan User ID" required>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									<div class="form-actions">
										<input type="submit" class="btn btn-primary" id="save" value="Reset Password"/>
									</div> <!-- /form-actions -->
								</fieldset>
							</form>
						</div>
					</div>
			    </div>
			</div>

		</div>
	</div>
</div>