<script type="text/javascript">
jQuery(document).ready(function($) {
    $('input[name^=old]').autocomplete({
        source: '<?php echo base_url('extra/servis/load_dosen_autocomplete');?>',
        minLength: 1,
        select: function (evt, ui) {
            this.form.old.value = ui.item.value;
        }
    });
});
</script>

<style>
	.form-horizontal .control-label {
		width: 0 !important;
	}

	.form-horizontal .controls {
		margin-left: 80px !important;
	}
</style>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-money"></i>
  				<h3>Biaya Semester</h3>
  				<button 
  					class="btn btn-warning pull-right" 
  					style="margin-top: 6px; margin-right: 10px;"
  					data-toggle="modal"
  					data-target="#myModal">
  					Informasi
  				</button>
			</div>
			<?php $init_year = date('Y') - 7; ?>
			<div class="widget-content">
				<div class="span11">
					<form action="" class="form-horizontal">
						<div class="control-group">											
							<label class="control-label">Angkatan</label>
							<div class="controls">
								<select name="angkatan" id="angkatan" class="form-group span3">
									<option selected="" disabled=""></option>
									<?php for ($i = $init_year; $i <= date('Y'); $i++) : ?>
										<option value="<?= $i ?>"><?= $i ?></option>
									<?php endfor; ?>
								</select>
							</div>
						</div>
						<div class="control-group">											
							<label class="control-label">Kelas</label>
							<div class="controls">
								<select name="kelas" id="kelas" class="form-group span3">
									<option selected="" disabled=""></option>
									<option value="PG">A</option>
									<option value="SR">B</option>
									<option value="PK">C</option>
								</select>
							</div> <!-- /controls -->				
						</div>
					</form>

					<hr>
					
					<div class="control-group">											
						<label class="control-label">Atur Biaya Semester</label>
						<div class="controls">
							<input type="text" class="form-group span4" id="fee" onkeypress="return isNumber(event)">
							<button class="btn btn-warning" id="btn-set" style="margin-top: -9px" onclick="set_fee()">
								<i class="icon-gear"></i>
							</button>
						</div>
					</div>

					<form id="edit-profile" class="form-horizontal" method="post" action="">
                        <fieldset>
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr> 
                                        <th>No</th>
                                        <th>NPM</th>
                                        <th width="400">Nama</th>
                                        <th>Kelas</th>
                                        <th>Biaya (Rp.)</th>
                                    </tr>
                                </thead>
                                <tbody id="tabel-content">
                                    
                                </tbody>
                            </table>
                        </fieldset>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Cara Penggunaan Fitur Biaya Semester</h4>
      </div>
      <div class="modal-body">
        <ul>
        	<li>Pilih angkatan dan kategori kelas yang akan diperbarui data biaya semesternya</li>
        	<li>Tinjau kembali daftar mahasiswa yang dimunculkan. Jika terdapat anomali silahkan lapor ke Dit. PTI </li>
        	<li>Atur biaya semester yang akan diberlakukan untuk angkatan dan kategori kelas terkait</li>
        	<li>Klik tombol kuning yang terdapat disamping kolom <b>atur biaya semester</b></li>
        	<li>Akan muncul dialog untuk memastikan biaya yang diatur telah sesuai</li>
        	<li>Data biaya semester ini akan dimunculkan di sub menu <b>status mahasiswa</b> dan <b>KRS & KHS mahasiswa</b> pada menu feeder</li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>

	$('#kelas, #angkatan').change(function() {
		$('#tabel-content').empty();
		var url = '<?= base_url('extra/semester_fees/get_mahasiswa/') ?>' + $('#angkatan').val() +'/'+ $('#kelas').val();
		$.get(url, function(res) {
			var parse = JSON.parse(res);
			parse.forEach(function(element, i) {
				document.getElementById("tabel-content").innerHTML +=
				`<tr> 
	                <td>${i+1}</td>
	                <td>${element.npm}</td>
	                <td>${element.nama}</td>
	                <td>${element.kelas}</td>
	                <td>${idr_format(element.biaya)}</td>
	            </tr>`;
			});
		});
	});

	function idr_format(curr) {
		var bilangan = curr;
	
		var	number_string = bilangan.toString(),
			sisa 	= number_string.length % 3,
			rupiah 	= number_string.substr(0, sisa),
			ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
				
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		return rupiah;
	}

	function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

	function set_fee() {
		if (confirm("Proses ini akan mengubah biaya semester pada angkatan dan ketegori kelas terkait, lanjutkan?")) {
			var fee = document.getElementById('fee').value;
			$.post('<?= base_url('extra/semester_fees/set_fee') ?>', {
				angkatan: $('#angkatan').val(), 
				kelas: $('#kelas').val(),
				biaya: $('#fee').val()
			}, function(res) {
				$('#tabel-content').empty();
				var url = '<?= base_url('extra/semester_fees/get_mahasiswa/') ?>' + $('#angkatan').val() +'/'+ $('#kelas').val();
				$.get(url, function(res) {
					var parse = JSON.parse(res);
					parse.forEach(function(element, i) {
						document.getElementById("tabel-content").innerHTML +=
						`<tr> 
		                    <td>${i+1}</td>
		                    <td>${element.npm}</td>
		                    <td>${element.nama}</td>
		                    <td>${element.kelas}</td>
		                    <td>${element.biaya}</td>
		                </tr>`;
					});
				});
			})
		}
	}
</script>