<script type="text/javascript">
	jQuery(document).ready(function($) {
	    $('input[name^=npm]').autocomplete({
	        source: '<?php echo base_url('extra/servis/load_mhs_autocomplete');?>',
	        minLength: 4,
	        select: function (evt, ui) {
	            this.form.npm.value = ui.item.value;
	        }
	    });
	});
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-refresh"></i>
  				<h3>Rubah Kelas</h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="span11">
					<form class="form-horizontal" method="post" action="<?= base_url(); ?>extra/servis/executeChangeClass">
						<fieldset>
							<div class="control-group">											
								<label class="control-label">NPM</label>
								<div class="controls">
									<input type="text" class="span3" name="npm" placeholder="Masukan NPM" required>
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							<div class="control-group">											
								<label class="control-label">Kelas</label>
								<div class="controls">
									<select name="kelas" class="form-group span3">
										<option selected="" disabled=""></option>
										<option value="PG">A</option>
										<option value="SR">B</option>
										<option value="PK">C</option>
									</select>
								</div> <!-- /controls -->				
							</div>
							<div class="form-actions">
								<input type="submit" class="btn btn-primary" id="save" value="Rubah Kelas"/>
							</div> <!-- /form-actions -->
						</fieldset>
					</form>
			    </div>
			</div>
		</div>
	</div>
</div>