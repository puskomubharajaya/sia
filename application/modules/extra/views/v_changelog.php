<div class="row">
  <div class="span12">
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-list-alt"></i>
      </div> <!-- /widget-header -->
      <div class="widget-content">
        <div class="span11">
          <a href="<?= base_url() ?>" class="btn btn-warning pull-right"><i class="icon-chevron-left"></i> Kembali</a>
          <h1>Changelog</h1>
          <hr>
          <h4 style="margin-bottom: 50px">Semua perubahan penting pada proyek ini akan didokumentasikan dalam file ini.</h4>
          <?php foreach ($versi as $row) { ?>
            <h2>
              <?php $date = explode(' ', $row->created_at)[0]; $clock = explode(' ', $row->created_at)[1]; ?>
              <span style="color:blue">Version <?= $row->versi ?></span> - <?= TanggalIndo($date).', '.$clock ?>
            </h2>
            <hr>
            <span><?= $row->keterangan ?></span>
            <h3>Penambahan / Perubahan</h3>
            <ul>
              <?php foreach ($detail as $dtl) {
                if ($dtl->versi_sia == $row->id_versi) {
                  $pisah = explode(",", $dtl->vlog);
                  foreach ($pisah as $satuan) { ?>
                    <li><?= $satuan ?></li>
                  <?php }
                }
              } ?>
            </ul>
            <br>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<br>