<form id="" action="" method="post" >            
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= $announcement->titles ?></h4>
    </div>

    <div class="modal-body">
        <embed src="<?= base_url(substr($announcement->pathfile, 2)) ?>" class="span12" style="height: 500px" alt="">
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>