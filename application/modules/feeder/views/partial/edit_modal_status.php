<form action="<?= base_url('feeder/status_mahasiswa/update_akm_unit') ?>" method="post" class="form-horizontal">
	<div class="modal-header">
		<h4 class="modal-title">Edit Komponen AKM - <?= $nama.' ('.$npm.')' ?></h4>
	</div>
	<div class="modal-body">
		<div class="alert alert-warning">
          <strong>Perhatian!</strong> Setiap perubahan akan dicatat waktu dan aktor yang melakukan perubahan.
        </div>
		<input type="hidden" name="npm" value="<?= $npm ?>">
		<input type="hidden" name="tahunakademik" value="<?= $akm->THSMSTRAKM ?>">
		<input type="hidden" name="id_reg_mhs" value="<?= $id_reg_mhs ?>">
		<input type="hidden" name="status" value="<?= $status ?>">
		<div class="control-group">                     
		    <label class="control-label">Semester terakhir</label>
		    <div class="controls">
		      	<input type="text" value="<?= get_thnajar($akm->THSMSTRAKM) ?>" class="span3" readonly="" />
		    </div>
	  	</div>
		<div class="control-group">                     
		    <label class="control-label">SKS Total</label>
		    <div class="controls">
		      	<input type="text" value="<?= explode('.', $akm->SKSTTTRAKM)[0] ?>" class="span3" name="sks" maxlength="3" onkeypress="return isValid(event)">
		    </div>
	  	</div>
	  	<div class="control-group">                     
		    <label class="control-label">IPK</label>
		    <div class="controls">
		      	<input type="text" value="<?= $akm->NLIPKTRAKM ?>" class="span3" name="ipk" maxlength="4" onkeypress="return isValid(event)">
		    </div>
	  	</div>    
	  </div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-warning">Update</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>

<script>
	// input validation
    var RegEx = new RegExp(/^\d*\.\d*$/);

    function isValid(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
		 	return false;
		}

		return true;
    }
</script>