<?php 
	$angkatan = $this->session->userdata('session_akm')['angkatan'];
	$semester = $this->session->userdata('session_akm')['tahunakademik'];
 ?>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-file"></i>
  				<button 
  					type="button"
  					onclick="return location.href = '<?= base_url('sync_feed/krs/akm_list') ?>'" 
  					class="btn btn-warning pull-right" style="margin-top: 6px; margin-right: 5px;">
  					Kembali
  				</button>
  				<h3>Aktifitas Kuliah Mahasiswa Angkatan <?= $angkatan ?> Tahun Akademik <?= get_thnajar($semester) ?></h3>
			</div> <!-- /widget-header -->

			<div class="widget-content">
				<div class="span11">
  				<div class="alert alert-warning">
					<strong>Perhatian!</strong> Fitur ini hanya digunakan untuk memperbarui data yang telah di-<i>input</i> sebelumnya. Jika data pada tahun akademik terpilih belum pernah di-<i>input</i> maka tidak akan ada perubahan. Data berikut merupakan data yang diambil dari feeder. Setiap perubahan yang dilakukan akan <u>berdampak langsung pada data riil feeder</u> dan <u>data riil SIA</u>.
				</div>
				
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>NIM</th>
	                        	<th>Nama Mahasiswa</th>
	                        	<th>SKS Semester</th>
	                        	<th>IPS</th>
	                        	<th>SKS Total</th>
	                        	<th>IPK</th>
	                        	<th>Biaya Kuliah</th>
	                        	<th>Edit</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php $no=1; foreach ($akm as $data) { ?>
		                        <tr>
		                        	<td><?= $no; ?></td>
		                        	<td><?= $data->nim; ?></td>
		                        	<td><?= $data->nama_mahasiswa; ?></td>
	                                <td><?= $data->sks_semester; ?></td>
	                                <td><?= $data->ips ?></td>
									<td><?= $data->sks_total ?></td>
									<td><?= $data->ipk ?></td>
									<td><?= 'Rp. '.format_harga_indo($data->biaya_kuliah_smt) ?></td>
									<td>
										<button 
											type="button" 
											class="btn btn-warning" 
											data-toggle="modal" 
											data-target="#editModal" 
											onclick="edit('<?= $data->id_registrasi_mahasiswa ?>')">
											<i class="icon-pencil"></i>
										</button>
									</td>
	                                
								</tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>
<br>

<div id="editModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?= base_url('feeder/akm/sync_akm') ?>" method="POST" class="form-horizontal">
				<div class="modal-header">
					<h4 class="modal-title">Ubah Komponen AKM</h4>
				</div>
				<div class="modal-body" id="content">

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">Update</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	function edit (id_reg_mhs) {
		$('#content').load('<?= base_url('feeder/akm/load_detail_akm/') ?>' + id_reg_mhs);
	}
</script>