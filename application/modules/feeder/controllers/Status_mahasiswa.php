<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_mahasiswa extends CI_Controller {

	private $userid, $usergroup, $token, $tahunakademik;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'),'refresh');
		}

		if (!$this->session->userdata('tahunajaran_stmhs')) {
			echo '<script>alert("Mohon pilih tahun ajaran terlebih dahulu!")</script>';
			redirect(base_url('sync_feed/status_mhs'),'refresh');
		}

		$this->load->model('feeder_perkuliahan_model','feedkuliah');
		$this->load->library('pddikti');
		$this->tahunakademik = $this->session->userdata('tahunajaran_stmhs');
		$this->token = $this->pddikti->get_token();
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
	}

	public function cuti()
	{
		$reg_prodi = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
		$data['status'] = 'cuti';
		$data['tahunajar'] = $this->tahunakademik;
		$data['data'] = $this->_status_mahasiswa($this->tahunakademik, $reg_prodi, 'C');
		$data['page'] = "feeder_status_mhs_v";
		$this->load->view('template/template', $data);
	}

	public function nonaktif()
	{
		$reg_prodi = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
		$data['status'] = 'nonaktif';
		$data['tahunajar'] = $this->tahunakademik;
		$data['data'] = $this->_status_mahasiswa($this->tahunakademik, $reg_prodi, 'N');
		$data['page'] = "feeder_status_mhs_v";
		$this->load->view('template/template', $data);
	}

	private function _status_mahasiswa($tahunakademik, $prodi, $status)
	{
		$filter = "id_semester = '$tahunakademik' and id_prodi = '$prodi' and id_status_mahasiswa = '$status'";
		$payload = [
			"act"    => "GetDetailPerkuliahanMahasiswa",
			"token"  => $this->token,
			"filter" => $filter
		];
		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec)->data;
		return $result;
	}

	public function get_gap($status)
	{
		$mhs        = $this->input->post('mhs');
		$all_mhs    = $this->_npm_object($mhs);

		if ($status == 'cuti') {
			$unsycn_mhs = $this->feedkuliah->get_unsync_cuti($this->tahunakademik, $this->userid, $all_mhs);
		} else {
			$unsycn_mhs = $this->feedkuliah->get_unsync_nonaktif($this->tahunakademik, $this->userid, $all_mhs);
		}

		if (count($unsycn_mhs) < 1) {
			echo '<script>alert("Tidak ditemukan data baru!")</script>';
			redirect(base_url('feeder/status_mahasiswa/'.$status),'refresh');
		}

		$this->_resync($unsycn_mhs, $this->tahunakademik, $status);
	}

	private function _npm_object($mhs)
	{
		$mhs_list = '(';
		foreach ($mhs as $row => $value) {
			$mhs_list .= "'$value'".',';
		}
		$mhs_list .= ')';
		$all_mhs = substr($mhs_list, 0, -2).')';
		return $all_mhs;
	}

	private function _resync($unsycn_mhs, $academic_year, $status)
	{
		$status_mhs = $status == 'cuti' ? 'C' : 'N';

		foreach ($unsycn_mhs as $value) {
			$mhs = $value->NIMHSMSMHS;
	        $ipk = $value->ipk;
	        $fee = $value->biaya_semester;
	        $sks = $value->sks;

	        $id_reg_pd = $this->_get_mahasiswa_feeder($this->token, $mhs);

			$record['id_semester']             = $academic_year;
			$record['id_registrasi_mahasiswa'] = json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa;
			$record['id_status_mahasiswa']     = $status_mhs;
			$record['ips']                     = 0;
			$record['ipk']                     = $ipk; 
			$record['sks_semester']            = 0;
			$record['total_sks']               = $sks;
			$record['biaya_kuliah_smt']        = (int)$fee;

			$payload = [
				'act'    => 'InsertPerkuliahanMahasiswa',
				'token'  => $this->token,
				'record' => $record
			];

	        $result = $this->pddikti->runWS($payload);
	        dd(json_decode($result));
	        echo "<hr>";	
		}	
	}

	private function _get_mahasiswa_feeder($token, $npm)
    {
        $payload2 = [
            'act'    => 'GetListMahasiswa',
            'token'  => $token,
            'filter' => "nim = '$npm'" 
        ];
        $mahasiswa = $this->pddikti->runWS($payload2);
        return $mahasiswa;
    }

    public function difference($status)
    {
    	$status_mhs = $status == 'cuti' ? 'C' : 'N';
    	$reg_prodi = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;

    	$data_feeder = $this->_status_mahasiswa($this->tahunakademik, $reg_prodi, $status_mhs);
    	foreach ($data_feeder as $feeder) {
    		$feeds[] = $feeder->nim;
    	}

    	if ($status == 'cuti') {
			$data_sia = $this->feedkuliah->get_cuti_list($this->tahunakademik, $this->userid);
		} else {
			$data_sia = $this->feedkuliah->get_nonactive_current_year($this->userid,$this->tahunakademik,studyStart($this->tahunakademik));
		}
		foreach ($data_sia as $sia) {
			$siak[] = $sia->NIMHSMSMHS;
		}

		$data['status'] = $status;
		$data['tahunakademik'] = get_thnajar($this->tahunakademik);
		$data['difference'] = array_diff($siak, $feeds);
		$this->load->view('partial/gap_modal_v', $data);
    }

    public function unit_sync($npm, $status)
    {
    	$status_mhs = ($status == 'cuti') ? "C" : "N";
    	$get_mhs = $this->feedkuliah->get_unit_mhs($npm);

    	$mhs = $get_mhs->NIMHSTRAKM;
        $ipk = $get_mhs->NLIPKTRAKM;
        $fee = $get_mhs->biaya_semester;
        $sks = $get_mhs->SKSTTTRAKM;

        $id_reg_pd = $this->_get_mahasiswa_feeder($this->token, $npm);

		$record['id_semester']             = $this->tahunakademik;
		$record['id_registrasi_mahasiswa'] = json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa;
		$record['id_status_mahasiswa']     = $status_mhs;
		$record['ips']                     = 0;
		$record['ipk']                     = $ipk; 
		$record['sks_semester']            = 0;
		$record['total_sks']               = $sks;
		$record['biaya_kuliah_smt']        = (int)$fee;

		$payload = [
			'act'    => 'InsertPerkuliahanMahasiswa',
			'token'  => $this->token,
			'record' => $record
		];

        $result = $this->pddikti->runWS($payload);
        dd(json_decode($result));
        echo "<hr>";
    }

    public function edit_on_feeder($id_reg_mhs, $npm, $status)
    {
		$data['akm']        = $this->feedkuliah->get_akm_unit($npm);
		$data['npm']        = $npm;
		$data['nama']       = get_nm_mhs($npm);
		$data['status']     = $status;
		$data['id_reg_mhs'] = $id_reg_mhs;
    	$this->load->view('partial/edit_modal_status', $data);
    }

    public function update_akm_unit()
    {
		$status        = $this->input->post('status');
		$status_mhs    = ($status == 'cuti') ? 'C' : 'N';
		$id_reg_mhs    = $this->input->post('id_reg_mhs');
		$tahunakademik = $this->input->post('tahunakademik');
		$npm           = $this->input->post('npm');
		$sks           = $this->input->post('sks');
		$ipk           = $this->input->post('ipk');

		if (substr($ipk, 0,1) == '.') {
			echo '<script>alert("Format IPK tidak valid!")</script>';
			redirect(base_url('/feeder/status_mahasiswa/'.$status),'refresh');
		}

		$record['id_semester']             = $tahunakademik;
		$record['id_registrasi_mahasiswa'] = $id_reg_mhs;
		$record['id_status_mahasiswa']     = $status_mhs;
		$record['ips']                     = 0;
		$record['ipk']                     = $ipk; 
		$record['sks_semester']            = 0;
		$record['total_sks']               = $sks;

		$payload = [
			'act'    => 'UpdatePerkuliahanMahasiswa',
			'key'    => ['id_registrasi_mahasiswa' => $id_reg_mhs],
			'token'  => $this->token,
			'record' => $record
		];

        $exec = $this->pddikti->runWS($payload);
        $result = json_decode($exec);

        if ($result->error_code == 0) {
        	$this->_update_akm_sia($tahunakademik, $npm, $sks, $ipk, $this->userid);
        	echo '<script>alert("Update berhasil!"); history.go(-1);</script>';
        	exit();
        }

        dd($result,2);
    }

    private function _update_akm_sia($tahunakademik, $npm, $sks, $ipk, $userid)
    {
    	$this->feedkuliah->update_akm_sia($tahunakademik, $npm, $sks, $ipk, $userid);
    	return;
    }

}

/* End of file Status_mahasiswa.php */
/* Location: ./application/modules/feeder/controllers/Status_mahasiswa.php */