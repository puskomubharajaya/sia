<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	private $userid, $username, $usergroup, $token, $tahunakademik;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'),'refresh');
		}

		if (!$this->session->userdata('tahunajaran_kelas')) {
			echo '<script>alert("Harap pilih tahun ajaran dahulu!")</script>';
			redirect(base_url('sync_feed/kelas'),'refresh');
		}

		$this->load->library('pddikti');
		$this->token = $this->pddikti->get_token();
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->username = $this->session->userdata('sess_login')['username'];
		$this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->tahunakademik = $this->session->userdata('tahunajaran_kelas');
	}

	public function index()
	{
		$id_prodi_feeder   = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
		$data['tahunajar'] = $this->session->userdata('tahunajaran_kelas');
		$filter            = "id_prodi = '$id_prodi_feeder' and id_semester = '$this->tahunakademik'";
		$data['data']      = $this->_get_data_feeder($this->token, 'GetListKelasKuliah', $filter);
		$data['page']      = "feeder_kelas_v";
		$this->load->view('template/template', $data);
	}

	private function _get_data_feeder($token, $act, $filter)
	{
		$payload = [
			"act"    => $act,
			"token"  => $token,
			"filter" => $filter
		];
		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec)->data;
		return $result;
	}

	public function detail($id_kelas_feeder)
	{
		$filter = "id_kelas_kuliah = '$id_kelas_feeder'";
		$data['id_kelas_feeder'] = $id_kelas_feeder;
		$data['data'] = $this->_get_data_feeder($this->token, 'GetPesertaKelasKuliah', $filter);
		$data['page'] = "feeder_peserta_kelas_v";
		$this->load->view('template/template', $data);
	}

	public function remove()
	{
		extract(PopulateForm());

		foreach ($npm as $key => $value) {
			$filter = "nim = '$value'";
			$id_reg_mhs = $this->_get_data_feeder($this->token, "GetListRiwayatPendidikanMahasiswa", $filter);

			$payload = [
				"act" => "DeletePesertaKelasKuliah",
				"token" => $this->token,
				"key" => [
					"id_kelas_kuliah" => $id_kelas_feeder,
					"id_registrasi_mahasiswa" => $id_reg_mhs[0]->id_registrasi_mahasiswa
				]
			];

			$exec = $this->pddikti->runWS($payload);
			$result = json_decode($exec);

			if ($result->error_code == 0) {
				$log = [
					"activity" => "delete",
					"log_message" => "hapus mahasiswa ".$value." dari kelas perkuliahan di feeder (id_kelas: ".$id_kelas_feeder.")",
					"created_at" => date('Y-m-d H:i:s'),
					"created_by" => $this->username
				];
				$this->db->insert('tbl_log_activity_feeder', $log);
			}

			dd($result); echo '<hr>';
		}
	}

	public function edit($id_kelas)
	{
		$kelas = $this->_get_data_feeder($this->token, "GetDetailKelasKuliah", "id_kelas_kuliah = '$id_kelas'");
		$data['id_kelas'] = $id_kelas;
		$data['kelas'] = $kelas[0]->nama_kelas_kuliah;
		$this->load->view('partial/edit_kelas_v', $data);
	}

	public function update_kelas()
	{
		extract(PopulateForm());

		$detail_kelas = $this->_get_data_feeder($this->token, "GetDetailKelasKuliah", "id_kelas_kuliah = '$id_kelas'");

		$payload = [
			"act" => "UpdateKelasKuliah",
			"token" => $this->token,
			"key" => ["id_kelas_kuliah" => $id_kelas],
			"record" => [
				'id_prodi'              => $detail_kelas[0]->id_prodi,
                'id_semester'           => $this->tahunakademik,
                'id_matkul'             => $detail_kelas[0]->id_matkul,
                'nama_kelas_kuliah'     => $kelas
			]
		];
		dd($payload,2);
		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec);

		if ($result->error_code == 0) {
			$log = [
					"activity" => "update",
					"log_message" => "update data kelas ".$detail_kelas[0]->nama_kelas_kuliah." di feeder (id_kelas: ".$id_kelas.")",
					"created_at" => date('Y-m-d H:i:s'),
					"created_by" => $this->username
				];
				$this->db->insert('tbl_log_activity_feeder', $log);
		}

		dd($result,2);
	}

}

/* End of file Kelas.php */
/* Location: ./application/modules/feeder/controllers/Kelas.php */