<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akm extends CI_Controller {

	private $userid, $usergroup, $token, $id_prodi_feeder;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'),'refresh');
		}

		if (!$this->session->userdata('session_akm')) {
			redirect(base_url('sync_feed/krs'),'refresh');
		}

		$this->load->library('pddikti');
		$this->token = $this->pddikti->get_token();
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
		$this->id_prodi_feeder = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
	}

	public function index()
	{
		$data['akm']  = $this->_get_akm();
		$data['page'] = "feeder_akm_v";
		$this->load->view('template/template', $data);
	}

	private function _get_akm($id_reg_mhs='')
	{
		$angkatan = $this->session->userdata('session_akm')['angkatan'];
		$semester = $this->session->userdata('session_akm')['tahunakademik'];

		if (empty($id_reg_mhs)) {
			$filter = "id_semester = '$semester' and angkatan = '$angkatan' and id_prodi = '$this->id_prodi_feeder' and id_status_mahasiswa = 'A'";
		} else {
			$filter = "id_semester = '$semester' and id_registrasi_mahasiswa = '$id_reg_mhs'";
		}

		$payload = [
			"act"    => "GetAktivitasKuliahMahasiswa",
			"token"  => $this->token,
			"filter" => $filter
		];

		$request = $this->pddikti->runWS($payload);
		$result  = json_decode($request)->data;
		return $result;
	}

	public function load_detail_akm($id_reg_mhs)
	{
		$data['akm'] = $this->_get_akm($id_reg_mhs);
		$this->load->view('partial/akm_modal_isi', $data);
	}

	public function sync_akm()
	{
		$id_reg_mhs    = $this->input->post('id_reg_mhs');
		$sks_smt       = $this->input->post('sks_smt');
		$ips           = $this->input->post('ips');
		$sks_total     = $this->input->post('sks_total');
		$ipk           = $this->input->post('ipk');
		$npm           = $this->input->post('npm');
		$tahunakademik = $this->session->userdata('session_akm')['tahunakademik'];

		$key = [
				"id_registrasi_mahasiswa" => $id_reg_mhs,
				"id_semester" => $tahunakademik
			];

		$record = [
				"id_status_mahasiswa" => "A",
				"ips"                 => $ips,
				"ipk"                 => $ipk,
				"sks_semester"        => $sks_smt,
				"total_sks"           => $sks_total
			];

		$payload = [
			"act"    => "UpdatePerkuliahanMahasiswa",
			"token"  => $this->token,
			"key"    => $key,
			"record" => $record
		];

		$exec   = $this->pddikti->runWS($payload);
		$result = json_decode($exec);

		if ($result['error_code'] != 0) {
			dd('Data gagal disimpan. Kode error: '.$result['error_code'].'. Pesan: '.$result['error_desc'],2);
		}

		$this->_update_akm($npm, $tahunakademik, $sks_smt, $ips, $sks_total, $ipk);
		echo '<script>Data berhasil diperbarui!</script>';
	}

	private function _update_akm($npm, $tahunakademik)
	{
		$data = [
			'THSMSTRAKM' => $tahunakademik,
			'KDPTITRAKM' => '031036',
			'KDJENTRAKM' => 'C',
			'KDPSTTRAKM' => $this->userid,
			'NIMHSTRAKM' => $npm,
			'SKSEMTRAKM' => $sks_smt,
			'NLIPSTRAKM' => $ips,
			'SKSTTTRAKM' => $sks_total,
			'NLIPKTRAKM' => $ipk
		];

		$is_data_exist = $this->_is_akm_exist($npm, $tahunakademik);
		if ($is_data_exist) {
			$this->db->where('NIMHSTRAKM', $npm);
			$this->db->where('THSMSTRAKM', $tahunakademik);
			$this->db->update('tbl_aktifitas_kuliah_mahasiswa', $data);	
		} else {
			$this->db->insert('tbl_aktifitas_kuliah_mahasiswa', $data);
		}
	}

	private function _is_akm_exist($npm, $tahunakademik)
	{
		$check = $this->db->get_where('tbl_aktifitas_kuliah_mahasiswa', ['NIMHSTRAKM' => $npm, 'THSMSTRAKM' => $tahunakademik]);
		if ($check->num_rows() > 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}

/* End of file Akm.php */
/* Location: ./application/modules/feeder/controllers/Akm.php */