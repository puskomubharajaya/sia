<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Smt_pdk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//error_reporting(0);
		$this->load->library('Cfpdf');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(61)->result();
			/*if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}*/
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{
		$data['r'] = $this->db->query('SELECT * from tbl_verifikasi_krs a join tbl_mahasiswa b 
										on a.`npm_mahasiswa`=b.`NIMHSMSMHS` join tbl_jurusan_prodi c
										on c.`kd_prodi`=b.`KDPSTMSMHS` join tbl_fakultas d
										on d.`kd_fakultas`=c.`kd_fakultas` join tbl_sinkronisasi_renkeu e
										on e.`npm_mahasiswa`=a.`npm_mahasiswa`
										where status_verifikasi = 2')->result();

		$data['page'] = 'v_espe';
		$this->load->view('template/template', $data);
	}

	function update_status()
	{
		$a = $this->session->userdata('sess_login');
		$b = $a['userid'];	

		$jum = count($this->input->post('status', TRUE));
		for ($i=0; $i < $jum ; $i++) { 
			$v = explode('eek', $this->input->post('status['.$i.']', TRUE));
			$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$v[1],'NIMHSMSMHS','asc')->row();
			$k = $mhs->KDPSTMSMHS; 
			$t = $mhs->SMAWLMSMHS;
			$o = $mhs->NIMHSMSMHS;

			$y = $this->app_model->get_semester($t);
				$data['briva_mhs'] = '70306'.substr($v[1], 2);
				$data['npm_mahasiswa'] = $v[1];
				if ($v[0] == 'z') {
				 	$data['kode_sink'] = NULL;
				 } else {
				 	$data['kode_sink'] = $v[0];
				 }
				$data['transaksi_terakhir'] = date('Y-m-d');
				$data['userid'] = $b;
				$data['semester'] = $y;
		}

		$q = $this->db->query('SELECT * from tbl_sinkronisasi_renkeu where npm_mahasiswa = "'.$o.'"')->row();

		if (count($q) > 0) {
			$mn = $q->id_sink;
			$l   = $this->app_model->getdetail('tbl_sinkronisasi_renkeu','id_sink',$mn,'id_sink','asc')->row();
			$gb  = $l->npm_mahasiswa;
			$this->app_model->updatedata('tbl_sinkronisasi_renkeu','npm_mahasiswa',$gb,$data);
		} else {
			$this->app_model->insertdata('tbl_sinkronisasi_renkeu', $data);
		}
		
		//redirect('perkuliahan/smt_pdk','refresh');
	}

}

/* End of file Smt_pdk.php */
/* Location: .//tmp/fz3temp-1/Smt_pdk.php */