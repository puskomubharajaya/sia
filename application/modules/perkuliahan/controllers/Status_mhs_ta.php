<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_mhs_ta extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
	}

	public function index()
	{
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = "v_sts_mhs_ta";
		$this->load->view('template/template', $data);
	}

	function sesi()
	{
		$this->session->set_userdata('thn',$this->input->post('ta'));
		redirect(base_url('perkuliahan/status_mhs_ta/detil_sts'));
	}

	function detil_sts()
	{		
		$sesi = $this->session->userdata('sess_login');
		$sess = $sesi['userid'];

		$this->db->select('a.*,b.NMMHSMSMHS');
		$this->db->from('tbl_status_mahasiswa a');
		$this->db->join('tbl_mahasiswa b', 'a.npm = b.NIMHSMSMHS');
		$this->db->where('a.tahunajaran', $this->session->userdata('thn'));
		$this->db->where('b.KDPSTMSMHS', $sess);
		$data['que'] = $this->db->get()->result();

		$data['page'] = "result_sts_mhs";
		$this->load->view('template/template', $data);
	}

}

/* End of file Status_mhs_ta.php */
/* Location: ./application/modules/perkuliahan/controllers/Status_mhs_ta.php */