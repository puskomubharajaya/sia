<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('perkuliahan/jdl_kuliah/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {



            // $('#hargasatuan').html(ui.item.harga_jual);

            // $('#satuan').html(ui.item.satuan);

            // $('#stok').html(ui.item.stok);

            // this.form.kode.value = ui.item.kode;

            this.form.dosen.value = ui.item.value;

            this.form.kd_dosen.value = ui.item.nid;

            // this.form.hargabeli.value = ui.item.harga_beli;

            // this.form.hargajual.value = ui.item.harga_jual;

            //$('#qtyk').focus();

        }

    });

});

</script>

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Form Penugasan Dosen</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/jdl_kuliah/update_dosen" method="post" enctype="multipart/form-data">

                <div class="modal-body" style="margin-left: -60px;">

                    <div class="control-group" id="">

                        <label class="control-label">Nama Dosen</label>

                        <div class="controls">

                            <input type="hidden" name="id_jadwal" value="<?php echo $jadwal->id_jadwal;?>" >

                            <input type="text" id='dosen' name="dosen" >
                            
                            <input type="hidden" id='kd_dosen' name="kd_dosen" >

                        </div>

                    </div>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>