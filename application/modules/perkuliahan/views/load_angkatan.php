<?php // var_dump($nm_dosen);die(); ?>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script>
function edit(id){
$('#absen').load('<?php echo base_url();?>akademik/ajar/view_absen/'+id);
}
</script>

<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Cetak Kartu Ujian</h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">

                    <a href="<?php echo base_url();?>perkuliahan/ujian" class="btn btn-primary "> << Kembali</a><hr>

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 


                                <th>NO</th>

                                <th>Program Studi</th>

                                <th>Angkatan</th>>

                                <th width="80">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($rows as $isi): ?>

                                <tr>
                                    <td><?php echo $no; ?></td>

                                    <td><?php echo $isi->prodi; ?></td>

                                    <td><?php echo $isi->TAHUNMSMHS; ?></td>

                                    <td class="td-actions">

                                        <a href="<?php echo base_url();?>perkuliahan/ujian/cetak_kartu/<?php echo $isi->kd_prodi ?>/<?php echo $isi->TAHUNMSMHS; ?>" target='_blank' class="btn btn-primary btn-small"><i class="btn-icon-only icon-print"></i></a>

                                        <a href="<?php echo base_url();?>perkuliahan/ujian/load_mahasiswa/<?php echo $isi->kd_prodi ?>/<?php echo $isi->TAHUNMSMHS; ?>" class="btn btn-success btn-small"><i class="btn-icon-only icon-ok"></i></a>

                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                            

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="absen">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->