 <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Jadwal Mata Kuliah</h4>
</div>
<form class ='form-horizontal' action="#" method="post">
    <div class="modal-body" style="margin-left:-40px;">
        <div class="control-group" id="">
            <label class="control-label">Dosen</label>
            <div class="controls">
                <input type="text" class="span4" name="" placeholder="" class="form-control" value="" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Hari / Waktu</label>
            <div class="controls">
                <input type="text" class="span1" name="" placeholder="" class="form-control" value="" required/> / <input type="text" class="span2" name="" placeholder="" class="form-control" value="" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Kelas</label>
            <div class="controls">
                <input type="text" class="span2" name="" placeholder="" class="form-control" value="" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Kampus</label>
            <div class="controls">
                <input type="text" class="span2" name="" placeholder="" class="form-control" value="" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Gedung</label>
            <div class="controls">
                <input type="text" class="span2" name="" placeholder="" class="form-control" value="" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Lantai</label>
            <div class="controls">
                <input type="text" class="span2" name="" placeholder="" class="form-control" value="" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Ruang</label>
            <div class="controls">
                <input type="text" class="span2" name="" placeholder="" class="form-control" value="" required/>
            </div>
        </div>
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>