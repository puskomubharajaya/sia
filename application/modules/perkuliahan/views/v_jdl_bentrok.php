

<style type="text/css">
#circle { width: 20px; height: 20px; background: #FFB200; -moz-border-radius: 50px; -webkit-border-radius: 50px; border-radius: 50px; }
</style>




<!--script src="<?php //echo base_url();?>assets/js/jquery-ui/js/jquery-ui.js"></script-->

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.css">
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery.ui.autocomplete.js"></script>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css">

<script>
	function edit(idk){
		$('#edit1').load('<?php echo base_url();?>perkuliahan/jdl_kuliah2/edit_jadwal_new/'+idk);
	}

	function edit2(idk){
		$('#edit2').load('<?php echo base_url();?>perkuliahan/jdl_kuliah/penugasan_new/'+idk);
	}

	function edit3(idk){
		$('#edit3').load('<?php echo base_url();?>perkuliahan/jdl_kuliah/edit_nama_kelas/'+idk);
	}
</script>


<script type="text/javascript">

$(document).ready(function(){

	$('#jam_masuk').timepicker();

	$('#jam_keluar').timepicker();

	$('#jam_masukedit').timepicker();
});



</script>



<script type="text/javascript">

jQuery(document).ready(function($) {

	$('input[name^=dosen]').autocomplete({

        source: '<?php echo base_url('perkuliahan/jdl_kuliah/getdosen');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.nik.value = ui.item.nik;

            this.form.dosen.value = ui.item.value;

        }

    });

});

</script>

<div class="row">

	<div class="span12">      		  		

  		<div class="widget ">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Data Jadwal Bentrok</h3>

			</div> <!-- /widget-header -->

			

			<div class="widget-content" style="padding:30px;">
				<div class="tabbable">

                        
					<div class="tab-content">
              	<?php
            	for ($i = 1; $i < 2; $i++) {
            		
            		if ($i==1) { 
            			echo '<div class="tab-pane active" id="tab'.$i.'">'; ?>
            				<div class="span11">

									<?php
									
									$user = $this->session->userdata('sess_login');
							        $pecah = explode(',', $user['id_user_group']);
							        $jmlh = count($pecah);
							        for ($i=0; $i < $jmlh; $i++) { 
							            $grup[] = $pecah[$i];
							        }?>


								<a href="<?php echo base_url(); ?>perkuliahan/jdl_kuliah2/view_matakuliah" class="btn btn-info "><i class="btn-icon-only icon-chevron-left "> Kembali</i></a>
									<table class="table table-bordered table-striped">

					                	<thead>

					                        <tr> 

					                        	<th>No</th>

												<th>Kelas</th>

												<th>Hari</th>

					                            <th>Kode MataKuliah</th>
					                            
					                            <th>Nama MataKuliah</th>

												<th>Waktu</th>
												
												<th>Dosen</th>
												
												<th>Ruangan</th>

					                        </tr>

					                    </thead>

					                    <tbody>
										
					                    	<?php $no=1; foreach ($jadwal as $isi)  { 
											$prodi=substr($isi->kd_jadwal,0,5);?>
					                    		
					                    	<tr>

												<td><?php echo $no; ?></td>

												<td><?php echo $isi->kelas; ?></td>

												<td><?php echo notohari($isi->hari); ?></td>

												<td><?php echo $isi->kd_matakuliah; ?></td>
												
												<td><?php echo get_nama_mk($isi->kd_matakuliah,$prodi); ?></td>

												<td><?= substr($isi->waktu_mulai,0,5).'-'.substr($isi->waktu_selesai,0,5); ?></td>
					
									
												<td><?php echo nama_dsn($isi->kd_dosen); ?></td>
												
												<td><?php echo get_room($isi->kd_ruangan); ?></td>
													
											</tr>
												<?php $no++; } ?>

												<?php } ?>
												<?php } ?>
					                    </tbody>

					               	</table>

								</div>
                <?php echo '</div>'; ?>
                  
                
				
					</div>
			  
				</div>
			</div>
		</div>
	</div>
			
</div> <!-- /widget-content -->

