<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">FORM EDIT DATA</h4>
</div>

<form class ='form-horizontal' action="<?= base_url();?>perkuliahan/jdl_kuliah_baa/update_data_matakuliah" method="post">

	<div class="modal-body">    
	    
	    <div class="control-group">
			<label class="control-label">Kelompok Kelas</label>
			<div class="controls">
				<input type="hidden" name="id_jadwal" id="id_jadwal" value="<?= $idJadwal ?>">
				<input 
					type="radio" 
					name="st_kelas" 
					value="PG" 
					<?= $rows->waktu_kelas == 'PG' ? 'checked=""' : null; ?>> PAGI 
					&nbsp;&nbsp; 
				<input 
					type="radio" 
					name="st_kelas" 
					value="SR" 
					<?= $rows->waktu_kelas == 'SR' ? 'checked=""' : null; ?>> SORE 
					&nbsp;&nbsp;
				<input 
					type="radio" 
					name="st_kelas" 
					value="PK" 
					<?= $rows->waktu_kelas == 'PK' ? 'checked=""' : null; ?>> P2K  
					&nbsp;&nbsp;
			</div>
		</div>

		<div class="control-group" id="">
	        <label class="control-label">Kelas Gabungan</label>
	        <div class="controls">
	            <select id="gabung1" name="gabung" class="span3" class="form-control">
	                <option value="<?= $rows->gabung; ?>"><?= $rows->gabung > 0 ? "Gabungan" : "Tidak" ?></option>
	                <option>--Pilihan--</option>
	                <option value="1">Gabungan</option>
	                <option value="0">Tidak</option>
	            </select>
	        </div>
	    </div>

	    <div class="control-group" id="">
	        <label class="control-label">Kelas Induk</label>
	        <div class="controls">
	            <select name="kelas_gab" id="kelas_gab1" class="span3" class="form-control" required>
	                <?php
	                $year = $years;
	                if ($rows->gabung > 0) {
	                	$getClass 	= $this->db->query("SELECT 
	                										a.kd_jadwal,
	                										a.kd_matakuliah,
	                										a.kelas,
	                										a.id_jadwal,
	                										c.nama_matakuliah 
	                									FROM tbl_jadwal_matkul a
														JOIN tbl_matakuliah c ON c.kd_matakuliah = a.kd_matakuliah
														WHERE kd_tahunajaran = '".$year."'
														AND a.kd_jadwal = '".$rows->referensi."' 
														ORDER BY id_jadwal asc")->row();
	                } ?>
					<option value='<?= $rows->gabung > 0 ? $getClass->kd_jadwal : 0;?>'>
						<?= $rows->gabung > 0 ? $getClass->kd_matakuliah.' - '.$getClass->nama_matakuliah.' ('.$getClass->kelas.')' : '-';?>
					</option>
					<option value="" disabled="">-- Pilih Kelas Induk --</option>
					<?php foreach ($kelasInduk as $induk) { ?>
						<option value="<?= $induk->kd_jadwal ?>"><?= $induk->kd_matakuliah.' - '.$induk->nama_matakuliah.' ('.$induk->kelas.')' ?></option>	
					<?php } ?>
	            </select>
	        </div>
	    </div>

		<div class="control-group">											
			<label class="control-label" for="firstname">Nama Matakuliah</label>
			<div class="controls">
				<input type="hidden" name="matakuliah" value="<?= $rows->kd_matakuliah.'___'.$rows->id_matakuliah ?>">
				<select class="form-control span3" name="matakuliah_disabled" id="matakuliah_id" required="" disabled="">
					<option value="<?= $rows->kd_matakuliah.'___'.$rows->id_matakuliah ?>"><?= $rows->kd_matakuliah.' - '.$rows->nama_matakuliah; ?></option>
					<option value="" disabled="">-- Pilih MataKuliah --</option>
					<?php foreach ($matkul as $isi) { ?>
	                    <option value="<?= $isi->kd_matakuliah.'___'.$isi->id_matakuliah; ?>">
	                    	<?= $isi->kd_matakuliah; ?> - 
	                    	<?= $isi->nama_matakuliah; ?> 
	                    	(<?= $isi->sks_matakuliah; ?> sks)
	                    </option>
	                <?php } ?>
				</select>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->

		<div class="control-group" id="kelas_group2">											
			<label class="control-label" for="jam">Nama Kelas</label>
			<div class="controls">
				<input 
					type="text" 
					class="form-control span3" 
					placeholder="masukan nama kelas" 
					id="kelas" 
					name="kelas" 
					value="<?= $rows->kelas; ?>"
					required="">
			</div> <!-- /controls -->				
		</div>

		<div class="control-group" id="hari_group2">											
			<label class="control-label" for="firstname">Hari Kuliah</label>
			<div class="controls">
				<select class="form-control span3" name="hari_kuliah" id="hari_kuliah" required="">
					<option value="">-- Hari Kuliah --</option>
					<option value="1" <?= $rows->hari == 1 ? 'selected=""' : NULL; ?>>Senin</option>
					<option value="2" <?= $rows->hari == 2 ? 'selected=""' : NULL; ?>>Selasa</option>
					<option value="3" <?= $rows->hari == 3 ? 'selected=""' : NULL; ?>>Rabu</option>
					<option value="4" <?= $rows->hari == 4 ? 'selected=""' : NULL; ?>>Kamis</option>
					<option value="5" <?= $rows->hari == 5 ? 'selected=""' : NULL; ?>>Jum'at</option>
					<option value="6" <?= $rows->hari == 6 ? 'selected=""' : NULL; ?>>Sabtu</option>
					<option value="7" <?= $rows->hari == 7 ? 'selected=""' : NULL; ?>>Minggu</option>
				</select>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->

		<div class="control-group" id="jam_group2">											
			<label class="control-label" for="jam">Jam Masuk</label>
			<div class="controls">
				<input 
					type="text" 
					class="form-control span3" 
					placeholder="Input Jam Masuk" 
					id="jam_masukedit" 
					name="jam_masuk" 
					value="<?= $rows->waktu_mulai; ?>"
					required="">
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
			
		<div class="control-group" id="gedung_group2">                                         
	        <label class="control-label" for="gedung">Gedung</label>
	        <div class="controls">
	            <select class="form-control span3" name="gedung" id="gedung2" required="">
	                <option value="">--Pilih Gedung--</option>
	                <?php foreach ($gedung as $isi) {?>
	                    <option value="<?= $isi->id_gedung ?>"><?= $isi->gedung ?></option>   
	                <?php } ?>
	            </select>
	        </div> <!-- /controls -->               
	    </div> <!-- /control-group -->

	    <div class="control-group" id="lantai_group2">                                         
	        <label class="control-label" for="lantai">Lantai</label>
	        <div class="controls">
	            <select name="lantai" id="lantai2" required="" class="form-control span3">
	                <option value="">--Pilih Lantai--</option>
	            </select>
	        </div> <!-- /controls -->               
	    </div> <!-- /control-group -->

	    <div class="control-group" id="ruang_group2">                                         
	        <label class="control-label" for="ruangan">Ruangan</label>
	        <div class="controls">
	            <select name="ruangan" class="form-control span3" id="ruangan2" required>
	            	<option value="<?= $rows->id_ruangan ?>"><?= $rows->kode_ruangan.' - '.$rows->ruangan ?></option>
	                <option value="">--Pilih Ruangan--</option>
	            </select>
	        </div> <!-- /controls -->               
	    </div> <!-- /control-group -->
	</div> 

	<div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    <input type="submit" class="btn btn-primary" value="Save changes"/>
	</div>

</form>

<script type="text/javascript">
	$(document).ready(function(){

		$('#jam_masuk').timepicker();
		$('#jam_keluar').timepicker();
		$('#jam_masukedit').timepicker();

		$('#semester').change(function(){
			$.post('<?= base_url();?>perkuliahan/jdl_kuliah_baa/get_detail_matakuliah_by_semester/'+$(this).val(),{},function(get){
				$('#matakuliah_id').html(get);
			});
		});

		$('#matakuliah_id').change(function() {
			$('#kode_mk').val($('#matakuliah_id').val());
		});

		$('#gabung1').change(function(){
            $.post('<?php echo base_url();?>perkuliahan/Jdl_kuliah_baa/get_kelas_gabungan/'+$(this).val()+'/'+$('#matakuliah_id').val()+'/'+$('#id_jadwal').val(),{},function(get){
                $('#kelas_gab1').html(get);
            });
        });

        $('#gedung2').change(function(){
            $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_lantai/'+$(this).val(),{},function(get){
                $('#lantai2').html(get);
            });
        });

        $('#lantai2').change(function(){
            $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_ruangan/'+$(this).val(),{},function(get){
                $('#ruangan2').html(get);
            });
        });

        $(function() {
	        $('#gabung1').change(function(){
	            if ($(this).val() === '1') {
	                $('#kelas').removeAttr('required').val('');
	                $('#hari_kuliah').removeAttr('required').val('');
	                $('#jam_masukedit').removeAttr('required').val('');
	                $('#gedung2').removeAttr('required').val('');
	                $('#lantai2').removeAttr('required').val('');
	                $('#ruangan2').removeAttr('required').val('');

	                $('#kelas_group2').hide();
	                $('#hari_group2').hide();
	                $('#jam_group2').hide();
	                $('#gedung_group2').hide();
	                $('#lantai_group2').hide();
	                $('#ruang_group2').hide();

	            } else {
	                $('#kelas').attr('required','required');
	                $('#hari_kuliah').attr('required','required');
	                $('#jam_masukedit').attr('required','required');
	                $('#gedung2').attr('required','required');
	                $('#lantai2').attr('required','required');
	                $('#ruangan2').attr('required','required');

	                $('#kelas_group2').show();
	                $('#hari_group2').show();
	                $('#jam_group2').show();
	                $('#gedung_group2').show();
	                $('#lantai_group2').show();
	                $('#ruang_group2').show();
	            }
	        });

	        if ($('#gabung1').val() === '1') {
	        	$('#kelas_group2').hide();
                $('#hari_group2').hide();
                $('#jam_group2').hide();
                $('#gedung_group2').hide();
                $('#lantai_group2').hide();
                $('#ruang_group2').hide();

                $('#kelas').removeAttr('required').val('');
                $('#hari_kuliah').removeAttr('required').val('');
                $('#jam_masukedit').removeAttr('required').val('');
                $('#gedung2').removeAttr('required').val('');
                $('#lantai2').removeAttr('required').val('');
                $('#ruangan2').removeAttr('required').val('');
	        }
	    })

	});
</script>