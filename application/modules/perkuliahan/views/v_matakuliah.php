<script type="text/javascript">
	function isNumber(evt) {
	    evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	        return false;
	    }
	    return true;
	}

	$(document).ready(function(){
		var table = $('#tabel_matakuliah');
		
		var oTable = table.dataTable({
			"searching": false
		});
		
		$('#new').click(function(){
			 $("#prasyarat_matakuliah").val([]);
			 $("#konversi_matakuliah").val([]);
			 $('#kd_matakuliah').val('');
			 $('#kd_matakuliah_lama').val('');
			 $('#nama_matakuliah').val('');
			 $('#sks_matakuliah').val('');
		});
		
		table.on('click', '.edit', function(e) {
			 $("#prasyarat_matakuliah").val([]);
			 $("#konversi_matakuliah").val([]);
	         $('#edit_modal').modal('show');
			 var nRow = $(this).parents('tr')[0];
	         var aData = oTable.fnGetData(nRow);
			 $('#kd_matakuliah').val(aData[1]);
			 $('#kd_matakuliah_lama').val(aData[1]);
			 $('#nama_matakuliah').val(aData[2]);
			 $('#en_name').val(aData[3]);
			 $('#sks_matakuliah').val(aData[4]);
			 //$('#semester_matakuliah').val(aData[4]);
			 var p = aData[4];
			 var pc = p.replace('[', '');
			 var pc2 = pc.replace(']', '');

			 var kv = aData[5];
			 var kv1 = kv.replace('[', '');
			 var kv2 = kv1.replace(']', '');
			 
			 $.each(pc2.split(","), function(i,e){
				$("#prasyarat_matakuliah option[value='" + e + "']").prop("selected", true);
				});
			 $.each(kv2.split(","), function(i,e){
				$("#konversi_matakuliah option[value='" + e + "']").prop("selected", true);
				});
			 
		});
	});
</script>

<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM EDIT DATA MATAKULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url();?>perkuliahan/matakuliah/update_data" method="post">
                <div class="modal-body">    
                    <div class="control-group">											
						<label class="control-label" for="firstname">Kode MK</label>
						<div class="controls">
							<input type="text" class="span3" id="kd_matakuliah" name="kd_matakuliah">
							<input type="hidden" id="kd_matakuliah_lama" name="kd_matakuliah_lama">
						</div> <!-- /controls -->				
					</div> <!-- /control-group -->
					<div class="control-group">											
						<label class="control-label" for="firstname">
							Nama Matakuliah <small><i>(Indonesian)</i></small>
						</label>
						<div class="controls">
							<input type="text" class="span3" id="nama_matakuliah" name="nama_matakuliah" placeholder="in Indonesian">
						</div> <!-- /controls -->				
					</div> <!-- /control-group -->
					<div class="control-group">											
						<label class="control-label" for="firstname">
							Nama Matakuliah <small><i>(English)</i></small>
						</label>
						<div class="controls">
							<input type="text" class="span3" name="en_name" id="en_name" placeholder="in English">
						</div> <!-- /controls -->				
					</div>
					<div class="control-group">											
						<label class="control-label" for="firstname">SKS Mata Kuliah</label>
						<div class="controls">
							<input type="text" onkeypress="return isNumber(e)" maxlength="1" class="span3" id="sks_matakuliah" name="sks_matakuliah">
						</div> <!-- /controls -->				
					</div> <!-- /control-group -->
					<div class="control-group">											
						<label class="control-label" for="firstname">SKS Tatap Muka</label>
						<div class="controls">
							<input type="text" onkeypress="return isNumber(e)" maxlength="1" class="span3" name="sks_etatap">
						</div> <!-- /controls -->				
					</div>
					<div class="control-group">											
						<label class="control-label" for="firstname">SKS Praktikum</label>
						<div class="controls">
							<input type="text" onkeypress="return isNumber(e)" maxlength="1" class="span3" name="sks_eprak">
						</div> <!-- /controls -->				
					</div>
					<div class="control-group">											
						<label class="control-label" for="firstname">SKS Praktikum Lapangan</label>
						<div class="controls">
							<input type="text" onkeypress="return isNumber(e)" maxlength="1" class="span3" name="sks_epraklap">
						</div> <!-- /controls -->				
					</div>
					<div class="control-group">											
						<label class="control-label" for="firstname">SKS Simulasi</label>
						<div class="controls">
							<input type="text" onkeypress="return isNumber(e)" maxlength="1" class="span3" name="sks_esim">
						</div> <!-- /controls -->				
					</div>
					<input type="hidden" id="fakultas" value="<?= $this->uri->segment(4); ?>" name="fakultas">
					<input type="hidden" id="jurusan" value="<?= $this->uri->segment(5); ?>" name="jurusan">
					
					<div class="control-group">											
						<label class="control-label" for="firstname">Prasyarat</label>
						<div class="controls">
						<select class="form-control" id="prasyarat_matakuliah" name="prasyarat_matakuliah[]" multiple>
							<?php foreach($matakuliah as $row){ ?>
								<option value="<?= $row->kd_matakuliah; ?>"><?= $row->kd_matakuliah.' - '.$row->nama_matakuliah; ?></option>
							<?php } ?>
						</select>												
						</div> <!-- /controls -->				
					</div> <!-- /control-group -->
					<div class="control-group">											
						<label class="control-label" for="firstname">Konversi</label>
						<div class="controls">
						<select class="form-control" id="konversi_matakuliah" name="konversi_matakuliah[]" multiple>
							<?php foreach($matakuliah as $row){ ?>
								<option value="<?= $row->kd_matakuliah; ?>"><?= $row->kd_matakuliah.' - '.$row->nama_matakuliah; ?></option>
							<?php } ?>
						</select>												
						</div> <!-- /controls -->				
					</div> <!-- /control-group -->
					<div class="control-group">											
						<label class="control-label" for="firstname">Mata Kuliah Wajib</label>
						<div class="controls">
							<input type="checkbox" name="wajib" value="1">											
						</div> <!-- /controls -->				
					</div>
					<div class="control-group">											
						<label class="control-label" for="firstname">Mata Kuliah Tugas Akhir * 
							<br><small style="color: red"><i>*skripsi/magang/KKN/kerja praktek</i></small>
						</label>
						<div class="controls">
							<input type="checkbox" name="tugasakhir" value="1">											
						</div> <!-- /controls -->				
					</div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-book"></i>
  				<h3>Data Matakuliah</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?= base_url(); ?>" class="btn btn-warning"><i class="icon-chevron-left"></i> Kembali </a>
					<?php if ($aktif->status != 0 && $crud->edit > 0) { ?>
						<a data-toggle="modal" id="new" href="#myModal" class="btn btn-primary"><i class="icon-plus"></i> New Data </a><br>
					<?php } elseif ($aktif->status == 0) { ?>
						<input type="hidden">
					<?php } ?>
					<hr><br>
					<table id="tabel_matakuliah" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                            <th>Kode MK</th>
	                            <th>Matakuliah</th>
	                            <th>Matakuliah (English)</th>
								<th>SKS</th>
								<th>SKS Tatap Muka</th>
								<th>Prasyarat</th>
								<th>Konversi</th>
								<th>Fakultas</th>
								<th>Prodi</th>
	                            <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($matakuliah as $row){?>
	                        <tr>
	                        	<td><?= $no;?></td>
	                        	<td><?= $row->kd_matakuliah;?></td>
	                        	<td><?= $row->nama_matakuliah;?></td>
	                        	<td><?= $row->en_name;?></td>
								<td><?= $row->sks_matakuliah;?></td>
								<td><?= $row->sks_tatapmuka;?></td>
								<td><?= $row->prasyarat_matakuliah;?></td>
								<td><?= $row->konversi_matakuliah;?></td>
								<td><?= $row->fakultas;?></td>
								<td><?= $row->prodi;?></td>
	                        	<td class="td-actions">
									<?php if (($aktif->status != 0) && ($crud->edit > 0)) { ?>
									<button type="button" class="btn btn-primary btn-small edit">
										<i class="btn-icon-only icon-pencil"> </i>
									</button>
									<?php } else { echo "-";}?>
									<a 
										onclick="return confirm('Apakah Anda Yakin?');" 
										class="btn btn-danger btn-small" 
										href="<?= base_url('perkuliahan/matakuliah/delete_data/'.$row->id_matakuliah);?>">
										<i class="btn-icon-only icon-remove"> </i>
									</a>
								</td>
	                        </tr>
							<?php $no++;} ?>
	                      
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM TAMBAH DATA MATAKULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url();?>perkuliahan/matakuliah/add_new" method="post">
                <div class="modal-body">    
                    <div class="control-group">											
							<label class="control-label" for="firstname">Kode MK</label>
							<div class="controls">
								<input type="text" class="span3" name="kd_matakuliah">
							</div> <!-- /controls -->				
						</div> <!-- /control-group -->
						<div class="control-group">											
							<label class="control-label" for="firstname">
								Nama Matakuliah <small><i>(Indonesian)</i></small>
							</label>
							<div class="controls">
								<input type="text" class="span3" name="nama_matakuliah" placeholder="in Indonesian">
							</div> <!-- /controls -->				
						</div> <!-- /control-group -->
						<div class="control-group">											
							<label class="control-label" for="firstname">
								Nama Matakuliah <small><i>(English)</i></small>
							</label>
							<div class="controls">
								<input type="text" class="span3" name="en_name" placeholder="in English">
							</div> <!-- /controls -->				
						</div> <!-- /control-group -->
						<div class="control-group">											
							<label class="control-label" for="firstname">SKS Mata Kuliah</label>
							<div class="controls">
								<input type="text" class="span3" onkeypress="return isNumber(event)" maxlength="1" name="sks_matakuliah">
							</div> <!-- /controls -->				
						</div> <!-- /control-group -->
						<div class="control-group">											
							<label class="control-label" for="firstname">SKS Tatap Muka</label>
							<div class="controls">
								<input type="text" class="span3" onkeypress="return isNumber(event)" maxlength="1" name="sks_tatap">
							</div> <!-- /controls -->				
						</div>
						<div class="control-group">											
							<label class="control-label" for="firstname">SKS Praktikum</label>
							<div class="controls">
								<input type="text" class="span3" onkeypress="return isNumber(event)" maxlength="1" name="sks_prak">
							</div> <!-- /controls -->				
						</div>
						<div class="control-group">											
							<label class="control-label" for="firstname">SKS Praktikum Lapangan</label>
							<div class="controls">
								<input type="text" class="span3" onkeypress="return isNumber(event)" maxlength="1" name="sks_praklap">
							</div> <!-- /controls -->				
						</div>
						<div class="control-group">											
							<label class="control-label" for="firstname">SKS Simulasi</label>
							<div class="controls">
								<input type="text" class="span3" onkeypress="return isNumber(event)" maxlength="1" name="sks_sim">
							</div> <!-- /controls -->				
						</div>
						<input type="hidden" id="fakultas" value="<?= $this->uri->segment(4); ?>" name="fakultas">
						<input type="hidden" id="jurusan" value="<?= $this->uri->segment(5); ?>" name="jurusan">
					
					<div class="control-group">											
						<label class="control-label" for="firstname">Prasyarat</label>
						<div class="controls">
							<select class="form-control span3" name="prasyarat_matakuliah[]" multiple>
								<?php foreach($matakuliah as $row){ ?>
									<option value="<?= $row->kd_matakuliah; ?>">
										<?= $row->kd_matakuliah.' - '.$row->nama_matakuliah; ?>
									</option>
								<?php } ?>
							</select>												
						</div> <!-- /controls -->				
					</div> <!-- /control-group -->
					<div class="control-group">											
						<label class="control-label" for="firstname">Konversi</label>
						<div class="controls">
							<select class="form-control span3" name="konversi_matakuliah">
								<?php foreach($matakuliah as $row){ ?>
									<option value="<?= $row->kd_matakuliah; ?>">
										<?= $row->kd_matakuliah.' - '.$row->nama_matakuliah; ?>
									</option>
								<?php } ?>
							</select>												
						</div> <!-- /controls -->				
					</div> <!-- /control-group -->
					<div class="control-group">											
						<label class="control-label" for="firstname">Mata Kuliah Wajib</label>
						<div class="controls">
							<input type="checkbox" name="wajib" value="1">											
						</div> <!-- /controls -->				
					</div>
					<div class="control-group">											
						<label class="control-label" for="firstname">Mata Kuliah Tugas Akhir * 
							<br><small style="color: red"><i>*skripsi/magang/KKN/kerja praktek</i></small>
						</label>
						<div class="controls">
							<input type="checkbox" name="tugasakhir" value="1">											
						</div> <!-- /controls -->				
					</div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
