<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM EDIT DATA JADWAL UJIAN</h4>
            </div>
            <form class ='form-horizontal' action="#" method="post">
                <input type="hidden" name="jurusan" value=""/>
                <input type="hidden" name="angkatan" value=""/>
                <input type="hidden" name="kelas" value=""/>
                <div class="modal-body" style="margin-left: -30px;">  
                    <div class="control-group" id="">
                        <label class="control-label">Mata Kuliah</label>
                        <div class="controls">
                            <select name="" class="span4 form-control" required></select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">SKS</label>
                        <div class="controls">
                            <input type="text" name="" class="span2 form-control" value="" readonly required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Dosen</label>
                        <div class="controls">
                            <select name="" class="span4 form-control" required></select>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Hari/Tanggal</label>
                        <div class="controls">
                            <input type="text" name="" class="span1 form-control" value="" required/> / <input type="text" name="" class="span2 form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Waktu</label>
                        <div class="controls">
                            <input type="text" name="" class="span2 form-control" value="" required/> / <input type="text" name="" class="span2 form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Ruang</label>
                        <div class="controls">
                            <input type="text" name="" class="span2 form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Pengawas</label>
                        <div class="controls">
                            <select name="" class="span4 form-control" required></select>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>