
<script>
	function autocomp(ids) {
		var formName = ids+'_mk';
		$('#'+ids).autocomplete({
	        source: '<?= base_url('perkuliahan/konversi_matakuliah/autocomplete') ?>',
	        minLength: 1,
	        select: function (evt, ui) {
	        	this.form.formName.value = ui.item.value;
	        }
	    })
	}

	function edit(id){
		$.post('<?= base_url();?>perkuliahan/konversi_matkul/load_data/'+id, function(res){
			var valuemk = JSON.parse(res);
			$('#last').val(valuemk.kd_lama);
			$('#new').val(valuemk.kd_baru);
			$('#idkonv').val(valuemk.id);
			$('#btnsbm').val('Update');
		});
	}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-calendar"></i>
  				<h3>Daftar Mata Kuliah Konversi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<button class="btn btn-primary" data-target="#addmodal" data-toggle="modal">
						<i class="icon icon-plus"></i> Tambah Data
					</button>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Mata Kuliah Lama</th>
                                <th>Mata Kuliah Baru</th>
	                            <th width="110">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1;foreach ($listmk as $value) { ?>
	                        <tr>
	                        	<td><?= $no ?></td>
	                        	<td><?= $value->kd_lama.' - '.get_nama_mk($value->kd_lama,$user); ?></td>
	                        	<td><?= $value->kd_baru.' - '.get_nama_mk($value->kd_baru,$user); ?></td>
	                        	<td class="td-actions">
									<a onclick="edit(<?= $value->id ?>)" class="btn btn-success" href="#addmodal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
									<a href="<?= base_url('perkuliahan/konversi_matkul/remove_data/'.$value->id) ?>" title="hapus data" class="btn btn-danger"><i class="icon icon-remove"></i></a>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addmodal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Mata Kuliah Konversi</h4>
            </div>
            <form class ='form-horizontal' action="<?= base_url('perkuliahan/konversi_matakuliah/add_konv') ?>" method="post">
                <div class="modal-body" style="margin-left:-20px;">
                    <div class="control-group" id="">
                        <label class="control-label">Mata Kuliah Lama</label>
                        <div class="controls">
                            <input type="text" 
                            		class="span3" 
                            		name="last_mk" 
                            		class="form-control" 
                            		value="" 
                            		id="last" 
                            		onkeyup="autocomp('last')" 
                            		required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Mata Kuliah baru</label>
                        <div class="controls">
                            <input type="text" 
                            		class="span3" 
                            		name="new_mk" 
                            		class="form-control" 
                            		value="" 
                            		id="new" 
                            		onkeyup="autocomp('new')" 
                            		required/>
                        </div>
                        <input type="hidden" value="" name="idkonv" id="idkonv">
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" id="btnsbm" name="btnsbm" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->