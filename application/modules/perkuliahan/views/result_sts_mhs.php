<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Status Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a href="<?php echo base_url(); ?>perkuliahan/status_mhs_ta" class="btn btn-warning"> << Kembali </a>
                    <!-- <a data-toggle="modal" class="btn btn-success" href="#myModal"><i class="btn-icon-only icon-plus"> </i> Tambah Data</a> -->
                    <hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NPM</th>
                                <th>Nama</th>
                                <th>Status</th>
                                <th>Tahun Akademik</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($que as $value) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
                                <td><?php echo $value->npm; ?></td>
                                <td><?php echo $value->NMMHSMSMHS; ?></td>
                                <?php if ($value->status == 'C') {
                                    $st = 'CUTI';
                                } else {
                                    $st = 'NON AKTIF';
                                }
                                 ?>
                                <td><?php echo $st; ?></td>
                                <td><?php echo $value->tahunajaran; ?></td>
                                <!-- <td class="td-actions">
                                    <a onclick="edit(1)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="#"><i class="btn-icon-only icon-remove"> </i></a>
                                </td> -->
                            </tr>
                            <?php $no++;} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>