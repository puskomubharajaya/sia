<script type="text/javascript">
    function edit(edk) {
        $("#edit_kalen").load('<?php echo base_url()?>perkuliahan/Kalender/get_list_kal/'+edk);
    }
</script>

<script type="text/javascript">
    function look(ddk) {
        $("#desk_keg").load('<?php echo base_url()?>perkuliahan/Kalender/get_desk/'+ddk);
    }
</script>

<script type="text/javascript" src="<?php echo base_url();?>assets/jquerymasked/src/jquery.maskedinput.js"></script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-calendar"></i>
  				<h3>Kalender Akademik</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <a data-toggle="modal" href="#myModal" class="btn btn-primary"> Tambah Data </a><br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Judul Kegiatan</th>
                                <th>Deskripsi</th>
                                <th>Mulai</th>
                                <th>Akhir</th>
                                <th>Peserta</th>
	                            <th width="120">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($kale->result() as $row) { ?>
	                        <tr>
                                <td><?php echo $no;?></td>
	                        	<td><?php echo $row->jns_kegiatan;?></td>
	                        	<td><?php echo $row->deskripsi;?></td>
	                        	<td><?php echo $row->mulai;?></td>
	                        	<td><?php echo $row->akhir;?></td>
	                        	<td><?php echo $row->peserta;?></td>
	                        	<td class="td-actions">
	                        		<a onclick="look(<?php echo $row->id_kalender;?>)" class="btn btn-success btn-small" href="#deskModal" data-toggle="modal"><i class="btn-icon-only icon-ok"> </i></a>
									<a onclick="edit(<?php echo $row->id_kalender;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
									<a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url();?>perkuliahan/Kalender/delete/<?php echo $row->id_kalender;?>"><i class="btn-icon-only icon-remove"> </i></a>
								</td>
	                        </tr>
                            <?php $no++; } ?>
							
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Kegiatan</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url('perkuliahan/Kalender/tambah');?>" method="post">
                <div class="modal-body" style="margin-left:-20px;">    
                    <div class="control-group" id="">
                        <label class="control-label">Jenis Kegiatan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="jk_keg" placeholder="input jenis kegiatan" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Deskripsi</label>
                        <div class="controls">
                        	<textarea class="span4 form-control" name="desk"></textarea>
                            <!-- <input type="text" class="span4" name="deskripsi_keg" placeholder="Input Nama Fakultas" class="form-control" value="" required/> -->
                        </div>
                    </div>
                          <script>
                              $(function() {
                                $( "#from" ).datepicker({
                                  changeMonth: true,
                                  numberOfMonths: 2,
                                  onClose: function( selectedDate ) {
                                    $( "#to" ).datepicker( "option", "minDate", selectedDate );
                                  }
                                });
                                $( "#to" ).datepicker({
                                  changeMonth: true,
                                  numberOfMonths: 2,
                                  onClose: function( selectedDate ) {
                                    $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                                  }
                                });
                              });
                            </script>
                    <div class="control-group" id="">
                        <label class="control-label">Mulai</label>
                        <div class="controls">
                            <input type="text" class="span4" name="mulai_keg" id="from" placeholder="dd/mm/yyyy" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Akhir</label>
                        <div class="controls">
                            <input type="text" class="span4" name="akhir_keg" id="to" placeholder="dd/mm/yyyy" class="form-control" value="" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Peserta</label>
                        <div class="controls">
                            <input type="text" class="span4" name="peserta" placeholder="peserta" class="form-control" value="" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_kalen">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="deskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="desk_keg">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>