<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Detil Data Kegiatan</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url('perkuliahan/Kalender/tambah');?>" method="post">
                <div class="modal-body" style="margin-left:-20px;">    
                    <div class="control-group" id="">
                        <label class="control-label">Jenis Kegiatan</label>
                        <div class="controls">
                            <input type="text" class="span4" name="jk_keg" class="form-control" value="<?php echo $list->jns_kegiatan;?>" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Deskripsi</label>
                        <div class="controls">
                            <textarea class="span4 form-control" name="desk" disabled="disabled"><?php echo $list->deskripsi;?></textarea>
                            <!-- <input type="text" class="span4" name="deskripsi_keg" placeholder="Input Nama Fakultas" class="form-control" value="" required/> -->
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Mulai</label>
                        <div class="controls">
                            <input type="text" class="span4" name="mulai_keg" id="tgl_awal" class="form-control" value="<?php echo $list->mulai;?>" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Akhir</label>
                        <div class="controls">
                            <input type="text" class="span4" name="akhir_keg" id="tgl_akhir" class="form-control" value="<?php echo $list->akhir;?>" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Peserta</label>
                        <div class="controls">
                            <input type="text" class="span4" name="peserta" placeholder="peserta" class="form-control" value="<?php echo $list->peserta;?>" disabled="disabled"/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    
                </div>
            </form>