    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

        <h4 class="modal-title">Edit Nama Kelas</h4>

    </div>

    <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/jdl_kuliah/save_nama_kelas" method="post">

        <div class="modal-body" style="margin-left: -60px;">

            <div class="control-group" id="">

                <label class="control-label">Nama Kelas</label>

                <div class="controls">

                    <input type="hidden" name="id_jadwal" value="<?php echo $id_jadwal;?>" >

                    <input type="text" name="kelas" value="<?php echo $jadwal->kelas;?>" required>

                </div>
            </div>
            <!-- <script type="text/javascript">

                $(document).ready(function(){

                    $('#gedung1').change(function(){

                        $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_lantai/'+$(this).val(),{},function(get){

                            $('#lantai1').html(get);

                        });

                    });

                });

            </script>

            <div class="control-group">                                         

                <label class="control-label" for="gedung">Gedung</label>

                <div class="controls">

                    <select class="form-control" name="gedung" id="gedung1">

                        <option value="">--Pilih Gedung--</option>

                        <?php foreach ($gedung as $isi) {?>

                            <option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>   

                        <?php } ?>

                    </select>

                </div> <!-- /controls             

            </div> 

            <script type="text/javascript">

                $(document).ready(function(){

                    $('#lantai1').change(function(){

                        $.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_ruangan/'+$(this).val(),{},function(get){

                            $('#ruangan1').html(get);

                        });

                    });

                });

            </script>

            <div class="control-group">                                         

                <label class="control-label" for="lantai">Lantai</label>

                <div class="controls">

                    <select name="lantai" id="lantai1">

                        <option value="">--Pilih Lantai--</option>

                    </select>

                </div> <!-- /controls             

            </div> 

            

            <div class="control-group">                                         

                <label class="control-label" for="ruangan">Ruangan</label>

                <div class="controls">

                    <select name="ruangan" id="ruangan1" required>

                        <option value="">--Pilih Ruangan--</option>

                    </select>

                </div> <!-- /controls -->               

            </div> <!-- /control-group -->

        </div>

        </div> 

        <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

            <input type="submit" class="btn btn-primary" value="Simpan"/>

        </div>

    </form>