<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var table = $('.table');
	
	var oTable = table.dataTable({
	});
});
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kurikulum</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <?php 
                        $logg = $this->session->userdata('sess_login');
                        if ($logg['userid'] == 'LPM') {
                             
                         } else { ?>
                            <a href="<?php echo base_url(); ?>perkuliahan/kurikulum" class="btn btn-warning"> << Kembali </a>
                            <?php if ($crud->create > 0) { ?>
                            <a data-toggle="modal" href="#edit_modal" class="btn btn-primary"> New Data </a>
                            <?php } ?>
                    <?php } ?>
					
					<center><h3>Kurikulum <?php echo $kurikulum; ?> </h3></center><br>
					<?php $tsksw = 0; $tsksp = 0; for($i=1; $i<=8; $i++){ ?>
					<h3>Semester <?php echo $i; ?></h3>
					<hr>
					<table class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode Mata Kuliah</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>
                                <th>Semester</th>
                                <th>Prasyarat</th>
                                <th>Wajib</th>

                                <?php
                                if ($logg['userid'] == 'LPM') {
                                     
                                 } else { ?>
                                     <th width="40">Aksi</th>
                                <?php   } ?>
	                            
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach (${'data_table'.$i} as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->semester_kd_matakuliah; ?></td>
                                <?php if ($row->prasyarat_matakuliah == '[]') {
                                	$pera = '';
                                } else {
                                	$pera = $row->prasyarat_matakuliah;
                                }
                                 ?>
                                <td><?php echo $pera; ?></td>
                                <?php if ($row->status_wajib == 1) { $tsksw = $tsksw+$row->sks_matakuliah; ?>
                                    <td><i class="icon-ok"></i></td>
                                <?php } else { $tsksp = $tsksp+$row->sks_matakuliah;?>
                                    <td><u><i><b>PILIHAN</b></i></u></td>
                                <?php } ?>
                                <?php 
                                if ($logg['userid'] == 'LPM') {
                                     
                                 } else { ?>
                                     <td class="td-actions">
                                         <?php if ($crud->delete > 0) { ?>
                                        <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url().'perkuliahan/kurikulum/delte_matakuliah_kurikulum/'.$row->id_kurmat.'/'.$row->kd_kurikulum; ?>"><i class="btn-icon-only icon-remove"> </i></a>
                                        <?php } else {echo "-";} ?>
                                </td>
                                <?php  } ?>
	                        	
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
					<?php } ?>
                    <u><center><h3>Total SKS Wajib : <?php echo $tsksw; ?>. Total SKS Pilihan : <?php echo $tsksp; ?></h3></center></u>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">TAMBAH MATAKULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/kurikulum/save_matakuliah" method="post">
                <div class="modal-body" style="margin-left: -30px;">  
					<input type="hidden" value="<?php echo $this->uri->segment(4); ?>" name="kd_kurikulum" class="span4" />
                    <div class="control-group" id="">
                        <label class="control-label">Matakuliah</label>
                        <div class="controls">
                            <select class="form-control span4" name="matakuliah" id="matakuliah" required>
                                    <option value="">--Pilih matakuliah--</option>
                                    <?php foreach ($data_matakuliah as $row) { ?>
                                    <option value="<?php echo $row->kd_matakuliah; ?>"><?php echo $row->kd_matakuliah; ?> - <?php echo $row->nama_matakuliah;?></option>
                                    <?php } ?>
                                  </select>
                        </div>
                    </div>
                    <div class="control-group">                                         
                        <label class="control-label" for="firstname">Semester</label>
                        <div class="controls">
                            <input type="number" min="1" max="8" id="semester_matakuliah" name="semester_matakuliah" required/>
                        </div> <!-- /controls -->               
                    </div>
                    <div class="control-group">                                         
                        <label class="control-label" for="firstname">Wajib</label>
                        <div class="controls">
                            <input type="checkbox" value="1" name="status_wajib"/>
                        </div> <!-- /controls -->               
                    </div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->