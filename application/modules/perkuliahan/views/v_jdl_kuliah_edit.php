<script type="text/javascript">

$(document).ready(function(){

	$('#jam_masuk').timepicker();

	$('#jam_keluar').timepicker();

	$('#jam_masukedit').timepicker();	

});



</script>

			<div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">FORM EDIT DATA</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>perkuliahan/jdl_kuliah/update_data_matakuliah" method="post">

                <div class="modal-body">    

                    <div class="control-group">

                                      <label class="control-label">Jurusan</label>

                                      <div class="controls">

                                        <input type='hidden' class="form-control" name="jurusan" value="<?php echo $this->session->userdata('id_jurusan_prasyarat'); ?>" readonly >
                                        <input type='hidden' class="form-control" name="id_jadwal"  value="<?php echo $rows->id_jadwal; ?>">

                                        <input type='text' class="form-control"  value="<?php echo $this->session->userdata('nama_jurusan_prasyarat'); ?>" readonly>

                                      </div>

                                    </div>

                                    <script type="text/javascript">

                                    	$(document).ready(function(){

											$('#semester1').change(function(){

												$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_detail_matakuliah_by_semester/'+$(this).val(),{},function(get){

													$('#ala_ala1').html(get);

												});

											});

										});

                                    </script>

                                    <!-- <div class="control-group">

                                      <label class="control-label">Tahun Akademik</label>

                                      <div class="controls">

                                        <select class="form-control" name="thn_akd" required="">

                                          <option>--Pilih Tahun Akademik--</option>

                                          <option value="20151" <?php // if ($rows->kd_tahunajaran == '20151') {echo 'selected=""';} ?>>20151 - Ganjil</option>

                                          <option value="20152" <?php // if ($rows->kd_tahunajaran == '20152') {echo 'selected=""';} ?> >20152 - Genap</option>


                                          <option value="20161" <?php // if ($rows->kd_tahunajaran == '20161') {echo 'selected=""';} ?>>20161 - Ganjil</option>

                                          <option value="20162" <?php // if ($rows->kd_tahunajaran == '20162') {echo 'selected=""';} ?> >20162 - Genap</option>

                                        </select>

                                      </div>

                                    </div> -->

                                    <div class="control-group">

                                      <label class="control-label">Semester</label>

                                      <div class="controls">

                                        <select id="semester1" class="form-control" name="semester" >
                                          <option value="<?php echo $rows->semester_matakuliah; ?>"><?php echo $rows->semester_matakuliah; ?></option>

                                          <option>--Pilih Semester--</option>

                                          <option value="1">1</option>

                                          <option value="2">2</option>

                                          <option value="3">3</option>

                                          <option value="4">4</option>

                                          <option value="5">5</option>

                                          <option value="6">6</option>

                                          <option value="7">7</option>

                                          <option value="8">8</option>

                                        </select>

                                      </div>

                                    </div>

                                       <div class="control-group">
										<label class="control-label">Kelompok Kelas</label>
										<div class="controls">
											<input type="radio" name="st_kelas" value="PG" <?php if ($rows->waktu_kelas == 'PG') {echo 'checked=""';} ?> > A &nbsp;&nbsp; 
											<input type="radio" name="st_kelas" value="SR" <?php if ($rows->waktu_kelas == 'SR') {echo 'checked=""';} ?>> B &nbsp;&nbsp;
											<input type="radio" name="st_kelas" value="PK" <?php if ($rows->waktu_kelas == 'PK') {echo 'checked=""';} ?>> C  &nbsp;&nbsp;
										</div>
									</div>

                                    	<div class="control-group">											

											<label class="control-label" for="kelas">Nama Kelas</label>

											<div class="controls">

												<input type="text" class="form-control" placeholder="Input Kelas" id="kelas" name="kelas" value="<?php echo $rows->kelas ?>" >

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<script type="text/javascript">

										$(document).ready(function() {

											$('#ala_ala1').change(function() {

												$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_kode_mk/'+$(this).val(),{},function(get){

													$('#kode_mk1').val(get);

												});

											});

										});

										</script>



										<div class="control-group">											

											<label class="control-label" for="firstname">Hari Kuliah</label>

											<div class="controls">

												<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

												<select class="form-control" name="hari_kuliah" id="hari_kuliah" required>
													<option value="<?php echo $rows->hari ?>"><?php echo notohari($rows->hari); ?></option>
													<option disabled>--Hari Kuliah--</option>

													<option value="1">Senin</option>

													<option value="2">Selasa</option>

													<option value="3">Rabu</option>

													<option value="4">Kamis</option>

													<option value="5">Jum'at</option>

													<option value="6">Sabtu</option>

													<option value="7">Minggu</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->	

                                    

                						<div class="control-group">											

											<label class="control-label" for="firstname">Nama Matakuliah</label>

											<div class="controls">

												<!-- <input type="text" class="span2" name="nama_matakuliah"> -->

												<select class="form-control" name="nama_matakuliah" id="ala_ala1" >
													<option value="<?php echo $rows->id_matakuliah ?>"><?php echo $rows->nama_matakuliah; ?> </option>
													<option>--Pilih MataKuliah--</option>
												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->    

                    					

                    					<div class="control-group">										

											<label class="control-label" for="kode_mk">Kode MK</label>

											<div class="controls">

												<input type="text" class="span2" name="kd_matakuliah" placeholder="Kode Matakuliah" id="kode_mk1" value="<?php echo $rows->kd_matakuliah; ?>" readonly>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<script type="text/javascript">

	                                    	$(document).ready(function(){

												$('#gedung').change(function(){

													$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_lantai/'+$(this).val(),{},function(get){

														$('#lantai').html(get);

													});

												});

											});

                                    	</script>

										<div class="control-group">											

											<label class="control-label" for="gedung">Gedung</label>

											<div class="controls">

												<select class="form-control" name="gedung" id="gedung">
													
													<option value="">--Pilih Gedung--</option>

													<?php foreach ($gedung as $isi) {?>

														<option value="<?php echo $isi->id_gedung ?>"><?php echo $isi->gedung ?></option>	

													<?php } ?>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<script type="text/javascript">

	                                    	$(document).ready(function(){

												$('#lantai').change(function(){

													$.post('<?php echo base_url();?>perkuliahan/jdl_kuliah/get_ruangan/'+$(this).val(),{},function(get){

														$('#ruangan').html(get);

													});

												});

											});

                                    	</script>

										<div class="control-group">											

											<label class="control-label" for="lantai">Lantai</label>

											<div class="controls">

												<select name="lantai" id="lantai">
													
													<option value="">--Pilih Lantai--</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ruangan">Ruangan</label>

											<div class="controls">

												<select name="ruangan" id="ruangan" required>
													<option value="<?php echo $rows->id_ruangan ?>"><?php echo $rows->kd_ruangan ?></option>
													<option>--Pilih Ruangan--</option>

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<!-- <div class="control-group">											

											<label class="control-label" for="dosen">Dosen</label>

											<div class="controls">

												<input type="hidden" name="id_kary" id="id_kary">

												<input type="text" name="dosen" id="dosen">

											</div> 				

										</div>  -->



									

										<!-- <div class="control-group">											

											<label class="control-label" for="firstname">Perasyarat</label>

											<div class="controls">

											<select class="form-control" name="prasyarat_matakuliah[]" multiple>

												<?php //foreach($matakuliah as $row){ ?>

													<option value="<?php //echo $row->kd_matakuliah; ?>"><?php //echo $row->nama_matakuliah; ?></option>

												<?php //} ?>

											</select>												

											</div>				

										</div> -->

										<div class="control-group">											

											<label class="control-label" for="jam">Jam Masuk</label>

											<div class="controls">

												<input type="text" class="form-control" placeholder="Input Jam Masuk" id="jam_masukedit" name="jam_masuk" value="<?php echo $rows->waktu_mulai; ?>">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										<!-- <div class="control-group">											

											<label class="control-label" for="jam">Jam Keluar</label>

											<div class="controls">

												<input type="text" class="form-control" placeholder="Input Jam keluar" id="jam_keluar" name="jam_keluar">

											</div> 

										</div> -->

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save changes"/>

                </div>

            </form>