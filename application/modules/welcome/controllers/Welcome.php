<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Welcome extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

	public function index()

	{
		//$this->load->view('welcome_message');
		// echo $this->benchmark->memory_usage();
		// 
		$str_encrypted = "$2y$08$VCKwI5FKFHfIIEplIQg2buzvTtMJ55mYL.IOUX.FYY0Z9PcCVIxa.=";
		// $str_encrypted = "J0FxiGpok4Xw3UjVxoaM9l74BlEpDPJnKg9Q0qVbors=";
		echo $this->widienc($str_encrypted);

	}



	function widienc($text, $kata_dasar = 'tahu tahu tahu'){
		$text_encrypted = $this->md5_decrypt($text,$kata_dasar);
		return $text_encrypted;
	}

	function md5_decrypt($enc_text, $password, $iv_len = 16)
	{
		$enc_text = str_replace('@', '+', $enc_text);
		$enc_text = str_replace('_', '/', $enc_text);
		$enc_text = base64_decode($enc_text);
		$n = strlen($enc_text);
		$i = $iv_len;
		$plain_text = '';
		$iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
		while ($i < $n) {
			$block = substr($enc_text, $i, 16);
			$plain_text .= $block ^ pack('H*', md5($iv));
			$iv = substr($block . $iv, 0, 512) ^ $password;
			$i += 16;
		}
		return preg_replace('/\\x13\\x00*$/', '', $plain_text);
	}

	function nyetak()
	{
		$data['q'] = $this->app_model->getdata('tbl_jurusan_prodi','kd_prodi','asc')->result();
		$this->load->view('nyetak',$data);
	}

	function nyetak_banyak()
	{
		$this->load->library('Cfpdf');
		$this->load->model('temph_model');

		$id = $this->input->post('jur');
		$day = $this->input->post('hari');

		$data['titles'] = $this->db->query('SELECT * FROM tbl_jurusan_prodi b
											JOIN tbl_fakultas c ON b.`kd_fakultas` = c.`kd_fakultas`
											WHERE b.`kd_prodi` = "'.$id.'"')->row();

		$data['jadwal'] = $this->db->query("SELECT jdl.*,mk.`nama_matakuliah`,mk.semester_matakuliah,mk.`sks_matakuliah`,r.`kode_ruangan`,r.`kuota` FROM tbl_jadwal_matkul jdl
											JOIN tbl_matakuliah mk ON jdl.`kd_matakuliah` = mk.`kd_matakuliah`
											LEFT JOIN tbl_ruangan r ON jdl.`kd_ruangan` = r.`id_ruangan`
											WHERE jdl.`kd_jadwal` LIKE '".$id."%' AND jdl.`kd_tahunajaran` = '20172' AND mk.`kd_prodi` = '".$id."' and jdl.hari = ".$day."
											AND jdl.`kd_jadwal` IN (SELECT kd_jadwal FROM tbl_krs WHERE kd_krs LIKE CONCAT(npm_mahasiswa,'20172%'))
											-- limit 1
											")->result();
		
		$this->load->view('akademik/cetak_absensi_gelondongan',$data);
		
	}


	function semestermhs()

	{

		//define

		$nim = '201320152010';

		$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();

		echo "Nama : ".$mhs->NMMHSMSMHS."<br/>";

		echo "NPM : ".$mhs->NIMHSMSMHS."<br/>";

		echo "Angkatan : ".$mhs->TAHUNMSMHS."<br/>";

		$this->db->order_by('kode', 'desc');

		$this->db->where('status', '1');

		$tahunakademik = $this->db->get('tbl_tahunakademik', 1)->row();

		$selisih = ($tahunakademik->kode) - ($mhs->SMAWLMSMHS);

		$semawal = (substr($selisih, 0,1)) * 2;

		$semtambah = substr($selisih, 1,2) + 1;

		$semester = $semawal + $semtambah;

		//return $semester;
		$semester1 = $this->app_model->get_semester_khs('20121','20151');



		echo "Semester Mahasiswa saat ini : ".$semester1."<br/>";



	}



	function kodekrs()

	{
		//$this->benchmark->mark('code_start');

		// Some code happens here


		$npm = '201420251001';

		//echo substr($npm, 4,1);
		//exit();

		$ta = '20152';

		$kodeawal = $npm.$ta;

		$hitung = strlen($kodeawal);

		echo $kodeawal.'<br/>';

		$jumlah = 0;

		for ($i=0; $i <= $hitung; $i++) {

			$char = substr($kodeawal,$i,1);

			$jumlah = $jumlah + $char;

		}

		echo $jumlah.'<br/>';

		$mod = $jumlah%10;

		echo $mod.'<br/>';

		$kodebaru = $npm.$ta.$mod;

		echo $kodebaru.'<br/>';

		//$this->benchmark->mark('code_end');

		//echo $this->benchmark->elapsed_time('code_start', 'code_end');

	}



	function kary_sync()

	{

		$this->load->model('setting_model');

		$data = $this->setting_model->getkarynonakun()->result();

		//var_dump($data);exit();

		if (count($data) > 0) {

			foreach ($data as $value) {

				/*if ($value->nik != '') {

					$data1['username']= str_replace(' ', '', $value->nik);

					$data1['userid']= str_replace(' ', '', $value->nik);

					$data1['status']= 1;

					//$pass = explode('-', $value->TGLHRMSMHS);

					$password = $data1['username'];

					$data1['password_plain']= str_replace(' ', '', $password);

					$data1['user_type'] = 1;

					$data1['password']= sha1(md5($password).key);

					$data1['id_user_group']= '7,6';

				} else {*/

					$data1['username']= trim($value->nid);

					$data1['userid']= $value->nid;

					$data1['status']= 1;

					//$pass = explode('-', $value->TGLHRMSMHS);

					$password = 'Ubharaj4y4';

					$data1['password_plain']= $password;

					$data1['user_type'] = 1;

					$data1['password']= sha1(md5($password).key);

					$data1['id_user_group']= '7,6';
				
				//}

				//$data1['id_user_group']= 5;

				//var_dump($data1);

				$insert = $this->app_model->insertdata('tbl_user_login',$data1);

				if ($insert == TRUE) {

					echo "<script>alert('Berhasil');document.location.href='".base_url()."home';</script>";

				} else {

					echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";

				}

				//echo "<br/>";

			}

			//exit();

		} else {

			echo "<script>alert('Sinkronisasi Selesai');document.location.href='".base_url()."home';</script>";

		}

	}



	function jajalgrup()

	{

		$sess = $this->session->userdata('sess_login');

		$user_group = $sess['id_user_group'];

		$grup = '7,5';

		$pecah = explode(',', $grup);

		$jmlh = count($pecah);

		//$jajal1 = array();

		for ($i=0; $i < $jmlh; $i++) { 

			$jajal1[] = $pecah[$i];

		}	

		//var_dump($jajal1);exit();

		//$q = $this->db->query("SELECT DISTINCT menu_id,menu FROM view_role WHERE user_group_id IN(".$jajal1.")");

		$this->db->distinct();

		$this->db->select("menu_id");

		$this->db->where_in('user_group_id', $jajal1);

		$q = $this->db->get('tbl_role_access')->result();

		foreach ($q as $value) {

			echo $value->menu_id."<br>";

		}

		//var_dump($q);exit();

	}

	function coba_gen_bayaran(){

		$this->db->select('*');

		$this->db->from('tbl_mahasiswa a');

		$this->db->join('tbl_index_pembayaran b', 'a.KDPSTMSMHS = b.kd_prodi', 'left');

		$this->db->where('a.id_mhs', 37157);

		$q=$this->db->get()->result();

		

		foreach ($q as $isi) {

			$ambil = substr($isi->NIMHSMSMHS, 2);

			$briva = '70306'.$ambil;

			$balance = (0-($isi->jumlah));



			if ($isi->kd_jenis == 'BPP') {

				for ($smtr=1; $smtr < 2; $smtr++) { 

					for ($i=1; $i < 7 ; $i++) {



						$data = array(

							'nim' 		=> $isi->NIMHSMSMHS,

							'briva' 	=> $briva,

							'keterangan'=> $isi->kd_jenis.' ANG-'.$i,

							'debit'		=> 0,

							'credit'	=> $isi->jumlah,

							'balance'	=> $balance,

							'smtr'		=> $smtr,

							'type'		=> 1,

							'flag'		=> 3,

							'idx'		=> $i+2

							);

						$this->db->insert('tbl_rekap_pembayaran_mhs', $data);

					}

				}

			}elseif ($isi->kd_jenis == 'PRAK') {

				for ($smtr=1; $smtr < 2; $smtr++) { 

					$data = array(

							'nim' 		=> $isi->NIMHSMSMHS,

							'briva' 	=> $briva,

							'keterangan'=> $isi->kd_jenis,

							'debit'		=> 0,

							'credit'	=> $isi->jumlah,

							'balance'	=> $balance,

							'smtr'		=> $smtr,

							'type'		=> 1,

							'flag'		=> 2,

							'idx'		=> 2

							);

					$this->db->insert('tbl_rekap_pembayaran_mhs', $data);

				}

			}elseif ($isi->kd_jenis == 'KMHS') {

				for ($smtr=1; $smtr < 2; $smtr++) { 

					$data = array(

							'nim' 		=> $isi->NIMHSMSMHS,

							'briva' 	=> $briva,

							'keterangan'=> $isi->kd_jenis,

							'debit'		=> 0,

							'credit'	=> $isi->jumlah,

							'balance'	=> $balance,

							'smtr'		=> $smtr,

							'type'		=> 1,

							'flag'		=> 1,

							'idx'		=> 1

							);

					$this->db->insert('tbl_rekap_pembayaran_mhs', $data);

				}

			}

		}





	}


	function ubah_kode(){

		$q = $this->db->query('SELECT * FROM tbl_matakuliah WHERE kd_prodi = 61201')->result();

		//var_dump($q);die();

		foreach ($q as $isi) {
			$kode = $isi->kd_matakuliah;

			$kodebaru1 = substr($kode, 0,3);

			$kodebaru2 = substr($kode, 3,8);


			$kodebaru = $kodebaru1.'1'.$kodebaru2;

			//die($kodebaru);

			$this->db->query('update tbl_matakuliah set kd_matakuliah = "'.$kodebaru.'" where id_matakuliah = '.$isi->id_matakuliah.'');
		}

	}

	function test_cetak_nr(){
		$this->load->library('Cfpdf');

		$id="20131022500220151";

		$data['npm'] = substr($id, 0,12);
		$data['ta']  = substr($id, 12,4);
		
		$a=substr($id, 16,1);

		if ($a == 1) {
			$b = 'Ganjil';
		} else {
			$b = 'Genap';
		}
		

		$data['gg']  = $b;

		$this->load->view('welcome/print/test_krs_old',$data);
	}

	function get_ta(){
		$query=$this->db->query('SELECT a.*,b.`semester_matakuliah` FROM tbl_jadwal_matkul a 
						  LEFT JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`')->result();

		//print_r($query);die();

		foreach ($query as $key) {
			if ($key->semester_matakuliah == '1') {
				$this->db->query('UPDATE tbl_jadwal_matkul SET kd_tahunajaran = 6 WHERE id_jadwal = '.$key->id_jadwal.'');
			}elseif ($key->semester_matakuliah == '3') {
				$this->db->query('UPDATE tbl_jadwal_matkul SET kd_tahunajaran = 4 WHERE id_jadwal = '.$key->id_jadwal.'');
			}elseif ($key->semester_matakuliah == '5'){
				$this->db->query('UPDATE tbl_jadwal_matkul SET kd_tahunajaran = 9 WHERE id_jadwal = '.$key->id_jadwal.'');
			}elseif ($key->semester_matakuliah == '7') {
				$this->db->query('UPDATE tbl_jadwal_matkul SET kd_tahunajaran = 8 WHERE id_jadwal = '.$key->id_jadwal.'');
			}
		}
	}

	function update_verifikasi()
	{
		$data = $this->app_model->getdata('tbl_verifikasi_krs','id_verifikasi','asc')->result();
		foreach ($data as $value) {
			$kode = trim($value->kd_krs);
			$thn = substr($kode, 12,4);
			$ta = substr($kode, 12,5);
			$npm = substr($kode, 0,12);
			$getjurusan = $this->db->query("select KDPSTMSMHS,TAHUNMSMHS from tbl_mahasiswa where NIMHSMSMHS like '".$npm."%' ")->row();
			if (isset($getjurusan->KDPSTMSMHS)) {
				$selisih = $thn - ($getjurusan->TAHUNMSMHS);
				$tambah = substr($kode, 16,1);
				$semawal = $selisih * 2;
				$semester = $semawal + $tambah;
				echo $kode.' - '.$ta.' - '.$npm.' - '.$getjurusan->KDPSTMSMHS.' - '.$semester;
			}
			echo "<br/>";
		}
	}

	function generate_jadwal_init($kode)
	{
		$sql1 = "SELECT * FROM tbl_mahasiswa a WHERE a.NIMHSMSMHS IN(SELECT distinct npm_mahasiswa FROM tbl_krs) and a.TAHUNMSMHS = '2015' and KDPSTMSMHS = '".$kode."' ORDER BY a.NIMHSMSMHS ASC";
		$data['mhs'] = $this->db->query($sql1)->result();

		$sql2 = "SELECT DISTINCT b.kelas FROM tbl_jadwal_matkul b JOIN tbl_matakuliah c ON b.kd_matakuliah = c.kd_matakuliah WHERE c.semester_matakuliah = 1 AND c.`kd_prodi` = '".$kode."' AND SUBSTR(b.`kd_jadwal`,1,5) = '".$kode."' ORDER BY b.kelas ASC";
		$data['kelas'] = $this->db->query($sql2)->result();
		$this->load->view('form_masuk_jadwal',$data);
	}

	function generate_jadwal()
	{
		error_reporting(0);
		$jml = count($this->input->post('nim'));
		$mhs = $this->input->post('nim');
		//var_dump($jml);
		$kelas = $this->input->post('kelas', TRUE);
		for ($i=0; $i < $jml; $i++) { 
			echo $mhs[$i].':';
			$sqlkrs = "SELECT kd_matakuliah from tbl_krs where npm_mahasiswa = '".$mhs[$i]."'";
			$sqlprodi = "SELECT KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = '".$mhs[$i]."'";
			$krs = $this->db->query($sqlkrs)->result();
			$prodi = $this->db->query($sqlprodi)->row();
			foreach ($krs as $mk) {
				if ($mk->kd_matakuliah == 'FEK-1205') {
					continue;
				} else {
					$sqljadwal = "SELECT kd_jadwal from tbl_jadwal_matkul a join tbl_matakuliah b on a.kd_matakuliah = b.kd_matakuliah where a.kd_matakuliah = '".$mk->kd_matakuliah."' and b.kd_prodi = '".$prodi->KDPSTMSMHS."' ";
					$jadwal = $this->db->query($sqljadwal)->row();
					echo $mk->kd_matakuliah.'-'.$jadwal->kd_jadwal.'<br>';
				}
			}
			$data[''] = '';
			$this->db->where('npm_mahasiswa','');
			$this->db->where('kd_krs','');
			$q = $this->db->update('tbl_krs', $data);
			echo "<br>";
		}
	}


	function tambah_jam_sks(){
		date_default_timezone_set("Asia/Jakarta"); 

		$this->db->select('*');
		$this->db->from('tbl_jadwal_matkul a'); 
		$this->db->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah', 'left');
		$this->db->where('id_jadwal', 1637);
		$q=$this->db->get()->result();

		foreach ($q as $isi) {

			$waktu_kuliah = '';
			$mk = $this->db->where('id_matakuliah', $isi->id_matakuliah)
					 	   ->get('tbl_matakuliah')->row();


			
			$time1 = strtotime(date('Y-m-d').' '.$isi->waktu_masuk.'');

			if ($mk->sks_matakuliah == 2) {

	            $waktu_kuliah = '01:40';   

	        }elseif ($mk->sks_matakuliah == 2) {

	        	$waktu_kuliah = '02:30';
	        
	        }

            $time2 = strtotime(date('Y-m-d').' '.$waktu_kuliah.':00');

            $begin_day_unix = strtotime(date('Y-m-d').' 00:00:00');

            $jumlah_time = date('H:i', ($time1 + ($time2- $begin_day_unix)));

            die($time1.'-'.$time2);

            $this->db->query('UPDATE tbl_jadwal_matkul SET waktu_selesai = "'.$jumlah_time.':00" WHERE id_matakuliah = '.$isi->id_jadwal.'');

		}
	}

	function hc(){

		// $series_data[] =  array('name' => 'Nurfan','data' => array(1,2,3) );
		// $series_data[] =  array('name' => 'Hilman','data' => array(4,5,6) ); 

		// $data['series_data'] = json_encode($series_data);

		$data['rows'] = $this->db->query('SELECT prd.`prodi`, COUNT(mhs.`KDPSTMSMHS`) AS jml FROM tbl_mahasiswa mhs
											JOIN tbl_verifikasi_krs krs ON mhs.`NIMHSMSMHS` = krs.`npm_mahasiswa`
											JOIN tbl_jurusan_prodi prd ON mhs.`KDPSTMSMHS` = prd.`kd_prodi`
											GROUP BY mhs.`KDPSTMSMHS`')->result();



		$this->load->view('test_hc',$data);
	}

	function get_data_chart(){

		$query = $this->db->query('SELECT prd.`prodi`, COUNT(mhs.`KDPSTMSMHS`) AS jml FROM tbl_mahasiswa mhs
											JOIN tbl_verifikasi_krs krs ON mhs.`NIMHSMSMHS` = krs.`npm_mahasiswa`
											JOIN tbl_jurusan_prodi prd ON mhs.`KDPSTMSMHS` = prd.`kd_prodi`
											GROUP BY mhs.`KDPSTMSMHS`');

		echo json_encode($query);
	}

	function updatekurikulum($id)
	{
		$this->db->select('a.*,b.nama_matakuliah,b.semester_matakuliah,c.kurikulum');
		$this->db->from('tbl_kurikulum_matkul a');
		$this->db->join('tbl_matakuliah b','a.kd_matakuliah = b.kd_matakuliah');
		$this->db->join('tbl_kurikulum c','c.kd_kurikulum = a.kd_kurikulum');
		$this->db->where('c.kd_kurikulum', $id);
		$q = $this->db->get()->result();
		foreach ($q as $value) {
			$this->db->query("update tbl_kurikulum_matkul set semester_kd_matakuliah = ".$value->semester_matakuliah." where kd_matakuliah = '".$value->kd_matakuliah."' and kd_kurikulum = '".$id."' ");
			echo $value->kd_matakuliah.' - '.$value->nama_matakuliah.' - '.$value->semester_matakuliah;
			echo "<br>";
		}
	}

	function cetak_kartu(){
		
		//$this->load->library('Cfpdf');

		$id="201210225045";

		$data['npm'] = substr($id, 0,12);
		$data['ta']  = substr($id, 12,4);
		
		$a=substr($id, 16,1);

		//var_dump($data);die();

		if ($a == 1) {
			$b = 'Ganjil';
		} else {
			$b = 'Genap';
		}

		$data['gg']  = $b;

		$this->load->view('welcome/print/kartu_uts',$data);
	}

	function test_kartu(){
		
		$this->load->library('Cfpdf');

		$id="201210225045";

		$data['npm'] = substr($id, 0,12);
		$data['ta']  = substr($id, 12,4);
		
		$a=substr($id, 16,1);

		//var_dump($data);die();

		if ($a == 1) {
			$b = 'Ganjil';
		} else {
			$b = 'Genap';
		}

		$data['gg']  = $b;

		$this->load->view('welcome/print/surat_lulus',$data);
	}

	function test_sp(){
		// $this->load->model('test_sp');
		// $this->test_sp->
		$npm = 201210225045;
		$email_list= '';
		$a=$this->db->query('call test_sp2')->result();

		//echo $a;

		echo json_encode($a);

		// var_dump($a);
		// die();
	}

	function ganti_id_reg_pmb(){
		$q= $this->db->get('tbl_form_camaba')->result();

		foreach ($q as $isi) {

			$depan = substr($isi->nomor_registrasi, 0,6);

			$belakang = substr($isi->nomor_registrasi, 6);

			$tengah = '1.';

			

			$nomor_registrasi = $depan.$tengah.$belakang;

			$this->db->query('update tbl_form_camaba set nomor_registrasi = "'.$nomor_registrasi.'" where nomor_registrasi = "'.$isi->nomor_registrasi.'"');
		}
	}

	function cekkodeverifikasi($kode)
	{
		$cek = $this->app_model->getdetail('tbl_verifikasi_krs','kd_krs_verifikasi',$kode,'kd_krs_verifikasi','asc');
		if ($cek->num_rows() > 0) {
			echo "<b>Kode KRS: </b>".$cek->row()->kd_krs.'<br/>';
			$mhs = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$cek->row()->npm_mahasiswa,'NIMHSMSMHS','asc')->row();
			echo "<b>NPM: </b>".$cek->row()->npm_mahasiswa.' - '.$mhs->NMMHSMSMHS.'<br/>';
			echo "<b>Tahun Ajaran: </b>".$cek->row()->tahunajaran.'<br/>';
			echo "<b>Total SKS: </b>".$cek->row()->jumlah_sks.' SKS <br/>';
			echo '<b>Tanggal Perwalian: </b>'.$cek->row()->tgl_bimbingan.'<br/>';
			$kary = $this->app_model->getdetail('tbl_karyawan','nid',$cek->row()->id_pembimbing,'nid','asc')->row();	
			echo '<b>Pembimbing Akademik: </b>('.$cek->row()->id_pembimbing.') - '.$kary->nama.' <br/>';

			echo '<hr/>';

			$krs = $this->app_model->getdetail('tbl_krs','kd_krs',$cek->row()->kd_krs,'kd_matakuliah','asc')->result();
			foreach ($krs as $key) {
				$mk = $this->app_model->getdetail('tbl_matakuliah','kd_matakuliah',$key->kd_matakuliah,'kd_matakuliah','asc')->row();
				$kelas = $this->app_model->getdetail('tbl_jadwal_matkul','kd_jadwal',$key->kd_jadwal,'kd_jadwal','asc')->row()->kelas;
				echo $key->kd_matakuliah.' ('.$mk->sks_matakuliah.' SKS) - '.$mk->nama_matakuliah.' ('.$kelas.') '.'<br/>';
			}

		} else {
			echo "<b>PALSU</b>";
		}
		
	}

	function delete_oi(){
		$q = $this->db->query("SELECT DISTINCT npm_mahasiswa as npm FROM tbl_nilai_detail WHERE kd_matakuliah = 'MMJ-5210' AND nid = '011405032'")->result();

		foreach ($q as $key) {
			//echo $key->npm.'-';

			$this->db->query('delete from tbl_transaksi_nilai where NIMHSTRLNM = "'.$key->npm.'"');
		}
	}

	function exportexceldatamhs()
	{
		$data['mhs'] = $this->db->query("select a.npm_mahasiswa,a.tahunajaran,a.status,b.NMMHSMSMHS,b.TAHUNMSMHS,c.prodi from tbl_sinkronisasi_renkeu a 
										join tbl_mahasiswa b on a.npm_mahasiswa = b.NIMHSMSMHS 
										join tbl_jurusan_prodi c on b.KDPSTMSMHS = c.kd_prodi
										where tahunajaran = '20152' and c.kd_fakultas = '2' and a.npm_mahasiswa != '201110225043' order by NIMHSMSMHS asc")->result();
		$this->load->view('data_excel', $data);
	}

	function bangsat()
	{
		$q = $this->db->query("select npm_mahasiswa,kd_matakuliah,kd_krs,kd_jadwal from tbl_krs_copyz where kd_krs is not null and kd_jadwal is null")->result();
		foreach ($q as $value) {
			$jadwal = $this->db->query("select distinct kd_jadwal from tbl_nilai_detail where kd_krs = '".$value->kd_krs."' and kd_matakuliah = '".$value->kd_matakuliah."' and npm_mahasiswa = '".$value->npm_mahasiswa."' ")->row();
			$this->db->query("update tbl_krs_copyz set kd_jadwal = '".$jadwal->kd_jadwal."' where kd_krs = '".$value->kd_krs."' and kd_matakuliah = '".$value->kd_matakuliah."' and npm_mahasiswa = '".$value->npm_mahasiswa."'");
		}
	}

	function bangsat2()
	{
		$q = $this->db->query("select npm_mahasiswa,kd_matakuliah,kd_krs,kd_jadwal from tbl_krs where kd_krs is not null and kd_jadwal is null")->result();
		foreach ($q as $value) {
			$jadwal = $this->db->query("select distinct kd_jadwal from tbl_nilai_detail where kd_krs = '".$value->kd_krs."' and kd_matakuliah = '".$value->kd_matakuliah."' and npm_mahasiswa = '".$value->npm_mahasiswa."' ")->row();
			$this->db->query("update tbl_krs set kd_jadwal = '".$jadwal->kd_jadwal."' where kd_krs = '".$value->kd_krs."' and kd_matakuliah = '".$value->kd_matakuliah."' and npm_mahasiswa = '".$value->npm_mahasiswa."'");
		}
	}

	function update_krs_feeder()
	{
		$query = $this->db->query("SELECT kd_krs FROM tbl_verifikasi_krs WHERE tgl_bimbingan > '2016-09-18'")->result();
		foreach ($query as $value) {
			$this->db->query("delete from tbl_krs_feeder where kd_krs = '".$value->kd_krs."'");
			$query1 = $this->db->query("SELECT * FROM tbl_krs WHERE kd_krs = '".$value->kd_krs."'")->result();
			foreach ($query1 as $key) {
				$datax[] = array(
	                'kd_krs'		=>$key->kd_krs,
	                'npm_mahasiswa'	=>$key->npm_mahasiswa,
	                'semester_krs'	=>$key->semester_krs,
	                'kd_matakuliah'	=>$key->kd_matakuliah,
	                'kd_jadwal'		=>$key->kd_jadwal);
			}
		}
		$this->db->insert_batch('tbl_krs_feeder',$datax);
	}

	function enur(){

		$aa=$this->db->query('select * from tbl_krs_feeder  order by id_krs desc')->result();

		//var_dump($aa);die();
		foreach ($aa as $isi) {
			$kd_krs = $isi->kd_krs;
			$npm_mahasiswa = substr($kd_krs, 0,12);
			//die($npm_mahasiswa);

			$data = array('npm_mahasiswa' => $npm_mahasiswa );

			$this->db->where('id_krs', $isi->id_krs);
			$this->db->update('tbl_krs_feeder', $data);

		}
		

		
	}

	function import_pa(){
		$query = $this->db->query('SELECT DISTINCT npm_mahasiswa FROM tbl_verifikasi_krs')->result();

		//var_dump($query);die();

		foreach ($query as $key) {
			$qq = $this->db->query('SELECT * FROM tbl_verifikasi_krs WHERE npm_mahasiswa = "'.$key->npm_mahasiswa.'" ORDER BY id_verifikasi DESC LIMIT 1')->row();

			$object = array('kd_dosen' => $qq->id_pembimbing,'npm_mahasiswa' => $qq->npm_mahasiswa,'kelas_mhs' => $qq->kelas);

			$this->db->insert('tbl_pa_copy', $object);

		}
	}

	// function nurfan(){
	// 	$user = $this->session->userdata('sess_login');
	// 	$nim  = $user['userid'];

	// 	//echo $nim;die();

	// 	$array1 = array(0 => 'zero_a', 2 => 'two_a', 3 => 'three_a');
	// 	$array2 = array(1 => 'one_b', 3 => 'three_b', 4 => 'four_b');
	// 	$result = $array1 + $array2;

	// 	var_dump($result);
	// }

	function nurfan(){
		$mk = $this->db->query("SELECT DISTINCT kd_matakuliah FROM tbl_krs WHERE kd_krs LIKE CONCAT(npm_mahasiswa,'20171%')")->result();

		foreach ($mk as $row) {
			$ganda = $this->db->query("SELECT *,COUNT(*) AS ganda FROM tbl_krs 
									WHERE kd_krs LIKE CONCAT(npm_mahasiswa,'20171%') 
									AND kd_matakuliah = '".$row->kd_matakuliah."'
									GROUP BY npm_mahasiswa 
									HAVING      (COUNT(*) > 1)")->result();	
			echo $row->kd_matakuliah."</br>";
			foreach ($ganda as $isi) {
				echo $isi->npm_mahasiswa." - ".$row->kd_jadwal."</br>";
			}
		}


		//fungsi ruang kelas kosong
		// $ruangan = $this->db->query("SELECT ru.* FROM tbl_gedung gd
		// 					JOIN tbl_lantai lt ON gd.`id_gedung` = lt.`id_gedung`
		// 					JOIN tbl_ruangan ru ON lt.`id_lantai` = ru.`id_lantai`
		// 					WHERE gd.`id_gedung` IN ('3','6','7')")->result();
		
		// $startTime = new DateTime("08:00:00");
		// $startsks = new DateTime("08:00:00");
		// $endTime = new DateTime("22:00:00");


		// while($startTime < $endTime) {

		// 	$masuk = $startTime->format('H:i:s');
			
		// 	$startsks->modify('+100 minutes');
		// 	$pulang = $startsks->format('H:i:s');


		// 	foreach ($ruangan as $ruang) {

		// 		for ($i=1; $i <= 7 ; $i++) { 
		// 				$cek = $this->db->query("
		// 						SELECT * FROM tbl_jadwal_matkul WHERE ((waktu_mulai BETWEEN '".$masuk."' AND '".$pulang."') 
		// 						OR (waktu_selesai BETWEEN '".$masuk."' AND '".$pulang."')) AND kd_tahunajaran = 20171 AND 
		// 						waktu_mulai != '".$pulang."' AND waktu_selesai != '".$masuk."' AND
		// 						kd_ruangan = ".$ruang->id_ruangan." AND hari = ".$i."
		// 				")->num_rows();

		// 			if ($cek == 0) {

		// 				echo $ruang->id_ruangan.'Ruangan '.$ruang->ruangan.' ('.$ruang->kode_ruangan.') = hari '.notohari($i).'/jam '.$masuk.' s.d '.$pulang."<br>";
		// 			}
		// 		}

		// 	}
			


		// 	$startTime->modify('+30 minutes'); // can be seconds, hours.. etc
		// 	$startsks = new DateTime($startTime->format('H:i:s'));

		//}

	}

	function generate_login_kv()
	{
		
		$var = $this->db->query("SELECT * from tbl_form_camaba where jenis_pmb = 'KV' and nomor_registrasi like '17%' and (npm_baru != '' or npm_baru != 'NULL')")->result();
		foreach ($var as $key) {
			$data['username'] = $key->npm_baru;
			$data['password'] = 'db39ba82c32d664162eb081c86edcd4911b3c87c';
			$data['password_plain'] = 'Ubharaj4y4';
			$data['userid'] = $key->npm_baru;
			$data['id_user_group'] = '5';
			$data['status'] = '1';
			$data['user_type'] = '2';
			$this->db->insert('tbl_user_login', $data);
		}
	}

	function del_dobel()
	{
		$del = $this->db->query("SELECT npm_mahasiswa FROM tbl_pa 
 
GROUP BY npm_mahasiswa 
HAVING (COUNT(npm_mahasiswa) > 1)")->result();
		
		foreach ($del as $val) {
			echo '\''.$val->npm_mahasiswa.'\',<br>';
		}
	}

	function maintenance()
	{
		$this->load->view('maintenance');
	}

	function kartu_ujian(){
		$this->load->library('Cfpdf');

		$this->load->view('welcome/print/kartu_ujian_pmb');
	}

	function update_null_idmk_in_jadwal()
	{
		$updateData = $this->db->query("SELECT kd_jadwal, kd_matakuliah FROM tbl_jadwal_matkul WHERE id_matakuliah IS NULL AND kd_jadwal NOT LIKE '1/%' AND kd_jadwal NOT LIKE '2/%' AND kd_jadwal NOT LIKE '3/%' AND kd_jadwal NOT LIKE '4/%' AND kd_jadwal NOT LIKE '5/%' AND kd_jadwal NOT LIKE '7/%'");
		foreach ($updateData->result() as $data) {
			// get matkul before
			$matakuliah = $this->db->query("SELECT id_matakuliah FROM tbl_matakuliah WHERE kd_matakuliah = '$data->kd_matakuliah' AND kd_prodi = SUBSTRING('$data->kd_jadwal', 1,5)")->row();

			if (!is_null($matakuliah->id_matakuliah)) {
				$this->db->query("UPDATE tbl_jadwal_matkul SET id_matakuliah = $matakuliah->id_matakuliah WHERE kd_jadwal = '$data->kd_jadwal'");
				
			}
		}
		echo "selesai";
	}
}