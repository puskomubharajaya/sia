<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wisudawan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['q'] = $this->db->query("SELECT * from tbl_wisuda where fak = 'Ekonomi' and jur like '%Akuntansi' order by nama")->result();

		$this->load->view('welcome/wisudawan',$data);
	}


}

/* End of file Wisudawan.php */
/* Location: ./application/controllers/Wisudawan.php */