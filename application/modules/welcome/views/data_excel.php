<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_maba_s1.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr> 
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
            <th>ANGKATAN</th>
            <th>Prodi</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($mhs as $row) { $a = $no;?>
        
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $row->npm_mahasiswa;?></td>
            <td><?php echo $row->NMMHSMSMHS;?></td>
            <?php if ($row->status == '1') {
                $kls = 'DAFTAR ULANG';
            } elseif ($row->status == '2') {
                $kls = 'UTS';
            } elseif ($row->status == '4') {
                $kls = 'UAS';
            } else {
                $kls = 'BELUM';
            }
             ?>
            <td><?php echo $row->TAHUNMSMHS; ?></td>
            <td><?php echo $row->prodi;?></td>
            <td><?php echo $kls; ?></td>
        </tr>
        <?php $no++; } ?>        
    </tbody>
</table>