<?php
// error_reporting(0);
//var_dump($detail_khs);exit();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();


$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',22); 



$pdf->image(FCPATH.'/assets/logo.gif',10,10,20);

$pdf->Ln(0);

$pdf->Cell(200,5,'KARTU HASIL STUDI MAHASISWA',0,3,'C');

$pdf->Ln(2);

$pdf->Cell(200,10,'UNIVERSITAS BHAYANGKARA',0,5,'C');

$pdf->Ln(0);

$pdf->Cell(200,10,'JAKARTA RAYA',0,1,'C');

$pdf->Ln(3);

$pdf->Cell(250,0,'',1,1,'C');


$q=$this->db->query('SELECT a.`NIMHSMSMHS`,a.`NMMHSMSMHS`,a.`TAHUNMSMHS`,a.`SMAWLMSMHS`,b.`kd_prodi`,b.`prodi`,c.`fakultas`,e.`kd_krs`,e.`id_pembimbing`,f.`nama`,f.`nid` 

                    FROM tbl_mahasiswa a

                    JOIN tbl_jurusan_prodi b ON a.`KDPSTMSMHS` = b.`kd_prodi`

                    JOIN tbl_fakultas c ON c.`kd_fakultas` = b.`kd_fakultas`

                    JOIN tbl_verifikasi_krs e ON e.`npm_mahasiswa` = a.`NIMHSMSMHS`

                    left JOIN tbl_karyawan f ON e.`id_pembimbing` = f.`nid`

                    WHERE a.`NIMHSMSMHS` = "'.$npm.'" AND e.tahunajaran = "'.substr($ta->kd_krs, 12,5).'"')->row();

$logged = $this->session->userdata('sess_login');
$prodi = $q->prodi;
$kaprodi=$this->db->query('SELECT * FROM tbl_jurusan_prodi
											WHERE kd_prodi = "' . $q->kd_prodi . '"')->row()->kaprodi;
											

$pdf->ln(6);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'Fakultas',0,0);

$pdf->Cell(10,5,':',0,0,'C');

$pdf->Cell(60,5,$q->fakultas,0,0,'L');

$pdf->Cell(40,5,'Semester',0,0);

$pdf->Cell(5,5,':',0,0,'L');
$a = $this->app_model->get_semester_khs($q->SMAWLMSMHS,substr($q->kd_krs, 12,5));

$pdf->Cell(60,5,$a,0,0,'L');


$pdf->ln(3);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,10,'Program Studi',0,0);

$pdf->Cell(10,10,':',0,0,'C');

$pdf->Cell(60,10,$q->prodi,0,0,'L');

$pdf->Cell(40,10,'Tahun Akademik',0,0);

$pdf->Cell(5,5,':',0,0,'L');

if (substr($ta->kd_krs, 16,1) == 1) {
	$zz = 'Ganjil';
} else {
	$zz = 'Genap';
}

$pdf->Cell(50,10,substr($ta->kd_krs, 12,4).'/'.$zz,0,0,'L');


$pdf->ln(10);

$pdf->SetFont('Arial','B',10); 

$pdf->Cell(40,10,'Identitas Mahasiswa',0,0);

$pdf->Cell(10,10,'',0,0,'C');

$pdf->Cell(60,10,'',0,0,'C');

$pdf->Cell(40,10,'Identitas Dosen PA',0,0);

$pdf->Cell(10,10,'',0,0,'L');

$pdf->Cell(60,10,'',0,0,'L');


$pdf->ln(5);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,10,'Nama Mahasiswa',0,0);

$pdf->Cell(10,10,':',0,0,'C');
$current_y = $pdf->GetY();
$current_x = $pdf->GetX();

$cell_width = 60;
$pdf->MultiCell($cell_width, 5, $q->NMMHSMSMHS, 0,'L',false);

$pdf->ln(5);

$x = 60;
$pdf->SetXY($current_x + $x, $current_y);
//$pdf->Cell(60,10,$q->NMMHSMSMHS,0,0,'L');

$pdf->Cell(40,10,'Nama Dosen PA',0,0);

$pdf->Cell(5,5,':',0,0,'L');

//$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
$pdf->MultiCell(50, 5, $q->nama, 0,'L');


$pdf->ln(7);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'NPM',0,0);

$pdf->Cell(10,5,':',0,0,'C');

$pdf->Cell(60,5,$q->NIMHSMSMHS,0,0,'L');

$pdf->Cell(40,5,'NID',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(60,5,$q->nid,0,0,'L');



$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'',0,0);

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(60,5,'',0,0,'C');

$pdf->SetFont('Arial','',12);

$pdf->Cell(40,5,'',0,0);

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(60,5,'',0,1,'L');


$pdf->SetFont('Arial','',10); 

$pdf->Cell(40,5,'IPS/IPK',0,0);

$pdf->Cell(10,5,':',0,0,'C');


	$ips = $this->app_model->getipsmhs($q->kd_prodi,$q->NIMHSMSMHS,substr($q->kd_krs, 12,5));


if (substr($q->NIMHSMSMHS, 0,4) > 2014) {
    $ipk = $this->app_model->getipkmhs($q->NIMHSMSMHS);
	$pdf->Cell(60,5,$ips.'/'.$ipk,0,0,'L');
}elseif ($a > 1) {
	$ipk = $this->app_model->getipkmhslain($q->NIMHSMSMHS,$q->kd_prodi);
    $pdf->Cell(60,5,$ips.'/'.$ipk,0,0,'L');
}

$pdf->ln(10);


$pdf->ln(10);

$pdf->SetLeftMargin(10);

$pdf->SetFont('Arial','',10); 

$pdf->Cell(15,8,'NO',1,0,'C');

$pdf->Cell(45,8,'KODE MATAKULIAH',1,0,'C');

$pdf->Cell(100,8,'NAMA MATAKULIAH',1,0,'C');

$pdf->Cell(15,8,'SKS',1,0,'C');

$pdf->Cell(15,8,'NILAI',1,1,'C');



$no=1;

$total_sks=0;


$no = 1; foreach ($krs as $row) {

	$pdf->Cell(15,8,number_format($no),1,0,'C');

	$pdf->Cell(45,8,$row->kd_matakuliah,1,0,'C');

	$pdf->SetFont('Arial','',8);

	$pdf->Cell(100,8,$row->nama_matakuliah,1,0,'L');
	//$pdf->MultiCell(50, 5, $q->nama, 0,'L');

	$pdf->SetFont('Arial','',10); 
	$pdf->Cell(15,8,$row->sks_matakuliah,1,0,'C');

	

	$adanilai = $this->db->query('SELECT * FROM tbl_transaksi_nilai WHERE NIMHSTRLNM  = '.$q->NIMHSMSMHS.' AND KDKMKTRLNM = "'.$row->kd_matakuliah.'" AND THSMSTRLNM = "'.substr($q->kd_krs, 12,5).'"');

    
    if ($adanilai->num_rows() != 0) {
        $pdf->Cell(15,8,$adanilai->row()->NLAKHTRLNM,1,1,'C');     
    }else {
        $pdf->Cell(15,8,'-',1,1,'C');
    }

	

	$total_sks = $total_sks + $row->sks_matakuliah;

	$no++;

}


$pdf->Cell(160,8,'Total',1,0,'C');

$pdf->Cell(15,8,$total_sks,1,0,'C');

$pdf->Cell(15,8,'',1,1,'C');


$pdf->ln(8);

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',12);

date_default_timezone_set('Asia/Jakarta'); 

$pdf->Cell(170,5,'Jakarta,'.date('d-m-Y').'',0,0,'R');


$pdf->ln(8);

$pdf->SetFont('Arial','B',12); 

$pdf->Cell(20,5,'MAHASISWA',0,0);

$pdf->Cell(75,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(30,5,'KETUA PROGRAM STUDI',0,0);

$pdf->Cell(60,5,'',0,0,'L');

$pdf->Cell(10,5,'',0,1,'C');



$pdf->ln(15);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(20,5,'Nama',0,0);

$pdf->Cell(5,5,':',0,0,'C');

$current_y = $pdf->GetY();
$current_x = $pdf->GetX();

$cell_width = 70;
$pdf->MultiCell($cell_width, 5, $q->NMMHSMSMHS, 0,'L',false);
//$pdf->Cell(80,5,,0,0,'C');

//$pdf->ln(5);

$x = 80;
$pdf->SetXY($current_x + $x, $current_y);

//$pdf->Cell(80,5,$q->NMMHSMSMHS,0,0,'C');



$pdf->Cell(20,5,'Nama',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->MultiCell(80, 5,nama_dsn($kaprodi), 0,'L');
//$pdf->Cell(80,5,$q->nama,0,0,'L');
$pdf->ln();
$pdf->SetFont('Arial','',12); 

$pdf->Cell(20,5,'NPM',0,0);

$pdf->Cell(5,5,':',0,0,'C');

$pdf->Cell(80,5,$q->NIMHSMSMHS,0,0,'L');

$pdf->Cell(20,5,'NID',0,0);

$pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(80,5,$kaprodi,0,0,'L');





$pdf->Output('KHS_'.$q->NIMHSMSMHS.'_'.substr($ta->kd_krs, 12,5).'.PDF','I');



?>

