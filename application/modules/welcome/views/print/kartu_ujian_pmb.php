<?php

//ob_start();
$pdf = new FPDF("L","mm", "A5");



$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(2, 0 ,0);

$pdf->SetFont('Arial','B',15); 

//$pdf->image('http://172.16.1.5:801/assets/ubhara.png',30,50,90);
$pdf->image('http://172.16.1.5:801/assets/logo.gif',5,2,15);
$pdf->setXY(22,4);
$pdf->Cell(200,5,'UNIVERSITAS BHAYANGKARA JAKARATA RAYA',0,5,'L');
$pdf->Ln(2);
$pdf->SetFont('Arial','',15); 
$pdf->setX(22);
$pdf->Cell(200,5,'PENERIMAAN MAHASISWA BARU',0,5,'L');
$pdf->Ln(2);
$pdf->Cell(205,0,'',1,1,'C');
$pdf->setXY(175,1);
$pdf->SetFont('Arial','',8);
$pdf->Cell(200,5,'Nomor Peserta :',0,5,'L');
$pdf->setXY(176,6);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(30,10,'2018100156',1,10,'C');
$pdf->image(base_url().'foto_wisudawan/Contoh.png',15,35,50);


$pdf->SetLeftMargin(5);
$pdf->setY(22);
$pdf->SetFont('Arial','B',11); 
$pdf->Cell(205,5,'KARTU TANDA PESERTA UJIAN',0,1,'C');
$pdf->setXY(14,34);
$pdf->Cell(53,77,'',1,1,'C');

$pdf->SetFont('Arial','',10); 
$pdf->setXY(80,32);
$pdf->Cell(28,5,'Nama Peserta',0,0,'L');
$pdf->Cell(1,5,' : ',0,1,'C');
$pdf->setXY(80,37);
$pdf->Cell(28,5,'Asal Sekolah',0,0,'L');
$pdf->Cell(1,5,' : ',0,1,'C');
$pdf->setXY(80,42);
$pdf->Cell(28,5,'Tanggal Ujian',0,0,'L');
$pdf->Cell(1,5,' : ',0,1,'C');
$pdf->setXY(80,47);
$pdf->Cell(28,5,'Lokasi Ujian',0,0,'L');
$pdf->Cell(1,5,' : ',0,1,'C');
$pdf->ln(5);


$pdf->setXY(80,57);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,9,'Waktu',1,0,'C');
$pdf->Cell(80,9,'Kegiatan',1,1,'C');
$pdf->SetFont('Arial','',10);
$pdf->setXY(80,66);
$pdf->Cell(40,7,'08.00 - 08.30',1,0,'C');
$pdf->Cell(80,7,'Pengisian Data Diri',1,1,'C');
$pdf->setXY(80,73);
$pdf->Cell(40,7,'08.30 - 10.30',1,0,'C');
$pdf->Cell(80,7,'Tes kemampuan Dasar',1,1,'C');
$pdf->setXY(80,80);
$pdf->Cell(40,7,'10.30 - 11.00',1,0,'C');
$pdf->Cell(80,7,'Istirahat',1,1,'C');
$pdf->setXY(80,87);
$pdf->Cell(40,7,'11.00 - 12.30',1,0,'C');
$pdf->Cell(80,7,'Tes Matematika Dasar',1,1,'C');


$pdf->ln(18);
$pdf->Cell(100,5,'Cetaklah kartu ujian menggunakan printer berwarna.',0,1,'L');
$pdf->Cell(100,5,'Ketika ujian, Anda harus membawa: kartu tanda peserta ujian, kartu identitas yang anda pilih ketika',0,1,'L');
$pdf->Cell(100,5,'membuat akun di penerimaan.ubharajaya.ac.id, dan perlengkapan tulis (pensil 2B, penghapus, rautan).',0,1,'L');
$pdf->image(base_url().'QRImage/000e3daa0ad52229f45c6ce0d5f4b387.png',170,110,30);

$pdf->setXY(5,112);
$pdf->Cell(164,15,'',1,1,'C');

$pdf->SetFont('Arial','',12);
date_default_timezone_set('Asia/Jakarta'); 

//exit();
$pdf->Output('Kartu_ujian_PMB_UBJ'.date('ymd_his').'.PDF','I');

?>

