<?php

ob_start();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();





$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',22); 



$pdf->image('http://172.16.1.5:801/assets/logo.gif',10,10,20);

$pdf->Ln(0);

$pdf->Cell(200,5,'SURAT PERMOHONAN CUTI',0,3,'C');

$pdf->Ln(2);

$pdf->Cell(200,10,'UNIVERSITAS BHAYANGKARA',0,5,'C');

$pdf->Ln(0);

$pdf->Cell(200,10,'JAKARTA RAYA',0,1,'C');

$pdf->Ln(3);

$pdf->Cell(250,0,'',1,1,'C');



$pdf->ln(7);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(130,5,'',0,0);

// $pdf->Cell(10,5,':',0,0,'C');

// $pdf->Cell(60,5,0,0,'L');

$pdf->Cell(40,5,'Kepada Yth,',0,0);

// $pdf->Cell(5,5,':',0,0,'L');

// $pdf->Cell(60,5,0,0,'L');



$pdf->ln(3);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(130,10,'',0,0);

// $pdf->Cell(10,10,':',0,0,'C');

// $pdf->Cell(60,10,0,0,'L');

$pdf->Cell(31,10,'Dekan Fakultas',0,0);
$pdf->Cell(10,10,$cetak->fak,0,0);
//$pdf->Cell(40,10,$cetak->fakultas,0,0);

// $pdf->Cell(5,5,':',0,0,'L');

$pdf->Cell(50,10,substr(0,4),0,0,'L');



$pdf->ln(6);

$pdf->SetFont('Arial','',12); 
$pdf->Cell(130,10,'',0,0);

$pdf->Cell(40,10,'Di Jakarta',0,0);

// $pdf->Cell(10,10,'',0,0,'C');

// $pdf->Cell(60,10,'',0,0,'C');

// $pdf->ln(5);

// $pdf->SetFont('Arial','',12);

// $pdf->Cell(130,10,'',0,0);

// $pdf->Cell(40,10,'Jakarta',0,0);


$pdf->ln(5);

$pdf->SetFont('Arial','',12);

$pdf->Cell(40,10,'',0,1);


$pdf->ln(5);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(5,10,'',0,0);

$pdf->Cell(40,10,'Dengan hormat,',0,1);

// $pdf->Cell(10,10,':',0,'C');

// $pdf->Cell(60,10,0,0,'L');


$pdf->ln(1);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(5,10,'',0,0);

$pdf->Cell(53,0,'Yang bertanda dibawah ini',0,0);

$pdf->Cell(5,0,':',0,0);


$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(65,10,'Nama',0,0);
$pdf->Cell(5,10,':',0,'L');
$pdf->Cell(10,10,$cetak->NMMHSMSMHS,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(65,0,'NPM',0,0);
$pdf->Cell(5,0,':',0,'L');
$pdf->Cell(10,0,$cetak->npm_mhs,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(65,10,'Fakultas / Jurusan',0,0);
$pdf->Cell(5,10,':',0,'L');
$pdf->Cell(5,10,$cetak->prodi,0,1,'L');

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(65,0,'Kelas',0,0);
$pdf->Cell(5,0,':',0,'L');
$pdf->Cell(27,0,'Pagi / Sore *)',0,0);
$pdf->Cell(20,0,'Semester',0,0);
$pdf->Cell(3,0,':',0,'L');
$pdf->Cell(10,0,$semester,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(65,10,'Jumlah SKS yang telas ditempuh',0,0);
$pdf->Cell(5,10,':',0,'L');
$pdf->Cell(10,10,'......',0,1);
//$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
// $pdf->MultiCell(50, 5,0,'L');
$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(65,0,'Alamat',0,0);
$pdf->Cell(5,0,':',0,'L');
$pdf->MultiCell(80,5,$cetak->nama_jalan,0,'L');

$pdf->ln(1);

$pdf->SetFont('Arial','',12); 
$pdf->Cell(5,10,'',0,0);

$pdf->Cell(0,5,'Dengan ini mengajukan cuti perkuliahan pada semester : Ganjil / Genap * ) Tahun Akademik :',0,0);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(10,10,'',0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(10,0,'Selanjutnya saya bersedia menyelesaikan administrasi yang ditentukan dan prosedur sebagaimana',0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(10,10,'mestinya.',0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(10,0,'Demikian permohonan ini saya buat untuk dapat diperhatikan.',0,1);
// $pdf->ln(5);

// $pdf->SetFont('Arial','',10); 

// $pdf->Cell(40,5,'',0,0);

// $pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(60,5,'',0,0,'C');

// $pdf->SetFont('Arial','',12);

// $pdf->Cell(40,5,'',0,0);

// $pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(60,5,'',0,1,'L');


$pdf->ln(8);

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',12);
$pdf->Cell(5,10,'',0,0);

date_default_timezone_set('Asia/Jakarta'); 

$pdf->Cell(170,5,'Jakarta,'.date('d-m-Y').'',0,0,'R');



$pdf->ln(8);

$pdf->SetFont('Arial','B',12);

$pdf->Cell(25,5,'',0,0,'C');

$pdf->Cell(30,5,'Mengetahui',0,0,'C');

$pdf->Cell(60,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

$pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(80,5,'Mahasiswa',0,0,'C');

$pdf->Cell(60,5,'',0,0,'L');

$pdf->Cell(10,5,'',0,1,'C');


$pdf->ln(1);

$pdf->SetFont('Arial','B',12);

$pdf->Cell(17,10,'',0,0);
$pdf->Cell(140,7,'Penasehat Akademik',0,0);
$pdf->Cell(10,5,'Mahasiswa',0,0,'C');


$pdf->ln(1);

$pdf->SetFont('Arial','B',12);

$pdf->Cell(8,10,'',0,1);


$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','B',12);
$pdf->Cell(12,10,'',0,0);
$pdf->Cell(115,10,$cetak->mana,0,0);
$pdf->Cell(140,10,$cetak->NMMHSMSMHS,0,1);


$pdf->Ln();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(90,10,'',0,0);
$pdf->Cell(10,7,'Disetujui',0,0);

$pdf->Ln();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(80,10,'',0,0);
$pdf->Cell(10,7,'Dekan/Wakil Dekan 1',0,1);
// $pdf->ln(30);

$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(65,10,'',0,0);
$pdf->Cell(10,7,'(........................................................)',0,0);

// $pdf->SetFont('Arial','',12); 

// $pdf->Cell(20,5,'',0,0,'C');

// $pdf->Cell(30,5,0,0,'C');

// $pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(80,5,'',0,0,'C');

// $pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(30,5,0,0,'C');

// $pdf->Cell(10,5,'',0,0,'L');

// $pdf->Cell(60,5,'',0,0,'L');



// $pdf->ln(5);

// $pdf->SetFont('Arial','',12); 

// $pdf->Cell(15,5,'',0,0,'C');

// $pdf->Cell(30,5,0,0,'C');

// $pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(80,5,'',0,0,'C');

// $pdf->Cell(10,5,'',0,0,'C');

// $pdf->Cell(30,5,0,0,'C');

// $pdf->Cell(10,5,'',0,0,'L');

// $pdf->Cell(60,5,'',0,0,'L');




//exit();
$pdf->Output();


ob_end_flush();
?>

