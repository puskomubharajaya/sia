<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_teknik.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th width="50">No</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th width="80">Absensi (10%)</th>
                                <th width="80">Tugas (20%)</th>
                                <th width="80">UTS (30%)</th>
                                <th width="80">UAS (40%)</th>
                                <th width="40">Angka</th>
                                <th width="40">Huruf</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($kolek as $row){?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->NIMHSMSMHS; ?></td>
                                <td><?php echo $row->NMMHSMSMHS; ?></td>
                                <?php $absensi = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,2,$row->kd_matakuliah)->row(); ?>
                                <?php $tugas = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,1,$row->kd_matakuliah)->row(); ?>
                                <?php $uts = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,3,$row->kd_matakuliah)->row(); ?>
                                <?php $uas = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,4,$row->kd_matakuliah)->row(); ?>

                                <?php $rt = ($absensi->nilai*0.1)+($tugas->nilai*0.2)+($uts->nilai*0.3)+($uas->nilai*0.4);

                                    $logged = $this->session->userdata('sess_login');
                                    if (($logged['userid'] == '61101') or ($logged['userid'] == '74101')) {
                                        $rtnew = number_format($rt,2);
                                        if (($rtnew >= 80) and ($rtnew <= 100)) {
                                            $rw = "A";
                                        } elseif (($rtnew >= 75) and ($rtnew <= 79.99)) {
                                            $rw = "A-";
                                        } elseif (($rtnew >= 70) and ($rtnew <= 74.99)) {
                                            $rw = "B+";
                                        } elseif (($rtnew >= 65) and ($rtnew <= 69.99)) {
                                            $rw = "B";
                                        } elseif (($rtnew >= 60) and ($rtnew <= 64.99)) {
                                            $rw = "B-";
                                        } elseif (($rtnew >= 55) and ($rtnew <= 59.99)) {
                                            $rw = "C";
                                        } elseif (($rtnew >= 40) and ($rtnew <= 54.99)) {
                                            $rw = "D";
                                        } elseif (($rtnew >= 0) and ($rtnew <= 39.99)) {
                                            $rw = "E";
                                        } elseif ($rtnew >100) {
                                            $rw = "T";
                                        }
                                    } else {
                                        $rtnew = number_format($rt,2);
                                        if (($rtnew >= 80) and ($rtnew <= 100)) {
                                            $rw = "A";
                                        } elseif (($rtnew >= 65) and ($rtnew <= 79.99)) {
                                            $rw = "B";
                                        } elseif (($rtnew >= 55) and ($rtnew <= 64.99)) {
                                            $rw = "C";
                                        } elseif (($rtnew >= 45) and ($rtnew <= 54.99)) {
                                            $rw = "D";
                                        } elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
                                            $rw = "E";
                                        }  elseif ($rtnew >100) {
                                            $rw = "T";
                                        }
                                    }
                                ?>

                                <td><?php if($absensi->nilai > 100){ echo "-"; } else { echo $absensi->nilai; } ?></td>
                                <td><?php if($tugas->nilai > 100){ echo "-"; } else { echo $tugas->nilai; } ?></td>
                                <td><?php if($uts->nilai > 100){ echo "-"; } else { echo $uts->nilai; } ?></td>
                                <td><?php if($uas->nilai > 100){ echo "-"; } else { echo $uas->nilai; } ?></td>
                                <td><?php if($rt > 100){ echo "-"; } else { echo $rt; } ?></td>
                                <td><?php echo $rw; ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>