<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktifitas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(148)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
			$this->load->model('datas/Aktifitas_model','activity');
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$this->session->unset_userdata('ta');
		$this->session->unset_userdata('prodi');

		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'aktifitas_select';
		$this->load->view('template/template', $data);
	}

	function save_session()
	{
		$user = $this->session->userdata('sess_login');
		$nik   = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup) or in_array(19, $grup))) {
			$this->session->set_userdata('prodi',$nik);
			$this->session->set_userdata('ta',$this->input->post('tahunajaran', TRUE));
		} else {
			$this->session->set_userdata('prodi',$this->input->post('jurusan', TRUE));
			$this->session->set_userdata('ta',$this->input->post('tahunajaran', TRUE));
		}
		
		redirect('datas/aktifitas/view','refresh');
	}

	function view()
	{
		$data['prodi_and_ta'] = [ 'prodi' => $this->session->userdata('prodi'), 'ta' => $this->session->userdata('ta') ];
		$data['getaktifitas'] = $this->app_model->getdataaktifitas($this->session->userdata('prodi'),$this->session->userdata('ta'));
		$data['page'] = 'aktifitas_view';
		$this->load->view('template/template', $data);
	}

	function viewdetail($id)
	{
		$data['page'] = 'aktifitas_detail';
		$this->load->view('template/template', $data);
	}

	function get_jurusan($id)
	{
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option disabled>--Pilih Program Studi--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;

	}

	public function exportLectureActivity($prodi, $ta)
	{
		$data['lectureActivity'] = $this->app_model->getdataaktifitas($prodi,$ta);
		$this->load->view('excel_lecture_activity', $data);
	}

}

/* End of file Aktifitas.php */
/* Location: ./application/modules/datas/controllers/Aktifitas.php */