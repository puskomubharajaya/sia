<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Karyawan extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(8)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth', 'refresh');
		}
	}

	function index()
	{
		$logged = $this->session->userdata('sess_login');
		$data['crud'] 	= $this->db->query('SELECT * from tbl_role_access where menu_id = 8 
											and user_group_id = ' . $logged['id_user_group'] . ' ')->row();
		$data['karyawan'] = $this->app_model->get_karyawan();

		$data['divisi'] = $this->app_model->getdata('tbl_jurusan_prodi', 'kd_prodi', 'asc');

		$data['page'] = 'datas/karyawan_view';
		$this->load->view('template/template', $data);
	}

	function load_edit($idk)
	{
		$data['prodi'] = $this->db->get('tbl_jurusan_prodi')->result();
		$data['group']  = $this->db->get('tbl_user_group')->result();
		$data['row'] = $this->db->select('*')
			->from('tbl_karyawan a')
			->join('tbl_jurusan_prodi b', 'a.jabatan_id = b.kd_prodi')
			->where('id_kary', $idk)
			->get()->row();

		$this->load->view('datas/karyawan_edit', $data);
	}

	function get_listjab($id)
	{
		$data 	= $this->db->query("SELECT * from tbl_jabatan where kd_divisi = '" . $id . "' 
									AND jabatan like '%TENAGA PENDIDIK%'")->result();
		$list = "<option> -- </option>";

		foreach ($data as $row) {
			$list .= "<option value='" . $row->id_jabatan . "'>" . $row->jabatan . "</option>";
		}
		$list .= "<option value='20'>TENAGA PENDIDIK (TIDAK TETAP)</option>";

		die($list);
	}

	function upload_photo($data, $name)
	{
		$file = $data;
		$folder = "./upload/";
		$folder = $folder . basename($name);
		move_uploaded_file($data['tmp_name'], $folder);
	}

	function save_karyawan()
	{
		date_default_timezone_set('Asia/Jakarta');
		$tgl  = date('YmdHis');
		//$photo = ''.$tgl.'_'.$_FILES['foto']['name'].'';
		//$this->upload_photo($_FILES['foto'],$photo);
		if ($this->input->post('status') == 'NULL') {
			$c = NULL;
		} else {
			$c = 1;
		}
		if ($this->input->post('homebase') == 'NULL') {
			$d = NULL;
		} else {
			$d = 1;
		}
		if ($this->input->post('struktural') == 'NULL') {
			$e = NULL;
		} else {
			$e = 1;
		}
		$data = array(
			'nid'		  => $this->input->post('nid'),
			'nidn'		  => $this->input->post('nidn'),
			'nik'		  => $this->input->post('nidk'),
			'nupn'		  => $this->input->post('nupn'),
			'nik_type'	  => 1,
			'nama'		  => $this->input->post('nama'),
			'jns_kel'	  => $this->input->post('jk'),
			'alamat'      => $this->input->post('alamat'),
			'hp'		  => $this->input->post('telepon'),
			'email'		  => $this->input->post('email'),
			'jabatan_id'  => $this->input->post('prodi'),
			'jabfung'  	  => $this->input->post('jabfung'),
			'tmt_jabfung' => $this->input->post('tmt'),
			'status'	  => 1,
			'tetap'	  	  => $c,
			'homebase'	  => $d,
			'struktural'  => $e,
			'aktif'		  => $this->input->post('aktf', TRUE)
			// 'tgl_masuk'	  => $this->input->post('masuk')
		);
		$cek = $this->app_model->getdetail('tbl_karyawan', 'nid', $this->input->post('nid'), 'nid', 'asc')->result();
		if ($cek == TRUE) {
			echo "<script>alert('NID SUDAH DIGUNAKAN');
			history.go(-1);</script>";
			//exit();
		} else {
			$this->app_model->insertdata('tbl_karyawan', $data);

			$bio = [
				'nid' => $this->input->post('nid'),
				'nidn' => $this->input->post('nidn'),
				'nama' => $this->input->post('nama'),
				'alamat' => $this->input->post('alamat'),
				'tlp' => $this->input->post('telepon'),
				'email' => $this->input->post('email'),
				'jabfung' => $this->input->post('jabfung')
			];
			$this->db->insert('tbl_biodata_dosen', $bio);

			echo "<script>alert('Sukses');
			document.location.href='" . base_url() . "datas/karyawan';</script>";
		}
	}

	function update_karyawan()
	{
		$sessLogin = $this->session->userdata('sess_login');

		$new_nid = $this->input->post('nid');
		$old_nid = $this->input->post('old_nid');

		$cek = $this->app_model->getdetail('tbl_karyawan', 'nid', $new_nid, 'nid', 'asc')->num_rows();
		if ($cek > 0 && $new_nid != $old_nid) {
			echo "<script>alert('NID SUDAH DIGUNAKAN');history.go(-1);</script>";
			return;
		}

		// if NID changes, all change
		if ($new_nid != $old_nid) {
			// update tbl_pa
			$this->db->where('kd_dosen', $old_nid);
			$this->db->update('tbl_pa', ['kd_dosen' => $new_nid]);

			// update tbl_jadwal_matkul
			$this->db->where('kd_dosen', $old_nid);
			$this->db->update('tbl_jadwal_matkul', ['kd_dosen' => $new_nid]);

			// update tbl_jadwal_matkul_feeder
			$this->db->where('kd_dosen', $old_nid);
			$this->db->update('tbl_jadwal_matkul_feeder', ['kd_dosen' => $new_nid]);

			// update tbl_biodata_dosen
			$this->db->where('nid', $old_nid);
			$this->db->update('tbl_biodata_dosen', ['nid' => $new_nid]);

			// update tbl_exception (dispensasi upload nilai)
			$this->db->where('nid', $old_nid);
			$this->db->update('tbl_exception', ['nid' => $new_nid]);

			// tbl_penelitian_dosen
			$this->db->where('userid', $old_nid);
			$this->db->update('tbl_penelitian_dosen', ['userid' => $new_nid]);

			// tbl_pengabdian_dosen
			$this->db->where('userid', $old_nid);
			$this->db->update('tbl_pengabdian_dosen', ['userid' => $new_nid]);

			// tbl_publikasi_dosen
			$this->db->where('userid', $old_nid);
			$this->db->update('tbl_publikasi_dosen', ['userid' => $new_nid]);

			// tbl_verifikasi_krs
			$this->db->where('id_pembimbing', $old_nid);
			$this->db->update('tbl_verifikasi_krs', ['id_pembimbing' => $new_nid]);

			// tbl_verifikasi_krs_sp
			$this->db->where('id_pembimbing', $old_nid);
			$this->db->update('tbl_verifikasi_krs_sp', ['id_pembimbing' => $new_nid]);

			// tbl_verifikasi_krs_sp
			$this->db->where('userid', $old_nid);
			$this->db->update('tbl_user_login', [
				'userid' => $new_nid,
				'username' => $new_nid,
				'password' => sha1(md5('Ubharaj4y4') . key),
				'password_plain' => 'Ubharaj4y4'
			]);
		}

		if ($this->input->post('status') == 'NULL') {
			$c = NULL;
		} else {
			$c = 1;
		}
		if ($this->input->post('homebase') == 'NULL') {
			$d = NULL;
		} else {
			$d = 1;
		}
		if ($this->input->post('struktural') == 'NULL') {
			$e = NULL;
		} else {
			$e = 1;
		}

		$data = array(
			'nid'		  => $new_nid,
			'nidn'		  => $this->input->post('nidn'),
			'nik'		  => $this->input->post('nidk'),
			'nupn'		  => $this->input->post('nupn'),
			'nik_type'	  => 1,
			'nama'		  => $this->input->post('nama'),
			'jns_kel'	  => $this->input->post('jk'),
			'alamat'      => $this->input->post('alamat'),
			'hp'		  => $this->input->post('telepon'),
			'email'		  => $this->input->post('email'),
			'jabatan_id'  => $this->input->post('prodi'),
			'jabfung'  	  => $this->input->post('jabfung'),
			'tmt_jabfung' => $this->input->post('tmt'),
			'status'	  => 1,
			'tetap'	  	  => $c,
			'homebase'	  => $d,
			'struktural'  => $e,
			'aktif'		  => $this->input->post('stsakt', TRUE),
			'updated_at' => date('Y-m-d H:i:s'),
			'updated_by' => $sessLogin['userid']
		);


		$this->db->where('nid', $old_nid);
		$this->db->update('tbl_karyawan', $data);
		echo "<script>alert('Sukses');
		document.location.href='" . base_url() . "datas/karyawan';</script>";
	}

	function del_karyawan($id)
	{
		$this->app_model->deletedata('tbl_karyawan', 'id_kary', $id);
		echo "<script>alert('Berhasil');
		document.location.href='" . base_url() . "datas/karyawan';</script>";
	}

	function detlKaryawan($id)
	{
		$this->session->set_userdata('sess_biokaryawan', $id);
		redirect(base_url('sync_feed/biodata/profil'));
	}

	function exprt()
	{
		$this->load->model('temph_model');
		extract(PopulateForm());

		$data['load'] = $this->temph_model->exportdosen($tipedosen, $jabfung, $home, $prodi)->result();

		$this->load->view('exceldosen', $data);
	}
}

/* End of file karyawan.php */
/* Location: ./application/controllers/karyawan.php */
