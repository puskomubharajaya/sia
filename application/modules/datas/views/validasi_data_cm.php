<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Validasi Pendaftaran Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <form action="<?php echo base_url(); ?>datas/validasipmb/savevalidasi" method="post">
                        <a class="btn btn-success btn-large" href="<?php echo base_url(); ?>data/validasipmb/print_xls"><i class="btn-icon-only icon-print"></i>Print</a>
                        <input type="submit" class="btn btn-primary btn-large" value="Submit">
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th width="100">Nomor ID</th>
                                    <th>Nama</th>
                                    <th>Asal Sekolah</th>
                                    <th>Pilihan</th>
                                    <th width="40">Gelombang</th>
                                    <th width="40">Pembayaran Formulir</th>
                                    <th>Print</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($list as $row) { ?>
                                <tr>
                                    <td><?php echo $row->ID_registrasi; ?><!--input type="hidden" value="<?php //echo $row->ID_registrasi; ?>" name="reg"></td-->
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php echo $row->asal_skl; ?></td>
                                    <td><?php echo $row->prodi; ?></td>
                                    <td><?php echo substr($row->ID_registrasi, 0,1); ?></td>
                                    <?php if ($row->status != 1) { ?>
                                       <td><input type="checkbox" name="reg[]" value="1zzz<?php echo $row->ID_registrasi;?>"></td>
                                    <?php } else { ?>
                                        <td><input type="checkbox" name="reg[]" value="1zzz<?php echo $row->ID_registrasi;?>" checked disabled></td>
                                    <?php }?>
                                    <?php if ($row->status != 1) { ?>
                                       <td>-</td>
                                    <?php } else { ?>
                                        <td width="40"><a href="<?php echo base_url();?>datas/validasipmb/print_kareg/<?php echo $row->ID_registrasi; ?>" target="_blank" class="btn btn-success"><i class="icon-print"></i></a></td>
                                    <?php }?>  
                                     
                                
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>