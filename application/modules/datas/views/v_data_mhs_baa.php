<script type="text/javascript">
  function change() {
    var selectBox = document.getElementById("faks");
    var selected = selectBox.options[selectBox.selectedIndex].value;


    $('#jurs option').prop('selected', function() {
        return this.defaultSelected;
    });

    $('#angk option').prop('selected', function() {
        return this.defaultSelected;
    });

  }
  
</script>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Rekap Data Akademik</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>datas/mahasiswa/view" target="_blank">
          <fieldset>

                 <div id="fakultas" class="control-group">
                    <label class="control-label">Fakultas</label>
                    <div class="controls">
                      <select class="form-control span6" name="fakultas" onchange="change(this)" id="faks" required>
                        <option>--Pilih Fakultas--</option>
                        <?php foreach ($fakultas as $row) { ?>
                        <option value="<?php echo $row->kd_fakultas;?>"><?php echo $row->fakultas;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div id="jurusan" class="control-group">
                    <label class="control-label">Jurusan</label>
                    <div class="controls">
                      <select class="form-control span6" name="jurusan" id="jurs" required>
                        <option>--Pilih Jurusan--</option>
                      </select>
                    </div>
                  </div>
                <div id="tahun" class="control-group">
                  <label class="control-label">Tahun Ajaran</label>
                  <div class="controls">
                    <select class="form-control span6" name="tahun" id="angk" required>
                        <option selected>--Pilih Angkatan--</option>
                      </select>
                  </div>
                </div>    
              <br />
                    <div class="form-actions">
                        <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
                    </div> <!-- /form-actions -->
                </fieldset>
            </form>
          
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){

  function change() {
    var selectBox = document.getElementById("faks");
    var selected = selectBox.options[selectBox.selectedIndex].value;


    $('#jurs option').prop('selected', function() {
        return this.defaultSelected;
    });

    $('#angk option').prop('selected', function() {
        return this.defaultSelected;
    });

  }

  $('#faks').change(function(){
    $.post('<?php echo base_url()?>datas/mahasiswa/get_jurusan/'+$(this).val(),{},function(get){
      $('#jurs').html(get);
    });
  });

  $('#jurs').change(function(){
    $.post('<?php echo base_url()?>datas/mahasiswa/get_angkatan/'+$(this).val(),{},function(get){
      $('#angk').html(get);
    });
  });
});
</script>