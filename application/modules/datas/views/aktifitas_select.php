<?php
  $user = $this->session->userdata('sess_login');
  $nik   = $user['userid'];
  $pecah = explode(',', $user['id_user_group']);
  $jmlh = count($pecah);
  for ($i=0; $i < $jmlh; $i++) { 
    $grup[] = $pecah[$i];
  }
?>

<div class="row">

  <div class="span12">                

      <div class="widget ">

        <div class="widget-header">

          <i class="icon-user"></i>

          <h3>Data Aktifitas Kuliah Mahasiswa</h3>

      </div> <!-- /widget-header -->      

      <div class="widget-content">

        <div class="span11">

        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>datas/aktifitas/save_session">

                        <fieldset>
                        <?php if ( (in_array(10, $grup)) or (in_array(1, $grup)) ) { ?>
                          <script>

                            $(document).ready(function(){

                              $('#faks').change(function(){

                                $.post('<?php echo base_url()?>datas/aktifitas/get_jurusan/'+$(this).val(),{},function(get){

                                  $('#jurs').html(get);

                                });

                              });

                            });

                            </script>

                              <div class="control-group">

                                <label class="control-label">Fakultas</label>

                                <div class="controls">

                                  <select class="form-control span6" name="fakultas" id="faks">

                                    <option>--Pilih Fakultas--</option>

                                    <?php $fakultas =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result(); foreach ($fakultas as $row) { ?>

                                    <option value="<?php echo $row->kd_fakultas.'-'.$row->fakultas;?>"><?php echo $row->fakultas;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div>

                              <div class="control-group">

                                <label class="control-label">Program Studi</label>

                                <div class="controls">

                                  <select class="form-control span6" name="jurusan" id="jurs">

                                    <option>--Pilih Program Studi--</option>

                                  </select>

                                </div>

                              </div>

                              <div class="control-group">

                                <label class="control-label">Tahun Akademik</label>

                                <div class="controls">

                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">

                                    <option>--Pilih Tahun Akademik--</option>

                                    <?php foreach ($tahunajar as $row) { ?>

                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div> 

                              <?php } elseif (in_array(9, $grup)) { ?>

                              <div class="control-group">

                                <label class="control-label">Program Studi</label>

                                <div class="controls">

                                  <select class="form-control span6" name="jurusan" id="jurs">

                                    <?php $prodi =$this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$nik,'kd_prodi','asc')->result(); foreach ($prodi as $row) { ?>

                                    <option value="<?php echo $row->kd_prodi.'-'.$row->prodi;?>"><?php echo $row->prodi;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div>

                              <div class="control-group">

                                <label class="control-label">Tahun Akademik</label>

                                <div class="controls">

                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">

                                    <option>--Pilih Tahun Akademik--</option>

                                    <?php foreach ($tahunajar as $row) { ?>

                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div> 

                              <?php } else { ?>

                              <div class="control-group">

                                <label class="control-label">Tahun Akademik</label>

                                <div class="controls">

                                  <select class="form-control span6" name="tahunajaran" id="tahunajaran">

                                    <option>--Pilih Tahun Akademik--</option>

                                    <?php foreach ($tahunajar as $row) { ?>

                                    <option value="<?php echo $row->kode;?>"><?php echo $row->tahun_akademik;?></option>

                                    <?php } ?>

                                  </select>

                                </div>

                              </div> 
                              <?php } ?> 

                            <br/>

                              

                            <div class="form-actions">

                                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 

                            </div> <!-- /form-actions -->

                        </fieldset>

                    </form>

        </div>

      </div>

    </div>

  </div>

</div>

