<script>
function edit(id){
$('#edit_bio').load('<?php echo base_url();?>datas/load_edit_bio/load/'+id);
}
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <?php $user = $this->session->userdata('sess_login'); if ($user['id_user_group'] == 1 || $user['id_user_group'] == 10) { ?>

                        <a href="<?php echo base_url(); ?>datas/mahasiswa" class="btn btn-warning"> << Kembali</a>

                    <?php } ?>
                    <a href="<?php echo base_url(); ?>data/mahasiswa/printdata" class="btn btn-success"><i class="btn-icon-only icon-print"> </i> Print Excel</a>
                    <br><hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>NPM</th>
                                <th>Nama Mahasiswa</th>
                                <th>Fakultas</th>
                                <th>Jurusan</th>
                                <th>Tahun Masuk</th>
                                <td>Status</td>
	                            <th width="80">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($getData as $value) {?>
	                        <tr>
	                        	<td><?php echo number_format($no); ?></td>
                                <td><?php echo $value->NIMHSMSMHS; ?></td>
                                <td><?php echo $value->NMMHSMSMHS; ?></td>
                                <?php $jurusan=$this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$value->KDPSTMSMHS,'kd_prodi','asc')->row(); 
                                $fakultas=$this->app_model->getdetail('tbl_fakultas','kd_fakultas',$jurusan->kd_fakultas,'kd_fakultas','asc')->row(); ?>
                                <td><?php echo $fakultas->fakultas; ?></td>
                                <td><?php echo $jurusan->prodi; ?></td>
	                        	<td><?php echo $value->TAHUNMSMHS; ?></td>
                                <?php switch ($value->STMHSMSMHS) {
                                    case 'A':
                                        $stts = 'AKTIF';
                                        break;
                                    case 'L':
                                        $stts = 'LULUS';
                                        break;
                                    case 'K':
                                        $stts = 'KELUAR';
                                        break;
                                    case 'C':
                                        $stts = 'CUTI';
                                        break;
                                    case 'D':
                                        $stts = 'DIKELUARKAN';
                                        break;
                                    default: 
                                        $stts = '';
                                        break;
                                } ?>
                                <td><?php echo $stts; ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-success btn-small" data-toggle="tooltip" data-placement="right" title="Riwayat Studi" href="#" ><i class="btn-icon-only icon-file-text"> </i></a>
                                    <?php 
										$logged = $this->session->userdata('sess_login');

										if ($logged['id_user_group'] == 10){ 
									?>
									<a onclick="edit(<?php echo $value->NIMHSMSMHS;?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
									<!-- <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="#"><i class="btn-icon-only icon-remove"> </i></a> -->
										<?php }?>
								</td>
	                        </tr>
                            <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Tahun Ajaran</h4>
            </div>
            <form class ='form-horizontal' action="#" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -30px;">  
									
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:50%;margin-right:150px;">
    <div class="modal-dialog">
        <div class="modal-content" id="edit_bio">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->