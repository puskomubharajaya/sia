<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_maba_s2.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr> 
            <th>No</th>
            <th>ID Registrasi</th>
            <th>Nama</th>
            <th>Program Pilihan</th>
            <th>Asal Universitas</th>
            <th>Kampus</th>
            <th>Uang Pendaftaran</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($look as $row) { //$a = $no;?>
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $row->ID_registrasi;?></td>
            <td><?php echo $row->nama;?></td>
            <td><?php echo $row->prodi;?></td>
            <td><?php echo $row->nm_univ;?></td>
            <?php if ($row->kampus == 'bks') {
                $kps = 'BEKASI';
            } else {
                $kps = 'JAKARTA';
            }
             ?>
            <td><?php echo $kps; ?></td>
            <td><?php $uang = 400000; echo $uang;?></td>
        </tr>
        <?php $no++; } ?>
        <tr>
            <td colspan="7" ></td>
            <td><?php // echo $uang*$a; ?></td>
        </tr>
        
    </tbody>
</table>