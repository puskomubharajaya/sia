<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(34)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
	}

	function index()
	{	
		$akademik['page'] = 'akademik/mk_view';
		$this->load->view('template/template',$akademik);
	}

	function view_mk(){
		$akademik['page'] = 'akademik/mk_lihat';
		$this->load->view('template/template',$akademik);
	}

}

/* End of file mk.php */
/* Location: ./application/modules/akademik/controllers/mahasiswa.php */