<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_ujian extends CI_Controller {

    protected $userid;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			echo '<script>alert("Sesi anda telah habis!")</script>';
			redirect(base_url('auth/logout'),'refresh');
		}
        $this->userid = $this->session->userdata('sess_login')['userid'];
		$this->load->model('ujian_model');
		$this->load->model('hilman_model');
        error_reporting(0);
	}

	public function index()
	{
        $this->session->unset_userdata('sessjadwaluji');
        $this->session->unset_userdata('sessjdlbaa');

		$sess = $this->session->userdata('sess_login');
		$data['grup'] = $sess['id_user_group'];

		// if user as BAA
		if ($sess['id_user_group'] == 10) {
			$data['prod'] = $this->app_model->getdata('tbl_jurusan_prodi','kd_prodi','asc')->result();	
		}
		// end if user as BAA

		$data['date'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();
		$data['page'] = "v_jadwalujian";
		$this->load->view('template/template', $data);
	}

    public function exam_deadline()
    {
        $data['tahunajaran'] = $this->db->get('tbl_tahunakademik')->result();
        $data['data'] = $this->ujian_model->exam_deadline();
        $data['page'] = "exam_deadline_v";
        $this->load->view('template/template', $data);
    }

    public function store_exam_deadline()
    {
        extract(PopulateForm());

        if (empty($is_updated)) {
            $data = [
                'tahunajaran'       => $tahunajaran,
                'tipe_uji'          => $tipe,
                'deadline_uji'      => $deadline,
                'deadline_interval' => $interval,
                'deadline_scoring'  => $this->_set_deadline_scoring($deadline, $interval),
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => $this->userid        
            ];
            $this->db->insert('tbl_deadline_uji', $data);

            $this->_update_end_date($tahunajaran, $tipe, $data['deadline_scoring']);

            echo '<script>alert("Berhasil manambahkan data!")</script>';
            redirect(base_url('akademik/jadwal_ujian/exam_deadline'),'refresh');
        } else {
            $data = [
                'tahunajaran'       => $tahunajaran,
                'tipe_uji'          => $tipe,
                'deadline_uji'      => $deadline,
                'deadline_interval' => $interval,
                'deadline_scoring'  => $this->_set_deadline_scoring($deadline, $interval),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => $this->userid,
            ];
            $this->db->where('id', $is_updated);
            $this->db->update('tbl_deadline_uji', $data);

            $this->_update_end_date($tahunajaran, $tipe, $data['deadline_scoring']);

            echo '<script>alert("Berhasil mengubah data!")</script>';
            redirect(base_url('akademik/jadwal_ujian/exam_deadline'),'refresh');
        }
    }

    private function _update_end_date($tahunajaran, $tipe_uji, $end_date)
    {
        $this->db->where('tahunakademik', $tahunajaran);
        $this->db->where('tipe_uji', $tipe_uji);
        $this->db->update('tbl_jadwaluji', ['end_date' =>  $end_date]);
        return;
    }

    public function get_deadline($id)
    {
        $data = $this->db->get_where('tbl_deadline_uji', ['id' => $id])->row();
        $response = [
            'tahunajaran' => $data->tahunajaran,
            'tipe'        => $data->tipe_uji,
            'deadline'    => $data->deadline_uji,
            'interval'    => $data->deadline_interval
        ];
        echo json_encode($response);
    }

    public function remove_deadline($id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_deadline_uji', ['deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => $this->userid]);
        echo '<script>alert("Berhasil menghapus data!")</script>';
        redirect(base_url('akademik/jadwal_ujian/exam_deadline'),'refresh');
    }

	public function save_sess()
	{
		$userlog = $this->session->userdata('sess_login');

		// if login as BAA
		if ($userlog['id_user_group'] == 10) {
			$prod = $this->input->post('prodi', TRUE);
			$this->session->set_userdata('sessjdlbaa',$prod);	
		}

		$date = $this->input->post('year', TRUE);

		$array = array(
			'akad' => $date
		);
		
		$this->session->set_userdata('sessjadwaluji',$array);

        // if login as lecture
        if ($userlog['id_user_group'] != 10 && $userlog['id_user_group'] != 8) {
            redirect(base_url('akademik/jadwal_ujian/loaddatadosen'));
        } else {
            redirect(base_url('akademik/jadwal_ujian/loaddata'));
        }
	}

    public function loaddatadosen()
    {
        $log = $this->session->userdata('sess_login');
        $uid = $log['userid'];
        $sesijadwal   = $this->session->userdata('sessjadwaluji');
        $data['year'] = $sesijadwal['akad'];
        $data['list'] = $this->ujian_model->loadByDosen($sesijadwal['akad'],$uid);
        $data['page'] = "v_loadjadwaluji";
        $this->load->view('template/template', $data);
    }

	public function loaddata()
	{
        $sess = $this->session->userdata('sess_login');

        if ($sess['id_user_group'] == 10) {
            $uid = $this->session->userdata('sessjdlbaa');
        } else {
            $uid = $sess['userid'];    
        }
        
        $data['uid'] = $uid;
		$sesijadwal   = $this->session->userdata('sessjadwaluji');
		$data['year'] = $sesijadwal['akad'];
		$data['page'] = "v_loadjadwaluji";
        $data['list'] = $this->ujian_model->loaduji($sesijadwal['akad'],$uid);
		$this->load->view('template/template', $data);
	}

	public function loaddosen()
	{
		$this->db->distinct();
        $this->db->select("*");
        $this->db->from('tbl_karyawan');
        $this->db->like('nama', $_GET['term'], 'both');
        $this->db->or_like('nid', $_GET['term'], 'both');
        $sql  = $this->db->get();
        
        $data = array();
        foreach ($sql->result() as $row) {
            $data[] = array(
                'dosen' => $row->nid,
                'value' => $row->nid.' - '.$row->nama
            );
        }
        echo json_encode($data);
    }

    public function loadroom()
	{
		$this->db->distinct();
        $this->db->select("*");
        $this->db->from('tbl_ruangan');
        $this->db->like('ruangan', $_GET['term'], 'both');
        $this->db->or_like('kode_ruangan', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();
        foreach ($sql->result() as $row) {
            $data[] = array(
                'idroom' => $row->id_ruangan,
                'ruang'  => $row->kode_ruangan,
                'value'  => $row->kode_ruangan.' - '.$row->ruangan
            );
        }
        echo json_encode($data);
    }

    public function addmodal($id)
    {
    	$jdl = $this->session->userdata('sessjadwaluji');
    	$data['data'] = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$id,'id_jadwal','asc')->row();

        // kondisi untuk cek ketersediaan data uts atau uas
        $uts = array('id_jadwal' => $id, 'tipe_uji' => 1);
        $uas = array('id_jadwal' => $id, 'tipe_uji' => 2);
        $data['cek1'] = $this->hilman_model->moreLike($uts, 'tbl_jadwaluji', 'after')->num_rows();
        $data['cek2'] = $this->hilman_model->moreLike($uas, 'tbl_jadwaluji', 'after')->num_rows();
    	
    	$data['akad'] = $jdl['akad'];
    	$this->load->view('modal_addjdluji', $data);
    }

    public function addjadwal()
    {
    	extract(PopulateForm());

        $prods   = substr($prodi, 0,5);
        $uid     = $this->userid;
        $strdate = ymdDate($start);
        $endate  = $this->_get_end_date($tahun, $tipe);

        $arr['id_jadwal']     = $id;
        $arr['kd_jadwal']     = $prodi;
        $arr['prodi']         = $prods;
        $arr['start_date']    = $strdate;
        $arr['end_date']      = $endate;
        $arr['tipe_uji']      = $tipe;
        $arr['tahunakademik'] = $tahun;
        $arr['userinput']     = $uid;
        $arr['kd_ruang']      = $hiddenroom;
        // $arr['pengawas']   = $hiddenguide;
        $arr['dateinput']     = date('Y-m-d');
        $arr['start_time']    = $starttime;

        $this->app_model->insertdata('tbl_jadwaluji', $arr);
        redirect(base_url('akademik/jadwal_ujian/loaddata'),'refresh');
    }

    public function detmodal($id)
    {
    	$data['uts'] = $this->ujian_model->getDetailJdl($id,1);
    	$data['uas'] = $this->ujian_model->getDetailJdl($id,2);
    	$this->load->view('modal_dtljdl', $data);
    }

    public function edtmodal($id)
    {
    	$data['uts'] = $this->ujian_model->getDetailJdl($id,1);
    	$data['uas'] = $this->ujian_model->getDetailJdl($id,2);
    	$this->load->view('modal_edtjdl', $data);
    }

    public function changeDataUts()
    {
    	extract(PopulateForm());

        $uid     = $this->userid;
        $strdate = $tgl;
        $endate  = $this->_get_end_date($tahunajaran, 1);
        $data    = array('id_jadwal' => $id, 'tipe_uji' => $typ);

        $arr['start_date']  = $strdate;
        $arr['end_date']    = $endate;
        $arr['userinput']   = $uid;
        $arr['kd_ruang']    = $hiddenrooms;
        $arr['start_time']  = $starttimeuts;
        // $arr['pengawas'] = $hiddenwatch;
        $arr['dateinput']   = date('Y-m-d');

        $this->hilman_model->updateMoreParams($data, 'tbl_jadwaluji', $arr);
        redirect(base_url('akademik/jadwal_ujian/loaddata'),'refresh');
    }

    public function changeDataUas()
    {
    	extract(PopulateForm());
        $uid     = $this->userid;
        $strdate = $uastgl;
        $endate  = $this->_get_end_date($tahunajaran, 2);
        $data    = array('id_jadwal' => $uasid, 'tipe_uji' => $uastyp);

        $arr['start_date'] = $strdate;
        $arr['end_date']   = $endate;
        $arr['userinput']  = $uid;
        $arr['kd_ruang']   = $uashiddenroom;
        $arr['start_time'] = $starttimeuas;
        $arr['pengawas']   = $uashiddenwatch;
        $arr['dateinput']  = date('Y-m-d');

        $this->hilman_model->updateMoreParams($data, 'tbl_jadwaluji', $arr);
        redirect(base_url('akademik/jadwal_ujian/loaddata'),'refresh');
    }

    private function _set_deadline_scoring($end_date, $interval)
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = new DateTime($end_date);
        $date->add(new DateInterval('P'.$interval.'D'));
        return $date->format('Y-m-d');
    }

    private function _get_end_date($tahunajaran, $tipe_uji)
    {
        $this->db->where('tahunajaran', $tahunajaran);
        $this->db->where('tipe_uji', $tipe_uji);
        $this->db->where('deleted_at');
        $data  = $this->db->get('tbl_deadline_uji')->row()->deadline_scoring;
        return $data;
    }

    public function exportxls()
    {
        $year   = $this->session->userdata('sessjadwaluji');
        $uid    = $this->session->userdata('sess_login');
        $type   = $this->input->post('tipe');
        $data['tipe'] = $type;
        $data['akad'] = $year['akad'];
        $data['user'] = $uid['userid'];

        if ($uid['id_user_group'] == 10 or $uid['id_user_group'] == 8) {
            $data['load'] = $this->ujian_model->export($year['akad'],$uid['userid'],$type);
        } else {
            $data['load'] = $this->ujian_model->export_byDosen($year['akad'],$uid['userid'],$type);
        }
        
        $this->load->view('xls_jadwaluji', $data);
    }

    public function whoseHavent()
    {
        $data['user'] = $this->app_model->getdata('tbl_jurusan_prodi','kd_prodi','asc')->result();
        $data['page'] = "v_belumupload";
        $this->load->view('template/template', $data);
    }

}

/* End of file Jadwal_ujian.php */
/* Location: .//tmp/fz3temp-1/Jadwal_ujian.php */