<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubah_kaprodi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->output->cache(60);
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			/* $cekakses = $this->role_model->cekakses(71)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			} */
		} else {
			redirect('auth','refresh');
		}
	} 

	function index()
	{	
	
		$user = $this->session->userdata('sess_login');

		$prodi   = $user['userid'];
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		

		if (in_array(19, $grup)) {
	    	    	
			$data['kaprodi']=$this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$prodi,'kd_prodi','asc',1)->row()->kaprodi;
	        $data['page']='ganti_kaprodi';

	        $this->load->view('template/template', $data);

		}else{
			redirect(base_url());
		}
	}
	function load_dosen_autocomplete(){

		$bro = $this->session->userdata('sess_login');

		$prodi = $bro['userid'];

        $this->db->distinct();

        $this->db->select("nid,nama");

        $this->db->from('tbl_karyawan');

        $this->db->like('nama', $_GET['term'], 'both');

        $this->db->or_like('nid', $_GET['term'], 'both');

        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {

            $data[] = array(

                            'value' => $row->nid.' - '.$row->nama

                            );

        }

        echo json_encode($data);

    }
	function ubah(){
		$user 	= $this->session->userdata('sess_login');
		$prodi  = $user['userid'];
		$ini 	= $this->input->post('nid');
		$pecah 	= explode(' - ', $ini);
		$cek 	= $this->db->query('SELECT * from tbl_karyawan where nid = "'.$pecah[0].'"')->row();
		if ($cek == FALSE) {
			echo "<script>alert('Data Tidak Ditemukan! Periksa User ID Pengguna!');document.location.href='".base_url()."akademik/ubah_kaprodi';</script>";
		} else {
			$data = array(
					'kaprodi'			=> $pecah[0]
				);
			// echo "<pre>";
			// print_r($prodi);
			// exit;
				$this->app_model->updatedata('tbl_jurusan_prodi','kd_prodi',$prodi,$data);
				
				echo "<script>alert('Sukses');document.location.href='".base_url()."akademik/ubah_kaprodi';</script>";
		}

    }

}

/* End of file Ajar.php */
/* Location: .//C/Users/danum246/AppData/Local/Temp/fz3temp-1/Ajar.php */