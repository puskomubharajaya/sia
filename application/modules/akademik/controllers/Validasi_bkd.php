<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Kodevikasi 
 *
 *	NULL => Dosen baru mengajukan
 *	1    => Approve By GPM (Gugus Penjamin Mutu) Tingkatan prodi
 *	2    => Approve BY Spm (Sistem penjamin mutu) Tingkatan fakultas
 *	3    => Approve By Warek I 
 *	4    => Revisi By GPM
 *	5    => Revisi By SPM
 *	6    => Revisi By Warek I 
 *	9	 => Dosen Sudah Merevisi
*/

error_reporting(-1);

class Validasi_bkd extends CI_Controller {

	public $path = "akademik/validasi_bkd";
	public $userid;
	public $username;
	public $userGroup;

	public function __construct()
	{
		parent::__construct();

		// if (!$this->session->userdata('sess_login')) {
		// 	echo '<script>alert("Sesi Anda telah habis. Silahkan Log-in kembali!")</script>';
		// 	redirect('auth','refresh');
		// }

		$this->db->db_debug = TRUE;
		$this->load->model('My_model', 'data');

		// set userid
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->username = $this->session->userdata('sess_login')['username'];
		$this->userGroup = $this->session->userdata('sess_login')['id_user_group'];
	}

	public function index()
	{
		$userID = $this->userid;
		$condition = "";
		switch ($this->userGroup) {
			case '21':
				$condition = "WHERE user IN (SELECT nid from tbl_karyawan where jabatan_id = $userID)";
				break;
			case '22':
				$condition = "WHERE user IN (SELECT 
													nid 
											FROM tbl_karyawan 
												WHERE 
													jabatan_id IN (
														SELECT kd_prodi 
															FROM tbl_jurusan_prodi 
																WHERE kd_fakultas = $userID)
											) and status_id in (1,2,5,3,6)";
				break;
			case '9':
				$condition = "WHERE user IN (SELECT 
													nid 
											FROM tbl_karyawan 
												WHERE 
													jabatan_id IN (
														SELECT kd_prodi 
															FROM tbl_jurusan_prodi 
																WHERE kd_fakultas = $userID)
											) and status_id in (1,2,5,3,6)";
				break;
			case '23':
				$condition = '';
		}

		$list = $this->data->raw('SELECT * FROM v_lecturer_research '.$condition)->result();

		$data['user'] = $this->userGroup;
		$data['title'] = 'Data Beban Kinerja Dosen';
		$data['list']  = $list;
		$data['page']  = $this->path.'/home';

		$this->load->view('template/template', $data);
	}

	/**
	 * Modal untuk  revisi
	 * @param  int  $id 	id dari tbl_lecturer_research
	 * @return HTML 		modal revisi
	 */
	function revisi_modal($type,$id='')
	{
		$this->load->view($this->path.'/revisi_modal',['id' => $id, 'type' => $type]);
	}

	/**
	 * Revisi BKD
	 * @param int $lrid $_POST['lrid']
	 * @param int $note $_POST['note']
	 * @return alert 
	 */
	function revisi()
	{
		$type = $this->input->post('type', TRUE);
		switch ($type) {
			case '1': // Pengajaran/ Pendidikan
				$key       = 'id_pengajaran';
				$tableName = 'tbl_pengajaran_tambahan_dosen';
				$tableLog  = 'pengajaran';
				$message   = 'Revisi Pengajaran';
				break;
			case '2': // Penelitian
				$key       = 'id_penelitian';
				$tableName = 'tbl_lecturer_research';
				$tableLog  = 'penelitian';
				$message   = 'Revisi Penelitian';
				break;
			case '3': // Pengabdian
				$key       = 'id_pengabdian';
				$tableName = 'tbl_abdimas_dosen';
				$tableLog  = 'penelitian';
				$message   = 'Revisi Pengabdian';
				break;
			case '4': // Lain2
				$key       = 'id_lain2';
				$tableName = 'tbl_abdimas_dosen';
				$tableLog  = 'penelitian';
				$message   = 'Revisi Tugas Tambahan';
				break;
		}

		
		$penelitianID = $this->input->post('lrid', TRUE);

		$status = approval_status_bkd(2,$this->userGroup);
		
		$dataInsert = [
			$key => $penelitianID,
			'status'        => $status,
			'note'          => $this->input->post('note', TRUE),
			'auditor'       => $this->username,
			'audit_date'    => date('Y-m-d H:i:s'),
		];

		$this->data->update($tableName, ['status' => $status], ['id' => $penelitianID]);
		$affected = $this->db->affected_rows();

		// jika update sukses
		if ($affected > 0) {
			$this->data->insert('tbl_riwayat_pengesahan_'.$tableLog, $dataInsert);
			$insertID = $this->db->insert_id();

			if ($insertID > 0) {
				echo "<script>alert('".$message." Sukses');
				document.location.href='".base_url()."akademik/validasi-bkd';</script>";
			}
		}else{
			echo "<script>alert('".$message." Gagal');
				document.location.href='".base_url()."akademik/validasi-bkd';</script>";
		}
	}

	/**
	 * Approval BKD
	 * @param int $id id dari primary key
	 * @return alert 
	 */
	function approve($type,$id){

		switch ($type) {
			case '1': // Pengajaran/ Pendidikan
				$key       = 'id_pengajaran';
				$tableName = 'tbl_pengajaran_tambahan_dosen';
				$tableLog  = 'pengajaran';
				$message   = 'Pengesahan Pengajaran';
				break;
			case '2': // Penelitian
				$key       = 'id_penelitian';
				$tableName = 'tbl_lecturer_research';
				$tableLog  = 'penelitian';
				$message   = 'Pengesahan Penelitian';
				break;
			case '3': // Pengabdian
				$key       = 'id_pengabdian';
				$tableName = 'tbl_abdimas_dosen';
				$tableLog  = 'penelitian';
				$message   = 'Pengesahan Pengabdian';
				break;
			case '4': // Lain2
				$key       = 'id_lain2';
				$tableName = 'tbl_abdimas_dosen';
				$tableLog  = 'penelitian';
				$message   = 'Pengesahan Tugas Tambahan';
				break;
		}

		$status = approval_status_bkd(1,$this->userGroup);

		$dataInsert = [
			$key => $id,
			'status'        => $status,
			'auditor'       => $this->username,
			'audit_date'    => date('Y-m-d H:i:s'),
		];

		$this->data->update($tableName, ['status' => $status], ['id' => $id]);
		
		$affected = $this->db->affected_rows();

		// jika update sukses
		if ($affected > 0) {
			$this->data->insert('tbl_riwayat_pengesahan_'.$tableLog, $dataInsert);
			$insertID = $this->db->insert_id();

			if ($insertID > 0) {
				echo "<script>alert('".$message." Sukses');
				document.location.href='".base_url()."akademik/validasi-bkd';</script>";
			}
		}else{
			echo "<script>alert('".$message." Gagal');
				document.location.href='".base_url()."akademik/validasi-bkd';</script>";
		}
	}

	public function list_tugas($nid="")
	{
		$this->load->model('bkd_model', 'bkd');
		
		$this->userid     = $nid;

		$nama = nama_dsn($nid);
		$data['nama'] = $nama;
		
		$tahunAjar        = getactyear();
		$data['semester'] = $this->db->get_where('tbl_tahunakademik', ['kode' => $tahunAjar])->row()->tahun_akademik;
		$data['aditional'] = $this->bkd->additional_teaching_list($this->userid, $tahunAjar);
		$data['research']  = $this->bkd->research_list_by_year($this->userid, $tahunAjar);
		$data['devotion']  = $this->bkd->get_lecturer_devotion($this->userid, $tahunAjar);
		$data['others']    = $this->bkd->get_struktural_dosen($this->userid, $tahunAjar);

		$data['totalData'] = count($data['aditional']) + count($data['research']) + count($data['devotion']) + count($data['others']);
		$data['userGroup'] = $this->userGroup;
		
		$this->load->view($this->path.'/list_tugas_modal', $data);
	}
}

/* End of file Validasi_bkd.php */
/* Location: ./application/controllers/Validasi_bkd.php */