<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perbaikan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->load->model('setting_model');
		$this->load->model('temph_model');
		$this->load->library('Cfpdf');
		if ($this->session->userdata('sess_login') == TRUE) {
			$user = $this->session->userdata('sess_login');
			$akses = $this->role_model->cekakses(151)->result();
			if ($akses != TRUE) {
				echo "<script>alert('Akses Tidak Diizinkan');document.location.href='" . base_url() . "home';</script>";
			}
		} else {
			redirect('auth', 'refresh');
		}
	}

	public function index()
	{
		$this->session->unset_userdata('ta');
		$data['select'] = $this->db->where('kode =', getactyear())->get('tbl_tahunakademik')->result();
		$data['page'] = 'validasi_select';
		$this->load->view('template/template', $data);
	}

	public function session_create()
	{
		$tahun = $this->input->post('tahunajaran');
		$this->session->set_userdata('ta', $tahun);
		redirect(base_url() . 'akademik/perbaikan/kls_perbaikan', 'refresh');
	}

	public function kls_perbaikan()
	{
		$actyear 	=  $this->session->userdata('ta');
		$log = $this->session->userdata('sess_login');

		$kdkrs = $log['userid'] . $actyear;
		$cek_available = $this->temph_model->cek_verif_krs($kdkrs)->num_rows();
		if ($cek_available == 0) {
			$data['filter'] = $this->temph_model->get_mksp($log['userid'])->result(); 
			$data['hitung'] = count($data['filter']); 
			$data['nim']    = $log['userid']; 
			$data['page']   = "v_mhs_mksp";
			$this->load->view('template/template', $data);
		} else {
			redirect(base_url('akademik/perbaikan/show_krs_sp'));
		}
	}

	public function addkrs()
	{
		$log  = $this->session->userdata('sess_login');
		$npm  = $log['userid'];
		$kdjd = $this->input->post('kdjdl');
		$this->load->model('Model_krs_sp');
		$mk = $this->Model_krs_sp->cekmk($kdjd);
		foreach ($mk as $row) {
			$ini[] = $row->kd_matakuliah;
		}
		if (count($ini) != count(array_unique($ini))) {
			echo "<script>alert('Silahkan pilih 1 jadwal dari 1 matakuliah');document.location.href='" . base_url('akademik/perbaikan') . "';</script>";
		} else {
			$this->temph_model->add_krs($npm, $kdjd);
			redirect(base_url('akademik/perbaikan/show_krs_sp'));
		}
	}

	public function show_krs_sp()
	{
		$yearimprove = $this->session->userdata('ta');

		$log = $this->session->userdata('sess_login');
		$data['filter'] = $this->temph_model->list_krs_sp($log['userid'])->result();
		$data['krs'] = $this->temph_model->list_krs_sp($log['userid'])->row();

		if (is_null($data['krs'])) {
			echo "<script>alert('Tidak Ada Data Terinput !');document.location.href='" . base_url('akademik/perbaikan') . "';</script>";
		} else {
			$select = $this->db->where('status', 1)->get('tbl_tahunakademik')->result();
			foreach ($select as $key) {
				$ta = substr($key->kode, -1);
				if ($ta == '1') {
					$ta = substr($key->kode, 0, -1);
					$ta_sp = $ta . '3';
				} elseif ($ta == '2') {
					$ta = substr($key->kode, 0, -1);
					$ta_sp = $ta . '4';
				}
			}
			$thn = $this->temph_model->cek_thsp($log['userid']);
			$data['bayar'] 	= $this->db->query("SELECT COUNT(npm_mahasiswa) AS byr FROM tbl_sinkronisasi_renkeu 
												WHERE npm_mahasiswa = '{$log['userid']}' 
												AND tahunajaran = '{$yearimprove}' 
												AND status = '1'")->row()->byr;

			$data['page'] = "v_krs_sp";
			$this->load->view('template/template', $data);
		}
	}

	public function ubah($krs, $id)
	{
		$log = $this->session->userdata('sess_login');
		$data['nim'] = $log['userid'];
		$data['filter'] = $this->temph_model->change_krs($krs)->result();

		foreach ($data['filter'] as $gigs) {
			$dats[] = $gigs->id_jadwal;
		}

		$data['unselectedmk'] = $this->temph_model->getMkSpWhenEdit($log['userid'], $dats)->result();

		if (count($data['filter']) == 0) {
			echo "<script>alert('Data KRS Tidak Ditemukan!');document.location.href='" . base_url('akademik/perbaikan') . "';</script>";
		} else {
			$data['krs'] = $krs;
			$data['idjd'] = get_kd_jdl_remid($id);
			$data['page'] = "v_mhs_mksp_edit";
			$this->load->view('template/template', $data);
		}
	}

	public function del_mksp($kd)
	{
		$this->db->where('status', 1);
		$hey = $this->db->get('tbl_tahunakademik')->row();
		$fixyear = $hey->kode + 2;

		$this->db->where('kd_krs', $kd)->delete('tbl_verifikasi_krs_sp');
		$this->db->where('kd_krs', $kd)->delete('tbl_krs_sp');

		$this->db->where('npm_mahasiswa', substr($kd, 0, 12));
		$this->db->where('tahunajaran', $fixyear);
		$huu = $this->db->get('tbl_sinkronisasi_renkeu')->result();

		if ($huu) {
			$this->db->where('npm_mahasiswa', substr($kd, 0, 12));
			$this->db->where('tahunajaran', $fixyear);
			$this->db->delete('tbl_sinkronisasi_renkeu');
		}

		echo "<script>alert('Data Matakuliah Terhapus!');document.location.href='" . base_url('akademik/perbaikan') . "';</script>";
	}

	public function print_formulir($kode)
	{
		$this->load->library('Cfpdf');
		$log = $this->session->userdata('sess_login');


		$data['prodi'] = get_mhs_jur($log['userid']);
		$ta = $this->db->where('status', 1)->get('tbl_tahunakademik')->row();

		$data['tahun'] = substr($ta->tahun_akademik, 0, 9);
		$data['smtr'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', substr($kode, 0, 12), 'NIMHSMSMHS', 'asc')->row();

		$npm = substr($kode, 0, 12);

		$a = substr($kode, 16, 1);

		if ($a = 3) {
			$data['ganjilgenap'] = 'Ganjil';
		} elseif ($a = 4) {
			$data['ganjilgenap'] = 'Genap';
		}

		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,a.KDPSTMSMHS,b.prodi');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
		$this->db->where('NIMHSMSMHS', $npm);
		$data['info_mhs'] = $this->db->get()->row();

		$this->db->where('npm_mahasiswa', $npm);
		$this->db->order_by('id_verifikasi', 'desc');
		$dsn = $this->db->get('tbl_verifikasi_krs', 1)->row()->id_pembimbing;

		$this->db->where('nid', $dsn);
		$data['dosen'] = $this->db->get('tbl_karyawan', 1)->row();

		$qq = $this->app_model->get_KDPSTMSMHS($npm)->row();

		$data['matkul'] = $this->db->query("SELECT DISTINCT * FROM tbl_krs_sp b 
											JOIN tbl_verifikasi_krs_sp a ON a.kd_krs = b.kd_krs
									        JOIN tbl_matakuliah c ON b.kd_matakuliah = c.kd_matakuliah
									        JOIN tbl_jadwal_matkul_sp d ON d.kd_jadwal = b.kd_jadwal
									        JOIN tbl_karyawan e ON e.nid = d.kd_dosen 
       										WHERE b.kd_krs = '{$kode}' 
       										AND d.`open` = 1 
       										AND c.kd_prodi = '{$qq->KDPSTMSMHS}' ")->result();

		$this->load->view('akademik/espe_formulir', $data);
	}

	public function print_sp($kode)
	{
		$this->load->library('Cfpdf');
		$log = $this->session->userdata('sess_login');

		$data['prodi'] = get_mhs_jur($log['userid']);

		$data['tahun'] = getactyear();
		$data['smtr'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', substr($kode, 0, 12), 'NIMHSMSMHS', 'asc')->row();
		$a = substr($kode, 16, 1);

		if ($a = 3) {
			$data['ganjilgenap'] = 'Ganjil';
		} elseif ($a = 4) {
			$data['ganjilgenap'] = 'Genap';
		}

		$npm = substr($kode, 0, 12);

		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.KDPSTMSMHS,a.TAHUNMSMHS,a.KDPSTMSMHS,b.prodi');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_jurusan_prodi b', 'a.KDPSTMSMHS = b.kd_prodi');
		$this->db->where('NIMHSMSMHS', $npm);
		$data['info_mhs'] = $this->db->get()->row();

		$this->db->where('npm_mahasiswa', $npm);
		$this->db->order_by('id_verifikasi', 'desc');
		$dsn = $this->db->get('tbl_verifikasi_krs', 1)->row()->id_pembimbing;

		$this->db->where('nid', $dsn);
		$data['dosen'] = $this->db->get('tbl_karyawan', 1)->row();

		$data['matkul'] = $this->db->query("SELECT DISTINCT * FROM tbl_krs_sp b 
											JOIN tbl_verifikasi_krs_sp a ON a.kd_krs = b.kd_krs
											JOIN tbl_matakuliah c ON b.kd_matakuliah = c.kd_matakuliah
											JOIN tbl_jadwal_matkul_sp d ON d.kd_jadwal = b.kd_jadwal
											JOIN tbl_karyawan e ON e.nid = d.kd_dosen 
											WHERE b.kd_krs = '{$kode}' 
											AND d.open = 1 
											AND c.kd_prodi = '".get_mhs_jur($log['userid'])."' ")->result();

		$this->load->view('akademik/espe', $data);
	}

	public function khs_sp()
	{
		$logged = $this->session->userdata('sess_login');
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $logged['userid'], 'NIMHSMSMHS', 'asc')->row();
		$data['detail_khs'] = $this->temph_model->get_all_khs_mahasiswa($logged['userid'])->result();
		$data['page'] = "v_khs_sp";
		$this->load->view('template/template', $data);
	}

	public function detailkhs($npm, $id)
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		if ((in_array(5, $grup))) {
			$nim = $logged['userid'];
		} else {
			$nim = $npm;
		}
		$data['npm'] = $nim;

		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $nim, 'NIMHSMSMHS', 'asc')->row();
		$data['semester'] = $a = $this->app_model->get_semester_khs($data['mhs']->SMAWLMSMHS, $id);

		$data['tahunakademik'] = $id;

		if ($id < '20151') {
			$data['detail_khs'] = $this->db->query("SELECT DISTINCT b.kd_matakuliah,b.nama_matakuliah,b.sks_matakuliah 
													FROM tbl_transaksi_nilai_sp a 
													JOIN tbl_matakuliah_copy b ON a.KDKMKTRLNM = b.kd_matakuliah 
													WHERE a.THSMSTRLNM = '{$id}' 
													AND b.tahunakademik = '{$id}' 
													AND a.NIMHSTRLNM = '{$nim}' 
													AND b.kd_prodi = '{$data['mhs']->KDPSTMSMHS}' ")->result();

			$data['kode'] = $this->db->query("SELECT DISTINCT kd_krs FROM tbl_krs WHERE kd_krs LIKE '".$nim.$id."%'")->row();

		} else {
			$data['detail_khs'] = $this->db->query("SELECT DISTINCT a.*,b.nama_matakuliah,b.sks_matakuliah FROM tbl_krs_sp a 
													JOIN tbl_matakuliah b ON a.kd_matakuliah = b.kd_matakuliah 
													WHERE a.kd_krs LIKE '".$nim.$id."%' 
													AND b.kd_prodi = '{$data['mhs']->KDPSTMSMHS}'")->result();

			$data['kode'] = $this->db->query("SELECT DISTINCT kd_krs FROM tbl_krs_sp WHERE kd_krs LIKE '".$nim.$id."%'")->row();
		}

		$logged = $this->session->userdata('sess_login');

		$data['page'] = 'akademik/detail_khs_sp';
		$this->load->view('template/template', $data);
	}
}

/* End of file Perbaikan.php */
/* Location: ./application/modules/akademik/controllers/Perbaikan.php */
