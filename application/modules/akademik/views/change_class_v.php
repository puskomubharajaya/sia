<form id="result_kls" class="form-horizontal" method="post" action="<?php echo base_url(); ?>akademik/kelas/save">
    <div class="control-group">
        <label class="control-label">Nama :</label>
        <div class="controls">
            <input type="text" value="<?php echo $nama ?>" class="form-control" disabled />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Kelas : </label>
        <div class="controls">
            <input type="text" value="<?php echo classType($kelas) ?>" class="form-control" disabled />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Kelas Baru</label>
        <div class="controls">
            <select class="form-control span3" name="kls" required>
                <option value="" disabled hidden selected>-- Pilih Kelas --</option>
                <option value="PG">A</option>
                <option value="SR">B</option>
                <option value="PK">C</option>
            </select>
        </div>
    </div>
    <input type="hidden" value="<?php echo $npm ?>" name="npm" />
    <div class="form-actions">
        <input type="submit" class="btn btn-primary" id="save" value="Submit" />
    </div> <!-- /form-actions -->
</form>