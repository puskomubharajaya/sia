<h3>
	<i class="icon-copy"></i> Tugas Tambahan Dosen Tahun Akademik <?= get_thajar(getactyear()); ?>
	<span data-toggle="tooltip" title="lihat rincian lain-lain">
		<a href="#otherInfo" data-toggle="modal" onclick="load_structural_position()">
			<i class="icon-eye-open"></i>
		</a>
	</span>
</h3>
<br>
<form class="form-horizontal" action="<?= base_url('akademik/beban_kinerja_dosen/store_other_data') ?>" method="post">
	<div class="control-group">
		<label for="position" class="control-label">Jabatan Struktural</label>
		<div class="controls">
			<select name="position" id="position" class="form-control span4">
				<option disabled="" selected="" value="">-- Pilih Jabatan --</option>
				<?php foreach ($others as $other) { ?>
					<option value="<?= $other->id.'-'.$other->sks ?>"><?= $other->jabatan ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<label for="sks" class="control-label">Beban SKS</label>
		<div class="controls">
			<input type="text" name="sks" readonly="" value="" id="sks" class="form-control span1" placeholder="SKS">
		</div>
	</div>
	
	<div class="form-actions">
		<button class="btn btn-success" type="submit"><i class="icon icon-plus"></i> Tambah</button>
	</div>
</form>

<div id="otherInfo" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Informasi Jabatan Struktural</h4>
			</div>
			<div class="modal-body">
				<p id="textContent"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();

		// set sks to input box
		$('#position').change(function() {
			var credits = $(this).val().split("-")[1];
			$('#sks').val(credits);
		})
	})

	function load_structural_position() {
		$.get('<?= base_url('akademik/beban_kinerja_dosen/load_structural_data') ?>', function(response) {
			var dataParse = JSON.parse(response);
			$('#textContent').text(`Sebagai ${dataParse.jabatan}, pada tahun akademik ${dataParse.tahunakademik}`);
			$('#textContent').append(`
				<a 
					class="btn btn-default pull-right" 
					onclick="return confirm('Yakin ingin menghapus data ini?')"
					href="<?= base_url('akademik/beban_kinerja_dosen/remove_structural') ?>" 
					data-toggle="tooltip" 
					title="hapus tugas tambahan">
					<i class="icon-trash icon-1x"></i>
				</a>`)
		})
	}
</script>