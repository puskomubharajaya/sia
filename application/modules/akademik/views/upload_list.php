<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Dosen Mengajar</h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">

                   <a href="#" class="btn btn-success "><i class="btn-icon-only icon-print"> </i> Export Excel</a><hr>

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th>No</th>

                                <th>NIP</th>

                                <th>Nama Dosen</th>

                                <th>Total Sks</th>

                                <th width="80">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no=1; foreach ($dosen as $isi): ?>

                                <tr>

                                    <td><?php echo $no; ?></td>

                                    <td><?php echo $isi->nid; ?></td>

                                    <td><?php echo $isi->nama; ?></td>

                                    <td><?php echo $isi->sks; ?></td>

                                    <td class="td-actions">
									
									<center>
									
                                       <a target="blank" href="<?php echo base_url();?>akademik/upload_soal/show_dokumen_dosen/<?php echo $isi->nid; ?>" class="btn btn-primary btn-small"><i class="btn-icon-only icon-ok"> </i></a>
									
									</center>

                                    </td>

                                </tr>    

                            <?php $no++; endforeach ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>