<?php 
	$team = $this->session->userdata('research');
	$c    = $this->session->userdata('jml_array');
	$no   = 1;
	for ($i=0; $i < $c; $i++) { 
		if (isset($team[$i])) { ?>
			
			<tr class="additionalRow">
				<input type="hidden" name="tahunakademik[]" value="<?= $team[$i]['tahunakademik']; ?>">
				<input type="hidden" name="judul[]" value="<?= $team[$i]['judul']; ?>">
				<input type="hidden" name="program[]" value="<?= $team[$i]['program']; ?>">
				<input type="hidden" name="kegiatan[]" value="<?= $team[$i]['kegiatan']; ?>">
				<input type="hidden" name="param[]" value="<?= $team[$i]['param']; ?>">
				<input type="hidden" name="sks[]" value="<?= $team[$i]['sks']; ?>">

				<td><?= $no; ?></td>
				<td><?= $team[$i]['judul']; ?></td>
				<td><?= get_thajar($team[$i]['tahunakademik']); ?></td>
				<td><?= research_program($team[$i]['program']); ?></td>
				<td><?= activity_research($team[$i]['kegiatan']); ?></td>
				<td><?= param_research($team[$i]['param']); ?></td>
				<td><?= $team[$i]['sks']; ?></td>
				<td><a class="btn btn-danger btn-sm" onclick="rmData(<?= $i ?>)"><i class="icon icon-remove"></i></a></td>	
			</tr>

		<?php }
		$no++;
	}
?>