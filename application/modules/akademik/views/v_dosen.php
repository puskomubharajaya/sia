<script type="text/javascript">
$(function () {
    $('#dosen2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Status Dosen UBJ per <?php echo $tahundosen; ?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Dosen Tetap', 
                y: <?php echo $dosen_tetap->jumlh_dsn; ?>
            }, {
                name: 'Dosen Tidak Tetap',
                y: <?php echo $dosen_tidak->juml_dsn; ?>
            }]
        }]
    });
});
</script>
<div id="dosen2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>