<script type="text/javascript">

$(document).ready(function(){

	var table = $('#tabel_krs');

	var oTable = table.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
		
	var hari = [];
		hari[1] = 'Senin'; 
		hari[2] = 'Selasa';
		hari[3] = 'Rabu';
		hari[4] = 'Kamis';
		hari[5] = 'Jumat';
		hari[6] = 'Sabtu';
		hari[7] = 'Minggu';
	
		
	var table_jadwal = $('#tabel_jadwal');	

	var oTable_jadwal = table_jadwal.dataTable({

		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5,6] }],

		"bLengthChange": false,

        "bFilter": false, 

		"bInfo": false,

		"bPaginate": false,

		"bSort": false,

	});

	oTable.on('click', 'tbody tr .edit', function() {

		var nRow = $(this).parents('tr')[0];

		var aData = oTable.fnGetData(nRow);

		var kd_matakuliah = aData[1];

		$.ajax({

			url: "<?php echo base_url('akademik/krs_mhs/get_jadwal'); ?>",

			type: "post",

			data: {kd_matakuliah: kd_matakuliah},

			success: function(d) {

				var parsed = JSON.parse(d);

                var arr = [];

                for (var prop in parsed) {

                    arr.push(parsed[prop]);

                }

                oTable_jadwal.fnClearTable();

                oTable_jadwal.fnDeleteRow(0);

                for (var i = 0; i < arr.length; i++) {

                    oTable_jadwal.fnAddData([hari[arr[i]['hari']],arr[i]['kelas'],arr[i]['waktu_mulai'].substring(0, 5)+'/'+arr[i]['waktu_selesai'].substring(0, 5), arr[i]['kode_ruangan'],arr[i]['nama'],arr[i]['kd_jadwal'],arr[i]['kd_matakuliah'],arr[i]['kuota'],arr[i]['jumlah']]);

                }

				$('#jadwal').modal('show');

			}

		});	

	});


	oTable_jadwal.on( 'click', 'tr', function () {

        if ( $(this).hasClass('active') ) {

            $(this).removeClass('active');

        }

        else {

            table_jadwal.$('tr.active').removeClass('active');

            $(this).addClass('active');

			var aData = oTable_jadwal.fnGetData(this);

			$('#kd_jadwal').val(aData[5]);

			$('#kd_matakuliah').val(aData[6]);

        }

    });

});
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			<?php $renkeu = $this->app_model->renkeu_printKRS($npm_mahasiswa,$ta)->row(); ?> 
			<div class="widget-content">
				<div class="span11">

					<?php if ($pembimbing['status_verifikasi'] == '' or is_null($pembimbing['status_verifikasi'])) { ?>
						<div class="alert alert-success">
							<b>Perhatian!</b> Mohon isi semua jadwal matakuliah setelah mengisi formulir KRS dengan cara klik pilih jadwal pada tabel dibawah, kemudian klik ajukan.
						</div>
					<?php } ?>
					
					<?php if ($renkeu == false) { ?>
						<div class="alert alert-warning">
							<b>Perhatian!</b> Harap menyelesaikan pembayaran terlebih dahulu sebelum mencetak KRS
						</div>
					<?php } ?>
					

					<table>
						<tr>
							<td width="50" style="vertical-align:top">Status</td>
							<td> : 
							<?php if($pembimbing['status_verifikasi'] == 1) 
							{ 
								echo '<b style="background:#ADFF2F;padding:2px;border-radius:10px;">TERIMA</b>'; 
							} elseif ($pembimbing['status_verifikasi'] == 2) {
								echo '<b style="background:#FFA500;padding:2px;border-radius:10px;">REVISI</b>';
							} elseif ($pembimbing['status_verifikasi'] == 3) {
								echo "TOLAK";
							} elseif ($pembimbing['status_verifikasi'] == 4) {
								echo '<b style="background:#FFA500;padding:2px;border-radius:10px;color:white">TERAJUKAN</b> <br><br> 
									<div class="alert alert-yellow">
										<b>Perhatian!</b> KRS anda telah diajukan, silahkan cek e-mail anda dan tunggu konfirmasi dari dosen pembimbing anda.
									</div>';
							} else {
								echo '<b style="background:#FF0000;padding:2px;border-radius:10px;color:white">PENGAJUAN<b>';
							}?></td>
							<td></td>
						</tr>
						<!-- <tr>
							<td>Pembimbing Akademik</td>
							<td> : <?php //echo $pembimbing['nama']; ?></td>
							<td></td>
						</tr> -->
						<!-- <tr>
							<td>Catatan Pembimbing</td>
							<td> : <?php //echo $noteformhs['note']; ?></td>
							<td></td>
						</tr> -->
					</table>
					<br>
					
					<?php $cek = $this->app_model->validjadwal_tes($kd_krs)->result(); $cek2 = $this->db->where('kd_krs', $kd_krs)->get('tbl_verifikasi_krs')->row(); 
					if (count($cek) != 0) { ?>
						<a href="javascript:void(0);" onclick="alert('Harap Isi Jadwal Terlebih Dahulu!')" class="btn btn-danger" title=""><i class="icon icon-remove"></i> Ajukan KRS</a>
						<a href="<?php echo base_url(); ?>akademik/krs_mhs/revisi_krs/<?php echo $kd_krs ?>" class="btn btn-success"><i class="btn-icon-only icon-edit"> </i> Edit KRS</a>
					<?php } elseif ((count($cek) == 0) and (is_null($cek2->status_verifikasi))) { ?>
						<a href="<?php echo base_url('akademik/krs_mhs/ajukan_krs/'.$kd_krs); ?>" class="btn btn-primary" title=""><i class="icon icon-ok"></i> Ajukan KRS</a>
						<a href="<?php echo base_url(); ?>akademik/krs_mhs/revisi_krs/<?php echo $kd_krs ?>" class="btn btn-success"><i class="btn-icon-only icon-edit"> </i> Edit KRS</a>
					<?php } elseif ((count($cek) == 0) and ($cek2->status_verifikasi == 4)) { ?>
						<!-- <a href="javascript:void(0);" class="btn btn-primary" title="" disabled=""><i class="icon icon-ok"></i> Submit</a> -->
					<?php } elseif ((count($cek) == 0) and ($cek2->status_verifikasi == 1)) { ?>
						<?php if ($renkeu == true){?>
							<a target="_blank" href="<?php echo base_url(); ?>akademik/krs_mhs/printkrs/<?php echo $kd_krs ?>" class="btn btn-success"><i class="btn-icon-only icon-print"> </i> Print KRS</a>
						<?php } ?>
					<?php } elseif ($cek2->status_verifikasi == 2 || $cek2->status_verifikasi == 3) { ?>
						<a href="<?php echo base_url(); ?>akademik/krs_mhs/revisi_krs/<?php echo $kd_krs ?>" class="btn btn-primary"><i class="btn-icon-only icon-edit"> </i> Edit KRS</a>
					<?php } ?>
					<hr>
					<!-- <small><?php echo $kdkrs; ?></small> -->
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No.</th>
	                        	<th>Kode MK</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>
	                        	<th>Dosen</th>
								<th>Hari/Waktu</th>
								<th>Ruangan</th>
								<?php //$this->load->model('setting_model'); $aktif = $this->setting_model->getaktivasi('kelas')->result(); $logged = $this->session->userdata('sess_login'); if (($logged['id_user_group'] == 5) && (count($aktif) == 1) ) { ?>
								<?php if ((is_null($pembimbing['status_verifikasi'])) or ($comparemk != $comparejd)) { ?>
									<th width="40">Pilih Jadwal</th>
									<th width="40">Kosongkan Jadwal</th>
								<?php } ?>								
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($detail_krs as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo notohari($row->hari).' / '.substr($row->waktu_mulai, 0,5).'-'.substr($row->waktu_selesai, 0,5); ?></td>

                                <td><?php echo $row->kode_ruangan; ?></td>

                               	<?php if ((is_null($pembimbing['status_verifikasi'])) or ($comparemk != $comparejd)) { ?>
									<td class="td-actions">
	                                	<a data-toggle="modal" class="btn btn-success btn-small edit">
	                                		<i class="btn-icon-only icon-ok"></i>
	                                	</a>
	                                </td>
	                                <td>
	                                	<a href="<?= base_url(); ?>akademik/krs_mhs/kosongkan_jadwal/<?= $row->kd_krs; ?>/<?= $row->kd_matakuliah; ?>" class="btn btn-danger btn-small edit"><i class="btn-icon-only icon-remove"> </i></a>
	                                </td>
								<?php } ?>
                            </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">PILIH JADWAL MATAKULIAH</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/krs_mhs/save_jadwal" method="post">

                <div class="modal-body">    

					<input type="hidden" id="kd_jadwal" name="kd_jadwal" value=""/>

					<input type="hidden" id="kd_matakuliah" name="kd_matakuliah" value=""/>

					<input type="hidden" id="kd_krs" name="kd_krs" value="<?php echo $kd_krs; ?>"/>

                    <table id="tabel_jadwal" class="table table-bordered table-striped">

	                  <thead>

	                        <tr> 

	                          <th>Hari</th>

	                          <th>Kelas</th>

	                          <th>Jam</th>

	                          <th>Ruang</th>

							  <th>Dosen</th>

							  <th></th>

							  <th></th>

							  <th>Kuota</th>

							  <th>Jumlah</th>

	                        </tr>

	                    </thead>

	                    <tbody>

	                    	<tr>

	                    		<td></td>

	                    		<td></td>

	                    		<td></td>

	                    		<td></td>

	                    		<td></td>

								<td></td>

								<td></td>

								<td></td>

								<td></td>

	                    	</tr>

	                    </tbody>

	                </table>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" class="btn btn-primary" value="Save changes"/>

                </div>

            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->