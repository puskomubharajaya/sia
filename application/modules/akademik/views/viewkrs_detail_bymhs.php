<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_krs');
	var oTable = table.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
		
	var hari = [];
		hari[1] = 'Senin'; 
		hari[2] = 'Selasa';
		hari[3] = 'Rabu';
		hari[4] = 'Kamis';
		hari[5] = 'Jumat';
		hari[6] = 'Sabtu';
		hari[7] = 'Minggu';
	
		
	var table_jadwal = $('#tabel_jadwal');	
	var oTable_jadwal = table_jadwal.dataTable({
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5,6] }],
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});
	oTable.on('click', 'tbody tr .edit', function() {
		var nRow = $(this).parents('tr')[0];
		var aData = oTable.fnGetData(nRow);
		var kd_matakuliah = aData[1];
		$.ajax({
			url: "<?php echo base_url('akademik/viewkrs/get_jadwal'); ?>",
			type: "post",
			data: {kd_matakuliah: kd_matakuliah},
			success: function(d) {
				var parsed = JSON.parse(d);
                var arr = [];
                for (var prop in parsed) {
                    arr.push(parsed[prop]);
                }
                oTable_jadwal.fnClearTable();
                oTable_jadwal.fnDeleteRow(0);
                for (var i = 0; i < arr.length; i++) {
                    oTable_jadwal.fnAddData([hari[arr[i]['hari']],arr[i]['kelas'],arr[i]['waktu_mulai'].substring(0, 5)+'/'+arr[i]['waktu_selesai'].substring(0, 5), arr[i]['kode_ruangan'],arr[i]['nama'],arr[i]['kd_jadwal'],arr[i]['kd_matakuliah'],arr[i]['kuota'],arr[i]['jumlah']]);
                }
				$('#jadwal').modal('show');
			}
		});	
	});
	
	oTable_jadwal.on( 'click', 'tr', function () {
	    if ( $(this).hasClass('active') ) {
	        $(this).removeClass('active');
	    }
	    else {
	        table_jadwal.$('tr.active').removeClass('active');
	        $(this).addClass('active');
			var aData = oTable_jadwal.fnGetData(this);
			$('#kd_jadwal').val(aData[5]);
			$('#kd_matakuliah').val(aData[6]);
	    }
    });

    $(".absensi").on("click", function(e){
    	let idj = $(this).attr('data-id');

    	$.post("<?php echo base_url('akademik/viewkrs/absensi') ?>", {idj : idj}, function (data) {
    		$("#loadabsen").empty();
    		$("#loadabsen").append(data)
    	})
    })
});

	function muatData(id) {
		$("#contentuji").load('<?= base_url('akademik/viewkrs/loadjadwaluji/') ?>'+id);
	}
</script>
<?php $renkeu = $this->app_model->renkeu_printKRS($npm,$ta)->row(); ?>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content" style="overflow:auto;">
				<div class="span11">
					<a href="<?php echo base_url(); ?>akademik/viewkrs" class="btn btn-success"><< Kembali</a>
						<?php $cek = $this->app_model->validjadwal($kd_krs)->result();  ?>


<?php if ($renkeu == true){?>

						<?php if ($status_krs->status_verifikasi == 1){ ?>
							<a href="<?php echo base_url(); ?>akademik/viewkrs/printkrs/<?php echo $kd_krs ?>" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print KRS</a>
						<?php } elseif (count($cek) == 0 and substr($kd_krs, 12,5) > 20162 and $status_krs->status_verifikasi == 1){ ?>
							<!-- <a href="<?php //echo base_url(); ?>akademik/krs_mhs/revisi_krs/<?php //echo $kd_krs ?>" class="btn btn-primary"><i class="btn-icon-only icon-edit"> </i> Edit KRS</a> -->
							<a href="<?php echo base_url(); ?>akademik/viewkrs/printkrs/<?php echo $kd_krs ?>" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print KRS</a>
						<?php } ?>
<?php } ?>

						
					<hr>
					<table>
						<!--tr>
							<td>Status</td>
							<td> : <?php //if($pembimbing['status_verifikasi'] == 0) { echo 'Belum Disetujui'; } ?></td>
							<td></td>
						</tr-->

						<tr>
							<td>Pembimbing Akademik</td>
							<td> : <?php echo $pembimbing['nama']; ?></td>
							<td></td>
						</tr>
						
						<tr>
							<td>Catatan</td>
							<td> : <?php echo $pembimbing['keterangan_krs']; ?></td>
							<td></td>
						</tr>
					</table>
					<table id="tabel_krs" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Kode MK</th>
	                        	<th>Mata Kuliah</th>
	                        	<th>SKS</th>								
	                        	<th>Dosen</th>
								<th>Hari/Waktu</th>
								<th>Ruangan</th>
								<?php $this->load->model('setting_model'); $aktif = $this->setting_model->getaktivasi('kelas')->result(); $logged = $this->session->userdata('sess_login'); if (($logged['id_user_group'] == 8) && (count($aktif) == 1) ) { ?>
									<th width="40">Pilih Jadwal</th>
									<th width="40">Kosongkan Jadwal</th>
								<?php } ?>
								<th>Jadwal ujian</th>
								<th>Absensi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no = 1; foreach ($detail_krs as $row) { ?>
	                        <tr>
								<?php
									switch ($row->hari){
										case 1 :
											$hari = 'Senin';
										break;
										case 2 :
											$hari = 'Selasa';
										break;
										case 3 :
											$hari = 'Rabu';
										break;
										case 4 :
											$hari = 'Kamis';
										break;
										case 5 :
											$hari = 'Jumat';
										break;
										case 6 :
											$hari = 'Sabtu';
										break;
										case 7 :
											$hari = 'Minggu';
										break;
										default:
										$hari = '';
									}
								?>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->kd_matakuliah; ?></td>
                                <td><?php echo $row->nama_matakuliah; ?></td>
                                <td><?php echo $row->sks_matakuliah; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $hari.' / '.substr($row->waktu_mulai, 0,5).'-'.substr($row->waktu_selesai, 0,5); ?></td>
                                <td><?php echo $row->kode_ruangan; ?></td>
                                <?php $this->load->model('setting_model'); $aktif = $this->setting_model->getaktivasi('kelas')->result(); $logged = $this->session->userdata('sess_login'); if (($logged['id_user_group'] == 8) && (count($aktif) == 1) ) { ?>
                                <td class="td-actions">
                                	<a data-toggle="modal" class="btn btn-success btn-small edit"><i class="btn-icon-only icon-ok"> </i></a>
                                	
                                </td>
                                <td>
                                	<a href="<?php //echo base_url(); ?>akademik/viewkrs/kosongkan_jadwal/<?php echo $row->id_krs; ?>" class="btn btn-danger btn-small edit"><i class="btn-icon-only icon-remove"> </i></a>
                                </td>
                                <?php } ?>
                                <td>
                                	<button data-toggle="modal" data-target="#moduji" class="btn btn-warning" onclick="muatData('<?= $row->id_jadwal; ?>')">
                                		<i class="icon icon-info"></i>
                                	</button>
                                </td>
                                <td>
                                	<button  class="btn btn-info absensi" data-toggle="modal" data-target="#absensi" data-id="<?php echo $row->id_jadwal ?>">
                                		<i class="icon icon-eye-open"></i>
                                	</button>
                                </td>
	                        </tr>
	                        <?php $no++; } ?>
	                    </tbody>
	               	</table>
					<br>
					<!-- <h3>Rekam Bimbingan</h3>
					<table class="table table-bordered table-striped">
	                	<thead>
	                        <tr>
		               			<td style="width:80px">Tanggal</td>
		               			<td>Catatan</td>
		               			<td style="width:110px">Status</td>
		               		</tr>
	                    </thead>
	                    <tbody>
                            <?php //foreach ($bimbingan as $isi) { ?>
	               				<tr>
	               					<td style="width:80px"><?php //echo $isi->tgl; ?></td>
	               					<td><?php //echo $isi->note; ?></td>
	               					<td style="width:110px">
	               					<?php //if ($isi->status == 0) {
	               						//echo "KRS anda di Tolak";
	               					//}elseif ($isi->status == 1) {
	               						//echo "KRS OK";
	               					//}elseif ($isi->status == 2) {
	               						//echo "Mohon Revisi KRS anda";
	               					//} ?>
	               					</td>
	               				</tr>
	               			<?php //} ?>
	                    </tbody>
	               	</table> -->
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">PILIH JADWAL MATAKULIAH</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/viewkrs/save_jadwal" method="post">
                <div class="modal-body">    
					<input type="hidden" id="kd_jadwal" name="kd_jadwal" value=""/>
					<input type="hidden" id="kd_matakuliah" name="kd_matakuliah" value=""/>
					<input type="hidden" id="kd_krs" name="kd_krs" value="<?php echo $kd_krs; ?>"/>
                    <table id="tabel_jadwal" class="table table-bordered table-striped">
	                  <thead>
	                        <tr> 
	                          <th>Hari</th>
	                          <th>Kelas</th>
	                          <th>Jam</th>
	                          <th>Ruang</th>
							  <th>Dosen</th>
							  <th></th>
							  <th></th>
							  <th>Kuota</th>
							  <th>Jumlah</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<tr>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
	                    		<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
	                    	</tr>
	                    </tbody>
	                </table>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- modal detil ujian -->
<div class="modal fade" id="moduji" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="contentuji">
            
        </div>
    </div>
</div>

<!-- modal detil ujian -->
<div class="modal fade" id="absensi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="loadabsen">
            
        </div>
    </div>
</div>