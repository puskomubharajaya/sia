<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<?php error_reporting(0); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Hasil Studi Semester Perbaikan</h3>
			</div> <!-- /widget-header -->
			
			
			<div class="widget-content" style="overflow:auto;">
				<div class="span11">
					<?php //if (substr($mhs->NIMHSMSMHS, 0,4) > 2014) { ?>
						<a href="<?php echo base_url(); ?>akademik/khs/transkrip/<?php echo $mhs->NIMHSMSMHS; ?>" class="btn btn-primary"><i class="icon icon-print"></i> Transkrip</a>
					<?php //} ?>
					<!-- <a href="<?php //echo base_url(); ?>akademik/khs/transkrip" class="btn btn-primary"><i class="icon icon-print"></i> Transkrip</a> -->
					<?php 
                        $log = $this->session->userdata('sess_login');
                        $pecah = explode(',', $log['id_user_group']);
                        $jmlh = count($pecah);
                        for ($i=0; $i < $jmlh; $i++) { 
                            $grup[] = $pecah[$i];
                        }
                        if ((in_array(7, $grup))) { ?>
                            <!-- <a href="<?php echo base_url(); ?>akademik/bimbingan/viewmhs_tes/<?php echo $this->session->userdata('kode_krs'); ?>" class="btn btn-warning"><< Kembali</a> -->
                        <?php } ?>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Semester Diperbaiki</th>
	                        	<th>Total SKS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach ($detail_khs as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<?php $a = $this->app_model->get_semester_khs($mhs->SMAWLMSMHS,$row->THSMSTRLNM-2); ?>
	                        	<td><?php echo $a; ?></td>
	                        	<?php
	                        	if ($row->THSMSTRLNM < '20151') {
	                        		$q = $this->db->query("select distinct KDKMKTRLNM from tbl_transaksi_nilai where NIMHSTRLNM = '".$mhs->NIMHSMSMHS."' and THSMSTRLNM = '".$row->THSMSTRLNM."'")->result();
		                        	$sumsks = 0;
		                        	foreach ($q as $value1) {
		                        		$sksmk = $this->db->query("select distinct sks_matakuliah from tbl_matakuliah_copy where kd_matakuliah = '".$value1->KDKMKTRLNM."' AND kd_prodi = ".trim($row->KDPSTTRLNM)."")->row()->sks_matakuliah; 
		                        		$sumsks = $sumsks + $sksmk;
		                        	}
	                        	} else {
	                        		$q = $this->db->query("SELECT distinct kd_matakuliah from tbl_krs_sp where kd_krs LIKE CONCAT('".$mhs->NIMHSMSMHS."','".$row->THSMSTRLNM."%')")->result();
		                        	$sumsks = 0;
		                        	foreach ($q as $value1) {
		                        		$sksmk = $this->db->query("SELECT distinct sks_matakuliah from tbl_matakuliah where kd_matakuliah = '".$value1->kd_matakuliah."' AND kd_prodi = ".trim($row->KDPSTTRLNM)."")->row()->sks_matakuliah; 
		                        		$sumsks = $sumsks + $sksmk;
		                        	}
	                        	}
	                        	?>
                                <td><?php echo $sumsks; ?></td>
                                
	                        	<td class="td-actions">
	                        		<?php if ((in_array(7, $grup)) || (in_array(6, $grup))) { ?>
										<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/bimbingan/detailKhs/<?php echo $mhs->NIMHSMSMHS; ?>/<?php echo $row->THSMSTRLNM; ?>"><i class="btn-icon-only icon-ok"> </i></a>
									<?php } else { ?>
										<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/perbaikan/detailKhs/<?php echo $mhs->NIMHSMSMHS; ?>/<?php echo $row->THSMSTRLNM; ?>"><i class="btn-icon-only icon-ok"> </i></a>
									<?php } ?>
									
									<?php //} ?>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>