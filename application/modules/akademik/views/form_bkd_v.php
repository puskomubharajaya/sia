<h3><i class="icon-file-text-alt"></i> Formulir Beban Kinerja Dosen</h3>
<br>
<div class="form-horizontal">
	<form action="<?= base_url('akademik/beban_kinerja_dosen/set_form_option') ?>" method="post">
		<fieldset>
	  		<div class="control-group">
	  			<label class="control-label">Pilih Tahun Akademik</label>
				<div class="controls">
					<select name="tahunajar" id="" class="form-control span4">
						<?php foreach ($yearAcademic as $year) { ?>
							<option value="<?= $year->kode ?>"><?= $year->tahun_akademik ?></option>
						<?php } ?>
					</select>
				</div> <!-- /controls -->				
			</div>
		
			<div class="form-actions">
            	<button type="submit" class="btn btn-primary">Lihat</button>
          	</div> <!-- /form-actions -->
        </fieldset>
	</form>
</div>