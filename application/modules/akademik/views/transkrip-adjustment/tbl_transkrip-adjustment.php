<table>
	<tr>
		<td colspan="2">NAMA</td>
		<td>:</td>
		<td><?php echo $head->nama_mhs ?></td>
		<td colspan="13"></td>
	</tr>
	<tr>
		<td colspan="2">NPM</td>
		<td>:</td>
		<td id="npm-mhs"><?php echo trim($head->npm_mhs) ?></td>
		<td colspan="13"></td>
	</tr>
	<tr>
		<td colspan="2">PROGRAM STUDI</td>
		<td>:</td>
		<td><?php echo $head->prodi ?></td>
		<td colspan="13"></td>
	</tr>
	<tr>
		<td colspan="2">JENJANG</td>
		<td>:</td>
		<td><?php echo $head->jenjang ?></td>
		<td colspan="13"></td>
	</tr>
</table>
<br>
<?php
	$totalSks = 0;
	$tnl = 0;
?>
<table id="tbl_transkrip-adjust" class="table table-bordered table-striped">
	<thead>
		<tr> 
			<th style="text-align: center">NO</th>
			<th style="text-align: center">KODE</th>
			<th style="text-align: center" colspan="2">MATA KULIAH</th>
			<th style="text-align: center">SEM</th>
			<th style="text-align: center">SKS<br/>(K)</th>
			<th style="text-align: center">NILAI</th>
			<th style="text-align: center">BBT<br/>(N)</th>
			<th style="text-align: center">NxK</th>
			<th style="text-align: center" width="80">Aksi</th>
		</tr>
	</thead>
	<tbody id="appearHere">
		<?php 
		$i = 1;  
		foreach ($transkrip as $row): 
			$nk = ($row->sks_matakuliah * $row->BOBOTTRLNM);
			?>
			<tr>
				<td style="text-align: right"><?php echo $i++; ?></td>
				<td><?php echo $row->KDKMKTRLNM ?></td>
				<td colspan="2"><?php echo $row->nama_matakuliah ?></td>
				<td style="text-align: center"><?php echo $smtr_mk = $this->app_model->get_semester_khs($head->SMAWLMSMHS, $row->THSMSTRLNM); ?></td>
				<td style="text-align: center"><?php echo $row->sks_matakuliah ?></td>
				<td style="text-align: center"><?php echo $row->NLAKHTRLNM ?></td>
				<td style="text-align: center"><?php echo $row->BOBOTTRLNM ?></td>
				<td style="text-align: center"><?php echo $nk ?></td>
				<td style="text-align: center"><button class="btn btn-danger del-transkrip" name="<?php echo $row->KDKMKTRLNM ?>"><i class="icon icon-remove"></i></button></td>

			</tr>
			<?php
				if ($row->NLAKHTRLNM == 'T') {
					$totalSks = $totalSks;
					$tnl = $tnl;
				} else {
					$totalSks = $totalSks + $row->sks_matakuliah;
					$tnl = $tnl + $nk;
				}
			?>
		<?php endforeach ?>
     </tbody>
</table>
<table id="tbl_transkrip-adjust-revisi" class="table table-bordered table-striped">
	<thead>
		<tr> 
			<th style="text-align: center">NO</th>
			<th style="text-align: center">KODE</th>
			<th style="text-align: center" colspan="2">MATA KULIAH</th>
			<th style="text-align: center">SEM</th>
			<th style="text-align: center">SKS<br/>(K)</th>
			<th style="text-align: center">NILAI</th>
			<th style="text-align: center">BBT<br/>(N)</th>
			<th style="text-align: center">NxK</th>
			<th style="text-align: center" width="80">Aksi</th>
		</tr>
	</thead>

	<tbody id="appearHere">
    <?php 
    $i = 1;  
    $count = count($transkrip_konversi);
    
    if ($count > 0 && !empty($transkrip_konversi)):
     	foreach ($transkrip_konversi as $row): 
	    	$nk = ($row->sks_matakuliah * $row->BOBOTTRLNM);
	?>
	    	<tr>
	    		<td style="text-align: right"><?php echo $i++; ?></td>
	    		<td><?php echo $row->KDKMKTRLNM ?></td>
	    		<td colspan="2"><?php echo $row->nama_matakuliah ?></td>
	    		<td style="text-align: center"><?php echo $smtr_mk = $this->app_model->get_semester_khs($head->SMAWLMSMHS, $row->THSMSTRLNM); ?></td>
	    		<td style="text-align: center"><?php echo $row->sks_matakuliah ?></td>
	    		<td style="text-align: center"><?php echo $row->NLAKHTRLNM ?></td>
	    		<td style="text-align: center"><?php echo $row->BOBOTTRLNM ?></td>
	    		<td style="text-align: center"><?php echo $nk ?></td>
	    		<td style="text-align: center"><button class="btn btn-danger del-transkrip" t="konversi" name="<?php echo $row->KDKMKTRLNM ?>"><i class="icon icon-remove"></i></button></td>
	    	</tr>

	    	<?php 
	    		if ($row->NLAKHTRLNM == 'T') {
					$totalSks = $totalSks;
					$tnl = $tnl;
				} else {
					$totalSks = $totalSks + $row->sks_matakuliah;
					$tnl = $tnl + $nk;
				}
	    	?>
    	<?php endforeach ?>
    <?php else:?>
    	<tr id="toRemove">
          <td colspan="6"><i>Tidak Ada Data di Table Konversi</i></td>
        </tr>
    <?php endif ?>
 </tbody>
</table>
<br><br>
<table>
	<tr>
		<td>*BBT</td>
		<td>=</td>
		<td>Bobot Nilai</td>
		<td colspan="3"></td>
		<td colspan="11"></td>
	</tr>
	<tr>
		<td>*NxK</td>
		<td>=</td>
		<td colspan="4">Bobot Nilai x Kredit</td>
		<td colspan="11"></td>
	</tr>
	<tr>
		<td colspan="3">SKS yang telah di tempuh :</td>
		<td style="text-align: right" colspan="3"><?php echo $totalSks ?></td>
		<td colspan="11"></td>
	</tr>
	<tr>
		<td colspan="3">Indeks Prestasi Kumulatif :</td>
		<td style="text-align: right" colspan="3"><?php echo number_format($tnl / $totalSks, 2) ?></td>
		<td colspan="11"></td>
	</tr>
</table>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  
    $(".del-transkrip").on('click', function (e) {
    	e.preventDefault();
    	var result = confirm("Yakin akan hapus mata kuliah ini ?!");
    	var txt;
    	var t = $(this).attr('t');

    	if (t == undefined) t = '';

    	if (result == true) {
			var npm = $("#npm-mhs").text();
			txt = "You pressed OK!";
			$.ajax({
				url : "<?php echo base_url('akademik/transkrip-adjustment/rm')?>",
				type: "POST",
				data: {KDKMKTRLNM : $(this).attr('name') , NIMHSTRLNM: npm.trim(), t: t},
				success: function (data) {
					var res = JSON.parse(data)

					if (res.status) {
						alert(res.msg)
						$("#load_table").load('<?php echo base_url() ?>' + 'akademik/transkrip-adjustment/viewTranskrip?npm=' + npm.trim())
					} else {
						alert(res.msg)
					}
				}
		  	})
		} 
    })
  });
</script>
