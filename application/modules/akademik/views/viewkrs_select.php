<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data KRS Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span12">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Per Angkatan</a></li>
            <li><a data-toggle="tab" href="#menu1">Per Nama</a></li>
          </ul>
          <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
              <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>akademik/viewkrs/viewkrsprodi">
                <fieldset>
                    <div class="control-group">
                      <label class="control-label">Angkatan</label>
                      <div class="controls">
                        <select class="form-control span6" name="tahun">
                          <option disabled selected>--Pilih Angkatan--</option>
                          <option value="all">Semua Angkatan</option>
                          <?php for ($i=2008; $i < date('Y')+1; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>   

                    <div class="control-group">
                      <label class="control-label">Tahun Akademik</label>
                      <div class="controls">
                        <select class="form-control span6" name="akad">
                          <option disabled selected>--Pilih Tahun Akademik--</option>
                          <?php foreach ($tahunajar as $key) { ?>
                            <option value="<?php echo $key->kode; ?>"><?php echo $key->tahun_akademik; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                      
                    <div class="form-actions">
                        <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                    </div> <!-- /form-actions -->
                </fieldset>
              </form>    
            </div>
            <div id="menu1" class="tab-pane fade">
              <form class="form-horizontal" action="<?php echo base_url('akademik/viewkrs/create_sess'); ?>" method="post">
                <fieldset>
                  <div class="control-group">
                    <label class="control-label">Nama / NPM</label>
                    <div class="controls">
                      <input class=" form-control span4" name="npm">
                    </div>
                  </div>
                  <div class="form-actions">
                    <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
                  </div>
                </fieldset>
              </form>
            </div>
  				</div>
				</div>
			</div>
		</div>
	</div>
</div>
