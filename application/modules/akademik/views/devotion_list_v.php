<div class="container">	
    <div class="row">
      	<div class="span12">   
      		<div class="widget ">
      			<div class="widget-header">
      				<i class="icon-list"></i>
      				<h3>Detail Daftar Pengabdian</h3>
  				</div> 
				<div class="widget-content">
					<div class="span11">
						<a href="<?= base_url('akademik/beban_kinerja_dosen') ?>" class="btn btn-warning"><i class="icon-chevron-left"></i> Kembali</a>
						<hr>
						<table id="example1" class="table table-bordered table-striped">
	                        <thead>
	                            <tr> 
	                                <th>No</th>
	                                <th>Jenis Pengabdian</th>
	                                <th>Program</th>
	                                <th>Tahun Akademik</th>
	                                <th>Peran / Kategori</th>
	                                <th>Beban SKS</th>
	                                <th>Status</th>
	                                <th>Hapus</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	<?php $no = 1; foreach ($devotions as $devotion) { ?>
	                        		<tr>
		                        		<td><?= $no; ?></td>
		                        		<td><?= $devotion->nama; ?></td>
		                        		<td><?= $devotion->program; ?></td>
		                        		<td><?= get_thajar($devotion->tahunakademik); ?></td>
		                        		<td><?= param_research($devotion->param); ?></td>
		                        		<td><?= $devotion->bobot_sks; ?></td>
		                        		<td><?= $devotion->status ?></td>
										<td>
											<a 
												class="btn btn-danger pull-right" 
												onclick="return confirm('Yakin ingin menghapus data ini?')"
												href="<?= base_url('akademik/beban_kinerja_dosen/remove_devotion/'.$devotion->id) ?>"
												data-toggle="tooltip" 
												title="hapus pengabdian">
												<i class="icon-trash"></i>
											</a>
										</td>
	                        		</tr>
	                        	<?php $no++; } ?>
	                        </tbody>
	                    </table>
					</div>
				</div>
			</div>
	    </div>
    </div>
</div>