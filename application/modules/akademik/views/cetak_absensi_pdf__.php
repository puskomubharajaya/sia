<?php

//var_dump($rows);die();

$pdf = new FPDF("L","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();


$pdf->SetMargins(3, 3 ,0);
$pdf->SetAutoPageBreak(TRUE, 3);
$pdf->SetFont('Arial','B',10); 

$pdf->Ln(0);

if ($years < 20192) {
	$pdf->Cell(200,5,'FAKULTAS '.strtoupper(get_fak_byprodi($kodeprodi)),0,0,'L');
}elseif (is_null($rows->gabung)) {
	$pdf->Cell(200,5,'FAKULTAS '.strtoupper(get_fak_byprodi($kodeprodi)),0,0,'L');
} else {
	$pdf->Cell(200,5,'FAKULTAS '.strtoupper(get_fak($kodeprodi)),0,0,'L');
}


$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

if ($years < 20192) {
	$pdf->Cell(200,5,''.strtoupper(get_jur($kodeprodi)).' - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');
} else if (is_null($rows->gabung)) {
	$pdf->Cell(200,5,''.strtoupper(get_jur($kodeprodi)).' - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');
}else {
	$pdf->Cell(200,5,''.strtoupper('UNIVERSITAS BHAYANGKARA JAKARTA RAYA'),0,0,'L');
}

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS II',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

if (substr($rows->kd_tahunajaran, 4) == 1) {
	$ta = 'Ganjil';
} else {
	$ta = 'Genap';
}

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->semester_matakuliah.' / '.substr($rows->kd_tahunajaran, 0, 4).' - '.$ta,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,nama_dsn($rows->kd_dosen),0,0,'L');

$pdf->Cell(20,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kelas,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_dosen,0,0,'L');

$pdf->Cell(20,5,'Kuota',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kuota,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'RUANG',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->kode_ruangan,0,0,'L');

$pdf->Cell(15,5,'WAKTU',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,notohari($rows->hari).'/'.del_ms($rows->waktu_mulai).'-'.del_ms($rows->waktu_selesai),0,0,'L');

$pdf->Cell(20,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(20,5,'Jumlah Peserta',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

//$jumlah = $this->db->query("select count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = '".$rows->kd_jadwal."'")->row()->mhs;

$pdf->Cell(50,5,$jumlah.' orang',0,0,'L');




$pdf->ln(10);

$pdf->SetLeftMargin(5);

$pdf->SetFont('Arial','',12);

$pdf->Cell(289,8,'DAFTAR HADIR PESERTA KULIAH',1,0,'C');

$pdf->ln(8);



$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->Cell(176,5,'PERTEMUAN KULIAH','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');



for ($i=1; $i < 17; $i++) { 

	$pdf->Cell(11,5,$i,1,0,'C');	

}

$no=1;
$noo=1;

foreach ($mhs as $key) {
	
	$pdf->ln(5);

	$pdf->Cell(8,5,$no,1,0,'C');

	$pdf->Cell(25,5,$key->npm_mahasiswa,1,0,'C');

	$pdf->Cell(80,5,get_nm_mhs($key->npm_mahasiswa),1,0,'L');

	$sisaaa = 16 - $maxper;

	if (($this->session->userdata('tahunajaran') == '20152') or ($this->session->userdata('tahunajaran') == '20161')) {

        $absen 	= $this->db->query("SELECT pertemuan,kehadiran FROM tbl_absensi_mhs 
        							WHERE npm_mahasiswa = '".$key->npm_mahasiswa."' 
        							AND kd_jadwal = '".$rows->kd_jadwal."' 
        							ORDER BY pertemuan ASC")->result_array();

    } elseif ($this->session->userdata('tahunajaran') == '20162') {

    	$absen 	= $this->db->query("SELECT pertemuan,kehadiran FROM tbl_absensi_mhs_new 
    								WHERE npm_mahasiswa = '".$key->npm_mahasiswa."' 
    								AND kd_jadwal = '".$rows->kd_jadwal."' 
    								ORDER BY pertemuan ASC")->result_array();
    
    } else if ($rows->gabung > 0) {
    	$absen 	= $this->db->query("SELECT DISTINCT(pertemuan),kehadiran FROM tbl_absensi_mhs_new_20171 
    								WHERE npm_mahasiswa = '".$key->npm_mahasiswa."' 
    								AND kd_jadwal = '".$key->kd_jadwal."' 
    								ORDER BY pertemuan ASC")->result_array();
    	
    } else {
        $absen 	= $this->db->query("SELECT pertemuan,kehadiran FROM tbl_absensi_mhs_new_20171 
        							WHERE npm_mahasiswa = '".$key->npm_mahasiswa."' 
        							AND kd_jadwal = '".$rows->kd_jadwal."' 
        							ORDER BY pertemuan ASC")->result_array();
    }

    $arrayPertemuan2 = [];
    foreach ($absen as $keys => $value) {
    	$arrayPertemuan2[] = $value['pertemuan'];
    }
    $arrayPertemuan2 = array_unique($arrayPertemuan2, SORT_REGULAR);

    // mencari pertemuan keberapa yang kosong
    $diff = array_diff($arrayPertemuan, $arrayPertemuan2);
    if (!empty($diff)) {
    	foreach ($diff as $value) {
	    	$absen[] = ['pertemuan' => $value, 'kehadiran' => ""];
		}
		
		$keys = array_column($absen, 'pertemuan');
		
		array_multisort($keys, SORT_ASC, $absen);
		
    }
    
	foreach ($absen as $key1) {
		if ($key1['kehadiran'] == 'H') {
			$pdf->Cell(11,5,'v',1,0,'C');
		} elseif ($key1['kehadiran'] == 'A') {
			$pdf->Cell(11,5,'x',1,0,'C');
		} else {
			$pdf->Cell(11,5,$key1['kehadiran'],1,0,'C');
		}
	}

	for ($i=1; $i <= $sisaaa; $i++) {
		$pdf->Cell(11,5,'',1,0,'C');
	}

	$no++;
	$noo++;

	if ($noo == 21) {
		$pdf->ln(5);

		$pdf->Cell(113,5,'Tanggal Kuliah',1,0,'R');
		$pdf->SetFont('Arial','',7);
		for ($i=1; $i < 17; $i++) { 
			if (($this->session->userdata('tahunajaran') == '20152') or ($this->session->userdata('tahunajaran') == '20161')) {
		        $absen = $this->db->query("SELECT tanggal FROM tbl_absensi_mhs WHERE kd_jadwal = '".$rows->kd_jadwal."' AND pertemuan = ".$i." ")->row();
		    
		    } elseif ($this->session->userdata('tahunajaran') == '20162') {
		    	$absen = $this->db->query("SELECT tanggal FROM tbl_absensi_mhs_new WHERE kd_jadwal = '".$rows->kd_jadwal."' AND pertemuan = ".$i." ")->row();
		    
		    }elseif ($rows->gabung > 0) {
		    	$absen = $this->db->query("SELECT tanggal FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = '".$key->kd_jadwal."' AND pertemuan = ".$i." ")->row();
		    	
		    } else {
		        $absen = $this->db->query("SELECT tanggal FROM tbl_absensi_mhs_new_20171 WHERE kd_jadwal = '".$rows->kd_jadwal."' AND pertemuan = ".$i." ")->row();
		    }
			
			//$pdf->Cell(11,5,'',1,0,'C');
			if ($absen == true) {
				$pdf->Cell(11,5,date("d/m/y", strtotime($absen->tanggal)),1,0,'C');
			} else {
				$pdf->Cell(11,5,'',1,0,'C');
			}
		}
		
		$pdf->SetFont('Arial','',8);

		$pdf->ln(5);

		$pdf->Cell(113,5,'Jumlah Hadir',1,0,'R');


		for ($i=1; $i < 17; $i++) { 
			if (($this->session->userdata('tahunajaran') == '20152') or ($this->session->userdata('tahunajaran') == '20161')) {
		        $absen 	= $this->db->query("SELECT count(npm_mahasiswa) AS jml FROM tbl_absensi_mhs 
		        							WHERE kd_jadwal = '".$rows->kd_jadwal."' 
		        							AND pertemuan = ".$i." and kehadiran != 'A' ")->row();

		    } elseif ($this->session->userdata('tahunajaran') == '20162') {
		    	$absen 	= $this->db->query("SELECT count(npm_mahasiswa) AS jml FROM tbl_absensi_mhs_new 
		    								WHERE kd_jadwal = '".$rows->kd_jadwal."' 
		    								AND pertemuan = ".$i." and kehadiran != 'A' ")->row();

		    } elseif ($rows->gabung > 0) {
		    	$absen 	= $this->db->query("SELECT count(npm_mahasiswa) AS jml FROM tbl_absensi_mhs_new_20171 
		    								WHERE kd_jadwal IN ('$key->kd_jadwal','$rows->kd_jadwal') 
		    								AND pertemuan = ".$i." and kehadiran != 'A' ")->row();
		    
		    } else {
		        $absen 	= $this->db->query("SELECT count(npm_mahasiswa) AS jml FROM tbl_absensi_mhs_new_20171 
		        							WHERE kd_jadwal = '".$rows->kd_jadwal."' 
		        							AND pertemuan = ".$i." 
		        							AND kehadiran != 'A' ")->row();
		    }
			
			if ($absen == true) {
				//$pdf->Cell(11,5,'',1,0,'C');
				$pdf->Cell(11,5,$absen->jml,1,0,'C');
			} else {
				$pdf->Cell(11,5,'',1,0,'C');
			}
		}

		// dd($ararabsen, 2);

		$pdf->ln(5);

		$pdf->Cell(113,5,'Paraf Dosen',1,0,'R');



		for ($i=0; $i < 16; $i++) { 

			$pdf->Cell(11,5,'','L,R,B',0,'C');	

		}

		$pdf->ln(8);

		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,'Biro Administrasi Akademik',0,0,'C');
		$x = $pdf->GetX();
    	$y = $pdf->GetY();
		//$pdf->image('http://172.16.1.5:801/assets/ttd_baa.png',($x-30),($y+2),27);
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,'Kepala Program Studi',0,0,'C');

		$pdf->ln(20);
		$pdf->Cell(110,5,'',0,0,'C');
		$pdf->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
		$pdf->Cell(50,5,'',0,0,'C');
		$pdf->Cell(113,5,'(.............................................................)',0,0,'C');

		$noo = 1;

		$pdf->AliasNbPages();

$pdf->AddPage();



$pdf->SetMargins(3, 3 ,0);

$pdf->SetFont('Arial','B',10); 



//$pdf->Image(''.base_url().'assets/img/logo-albino.png',60,30,90);

$pdf->Ln(0);

if ($years < 20192) {
	$pdf->Cell(200,5,'FAKULTAS '.strtoupper(get_fak_byprodi($kodeprodi)),0,0,'L');
}elseif (is_null($rows->gabung)) {
	$pdf->Cell(200,5,'FAKULTAS '.strtoupper(get_fak_byprodi($kodeprodi)),0,0,'L');
} else {
	$pdf->Cell(200,5,'FAKULTAS '.strtoupper(get_fak($kodeprodi)),0,0,'L');
}


$pdf->Ln(4);

$pdf->SetFont('Arial','',10); 

if ($years < 20192) {
	$pdf->Cell(200,5,''.strtoupper(get_jur($kodeprodi)).' - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');
} else if (is_null($rows->gabung)) {
	$pdf->Cell(200,5,''.strtoupper(get_jur($kodeprodi)).' - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'L');
}else {
	$pdf->Cell(200,5,''.strtoupper('UNIVERSITAS BHAYANGKARA JAKARTA RAYA'),0,0,'L');
}

$pdf->Ln(4);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS I',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Dharmawangsa III No.1,Kebayoran Baru, Jakarta Selatan ',0,0,'L');

$pdf->Ln(2);



$pdf->SetFont('Arial','',6);

$pdf->Cell(13,3,'KAMPUS II',0,0,'L');

$pdf->Cell(3,3,' : ',0,0,'L');

$pdf->Cell(140,3,'Jl. Raya Perjuangan, Bekasi Barat',0,0,'L');

$pdf->Ln(4);

$pdf->Cell(290,0,'',1,0,'C');



$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'KODE MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->kd_matakuliah ,0,0,'L');

$pdf->Cell(15,5,'Smtr/Thn',0,0,'L');

if (substr($rows->kd_tahunajaran, 4)) {
	$ta = 'Ganjil';
} else {
	$ta = 'Genap';
}

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->semester_matakuliah.' / '.substr($rows->kd_tahunajaran, 0, 4).' - '.$ta,0,0,'L');

$pdf->Cell(20,5,'NAMA DOSEN',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,nama_dsn($rows->kd_dosen),0,0,'L');

$pdf->Cell(20,5,'KELAS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kelas,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'NAMA MK',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->nama_matakuliah,0,0,'L');

$pdf->Cell(15,5,'SKS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->sks_matakuliah,0,0,'L');

$pdf->Cell(20,5,'NID',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kd_dosen,0,0,'L');

$pdf->Cell(20,5,'Kuota',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,$rows->kuota,0,0,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',8);

$pdf->Cell(15,5,'RUANG',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(60,5,$rows->kode_ruangan,0,0,'L');

$pdf->Cell(15,5,'WAKTU',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,notohari($rows->hari).'/'.del_ms($rows->waktu_mulai).'-'.del_ms($rows->waktu_selesai),0,0,'L');

$pdf->Cell(20,5,'KAMPUS',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

$pdf->Cell(50,5,'Bekasi',0,0,'L');

$pdf->Cell(20,5,'Jumlah Peserta',0,0,'L');

$pdf->Cell(2,5,':',0,0,'L');

//$jumlah = $this->db->query("select count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = '".$rows->kd_jadwal."'")->row()->mhs;

$pdf->Cell(50,5,$jumlah.' orang',0,0,'L');



$pdf->ln(10);

$pdf->SetLeftMargin(5);

$pdf->SetFont('Arial','',12);

$pdf->Cell(289,8,'DAFTAR HADIR PESERTA KULIAH',1,0,'C');

$pdf->ln(8);



$pdf->SetFont('Arial','',8);

$pdf->Cell(8,10,'NO','L,T,R,B',0,'C');

$pdf->Cell(25,10,'NPM','L,T,R,B',0,'C');

$pdf->Cell(80,10,'NAMA','L,T,R,B',0,'C');

$pdf->Cell(176,5,'PERTEMUAN KULIAH','L,T,R,B',0,'C');

$pdf->ln(5);

$pdf->Cell(8,0,'',0,0,'C');

$pdf->Cell(25,0,'',0,0,'C');

$pdf->Cell(80,0,'',0,0,'C');



for ($i=1; $i < 17; $i++) { 

	$pdf->Cell(11,5,$i,1,0,'C');	

}
	}

	//ending header

}



$pdf->ln(5);

$pdf->Cell(113,5,'Tanggal Kuliah',1,0,'R');
$pdf->SetFont('Arial','',7);
for ($i=1; $i < 17; $i++) { 
	if (($this->session->userdata('tahunajaran') == '20152') or ($this->session->userdata('tahunajaran') == '20161')) {
        $absen = $this->db->query("select tanggal from tbl_absensi_mhs where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." ")->row();
    } elseif ($this->session->userdata('tahunajaran') == '20162') {
    	$absen = $this->db->query("select tanggal from tbl_absensi_mhs_new where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." ")->row();
    }elseif ($rows->gabung > 0) {
    	$absen = $this->db->query("select tanggal from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$key->kd_jadwal."' and pertemuan = ".$i." ")->row();
    } else {
        $absen = $this->db->query("select tanggal from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." ")->row();
    }
	
	//$pdf->Cell(11,5,'',1,0,'C');
	if ($absen == true) {
		$pdf->Cell(11,5,date("d/m/y", strtotime($absen->tanggal)),1,0,'C');
	} else {
		$pdf->Cell(11,5,'',1,0,'C');
	}
}
$pdf->SetFont('Arial','',8);

$pdf->ln(5);

$pdf->Cell(113,5,'Jumlah Hadir',1,0,'R');


for ($i=1; $i < 17; $i++) { 
	if (($this->session->userdata('tahunajaran') == '20152') or ($this->session->userdata('tahunajaran') == '20161')) {
        $absen = $this->db->query("select count(npm_mahasiswa) as jml from tbl_absensi_mhs where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." and kehadiran != 'A' ")->row();
    } elseif ($this->session->userdata('tahunajaran') == '20162') {
    	$absen = $this->db->query("select count(npm_mahasiswa) as jml from tbl_absensi_mhs_new where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." and kehadiran != 'A' ")->row();
    }elseif ($rows->gabung > 0) {
    	$absen = $this->db->query("select count(npm_mahasiswa) as jml from tbl_absensi_mhs_new_20171 where kd_jadwal IN ('$key->kd_jadwal','$rows->kd_jadwal') and pertemuan = ".$i." and kehadiran != 'A' ")->row();
    } else {
        $absen = $this->db->query("select count(npm_mahasiswa) as jml from tbl_absensi_mhs_new_20171 where kd_jadwal = '".$rows->kd_jadwal."' and pertemuan = ".$i." and kehadiran != 'A' ")->row();
    }
	
	if ($absen == true) {
		//$pdf->Cell(11,5,'',1,0,'C');
		$pdf->Cell(11,5,$absen->jml,1,0,'C');
	} else {
		$pdf->Cell(11,5,'',1,0,'C');
	}
}



$pdf->ln(5);

$pdf->Cell(113,5,'Paraf Dosen',1,0,'R');



for ($i=0; $i < 17; $i++) { 

	$pdf->Cell(11,5,'','L,R,B',0,'C');	

}

$pdf->ln(8);
		
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,'Biro Administrasi Akademik',0,0,'C');
$x = $pdf->GetX();
$y = $pdf->GetY();
//$pdf->image('http://172.16.1.5:801/assets/ttd_baa.png',($x-30),($y+2),27);
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,'Kepala Program Studi',0,0,'C');

$pdf->ln(20);
$pdf->Cell(110,5,'',0,0,'C');
$pdf->Cell(30,5,'ROULY G RATNA S, ST., MM',0,0,'C');
$pdf->Cell(50,5,'',0,0,'C');
$pdf->Cell(113,5,'(.............................................................)',0,0,'C');



$pdf->Output('ABSENSI.PDF','I');


?>

