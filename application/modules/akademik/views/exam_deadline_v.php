<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css"/>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>

<?php $sess = $this->session->userdata('sess_login'); ?>

<script type="text/javascript">
    $(document).ready(function(){
        $( "#deadline" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '<?= date('Y') ?>:<?= date('Y')+1; ?>',
            dateFormat: 'yy-mm-dd'
        });
    });

    function add() {
        document.getElementById('title').innerHTML = 'Tambah Data';
        document.getElementById('forms').reset();
    }

    function edit(id) {
        document.getElementById('title').innerHTML = 'Ubah Data';
        $.get('<?= base_url('akademik/jadwal_ujian/get_deadline/') ?>' + id, function(res) {
            var data = JSON.parse(res);
            document.getElementById('tahunajaran').value = data.tahunajaran;
            document.getElementById('deadline').value = data.deadline;
            document.getElementById('tipe').value = data.tipe;
            document.getElementById('interval').value = data.interval;
            document.getElementById('is_updated').value = id;
        })
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-calendar"></i>
                <h3>Atur Tanggal Akhir Ujian</h3>
            </div>

            <div class="widget-content">
                <a class="btn btn-warning" href="<?= base_url('akademik/jadwal_ujian') ?>">
                    <i class="icon icon-chevron-left"></i> Kembali
                </a>

                <button class="btn btn-primary" onclick="add()" data-target="#addmod" data-toggle="modal">
                    <i class="icon icon-plus"></i> Tambah Data
                </button>

                <div class="pull-right">
                    <button class="btn btn-danger" data-target="#inmod" data-toggle="modal">
                        <i class="icon icon-info"></i> Informasi
                    </button>
                </div>

                <hr>

                <div class="span11">

                    <table id="example1" class="table table-stripped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tahun Ajaran</th>
                                <th>Tipe Ujian</th>
                                <th>Tanggal Akhir Ujian</th>
                                <th>Rentang Waktu</th>
                                <th>Batas Pengisian Nilai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
						
                            <?php $no = 1; foreach ($data as $key) { ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $key->tahun_akademik; ?></td>
                                    <td><?= $key->tipe_uji == 1 ? 'UTS' : 'UAS'; ?></td>
                                    <td><?= $key->deadline_uji; ?></td>
                                    <td><?= $key->deadline_interval; ?> hari</td>
                                    <td><?= $key->deadline_scoring; ?></td>
                                    <td>
                                        <button 
                                            data-target="#addmod" 
                                            data-toggle="modal" 
                                            onclick="edit(<?= $key->id; ?>)" 
                                            class="btn btn-warning">
                                            <i class="icon icon-pencil"></i>
                                        </button>
                                        <a 
                                            onclick="return confirm('Yakin ingin menghapus data ini?')"
                                            href="<?= base_url('akademik/jadwal_ujian/remove_deadline/'.$key->id) ?>"
                                            class="btn btn-danger">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add modal -->
<div class="modal fade" id="addmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="contaddmod">
            <form action="<?= base_url('akademik/jadwal_ujian/store_exam_deadline'); ?>" method="post" id="forms">
                <div class="modal-header">
                    <h4 class="modal-title" id="title"></h4>
                </div>
                <input type="hidden" name="is_updated" value="" id="is_updated">
                <div class="modal-body">
                    <div class="control-group">
                        <label class="control-label">Tahun Akademik</label>
                        <div class="controls">
                            <select class="form-control span5" name="tahunajaran" id="tahunajaran">
                                <option disabled="" selected="">-- Pilih Tahun Akademik --</option>
                                <?php foreach ($tahunajaran as $value) : ?>
                                    <option value="<?= $value->kode ?>"><?= $value->tahun_akademik ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Tipe Ujian</label>
                        <div class="controls">
                            <select class="form-control span5" name="tipe" id="tipe">
                                <option disabled="" selected="">-- Pilih Tipe Ujian --</option>
                                <option value="1">UTS</option>
                                <option value="2">UAS</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Tanggal Akhir Ujian</label>
                        <div class="controls">
                            <input class="form-control span5" name="deadline" id="deadline">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Rentang Waktu Pengisian Nilai</label>
                        <div class="controls">
                            <input class="form-control span5" onkeypress="return isNumber(event)" name="interval" id="interval">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- info modal -->
<div class="modal fade" id="inmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Informasi Fitur</h4>
            </div>
            <div class="modal-body">                
                <p>Kolom <b>rentang waktu pengisian nilai</b> cukup diisi oleh angka. Nilai tersebut akan menjadi penentu tanggal terakhir pengisian nilai.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>