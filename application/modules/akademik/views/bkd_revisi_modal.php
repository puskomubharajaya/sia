<div class="modal-header">
	<h3>Revisi Data Penelitian</h3>
</div>

<div class="modal-body">
	<form class="form form-horizontal" id="frmRevisi" method="post" action="<?php echo base_url('akademik/beban-kinerja-dosen/revisi') ?>">
		<input type="hidden" name="id" value="<?php echo $research->id ."-".date('mYdiHs')."-".$research->user?>">
    	<div class="control-group">
			<label class="control-label">Tahun Akademik</label>
			<div class="controls">
				<select class="form-control span3" name="tahunakademik" id="devYear" required disabled>
					<!-- <option disabled selected value="">-- Pilih Tahun Ajaran --</option> -->
					<?php foreach ($yearAcademic as $key) {
						echo '<option value="'.$key->kode.'">'.$key->tahun_akademik.'</option>  ';
					} ?>
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Judul Penelitian</label>
			<div class="controls">
				<input 
					type="text" 
					name="rscTitle" 
					id="rscTitle" 
					placeholder="Masukan judul penelitian" 
					required="" 
					value="<?php echo $research->judul?>" 
					class="form-control span3" />
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Program Penelitian</label>
			<div class="controls">
				<select class="form-control span4" name="rscProgram" id="rscProgram" required>
					<option disabled selected value="">-- Pilih Program --</option>
					<?php foreach ($programPenelitian as $key => $value) {
						if ($research->program = $value->program) {
							echo '<option value="'.$value->kode_program.'" selected>'.$value->program.'</option>  ';
						}else{
							echo '<option value="'.$value->kode_program.'">'.$value->program.'</option>  ';
						}
					} ?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Kegiatan</label>
			<div class="controls">
				<select class="form-control span3" name="rscActivity" id="rscActivity" required>
					<?php foreach ($activity as $key => $value): ?>
						<option value="<?php echo $value->kode_kegiatan?>"
							<?php if ($value->kode_kegiatan == $research->kegiatan): ?>
								selected
							<?php endif ?>
							><?php echo $value->kegiatan ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" id="param-type">Peran</label>
			<div class="controls">
				<select class="form-control span3" name="rscParam" id="rscParam" required>
					<option value="<?php echo $peran->kode ?>"><?php echo $peran->param ?></option>
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Beban SKS</label>
			<div class="controls">
				<input 
					type="text" 
					value="<?php echo $research->sks ?>" 
					id="rscCredit" 
					readonly="" 
					name="rscCredit" 
					class="form-control span3" />
			</div>
		</div>
    </form>
</div>
<div class="modal-footer">
	<div class="control-group">
		<div class="controls pull-left span3" style="width: padding-left:25px;"><button id="submitRevisi" class="btn btn-primary" type="submit">Submit</button></div>
	</div>
</div>


<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
		// get kegiatn program
		$('#rscProgram').change(function(){
			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/get_program_activity/'+$(this).val(),{},function(get){
				$('#rscActivity').html(get);
				$('#rscCredit').val("");
			});
		});
		// get parameter program
		$('#rscProgram').change(function(){
			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/program_params/'+$(this).val(),{},function(get){
				var parseData = JSON.parse(get);
				$('#param-type').html(parseData.param);
				$('#rscParam').html(parseData.options);
			});
		});

		// set SKS by param
		$('#rscProgram,#rscActivity,#rscParam').change(function(){
			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/set_sks/'+$('#rscProgram option:selected').val()+'/'+$('#rscActivity').val()+'/'+$('#rscParam').val(),{},function(get){
				$('#rscCredit').val(get);
			});
		});

		$("#submitRevisi").on('click', function () {
			$("#devYear").prop('disabled', false)
			$("#frmRevisi").submit()
		})
	})

	function getTable() {
		$.post('<?= base_url(); ?>akademik/beban_kinerja_dosen/loadTable/', function(data) {
		  	$('#appearHere').html(data);
		});
	}

	$(function () {
		$('#addRsc').click(function (e) {
			if ($('#rscTitle,#rscProgram,#rscActivity,#rscParam,#rscCredit').val() === '') {
				alert('Tidak boleh ada kolom yang dikosongkan!');
				return;
			}

			$('#submitBtn').removeAttr('disabled')

			$.ajax({
				type: 'POST',
				url: '<?= base_url('akademik/beban_kinerja_dosen/temporaryResearch'); ?>',
				data: $('#temporaryform').serialize(),
				error: function (xhr, ajaxOption, thrownError) {
					return false;
				},
				success: function () {
					getTable();
					$('#toRemove').hide();
					$('#rscTitle,#rscProgram,#rscActivity,#rscParam,#rscCredit').val('');
				}
			});
			e.preventDefault();
		});
	});

	function rmData(id) {
	    $.ajax({
	        type: 'POST',
	        url: '<?= base_url('akademik/beban_kinerja_dosen/deleteList/');?>'+id,
	        error: function (xhr, ajaxOptions, thrownError) {
	            return false;           
	        },
	        success: function () {
	            getTable();
	            if ($('.additionalRow').length === 1) {
	            	$('#submitBtn').attr("disabled","");
	            }
	        }
	    });
	}
</script>