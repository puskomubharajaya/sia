<?php 
//var_dump($matkul);die();
ob_start();

// class PDF extends FPDF
// {
// 	function Footer()
//     {
//         // Go to 1.5 cm from bottom
// 	    $this->SetY(-10);
// 	    // Select Arial italic 8
// 	    $this->ln(0);
// 		$this->SetFont('Arial','',8);
// 		$this->Cell(10,0,'Menyetujui,',0,0,'L');

// 		$this->ln(8);
// 		$this->SetFont('Arial','',8);
// 		$this->Cell(135,0,'Ka.Prodi / Ka.bag TU',0,1,'L');
		
// 		$this->ln(10);
// 		$this->SetFont('Arial','',8);
// 		$this->Cell(135,0,'1.',0,0,'L');
// 		$this->Cell(135,0,'(...................................)',0,1,'L');
// 	}
// }


$pdf = new FPDF("P","mm", "A5");
$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 5 ,0);


$pdf->Ln(0);

$pdf->SetFont('Arial','B',8);

$pdf->image('http://172.16.1.5:801/assets/logo.gif',5,5,18);

$pdf->Ln();

$pdf->Ln();

$pdf->SetFont('Arial','B',12);

$pdf->Cell(40,5,'',0,'C');

$pdf->Cell(5,5,'KARTU SEMESTER PERBAIKAN',0,'C');

$pdf->Ln();

$pdf->Cell(28,5,'',0,'C');

$pdf->Cell(5,5,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA ',0,'C');
$pdf->Line(4,25,142,25);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','',8);

$pdf->Cell(20,5,'FAKULTAS',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,get_fak_byprodi($prodi),0,'C');

$pdf->Cell(23,5,'SEMESTER',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(5,5,$this->app_model->get_semester($smtr->SMAWLMSMHS),0,'C');

$pdf->Ln();

$pdf->Cell(20,5,'Program Studi',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,get_jur($prodi),0,'C');

$pdf->Cell(23,5,'Tahun Akademik',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(5,5,$tahun.' - '.$ganjilgenap,0,'C');

$pdf->Ln();

$pdf->SetFont('Arial','B',8);

$pdf->Cell(85,5,'Identitas Mahasiswa',0,'C');

$pdf->SetFont('Arial','B',8);

$pdf->Cell(20,5,'Identitas Dosen',0,'C');

$pdf->Ln();

$pdf->SetFont('Arial','',8);

$pdf->Cell(20,5,'NAMA',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,$info_mhs->NMMHSMSMHS,0,'C');

$pdf->Cell(23,5,'Nama Dosen PA',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->MultiCell(35,5,$dosen->nama,0,'L');

$pdf->Ln();

$pdf->Cell(20,5,'NPM',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(60,5,$info_mhs->NIMHSMSMHS,0,'C');

$pdf->Cell(23,5,'NID',0,'C');

$pdf->Cell(5,5,':',0,'C');

$pdf->Cell(5,5,$dosen->nid,0,'C');

$pdf->ln();

$pdf->ln();

$pdf->SetFont('Arial','B',8);
$pdf->Cell(5,10,'NO','L,T,R,B',0,'C');
$pdf->Cell(15,10,'KD MK','L,T,R,B',0,'C');
$pdf->Cell(40,10,'MK','L,T,R,B',0,'C');
$pdf->Cell(8,10,'SKS','L,T,R,B',0,'C');
$pdf->Cell(40,10,'Nama Dosen','L,T,R,B',0,'C');
$pdf->Cell(33,10,'Jadwal Kuliah','L,T,R,B',0,'C');

$pdf->ln();
$no=0;
$sks = 0;

	foreach ($matkul as $isi) {
		$no++;
		$this->db->select('*');
		$this->db->where('NIMHSTRLNM', $info_mhs->NIMHSMSMHS);
		$this->db->where('KDKMKTRLNM', $isi->kd_matakuliah);
		$this->db->order_by('id','desc');
		$nilai_lama=$this->db->get('tbl_transaksi_nilai',1)->row();

		$pdf->SetFont('Arial','',6);

		$pdf->Cell(5,5,$no,'L,T,R,B',0,'C');

		$pdf->Cell(15,5,$isi->kd_matakuliah,'L,T,R,B',0,'C');

		$pdf->Cell(40,5,$isi->nama_matakuliah,'L,T,R,B',0,'C');

		$pdf->Cell(8,5,$isi->sks_matakuliah,'L,T,R,B',0,'C');

		$pdf->Cell(40,5,$isi->nama,'L,T,R,B',0,'C');

		$pdf->Cell(33,5,notohari($isi->hari).' / '.$isi->waktu_mulai.' - '.$isi->waktu_selesai,'L,T,R,B',1,'C');

		$sks = $sks + $isi->sks_matakuliah;
	}
	

// $pdf->SetFont('Arial','',8);

// $pdf->Cell(47,5,'',0,'C');

// $pdf->Cell(10,5,'Jumlah SKS    '.$sks,0,'C');


// $pdf->ln(5);

// $pdf->SetFont('Arial','',8);

// $pdf->Cell(47,5,'Biaya yang harus dibayar :',0,'L');


// $pdf->ln();

// $pdf->SetFont('Arial','',8);

// $pdf->Cell(8,5,'',0,'C');

// $pdf->Cell(15,5,'1. Biaya SKS',0,'C');

// $pdf->Cell(5,5,':',0,'C');

// $pdf->Cell(8,5,$sks,0,'C');

// $pdf->Cell(3,5,'x',0,'C');

// $pdf->Cell(25,5,'@Rp. 100.000,-  =',0,'C');

// $biayasks = $sks*100000;

// $pdf->Cell(5,5,'Rp. '.number_format($biayasks),0,'C');

// $pdf->ln();

// $pdf->SetFont('Arial','',8);

// $pdf->Cell(8,5,'',0,'C');

// $pdf->Cell(15,5,'2. Biaya Ujian',0,'C');

// $pdf->Cell(5,5,':',0,'C');

// $pdf->Cell(8,5,$no,0,'C');

// $pdf->Cell(3,5,'x',0,'C');

// $pdf->Cell(25,5,'@Rp. 100.000,-  =',0,'C');

// $biayaujian = $no*100000;

// $pdf->Cell(5,5,'Rp. '.number_format($biayaujian),0,'C');

// $pdf->ln();

// $pdf->SetFont('Arial','',8);

// $pdf->Cell(8,5,'',0,'C');

// $pdf->Cell(53,5,'3. Jumlah yang harus dibayar',0,'C');

// $pdf->Cell(3,5,'=',0,'C');

// $jml_biaya=$biayasks+$biayaujian;

// $pdf->Cell(5,5,'Rp. '.number_format($jml_biaya),0,'C');


// $pdf->ln();

// $pdf->SetFont('Arial','',7);

// $pdf->Cell(8,5,'Keterangan :',0,'C');

// $pdf->ln();

// $pdf->Cell(10,5,'-',0,'C');

// $pdf->Cell(53,5,'Pembayaran Transfer ke BRIVA Ataupun ke Rek. BRI a.n. Univ. Bhayangkara 121101 0000 94 306 ',0,'C');

// $pdf->ln();

// $pdf->Cell(10,5,'-',0,'C');

// $pdf->Cell(53,5,'Bukti Transfer di Foto-Copy 2 Lembar',0,'C');

// $pdf->ln();

// $pdf->Cell(10,10,'',0,'C');

// $pdf->Cell(53,5,'1 Untuk Mahasiswa Ybs., 1 Untuk Bukti Sekretariat Fakultas',0,'C');

// $pdf->ln();

// $pdf->Cell(10,10,'',0,'C');

// $pdf->Cell(53,5,'Yang Asli Diserahkan ke Renkeu',0,'C');

// $pdf->ln();


// Go to 1.5 cm from bottom
//$pdf->SetY(-47);
// Select Arial italic 8
$pdf->ln(15);

$pdf->SetFont('Arial','',8);

$pdf->Cell(11,0,'',0,'L');

$pdf->Cell(200,0,'Mengetahui,',0,1,'L');

$pdf->SetX(96);
$pdf->Cell(110,0,'Bekasi,'.date('d-m-Y').'',0,'L');

//$pdf->SetY(-37);

$pdf->ln(4);



$pdf->Cell(87,0,'Dosen Pembimbing Akademik',0,'L');
//$info_mhs->NMMHSMSMHS
$pdf->SetX(96);
$pdf->Cell(110,0,'Mahasiswa Ybs,',0,'C');


$pdf->ln(24);

$pdf->SetFont('Arial','',8);

$pdf->Cell(2,0,'',0,'L');

$pdf->Cell(87,0,'( '.$dosen->nama.' )',0,'C');

$pdf->SetX(96);
$pdf->Cell(87,0,'( '.$info_mhs->NMMHSMSMHS.' )',0,'c');


$pdf->ln(16);

$pdf->SetFont('Arial','',8);

$pdf->SetX(63);
$pdf->Cell(110,0,'Menyetujui,',0,'C');
$pdf->ln(4);
$pdf->SetX(60);
$pdf->Cell(110,0,'Kaprodi/Wadek I,',0,'C');

$pdf->ln(26);
$pdf->SetX(55);
$pdf->Cell(110,0,'(.......................................)',0,'L');




		

$pdf->Output('SEMESTER_PENDEK_'.$tahun.'_'.$info_mhs->NMMHSMSMHS.'.PDF','I');
?>