<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    
    <meta name="description" content="Sistem Informasi Akademik Ubhara Jaya">
    <meta name="keywords" content="SIA,UBJ,SIA UBJ,SIAKAD UBJ">
    <meta name="author" content="Puskom UBJ">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <title>SISTEM INFORMASI AKADEMIK UBHARA JAYA</title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/temp_adm/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  	<style type="text/css">
  		.ipt {
  			background-color: transparent !important;
  		}

  		.log-form {
  			box-shadow: 1px 1px 5px 1px white;
  			background-color:rgba(204,255,153,0.2) !important;
  		}

		::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
		    color: white !important;
		    opacity: 1; /* Firefox */
		}

		.spn {
			color: white;
			font-size: 15px;
		}

		.ftn {
			text-shadow: 1px 1px 2px #404040;
		}
  	</style>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	<div id="login-page" style="padding-bottom:50px">
	  	<div class="container">
	  		<!-- <center>
	  			<a href="#" style="color:#f4f2f2;hover:#eae9e9">
		  			<div style="margin-top:80px;">
		  				<h3>SISTEM INFORMASI AKADEMIK UBHARA JAYA</h3>
		  				
		  			</div>
	  			</a>
	  		</center> -->
	  		
		    <form class="form-login ipt log-form" action="<?php echo base_url();?>auth/login" method="post" style="margin-top:30px;">
		        <h2 class="form-login-heading ipt">
		        	<img src="<?php echo base_url();?>assets/logo.png" style="width:35%;margin-top:-12px;margin-left:-1px;margin-bottom:-9px;padding-right:10px;"><br><br>
		        	SISTEM INFORMASI AKADEMIK UBHARA JAYA
		        	<!-- <img src="<?php echo base_url();?>assets/img/puskom.png" style="width:20%;margin-top:-12px;margin-left:10px;margin-bottom:-9px;padding-right:10px;"> -->
		        </h2>
		        <div class="login-wrap">
		        	<!-- <span class="help-block spn">Username</span> -->
		            <input type="text" name="username" class="form-control ipt" placeholder="Username" autofocus required>
					<br>
					<!-- <span class="help-block spn">Password</span> -->
		            <input type="password" name="password" class="form-control ipt" placeholder="Password" required>
		            <br>
		            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
		            <br>
		            <a href="<?php echo base_url();?>auth/pemulihan" class="ftn" style="color: white"><p>Lupa Password?</p></a>
		            <hr>
		            <div class="login-social-link centered">
		            	<p class="ftn" style="color: white">Hubungi Dir. PTI jika terjadi kendala</p>
		            </div>
		        </div>
		    </form>	
		    <br>
		    <br>
		    <center>
		    	<img src="<?php echo base_url();?>assets/img/puskom.png" style="width:5%;margin-top:-12px;margin-left:10px;margin-bottom:-9px;padding-right:10px;">
		    	<br>
		    	<a href="<?php echo base_url(); ?>" style="color:#f4f2f2;hover:#eae9e9">
		    		<h5 class="ftn">&copy Dir. PTI - <?php echo date('Y'); ?></h5>  
		    	</a>
		    </center>
		    
		    <center>
		    	<a href="http://ubharajaya.ac.id/" target="blank" style="color:#f4f2f2;hover:#eae9e9">
		    		<h5>UNIVERSITAS BHAYANGKARA JAKARTA RAYA</h5>  
		    	</a>
		    </center>
		    
	  	</div>
	</div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/temp_adm/assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/temp_adm/assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/temp_adm/assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?php echo base_url(); ?>assets/img/1280x1024.jpg", {speed: 500});
    </script>


  </body>
</html>
