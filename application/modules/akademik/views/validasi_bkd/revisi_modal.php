<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="revisiModalLabel">Revisi Penelitian</h3>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>akademik/validasi-bkd/revisi" method="post" enctype="multipart/form-data">
    <div class="modal-body" style="margin-left: -30px;">  
        <div class="control-group" id="">
            <label class="control-label">Catatan</label>
            <div class="controls">
                <textarea class="span4" name="note" placeholder="Catatan Untuk Revisi" class="form-control" value="" required></textarea>
            </div>

            <input type="hidden" name="lrid" value="<?php echo $id?>">
            <input type="hidden" name="type" value="<?php echo $type?>">
        </div>
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" id="submit" value="Simpan"/>
    </div>
</form>

<script type="text/javascript">
    $(function () {
        $("#submit").click(function (e) {
            if (confirm("Yakin ingin merevisi")) {
                $("form#submit").submit();
            }else{
                e.preventDefault();
            }
        })
    })
</script>