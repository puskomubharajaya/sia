<style>
	.progress-bar {
		float: left;
		width: 0%;
		height: 100%;
		font-size: 12px;
		line-height: 20px;
		color: #fff;
		text-align: center;
		background-color: #337ab7;
		-webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
		box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
		-webkit-transition: width .6s ease;
		-o-transition: width .6s ease;
		transition: width .6s ease;
	}
</style>

<div class="container">	
    <div class="row">
      	<div class="span12">   
      		<div class="widget ">
      			<div class="widget-header">
      				<i class="icon-file-text"></i>
      				<h3>Beban Kinerja Dosen</h3>
  				</div> 
				<div class="widget-content">

					<div class="progress">
						<div 
							class="progress-bar" 
							role="progressbar" 
							aria-valuenow="70"
							aria-valuemin="0" 
							aria-valuemax="100" style="width:70%">
							<b>70%</b> <i>(Divalidasi oleh SPM)</i>
						</div>
					</div> 

					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#home">Pendidikan dan Pengajaran</a></li>
					  	<li><a data-toggle="tab" href="#rsc" onclick="load_tab('rsc')">Penelitian</a></li>
					  	<li><a data-toggle="tab" href="#dev" onclick="load_tab('dev')">Pengabdian</a></li>
					  	<li><a data-toggle="tab" href="#oth" onclick="load_tab('oth')">Tugas Tambahan</a></li>
					  	<li><a data-toggle="tab" href="#frm" onclick="load_tab('frm')">Formulir BKD</a></li>
					</ul>

					<div class="tab-content">

						<div id="home" class="tab-pane fade in active">
							<?php $this->load->view('bkd_teaching_v'); ?>
					  	</div>

					  	<div id="rsc" class="tab-pane fade">
					  	</div>

					  	<div id="dev" class="tab-pane fade">
					  	</div>


					  	<div id="oth" class="tab-pane fade">					  		
					  	</div>

					  	<div id="frm" class="tab-pane fade">					  		
					  	</div>

					</div>
				</div>
			</div>
	    </div>
    </div>
</div>

<script>
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	function load_tab (arg) {
		$('#'+arg).load('<?= base_url('akademik/beban_kinerja_dosen/load_tab/') ?>' + arg);
	}
</script>