<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Rencana Studi</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<table>
						<tr>
							<td>Keterangan warna</td>
							<td>:</td>
							<td>
								<button class="btn" style="background: #DC143C; color: white">Form dispensasi</button>
								<button class="btn" style="background: #90EE90">KRS terverifikasi</button>
								<button class="btn" style="background: #f2eea7">KRS terajukan</button>
								<button class="btn" style="background: #FFB6C1">KRS belum diverivikasi</button>
							</td>
						</tr>
					</table>
					<hr>
					<!-- <a href="<?php echo base_url();?>akademik/khs/printkhsall"></a> -->
					<!-- <a class="btn btn-success btn-small" target="_blank" href="<?php echo base_url();?>akademik/khs/printkhsall ?>"><i class="btn-icon-only icon-print"> Cetak KHS  </i></a> -->
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>NIM</th>
	                        	<th>Nama MHS</th>
	                        	<th>Tahun Masuk</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php $no=1; foreach ($getData as $value) { ?>
	                    	<?php 
		                    	$warna = ''; 
		                    	$cekform3 = $this->app_model->getcekform3($value->NIMHSMSMHS, $tahunakademik)->row()->status_verifikasi; 
                    			
                    			if ($cekform3 == 10) {
                    				$warna = 'style="background:#DC143C;"';
                    			}

                    			if (($value->tahunajaran >= '20162') and ($value->status_verifikasi == '1')) {
                    				$tanda = 'style="background:#90EE90;"';
                    			} elseif (($value->tahunajaran >= '20162') and ($value->status_verifikasi == '4')) {
                    				$tanda = 'style="background:#f2eea7;"';
                    			} else {
                    				$tanda = 'style="background:#FFB6C1;"';
                    			}
	                    			
	                    	?>
	                        <tr <?php echo $warna; ?> <?php echo $tanda; ?>>
	                        	<td <?php echo $warna; ?> <?php echo $tanda; ?>><?php echo number_format($no); ?></td>
	                        	<td <?php echo $warna; ?> <?php echo $tanda; ?>><?php echo $value->NIMHSMSMHS; ?></td>
	                        	<td <?php echo $warna; ?> <?php echo $tanda; ?>><?php echo $value->NMMHSMSMHS; ?></td>
                                <td <?php echo $warna; ?> <?php echo $tanda; ?>><?php echo $value->TAHUNMSMHS; ?></td>
	                        	<td class="td-actions" <?php echo $warna; ?> <?php echo $tanda; ?>>
									<a class="btn btn-success btn-small" target="_blank" href="<?php echo base_url(); ?>akademik/viewkrs/viewkrsmhs_2/<?php echo $value->kd_krs; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								</td>
	                        </tr>
	                        <?php $no++;} ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>