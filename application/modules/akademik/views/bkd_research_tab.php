<fieldset>
	<h3>
		<i class="icon-beaker"></i> Tambah Data Penelitian
		<a href="<?=  base_url('akademik/beban_kinerja_dosen/research_list') ?>" data-toggle="tooltip" title="lihat rincian penelitian">
    		<i class="icon-eye-open"></i>
    	</a>
	</h3>
	<br>
    <form class="form-horizontal" id="temporaryform" method="post">
    	<div class="control-group">
			<label class="control-label">Tahun Akademik</label>
			<div class="controls">
				<select class="form-control span6" name="tahunakademik" id="year" required>
					<option disabled selected value="">-- Pilih Tahun Ajaran --</option>
					<?php foreach ($tahunakademik as $key) {
						echo '<option value="'.$key->kode.'">'.$key->tahun_akademik.'</option>  ';
					} ?>
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Judul Penelitian</label>
			<div class="controls">
				<input 
					type="text" 
					name="rscTitle" 
					id="rscTitle" 
					placeholder="Masukan judul penelitian" 
					required="" 
					class="form-control span6" />
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Program Penelitian</label>
			<div class="controls">
				<select class="form-control span6" name="rscProgram" id="rscProgram" required>
					<option disabled selected value="">-- Pilih Program --</option>
					<?php foreach ($programs as $program) {
						echo '<option value="'.$program->kode_program.'">'.$program->program.'</option>  ';
					} ?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Kegiatan</label>
			<div class="controls">
				<select class="form-control span6" name="rscActivity" id="rscActivity" required>
					<option disabled="" selected="" value="">-- Pilih Kegiatan --</option>
					
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" id="param-type">Peran</label>
			<div class="controls">
				<select class="form-control span6" name="rscParam" id="rscParam" required>
					<option disabled="" selected="" value="">-- Pilih Peran --</option>
					
				</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Beban SKS</label>
			<div class="controls">
				<input 
					type="text" 
					value="" 
					id="rscCredit" 
					readonly="" 
					name="rscCredit" 
					class="form-control span6" />
			</div>
		</div>
		
		<center>
			<button class="btn btn-success" id="addRsc"><i class="icon-plus"></i> Tambah Penelitian</button>
		</center>
    </form>

    <hr>

    <form class="form-horizontal" action="<?= base_url(); ?>akademik/beban_kinerja_dosen/add" method="post">
      <h3><i class="icon icon-list"></i> Daftar Penelitian</h3>
      <br>
      <table id="example99" class="table table-bordered table-striped">
        <thead>
              <tr> 
                <th>No</th>
                <th>Judul Penelitian</th>
                <th>Tahun Akademik</th>
                <th>Program</th>
                <th>Kegiatan</th>
                <th>Peran/Progress</th>
                <th>Beban SKS</th>
                <th width="80">Aksi</th>
              </tr>
          </thead>
          <tbody id="appearHere">
              <tr id="toRemove">
                <td colspan="8"><i>No data available</i></td>
              </tr>
          </tbody>
      </table>
      <div class="form-actions">
        <input class="btn btn-default btn-primary" type="submit" value="Submit" id="submitBtn" disabled="">
      </div>
    </form>
</fieldset>

<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
		// get kegiatn program
		$('#rscProgram').change(function(){
			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/get_program_activity/'+$(this).val(),{},function(get){
				$('#rscActivity').html(get);
			});
		});

		// get parameter program
		$('#rscProgram').change(function(){
			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/program_params/'+$(this).val(),{},function(get){
				var parseData = JSON.parse(get);
				$('#param-type').html(parseData.param);
				$('#rscParam').html(parseData.options);
			});
		});

		// set SKS by param
		$('#rscProgram,#rscActivity,#rscParam').change(function(){
			$.post('<?= base_url() ?>akademik/beban_kinerja_dosen/set_sks/'+$('#rscProgram').val()+'/'+$('#rscActivity').val()+'/'+$('#rscParam').val(),{},function(get){
				$('#rscCredit').val(get);
			});
		});
	})

	function getTable() {
		$.post('<?= base_url(); ?>akademik/beban_kinerja_dosen/loadTable/', function(data) {
		  	$('#appearHere').html(data);
		});
	}

	$(function () {
		$('#addRsc').click(function (e) {
			if ($('#rscTitle,#rscProgram,#rscActivity,#rscParam,#rscCredit').val() === '') {
				alert('Tidak boleh ada kolom yang dikosongkan!');
				return;
			}

			$('#submitBtn').removeAttr('disabled')

			$.ajax({
				type: 'POST',
				url: '<?= base_url('akademik/beban_kinerja_dosen/temporaryResearch'); ?>',
				data: $('#temporaryform').serialize(),
				error: function (xhr, ajaxOption, thrownError) {
					return false;
				},
				success: function () {
					getTable();
					$('#toRemove').hide();
					$('#rscTitle,#rscProgram,#rscActivity,#rscParam,#rscCredit').val('');
				}
			});
			e.preventDefault();
		});
	});

	function rmData(id) {
	    $.ajax({
	        type: 'POST',
	        url: '<?= base_url('akademik/beban_kinerja_dosen/deleteList/');?>'+id,
	        error: function (xhr, ajaxOptions, thrownError) {
	            return false;           
	        },
	        success: function () {
	            getTable();
	            if ($('.additionalRow').length === 1) {
	            	$('#submitBtn').attr("disabled","");
	            }
	        }
	    });
	}
</script>