<div class="modal-header">
	<h3>Kehadiran selama <?php echo $pertemuan ?> Pertemuan	</h3>
</div>
<div class="modal-body">
	<div class="col-lg-12">
		<table class="table table-bordered">
			<tr>
				<td width="30%">Hadir</td>
				<td width="5%">:</td>
				<td><?php echo $hadir ?></td>
			</tr>
			<tr>
				<td>Alpha</td>
				<td>:</td>
				<td><?php echo $alpha ?></td>
			</tr>
			<tr>
				<td>Sakit</td>
				<td>:</td>
				<td><?php echo $sakit ?></td>
			</tr>
			<tr>
				<td>Ijin</td>
				<td>:</td>
				<td><?php echo $ijin ?></td>
			</tr>
			<tr>
				<td>Persentase Kehadiran</td>
				<td>:</td>
				<td><?php echo $persentase ?> %</td>
			</tr>
		</table>
	</div>
</div>