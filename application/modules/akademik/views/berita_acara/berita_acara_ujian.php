<!DOCTYPE html>
<html>
<head>
	<title>Berita Acara Ujian</title>
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<style>
		* { 
		    box-sizing:border-box;
		}
		body {
		    margin: 0;
		    padding:0;
		}

		body, table tr th, table tr td{
			text-transform: capitalize;
		}
		.italic {
			font-style: italic;
			text-transform: none;
		}
		.clear-text, table tr td.clear-text{
			text-transform: none;
		}
		.text-inline {
			display: inline;
		}
		p{
			line-height: 2em;
		}
		img {
			height: 100px !important;
		}

		.clear-row tr td {
			border-top: none !important;	
		}
		.center {
			margin: 0 auto;
			padding: 60px;
			text-align: center;
		}
		.upper-text, table tr td.upper-text {
			text-transform: uppercase !important;
		}
		table tr th {
			text-align: center;
		}
	</style>
</head>
<body>
	<div class="center" style="padding-bottom: 10px;">
		<img src="<?php echo IMG_ALBINO?>" alt="logo" height="50px">
		<h2>berita acara</h2>
	</div>
	<p class="clear-text" style="text-indent: 0.5em;">Penanda tangan di bawah ini menyatakan bahwa:</p>
	<table class="table clear-row">
		<tr>
			<td width="150px">fakultas / prodi</td>
			<td width="10px">:</td>
			<td><?php echo $jadwal->fakultas ." / ". $jadwal->prodi ?></td>
		</tr>
		<tr>
			<td class="clear-text">Semester / TA / Kls
			<td>:</td>
			<td><?php echo $jadwal->smt ." / ". $jadwal->ta. " / ( ". $jadwal->kls ." )"?></td>
		<tr>
			<td>jenis ujian</td>
			<td>:</td>
			<td class="upper-text"><?php echo $jadwal->tipe ?></td>
		</tr>
		<tr>
			<td>mata kuliah</td>
			<td>:</td>
			<td><?php echo $jadwal->matkul; ?></td>
		</tr>
		<tr>
			<td>dosen penguji</td>
			<td>:</td>
			<td><?php echo $jadwal->dosen ?></td>
		</tr>
		<tr>
			<td>hari / tgl ujian</td>
			<td>:</td>
			<td><?php echo $jadwal->hari ." / ". $jadwal->tanggal; ?></td>
		</tr>
		<tr>
			<td>waktu</td>
			<td>:</td>
			<td class="upper-text"> <?php echo $jadwal->waktu ." / " ?> ruang:  <?php echo $jadwal->ruang ?></td>
		</tr>
		<tr>
			<td>jumlah peserta</td>
			<td>:</td>
			<td> <?php echo $jadwal->total_peserta ?> mahasiswa</td>
		</tr>
	</table>
	<p class="italic text-inline">Catatan khusus mengenai peserta ujian : </p>
	<table class="table " style="height: 320px !important;">
		<tr style="height: 230px;">
			<td><?php echo $jadwal->catatan ?></td>
		</tr>
	</table>
	<p class="clear-text text-inline">Demikian berita acara ini dibuat dengan sebenarnya, dan bila diperlukan bersedia memberikan kesaksian.</p>
	<p style="float: right !important;margin-left: 60%;">……………………… , …………………. <?php echo date('Y') ?></p>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="20px">no.</th>
				<th>nama pengawas</th>
				<th>jabatan</th>
				<th colspan="2">tanda tangan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1.</td>
				<td><?php echo $jadwal->pengawas1 ?></td>
				<td align="center">pengawas-1</td>
				<td>1.</td>
				<td></td>
			</tr>
			<tr>
				<td>2.</td>
				<td><?php echo $jadwal->pengawas2?></td>
				<td align="center">pengawas-2</td>
				<td></td>
				<td>2.</td>
			</tr>
		</tbody>
	</table>
	<p class="italic text-inline"><b>Catatan:</b></p>
	<div style="text-transform: none"><b>Satu lembar soal, berita acara dan daftar hadir ujian diserahkan ke fakultas <?php echo strtolower($jadwal->fakultas) ?></b></div>
</body>
</html>