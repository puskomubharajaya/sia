<div class="row">
  <div class="span12">
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Form Berita Acara</h3>
      </div> <!-- /widget-header -->

      <div class="widget-content">
        <form action="<?php echo base_url('akademik/ajar/store-ba') ?>" method="POST" class="form form-horizontal">
          <!-- Fakultas -->
          <div class="control-group">
            <label for="" class="control-label">Fakultas / Jurusan <span class="error">*</span></label>
            <div class="controls">
              <input type="text" name="fakultas" id="fakultas" placeholder="Fakultas" required value="<?php echo $fakultas ?>">
              <input type="text" name="prodi" id="prodi" placeholder="Jurusan" required value="<?php echo $prodi ?>">
            </div>
          </div>

          <!-- Semester -->
          <div class="control-group">
            <label for="" class="control-label">Semester / TA / Kls <span class="error">*</span></label>
            <div class="controls">
              <input type="text" class="ba-smt" name="smt" id="smt" placeholder="Semester" required value="<?php echo $semester ?>">
              <span class="garing">/</span>
              <input type="text" class="ba-ta" name="ta" id="ta" placeholder="TA" required value="<?php echo $ta ?>">
              <span class="garing">/</span>
              <input type="text" class="ba-kelas" name="kls" id="kls" placeholder="Kelas" required value="<?php echo $kelas ?>">
            </div>
          </div>

          <!-- Jenis Ujian -->
          <div class="control-group">
            <label for="" class="control-label">Jenis Ujian <span class="error">*</span></label>
            <div class="controls">
              <select name="tipe_uji" id="tipe_uji" class="form-control" required>
                <option value="" selected disabled> -- Pilih Tipe Ujian -- </option>
                <option value="1" <?php if ($tipe_uji == 1) echo "selected" ?> >UTS</option>
                <option value="2" <?php if ($tipe_uji == 2) echo "selected" ?> >UAS</option>
              </select>
            </div>
          </div>

          <!-- Mata kuliah -->
          <div class="control-group">
            <label for="" class="control-label">Mata Kuliah <span class="error">*</span></label>
            <div class="controls">
              <input type="text" class="ba-matkul" name="matkul" id="matkul" placeholder="Mata Kuliah" required value="<?php echo $matkulName ?>">
              <input type="hidden" name="kd_jadwal" value="<?php echo $kd_jadwal ?>">
            </div>
          </div>

          <!-- Dose Penguji -->
          <div class="control-group">
            <label for="" class="control-label">Dosen Penguji <span class="error">*</span></label>
            <div class="controls">
              <input type="text" name="dosen" id="dosen" placeholder="Dosen Penguji" required value="<?php echo $dosen ?>">
              <input type="hidden" name="kd_dosen" value="<?php echo $kd_dosen ?>">
            </div>
          </div>

          <!-- Hari -->
          <div class="control-group">
            <label for="" class="control-label">Hari / Tgl Ujian <span class="error">*</span></label>
            <div class="controls">
              <input type="text" class="ba-hari" name="hari" id="hari" placeholder="Hari" required value="<?php echo $hari ?>">
              <span class="garing">/</span> 
              <input type="date" class="ba-tanggal" name="tanggal" id="tanggal" placeholder="Tgl Ujian" required value="<?php echo $start_date ?>">
            </div>
          </div>

          <!-- Waktu -->
          <div class="control-group">
            <label for="" class="control-label">Waktu / Ruang <span class="error">*</span></label>
            <div class="controls">
              <input type="time" min="00:00" max="23:59" pattern="[0-9]{2}:[0-9]{2}" class="ba-waktu" name="waktu" id="waktu" placeholder="Waktu" required value="<?php echo $start_time ?>">
              <span class="garing">/</span>
              <input type="text" class="ba-ruang" name="ruang" id="ruang" placeholder="Ruang" required value="<?php echo $ruangan ?>">
            </div>
          </div>

          <!-- Jumlah Peserta -->
          <div class="control-group">
            <label for="" class="control-label">Jumlah Peserta <span class="error">*</span></label>
            <div class="controls">
              <input type="text" class="ba-peserta" name="jml_mhs" id="jml_mhs" placeholder="Jumlah Peserta" required value="<?php echo $peserta ?>">
              <span>Mahasiswa</span>
            </div>
          </div>

          <!-- Catatan -->
          <p style="text-indent: 2px;"><i>Catatan khusus mengenai mahasiswa peserta ujian</i></p>

          <div class="control-group">
            <label for="" class="control-label">Catatan Ujian</label>
            <div class="controls">
              <textarea name="catatan" class="ba-catatan" id="catatan"></textarea>
            </div>
          </div>

          <!-- Pengawas -->
          <div class="control-group">
            <label for="" class="control-label">Pengawas-1 <span class="error">*</span></label>
            <div class="controls">
              <input type="text" name="pengawas1" id="pengawas1" placeholder="Pengawas-1" required value="<?php echo $dosen ?>">
            </div>
          </div>

          <div class="control-group">
            <label for="" class="control-label">Pengawas-2 </label>
            <div class="controls">
              <input type="text" name="pengawas2" id="pengawas2" placeholder="Pengawas-2">
            </div>
          </div>

          <br>
  
          <!-- Action -->
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-primary"><?php echo $update == false ? 'Submit' : 'Ubah Data' ?></button>
              <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>