<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-remove"></i>
                <h3>Hapus Data Mahasiswa Baru</h3>
            </div> <!-- /widget-header -->           

            <div class="widget-content">
                <div class="span11">
                    <form method="post" class="form-horizontal" action="<?= base_url(); ?>akademik/void_new_student/savesess">
                        <fieldset>
                            <div class="control-group">
                              <label class="control-label">Angkatan</label>
                              <div class="controls">
                                <select class="form-control span6" name="angkatan">
                                  <option disabled selected>--Pilih Angkatan--</option>
                                  <?php for ($i=2016; $i < date('Y')+1; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <button class="btn btn-success" type="submit">Submit</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>