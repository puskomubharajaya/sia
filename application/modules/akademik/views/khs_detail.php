<?php $eror;
error_reporting(0); ?>
<!-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script> -->



<script type="text/javascript">
    function detail(idj, nim) {
        $("#x").load('<?= base_url() ?>akademik/khs/nilai_detail/' + idj + '/' + nim);
    }
</script>
<script type="text/javascript">
    function detailold(id, nim) {
        $("#x").load('<?= base_url() ?>akademik/khs/nilai_old/' + id + '/' + nim);
    }
</script>
<script type="text/javascript">
    function edit(idj) {
        $("#edit").load('<?= base_url() ?>akademik/khs/view_edit/' + idj);
    }
</script>

<div class="row">

    <div class="span12">

        <div class="widget">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Kartu Hasil Studi <?= substr($kode->kd_krs,0,12) ?></h3>

            </div> <!-- /widget-header -->



            <div class="widget-content">

                <div class="span11">

                    <?php
                    $log = $this->session->userdata('sess_login');
                    $pecah = explode(',', $log['id_user_group']);
                    $jmlh = count($pecah);
                    for ($i = 0; $i < $jmlh; $i++) {
                        $grup[] = $pecah[$i];
                    }
                    if ((in_array(7, $grup))) { ?>
                        <a href="<?= base_url(); ?>akademik/bimbingan/dp_view/<?= $mhs->NIMHSMSMHS; ?>" class="btn btn-warning"> << Kembali</a> 
                    <?php } else { ?> 
                        <a href="<?= base_url(); ?>akademik/khs" class="btn btn-success"> << Kembali</a> 
                    <?php } ?> 

                    <?php
                    $logged = $this->session->userdata('sess_login');
                    $pecah  = explode(',', $logged['id_user_group']);
                    $jmlh   = count($pecah);
                    for ($i = 0; $i < $jmlh; $i++) {
                        $grup[] = $pecah[$i];
                    }
                    if ((in_array(5, $grup)) || (in_array(7, $grup))) {
                        $prodi = $mhs->KDPSTMSMHS;
                    } else {
                        $prodi = $logged['userid'];
                    }
                    
                    if ($tahunakademik < '20151') {
                        $hitung_ips = $this->db->query('SELECT distinct 
                                                            a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` 
                                                        FROM tbl_transaksi_nilai a
                                                        JOIN tbl_matakuliah_copy b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                                                        WHERE b.`tahunakademik` = "' . $tahunakademik . '" 
                                                        AND kd_prodi = "' . $prodi . '" 
                                                        AND NIMHSTRLNM = "' . $mhs->NIMHSMSMHS . '" 
                                                        and THSMSTRLNM = "' . $tahunakademik . '" ')->result();
                    } else {
                        $hitung_ips = $this->db->query('SELECT distinct 
                                                            a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` 
                                                        FROM tbl_transaksi_nilai a
                                                        JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                                                        WHERE a.`kd_transaksi_nilai` IS NOT NULL 
                                                        AND kd_prodi = "' . $prodi . '" 
                                                        AND NIMHSTRLNM = "' . $mhs->NIMHSMSMHS . '" 
                                                        and THSMSTRLNM = "' . $tahunakademik . '" 
                                                        and a.NLAKHTRLNM <> "T" ')->result();
                    }

                    $st = 0;
                    $ht = 0;
                    foreach ($hitung_ips as $iso) {
                        $h = 0;


                        $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
                        $ht =  $ht + $h;

                        $st = $st + $iso->sks_matakuliah;
                    }

                    $ips_nr = $ht / $st;
                    //$ipk = $this->app_model->get_ipk_mahasiswa($q->NIMHSMSMHS)->row()->ipk;
                    for ($i = 0; $i < $jmlh; $i++) {
                        $grup[] = $pecah[$i];
                    }

                    $cek = $this->db->query("SELECT * from tbl_sinkronisasi_renkeu 
                                            where npm_mahasiswa = '" . substr($kode->kd_krs, 0, 12) . "'
                                            and (status >= 1 and status < 8) 
                                            and tahunajaran = '" . substr($kode->kd_krs, 12, 5) . "'")->result();


                    $krsan = $this->app_model->getdetail('tbl_verifikasi_krs', 'kd_krs', $kode->kd_krs, 'kd_krs', 'asc')->row()->status_verifikasi;
                                                           
                    if ($cek && ($krsan != 9 || $krsan != 10)) { ?> 

                        <?php
                            $log = $this->session->userdata('sess_login');
                            $pecah = explode(',', $logged['id_user_group']);
                            $jmlh = count($pecah);
                            for ($i = 0; $i < $jmlh; $i++) {
                                $grup[] = $pecah[$i];
                            }
                            if (in_array(5, $grup) || in_array(8, $grup) || in_array(19, $grup)) { ?> 
                                <a href="<?= base_url(); ?>akademik/khs/printkhs/<?= $npm ?>/<?= $semester; ?>" target="_blank" class="btn btn-primary">
                                    <i class="btn-icon-only icon-print"> </i> Print KHS </a>
                        <?php } ?>

                    <?php } ?>

                <hr>

                <table>

                    <tr>

                        <td>NPM</td>

                        <td>:</td>

                        <td><?= $mhs->NIMHSMSMHS; ?></td>

                    </tr>

                    <tr>

                        <td>Nama</td>

                        <td>:</td>

                        <td><?= $mhs->NMMHSMSMHS; ?></td>

                        <td width="100"></td>

                        <td>Semester</td>

                        <td>:</td>
                        <?php $a = $this->app_model->get_semester_khs($mhs->SMAWLMSMHS, $tahunakademik); ?>
                        <td><?= $a; ?></td>

                    </tr>

                    <tr>

                        <td>IPS</td>

                        <td>:</td>

                        <td><?= number_format($ips_nr, 2); ?></td>

                    </tr>

                </table>

                <hr>

                <table id="example1" class="table table-bordered table-striped">

                    <thead>

                        <tr>

                            <th>No</th>

                            <th>Kode MK</th>

                            <th>Mata Kuliah</th>

                            <th>SKS</th>

                            <!--th>Dosen</th-->

                            <th>Nilai</th>

                            <th>Detail</th>

                            <!-- <th width="40">Detail Nilai</th> -->

                        </tr>

                    </thead>

                    <tbody>

                        <?php $no = 1;
                        foreach ($detail_khs as $row) { ?>

                            <tr>

                                <td><?= $no; ?></td>

                                <td><?= $row->kd_matakuliah; ?></td>

                                <td><?= $row->nama_matakuliah; ?></td>

                                <td><?= $row->sks_matakuliah ?></td>
                                <?php
                                $nilai = $this->db->query("select * from tbl_transaksi_nilai where NIMHSTRLNM = '" . $mhs->NIMHSMSMHS . "' and KDKMKTRLNM = '" . $row->kd_matakuliah . "' and THSMSTRLNM = '" . $tahunakademik . "' ")->row();

                                ?>
                                <?php if ($krsan != 10) { ?>
                                    <td><?= $nilai->NLAKHTRLNM; ?></td>
                                <?php } else { ?>
                                    <td> - </td>
                                <?php } ?>

                                <td>
                                    <?php if ($tahunakademik >= '20162') { ?>
                                        <button data-toggle="modal" data-target="#xx" class="btn btn-warning" onclick="detail('<?= get_id_jdl($row->kd_jadwal); ?>/<?= $mhs->NIMHSMSMHS; ?>')">
                                            <i class="icon icon-info"></i>
                                        </button>
                                    <?php } else { ?>
                                        <button data-toggle="modal" data-target="#xx" class="btn btn-warning" onclick="detailold('<?= get_id_jdl($row->kd_jadwal); ?>/<?= $mhs->NIMHSMSMHS; ?>')">
                                            <i class="icon icon-info"></i>
                                        </button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php $no++;
                        } ?>
                    </tbody>

                </table>

                </div>

            </div>

        </div>

    </div>

</div>



<div class="modal fade" id="xx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-sm">

        <div class="modal-content" id="x">



        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" id="detil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="edit">



        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->