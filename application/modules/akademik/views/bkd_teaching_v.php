<h3><i class="icon-list-alt"></i> Daftar Pengajaran Tahun akademik <?= get_thajar(getactyear()); ?></h3>
<br>
<table class="table table-bordered table-striped">
    <thead>
        <tr> 
            <th>No</th>
            <th>Kode Matakuliah</th>
            <th>Nama Matakuliah</th>
            <th>SKS</th>
            <th>Kelas</th>
            <th>Hari Mengajar</th>
            <th>Waktu Mulai</th>
            <th>Waktu Selesai</th>
        </tr>
    </thead>
    <tbody>
    	<?php $no = 1; foreach ($teaching as $teach) { ?>
    		<tr>
        		<td><?= $no; ?></td>
        		<td><?= $teach->kd_matakuliah; ?></td>
        		<td><?= $teach->nama_matakuliah; ?></td>
        		<td><?= $teach->sks_matakuliah; ?></td>
        		<td><?= $teach->kelas; ?></td>
        		<td><?= notohari($teach->hari); ?></td>
        		<td><?= $teach->waktu_mulai; ?></td>
        		<td><?= $teach->waktu_selesai; ?></td>
    		</tr>
    	<?php $no++; } ?>
    </tbody>
</table>

<hr>

<h3>
	<i class="icon-edit"></i> Pengajaran Tambahan Dosen <?= get_thajar(getactyear()); ?> 
	<span data-toggle="tooltip" title="lihat detail tugas">
		<a href="#myModal" data-toggle="modal" onclick="load_detail_teaching()"><i class="icon-eye-open"></i></a>
	</span>
	<a class="btn btn-info pull-right" data-toggle="modal" href="#myInfo"><i class="icon-info"></i> Informasi</a>
</h3>
<br>
<div class="form-horizontal">
	<select name="component" id="component" class="form-control span4">
		<option disabled="" selected="" value="">-- Pilih Komponen Kegiatan --</option>
		<?php foreach ($otherTeaching as $teach) { ?>
			<option value="<?= $teach->id.'-'.$teach->kode_pengajaran ?>"><?= $teach->komponen ?></option>
		<?php } ?>
	</select>

	<span id="param-show-here"></span>

	<div class="input-prepend input-append">
		<input 
			type="text" 
			name="" 
			readonly="" 
			value="" 
			id="credit" 
			class="form-control span1" 
			placeholder="SKS" />
		<span class="add-on">SKS</span>
	</div>

	<button 
		class="btn btn-success" 
		onclick="addTask()" 
		style="margin-bottom: 1px">
		<i class="icon icon-plus"></i>
	</button>

	<form action="<?= base_url(); ?>akademik/beban_kinerja_dosen/add_additional_teaching" method="post">
		<div id="appendHere">
			
		</div>
		<br>
		<center>
			<button disabled="" type="submit" id="btnComp" class="btn btn-primary">
				Submit
			</button>
		</center>
	</form>
</div>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Penugasan tahun akademik <?= get_thajar(getactyear()); ?></h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped" id="tableTeaching">
                    <thead>
                        <tr> 
                        	<th>No</th>
                            <th>Komponen penugasan</th>
                            <th>Beban SKS</th>
                        </tr>
                    </thead>
                    <tbody id="contentTable">
                    </tbody>
				</table>
			</div>
			<div class="modal-footer">
				<!-- <a class="btn btn-primary" href="<?= base_url('akademik/beban_kinerja_dosen/teaching_detail') ?>">Detail</a> -->
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="myInfo" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Informasi Pengajaran Tambahan Dosen</h4>
			</div>
			<div class="modal-body">
				<ul>
					<li>Rumus penghitungan untuk pengajaran tambahan berupa <b>Pembimbing utama skripsi/tesis (maks. 10 lulusan per semester)</b> dihitung dengan rumus <b class="label label-info">(n / 10) x beban SKS</b>, dimana <b>n</b> adalah jumlah mahasiswa.</li>
					<li>Rumus penghitungan untuk pengajaran tambahan berupa <b>Ketua penguji skripsi/tesis (4 lulusan per semester)</b> dihitung dengan rumus <b class="label label-info">(n / 4) x beban SKS</b>, dimana <b>n</b> adalah sidang.</li>
					<li>Rumus penghitungan untuk pengajaran tambahan berupa <b>Pembimbing akademik</b> dihitung dengan rumus <b class="label label-info">(n / 30) x beban SKS</b>, dimana <b>n</b> adalah jumlah mahasiswa dan <b>30</b> merupakan jumlah ideal mahasiswa bimbingan.</li>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		// set SKS by component
		$('#component').change(function(){
			$.get('<?= base_url() ?>akademik/beban_kinerja_dosen/set_sks_teaching/'+$(this).val().split("-")[0],{},function(get){
				$('#credit').val(get);
			});
		});

		// set teaching params
		$('#component').change(function(){
			$('#param-show-here').empty();
			$.get('<?= base_url() ?>akademik/beban_kinerja_dosen/set_teaching_param/'+$(this).val().split("-")[0],{},function(res){
				var response = JSON.parse(res);
				response.forEach(function (arg) {
					$('#param-show-here').append(arg);
				});
			});
		});
	})

	var counter = 1;
	// fungsi tambah row
	function addTask()
	{
		if ($('#component').val() === '') {
			alert('Pilih salah satu komponen terlebih dahulu!'); 
			return;
		}

		// var param        = parseInt($('#param').val());
		var componentVal = $('#component').val().split("-")[1];
		var paramCode    = document.querySelectorAll('.paramCode');
		var paramName    = document.querySelectorAll('.add-on');
		var paramVal     = document.querySelectorAll('.params');
		var credit       = parseFloat($('#credit').val());

		console.log(paramVal)
		counter++;;
		var cols = "";
		// init div
		cols += '<div id="component_'+counter+'" class="comps" style="margin-top:5px">';

		// judul
		cols += '<input type="text" name="" style="margin-right:3px" class="span4" value="'+$('#component option:selected').text()+'" readonly=""/>';
		cols += '<input type="hidden" name="components[]" style="margin-right:3px" value="'+componentVal+'" readonly=""/>';

		// param
		for(var i = 0; i < paramVal.length; i++){
			cols += '<div class="input-prepend input-append" style="margin-left:4px;">';
			cols += '<span class="add-on">'+paramName[i].innerHTML+'</span>';
			cols += '<input type="hidden" name="paramCode['+componentVal+']['+i+'][]" value="'+paramCode[i].value+'">'
			cols += '<input type="text" name="paramValue['+componentVal+']['+i+'][]" style="margin-right:3px" class="span2" value="'+paramVal[i].value+'" readonly=""/>';
			cols += '</div>';
		}


		// formula to count final credit
		if (componentVal === 'COM1' || componentVal === 'COM2') {
			var finalcredit = parseFloat((paramVal[0].value / 10) * credit);
		} else if (componentVal === 'COM5') {
			var finalcredit = parseFloat((paramVal[0].value / 4) * credit);
		} else if (componentVal === 'COM7') {
			var finalcredit = parseFloat((paramVal[0].value / 30) * credit);
		} else {
			var finalcredit = credit;
		}

		// sks
		cols += '<div class="input-prepend input-append" style="margin-right:3px;">';
		cols += '<input type="text" name="sks[]" style="margin-right:3px" class="span1" value="'+(Math.round(finalcredit*100)/100)+'" readonly=""/>';
		cols += '<span class="add-on">SKS</span>';
		cols += '</div>';

		// button
		cols += '<button class="btn btn-danger" onclick="delResearch(\'component_'+counter+'\',\'comps\',\'btnComp\')" style="margin-bottom: 1px"><i class="icon icon-remove"></i></button>';
		cols += '</div>';
	
		$('#appendHere').append(cols);
		$('#credit,.params').val('');
		$('#component option:first').prop('selected',true);

		// delete prop disabled in #btnRsc if form != null
		if ($('.comps').length !== 0) {
			$("#btnComp").removeAttr("disabled");
		}
	}

	// fungsi delete row
	function delResearch(skidrow,skiddy,btnSbm)
	{
		$("#"+skidrow).remove();

		// disable the button if input == 0
		if ($("."+skiddy).length === 0) {
			$("#"+btnSbm).attr("disabled","");
		}
	}

	function load_detail_teaching() {
		
		$.get('<?= base_url('akademik/beban_kinerja_dosen/load_additional_teaching') ?>', function(response) {
			var parse = JSON.parse(response);
			var no = 0;
			parse.forEach(function(data) {
				no++;
				$('#contentTable').append(`
            		<tr class="teachList">
            			<td>${no}</td>
                		<td>${data.komponen}</td>
                		<td>${data.sks}</td>
            		</tr>
	            `)
			});

			// count total SKS
			var table = document.getElementById('tableTeaching'), constVal = 0;
			for(var i = 1, length1 = table.rows.length; i < length1; i++){
				constVal = constVal + parseFloat(table.rows[i].cells[2].innerHTML);
			}
			$('#contentTable').append(`
				<tr class="teachList">
					<td colspan="2" style="text-align: center"><b>Total SKS</b></td>
					<td>${Math.round(constVal*100)/100}</td>
				</tr>
			`)
		});
	}

	$('#myModal').on('hidden', function () {
		$('.teachList').remove();
	});

	function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>