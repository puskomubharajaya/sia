<link rel="stylesheet" href="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.css"/>
<script src="<?php echo base_url();?>assets/js/timepicker/jquery.ui.timepicker.js"></script>

<?php $sess = $this->session->userdata('sess_login'); ?>

<script type="text/javascript">
    function loadaddmod(id)
    {
        $('#contaddmod').load('<?= base_url('akademik/jadwal_ujian/addmodal/'); ?>'+id);
    }

    function loaddetmod(id)
    {
        $('#contdetmod').load('<?= base_url('akademik/jadwal_ujian/detmodal/'); ?>'+id);
    }

    function loadedtmod(id)
    {
        $('#contedtmod').load('<?= base_url('akademik/jadwal_ujian/edtmodal/'); ?>'+id);
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-calendar"></i>
                <h3>Data Jadwal Ujian Tahun Akademik <?php echo get_thajar($year); ?></h3>
            </div>

            <div class="widget-content">
                <a class="btn btn-warning" href="<?= base_url('akademik/jadwal_ujian') ?>">
                    <i class="icon icon-chevron-left"></i> Kembali
                </a>

                <button class="btn btn-primary" data-target="#exmod" data-toggle="modal">
                    <i class="icon icon-download"></i> Ekspor Data
                </button>

                <div class="pull-right">
                    <button class="btn btn-danger" data-target="#inmod" data-toggle="modal">
                        <i class="icon icon-info"></i> Informasi
                    </button>
                </div>

                <hr>

                <div class="span11">

                    <table id="example1" class="table table-stripped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kelas</th>
                                <th>Kode Mata Kuliah</th>
                                <th>Mata Kuliah</th>
                                <th>Dosen</th>

                                <?php if ($sess['id_user_group'] == 8) { ?>
                                    <th>Tambah Jadwal</th>
                                    <th>Rubah Jadwal</th>
                                <?php } ?>
                                
                                <th>Lihat Jadwal</th>
                            </tr>
                        </thead>
                        <tbody>
						
                            <?php $no = 1; foreach ($list->result() as $key) { ?>

                                <!-- script for color information -->
                                <?php 
                                    if (is_null($key->uts) && is_null($key->uas)) {
                                        $color = 'style="background-color: #F08080"';
                                    } elseif ($key->uts == 1 && is_null($key->uas)) {
                                        $color = 'style="background-color: #FFA500"';
                                    } elseif (is_null($key->uts) && $key->uas == 2) {
                                        $color = 'style="background-color: #48D1CC"';
                                    } elseif ($key->uts == 1 && $key->uas == 2) {
                                        $color = 'style="background-color: #00FF7F"';
                                    } ?>

                                <tr>
                                    <td <?= $color; ?> ><?= $no; ?></td>
                                    <td <?= $color; ?> ><?= $key->kelas; ?></td>
                                    <td <?= $color; ?> ><?= $key->kd_matakuliah; ?></td>
                                    <td <?= $color; ?> ><?= $key->nama_matakuliah; ?></td>
                                    <td <?= $color; ?> ><?= $key->nama; ?></td>
                                    
                                    <!-- if prodi -->
                                    <?php if ($sess['id_user_group'] == 8) { ?>
                                        <td <?= $color; ?> >
                                            <button data-target="#addmod" data-toggle="modal" onclick="loadaddmod(<?= $key->id_jadwal; ?>)" class="btn btn-primary">
                                                <i class="icon icon-plus"></i>
                                            </button>
                                        </td>    
                                        <td <?= $color; ?> >
                                            <button data-target="#edtmod" data-toggle="modal" onclick="loadedtmod(<?= $key->id_jadwal; ?>)" class="btn btn-warning">
                                                <i class="icon icon-pencil"></i>
                                            </button>
                                        </td>    
                                    <?php } ?>
                                    <!-- /end if prodi -->

                                    <td <?= $color; ?> >
                                        <button data-target="#seemod" data-toggle="modal" onclick="loaddetmod(<?= $key->id_jadwal; ?>)" class="btn btn-success">
                                            <i class="icon icon-eye-open"></i>
                                        </button>
                                    </td>
                                    
                                </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add modal -->
<div class="modal fade" id="addmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="contaddmod">
        
        </div>
    </div>
</div>

<!-- detail modal -->
<div class="modal fade" id="seemod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="contdetmod">

        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="edtmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="contedtmod">
            
        </div>
    </div>
</div>

<!-- export modal -->
<div class="modal fade" id="exmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('akademik/jadwal_ujian/exportxls'); ?>" method="post" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ekspor Data Jadwal (excel)</h4>
                </div>
                <div class="modal-body">
                    <div class="control-group">
                        <label class="control-label">Tipe Ujian</label>
                        <div class="controls">
                            <select class="form-control span5" name="tipe">
                                <option disabled="" selected="">-- Pilih Tipe Ujian --</option>
                                <option value="1">UTS</option>
                                <option value="2">UAS</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- info modal -->
<div class="modal fade" id="inmod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Informasi Keterangan Warna</h4>
            </div>
            <div class="modal-body">

                <button class="btn btn-danger btn-small"><i class="icon icon-tag"></i></button>
                <b style="font-size: 14px">&nbsp; Kedua jadwal belum di unggah (UTS & UAS)</b>
                <br><br>

                <button class="btn btn-warning btn-small"><i class="icon icon-tag"></i></button>
                <b style="font-size: 14px">&nbsp; Hanya jadwal <i>UTS</i> yang telah diunggah</b>
                <br><br>

                <button class="btn btn-primary btn-small"><i class="icon icon-tag"></i></button>
                <b style="font-size: 14px">&nbsp; Hanya jadwal <i>UAS</i> yang telah diunggah</b>
                <br><br>

                <button class="btn btn-success btn-small"><i class="icon icon-tag"></i></button>
                <b style="font-size: 14px">&nbsp; Kedua jadwal telah diunggah (UTS & UAS)</b>
                <br><br>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>