<script type="text/javascript">
function edit(idk){
        $('#edit').load('<?php echo base_url();?>akademik/status/load_detail/'+idk);
    }
</script>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Status Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
                <!-- <a href="#" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print</a>
                <hr> -->
				<div class="span11">
                    <table>
                        <tr>
                            <td>NPM</td>
                            <td>:</td>
                            <td><?php echo $query->NIMHSMSMHS;?></td>
                            <td width="100"></td>
                            <td>Briva</td>
                            <td>:</td>
                            <td><?php echo $c; ?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><?php echo $query->NMMHSMSMHS;?></td>
                            <td width="100"></td>
                            <td>Semester</td>
                            <td>:</td>
                            <?php $sms = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$query->NIMHSMSMHS,'NIMHSMSMHS','asc')->row(); ?>
                            <td><?php echo $this->app_model->get_semester($sms->SMAWLMSMHS);?></td>
                            <td width="100"></td>
                            <td>Angkatan</td>
                            <td>:</td>
                            <td><?php echo $query->TAHUNMSMHS; ?></td>
                        </tr>
                        <tr>
                            <td>Fakultas</td>
                            <td>:</td>
                            <td><?php echo $query->fakultas; ?></td>
                            <td width="100"></td>
                            <td>Jurusan</td>
                            <td>:</td>
                            <td><?php echo $query->prodi; ?></td>
                            <td width="100"></td>
                            <!-- <td>Kelas</td>
                            <td>:</td>
                            <td><?php //echo $query->SHIFTMSMHS; ?></td> -->
                        </tr>
                        <?php $cekkartu = $this->db->query("SELECT status from tbl_sinkronisasi_renkeu where tahunajaran = '".getactyear()."' and npm_mahasiswa = '".$query->NIMHSMSMHS."'")->row(); ?>
                        <tr>
                            <?php if ($cekkartu->status == 1) {
                                $stat = 'AKTIF';
                            }elseif ($cekkartu->status == 2) {
                                $stat = 'SUDAH DIIZINKAN MENGIKUTI UTS';
                            }elseif ($cekkartu->status >= 3) {
                                $stat = 'SUDAH DIIZINKAN MENGIKUTI UAS';
                            }elseif ($cekkartu->status == 8) {
                                $stat = 'CUTI';
                            } else {
                                $stat = 'NON-AKTIF';
                            }
                            ?>
                            <td>Status</td>
                            <td>:</td>
                            <td><?php echo $stat; ?></td>
                        </tr>
                    </table>
                    <hr>
                    <p>*Apabila IPK atau IPS ada permasalahan, silahkan hubungi prodi terkait</p>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Tahun Ajaran</th>
                                <th>Keterangan</th>
                                <td>Status</td>
                                <td>Total SKS</td>
                                <th>IPS</th>
                                <th>IPK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach ($ta as $value) { 
                            ?>
                            <tr>
                                <td><?php echo $value->kode; ?></td>
                                <td><?php echo $value->tahun_akademik; ?></td>

                                <?php 
                                    $cekkrs = $this->db->query("SELECT kd_krs from tbl_verifikasi_krs where kd_krs like '".substr($query->NIMHSMSMHS, 0,12).$value->kode."%'")->row();
                                    $cekcuti = $this->db->query("SELECT * from tbl_status_mahasiswa where npm = '".substr($query->NIMHSMSMHS, 0,12)."' and tahunajaran = '".$value->kode."' and validate = 1")->row();
                                
                                    if ($cekkrs == true) {
                                        $status = 'Aktif';

                                        $res = $this->db->query('SELECT NIMHSTRAKM, THSMSTRAKM, KDPSTTRAKM, NLIPKTRAKM, NLIPSTRAKM,SKSEMTRAKM  FROM tbl_aktifitas_kuliah_mahasiswa WHERE NIMHSTRAKM = ? AND THSMSTRAKM  = ?', [$query->NIMHSMSMHS, $value->kode])->row(); 
                                        
                                        $ips = number_format($res->NLIPSTRAKM,2);
                                        $ipk = number_format($res->NLIPKTRAKM,2);
                                        $sks = number_format($res->SKSEMTRAKM);

                                    } elseif ($cekcuti == true) {
                                        $status = 'Cuti';
                                        $ips = '-';
                                        $ipk = '-';
                                        $jml_sks = '-';
                                    } elseif(($cekcuti == false) and ($cekkrs == false)) {
                                        $status = '-';
                                        $ips = '-';
                                        $ipk = '-';
                                        $jml_sks = '-';
                                    }
                                ?>
                                <td><?php echo $status; ?></td>
                                <?php if ($status == '-') { ?>
                                    <td>-</td>
                                    <td>-</td>
                                <?php } else { ?>
                                    <td><?php echo $sks; ?></td>
                                    <td><?php echo $ips; ?></td>
                                <?php } ?>
                                <td><?php echo $ipk; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
					<!-- <table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
                                <th>Jenis Pembayaran</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php //$no=1; foreach ($row as $key): ?>
                                <tr>
                                    <td><?php //echo $no; ?> </td>
                                    <td><?php //echo 'semester'.$key->smtr; ?></td>
                                    <td class="td-actions">
                                    <a class="btn btn-primary btn-small" onclick="edit(<?php //echo ''.$query->NIMHSMSMHS.$key->smtr.''; ?>)"  data-toggle="modal" href="#myModal"><i class="btn-icon-only icon-ok"> </i></a>
                                </td>
                                </tr>
                                <?php //$no++; //endforeach ?>
	                    </tbody>
	               	</table> -->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->