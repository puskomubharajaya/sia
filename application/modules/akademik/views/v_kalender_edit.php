<script type="text/javascript">
$(document).ready(function() {
  $('#tgl_skep').datepicker({dateFormat: 'yy-mm-dd',
                  yearRange: "2015:2025",
                  changeMonth: true,
                  changeYear: true
  });

});
</script>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Update Data Kalender</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url(); ?>akademik/kalender/exe_edit_kalender" method="post" enctype="multipart/form-data">
    <div class="modal-body">
        <div class="control-group" id="">
            <label class="control-label">Tahun Akademik</label>
            <div class="controls">
                <input name="ta" value="<?php echo substr($row->kd_kalender, 0,5); ?>" class="span1" class="form-control" disabled/>
                <input type="hidden" name="kd_kalender" value="<?php echo $row->kd_kalender; ?>" />
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Nomer SKEP.</label>
            <div class="controls">
                <input type="text" class="span4 form-control" value="<?php echo $row->no_skep ?>" name="no_skep" required/>
            </div>
        </div>
        <div class="control-group" id="">
            <label class="control-label">Tanggal SKEP.</label>
            <div class="controls">
                <input type="text" class="span2 form-control" value="<?php echo $row->tgl_skep ?>" id="tgl_skep" name="tgl_skep" required/>
            </div>
        </div>
        <!-- <div class="control-group" id="">
            <label class="control-label">Upload Data Nilai (.xls) </label>
            <div class="controls">
                <input type="file" class="span4 form-control" name="userfile" required/>
                <input type="hidden" name="id_jadwal" value="<?php echo $id ?>" />
                <input type="hidden" name="kode_jdl" value="<?php echo $kode_jadwal_bro; ?>">
            </div>
        </div> -->
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      <input type="submit" class="btn btn-primary" value="Submit"/>
    </div>
</form>