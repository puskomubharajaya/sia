<?php $eror;  ?>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
    function edit(idj) {
        $("#edit").load('<?php echo base_url()?>akademik/khs/view_edit/'+idj);
    }
</script>

<div class="row">

    <div class="span12">                    

        <div class="widget">

            <div class="widget-header">

                <i class="icon-user"></i>

                <h3>Data Hasil Studi Semester Perbaikan</h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

                <div class="span11">

                    
                    <?php 
                        $log = $this->session->userdata('sess_login');
                        $pecah = explode(',', $log['id_user_group']);
                        $jmlh = count($pecah);
                        for ($i=0; $i < $jmlh; $i++) { 
                            $grup[] = $pecah[$i];
                        }
                        if ((in_array(7, $grup))) { ?>
                            <a href="<?php echo base_url(); ?>akademik/bimbingan/dp_view/<?php echo $mhs->NIMHSMSMHS; ?>" class="btn btn-warning"><< Kembali</a>
                        <?php } else { ?>
                            <a href="<?php echo base_url(); ?>akademik/perbaikan/khs_sp" class="btn btn-success"><< Kembali</a>
                    <?php } ?>

                    <?php 
                    $logged = $this->session->userdata('sess_login');
                    $pecah = explode(',', $logged['id_user_group']);
                    $jmlh = count($pecah);
                    for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                    }
                    if ((in_array(5, $grup)) || (in_array(7, $grup))) {
                        $prodi = $mhs->KDPSTMSMHS;
                    } else {
                        $prodi = $logged['userid'];
                    }
                    //$ipk = $this->app_model->get_ipk_mahasiswa($q->NIMHSMSMHS)->row()->ipk;
                    for ($i=0; $i < $jmlh; $i++) { 
                        $grup[] = $pecah[$i];
                    }

                    $cek = $this->db->query("SELECT * from tbl_sinkronisasi_renkeu where npm_mahasiswa = '".substr($kode->kd_krs, 0,12)."' and (status >= 1 and status < 8) and tahunajaran = '".substr($kode->kd_krs, 12,5)."'")->result();
                    $krsan = $this->app_model->getdetail('tbl_verifikasi_krs_sp','kd_krs',$kode->kd_krs,'kd_krs','asc')->row()->status_verifikasi;
                    if (($cek == true) and (($krsan != 9) and ($krsan != 10))) { ?>
                        
                        <!-- <a href="<?php echo base_url(); ?>akademik/khs/printkhs/<?php echo $npm ?>/<?php echo $semester;?>" target="_blank" class="btn btn-primary"><i class="btn-icon-only icon-print"> </i> Print KHS</a> <hr> -->
                     
                    <?php } ?>

                    <br>
                    <br>

                    <table>

                        <tr>

                            <td>NPM</td>

                            <td>:</td>

                            <td><?php echo $mhs->NIMHSMSMHS;?></td>

                        </tr>

                        <tr>

                            <td>Nama</td>

                            <td>:</td>

                            <td><?php echo $mhs->NMMHSMSMHS;?></td>

                            <td width="100"></td>

                        <tr>

                            <td>Semester Diperbaiki</td>

                            <td>:</td>
                            <?php $a = $this->app_model->get_semester_khs($mhs->SMAWLMSMHS,$tahunakademik-2); ?>
                            <td><?php echo $a;?></td>

                        </tr>

                    </table>

                    <hr>

                    <table id="example1" class="table table-bordered table-striped">

                        <thead>

                            <tr> 

                                <th>No</th>

                                <th>Kode MK</th>

                                <th>Mata Kuliah</th>

                                <th>SKS</th>

                                <th>Nilai</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $no = 1; foreach ($detail_khs as $row) { ?>

                            <tr>

                                <td><?php echo $no; ?></td>

                                <td><?php echo $row->kd_matakuliah; ?></td>

                                <td><?php echo $row->nama_matakuliah; ?></td>

                                <td><?php echo $row->sks_matakuliah ?></td>
                                <?php   
                                    $nilai = $this->db->query("SELECT * from tbl_transaksi_nilai_sp where NIMHSTRLNM = '".$mhs->NIMHSMSMHS."' and KDKMKTRLNM = '".$row->kd_matakuliah."' and THSMSTRLNM = '".$tahunakademik."' ")->row();  
                                 ?>
                                <?php  ?>
                                <td><?php echo $nilai->NLAKHTRLNM; ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



<div class="modal fade" id="detil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content" id="edit">

            

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->