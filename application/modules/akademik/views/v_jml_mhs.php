<script type="text/javascript">
$(function () {
    // Create the chart
    $('#container2').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Jumlah Mahasiswa Aktif UBJ per <?php echo $summ; ?>'
        },
        subtitle: {
            text: 'Klik grafik untuk melihat jumlah per prodi'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.f} mhs'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}jml</b> of total<br/>'
        },
        series: [{
            name: 'Brands',
            // events: {
            //         click : function() {
            //             $('#TEKNIK').load('<?php echo base_url() ?>akademik/chart/jml/$ds->kd_fakultas');
            //         }
            //     }, 
            colorByPoint: true,
            data: [{
                <?php 
                    $baris = count($jumlah);
                    $no = 0;
                    foreach ($jumlah as $row) {
                        $no++;
                        if ($no == $baris) {
                            echo "name: '".$row->fakultas."', y:".$row->jmlh.", drilldown:'".$row->fakultas."'}]";
                        } else {
                            echo "name: '".$row->fakultas."', y:".$row->jmlh.", drilldown:'".$row->fakultas."'}, {";
                        };
                    }
                ?>
        }],
        drilldown: {
            series: [{
                name: 'teknik',
                id: 'TEKNIK',
                data: [
                        <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "2" AND a.`tahunajaran` = "'.$summ.'" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'ekonomi',
                id: 'EKONOMI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "1" AND a.`tahunajaran` = "'.$summ.'" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'hukum',
                id: 'HUKUM',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "3" AND a.`tahunajaran` = "'.$summ.'" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'fikom',
                id: 'ILMU KOMUNIKASI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "4" AND a.`tahunajaran` = "'.$summ.'" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'psikologi',
                id: 'PSIKOLOGI',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "5" AND a.`tahunajaran` = "'.$summ.'" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }, {
                name: 'pasca',
                id: 'PASCASARJANA',
                data: [
                    <?php
                            $q = $this->db->query('SELECT b.`prodi`,COUNT(DISTINCT a.npm_mahasiswa) AS jum FROM tbl_verifikasi_krs a JOIN tbl_jurusan_prodi b
                                                    ON b.`kd_prodi`=a.`kd_jurusan` JOIN  tbl_fakultas c
                                                    ON c.`kd_fakultas`=b.`kd_fakultas`
                                                    WHERE c.`kd_fakultas`= "6" AND a.`tahunajaran` = "'.$summ.'" GROUP BY b.`kd_prodi`')->result();
                            $idih = count($q);
                            $no = 0;
                            foreach ($q as $raw) {
                                $no++;
                                if ($no == $idih) {
                                    echo "['".$raw->prodi."',".$raw->jum."]]";
                                } else {
                                    echo "['".$raw->prodi."',".$raw->jum."],";
                                };
                            }
                        ?>
            }]
        }
    });
});
</script>

<div id="container2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>