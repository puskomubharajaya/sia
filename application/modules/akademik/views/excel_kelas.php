<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=jumlah_kelas_mahasiswa.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>


<style>
table, td, th {
    border: 2px solid black;
}


th {
    background-color: blue;
    color: black;
}
</style>

<table>
    <thead>
        <tr>
            <th>NO</th>
            <th>Jenis Kelas</th>
            <th>Jumlah Mahasiswa</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no = 1;
        foreach ($kelas as $key) { ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $key->wkt_kls; ?></td>
            <td><?php echo $key->jumjum; ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>