<?php //var_dump($rows);die(); ?>

<script>
    function edit(idk){
        $('#edit').load('<?php echo base_url();?>akademik/kalender/edit_kalender/'+idk);
    }

    function upload(idk){
        $('#edit').load('<?php echo base_url();?>akademik/kalender/upload_skep/'+idk);
    }

    function edit_no(){
        alert('Menunggu persetujuan Wakil Rektor 1, untuk input tanggal SKEP. dan nomer SKEP.');
    }
</script>

<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Data Kalender Akademik</h3>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="span11">
                    <a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="icon icon-plus"></i> Buat Kalender Akademik</a>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>Tahun Akademik</th>
                                <th>No SEKP.</th>
                                <th>Tanggal SKEP.</th>
                                <th>Status</th>
                                <th width="18%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1; foreach ($rows as $row) { 
                                if ($row->flag == '1') { 
                                    $sts = 'menunggu persetujuan';
                                    $ta = substr($row->kd_kalender, 0,5);
                                    $no_skep = 'menunggu persetujuan';
                                    $tgl_skep = 'menunggu persetujuan';
                                    $act1 = '<a class="btn btn-danger btn-small" onclick="edit_no()"><i class="btn-icon-only icon-ok"></i></a>';
                                    $act3 = '';
                                    $act4 = '';
                                } elseif ($row->flag == '2') {
                                    $sts = 'Disetujui';
                                    $ta = substr($row->kd_kalender, 0,5);
                                    if(is_null($row->no_skep)){ $no_skep  = '-';}else{$no_skep = $row->no_skep;};
                                    if(is_null($row->tgl_skep)){$tgl_skep = '-';}else{$tgl_skep = TanggalIndo($row->tgl_skep);};
                                    $act1 = '<a class="btn btn-primary btn-small" onclick="edit('.$row->id.')" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-ok"></i></a>';
                                    $act3 = '<a class="btn btn-success btn-small" onclick="upload('.$row->id.')" data-toggle="modal" href="#editModal1" ><i class="btn-icon-only icon-upload-alt"></i></a>';
                                    if (!is_null($row->file)) {
                                        $act4 = '<a href="'.base_url().'upload/skep_kamik/'.$row->file.'" class="btn btn-defaut btn-small edit"><i class="btn-icon-only icon-download-alt"> </i></a>';    
                                    }
                                }   
                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $ta ?></td>
                                <td><?php echo $no_skep ?></td>
                                <td><?php echo $tgl_skep ?></td>
                                <td><?php echo $sts ?></td>
                                <td width="18%">
                                    <?php echo $act1; ?>
                                    <a href="<?php echo base_url(); ?>akademik/kalender/isi_kalender/<?php echo $row->kd_kalender; ?>" class="btn btn-primary btn-small edit"><i class="btn-icon-only icon-list"> </i></a>
                                    <?php echo $act3; ?>
                                    <?php echo $act4; ?>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Mpdal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> <i class="icon icon-edit"></i> Buat Kalender Akademik</h4>
            </div>
            <div class="modal-body">    
                <p>Alur Pengajuan Kalender Akademik.</p>
                <ul>
                    <li><b>Pengajuan</b> : BAA mengajukan kalender akademik.</li>
                    <li><b>Wakil Rektor 1</b> : Memberi approval untuk kalender yang diajukan.</li>
                    <li>...</li>
                    <li>...</li>
                </ul>
                <form class ='form-horizontal' action="<?php echo base_url();?>akademik/kalender/add_kalender" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">
                                
                    <div class="control-group" id="">
                        <label class="control-label">Tahun Akademik</label>
                        <div class="controls">
                            <select name="ta" class="span2" class="form-control">
                                <option disabled>-- Pilih Tahun Akademik --</option>
                                <?php foreach ($tak as $key) { ?>
                                    <option value="<?php echo $key->kode; ?>"><?php echo $key->tahun_akademik; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div> 
            </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                <input type="submit" class="btn btn-primary" value="Simpan"/>
            </div>
            </form>
            </div><!-- /.modal-body -->    
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Mpdal -->

<div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">
            
        </div>
    </div>
</div>

