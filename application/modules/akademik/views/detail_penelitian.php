<div class="container">	
    <div class="row">
      	<div class="span12">   
      		<div class="widget ">
      			<div class="widget-header">
      				<i class="icon-list"></i>
      				<h3>Detail Daftar Penelitian</h3>
  				</div> 
				<div class="widget-content">
					<div class="span11">
						<a href="<?= base_url('akademik/beban_kinerja_dosen') ?>" class="btn btn-warning"><i class="icon-chevron-left"></i> Kembali</a>
						<hr>
						<table id="example1" class="table table-bordered table-striped">
	                        <thead>
	                            <tr> 
	                                <th>No</th>
	                                <th>Judul Penelitian</th>
	                                <th>Tahun akademik</th>
	                                <th>Program</th>
	                                <th>Kegiatan</th>
	                                <th>Peran / Progres</th>
	                                <th>Beban SKS</th>
	                                <th>Status</th>
	                                <th>Aksi</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	<?php $no = 1; foreach ($researches as $research) { ?>
	                        		<tr>
		                        		<td><?= $no; ?></td>
		                        		<td><?= $research->judul; ?></td>
		                        		<td><?= get_thajar($research->tahunakademik); ?></td>
		                        		<td><?= $research->program; ?></td>
		                        		<td><?= $research->kegiatan; ?></td>
		                        		<td><?= $research->param; ?></td>
		                        		<td><?= $research->sks; ?></td>
		                        		<td><?= approval_teaching_status($research->status); ?></td>
		                        		<td style="width: 85px;">
	                        				<a 
												class="btn btn-warning" 
												onclick="loadModal(<?php echo $research->id ?>)"
												href="#xx" 
												data-toggle="modal" 
												title="revisi penelitian">
												<i class="icon-edit"></i>
											</a>
		                        			<?php if (is_null($research->status)): ?>
		                        			<a 
												class="btn btn-danger" 
												onclick="return confirm('Yakin ingin menghapus data ini?')"
												href="<?= base_url('akademik/beban_kinerja_dosen/remove_research/'.$research->id) ?>" 
												data-toggle="tooltip" 
												title="hapus penelitian">
												<i class="icon-trash"></i>
											</a>
		                        			<?php endif ?>
		                        		</td>
	                        		</tr>
	                        	<?php $no++; } ?>
	                        </tbody>
	                    </table>
					</div>
				</div>
			</div>
	    </div>
    </div>
</div>

<!-- Revisi Modal -->
<div class="modal fade" id="xx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-sm">

        <div class="modal-content" id="x">



        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<script type="text/javascript">
    function loadModal(id) {
        $("#x").load('<?= base_url() ?>akademik/beban-kinerja-dosen/revmod/' + id);
    }
</script>