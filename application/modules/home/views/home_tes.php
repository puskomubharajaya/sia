<div class="row">
  <div class="span12">
    <div class="widget">
      <div class="widget-header"> <i class="icon-bookmark"></i>
        <h3>Pemberitahuan</h3>
      </div>
      <!-- /widget-header -->
      <div class="widget-content">
        <?php
        $log = $this->session->userdata('sess_login');
        $akt = substr($log['userid'], 0, 4);
        $actYear = getactyear();

        /*notifikasi mahasiswa non aktif untuk prodi*/
        if ($notif == "prd") {
          if ($notif_mhs->num_rows() > 0) {
            foreach ($notif_mhs->result() as $key => $value) {
              echo '<div class="alert alert-warning">
                      <button type="button" class="close close-notif" data-id="' . $value->id . '" data-dismiss="alert">×</button>
                      <p><strong>Informasi!</strong></p>
                      <p>' . $value->message . ' klik <a href="#mhs_non" data-toggle="modal" data-tooltip="tooltip" title="list mahasiswa non aktif"><strong class="detail">disini</strong></a> untuk detail </p>
                    </div>';
            }
          }
        }

        if ($notif == 'mhs') {
          $krs = $this->temph_model->notif_krs($log['userid'] . $actYear);
          $khs = $this->temph_model->notif_khs($log['userid'] . $actYear);

          if (count($notifikasi_mhs) > 0) {
            foreach ($notifikasi_mhs as $key => $value) { ?>
              <!-- Alert -->
              <div class="alert alert-danger">
                <button type="button" class="close close-notif" data-id="<?php echo $value->id ?>" data-dismiss="alert">×</button>
                <strong> <?php echo $value->message ?>.</strong>
              </div>
          <?php }
          } ?>

          <?php
          if ($krs->num_rows() > 0) {
            $krs = $krs->row();
            if ($krs->status_verifikasi == 1 and is_null($krs->notif)) {
              if ($akt != date('Y')) { ?>
                <!-- KRS Disetujui -->
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Perhatian!</strong> KRS Anda telah disetujui oleh pembimbing akademik.
                  <a href="<?= base_url('home/notif_update_krs/' . $log['userid'] . $actYear); ?>" onclick="" title=""><b>Lihat ...</b></a>
                </div>
              <?php }
            }
          }
          if ($khs->num_rows() > 0) {
            $khs = $khs->row();
            if ($khs->flag_publikasi == 2 and is_null($khs->notif)) { ?>
              <!-- KHS -->
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Perhatian!</strong> Nilai anda telah dapat dilihat.
                <a href="<?= base_url('home/notif_update_khs/' . $log['userid'] . '/' . $actYear); ?>" onclick="" title=""><b>Lihat ...</b></a>
              </div>
            <?php }
          }
        } elseif ($notif == 'dpa') {
          $ajukrs = $this->temph_model->notif_for_pa($log['userid'], $actYear)->result();
          if (count($ajukrs) > 0) { ?>
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>Perhatian!</strong> <?= count($ajukrs); ?> mahasiswa melakukan pengajuan KRS dan belum ditindak lanjuti.
              <a href="<?= base_url('home/notif_pa/' . $actYear); ?>" onclick="" title=""><b>Lihat ...</b></a>
            </div>
        <?php }
        } ?>

        <!-- Elearning -->
        <div class="alert alert-info">
          <strong>Informasi!</strong> Perkuliahan E-Learning
          <a type="button" data-toggle="modal" href="#popelearning"><b>Buka ...</b></a>
        </div>

        <?php if (($log['id_user_group'] == 5) or ($log['id_user_group'] == 6) or ($log['id_user_group'] == 7)) { ?>
          <!-- Surat Edaran -->
          <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Informasi!</strong> Surat Edaran Terbaru
            <a type="button" target="_BLANK" href="<?php echo base_url() ?>files_download/SE_006.pdf"><b>Buka ...</b></a>
          </div>
        <?php } ?>

      </div>
      <!-- /widget-content -->
    </div>
  </div>
</div>
<div class="row">
  <?php if ((!empty($versi)) or (@$kuesionerFIKOM)) {
    $spn = "span8";
  } else {
    $spn = "span12";
  } ?>
  <div class="<?php echo $spn; ?>">
    <div class="widget widget-nopad">
      <div class="widget-header"> <i class="icon-list-alt"></i>
        <h3> Kalender Akademik Universitas Bhayangkara Jakarta Raya</h3>
      </div>
      <div class="widget-content">
        <div class="tabbable">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#formcontrols" data-toggle="tab">Kalender Akademik</a>
            </li>
            <li><a href="#pra" data-toggle="tab">Pra Perkuliahan</a></li>
            <li><a href="#kuliah" data-toggle="tab">Semester Perkuliahan</a></li>
            <li><a href="#bayar" data-toggle="tab">Pembayaran Perkuliahan</a></li>
          </ul>

          <div class="tab-content">
            <div class="tab-pane  active" id="formcontrols">
              <embed src="<?= base_url('calendar/old/kalender/') ?>" style="border: 0" width="750" height="670" frameborder="0" scrolling="no" />
            </div>

            <div class="tab-pane" id="pra">
              <b>
                <center>KEGIATAN PRA PERKULIAHAN</center>
              </b><br>

              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kegiatan</th>
                    <th>Waktu Kegiatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($calender1 as $isi) {

                    if ($isi->mulai == $isi->ahir) {
                      $waktu = TanggalIndo($isi->mulai);
                    } else {
                      $waktu = TanggalIndoRange($isi->mulai, $isi->ahir);
                    } ?>

                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $isi->kegiatan ?></td>
                      <td><?= $waktu ?></td>
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>
            </div>
            <div class="tab-pane" id="kuliah">
              <b>
                <center>KEGIATAN PERKULIAHAN</center>
              </b><br>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kegiatan</th>
                    <th>Waktu Kegiatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($calender2 as $isi) {

                    if ($isi->mulai == $isi->ahir) {
                      $waktu = TanggalIndo($isi->mulai);
                    } else {
                      $waktu = TanggalIndoRange($isi->mulai, $isi->ahir);
                    } ?>

                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $isi->kegiatan ?></td>
                      <td><?= $waktu ?></td>
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>
            </div>
            <div class="tab-pane" id="bayar">
              <b>
                <center>PEMBAYARAN PERKULIAHAN</center>
              </b><br>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kegiatan</th>
                    <th>Waktu Kegiatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($calender3 as $isi) {

                    if ($isi->mulai == $isi->ahir) {
                      $waktu = TanggalIndo($isi->mulai);
                    } else {
                      $waktu = TanggalIndoRange($isi->mulai, $isi->ahir);
                    } ?>

                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $isi->kegiatan ?></td>
                      <td><?= $waktu ?></td>
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php if (@$kuesionerFIKOM) : ?>
    <div class="span4">
      <div class="widget widget-nopad">

        <div class="widget-header"> <i class="icon-list-alt"></i>
          <h3> Kuesioner Evaluasi Kualitas Pelayanan</h3>
        </div>

        <!-- /widget-header -->

        <div class="widget-content">
          <ol class="news-kuesioner">
            <li><a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSdhTMduccd9TWIKIBIlC_SDL7AQr8N0iYecpgnvY6Vod-qMIg/viewform">Keusioner Pelayanan FIKOM</a></li>
          </ol>
        </div>

      </div>
    </div>
  <?php endif ?>

  <?php if (!empty($versi)) { ?>
    <div class="span4">
      <div class="widget widget-nopad">
        <div class="widget-header"> <i class="icon-list-alt"></i>
          <h3>Version&nbsp;<?php echo $versi->versi ?></h3>
        </div>
        <!-- /widget-header -->
        <div class="widget big-stats-container">
          <div class="widget-content">
            <ul class="news-items">
              <li>
                <div class="news-item-date">
                  <span class="news-item-day">
                    <?php echo datetimeIdnday($versi->created_at) ?>
                  </span>
                  <span class="news-item-month">
                    <?php echo datetimeIdnmon($versi->created_at) ?>
                  </span>
                </div>
                <div class='news-item-detail'>
                  <p class='news-item-preview'><?php echo $versi->keterangan ?></p>
                </div>
              </li>
            </ul>
            <ul class="messages_layout">
              <?php
              if (!empty($dtl_version)) { ?>
                <center>

                  <table class="table table-striped" style="width:80%;border-top:0px;">
                    <tbody>
                      <?php

                      $n = 1;
                      foreach ($dtl_version as $row) {

                        $pisah = explode(",", $row->vlog);
                        foreach ($pisah as $satuan) {
                      ?>
                          <tr style="border-top:0px;">
                            <td style="border-top:0px;width:10%;"><?php echo $n ?></td>
                            <td style="border-top:0px;"><?php echo $satuan ?> </td>
                          </tr>
                      <?php
                          $n++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </center>
              <?php
              } else {
                echo 'Tidak ada pembaruan';
              }
              ?>
            </ul>
            <p align="center"><b><a href="<?= base_url('extra/changelog') ?>">Changelog detail</a></b></p>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</div>

<div class="modal fade" id="popcalendar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Kalender Akademik Pengisian KRS 2017/2018 - Ganjil</h4>
      </div>
      <form class='form-horizontal' action="<?= base_url(); ?>organisasi/ruang/save_ruang" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <td rowspan="2" style="text-align:center">NO</td>
                <td rowspan="2" style="text-align:center">PRODI</td>
                <td colspan="2" style="text-align:center">PERIODE</td>
              </tr>
              <tr>
                <td style="text-align:center">I</td>
                <td style="text-align:center">II</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Manajemen, Akuntansi, dan Psikologi</td>
                <td>14-16 Agustus 2017</td>
                <td>23-25 Agustus 2017</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Teknik Industri, Teknik Kimia, Teknik Perminyakan, Teknik Lingkungan, dan Ilmu Komunikasi</td>
                <td>17-19 Agustus 2017</td>
                <td>25-26 Agustus 2017</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Magister Manajemen, Magister Ilmu Hukum, Ilmu Hukum, dan Teknik Informatika</td>
                <td>20-22 Agustus 2017</td>
                <td>26-27 Agustus 2017</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Mahasiswa Baru 2017</td>
                <td colspan="2">25-26 Agustus 2017/mengikuti jadwal PMB</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="popbayaran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Kegiatan Akademik 2017/2018 - Ganjil</h4>
      </div>
      <form class='form-horizontal' action="<?= base_url(); ?>organisasi/ruang/save_ruang" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <td rowspan="2" style="text-align:center">NO</td>
                <td rowspan="2" style="text-align:center">Kegiatan Akademik</td>
                <td colspan="2" style="text-align:center">PERIODE</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Pembayaran tahap V (syarat UAS) T.A 2017/2018 - Ganjil dan Pengajuan Form Dispensasi ke Prodi masing-masing (14 Desember 2017)</td>
                <td>14 November - 20 Desember 2017</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php

if ($log['user_type'] == 1) {

  $nama = $this->app_model->getdetail('tbl_karyawan', 'nid', $log['userid'], 'nik', 'asc')->row();
  $name = $nama->nama;
} elseif ($log['user_type'] == 3) {

  $nama = $this->app_model->getdetail('tbl_divisi', 'kd_divisi', $log['userid'], 'kd_divisi', 'asc')->row();
  $name = $nama->divisi;
} elseif ($log['user_type'] == 4) {

  $nama = $this->app_model->getdetail('tbl_karyawan_2', 'nip', $log['userid'], 'nip', 'asc')->row();
  $name = $nama->nama;
} elseif ($log['user_type'] == 5 || $log['user_type'] == 6 || $log['user_type'] == 7) {

  $nama = $this->app_model->getdetail('tbl_divisi', 'kd_divisi', $log['username'], 'kd_divisi', 'asc')->row();
  $name = $nama->divisi;
} else {

  $nama = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $log['userid'], 'NIMHSMSMHS', 'asc')->row();
  $name = $nama->NMMHSMSMHS;
} ?>
<div class="modal fade" id="popelearning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Teknis Persiapan Pelaksanaan Elearning di UBJ</h4>
      </div>
      <div class="modal-body">
        <label>Akses E-learning</label>
        <a href="http://elearning.ubharajaya.ac.id" target="_blank">www.elearning.ubharajaya.ac.id</a>
        <hr>
        <ol>
          <li>Prodi
            <ul>
              <li>Buka melalui akun prodi dengan menggunakan username kode prodi masing2</li>
              <li>Buat matakuliah untuk dosen</li>
              <li>Plotkan nama dosen pada matakuliah tersebut</li>
            </ul>
          </li>
          <li>Dosen
            <ul>
              <li>Login dengan Username : NID dan Password : Ubharaj4y4</li>
              <li>Lihat matakuliah yang diampu & sudah diplotkan oleh prodi. Jika matakuliah yg diajar belum muncul maka artinya belum diplotkan oleh prodi</li>
              <li>Pilih matakuliah yang diajar & dapat melakukan aktifitas perkuliahan tsb (upload materi, buat quis, chat room kelas, dll)</li>
            </ul>
          </li>
          <li>Mahasiswa
            <ul>
              <li>Login menggunakan Username : NPM dan Password : Ubharaj4y4</li>
              <li>Pilih matakuliah yang diambil & sesuai dg KRS</li>
              <li>Lakukan aktifitas perkuliahan (download materi, upload tugas, quis, dll)</li>
            </ul>
          </li>
        </ol>
        <hr>
        <strong>Note :<br>
          Apabila terdapat kendala teknis, harap menghubungi Helpdesk PTI di <br>
          <a href="https://wa.me/6281389158913?text=Saya%20<?php echo $name ?>(<?php echo $log['userid'] ?>),%20ingin%20bertanya" target="_blank">081389158913</a>(Kendala E Mail)<br>
          <a href="https://wa.me/6289530635912?text=Saya%20<?php echo $name ?>(<?php echo $log['userid'] ?>),%20ingin%20bertanya" target="_blank">089530635912</a>(Kendala E Learning)</strong>
      </div><!-- /.modal-body -->
      <br>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php if ($notif == "prd") {
  if ($notif_mhs->num_rows() > 0) { ?>
    <!-- Modal List Mahasiswa Non Aktif -->
    <div class="modal fade" id="mhs_non" tabindex="-1" role="dialog" aria-labelledby="mhs_nonLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Mahasiswa Non Aktif Di <?php get_thajar(getactyear()); ?></h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered table-striped" id="example9">
              <thead>
                <tr>
                  <th style="text-align:center">NO</th>
                  <th style="text-align:center">NPM</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1;
                foreach ($list_mhs->result() as $key => $value) : ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $value->NIMHSMSMHS . " - " . $value->NMMHSMSMHS ?></td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal List Mahasiswa Non Aktif -->
<?php
  }
}
?>
<script type="text/javascript">
  $(function() {
    $('[data-toggle="tooltip"]').tooltip()

    <?php if ($notif == "prd" || $notif == "mhs") : ?>
      $(".close-notif").on("click", function(e) {
        let id = document.querySelector(".close-notif")
        $.post("<?php echo base_url("home/read-notif") ?>", {
          rid: id.getAttribute("data-id")
        })
      })
    <?php endif ?>
  })
</script>