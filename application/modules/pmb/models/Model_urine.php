<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Model_urine extends CI_Model {



	Public function getdata($table,$key,$order)

	{

		$this->db->order_by($key, $order);

		$q = $this->db->get($table);

		return $q;

	}
	Public function getPDF($year,$gelombang,$prodi)

	{ 
		if($gelombang == 'ALL'){
			$this->dbreg->select('*');
			$this->dbreg->from('tbl_form_pmb');
			$this->dbreg->group_start();
			$this->dbreg->like('nomor_registrasi',$year,'after');
			$this->dbreg->group_end();
			$this->dbreg->where('prodi',$prodi);
			$this->dbreg->where('npm_baru is NOT NULL');
			return $this->dbreg->get();
		} else {
			$this->dbreg->select('*');
			$this->dbreg->from('tbl_form_pmb');
			$this->dbreg->group_start();
			$this->dbreg->like('nomor_registrasi',$year,'after');
			$this->dbreg->group_end();
			$this->dbreg->where('gelombang',$gelombang);
			$this->dbreg->where('prodi',$prodi);
			$this->dbreg->where('npm_baru is NOT NULL');
			return $this->dbreg->get();
		}
	} 
}

