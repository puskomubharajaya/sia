<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(96)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-
					1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		date_default_timezone_set('Asia/Jakarta'); 
	}

	public function index()
	{
		$data['page'] = "v_laporan";
		$this->load->view('template/template', $data);
	}

}

/* End of file laporan.php */
/* Location: ./application/controllers/laporan.php */