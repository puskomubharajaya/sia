<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar_ulang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') != TRUE) {
			echo '<script>alert("Silahkan pindah ke meikarta!");</script>';
			redirect(base_url('auth/logout'),'refresh');
		}
		$this->dbreg = $this->load->database('regis', TRUE);
	}

	public function index()
	{
		$data['page'] = "v_daftar_ulang";
		$this->load->view('template/template', $data);
	}

	function load_data(){
		error_reporting(0);
		$qw = $this->input->post('kampus');
		$rt = $this->input->post('program');
		$rr = $this->input->post('gelombang');
		$tt = $this->input->post('tahun');
		$data['kampus'] = $qw;
		$data['ss']  	= $rt;
		$data['gel'] 	= $rr;
		$data['jenis'] 	= 'ALL';
		$data['tahun']	= $tt;

		$year = substr($tt, 2,4);

		if ($qw == 'ALL') {
			$camp = 'kampus IN ("jkt","bks")';
		} else {
			$camp = 'kampus = "'.$qw.'"';
		}

		if ($tt == '2016') {
			$park = [$this->db,'tbl_form_camaba_2016','tbl_pmb_s2_2016','ID_registrasi'];
		} elseif ($tt == '2017') {
			$park = [$this->db,'tbl_form_camaba','tbl_pmb_s2','nomor_registrasi'];
		} elseif ($tt > '2017') {
			$park = [$this->dbreg,'tbl_form_pmb','tbl_form_pmb','nomor_registrasi'];
		}

		if ($rt == 'S1') {
			$data['fakultas'] = $this->db->query("SELECT * from tbl_fakultas where kd_fakultas != 6")->result();
			if ($rr == 'ALL') {
				$data['jumlahgel'] = $park[0]->query("SELECT MAX(gelombang) as maksbro FROM ".$park[1]." where ".$park[3]." like '".$year."%' ")->row()->maksbro;
				$data['rekap'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and '.$park[3].' like "'.$year.'%" ')->result();
			} else {
				$data['rekap'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
			}
			$this->load->view('excel_jumlah_du', $data);

		} elseif ($rt == 'S2') {
			$data['fakultas'] = $this->app_model->getdetail('tbl_fakultas','kd_fakultas','6','kd_fakultas','asc')->result();
			if ($rr == 'ALL') {
				$data['jumlahgel'] = $park[0]->query("SELECT MAX(gelombang) as maksbro FROM ".$park[1]." where ".$park[3]." like '".$year."%' ")->row()->maksbro;
				$data['rekap'] = $park[0]->query('SELECT * from '.$park[2].' where '.$camp.' and status = 3 and '.$park[3].' like "'.$year.'%" ')->result();
			} else {
				
				$data['rekap'] = $park[0]->query('SELECT * from '.$park[2].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
			}
			$this->load->view('excel_jumlah_du', $data);

		} elseif ($rt == 'ALL') {
			$data['fakultas'] = $this->db->get('tbl_fakultas')->result();
			if ($rr == 'ALL') {
				$data['jumlahgel'] = $park[0]->query("SELECT MAX(gelombang) as maksbro FROM ".$park[1]." where ".$park[3]." like '".$year."%' ")->row()->maksbro;
			} else {
				if ($tt == '2018') {
					$data['rekaps1'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
					$data['rekaps2'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
				} else {
					$data['rekaps1'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
					$data['rekaps2'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ' )->result();
				}
			}
			$this->load->view('excel_jumlah_du', $data);
		}

		// if ($tt == '2016') {
		// 	if ($rt == 'S1') {
		// 		$data['fakultas'] = $this->db->query("select * from tbl_fakultas where kd_fakultas != 6")->result();
		// 		if ($rr == 'ALL') {
		// 			$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM tbl_form_camaba_2016")->row()->maksbro;
		// 			$data['rekap'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where kampus = "'.$qw.'" and status = 3')->result();
		// 		} else {
		// 			$data['rekap'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where kampus = "'.$qw.'" and status = 3 and gelombang = "'.$rr.'"')->result();
		// 		}
		// 		$this->load->view('excel_jumlah_du', $data);
		// 	} elseif ($rt == 'S2') {
		// 		$data['fakultas'] = $this->app_model->getdetail('tbl_fakultas','kd_fakultas','6','kd_fakultas','asc')->result();
		// 		if ($rr == 'ALL') {
		// 			$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM tbl_form_camaba_2016")->row()->maksbro;
		// 			$data['rekap'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where kampus = "'.$qw.'" and status = 3')->result();
		// 		} else {
		// 			$data['rekap'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where kampus = "'.$qw.'" and status = 3 and gelombang = "'.$rr.'"')->result();
		// 		}
		// 		$this->load->view('excel_jumlah_du', $data);
		// 	} elseif ($rt == 'ALL') {
		// 		//$data['gelombang'] = $this->db->get('tbl_gel_pmb')->result();
		// 		$data['fakultas'] = $this->db->get('tbl_fakultas')->result();
		// 		if ($rr == 'ALL') {
		// 			$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM tbl_form_camaba_2016")->row()->maksbro;
		// 			//$data['rekaps1'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where kampus = "'.$qw.'" and status = 3')->result();
		// 			//$data['rekaps2'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where kampus = "'.$qw.'" and status = 3')->result();
		// 		} else {
		// 			$data['rekaps1'] = $this->db->query('SELECT * from tbl_form_camaba_2016 where kampus = "'.$qw.'" and status = 3 and gelombang = "'.$rr.'"')->result();
		// 			$data['rekaps2'] = $this->db->query('SELECT * from tbl_pmb_s2_2016 where kampus = "'.$qw.'" and status = 3 and gelombang = "'.$rr.'"')->result();
		// 		}
		// 		$this->load->view('excel_jumlah_du', $data);
		// 	}
		// } elseif ($tt == '2017') {
		// 	if ($rt == 'S1') {
		// 		$data['fakultas'] = $this->db->query("select * from tbl_fakultas where kd_fakultas != 6")->result();
		// 		if ($rr == 'ALL') {
		// 			$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM tbl_form_camaba")->row()->maksbro;
		// 			$data['rekap'] = $this->db->query('SELECT * from tbl_form_camaba where kampus = "'.$qw.'" and status = 3')->result();
		// 		} else {
		// 			$data['rekap'] = $this->db->query('SELECT * from tbl_form_camaba where kampus = "'.$qw.'" and status = 3 and gelombang = "'.$rr.'"')->result();
		// 		}
		// 		$this->load->view('excel_jumlah_du', $data);
		// 	} elseif ($rt == 'S2') {
		// 		$data['fakultas'] = $this->app_model->getdetail('tbl_fakultas','kd_fakultas','6','kd_fakultas','asc')->result();
		// 		if ($rr == 'ALL') {
		// 			$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM tbl_form_camaba")->row()->maksbro;
		// 			$data['rekap'] = $this->db->query('SELECT * from tbl_pmb_s2 where kampus = "'.$qw.'" and status = 3')->result();
		// 		} else {
		// 			$data['rekap'] = $this->db->query('SELECT * from tbl_pmb_s2 where kampus = "'.$qw.'" and status = 3 and gelombang = "'.$rr.'"')->result();
		// 		}
		// 		$this->load->view('excel_jumlah_du', $data);
		// 	} elseif ($rt == 'ALL') {
		// 		//$data['gelombang'] = $this->db->get('tbl_gel_pmb')->result();
		// 		$data['fakultas'] = $this->db->get('tbl_fakultas')->result();
		// 		if ($rr == 'ALL') {
		// 			$data['jumlahgel'] = $this->db->query("SELECT MAX(gelombang) as maksbro FROM tbl_form_camaba")->row()->maksbro;
		// 			//$data['rekaps1'] = $this->db->query('SELECT * from tbl_form_camaba where kampus = "'.$qw.'" and status = 3')->result();
		// 			//$data['rekaps2'] = $this->db->query('SELECT * from tbl_pmb_s2 where kampus = "'.$qw.'" and status = 3')->result();
		// 		} else {
		// 			$data['rekaps1'] = $this->db->query('SELECT * from tbl_form_camaba where kampus = "'.$qw.'" and status = 3 and gelombang = "'.$rr.'"')->result();
		// 			$data['rekaps2'] = $this->db->query('SELECT * from tbl_pmb_s2 where kampus = "'.$qw.'" and status = 3 and gelombang = "'.$rr.'"')->result();
		// 		}
		// 		$this->load->view('excel_jumlah_du', $data);
		// 	}
		// }
		
		
	}

	function load_data2(){
		error_reporting(0);
		$qw = 'ALL';
		$rt = 'ALL';
		$rr = '4';
		$tt = '2019';
		$data['kampus'] = $qw;
		$data['ss']  	= $rt;
		$data['gel'] 	= $rr;
		$data['jenis'] 	= 'ALL';
		$data['tahun']	= $tt;

		$year = substr($tt, 2,4);

		if ($qw == 'ALL') {
			$camp = 'kampus IN ("jkt","bks")';
		} else {
			$camp = 'kampus = "'.$qw.'"';
		}

		if ($tt == '2016') {
			$park = [$this->db,'tbl_form_camaba_2016','tbl_pmb_s2_2016','ID_registrasi'];
		} elseif ($tt == '2017') {
			$park = [$this->db,'tbl_form_camaba','tbl_pmb_s2','nomor_registrasi'];
		} elseif ($tt > '2017') {
			$park = [$this->dbreg,'tbl_form_pmb','tbl_form_pmb','nomor_registrasi'];
		}

		if ($rt == 'S1') {
			$data['fakultas'] = $this->db->query("SELECT * from tbl_fakultas where kd_fakultas != 6")->result();
			if ($rr == 'ALL') {
				$data['jumlahgel'] = $park[0]->query("SELECT MAX(gelombang) as maksbro FROM ".$park[1]." where ".$park[3]." like '".$year."%' ")->row()->maksbro;
				$data['rekap'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and '.$park[3].' like "'.$year.'%" ')->result();
			} else {
				$data['rekap'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
			}
			$this->load->view('excel_jumlah_du', $data);

		} elseif ($rt == 'S2') {
			$data['fakultas'] = $this->app_model->getdetail('tbl_fakultas','kd_fakultas','6','kd_fakultas','asc')->result();
			if ($rr == 'ALL') {
				$data['jumlahgel'] = $park[0]->query("SELECT MAX(gelombang) as maksbro FROM ".$park[1]." where ".$park[3]." like '".$year."%' ")->row()->maksbro;
				$data['rekap'] = $park[0]->query('SELECT * from '.$park[2].' where '.$camp.' and status = 3 and '.$park[3].' like "'.$year.'%" ')->result();
			} else {
				
				$data['rekap'] = $park[0]->query('SELECT * from '.$park[2].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
			}
			$this->load->view('excel_jumlah_du', $data);

		} elseif ($rt == 'ALL') {
			$data['fakultas'] = $this->db->get('tbl_fakultas')->result();
			if ($rr == 'ALL') {
				$data['jumlahgel'] = $park[0]->query("SELECT MAX(gelombang) as maksbro FROM ".$park[1]." where ".$park[3]." like '".$year."%' ")->row()->maksbro;
			} else {
				if ($tt == '2018') {
					$data['rekaps1'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
					$data['rekaps2'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
				} else {
					$data['rekaps1'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ')->result();
					$data['rekaps2'] = $park[0]->query('SELECT * from '.$park[1].' where '.$camp.' and status = 3 and gelombang = "'.$rr.'" and '.$park[3].' like "'.$year.'%" ' )->result();
				}
			}
		}

		foreach ($data['rekaps1'] as $key => $value) {
			$arr[] = $value->nomor_registrasi;
		}

		foreach ($data['rekaps2'] as $key => $value) {
			$arr[] = $value->nomor_registrasi;
		}

			echo "<pre>";
			print_r ($arr);
			echo "</pre>"; die();
			// $this->load->view('excel_jumlah_du', $data);
	}

}

/* End of file daftar_ulang.php */
/* Location: ./application/controllers/daftar_ulang.php */