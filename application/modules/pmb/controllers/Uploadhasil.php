<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploadhasil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		// if ($this->session->userdata('sess_login') == TRUE) {
		// 	$cekakses = $this->role_model->cekakses(90)->result();
		// 	if ($cekakses != TRUE) {
		// 		echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
		// 	}
		// } else {
		// 	redirect('auth','refresh');
		// }
		date_default_timezone_set('Asia/Jakarta'); 
	}

	function index()
	{
		$data['gel']   = $this->db->group_by('gelombang')->get('tbl_form_camaba')->result();
		$data['prodi'] = $this->db->get('tbl_jurusan_prodi')->result();

		$data['page'] = 'v_uploadlulus_home';
		$this->load->view('template/template', $data);
	}

	function dwld_form()
	{
		 $ee = $this->input->post('gelombang');
		 $aa = $this->input->post('program');
		 //die($ee);
		
		if ($aa == 's1') {
			$data['qw'] = $this->db->query('SELECT * from tbl_form_camaba where gelombang = "'.$ee.'"')->result();
			$this->load->view('v_excel_s1', $data);
		} elseif($aa == 's2') {
			$data['qr'] = $this->db->query('SELECT * from tbl_pmb_s2 where gelombang = "'.$ee.'"')->result();
			$this->load->view('v_excel_s2', $data);
		}
		
	}

	function upload_excel()
	{
		$session=$this->session->userdata('sess_dosen');
		$user = $session['userid'];

		$sa = $this->input->post('gel');
		$ds = $this->input->post('prodi');

		
		if ($_FILES['userfile']) {
			$this->load->helper('inflector');
			$nama = underscore($_FILES['userfile']['name']);

			$config['upload_path'] = './temp_upload/';
			$config['allowed_types'] = 'xls|xlsx';
			$config['file_name'] = $nama;
			$config['max_size'] = 1000000;    
			$this->load->library('upload', $config);
			

			if (!$this->upload->do_upload("userfile")) {
				$data = array('error', $this->upload->display_errors());
				var_dump($data);exit();
				// echo "<script>alert('Gagal');
				// document.location.href='".base_url()."pmb/uploadhasil/';</script>";
			} else {
	   			$upload_data = $this->upload->data(); 
	   			$this->load->library('excel_reader');
	   			$this->excel_reader->setOutputEncoding('CP1251');
				$file=$upload_data['full_path'];
				$this->excel_reader->read($file);
				$data=$this->excel_reader->sheets[0];
				$dataexcel = array();

				for ($i = 1; $i <= $data['numRows']; $i++) {
					// if ($i != 1) {
						if($data['cells'][$i][1] == '') break;
		                $dataexcel[$i-1]['nomor_registrasi'] = $data['cells'][$i][2];
		                if (strtoupper($data['cells'][$i][4]) == 'LULUS') {
		                	$angka = 1;
		                } else {
		                	$angka = 0;
		                }
		                $dataexcel[$i-1]['status'] = $angka;
		                
					//}
				}
				$this->stat($dataexcel, $ds);
	    	}
		}
	}

	function stat($dataarray, $ds)
    {
    	//var_dump($dataarray);
    	//die($ds);
        for($i=1; $i<count($dataarray); $i++){

            if ($ds== 's1') {
            	$datax = array(
                	'status'	=> $dataarray[$i]['status']
                	);

            	$this->db->where('nomor_registrasi', $dataarray[$i]['nomor_registrasi']);
            	$this->db->update('tbl_form_camaba',$datax);
            	//$this->app_model->updatedata('tbl_form_camaba_palsu','id_form',, $datax);

            } elseif($ds == 's2') {
            	$datax = array(
                	'status'	=>$dataarray[$i]['status']
                    );
            	$this->db->where('ID_registrasi', $dataarray[$i]['nomor_registrasi']);
            	$this->db->update('tbl_pmb_s2',$datax);
             }
            echo "<script>alert('Sukses');
			document.location.href='".base_url()."pmb/uploadhasil/';</script>";
         }
    	
    }	
}

/* End of file UploadHasil.php */
/* Location: ./application/modules/pmb/controllers/UploadHasil.php */