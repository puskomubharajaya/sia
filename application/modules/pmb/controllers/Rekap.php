<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login') != TRUE) {
			redirect('auth');
		}
		$this->dbreg = $this->load->database('regis', TRUE);
	}

	public function index()
	{
		$sesi = $this->session->userdata('sess_login');
		if ($sesi['id_user_group'] == 8) {
			$data['prods'] = $sesi['userid'];
		} elseif ($sesi['id_user_group'] == 9) {
			$data['prods'] = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_fakultas = '".$sesi['userid']."'")->result();
		} else {
			$data['prods'] = $this->db->query('SELECT * from tbl_jurusan_prodi')->result();
		}
		
		$data['page'] = "v_rekap";
		$this->load->view('template/template', $data);
	}

	function load()
	{
		$sesi = $this->session->userdata('sess_login');
		$data['sess'] = $sesi['id_user_group'];
		if ($sesi['id_user_group'] == 8) {
			$data['prod'] = $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$sesi['userid']."'")->row();
		}

		$program 	= $this->input->post('program');
		$gelombang 	= $this->input->post('gelombang');
		$sts 		= $this->input->post('status');
		$prodd		= $this->input->post('prodi');
		$jenis 		= $this->input->post('jns');

		$data['prog'] = $this->input->post('program');
		$data['year'] = $this->input->post('tahun');

		$angkatan = substr($data['year'], 2,4);

		if ($this->input->post('tahun') == '2016') {
			$kuy = [$this->db,'tbl_form_camaba_2016','tbl_pmb_s2_2016','status', $angkatan, 'nomor_registrasi','ID_registrasi'];
		} elseif ($this->input->post('tahun') == '2017') {
			$kuy = [$this->db,'tbl_form_camaba','tbl_pmb_s2','status', $angkatan,'nomor_registrasi','ID_registrasi'];
		} else {
			$kuy = [$this->dbreg,'view_form_pmb','view_form_pmb','status', $angkatan, 'nomor_registrasi','nomor_registrasi'];
		}
		//var_dump($this->input->post('tahun'));exit();

			if ($program == 'S1') {

				if ($gelombang == 'ALL') {

					if ($sts == 'ALL') {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where jenis_pmb = "'.$jenis.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							}	

						} else {

							if ($jenis == 'AL') {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where prodi = "'.$prodd.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							}

						}

					} else {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where '.$kuy[3].' '.$sts.' and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where jenis_pmb = "'.$jenis.'" and '.$kuy[3].' '.$sts.' and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							}	

						} else {

							if ($jenis == 'AL') {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where prodi = "'.$prodd.'" and '.$kuy[3].' '.$sts.' and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();


							} else {
								// var_dump($kuy[0].'--'.$kuy[1].'--'.$prodd.'--'.$jenis.'--'.$kuy[3].'--'.$sts);exit();
								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and '.$kuy[3].' '.$sts.' and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();
								
							}

						}
						
					}
						
				} else {

					if ($sts == 'ALL') {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where gelombang = "'.$gelombang.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							}	

						} else {

							if ($jenis == 'AL') {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							}

						}

					} else {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where '.$kuy[3].' '.$sts.' and gelombang = "'.$gelombang.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where jenis_pmb = "'.$jenis.'" and '.$kuy[3].' '.$sts.' and gelombang = "'.$gelombang.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							}	

						} else {

							if ($jenis == 'AL') {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where prodi = "'.$prodd.'" and '.$kuy[3].' '.$sts.' and gelombang = "'.$gelombang.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wow'] = $kuy[0]->query('SELECT * from '.$kuy[1].' where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and '.$kuy[3].' '.$sts.' and gelombang = "'.$gelombang.'" and program ="1" and '.$kuy[5].' like "'.$angkatan.'%"')->result();
								
							}

						}
						
					}
						
				}
				
				$this->load->view('excel_rekap_s1', $data);

			} elseif ($program == 'S2') {

				if ($gelombang == 'ALL') {

					if ($sts == 'ALL') {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where '.$kuy[6].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where jenis_pmb = "'.$jenis.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							}	

						} else {

							if ($jenis == 'AL') {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where prodi = "'.$prodd.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							}

						}

					} else {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where '.$kuy[3].' '.$sts.'  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where jenis_pmb = "'.$jenis.'" and '.$kuy[3].' '.$sts.'  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							}	

						} else {

							if ($jenis == 'AL') {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where prodi = "'.$prodd.'" and '.$kuy[3].' '.$sts.'  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and '.$kuy[3].' '.$sts.'  and '.$kuy[6].' like "'.$angkatan.'%"')->result();
								
							}

						}
						
					}
						
				} else {

					if ($sts == 'ALL') {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where gelombang = "'.$gelombang.'"   and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							}	

						} else {

							if ($jenis == 'AL') {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where prodi = "'.$prodd.'" and gelombang = "'.$gelombang.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and gelombang = "'.$gelombang.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							}

						}

					} else {

						if ($prodd == 'ALL') {

							if ($jenis == 'AL') {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where status  '.$sts.' and gelombang = "'.$gelombang.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where jenis_pmb = "'.$jenis.'" and '.$kuy[3].' '.$sts.' and gelombang = "'.$gelombang.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							}	

						} else {

							if ($jenis == 'AL') {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where prodi = "'.$prodd.'" and '.$kuy[3].' '.$sts.' and gelombang = "'.$gelombang.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();

							} else {

								$data['wew'] = $kuy[0]->query('SELECT * from '.$kuy[2].' where prodi = "'.$prodd.'" and jenis_pmb = "'.$jenis.'" and '.$kuy[3].' '.$sts.' and gelombang = "'.$gelombang.'"  and '.$kuy[6].' like "'.$angkatan.'%"')->result();
								
							}

						}
						
					}
						
				}	
				// var_dump($data['wew']);exit();
				$this->load->view('excel_rekap_s2', $data);

			} 
		
	}

	function changejur($id){
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'jenjang', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='prodi' id='jurs'><option>--Pilih Prodi--</option><option value='ALL'>Semua Prodi</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

}

/* End of file Rekap.php */
/* Location: ./application/modules/pmb/controllers/Rekap.php */
