<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>pmb/gelombangpmb/view_edit/'+id);
}
</script>

<script>
    $(function() {
        $( "#from" ).datepicker({
            changeMonth: true,
            numberOfMonths: 1
        });
        $( "#to" ).datepicker({
            changeMonth: true,
            numberOfMonths: 1
        });
    });
</script>   

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Data Gelombang Pendaftaran</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                        <a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="btn-icon-only icon-plus"></i>Tambah Data</a>
                        <a href="#" class="btn btn-primary"><i class="btn-icon-only icon-fire"></i>Rekap Data</a>
                        <hr>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>Gelombang</th>
                                    <th>Waktu Mulai</th>
                                    <th>Waktu Selesai</th>
                                    <th width="80">Peserta</th>
                                    <th width="80">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($gel as $row) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->nm_gel; ?></td>
                                    <td><?php echo $row->mulai_gel; ?></td>
                                    <td><?php echo $row->akhir_gel; ?></td>
                                    <td></td>
                                    <td>
                                    	<!--a class="btn btn-warning btn-small" href="#"><i class="btn-icon-only icon-ok"> </i></a-->
                                        <a onclick="edit(<?php echo $row->id_gel; ?>)" class="btn btn-primary btn-small" href="#editModal" data-toggle="modal"><i class="btn-icon-only icon-pencil"> </i></a>
									    <a onclick="return confirm('Apakah Anda Yakin?');" class="btn btn-danger btn-small" href="<?php echo base_url(); ?>pmb/gelombangpmb/delete/<?php echo $row->id_gel; ?>"><i class="btn-icon-only icon-remove"> </i></a>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                </div>                
            </div>
        </div>
    </div>
</div>

<!-- add modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Form Tambah Data</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>pmb/gelombangpmb/add" method="post" enctype="multipart/form-data">
                <div class="modal-body" style="margin-left: -60px;">  
					<div class="control-group" id="">
                        <label class="control-label">Gelombang</label>
                        <div class="controls">
                            <input type="text" class="span4" name="kode" placeholder="Input Gelombang" class="form-control" value="" required/>
                        </div>
                    </div>			
                    <div class="control-group" id="">
                        <label class="control-label">Waktu Mulai</label>
                        <div class="controls">
                            <input type="text" class="span3" name="mulai" placeholder="Input Waktu Mulai" class="form-control" value="" id="from" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Waktu Selesai</label>
                        <div class="controls">
                            <input type="text" class="span3" name="akhir" placeholder="Input Waktu Selesai" class="form-control" value="" id="to" required/>
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
                    <input type="reset" class="btn btn-default" value="Clear"/>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
