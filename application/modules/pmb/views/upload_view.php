
<div class="row">

	<div class="span12">      		  		

  		<div class="widget">

  			<div class="widget-header">

  				<i class="icon-user"></i>

  				<h3>Form Upload Hasil Ujian PMB</h3>

			</div> <!-- /widget-header -->

			<div class="widget-content">

				<div class="span11">
					<!-- <form action="<?php echo base_url(); ?>pmb/uploadhasil/dwld_form">
	                    <div class="control-group" id="">
	                        <div class="controls">
	                            <select name="gelo" class="span4" required>
	                            	<option disabled selected> Pilih Program </option>
	                            	<option value="s1">S1</option>
	                            	<option value="s2">S2</option>
	                            </select>
	                            </div>
	                    </div><hr>
					</form> -->
					<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Download Excel </a><br><hr>
					
					<!-- <a href="<?php echo base_url(); ?>pmb/uploadhasil/dwld_form/1" target="_blank" title=""><button type="submit" class="btn btn-md btn-primary" style="margin-top:-10px;">Download Form S1</button></a>
	                <a href="<?php echo base_url(); ?>pmb/uploadhasil/dwld_form/2" target="_blank" title=""><button type="submit" class="btn btn-md btn-primary" style="margin-top:-10px;">Download Form S2</button></a>
	                        <br><hr> -->
					

					<form class ='form-horizontal' enctype="multipart/form-data" action="<?php echo base_url(); ?>pmb/uploadhasil/upload_excel" method="post">
		                
	                    <div class="control-group" id="">
	                        <label class="control-label">Upload Hasil Ujian</label>
	                        <div class="controls">
	                            <input type="file" class="span4 " name="userfile" class="form-control" value="#" required/>
	                        </div>
	                    </div>

	                    <!-- <div class="control-group" id="">
	                        <label class="control-label">Gelombang</label>
	                        <div class="controls">
	                            <select name="gel" class="span4" required>
	                            	<option disabled selected> Pilih Gelombang </option>
	                            	<?php foreach ($gelombang->result() as $value) { ?>
	                            		<option value="<?php echo $value->id_gel; ?>"> <?php echo $value->nm_gel; ?> </option>
	                            	<?php } ?>
	                            </select>
	                        </div>
	                    </div> -->
	                    <div class="control-group" id="">
	                        <label class="control-label">Program</label>
	                        <div class="controls">
	                        	<select name="prodi" class="span4" required>
	                            	<option disabled selected> Pilih Program </option>
	                            	<option value="s1"> S1 </option>
	                            	<option value="s2"> S2 </option>
	                            </select>
	                            <!-- <select name="prodi" class="span4" required>
	                            	<option disabled selected> Pilih Program </option>
	                            	<?php foreach ($prodi as $row) { ?>
	                            		<option value="<?php echo $row->kd_prodi; ?>"> <?php echo $row->prodi; ?> </option>
	                            	<?php } ?>
	                            </select> -->
	                        </div>
	                    </div>
		                <div>
		                    <button type="button" class="btn btn-default" >Keluar</button>
		                    <input type="submit" class="btn btn-primary" value="Submit"/>
		                </div>
	            	</form>

				</div>

			</div>

		</div>

	</div>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Download Excel</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url('pmb/uploadhasil/dwld_form');?>" method="post">
                <div class="modal-body" style="margin-left:-20px;">    
                   <div class="control-group" id="">
                        <label class="control-label">Program</label>
                        <div class="controls">
                            <select name="program" class="span4" required>
                            	<option disabled selected> Pilih Program </option>
                            	<option value="s1">S1</option>
                            	<option value="s2">S2</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="">
	                    <label class="control-label">Gelombang</label>
	                    <div class="controls">
	                        <select name="gelombang" class="span4" required>
	                        	<option disabled selected> Pilih Gelombang </option>
	                        	<option value="1">1</option>
	                        </select>
	                    </div>
	                </div>
                </div>   
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Submit"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>