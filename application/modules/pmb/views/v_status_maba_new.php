<?php
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=data_mhs_baru_s1.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<style>
    table,
    td,
    th {
        border: 1px solid black;
    }

    th {
        background-color: blue;
        color: black;
    }
</style>

<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Nomor Registrasi</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Agama</th>
            <th>Negeri/Swasta</th>
            <th>Asal Sekolah</th>
            <th>Kota Asal Sekolah</th>
            <th>Penghasilan</th>
            <th>Status</th>
            <th>NPM</th>
            <th>Fakultas</th>
            <th>Prodi</th>
            <th width="120">Gelombang</th>
            <th>Telepon</th>
            <th>Pekerjaan</th>
            <th>Pekerjaan Ayah</th>
            <th>Pekerjaan Ibu</th>
            <th>Ukuran Almamater</th>
            <th>Prodi 2</th>
            <th>Prodi 3</th>
            <th>Prodi 4</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($wew as $row) { ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $row->nomor_registrasi; ?></td>
                <td><?php echo $row->nama; ?></td>
                <?php if ($row->kelamin == 'L') {
                        $klm = 'Laki-laki';
                    } else {
                        $klm = 'Perempuan';
                    }
                    ?>
                <td><?php echo strtoupper($klm); ?></td>
                <?php switch ($row->agama) {
                        case 'ISL':
                            $agm = 'ISLAM';
                            break;
                        case 'KTL':
                            $agm = 'Katolik';
                            break;
                        case 'PRT':
                            $agm = 'Protestan';
                            break;
                        case 'BDH':
                            $agm = 'Budha';
                            break;
                        case 'HND':
                            $agm = 'Hindu';
                            break;
                        case '':
                            $agm = '-';
                            break;
                    } ?>
                    <td><?php echo strtoupper($agm); ?></td>
                    <?php if ($row->kategori_skl == 'NGR') {
                            $jadi = 'Negeri';
                        } else {
                            $jadi = 'Swasta';
                        }
                        ?>
                    <td><?php echo $jadi; ?></td>
                    <td><?php echo $row->asal_sch_maba; ?></td>
                    <td><?php echo $row->kota_sch_maba; ?></td>
                    <td><?php if ($row->penghasilan == 1) {
                                echo "Rp 1,000,000 - 2,000,000";
                            } elseif ($row->penghasilan == 2) {
                                echo "Rp 2,100,000 - 4,000,000";
                            } elseif ($row->penghasilan == 3) {
                                echo "Rp 4,100,000 - 5,999,000";
                            } elseif ($row->penghasilan == 4) {
                                echo ">= Rp 6,000,000";
                            } else {
                                echo "0";
                            }
                            ?></td>
                    <td><?php if ($row->status < 1) {
                                echo "Registrasi";
                            } elseif ($row->status == 1) {
                                echo "Lulus Tes";
                            } elseif ($row->status > 1) {
                                echo "Daftar Ulang";
                            } ?></td>
                    <td><?php echo $row->npm_baru; ?></td>
                    <?php $prodinya = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_prodi', $row->prodi, 'kd_prodi', 'asc')->row()->prodi; ?>
                    <td><?php echo $prodinya; ?></td>
                    <td><?= $row->fakultas?></td>
                    <td width="120"><?php echo $row->gelombang; ?></td>
                    <td><?php echo $row->tlp; ?></td>
                    <td><?php echo $row->pekerjaan; ?></td>
                    <?php
                        if ($row->workdad == 'LL') {
                            $parentwork = 'Lain-lain';
                        } elseif ($row->workdad == 'PN') {
                            $parentwork  = 'Pegawai Negeri';
                        } elseif ($row->workdad == 'PS') {
                            $parentwork = 'Pegawai Swasta';
                        } elseif ($row->workdad == 'PSN') {
                            $parentwork = 'Pensiun';
                        } elseif ($row->workdad == 'TK') {
                            $parentwork = 'Tidak Bekerja';
                        } elseif ($row->workdad == 'TP') {
                            $parentwork = 'TNI / POLRI';
                        } elseif ($row->workdad == 'WU') {
                            $parentwork = 'Wirausaha';
                        } else {
                            $parentwork = '-';
                        }
                        ?>
                    <td><?php echo $parentwork ?></td>
                    <?php
                        if ($row->workmom == 'LL') {
                            $momwork = 'Lain-lain';
                        } elseif ($row->workmom == 'PN') {
                            $momwork  = 'Pegawai Negeri';
                        } elseif ($row->workmom == 'PS') {
                            $momwork = 'Pegawai Swasta';
                        } elseif ($row->workmom == 'PSN') {
                            $momwork = 'Pensiun';
                        } elseif ($row->workmom == 'TK') {
                            $momwork = 'Tidak Bekerja';
                        } elseif ($row->workmom == 'TP') {
                            $momwork = 'TNI / POLRI';
                        } elseif ($row->workmom == 'WU') {
                            $momwork = 'Wirausaha';
                        } else {
                            $momwork = '-';
                        }
                        ?>
                    <td><?php echo $momwork ?></td>
                    <?php
                        if ($row->almet == 'S') {
                            $almet = 'S';
                        } elseif ($row->almet == 'M') {
                            $almet  = 'M';
                        } elseif ($row->almet == 'L') {
                            $almet = 'L';
                        } elseif ($row->almet == 'X') {
                            $almet = 'XL';
                        } elseif ($row->almet == '2') {
                            $almet = 'XXL';
                        } elseif ($row->almet == '3') {
                            $almet = 'XXXL';
                        } else {
                            $almet = '-';
                        }
                        ?>
                    <td><?php echo $almet ?></td>
                    <td><?php echo get_jur($row->prodi) ?></td>
                    <td><?php echo get_jur($row->prodi2) ?></td>
                    <td><?php echo get_jur($row->prodi3) ?></td>
                    <td><?php echo get_jur($row->prodi4) ?></td>
            </tr>
        <?php $no++;
        } ?>
    </tbody>
</table>