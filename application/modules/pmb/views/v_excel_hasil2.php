<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=hasil_tes.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table>
	<thead>
	    <tr> 
	        <th width="30">No</th>
	        <th>Nama Gelombang</th>
	        <th>Peserta</th>
	        <th>lulus</th>
	</thead>
	<tbody>
	    <?php $no=1; foreach ($pong as $row) { ?>
	    <tr>
	        <?php $k =  substr($row->ID_registrasi, 0,7); $j = substr($k, 6); ?>
	        <td><?php echo number_format($no); ?></td>
	        <td><?php echo 'GELOMBANG '.$j; ?></td>
	        <?php ?>
	        <td><?php echo $row->nama; ?></td>
	        <?php ?>
	        <td><?php if ($row->status == 1) {
	            echo "LULUS";
	        } elseif($row->status == 0) {
	            echo "TIDAK LULUS";
	        }
	         ?>
	         </td>
	    </tr>
	    <?php $no++; } ?>
	</tbody>
</table>