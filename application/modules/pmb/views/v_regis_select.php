<script type="text/javascript">

    $(document).ready(function() {

        <?php $tgl = date('Y'); $mintgl = $tgl-20; ?>

        $('#tgl1').datepicker({
            dateFormat: "yy-mm-dd",

            yearRange: "2017:<?php echo $tgl; ?>",

            changeMonth: true,

            changeYear: true

        });

        $('#tgl2').datepicker({
            dateFormat: "yy-mm-dd",

            yearRange: "2017:<?php echo $tgl; ?>",

            changeMonth: true,

            changeYear: true

        });

    });

</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-calendar"></i>
  				<h3>Pilih Tanggal Pendaftaran</h3>
			</div>
			<div class="widget-content">
				<form class="form-horizontal" action="<?php echo base_url(); ?>pmb/regist/save_sess" method="post">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Mulai Tanggal</label>
							<div class="controls">
								<input type="text" class="form-control span2" id="tgl1" name="tgl1" value="" placeholder="Masukan Tanggal Awal"> &nbsp;&nbsp; Sampai &nbsp;&nbsp;
                <input type="text" class="form-control span2" id="tgl2" name="tgl2" value="" placeholder="Masukan Tanggal Ahir">
							</div>
						</div>
						<div class="form-actions">
							<input class="btn btn-large btn-success" type="submit" value="Submit">
						</div>

					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

