<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-book"></i>
          <h3>NILAI PMB</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <b><center>INPUT NILAI PMB</center></b><br>
          <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>pmb/nilai/create_session">
            <fieldset>
              <div class="control-group">                     
                <label class="control-label">Penerimaan</label>
                <div class="controls">
                  <input type="text" class="span3" name="ta_show" value="Tahun Ajaran - 2017/2018" disabled>
                  <input type="hidden" name="ta" value="2017" required>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              
              <div class="control-group">                     
                <label class="control-label">Gelombang</label>
                <div class="controls">
                  <select name="gel" required>
                    <option>-- Pilih Gelombang PMB --</option>
                    <option value="1">Gelombang I</option>
                    <option value="2">Gelombang II</option>
                    <option value="3">Gelombang III</option>
                    <option value="4">Gelombang IV</option>
                    <option value="5">Gelombang V</option>
                    <option value="99">Gelombang EXTRA</option>
                  </select>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->

               <div class="control-group">                     
                <label class="control-label">Prodi</label>
                <div class="controls">
                  <select name="prodi" required>
                    <option>-- Pilih Program Studi --</option>
                    <?php foreach ($fak as $key): $prodi = $this->db->where('kd_fakultas',$key->kd_fakultas)->get('tbl_jurusan_prodi')->result();?>
                    <optgroup label="<?php echo $key->fakultas; ?>">
                      <?php foreach ($prodi as $isi): ?>
                          <option value="<?php echo $isi->kd_prodi ?>"><?php echo $isi->prodi; ?></option>
                      <?php endforeach ?>
                    </optgroup>
                    <?php endforeach ?>
                  </select>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->


              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>