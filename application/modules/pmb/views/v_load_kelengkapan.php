<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Daftar Kelengkapan Arsip PMB (<?php echo $mhspmb->nomor_registrasi; ?> - <?php echo $mhspmb->nama; ?>)</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url(); ?>datas/mahasiswapmb/save_lengkap" method="post">
<input type="hidden" name="id" value="<?php echo $mhspmb->nomor_registrasi; ?>">
    <div class="modal-body">  
        <div class="control-group" id="">
        <p>Tahun Lulus :  <?php echo $mhspmb->lulus_maba; ?></p>
            <table id="" class="table table-bordered table-striped">
            	<thead>
                    <tr> 
                        <th>No</th>
                        <th>Data Arsip</th>
                        <th width="50">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                	<?php $hasil = $this->db_regis->query("SELECT * from tbl_form_pmb where nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->row();
                	if ($hasil->jenis_pmb == 'MB') { ?>
	                	 <tr id="maba" class="akt">
	                        <td>1</td>
	                        <td>Akte Kelahiran</td>
	                        <?php $tanya = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%AKT%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="AKT" <?php if($tanya == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="kk">
	                        <td>2</td>
	                        <td>Kartu Keluarga (KK)</td>
	                        <?php $tanya2 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%KK%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="KK" <?php if($tanya2 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="ktp">
	                        <td>3</td>
	                        <td>Kartu Tanda Penduduk (KTP)</td>
	                        <?php $tanya3 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%KTP%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="KTP" <?php if($tanya3 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="rpt">
	                        <td>4</td>
	                        <td>Rapot</td>
	                        <?php $tanya4 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%RP%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="RP" <?php if($tanya4 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="skhun">
	                        <td>5</td>
	                        <td>SKHUN</td>
	                        <?php $tanya5 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%SKHUN%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="SKHUN" <?php if($tanya5 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="ijz">
	                        <td>6</td>
	                        <td>Ijazah</td>
	                        <?php $tanya6 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%IJZ%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="IJZ" <?php if($tanya6 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="skl">
	                        <td>7</td>
	                        <td>Surat Kelulusan</td>
	                        <?php $tanya7 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%SKL%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="SKL" <?php if($tanya7 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="ft">
	                        <td>8</td>
	                        <td>Foto (3x4 dan 4x6)</td>
	                        <?php $tanya8 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%FT%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="FT" <?php if($tanya8 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="lkp">
	                        <td>*</td>
	                        <td><b>Lengkap Administratif</b></td>
	                        <?php $tanya9 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%LLKP%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="LLKP" <?php if($tanya9 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
                	<?php } elseif ($hasil->jenis_pmb == 'RM') { ?>
	                	<tr id="maba" class="akt">
	                        <td>1</td>
	                        <td>Akte Kelahiran</td>
	                        <?php $tanya = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%AKT%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="AKT" <?php if($tanya == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="kk">
	                        <td>2</td>
	                        <td>Kartu Keluarga (KK)</td>
	                        <?php $tanya2 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%KK%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="KK" <?php if($tanya2 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="ktp">
	                        <td>3</td>
	                        <td>Kartu Tanda Penduduk (KTP)</td>
	                        <?php $tanya3 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%KTP%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="KTP" <?php if($tanya3 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                	 <tr id="rm" class="tkr">
	                        <td>4</td>
	                        <td>Transkrip</td>
	                        <?php $tanya10 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%TKR%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="TKR" <?php if($tanya10 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="rm" class="baa">
	                        <td>5</td>
	                        <td>Laporan BAA</td>
	                        <?php $tanya11 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%LBAA%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="LBAA" <?php if($tanya11 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="rm" class="bpak">
	                        <td>6</td>
	                        <td>Keterangan RENKEU</td>
	                        <?php $tanya12 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%KTREN%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="KTREN" <?php if($tanya12 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="ft">
	                        <td>7</td>
	                        <td>Foto (3x4 dan 4x6)</td>
	                        <?php $tanya8 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%FT%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="FT" <?php if($tanya8 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="lkp">
	                        <td>*</td>
	                        <td><b>Lengkap Administratif</b></td>
	                        <?php $tanya9 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%LLKP%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="LLKP" <?php if($tanya9 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
                	<?php } elseif ($hasil->jenis_pmb == 'KV') { ?>
	                	<tr id="maba" class="akt">
	                        <td>1</td>
	                        <td>Akte Kelahiran</td>
	                        <?php $tanya = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%AKT%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="AKT" <?php if($tanya == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="kk">
	                        <td>2</td>
	                        <td>Kartu Keluarga (KK)</td>
	                        <?php $tanya2 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%KK%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="KK" <?php if($tanya2 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="ktp">
	                        <td>3</td>
	                        <td>Kartu Tanda Penduduk (KTP)</td>
	                        <?php $tanya3 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%KTP%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="KTP" <?php if($tanya3 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
						<tr id="maba" class="ijz">
	                        <td>4</td>
	                        <td>Ijazah</td>
	                        <?php $tanya6 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%IJZ%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="IJZ" <?php if($tanya6 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
						<tr id="maba" class="ft">
	                        <td>5</td>
	                        <td>Foto (3x4 dan 4x6)</td>
	                        <?php $tanya8 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%FT%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="FT" <?php if($tanya8 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
						<tr id="rm" class="tkr">
	                        <td>6</td>
	                        <td>Transkrip</td>
	                        <?php $tanya10 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%TKR%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="TKR" <?php if($tanya10 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                	<tr id="kv" class="spd">
	                        <td>7</td>
	                        <td>Surat Pindah</td>
	                        <?php $tanya13 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%SPD%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="SPD" <?php if($tanya13 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="kv" class="stf">
	                        <td>8</td>
	                        <td>Sertifikat Akreditasi</td>
	                        <?php $tanya14 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%SAK%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="SAK" <?php if($tanya14 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
	                    <tr id="maba" class="lkp">
	                        <td>*</td>
	                        <td><b>Lengkap Administratif</b></td>
	                        <?php $tanya9 = $this->db_regis->query("select status_kelengkapan from tbl_form_pmb where status_kelengkapan LIKE '%LLKP%' AND nomor_registrasi = '".$mhspmb->nomor_registrasi."'")->result();   ?>
	                        <td><input type="checkbox" name="lengkap[]" value="LLKP" <?php if($tanya9 == TRUE){echo "checked";} ?>/></td>
	                    </tr>
                	<!--input type="hidden" name="jadwal" value="<?php //echo $value->kd_jadwal; ?>"/-->
                	<?php  } ?>
                </tbody>
            </table>
            <!--input type="hidden" name="jumlah" value="<?php //echo $no; ?>" /-->
            
        </div>              
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>