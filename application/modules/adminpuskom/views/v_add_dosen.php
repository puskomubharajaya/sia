<script>
    $(function() {
        $( "#tgl" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd",
            yearRange: "1950:2030"
        });
    });
</script>
<?php $user = $this->session->userdata('sess_mbalia'); ?>
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Tambah Data Dosen</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>adminpuskom/board">Home</a> / <a href="<?php echo base_url(); ?>adminpuskom/add_dosen">Tambah Dosen</a>
					<br><br>
					<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>adminpuskom/add_dosen/add" enctype="multipart/form-data">
						<fieldset>
							<div class="control-group">											
								<label class="control-label">NIDN : </label>
								<div class="controls">
									<input type="number" class="span6" id="aww" name="nidn" placeholder="Masukan NIDN" required>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">											
								<label class="control-label">NID : </label>
								<div class="controls">
									<input type="number" class="span6" id="aww" name="nid" placeholder="Masukan NID" required>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">											
								<label class="control-label">NUPN : </label>
								<div class="controls">
									<input type="number" class="span6" id="aww" name="nupn" placeholder="Masukan NUPN" required>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">											
								<label class="control-label">NIK : </label>
								<div class="controls">
									<input type="number" class="span6" id="aww" name="nik" placeholder="Masukan NIK" required>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">											
								<label class="control-label">Nama : </label>
								<div class="controls">
									<input type="text" class="span6" id="aww" name="nama" placeholder="Masukan Nama" required>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">
								<label class="control-label">Jenis Kelamin</label>
								<div class="controls">
									<select class="form-control span6"  name="jenis" required>
										<option disabled="" selected="">-- Pilih Jenis Kelamin --</option>
										<option value="L">Laki-laki</option>
										<option value="P">Perempuan</option>
									</select>
								</div>
							</div>
							<div class="control-group">											
								<label class="control-label">Alamat : </label>
								<div class="controls">
									<textarea type="text" class="span6" id="aww" name="alamat" placeholder="Masukan Alamat" required></textarea>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">											
								<label class="control-label">No. Handphone : </label>
								<div class="controls">
									<input type="number" class="span6" id="aww" name="hp" placeholder="Masukan No. HP" required>
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							<div class="control-group">											
								<label class="control-label">E-mail : </label>
								<div class="controls">
									<input type="email" class="span6" id="aww" name="email" placeholder="Masukan E-mail" required>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">											
								<label class="control-label">Jabatan : </label>
								<div class="controls">
									<input type="number" class="span6" id="aww" name="jab" placeholder="Masukan Jabatan" required>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">
								<label class="control-label">Status :</label>
								<div class="controls">
									<select class="form-control span6"  name="sts" required>
										<option disabled="" selected="">-- Pilih Status --</option>
										<option value="1">Aktif</option>
										<option value="0">Non-aktif</option>
									</select>
								</div>
							</div>
							<div class="control-group">											
								<label class="control-label">Foto : </label>
								<div class="controls">
									<input type="file" class="span6" id="aww" name="userfile">
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">											
								<label class="control-label">Tipe NIK : </label>
								<div class="controls">
									<input type="number" class="span6" id="aww" name="typ" placeholder="Masukan Tipe NIK" required>
								</div> <!-- /controls -->				
							</div>
							<div class="control-group">											
								<label class="control-label">Tanggal Masuk : </label>
								<div class="controls">
									<input type="text" class="span6" id="tgl" name="tgl" placeholder="Masukan Tanggal Masuk" required>
								</div> <!-- /controls -->				
							</div>
							<div class="form-actions">
								<input type="submit" class="btn btn-primary" id="save" value="Submit"/>
							</div> <!-- /form-actions -->
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>