<script type="text/javascript">

jQuery(document).ready(function($) {

    $('input[name^=old]').autocomplete({

        source: '<?php echo base_url('adminpuskom/reset/load_dosen_autocomplete');?>',

        minLength: 1,

        select: function (evt, ui) {

            this.form.old.value = ui.item.value;

        }

    });

});

</script>

<?php $user = $this->session->userdata('sess_mbalia'); ?>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Reset Password</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>adminpuskom/board">Home</a> / <a href="<?php echo base_url(); ?>adminpuskom/reset">reset password</a>
					<b><center>Reset Password Akun</center></b><br>
					<form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>adminpuskom/reset/ganti">
						<fieldset>
							<div class="control-group">											
								<label class="control-label">User ID : </label>
								<div class="controls">
									<input type="text" class="span3" id="old" name="old" placeholder="Masukan User ID" required>
								</div> <!-- /controls -->				
							</div> <!-- /control-group -->
							<div class="form-actions">
								<input type="submit" class="btn btn-primary" id="save" value="Reset Password"/>
							</div> <!-- /form-actions -->
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>