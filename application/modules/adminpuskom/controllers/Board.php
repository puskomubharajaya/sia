<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Board extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_mbalia') != TRUE) {
			redirect('adminpuskom/home', 'refresh');
		}
	}

	public function index()
	{
		$data['page'] = 'v_board';
		$this->load->view('adminpuskom/template', $data);
	}
}

/* End of file Board.php */
/* Location: ./application/modules/mbalia/controllers/Board.php */
