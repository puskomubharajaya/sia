<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formrps extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('temph_model');
		$this->load->library('excel');
		$logged = $this->session->userdata('sess_login');
		if ($this->session->userdata('sess_login') != TRUE) {
			// $cekakses = $this->role_model->cekakses(97)->result();
			// if ($cekakses != TRUE) {
			// 	echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			// }
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$log = $this->session->userdata('sess_login');
		$thn = $this->app_model->getdetail('tbl_tahunakademik','status',1,'id','asc')->row();
		$data['user'] = $log['userid'];
		$data['tahunajar'] = $thn->tahun_akademik;

		$pecah = explode(',', $log['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(6, $grup))) {
			$data['load'] = $this->temph_model->loadmkrps($log['userid'],$thn->kode)->result();
			$data['page'] = "v_list_mk_rps";
		} elseif ((in_array(8, $grup))) {
			$data['load'] = $this->temph_model->loaddosenrps($log['userid'],$thn->kode)->result();
			$data['page'] = "v_list_dosen_ajar_rps";
		}

		$this->load->view('template/template', $data);
	}

	function dwld($kd)
	{
		$log = $this->session->userdata('sess_login');
		$data['thn'] = $this->app_model->getdetail('tbl_tahunakademik','status',1,'id','asc')->row();
		$data['user'] = $this->app_model->getdetail('tbl_karyawan','nid',$log['userid'],'nid','asc')->row();
		$data['load'] = $this->temph_model->load_data_for_rps($kd)->row();
		$this->load->view('excel_rps', $data);
	}

	function load_modal($kode,$prod)
	{
		$data['kdmk'] = $kode;
		$data['prod'] = $prod;
		$this->load->view('v_modal_upld_rps', $data);
	}

	function upld()
	{
		if ($_FILES['userfile']) {
			//  cek prodi MK
			$kd = $this->input->post('kdmk');
			$prodi = $this->app_model->getdetail('tbl_matakuliah','kd_matakuliah',$kd,'id_matakuliah','asc')->row();

			// get tahun ajar
			$ta = $this->app_model->getdetail('tbl_tahunakademik','status',1,'id','asc')->row();
			
			// cek file
			$this->load->helper('inflector');
			$nama = rand(1,10000000).underscore(str_replace('/', '', $this->security->sanitize_filename($_FILES['userfile']['name'])));
			
			// cek format file
			$kadal = explode('.', $nama);
			if (count($kadal) > 2) {
				echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1)';</script>";exit();
			}
			// die($kadal[1]);
			if ($kadal[1] != 'pdf') {
				echo "<script>alert('FORMAT FILE TIDAK SESUAI!');history.go(-1)';</script>";
			}
			

			$config['upload_path'] = './temp_formrps/';
			$config['allowed_types'] = 'doc|docx|pdf';
			$config['file_name'] = $nama;
			$config['max_size'] = 1000000;
            
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload("userfile")) {
				$data = array('error', $this->upload->display_errors());
				var_dump($data);exit();
				echo "<script>alert('Gagal');document.location.href='".base_url()."form/formrps/';</script>";
			} else {
				$user = $this->session->userdata('sess_login');
				$data['filename'] 		= $config['file_name'];
				$data['filepath'] 		= $config['upload_path'].$nama;
				$data['userfile'] 		= $user['userid'];
				$data['kd_matakuliah'] 	= $kd;
				$data['prodi'] 			= $this->input->post('prod');
				$data['tahunajaran']	= $ta->kode;
				$data['time_upload'] 	= date('Y-m-d H:i:s');
				
				// var_dump($data);exit();
				$this->db->insert('tbl_file_rps', $data);
				echo "<script>alert('File Tersimpan');document.location.href='".base_url()."form/formrps/';</script>";    			    	
			}
	    }
	}
}

/* End of file Formrps.php */
/* Location: ./application/modules/form/controllers/Formrps.php */