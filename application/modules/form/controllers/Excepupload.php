<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excepupload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect(base_url('auth/logout'),'refresh');
		}
		$this->load->library('cart');
	}

	public function index()
	{
		$this->session->unset_userdata('excep');
        $this->session->set_userdata('excep',array());

		$data['page'] = "exception_v";
		$this->load->view('template/template', $data);
	}

	function load_dosen()
    {
        $this->db->distinct();
        $this->db->select("nid,nama");
        $this->db->from('tbl_karyawan');
        $this->db->like('nid', $_GET['term'], 'both');
        $this->db->or_like('nama', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();

        foreach ($sql->result() as $row) {
            $data[] = array(
                    'nama'          => $row->nama,
                    'value'         => $row->nid.' - '.$row->nama,
                    'nid'          => $row->nid,
                    );
        }
        echo json_encode($data);
    }

    function outtemporary()
    {
        extract(PopulateForm());

        $dsn = explode(' - ', $nid);
        $nip = $dsn[0];

        // make session for foreach
        $ses = $this->session->userdata('excep');
        $cp	 = count($ses);

        if ($cp == 0) {
            $data = array(
                    $cp => array(
                        'nid' => $nip, 
                        'nama' => $dsn[1],
                    )
                );
            $this->session->set_userdata('jml_array',$cp);     

            $arr = $ses + $data;
            $count = $cp+1;
            $this->session->unset_userdata('excep');
            $this->session->set_userdata('excep',$arr);
            $this->session->set_userdata('jml_array',$count);

         } else {
            $i_arr = $this->session->userdata('jml_array');
            $data = array(
                $i_arr => array(
                    'nid' => $nip, 
                    'nama' => $dsn[1],
                )
            );     

            $arr = $ses + $data;
            $count = $i_arr+1;
            $this->session->unset_userdata('excep');
            $this->session->unset_userdata('jml_array');
            
            $this->session->set_userdata('excep',$arr);
            $this->session->set_userdata('jml_array',$count);
         }
    }

    function loadTable()
    {
        $data['team'] = $this->cart->contents();
        $this->load->view('temptablexcep',$data); 
    }

    function deleteList($id){
        $arr = $this->session->userdata('excep');

        unset($arr[$id]);
        
        $this->session->unset_userdata('excep');
        $this->session->set_userdata('excep',$arr);
    }

    function add()
    {
    	$logged = $this->session->userdata('sess_login');
    	$actyear = getactyear();
    	extract(PopulateForm());

    	// Init Array dosen
    	$dosen = [];

        for ($i=0; $i < count($nid); $i++) { 
            
            // insert to tbl_dropout
            $dosen[] = [
                        'nid' => $nid[$i],
                        'tahunakademik' => $actyear,
                        'is_active' => 1,
                        'testtype' => NULL,
                        'insert_by' => $logged['userid']
                        ];
        }
    	
    	$this->db->insert_batch('tbl_exception', $dosen);

    	redirect(base_url('form/excepupload/listDosen'),'refresh');
    }

    function listDosen()
    {
        $log = $this->session->userdata('sess_login');

        $this->db->distinct();
        $this->db->select('a.nid,a.tahunakademik');
        $this->db->from('tbl_exception a');
        $this->db->join('tbl_jadwal_matkul b', 'a.nid = b.kd_dosen');
        $this->db->where('a.is_active', 1);
        $this->db->group_start();
        $this->db->like('b.kd_jadwal', $log['userid'], 'after');
        $this->db->group_end();
        $data['data'] = $this->db->get()->result();

    	// $data['data'] = $this->db->query("SELECT * from tbl_exception where is_active = '1'")->result();
    	$data['page'] = "listdosen_dispenupload";
    	$this->load->view('template/template', $data);
    }

    function endDispen($uid,$year)
    {
    	$arr = ['is_active' => null];
    	$this->app_model->updatedata('tbl_exception','nid',$uid,$arr);
    	echo "<script>alert('Berhasil!');history.go(-1)</script>";
    }

    function endAllDispen()
    {
        $log = $this->session->userdata('sess_login');

    	$arr = ['is_active' => null];
        $this->db->where('insert_by', $log['userid']);
    	$this->db->update('tbl_exception', $arr);
    	echo "<script>alert('Berhasil!');history.go(-1)</script>";
    }

}

/* End of file Exception.php */
/* Location: .//tmp/fz3temp-1/Exception.php */