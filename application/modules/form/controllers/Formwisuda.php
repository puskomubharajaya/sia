<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Formwisuda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		// ini_set('memory_limit', '1024M');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(83)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}
		$this->load->library('Cfpdf');
		date_default_timezone_set('Asia/Jakarta'); 
	}

	function index()
	{
		$data['page'] = 'v_tahun_wisuda';
		$this->load->view('template/template', $data);
	}

	function savesess()
	{
		extract(PopulateForm());
		$this->session->set_userdata('tahunwisuda', $tahun);
		redirect(base_url('form/formwisuda/list_wisudawan'));
	}

	function list_wisudawan()
	{
		$date = $this->session->userdata('tahunwisuda');
		$logged = $this->session->userdata('sess_login');
		$data['mhs'] = $this->app_model->getmhslulus($date)->result();
		// var_dump($logged['userid']);exit();
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ( (in_array(10, $grup)) OR (in_array(1, $grup))) {
			$data['page'] = 'wisuda_look_baa';
			$this->load->view('template/template', $data);
		} elseif ((in_array(9, $grup))) {
			$data['page'] = 'wisuda_look_fak';
			$this->load->view('template/template', $data);
		} elseif ((in_array(11, $grup))) {
			$data['page'] = 'wisuda_look_renkeu';
			$this->load->view('template/template', $data);
		} elseif ((in_array(12, $grup))) {
			$data['page'] = 'wisuda_look_perpus';
			$this->load->view('template/template', $data);
		}
	}

	function savefakultas()
	{
		$jumlahmhs = count($this->input->post('fak'));

		for ($i=0; $i < $jumlahmhs; $i++) { 
			$pecah = explode('zzz', $this->input->post('fak['.$i.']'));
			$param = $pecah[0];
			$data['flag_wisuda_fak'] = $param;
			$mhs = $pecah[1];
			$this->app_model->updatedata('tbl_mahasiswa','NIMHSMSMHS',$mhs,$data);
		}
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formwisuda';</script>";
	}

	function saverenkeu()
	{
		$jumlahmhs = count($this->input->post('renkeu'));

		for ($i=0; $i < $jumlahmhs; $i++) { 
			$pecah = explode('zzz', $this->input->post('renkeu['.$i.']'));
			$param = $pecah[0];
			$data['flag_wisuda_renkeu'] = $param;
			$mhs = $pecah[1];
			$this->app_model->updatedata('tbl_mahasiswa','NIMHSMSMHS',$mhs,$data);
		}
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formwisuda';</script>";
	}

	function savebaa()
	{
		$jumlahmhs = count($this->input->post('baa'));

		for ($i=0; $i < $jumlahmhs; $i++) { 
			$pecah = explode('zzz', $this->input->post('baa['.$i.']'));
			$param = $pecah[0];
			$data['flag_wisuda_baa'] = $param;
			$mhs = $pecah[1];
			$this->app_model->updatedata('tbl_mahasiswa','NIMHSMSMHS',$mhs,$data);
		}
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formwisuda';</script>";
	}

	function saveperpus()
	{
		$jumlahmhs = count($this->input->post('perpus'));

		for ($i=0; $i < $jumlahmhs; $i++) { 
			$pecah = explode('zzz', $this->input->post('perpus['.$i.']'));
			$param = $pecah[0];
			$data['flag_wisuda_perpus'] = $param;
			$mhs = $pecah[1];
			$this->app_model->updatedata('tbl_mahasiswa','NIMHSMSMHS',$mhs,$data);
		}
		echo "<script>alert('Sukses');
		document.location.href='".base_url()."form/formwisuda';</script>";
	}

	function exportdataexcel()
	{
		$date = date('Y');
		$data['mhs'] = $this->app_model->getmhslulus($date)->result();
		//var_dump($data['mhs']);
		$this->load->view('print/export_excel_wisudawan', $data);
	}

	function pdf_wisuda()
	{
		$this->load->view('wisuda_pdf',$data);
	}

	function cetakkartuwisuda($npm)
	{
		$data['wisudawan'] 	= $this->db->query("SELECT * from tbl_mahasiswa a join tbl_jurusan_prodi b 
												on a.KDPSTMSMHS = b.kd_prodi join tbl_fakultas c 
												on b.kd_fakultas = c.kd_fakultas 
												where a.NIMHSMSMHS LIKE '%".$npm."%'")->row();
		$this->load->view('welcome/print/surat_lulus',$data);
	}
}

/* End of file Formwisuda.php */
/* Location: ./application/controllers/Formwisuda.php */