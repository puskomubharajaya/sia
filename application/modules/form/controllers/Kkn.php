<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kkn extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		error_reporting(0);
		//$id_menu = 55 (database); cek apakah user memiliki akses
		/* if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(79)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		} */
		// check total credit
		if ($this->session->userdata('sess_login')['user_type'] == 2) {
			$isCreditsEnough = get_total_sks($this->session->userdata('sess_login')['userid']);
			if ($isCreditsEnough < 110) {
				echo '<script>alert("SKS tidak mencukupi! Minimal SKS untuk mendaftar KKN adalah 110 SKS.");history.go(-1);</script>';
				exit();
			}
		}
		$this->load->library('Cfpdf');
	}

	function index()
	{
		$user = $this->session->userdata('sess_login');
		$nik   = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		$cek_kkn = $this->app_model->getDetail('tbl_kkn', 'npm', $nik, 'id_kkn', 'desc', '1')->row();
		if ((in_array(5, $grup))) {
			if (!empty($cek_kkn)) {
				$data['list_formulir'] = $this->app_model->getDetail('tbl_kkn', 'npm', $nik, 'id_kkn', 'desc')->result();
				$data['formulir'] = $this->app_model->getDetail('tbl_kkn', 'npm', $nik, 'id_kkn', 'desc', '1')->row();
				$data['page'] = 'v_kkn_mhs';
			} else {
				redirect('form/kkn/isi', 'refresh');
			}
		} elseif ((in_array(20, $grup))) {
			$data['pengajuan'] = $this->app_model->getData('tbl_kkn', 'id_kkn', 'desc')->result();
			$data['page'] = 'v_kkn';
		}
		$this->load->view('template/template', $data);
	}

	function master()
	{
		$user = $this->session->userdata('sess_login');
		$nik   = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		for ($i = 0; $i < $jmlh; $i++) {
			$grup[] = $pecah[$i];
		}
		if ((in_array(20, $grup))) {
			$data['master'] = $this->app_model->getData('tbl_master_kkn', 'id_master_kkn', 'desc')->result();
			$data['page'] = 'v_kkn_master';
			$this->load->view('template/template', $data);
		} else {
			redirect(base_url(), 'refresh');
		}
	}
	function isi()
	{
		$user = $this->session->userdata('sess_login');
		$npm   = $user['userid'];
		$cek_kkn = $this->app_model->getDetail('tbl_kkn', 'npm', $npm, 'id_kkn', 'desc', '1')->row();
		if (empty($cek_kkn)) {
			$data['jenis'] = $this->app_model->getMasterKkn()->result();
			$data['mhs'] = $this->app_model->getDetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'id_mhs', 'DESC', '1')->row();
			$data['bio'] = $this->app_model->getDetail('tbl_bio_mhs', 'npm', $npm, 'id', 'DESC', '1')->row();
			$data['akt'] = $this->app_model->getDetail('tbl_aktifitas_kuliah_mahasiswa', 'NIMHSTRAKM', $npm, 'THSMSTRAKM', 'DESC', '1')->row();
			$data['kls'] = $this->app_model->getDetail('tbl_verifikasi_krs', 'npm_mahasiswa', $npm, 'tahunajaran', 'DESC', '1')->row();

			$data['page'] = 'form_kkn';
			$this->load->view('template/template', $data);
		} else {
			redirect('form/kkn', 'refresh');
		}
	}
	function cetak($id)
	{
		$user = $this->session->userdata('sess_login');
		$npm   = $user['userid'];
		$kkn = $this->app_model->getDetail('tbl_kkn', 'id_kkn', $id, 'id_kkn', 'DESC', '1')->row();
		if ($kkn->npm == $npm) {
			$data['mhs'] = $this->app_model->getDetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'id_mhs', 'DESC', '1')->row();
			$data['bio'] = $this->app_model->getDetail('tbl_bio_mhs', 'npm', $npm, 'id', 'DESC', '1')->row();
			$data['akt'] = $this->app_model->getDetail('tbl_aktifitas_kuliah_mahasiswa', 'NIMHSTRAKM', $npm, 'THSMSTRAKM', 'DESC', '1')->row();
			$data['kkn'] = $kkn;
			$data['kls'] = $this->app_model->getDetail('tbl_verifikasi_krs', 'npm_mahasiswa', $npm, 'tahunajaran', 'DESC', '1')->row();
			$this->load->view('cetak_kkn', $data);
		} else {
			redirect('form/kkn', 'refresh');
		}
	}
	function print_kkn($id, $npm)
	{
		$data['mhs'] = $this->app_model->getDetail('tbl_mahasiswa', 'NIMHSMSMHS', $npm, 'id_mhs', 'DESC', '1')->row();
		$data['bio'] = $this->app_model->getDetail('tbl_bio_mhs', 'npm', $npm, 'id', 'DESC', '1')->row();
		$data['akt'] = $this->app_model->getDetail('tbl_aktifitas_kuliah_mahasiswa', 'NIMHSTRAKM', $npm, 'THSMSTRAKM', 'DESC', '1')->row();
		$data['kkn'] = $this->app_model->getDetail('tbl_kkn', 'id_kkn', $id, 'id_kkn', 'DESC', '1')->row();
		$data['kls'] = $this->app_model->getDetail('tbl_verifikasi_krs', 'npm_mahasiswa', $npm, 'tahunajaran', 'DESC', '1')->row();
		$this->load->view('cetak_kkn', $data);
	}

	function save()
	{
		$user = $this->session->userdata('sess_login');
		$npm   = $user['userid'];
		$createddate = date('Y-m-d H:i:s');
		$data = array(
			'alamat'	=> $this->input->post('alamat'),
			'no_hp'		=> $this->input->post('tlp')
		);
		$data_kkn = array(
			'npm'			=> $npm,
			'penyakit'		=> $this->input->post('penyakit'),
			'kkn'		=> $this->input->post('opt_kkn'),
			'createddate'	=> $createddate
		);

		$this->app_model->updatedata('tbl_bio_mhs', 'npm', $npm, $data);
		$this->app_model->insertdata('tbl_kkn', $data_kkn);
		redirect('form/kkn', 'refresh');
	}

	function add_master()
	{
		$data = array(
			'kd_kkn'	=> strtoupper($this->input->post('kd_kkn')),
			'nama_kkn'	=> $this->input->post('nama_kkn'),
			'isactivated'	=> $this->input->post('isactivated')
		);

		$this->app_model->insertdata('tbl_master_kkn', $data);
		redirect('form/kkn/master', 'refresh');
	}
	function edit_master()
	{
		$data = array(
			'nama_kkn'	=> $this->input->post('nama_kkn'),
			'isactivated'	=> $this->input->post('isactivated')
		);

		$this->app_model->updatedata('tbl_master_kkn', 'kd_kkn', $this->input->post('kd_kkn'), $data);
		redirect('form/kkn/master', 'refresh');
	}
	function hapus_kkn($id)
	{
		$this->app_model->deletedata('tbl_master_kkn', 'id_master_kkn', $id);
		redirect('form/kkn/master', 'refresh');
	}
	function edit_kkn($id)
	{
		$data['kkn'] = $this->app_model->getdetail('tbl_master_kkn', 'id_master_kkn', $id, 'id_master_kkn', 'desc')->row();
		$this->load->view('v_kkn_master_edit', $data);
	}

	function cari()
	{
		if (isset($_POST['kd_kkn'])) {
			$kd_kkn = $_POST['kd_kkn'];
			$results = $this->app_model->cari_kd('tbl_master_kkn', 'kd_kkn', $kd_kkn);
			if ($results === TRUE) {
				$a = '<span style="color:red;">Kode KKN sudah digunakan sebelumnya</span>';
				$b = "<script>document.getElementById('save').disabled = true;</script>";
				echo $a, $b;
			} else {
				$a = '<span style="color:green;">Kode KKN dapat digunakan</span>';
				$b = "<script>document.getElementById('save').disabled = false;</script>";
				echo $a, $b;
			}
		} else {
			$a = '<span style="color:red;">Kode KKN harus diisi</span>';
			$b = "<script>document.getElementById('save').disabled = true;</script>";
			echo $a, $b;
		}
	}
}
