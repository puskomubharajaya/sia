<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetakabsensiujian extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
		error_reporting(0);
		//$id_menu = 55 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(79)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}
		$this->load->library('Cfpdf');
		$this->load->model('form/uji_model','ujian');
	}

	function index()
	{
		$user = $this->session->userdata('sess_login');

		$nik   = $user['userid'];
		
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if ( (in_array(1, $grup)) or (in_array(10, $grup)) ) {
			
			$data['fakultas'] =$this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();

	        $data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

	        $data['page']='absuji_view';

	    }elseif ((in_array(9, $grup))){

	    	$data['jurusan'] = $this->app_model->getdetail('tbl_jurusan_prodi','kd_fakultas',$nik, 'kd_fakultas', 'ASC')->result();

	    	$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

	    	$data['page']='absuji_view';

	    }elseif ((in_array(8, $grup) or in_array(19, $grup))) {

	    	$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();

	    	$data['page']='absuji_view';

		}
		$this->load->view('template/template', $data);
	}

	function get_jurusan($id)
	{
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function save_session()
	{
		$user = $this->session->userdata('sess_login');
		$nik = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		if (in_array(9, $grup)) {
			$fakultas = $nik;

			$jurusan = $this->input->post('jurusan');

	        $tahunajaran = $this->input->post('tahunajaran');

	        $semester = $this->input->post('semester');

	        $this->session->set_userdata('semester', $semester);

	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_fakultas_prasyarat', $nik);

			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);
		} elseif (in_array(8, $grup) or in_array(19, $grup)) {
			$jurusan = $nik;

			$namajur = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$nik,'kd_prodi','asc')->row();	

			$tahunajaran = $this->input->post('tahunajaran');

			$semester = $this->input->post('semester');

	        $this->session->set_userdata('semester', $semester);

	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_fakultas_prasyarat', $namajur->kd_fakultas);

			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);		
		} else {

			$fakultas = $this->input->post('fakultas');

			$jurusan = $this->input->post('jurusan');

	        $tahunajaran = $this->input->post('tahunajaran');

	        $semester = $this->input->post('semester');

	        $this->session->set_userdata('semester', $semester);

	        $this->session->set_userdata('tahunajaran', $tahunajaran);

			$this->session->set_userdata('id_fakultas_prasyarat', $fakultas);

			$this->session->set_userdata('id_jurusan_prasyarat', $jurusan);
		}
		
		redirect(base_url('form/cetakabsensiujian/viewdata'));
	}

	function viewdata()
	{
		$user = $this->session->userdata('sess_login');
		$nik = $user['userid'];
		$pecah = explode(',', $user['id_user_group']);
		$jmlh = count($pecah);
		
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}

		$data['rows'] = $this->app_model->get_matkul_ajar($this->session->userdata('tahunajaran'),$nik,$this->session->userdata('semester'))->result();

		$data['page'] = 'absuji_detail';
		$this->load->view('template/template', $data);
	}

	function cetakpdf($id_jdl)
	{
		$log                = $this->session->userdata('sess_login');
		$prodi              = $log['userid'];
		$tahunajaran        = $this->session->userdata('tahunajaran');
		$kd_jadwal          = get_kd_jdl($id_jdl);
		$data['kd_jadwal']  = $kd_jadwal;
		$data['tipe']       = 2;
		$data['rows']       = $this->app_model->get_kelas_mahasiswa_ujian2(2,$kd_jadwal,$tahunajaran)->result();
		$data['absendosen'] = $this->ujian->last_meeting($kd_jadwal);
		$data['prodi']      = $this->ujian->get_prodi_fakultas($prodi);
		$data['line']       = $this->ujian->get_detail_jadwal($kd_jadwal);
		$this->load->view('welcome/print/print_absen', $data);
	}

	function cetakpdfsusulan($kode)
	{
		$user = $this->session->userdata('sess_login');
		$nik = $user['userid'];
		$data['rows'] = $this->app_model->get_kelas_mahasiswa_ujian2($nik,4,5,$kode,$this->session->userdata('tahunajaran'))->result();
		/*$data['garis'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
											join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_jurusan_prodi c on c.`kd_prodi`=b.`kd_prodi`
											join tbl_fakultas d on d.`kd_fakultas`=c.`kd_fakultas`
											where a.`id_jadwal`="'.$kode.'" ')->row();*/
		$data['garis'] = $this->db->query('SELECT * from tbl_jurusan_prodi c
											join tbl_fakultas d on d.`kd_fakultas`=c.`kd_fakultas`
											where c.`kd_prodi` ="'.$nik.'" ')->row();
		$data['line'] =	$this->db->query('SELECT * from tbl_jadwal_matkul a
											join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_karyawan c on c.`nid`=a.`kd_dosen`
											where a.`kd_jadwal`="'.$kdjadwal.'"')->row();
		//var_dump($data['line']);exit();

		$this->load->view('welcome/print/print_absen', $data);
		//var_dump($data['rows']);
	}

	function cetakpdfuas($kode)
	{
		$user = $this->session->userdata('sess_login');
		$nik = $user['userid'];
		$kdjadwal = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$kode,'id_jadwal','asc')->row()->kd_jadwal;
		$data['kd_jadwal'] = $kdjadwal;
		$data['tipe'] = 4;
		$data['rows'] = $this->app_model->get_kelas_mahasiswa_ujian2(4,$kdjadwal,$this->session->userdata('tahunajaran'))->result();
		$data['absendosen'] = $this->db->query("SELECT MAX(pertemuan) as satu FROM tbl_absensi_mhs_new_20171 where kd_jadwal = '".$kdjadwal."'")->row();
		/*$data['garis'] = $this->db->query('SELECT * from tbl_jadwal_matkul a
											join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_jurusan_prodi c on c.`kd_prodi`=b.`kd_prodi`
											join tbl_fakultas d on d.`kd_fakultas`=c.`kd_fakultas`
											where a.`id_jadwal`="'.$kode.'" ')->row();*/
		$data['garis'] = $this->db->query('SELECT * from tbl_jurusan_prodi c
											join tbl_fakultas d on d.`kd_fakultas`=c.`kd_fakultas`
											where c.`kd_prodi` ="'.$nik.'" ')->row();
		$data['line'] =	$this->db->query('SELECT * from tbl_jadwal_matkul a
											join tbl_matakuliah b on a.`kd_matakuliah`=b.`kd_matakuliah`
											join tbl_karyawan c on c.`nid`=a.`kd_dosen`
											where a.`kd_jadwal`="'.$kdjadwal.'" AND SUBSTR(a.`kd_jadwal`,1,5) = b.`kd_prodi`')->row();
		//var_dump($data['line']);exit();

		$this->load->view('welcome/print/print_absen', $data);
		//var_dump($data['rows']);
	}

}

/* End of file Cetakabsensiujian.php */
/* Location: ./application/modules/form/controllers/Cetakabsensiujian.php */