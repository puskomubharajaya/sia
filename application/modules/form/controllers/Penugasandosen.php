<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penugasandosen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		error_reporting(0);		
		//$id_menu = 55 (database); cek apakah user memiliki akses
		if ($this->session->userdata('sess_login') != TRUE) {
		// 	$akses = $this->role_model->cekakses(50)->result();
		// 	if ($akses != TRUE) {
		// 		redirect('home','refresh');
		// 	}
		// } else {
			redirect('auth','refresh');
		}
	}

	function index(){
		// $user = $this->session->userdata('sess_login');
		// $data['user'] = $user['userid'];
		// //$divisi = $this->app_model->get_jabatan_user($user['userid'])->row();
		// $data['divisi'] = $this->app_model->getdetail('tbl_divisi','kd_divisi',$user['userid'],'kd_divisi','asc')->row_array();
		// $data['tahunajaran'] = $this->app_model->getdatathn()->result();
		// $data['dosen'] = $this->app_model->getdatadosen($user['userid'])->result();
		// $data['perihal'] = $this->app_model->getdata('tbl_perihal', 'id_perihal', 'ASC')->result();
		// $data['getData'] = $this->app_model->getdata('tbl_penugasan_dosen', 'id', 'ASC')->result();
		// $data['mahasiswakp'] = $this->app_model->getkrsmhskp($user['userid'])->result();
		// $data['mahasiswaskripsi'] = $this->app_model->getkrsmhsskripsi($user['userid'])->result();
		// $data['page']='form_penugasan_dosen';
		$data['tahunajar']=$this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page']='form_penugasan_dosen_select';
		$this->load->view('template/template', $data);
	}

	function save_session_prodi()
	{
		$ta = $this->input->post('tahunajaran', TRUE);
		$this->session->set_userdata('ta',$ta);
		$jenis = $this->input->post('jenis', TRUE);
		$this->session->set_userdata('jenis',$jenis);
		redirect('form/penugasandosen/viewdata','refresh');
	}

	function viewdata()
	{
		$user = $this->session->userdata('sess_login');
		//var_dump($this->session->userdata('jenis'));exit();
		switch ($this->session->userdata('jenis')) {
			case '1': //skripsi
				$data['getData'] = $this->app_model->getkrsmhsskripsi($user['userid'],$this->session->userdata('ta'))->result();
				break;
			case '2': //tesis
				$data['getData'] = $this->app_model->getkrsmhstesis($user['userid'],$this->session->userdata('ta'))->result();
				break;
			
			case '3': //kp
				$data['getData'] = $this->app_model->getkrsmhskp($user['userid'],$this->session->userdata('ta'))->result();
				break;
		}
		$data['tahunajaran'] = $this->app_model->getdetail('tbl_tahunakademik','kode',$this->session->userdata('ta'),'kode','asc')->row();
		$data['page']='form_penugasan_dosen_view';
		$this->load->view('template/template', $data);
	}

	function savedata()
	{
		// $data['perihal'] = $this->input->post('perihal', TRUE);
		// $data['nik'] = $this->input->post('dosen', TRUE);
		// $data['deskripsi'] = $this->input->post('deskripsi', TRUE);
		// $data['userid'] = $this->input->post('user', TRUE);
		// $data['kd_divisi'] = $this->input->post('unit', TRUE);
		// if ($data['perihal'] == 'pa') {
		// 	$data['penugasan'] = $this->input->post('angkatan', TRUE);
		// } else {
		// 	$total = '';
		// 	$mhs = $this->input->post('mhs', TRUE);
		// 	$jumlah = count($this->input->post('mhs', TRUE));
		// 	for ($i=0; $i < $jumlah; $i++) { 
		// 		$nim = $this->input->post('mhs['.$i.']', TRUE);
		// 		$total = $total.';'.$nim;
		// 	}
		// 	$data['penugasan'] = $total;
		// }
		// $insert = $this->app_model->insertdata('tbl_penugasan_dosen',$data);
		// if ($insert == TRUE) {
		// 	echo "<script>alert('Berhasil');document.location.href='".base_url()."form/penugasandosen';</script>";
		// } else {
		// 	echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
		// }
		$data['judul_skripsi'] = $this->input->post('judul_skripsi', TRUE);
		$data['pembimbing1'] = $this->input->post('kd_dosen1', TRUE);
		$data['pembimbing2'] = $this->input->post('kd_dosen2', TRUE);
		$data['npm_mahasiswa'] = $this->session->userdata('npm');
		$data['tahunajaran'] = $this->session->userdata('ta');
		$cek = $this->db->query("select * from tbl_skripsi where npm_mahasiswa = '".$this->session->userdata('npm')."' and tahunajaran = '".$this->session->userdata('ta')."'")->result();
		if ($cek == TRUE) {
			$this->db->where('npm_mahasiswa', $this->session->userdata('npm'));
			$this->db->where('tahunajaran', $this->session->userdata('ta'));
			$insert = $this->db->update('tbl_skripsi', $data);
			if ($insert == TRUE) {
				echo "<script>alert('Berhasil');document.location.href='".base_url()."form/penugasandosen/viewdata';</script>";
			} else {
				echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
			}
		} else {
			$insert = $this->app_model->insertdata('tbl_skripsi',$data);
			if ($insert == TRUE) {
				echo "<script>alert('Berhasil');document.location.href='".base_url()."form/penugasandosen/viewdata';</script>";
			} else {
				echo "<script>alert('Gagal Simpan Data');history.go(-1);</script>";
			}
		}
		
		
	}

	function delete($id)
	{
		$delete = $this->app_model->deletedata('tbl_penugasan_dosen','id',$id);
		if ($delete == TRUE) {
			echo "<script>alert('Berhasil');document.location.href='".base_url()."form/penugasandosen';</script>";
		} else {
			echo "<script>alert('Gagal Hapus Data');history.go(-1);</script>";
		}
	}

	function view($id)
	{
		$pecah = explode('zz', $id);
		$logged = $this->session->userdata('sess_login');
		$data['mhs'] = $this->app_model->getpembimbingdsnall($pecah[0],$pecah[1])->result();
		//var_dump($data['mhs']);exit();
		$data['page'] = 'form/pembimbing_detail';
		$this->load->view('template/template',$data);	
	}

	function load_detail($idk)
	{
		$this->session->unset_userdata('npm');
		$data['mhsces'] = $this->db->query("select * from tbl_skripsi where npm_mahasiswa = '".$idk."' and tahunajaran = '".$this->session->userdata('ta')."'")->row();
		$this->session->set_userdata('npm',$idk);
		$this->load->view('form_penugasan_dosen_edit',$data);
	}

	function load_status($idk)
	{
		$this->session->unset_userdata('npm');
		//$data['mhsces'] = $this->db->query("select * from tbl_skripsi where npm_mahasiswa = '".$idk."' and tahunajaran = '".$this->session->userdata('ta')."'")->row();
		$this->session->set_userdata('npm',$idk);
		$this->load->view('form_penugasan_dosen_status');
	}

	function load_status2($idk)
	{
		$this->session->unset_userdata('npm');
		//$data['mhsces'] = $this->db->query("select * from tbl_skripsi where npm_mahasiswa = '".$idk."' and tahunajaran = '".$this->session->userdata('ta')."'")->row();
		$this->session->set_userdata('npm',$idk);
		$this->load->view('form_penugasan_dosen_status2');
	}
}
	