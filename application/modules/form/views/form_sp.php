<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>FORM SEMESTER PENDEK</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#home1">Form Pendaftaran</a></li>
              <!-- <li><a data-toggle="tab" href="#home2">Data Pendaftar</a></li> -->
          </ul>
          <div class="tab-content">
            <div id="home1" class="tab-pane fade in active">
              <b><center>PENDAFTARAN SEMESTER PERBAIKAN</center></b><br>
              <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>auth/mhs_auth_sp">
                <fieldset>
                  <div class="control-group">                     
                    <label class="control-label">NIM</label>
                    <div class="controls">
                      <input type="text" class="span3" name="username" placeholder="NIM" required>
                    </div> <!-- /controls -->       
                  </div> <!-- /control-group -->
                  <div class="control-group">                     
                    <label class="control-label">Password</label>
                    <div class="controls">
                      <input type="password" class="span3" name="password" placeholder="Password" required>
                    </div> <!-- /controls -->       
                  </div> <!-- /control-group -->
                  <div class="form-actions">
                    <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                    <input type="reset" class="btn btn-warning" value="Reset"/>
                  </div> <!-- /form-actions -->
                </fieldset>
              </form>
            </div>
            <div id="home2" class="tab-pane fade in">
              <b><center>DATA PENDAFTAR SEMESTER PERBAIKAN</center></b><br>
                <br>
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                      <tr> 
                        <th>No</th>
                        <th>NPM </th>
                        <th>Nama Mahasiswa</th>
                        <th>Semester</th>
                        <th>Total SKS</th>
                        <th>SKS di Setujui</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; foreach ($mhs as $isi) { 

                        $sms = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$isi->npm_mahasiswa,'NIMHSMSMHS','asc')->row(); 
                        $smtr = $this->app_model->get_semester($sms->SMAWLMSMHS);
                      ?>

                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $isi->npm_mahasiswa; ?></td>
                        <td><?php echo get_nm_mhs($isi->npm_mahasiswa); ?></td>
                        <td><?php echo $smtr; ?></td>
                        <td><?php echo $isi->jumlah_sks; ?></td>
                        <td><?php echo $isi->jml_open; ?></td>
                        <td><a class="btn btn-info btn-small" data-toggle="tooltip" title="Cetak KRS" target="_blank" href="<?php echo base_url(); ?>form/formsp/print_sp/<?php echo $isi->kd_krs ?>" ><i class="btn-icon-only icon-print"></i></a></td>
                      </tr>
                    <?php $no++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
      </div>
    </div>
  </div>
</div>