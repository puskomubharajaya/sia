<?php
    header("Content-Type: application/xls");    
    header("Content-Disposition: attachment; filename=data_status_mhs_keluar.xls");  
    header("Pragma: no-cache"); 
    header("Expires: 0");
?>

<style>
    table, th {
        border: 2px solid black;
    }

    th {
        background-color: blue;
        color: black;
    }
</style>

<table>
    <thead>
        <tr> 
            <th>No</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Angkatan</th>
            <th>Prodi</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($list->result() as $row) { ?>
        <tr>
            <td><?= $no; ?></td>
            <td><?= $row->npm; ?></td>
            <td><?= get_nm_mhs($row->npm); ?></td>
            <td><?= substr($row->npm, 0, 4); ?></td>
            <td><?= get_jur($row->prodi); ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>