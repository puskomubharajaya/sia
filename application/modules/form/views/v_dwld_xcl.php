<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=data_nilai_mhs.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<style>
table, td, th {
    border: 1px solid black;
}

th {
    background-color: blue;
    color: black;
}
</style>

<table border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>NPM</th>
			<th>NAMA</th>
			<th>KODE MATAKULIAH</th>
			<th>PERSEN KEHADIRAN(%)</th>
			<th>TUGAS</th>
			<th>NILAI UTS</th>
			<th>NILAI UAS</th>
			<th>KEHADIRAN DSN</th>
			<th>KEHADIRAN MHS</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach($ping as $row) { ?>
		<tr>
			<td><?php echo number_format($no); ?></td>
			<td><?php echo $row->NIMHSMSMHS; ?></td>
			<td><?php echo $row->NMMHSMSMHS; ?></td>
			<td><?php echo $row->kd_matakuliah; ?></td>
			<?php $absensi = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,2,$row->kd_matakuliah)->row(); ?>
	        <?php $tugas = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,1,$row->kd_matakuliah)->row(); ?>
	        <?php $uts = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,3,$row->kd_matakuliah)->row(); ?>
	        <?php $uas = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,4,$row->kd_matakuliah)->row(); ?>
			<td><?php echo $absensi->nilai; ?></td>
			<td><?php echo $tugas->nilai; ?></td>
			<td><?php echo $uts->nilai; ?></td>
			<td><?php echo $uas->nilai; ?></td>
			<td><?php //$a = ($absensi->nilai)*16/100; echo number_format($a);?></td>
			<td><?php //$a = ($absensi->nilai)*16/100; echo number_format($a);?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>