<div class="row">
    <div class="span12">
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-group"></i>
                <h3>
                    <?= $detail ?>
                </h3>
            </div>

            <div class="widget-content">
                <form action="<?= $is_active_schedule ? base_url('form/nilai/store/' . $id_jadwal) : '' ?>" method="POST">
                    <div class="span11">
                        <a href="<?= base_url('form/nilai/lecturer_list') ?>" class="btn btn-warning pull-left" style="margin-right: 5px">
                            <i class="icon-chevron-left"></i> Kembali
                        </a>

                        <a href="<?= base_url(); ?>form/formnilai/cetak_nilai/<?= $id_jadwal; ?>" target="_blank" style="margin-right: 10px" class="btn btn-success pull-left"><i class="icon-print"></i> Cetak
                        </a>

                        <table id="example8" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>Absen</th>
                                    <th>Rata-rata Tugas</th>
                                    <th>UTS</th>
                                    <th>UAS</th>
                                    <th>Nilai Akhir</th>
                                    <th>Grade</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $now = date('Y-m-d');
                                $is_uts_available = getTipeUjian($id_jadwal, $tahunajar, 1);
                                $is_uas_available = getTipeUjian($id_jadwal, $tahunajar, 2);
                                if (isset($is_uts_available->start_date) || isset($is_uas_available->start_date)) {
                                    if ($now >= $is_uts_available->start_date && $now <= $is_uts_available->end_date) {
                                        $uts = '';
                                        $uas = 'readonly';
                                    } elseif ($now >= $is_uas_available->start_date && $now <= $is_uas_available->end_date) {
                                        $uts = 'readonly';
                                        $uas = '';
                                    } elseif ($is_active_schedule) {
                                        if (!is_null($is_uas_available->end_date) && $now > $is_uas_available->end_date) {
                                            $uts = 'readonly';
                                            $uas = '';
                                        } elseif (!is_null($is_uts_available->end_date) && $now > $is_uts_available->end_date) {
                                            $uts = '';
                                            $uas = 'readonly';
                                        }
                                    } else {
                                        $uts = 'readonly';
                                        $uas = 'readonly';
                                    }
                                } else {
                                    $uts = 'readonly';
                                    $uas = 'readonly';
                                }

                                $no = 1;
                                foreach ($participants as $row) : ?>
                                    <tr>
                                        <td><?= $no ?></td>
                                        <td><?= $row->NIMHSMSMHS ?></td>
                                        <td>
                                            <?= $row->NMMHSMSMHS ?>
                                        </td>
                                        <td>
                                            <?= number_format($row->total_absen, 2) ?>
                                        </td>
                                        <td><?= $row->ratarata ?></td>
                                        <td><?= ($row->uts == 0.00) ? NULL : $row->uts; ?>
                                        </td>
                                        <td><?= ($row->uas == 0.00) ? NULL : $row->uas; ?>
                                        </td>
                                        <td>
                                            <?= (is_null($row->final)) ? '-' : $row->final; ?>
                                        </td>
                                        <td><?= getValueRange($row->final) ?></td>
                                    </tr>
                                <?php $no++;
                                endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>