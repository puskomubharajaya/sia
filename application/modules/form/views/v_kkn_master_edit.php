<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Ubah KKN</h4>
</div>
<form class ='form-horizontal' action="<?php echo base_url();?>form/kkn/edit_master/" method="post">
	<div class="modal-body" style="margin-left: -30px;">    
		<div class="control-group" id="">
			<label class="control-label">Kode KKN</label>
			<div class="controls">
				<input type="text" class="span4" name="kd_kkn" placeholder="Max 3 Karakter" class="form-control" value="<?php echo $kkn->kd_kkn?>" maxlength="3" readonly required/>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Nama KKN</label>
			<div class="controls">
				<input type="text" class="span4" name="nama_kkn" placeholder="Nama KKN" class="form-control" value="<?php echo $kkn->nama_kkn?>" required/>
			</div>
		</div>
		<div class="control-group" id="">
			<label class="control-label">Status</label>
			<div class="controls"> 
				<input name="isactivated" value="1" type="radio" data-toggle="toggle" <?php if($kkn->isactivated == 1){echo 'checked';}?> required> Aktif
				<br>
				<input name="isactivated" value="0" type="radio" data-toggle="toggle" <?php if($kkn->isactivated == 0){echo 'checked';}?> required> Tidak Aktif
			</div>
		</div>
	</div>
	<div class="modal-footer">
	<button type="submit" class="btn btn-success">Simpan</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</form>