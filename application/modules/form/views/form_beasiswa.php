<link href="<?php echo base_url();?>assets/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
<script language="javascript">
	function numeric(e, decimal) { 
	var key;
	var keychar;
	 if (window.event) {
		 key = window.event.keyCode;
	 } else
	 if (e) {
		 key = e.which;
	 } else return true;
	
	keychar = String.fromCharCode(key);
	if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		return true;
	} else 
	if ((("0123456789").indexOf(keychar) > -1)) {
		return true; 
	} else 
	if (decimal && (keychar == ".")) {
		return true; 
	} else return false; 
	}
</script> 
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Formulir Beasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<form class ='form-horizontal' action="<?php echo base_url(); ?>form/beasiswa/save" method="post">
						<div class="row">
						<div class="span5">
						<div class="form-group">
							<label>Nama</label>       
							<input type="text" class="form-control span5" name="nama" value="<?php echo $mhs->NMMHSMSMHS?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>NPM</label>        
							<input type="text" class="form-control span5" name="npm"  value="<?php echo $mhs->NIMHSMSMHS?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>No. NIK/KTP</label>    
							<input type="text" class="form-control span5" onkeypress="return numeric(event, false)" placeholder="No. NIK/KTP" name="ktp" value="<?php echo $bio->ktp?>" <?php if(!empty($bio->ktp)){echo "readonly";}?> maxlength="16" required/>
						</div>
						<br>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<input type="text" class="form-control span5" name="jenkel" value="<?php if($mhs->KDJEKMSMHS == 'P') {echo "Perempuan";}elseif($mhs->KDJEKMSMHS == 'L') {echo "Laki-laki";}else{echo "";}?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>Tempat Lahir</label>        
							<input type="text" class="form-control span5" name="tempat_lahir" value="<?php echo $mhs->TPLHRMSMHS?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>Tanggal Lahir</label>      
							<input type="date" class="form-control span5" name="tanggal_lahir" value="<?php echo $mhs->TGLHRMSMHS?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>Alamat</label>    
							<textarea class="form-control span5" name="alamat" <?php if(!empty($bio->alamat)){echo "readonly";}?> placeholder="Alamat" required><?php echo $bio->alamat?></textarea>
						</div>
						</div>
						<div class="span6">
						<div class="form-group">
							<label>Fakultas</label>    
							<input type="text" class="form-control span5" name="fak" value="<?php echo get_fak_byprodi($mhs->KDPSTMSMHS)?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>Program Studi</label>      
							<input type="text" class="form-control span5" name="prodi" value="<?php echo get_jur($mhs->KDPSTMSMHS)?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>SKS yang telah ditempuh</label>         
							<input type="text" class="form-control span5" name="sks" value="<?php echo $akt->SKSTTTRAKM?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>IPK</label>     
							<input type="text" class="form-control span5" name="ipk" value="<?php echo $akt->NLIPKTRAKM?>" readonly required/>
						</div>
						<br>
						<div class="form-group">
							<label>Nomor telpon rumah / HP</label>      
							<input type="text" class="form-control span5" onkeypress="return numeric(event, false)" placeholder="Nomor telpon rumah/HP" name="tlp" value="<?php echo $bio->no_hp?>" <?php if(!empty($bio->no_hp)){echo "readonly";}?> maxlength="16" required/>
						</div>
						<br>
						<div class="form-group">
							<label>Prestasi</label>    
							<input type="text" class="form-control span5" name="prestasi" data-role="tagsinput" placeholder="Prestasi (pisahkan dengan koma)" required/>
							<br><small style="color:#AAA">Jika lebih dari 1 Pisahkan dengan Koma</small>
						</div>
						<br>
						<div class="form-group">
							<label>Pilihan Beasiswa</label>    
								<select class="form-control span5" name="opt_beasiswa" required>
									<option hidden selected>-- Pilih Beasiswa --</option>
									<?php foreach($jenis AS $row){?>
									<option value="<?php echo $row->kd_beasiswa?>"><?php echo $row->nama_beasiswa?></option>
									<?php } ?>
								</select>
						</div>
						</div>
						<div class="span11">
						<br>
							<div class="thumbnail" style="background-color:#fff585;">
								<b>*Khusus Beasiswa Unggulan kemendikbud hanya bisa diajukan <a target="_BLANK" href="https://buonline.beasiswaunggulan.kemdikbud.go.id">di sini</a></b><br>
								<b>*Dokumen persyaratan asli harap diserahkan ke Biro Kemahasiswaan</b>
							</div>
							<br>
							<div class="form-group">       
								<input type="submit" class="btn btn-primary" value="Ajukan"/>
							</div>
						</div>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap Tags Input Plugin Js -->
    <script src="<?php echo base_url();?>assets/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>