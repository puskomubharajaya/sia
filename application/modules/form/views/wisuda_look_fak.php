<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Wisudawan</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">

            <form action="<?php echo base_url(); ?>form/formwisuda/savefakultas" method="post">
                        <a class="btn btn-success btn-large" target="blank" href="<?php echo base_url(); ?>form/formwisuda/exportdataexcel"><i class="btn-icon-only icon-print"> Print</i></a>
                        <input type="submit" class="btn btn-primary btn-large" value="Submit">
                        <hr>

            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr> 
                        <th width="40">No</th>
                        <th>NPM</th>
                        <th>Nama</th>
                        <th>Jurusan</th>
                        <th width="40">Nilai Akademik</th>
                        <th width="40">Skripsi</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                $logged = $this->session->userdata('sess_login'); $mhsbro = array();
                ?> 

                    <?php $no=1; foreach ($mhs as $value) { 
                        if ($value->kd_fakultas == $logged['userid']) 
                            { $mhsbro[] = $value->NIMHSMSMHS; ?>
                        
                        <tr>
                            <td><?php echo $no; ?> </td>
                            <td><?php echo $value->NIMHSMSMHS; ?></td>
                            <td><?php echo $value->NMMHSMSMHS; ?></td>
                            <td><?php echo $value->prodi; ?></td>
                            <?php //$pecah = explode('n', $value->flag_wisuda); ?>
                            <?php if ($value->flag_wisuda_fak == 2) { ?>
                                <td>
                                    <input type="checkbox" name="fak[]" value="1zzz<?php echo $value->NIMHSMSMHS;?>" checked disabled>
                                </td>
                                <td>
                                    <input type="checkbox" name="fak[]" value="2zzz<?php echo $value->NIMHSMSMHS;?>" checked disabled>
                                </td>

                            <?php } elseif ($value->flag_wisuda_fak == 1) { ?>
                                <td>
                                    <input type="checkbox" name="fak[]" value="1zzz<?php echo $value->NIMHSMSMHS;?>" checked disabled>
                                </td>
                                <td><input type="checkbox" name="fak[]" value="2zzz<?php echo $value->NIMHSMSMHS;?>"></td>

                            <?php } else { ?>
                                <td><input type="checkbox" name="fak[]" value="1zzz<?php echo $value->NIMHSMSMHS;?>"></td>
                                <td><input type="checkbox" name="fak[]" value="2zzz<?php echo $value->NIMHSMSMHS;?>"></td>
                            <?php } ?>
                            
                        </tr>
                        <?php $no++; }} ?>
                </tbody>
            </table>

            </form>

        </div>
      </div>
    </div>
  </div>
</div>
