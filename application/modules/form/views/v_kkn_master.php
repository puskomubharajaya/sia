
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Kelola KKN</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<a class="btn btn-primary" href="#add" data-toggle="modal" title="Tambah KKN"><i class="icon-cogs"></i> Tambah KKN</a>
				<i class="icon-question-sign icon-2x pull-right" style="color:#aaa;" title="Bantuan" id="help"></i>
				<div class="thumbnail pull-right" id="bantuan">
					<i class="icon-sign-blank" style="color:#F08080"></i> Tidak Aktif
				</div>
				<hr>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr> 
								<th>No</th>
								<th>Kode KKN</th>
								<th>Nama KKN</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;
							foreach($master AS $row){ 
							if($row->isactivated == 0){
								$color="background-color:#F08080";
							}else{
								$color="";
							}
							
							?>
							<tr>
								<td style="<?php echo $color?>"><?php echo $no; ?></td>
								<td style="<?php echo $color?>"><?php echo $row->kd_kkn ?></td>
								<td style="<?php echo $color?>"><?php echo $row->nama_kkn ?></td>
								<td style="<?php echo $color?>"><?php echo getStatusMasterbea($row->isactivated) ?></td>
								<td style="<?php echo $color?>">
									<a class="btn btn-success" onclick="edit(<?php echo $row->id_master_kkn ?>)" href="#editModal" data-toggle="modal" title="Ubah"><i class="btn-icon-only icon-edit"></i></a>
									<a class="btn btn-danger" onclick="return confirmhapus();" href="<?php echo base_url();?>form/kkn/hapus_kkn/<?php echo $row->id_master_kkn; ?>" data-toggle="tooltip" title="Hapus"><i class="btn-icon-only icon-trash"></i></a>
								</td>
							</tr>
							<?php $no++;} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="ubah">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah KKN</h4>
            </div>
            <form id="kkn" class ='form-horizontal' action="<?php echo base_url();?>form/kkn/add_master/" method="post">
                <div class="modal-body" style="margin-left: -30px;">    
                    <div class="control-group" id="">
                        <label class="control-label">Kode KKN</label>
                        <div class="controls">
                            <input type="text" class="span4" id="kd_kkn" name="kd_kkn" placeholder="Max 3 Karakter" class="form-control" maxlength="3" required/><div id="msg"></div>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Nama KKN</label>
                        <div class="controls">
                            <input type="text" class="span4" name="nama_kkn" placeholder="Nama KKN" class="form-control" required/>
                        </div>
                    </div>
                    <div class="control-group" id="">
                        <label class="control-label">Status</label>
                        <div class="controls">
                            <input name="isactivated" value="1" type="radio" data-toggle="toggle" required> Aktif
							<br>
                            <input name="isactivated" value="0" type="radio" data-toggle="toggle" required> Tidak Aktif
                        </div>
                    </div>
                </div> 
                <div class="modal-footer">
				<button type="submit" id="save" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
$(document).ready(function() {
		$("#bantuan").hide();
		$('#help').css('cursor', 'pointer');
			$("#help").click(function() {
					$("#bantuan").toggle();
			});
	});
</script>
<script>
function confirmhapus() {
  var x=confirm("Yakin menghapus data?")
  if (x) {
	return true;
  } else {
	return false;
  } 
}
function edit(id) {
        $("#ubah").load('<?php echo base_url()?>form/kkn/edit_kkn/'+id);
    }
	
	
$(document).ready(function() {
	$("#kd_kkn").on("blur", function(e) {
		$('#msg').hide();
		if ($('#kd_kkn').val() == null || $('#kd_kkn').val() == "") {
			$('#msg').show();
			$("#msg").html("Kode KKN harus diisi").css("color", "red");
		} else {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>/form/kkn/cari/",
				data: $('#kkn').serialize(),
				dataType: "HTML",
				cache: false,
				success: function(msg) { 
					$('#msg').show();
					$("#msg").html(msg);
				}, 
				error: function(jqXHR, textStatus, errorThrown) {
					$('#msg').show();
					$("#msg").html(textStatus + " " + errorThrown);
				}
			});
		}
	});
});
</script>