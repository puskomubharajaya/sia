<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-calendar"></i>
        <h3>Pilih Tahun Wisuda</h3>
      </div> <!-- /widget-header -->
      <div class="widget-content">
        <div class="span11">
          <form method="post" class="form-horizontal" action="<?= base_url(); ?>form/formwisuda/savesess">
            <fieldset>
              <div id="tahun" class="control-group">
                <label class="control-label">Tahun</label>
                <div class="controls">
                  <select class="form-control span6" name="tahun" id="angk" required>
                    <?php $last = date('Y')-5; ?>
                    <?php for ($i=$last; $i <= date('Y'); $i++) { ?>
                      <option value="<?= $i ?>"><?= $i ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>    
              <br>
              <div class="form-actions">
                  <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>