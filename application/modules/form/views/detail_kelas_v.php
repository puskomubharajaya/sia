<div class="row">
    <div class="span12">
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-group"></i>
                <h3>
                    <?= (!in_array(6, $usergroup) || !in_array(7, $usergroup)) ? nama_dsn($dosen) . ' - ' : ''; ?>
                    <?= $detail ?>
                </h3>
            </div>

            <!-- if user is not lecturer disabled all -->
            <?php if (!in_array(6, $usergroup) || !in_array(7, $usergroup)) {
                $disabled_all = 'disabled=""';
            } ?>

            <div class="widget-content">
                <?php if ($this->session->flashdata('fail_storing')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?= $this->session->flashdata('fail_storing'); ?>
                    </div>
                <?php } ?>

                <?php $is_open_access = $this->db->get_where('open_uts',['id_jadwal' => $id_jadwal, 'is_active' => 1 ]); ?>

                <form 
                    action="<?= base_url('form/nilai/store/' . $id_jadwal)  ?>" 
                    method="POST">
                    <div class="span11">
                        <?php if (in_array(6, $usergroup) || in_array(7, $usergroup)) {
                            $path = base_url('form/nilai/courses_list');
                        } else {
                            $path = base_url('form/nilai/courses_list/' . $dosen);
                        } ?>
                        <a href="<?= $path ?>" class="btn btn-warning pull-left" style="margin-right: 10px">
                            <i class="icon-chevron-left"></i> Kembali
                        </a>
                        <?php 
                            if ($is_open_access->num_rows() > 0)  : ?>
                                <button type="submit" class="btn btn-success pull-left" style="margin-right: 10px" >
                                    <i class="icon-save"></i> Simpan
                                </button>
                                <?php $disabled_all = 'readonly=""'; ?>

                            <?php elseif ($is_active_schedule && (in_array(6, $usergroup) OR in_array(7, $usergroup))) : ?>
                                <button type="submit" class="btn btn-success pull-left" style="margin-right: 10px" >
                                    <i class="icon-save"></i> Simpan
                                </button>
                                <?php $disabled_all = ''; ?>

                            <?php else : $disabled_all = 'readonly=""'; endif; ?>

                        <a 
                            href="<?= base_url(); ?>form/formnilai/cetak_nilai/<?= $id_jadwal; ?>" 
                            target="_blank" 
                            style="margin-right: 10px" 
                            class="btn btn-primary pull-left"><i class="icon-print"></i> Cetak 
                        </a>

                        <button 
                            type="button" 
                            data-toggle="modal" 
                            data-target="#info" 
                            class="btn btn-warning pull-left">
                            <i class="icon-pushpin"></i> Petunjuk
                        </button>

                        <table id="example8" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NPM</th>
                                    <th>Nama</th>
                                    <th>Absen</th>
                                    <th>Tugas I</th>
                                    <th>Tugas II</th>
                                    <th>Tugas III</th>
                                    <th>Tugas IV</th>
                                    <th>Tugas V</th>
                                    <th>Rata-rata Tugas</th>
                                    <th>UTS</th>
                                    <th>UAS</th>
                                    <th>Nilai Akhir</th>
                                    <th>Grade</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $now = date('Y-m-d');
                                $is_uts_available = getTipeUjian($id_jadwal, $tahunajar, 1);
                                $is_uas_available = getTipeUjian($id_jadwal, $tahunajar, 2);
                                $remove_disabled = false;
                                if (isset($is_uts_available->start_date) OR isset($is_uas_available->start_date)) {
                                    if ($is_open_access->num_rows() > 0) {
                                        if ($is_open_access->row()->type == 1) {
                                            $remove_disabled = true;
                                            $uts = '';
                                            $uas = 'readonly';   
                                        } elseif ($is_open_access->row()->type == 2) {
                                            $remove_disabled = true;
                                            $uts = 'readonly';
                                            $uas = '';   
                                        } elseif ($is_open_access->row()->type == 3) {
                                            $remove_disabled = true;
                                            $uts = '';
                                            $uas = '';   
                                        }
                                    } elseif ($now >= $is_uts_available->start_date && $now <= $is_uts_available->end_date) {
                                        $uts = '';
                                        $uas = 'readonly';
                                    } elseif ($now >= $is_uas_available->start_date && $now <= $is_uas_available->end_date) {
                                        $uts = 'readonly';
                                        $uas = '';
                                    } elseif ($is_active_schedule) {
                                        $remove_disabled = true;
                                        $uts = '';
                                        $uas = '';
                                    } else {
                                        $uts = 'readonly';
                                        $uas = 'readonly';
                                    }
                                } else {
                                    $uts = 'readonly';
                                    $uas = 'readonly';
                                }

                                $no = 1;
                                foreach ($participants as $row) : ?>
                                    <tr>
                                        <td><?= $no ?></td>
                                        <td><?= $row->NIMHSMSMHS ?></td>
                                        <td>
                                            <?= $row->NMMHSMSMHS ?>
                                            <input type="hidden" name="npm[<?= $row->NIMHSMSMHS ?>]" value="<?= $row->NIMHSMSMHS ?>" />
                                        </td>
                                        <td id="absen-<?= $row->NIMHSMSMHS ?>">
                                            <?= number_format($row->total_absen, 2) ?>
                                            <input type="hidden" name="absen[<?= $row->NIMHSMSMHS ?>]" value="<?= number_format($row->total_absen, 2) ?>" />
                                        </td>
                                        <td id="task1-<?= $row->NIMHSMSMHS ?>">
                                            <input value="<?= (is_null($row->tugas1)) ? NULL : $row->tugas1; ?>" maxlength="5" placeholder="tugas 1" name="tugas1[<?= $row->NIMHSMSMHS ?>]" type="text" class="task1-<?= $row->NIMHSMSMHS ?> span1" oninput="isValid(this)" <?= $remove_disabled ? '' : $disabled_all; ?> />
                                        </td>
                                        <td id="task2-<?= $row->NIMHSMSMHS ?>">
                                            <input value="<?= (is_null($row->tugas2)) ? NULL : $row->tugas2; ?>" maxlength="5" placeholder="tugas 2" name="tugas2[<?= $row->NIMHSMSMHS ?>]" type="text" class="task2-<?= $row->NIMHSMSMHS ?> span1" oninput="isValid(this)" <?= $remove_disabled ? '' : $disabled_all; ?> />
                                        </td>
                                        <td id="task3-<?= $row->NIMHSMSMHS ?>">
                                            <input value="<?= (is_null($row->tugas3)) ? NULL : $row->tugas3; ?>" maxlength="5" placeholder="tugas 3" name="tugas3[<?= $row->NIMHSMSMHS ?>]" type="text" class="task3-<?= $row->NIMHSMSMHS ?> span1" oninput="isValid(this)" <?= $remove_disabled ? '' : $disabled_all; ?> />
                                        </td>
                                        <td id="task4-<?= $row->NIMHSMSMHS ?>">
                                            <input value="<?= (is_null($row->tugas4)) ? NULL : $row->tugas4; ?>" maxlength="5" placeholder="tugas 4" name="tugas4[<?= $row->NIMHSMSMHS ?>]" type="text" class="task4-<?= $row->NIMHSMSMHS ?> span1" oninput="isValid(this)" <?= $remove_disabled ? '' : $disabled_all; ?> />
                                        </td>
                                        <td id="task5-<?= $row->NIMHSMSMHS ?>">
                                            <input value="<?= (is_null($row->tugas5)) ? NULL : $row->tugas5; ?>" maxlength="5" placeholder="tugas 5" name="tugas5[<?= $row->NIMHSMSMHS ?>]" type="text" class="task5-<?= $row->NIMHSMSMHS ?> span1" oninput="isValid(this)" <?= $remove_disabled ? '' : $disabled_all; ?> />
                                        </td>
                                        <td><?= $row->ratarata ?></td>
                                        <td id="uts-<?= $row->NIMHSMSMHS ?>">
                                            <input 
                                                value="<?= $row->uts; ?>" 
                                                maxlength="5" 
                                                placeholder="UTS" 
                                                name="uts[<?= $row->NIMHSMSMHS ?>]" 
                                                type="text" 
                                                class="uts<?= $row->NIMHSMSMHS ?> span1" 
                                                oninput="isValid(this)" 
                                                <?= $uts ?> 
                                                <?= isset($remove_disabled) ? '' : $disabled_all ?> />
                                        </td>
                                        <td id="uas-<?= $row->NIMHSMSMHS ?>">
                                            <input 
                                                value="<?= $row->uas; ?>" 
                                                maxlength="5" 
                                                placeholder="UAS" 
                                                name="uas[<?= $row->NIMHSMSMHS ?>]" 
                                                type="text" 
                                                class="uas<?= $row->NIMHSMSMHS ?> span1" 
                                                oninput="isValid(this)" 
                                                <?= $uas ?> 
                                                <?= isset($remove_disabled) ? '' : $disabled_all ?> />
                                        </td>
                                        <td id="finalpoin-<?= $row->NIMHSMSMHS ?>">
                                            <?= (is_null($row->final)) ? '-' : $row->final; ?>
                                        </td>
                                        <td id="grade-<?= $row->NIMHSMSMHS ?>"><?= getValueRange($row->final) ?></td>
                                        <!-- matakuliah -->
                                        <input type="hidden" name="kode_matakuliah" value="<?= $matakuliah ?>">
                                    </tr>
                                <?php $no++;
                                endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>

<div id="info" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Petunjuk</h4>
            </div>
            <div class="modal-body">
                <p>Pengisian nilai untuk satu matakuliah dapat dilakukan berkala, tidak harus sekaligus. Dengan catatan Anda telah menekan tombol <b><u>simpan</u></b> terlebih dahulu. Kemudian Anda dapat melanjutkan penilaian yang belum diselesaikan.</p>
                <p>Ada beberapa kemungkinan yang menyebabkan Anda tidak dapat melakukan penyimpanan data maupun melakukan penilaian, diantaranya:</p>
                <ul>
                    <li>Masa ujian belum ditentukan oleh prodi</li>
                    <li>Masa ujian telah melewati tenggat waktu</li>
                    <li>Masa ujian berada di tahun akademik lampau</li>
                </ul>
                <p>Jika Anda menemukan kendala pada poin pertama dan kedua Anda dapat menghubungi pihak prodi terkait.</p>
                <p>
                    * <b><u>PERHATIKAN</u></b> penggunaan kolom <i>SEARCH</i> ketika anda menyimpan data. Data yang tersimpan hanya data yang ditampilkan pada monitor. Mekanisme penyimpanan menggunakan metode <i>replace</i> sehingga data lama akan diganti oleh data terbaru. Sehingga nilai mahasiswa yang tidak muncul pada <i>screen</i> akan dianggap <b><u>0 (nol)</u></b>. Oleh karena itu pastikan kolom <i>SEARCH</i> dalam keadaan kosong ketika Anda menyimpan data.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    // input validation
    var RegEx = new RegExp(/^\d*\.?\d*$/);

    function isValid(elem) {
        if (!RegEx.test(elem.value)) {
            elem.value = '';
        }
        if (elem.value.substr(0, 1) == '.') {
            elem.value = '';
        }
    }
</script>