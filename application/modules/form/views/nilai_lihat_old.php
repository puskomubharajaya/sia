<?php// echo $id_jadwal;die(); ?>

<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>data/divisi/view_edit/'+id);
}
</script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<?php 
  				$mk=$this->app_model->get_mk_by_jadwal($jdl_mk)->row();
  				 ?>
  				<h3>Data Nilai Mahasiswa ( '<?php echo $mk->nama_matakuliah ?>' ) (2015-2016 - Ganjil)</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
					<a href="<?php echo base_url(); ?>form/formnilai/authdosen" class="btn btn-warning"> << Kembali </a>
					<?php //if ($lock->flag_publikasi == 2) { ?>
						<!-- <input type="hidden"> -->
					<?php //} else { ?>
					<a data-toggle="modal" href="#myModal" class="btn btn-primary"> Upload Nilai </a>
					<a href="<?php echo base_url(); ?>form/formnilai/dwld_excel/<?php echo $id_jadwal; ?>" class="btn btn-primary"> Download Form </a>
					<?php //} ?>
					<a href="<?php echo base_url(); ?>form/formnilai/cetak_nilai/<?php echo $id_jadwal; ?>" target="_blank" class="btn btn-success"> Cetak </a>
					
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th width="50">No</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th width="80">Absensi (10%)</th>
                                <th width="80">Tugas (20%)</th>
                                <th width="80">UTS (30%)</th>
                                <th width="80">UAS (40%)</th>
	                            <th width="40">Angka</th>
	                            <th width="40">Huruf</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach($look as $row){?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<td><?php echo $row->NIMHSMSMHS; ?></td>
	                        	<td><?php echo $row->NMMHSMSMHS; ?></td>
	                        	<?php $absensi = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,2,$row->kd_matakuliah)->row(); ?>
	                        	<?php $tugas = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,1,$row->kd_matakuliah)->row(); ?>
	                        	<?php $uts = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,3,$row->kd_matakuliah)->row(); ?>
	                        	<?php $uas = $this->app_model->getnilai($row->kd_jadwal,$row->NIMHSMSMHS,4,$row->kd_matakuliah)->row(); ?>

	                        	<?php $rt = ($absensi->nilai*0.1)+($tugas->nilai*0.2)+($uts->nilai*0.3)+($uas->nilai*0.4);

	                        		$logged = $this->session->userdata('sess_login');
									if (($logged['userid'] == '61101') or ($logged['userid'] == '74101')) {
										$rtnew = number_format($rt,2);
										if (($rtnew >= 80) and ($rtnew <= 100)) {
											$rw = "A";
										} elseif (($rtnew >= 75) and ($rtnew <= 79.99)) {
											$rw = "A-";
										} elseif (($rtnew >= 70) and ($rtnew <= 74.99)) {
											$rw = "B+";
										} elseif (($rtnew >= 65) and ($rtnew <= 69.99)) {
											$rw = "B";
										} elseif (($rtnew >= 60) and ($rtnew <= 64.99)) {
											$rw = "B-";
										} elseif (($rtnew >= 55) and ($rtnew <= 59.99)) {
											$rw = "C";
										} elseif (($rtnew >= 40) and ($rtnew <= 54.99)) {
											$rw = "D";
										} elseif (($rtnew >= 0) and ($rtnew <= 39.99)) {
											$rw = "E";
										} elseif ($rtnew >100) {
											$rw = "T";
										}
									} else {
										$rtnew = number_format($rt,2);
										if (($rtnew >= 80) and ($rtnew <= 100)) {
											$rw = "A";
										} elseif (($rtnew >= 65) and ($rtnew <= 79.99)) {
											$rw = "B";
										} elseif (($rtnew >= 55) and ($rtnew <= 64.99)) {
											$rw = "C";
										} elseif (($rtnew >= 45) and ($rtnew <= 54.99)) {
											$rw = "D";
										} elseif (($rtnew >= 0) and ($rtnew <= 44.99)) {
											$rw = "E";
										}  elseif ($rtnew >100) {
											$rw = "T";
										}
									}
	                        	?>

                                <td><?php if($absensi->nilai > 100){ echo "-"; } else { echo $absensi->nilai; } ?></td>
                                <td><?php if($tugas->nilai > 100){ echo "-"; } else { echo $tugas->nilai; } ?></td>
                                <td><?php if($uts->nilai > 100){ echo "-"; } else { echo $uts->nilai; } ?></td>
                                <td><?php if($uas->nilai > 100){ echo "-"; } else { echo $uas->nilai; } ?></td>
	                        	<td><?php if($rt > 100){ echo "-"; } else { echo $rt; } ?></td>
	                        	<td><?php echo $rw; ?></td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">FORM UPLOAD NILAI</h4>
            </div>
            <form class ='form-horizontal' action="<?php echo base_url(); ?>form/formnilai/upload_excel" method="post" enctype="multipart/form-data">
	            <div class="modal-body">
	            	<!--div class="control-group" id="">
		                <label class="control-label">INPUT NILAI</label>
		                <div class="controls">
		                    <select name="tipe" required>
		                    	<option disabled selected>PILIH TIPE</option>
		                    	<option value="3">NILAI UTS</option>
		                    	<option value="4">NILAI UAS</option>
		                    	<option value="2">NILAI ABSEN</option>
		                    	<option value="1">NILAI TUGAS</option>
		                    </select>
		                </div>
		            </div-->
		            <div class="control-group" id="">
		                <label class="control-label">Upload Data Nilai (.xls) </label>
		                <div class="controls">
		                    <input type="file" class="span4 form-control" name="userfile" required/>
		                    <input type="hidden" name="id_jadwal" value="<?php echo $id ?>" />
		                    <input type="hidden" name="kode_jdl" value="<?php echo $ding->kd_jadwal; ?>">
		                    <!-- <input type="hidden" name="kd_trans" value="<?php echo $ding->kd_dosen.'/'.$ding->kd_matakuliah.$ding->NIMHSMSMHS; ?>"> -->
		                </div>
		            </div>
	            </div>
	            <div class="modal-footer">
	              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
	              <input type="submit" class="btn btn-primary" value="Submit"/>
	          	</div>
        	</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->