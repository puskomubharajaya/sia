<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>Data Wisudawan</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">

            <form action="<?php echo base_url(); ?>form/formwisuda/saverenkeu" method="post">
                        <a class="btn btn-success btn-large" target="blank" href="<?php echo base_url(); ?>form/formwisuda/exportdataexcel"><i class="btn-icon-only icon-print"> Print</i></a>
                        <input type="submit" class="btn btn-primary btn-large" value="Submit">
                        <hr>

            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr> 
                        <th width="40">No</th>
                        <th>NPM</th>
                        <th>Nama</th>
                        <th>Jurusan</th>
                        <th>Fakultas</th>
                        <th width="80">Iuran Mahasiswa</th>
                        <th width="80">Skripsi & WIsuda</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; foreach ($mhs as $value) { ?>
                        <tr>
                            <td><?php echo $no; ?> </td>
                            <td><?php echo $value->NIMHSMSMHS; ?></td>
                            <td><?php echo $value->NMMHSMSMHS; ?></td>
                            <td><?php echo $value->prodi; ?></td>
                            <td><?php echo $value->fakultas; ?></td>
                            <?php if ($value->flag_wisuda_renkeu == 2) { ?>
                                <td><input type="checkbox" name="renkeu[]" value="1zzz<?php echo $value->NIMHSMSMHS;?>" checked disabled></td>
                                <td><input type="checkbox" name="renkeu[]" value="2zzz<?php echo $value->NIMHSMSMHS;?>" checked disabled></td>
                            <?php } elseif ($value->flag_wisuda_renkeu == 1) { ?>
                                <td><input type="checkbox" name="renkeu[]" value="1zzz<?php echo $value->NIMHSMSMHS;?>" checked disabled></td>
                                <td><input type="checkbox" name="renkeu[]" value="2zzz<?php echo $value->NIMHSMSMHS;?>"></td>
                            <?php } else { ?>
                                <td><input type="checkbox" name="renkeu[]" value="1zzz<?php echo $value->NIMHSMSMHS;?>"></td>
                                <td><input type="checkbox" name="renkeu[]" value="2zzz<?php echo $value->NIMHSMSMHS;?>"></td>
                            <?php } ?>
                            <?php /*
                                $sess = $this->session->userdata('sess_login');
                                $d = $sess['id_user_group'];
                                if ($d == 11) { */ ?>
                                    <!--td class="td-actions">
                                        <input type="checkbox" value="1" name="fak" disabled/>
                                    </td>
                                    <td class="td-actions">
                                        <input type="checkbox" value="2" name="renkeu">
                                    </td>
                                    <td class="td-actions">
                                        <input type="checkbox" value="3" name="baa" disabled/>
                                    </td>
                                <?php //} elseif ($d == 10) { ?>
                                    <td class="td-actions">
                                        <input type="checkbox" value="1" name="fak"  disabled/>
                                    </td>
                                    <td class="td-actions">
                                        <input type="checkbox" value="2" name="renkeu" disabled/>
                                    </td>
                                    <td class="td-actions">
                                        <input type="checkbox" value="3" name="baa">
                                    </td>
                                <?php //} elseif ($d == 9) { ?>
                                     <td class="td-actions">
                                        <input type="checkbox" value="1" name="fak">
                                    </td>
                                    <td class="td-actions">
                                        <input type="checkbox" value="2" name="renkeu" disabled/>
                                    </td>
                                    <td class="td-actions">
                                        <input type="checkbox" value="3" name="baa" disabled/>
                                    </td-->
                                <?php //} ?>
                            
                        </tr>
                        <?php $no++; }?>
                </tbody>
            </table>

            </form>

        </div>
      </div>
    </div>
  </div>
</div>
