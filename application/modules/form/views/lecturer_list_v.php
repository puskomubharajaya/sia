<div class="row">
    <div class="span12">
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-list"></i>
                <h3>Daftar Dosen Mengajar Prodi <?= get_jur($prodi) ?> Tahun Akademik <?= get_thnajar($tahunajar) ?></h3>
            </div>

            <div class="widget-content">
                <div class="span11">
                    <a href="<?= base_url('form/nilai') ?>" class="btn btn-warning">
                        <i class="icon-chevron-left"></i> Kembali
                    </a>
                    <hr>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Matakuliah</th>
                                <th>Nama Matakuliah</th>
                                <th>Dosen Pengajar</th>
                                <th>Kelas</th>
                                <th width='40'>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($lecturer as $lecturers) : ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $lecturers->kd_matakuliah ?></td>
                                    <td><?= $lecturers->nama_matakuliah ?></td>
                                    <td><?= $lecturers->nama ?></td>
                                    <td><?= $lecturers->kelas ?></td>
                                    <td>
                                        <?php if ($lecturers->mk_ta == 1) { ?>
                                            <a href="<?= base_url('form/nilai/participants_ta/' . $lecturers->id_jadwal) ?>" class="btn btn-primary">
                                                <i class="icon-eye-open"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a href="<?= base_url('form/nilai/participants_nilai_prodi/' . $lecturers->id_jadwal) ?>" class="btn btn-primary">
                                                <i class="icon-eye-open"></i>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php $no++;
                            endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<br>