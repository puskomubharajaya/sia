<?php
//var_dump($cetak);die();
ob_start();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetMargins(3, 5 ,0);

$pdf->SetFont('Arial','',11); 

//$pdf->image('http://172.16.1.5:801/assets/logo.gif',10,10,20);

if ($row->tipe == 'P') {
	$titl = 'Pindah';
} else {
	$titl = 'Pengunduran Diri';
} 


//$pdf->Ln(0);
$pdf->Cell(35,5,'',0,0,'C');
$pdf->Cell(20,5,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,1,'C');
$pdf->Cell(40,5,'',0,0,'C');
$pdf->Cell(20,5,'BIRO ADMINISTRASI AKADEMIK',0,1,'C');
$pdf->Cell(20,5,'',0,0,'C');
$pdf->Cell(60,0,'',1,1,'C');

$pdf->ln(5);
$pdf->SetFont('Arial','B',13); 
$pdf->Cell(200,5,'FORMULIR PENGAJUAN '.strtoupper($titl).' UNIVERSITAS',0,1,'C');

$pdf->ln(5);
$pdf->SetFont('Arial','',11); 
$pdf->Cell(130,5,'',0,0);
$pdf->Cell(40,5,'Kepada Yth,',0,0);

$pdf->ln(7); 
$pdf->Cell(130,5,'',0,0);
$pdf->Cell(31,5,'Dekan Fakultas '.get_fak_byprodi($row->prodi),0,0);
$pdf->Cell(10,10,'',0,0);

$pdf->ln(7);
$pdf->Cell(130,5,'',0,0);
$pdf->Cell(40,5,'Di Jakarta',0,0);


$pdf->ln(5);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(5,10,'',0,0);

$pdf->Cell(40,10,'Dengan hormat,',0,1);

// $pdf->Cell(10,10,':',0,'C');

// $pdf->Cell(60,10,0,0,'L');


$pdf->ln(1);

$pdf->SetFont('Arial','',12); 

$pdf->Cell(5,10,'',0,0);

$pdf->Cell(53,0,'Yang bertanda dibawah ini',0,0);

$pdf->Cell(5,0,':',0,0);


$pdf->ln(5);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(65,5,'Nama',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(10,5,get_nm_mhs($row->npm),0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(65,5,'NPM / NIM',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(10,5,$row->npm,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(65,5,'Fakultas / Prodi',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(5,5,get_fak_byprodi($row->prodi).' / '.get_jur($row->prodi),0,1,'L');

switch ($row->kelas) {
	case 'PG':
		$kelas = 'A';
		break;

	case 'SR':
		$kelas = 'B';
		break;
		
	case 'KY':
		$kelas = 'C';
		break;
}

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(65,5,'Kelas',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(27,5,$kelas,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(65,5,'Jumlah SKS yang ditempuh',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(27,5,$row->sks,0,1);

$pdf->ln(1);
$pdf->SetFont('Arial','','12');
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(65,5,'No.Telp / HP',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->Cell(10,5,$row->tlp,0,1);
//$pdf->Cell(60,10,$q->nama,0,0,'L')MultiCell(60, 10, $q->nama, 0,'L');
// $pdf->MultiCell(50, 5,0,'L');
$pdf->ln(1);
$pdf->SetFont('Arial','',11);
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(65,5,'Alamat',0,0);
$pdf->Cell(5,5,':',0,'L');
$pdf->MultiCell(120,5,trim($row->alamat),0,'L');

if ($row->tahunakademik%2 == 0) {
	$smt = 'Genap';
} else {
	$smt = 'Ganjil';
}

// destinasi
if ($row->tipe = 'P') {
	$dest = ' ke '.$row->destinasi;
} else {
	$dest = '';
}

$pdf->ln(3);
$pdf->Cell(5,5,'',0,0);
$pdf->MultiCell(0,5,'Dengan ini atas kemauan sendiri, saya mengajukan pindah/transfer kuliah dari Universitas Bhayangkara Jakarta Raya'.$dest.' dengan alasan '.$row->keterangan.' pada semester : '.$smt.' Tahun Akademik : '.get_thajar($row->tahunakademik).'',0,'L');

$pdf->ln(2);
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(5,5,'Terlampir:',0,0);

$pdf->ln(5);
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(5,5,'1. KHS Semester 1 sampai dengan semester '.$semst.'',0,0);

$pdf->ln(5);
$pdf->Cell(5,5,'',0,0);
$pdf->Cell(5,5,'2. Validasi Biro Perencanaan Anggaran dan Keuangan (BPAK)',0,1);

$pdf->ln(2);
$pdf->Cell(5,5,'',0,0);
$pdf->MultiCell(190,5,'Selanjutnya saya bersedia menyelesaikan administrasi yang ditentukan dan prosedur sebagaimana mestinya. ',0,'L');

$pdf->ln(1);
$pdf->Cell(5,10,'',0,0);
$pdf->Cell(10,10,'Demikian permohonan ini saya buat untuk dapat diperhatikan.',0,1);


$pdf->ln(2);
$pdf->SetMargins(3, 5 ,0);
$pdf->SetFont('Arial','',11);
$pdf->Cell(5,10,'',0,0);
date_default_timezone_set('Asia/Jakarta'); 
$pdf->Cell(165,5,'Bekasi, '.date('d-m-Y').'',0,0,'R');

$pdf->ln();
$pdf->SetFont('Arial','',11);
$pdf->Cell(100,7,'Mengetahui,',0,0,'C');
$pdf->Cell(100,7,'',0,0,'L');

$pdf->ln();
$pdf->Cell(100,5,'Penasehat Akademik',0,0,'C');
$pdf->Cell(100,5,'Mahasiswa ybs,',0,0,'C');

$pdf->ln(20);
$pdf->Cell(100,5,''.get_nm_pa($row->pa).'',0,0,'C');
$pdf->Cell(100,5,''.get_nm_mhs($row->npm).'',0,0,'C');

$pdf->ln(5);
$pdf->Cell(100,5,''.$row->pa.'',0,0,'C');
$pdf->Cell(100,5,''.$row->npm.'',0,0,'C');

$pdf->Ln(10);
$pdf->Cell(200,7,'Menyetujui,',0,0,'C');

$pdf->Ln();
$pdf->Cell(200,7,'Kaprodi',0,1,'C');
$pdf->Ln(10);
$pdf->Cell(200,7,'(........................................................)',0,0,'C');

$pdf->SetFont('Arial','',9);
$pdf->Ln(10);
$pdf->Cell(143,5,'Tembusan yth :',0,0,'L');
$pdf->SetFont('Arial','',11);
// $pdf->Cell(80,5,'Mengeluarkan,',0,0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Ln(4);
$pdf->Cell(148,5,'1. Fakultas (Copy Arsip)',0,0,'L');
$pdf->SetFont('Arial','',11);
// $pdf->Cell(80,5,'Ka.BAA',0,0,'L');
$pdf->SetFont('Arial','',9);

$pdf->Ln(4);
$pdf->Cell(130,5,'2. Biro Administrasi Akademik (Asli)',0,0,'L');
$pdf->Cell(80,5,'',0,0,'L');

$pdf->Ln(4);
$pdf->Cell(130,5,'3. Biro Perencanaan Anggaran dan Keuangan (Copy Arsip)',0,0,'L');
$pdf->Cell(80,5,'',0,0,'L');

// $pdf->Ln(4);
// $pdf->Cell(130,5,'4. UPT IT & Komputer',0,0,'L');
// $pdf->Cell(80,5,'',0,0,'L');

// $pdf->ln(10);
// $pdf->SetFont('Arial','U',12);
// $pdf->Cell(17,5,'',0,0,'C');
// $pdf->Cell(100,5,'',0,0,'L');
// $pdf->Cell(80,5,'Rouly G Ratna Silalahi,ST.,MM',0,0,'C');
// $pdf->ln(5);
// $pdf->SetFont('Arial','',8);
// date_default_timezone_set('Asia/Jakarta'); 
// $pdf->Cell(117,5,'Print  : '.date('d-m-Y H:i').'',0,0,'L');
// $pdf->SetFont('Arial','U',12);
// $pdf->Cell(80,5,'NIP: 0607108',0,0,'C');





//exit();
$pdf->Output();
ob_end_flush();
?>