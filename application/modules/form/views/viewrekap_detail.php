<script type="text/javascript">
$(document).ready(function(){
	var table = $('#tabel_krs');
	
	var oTable = table.dataTable({
		"bLengthChange": false,
        "bFilter": false, 
		"bInfo": false,
		"bPaginate": false,
		"bSort": false,
	});	
});
</script>
<div class="row">
  <!-- /span6 -->
  <div class="span12">
     <div class="widget widget-nopad">
      <div class="widget-header"> <i class="icon-edit"></i>
        <h3>KRS Semester <?php echo $this->uri->segment(4); ?></h3>
      </div>
      <!-- /widget-header -->
      <div class="widget-content" style="padding:30px;">
				<a href="<?php echo base_url(); ?>form/formkrs/viewform#data_peserta" class="btn btn-warning"> << Kembali </a>
				<table id="tabel_krs" class="table table-bordered table-striped">
                    <thead>
                          <tr> 
                            <th>Kode MK</th>
                            <th>Mata Kuliah</th>
                            <th>SKS</th>
                            <th>Nilai</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; foreach ($detail_krs as $row) { ?>
                          <tr>
                            <td><?php echo $row->kd_matakuliah;?></td>
                            <td><?php echo $row->nama_matakuliah;?></td>
                            <td><?php echo $row->sks_matakuliah; ?></td>
                            <td><?php echo $row->NLAKHTRLNM; ?></td>
                          </tr>
                          <?php $no++; } ?>
                      </tbody>
                  </table>
			</div>
          </div> <!-- /widget-content -->
	</div>  
</div>