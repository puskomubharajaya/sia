<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3>FORM PENGISIAN NILAI PERBAIKAN/KHUSUS</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <b><center>FORM PENGISIAN NILAI PERBAIKAN/KHUSUS</center></b><br>
          <form id="edit-profile" class="form-horizontal" method="post" action="<?= base_url('form/formnilaisp/sess'); ?>">
            <fieldset>
              <div class="control-group">                     
                <label class="control-label">Tahun Akademik</label>
                <div class="controls">
                  <select name="tahun" class="span3" required>
                    <option disabled selected>-- Pilih Tahun Akademik --</option>
                    <option value="20164">2016/2017 (Genap) - Kelas Perbaikan</option>
                    <option value="20173">2017/2018 (Ganjil) - Kelas Perbaikan</option>
                    <option value="20174">2017/2018 (Genap) - Kelas Perbaikan</option>
                    <option value="20183">2018/2019 (Ganjil) - Kelas Perbaikan</option>
                    <option value="20184">2018/2019 (Genap) - Kelas Perbaikan</option>
                    <option value="20193">2019/2020 (Ganjil) - Kelas Perbaikan</option>
                    <option value="20194">2019/2020 (Genap) - Kelas Perbaikan</option>
                    <option value="20203">2020/2021 (Ganjil) - Kelas Perbaikan</option>
                    <option value="20204">2020/2021 (Genap) - Kelas Perbaikan</option>
                  </select>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>