
<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Daftar Pengajuan Beasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				<a class="btn btn-primary" href="<?php echo base_url();?>form/beasiswa/master" data-toggle="tooltip" title="Kelola Beasiswa"><i class="icon-cogs"></i> Kelola Beasiswa</a>
				<i class="icon-question-sign icon-2x pull-right" style="color:#aaa;" title="Bantuan" id="help"></i>
				<div class="thumbnail pull-right" id="bantuan">
					<i class="icon-sign-blank" style="color:#fff98f"></i> Proses Pengajuan<br>
					<i class="icon-sign-blank" style="color:#98FB98"></i> Diterima<br>
					<i class="icon-sign-blank" style="color:#F08080"></i> Ditolak
				</div>
				<hr>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr> 
								<th>No</th>
								<th>NPM</th>
								<th>Nama</th>
								<th>Prestasi</th>
								<th>Beasiswa</th>
								<th>Waktu Pengajuan</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;
							foreach($pengajuan AS $row){ 
							if($row->status == 2){
								$color="background-color:#F08080";
							}elseif($row->status == 1){
								$color="background-color:#98FB98";
							}else{
								$color="background-color:#fff98f";
							}
							
							?>
							<tr>
								<td style="<?php echo $color?>"><?php echo $no; ?></td>
								<td style="<?php echo $color?>"><?php echo $row->npm; ?></td>
								<td style="<?php echo $color?>"><?php echo getNameMhs($row->npm) ?></td>
								<td style="<?php echo $color?>"><?php echo $row->prestasi ?></td>
								<td style="<?php echo $color?>"><?php echo getBeasiswa($row->beasiswa) ?></td>
								<td style="<?php echo $color?>"><?php echo datetimeIdn($row->createddate) ?></td>
								<td style="<?php echo $color?>"><?php echo getStatusbeasiswa($row->status) ?></td>
								<td style="<?php echo $color?>">
								<?php if(is_null($row->status)){?>
									<a class="btn btn-success btn-xs" href="<?php echo base_url();?>form/beasiswa/approve/<?php echo $row->id_beasiswa; ?>" data-toggle="tooltip" title="Terima"><i class="icon-check"></i></a>
									<a class="btn btn-danger btn-xs" href="<?php echo base_url();?>form/beasiswa/reject/<?php echo $row->id_beasiswa; ?>" data-toggle="tooltip" title="Tolak"><i class="icon-remove"></i></a>
								<?php } ?>
								<br>
								<a href="<?php echo base_url();?>form/beasiswa/print_beasiswa/<?php echo $row->id_beasiswa?>/<?php echo $row->npm?>" class="btn btn-info btn-xs" title="Cetak Formulir" ><i class="icon-print"> </i></a>
								</td>
							</tr>
							<?php $no++;} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
		$("#bantuan").hide();
		$('#help').css('cursor', 'pointer');
			$("#help").click(function() {
					$("#bantuan").toggle();
			});
	});
</script>
