<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <h4 class="modal-title">UNGGAH FORM RPS</h4>

</div>

<form class ='form-horizontal' action="<?php echo base_url(); ?>form/formrps/upld" method="post" enctype="multipart/form-data">

    <div class="modal-body">
        
        <div class="control-group" id="">

            <label class="control-label">Unggah RPS (.pdf) </label>

            <div class="controls">

                <input type="file" class="span4 form-control" name="userfile" required/>

                <input type="hidden" name="kdmk" value="<?php echo $kdmk; ?>">

                <input type="hidden" name="prod" value="<?php echo $prod; ?>">

            </div>

        </div>

    </div>

    <div class="modal-footer">

      <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>

      <input type="submit" class="btn btn-primary" value="Submit"/>

    </div>

</form>