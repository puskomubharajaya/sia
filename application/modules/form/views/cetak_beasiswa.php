<?php

if($mhs->KDJEKMSMHS == "L"){
	$jk="Laki-laki";
}elseif($mhs->KDJEKMSMHS == "P"){
	$jk="Perempuan";
}else{
	$jk="";
}

error_reporting(0);

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();

$pdf->AddPage();

$pdf->SetFont('Arial','B',14); 
	
$pdf->image(base_url().'assets/logo.gif',10,10,20);

$pdf->Ln(4);

$pdf->Cell(20,5,'',0,0,'C');

$pdf->Cell(170,5,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,1,'C');

$pdf->SetFont('Arial','B',10);

$pdf->Cell(20,4,'',0,0,'C');

$pdf->Cell(170,4,'Kampus I : Jl. Harsono RM No.67 Ragunan Pasar Minggu, Jakarta Selatan',0,1,'C');


$pdf->Cell(20,4,'',0,0,'C');

$pdf->Cell(170,4,'Kampus II : Jl. Raya Perjuangan, Bekasi Barat',0,1,'C');

$pdf->Ln(4);

$pdf->Cell(190,0,'',1,0,'C');

$pdf->ln(4);

$pdf->SetFont('Arial','',14);

$pdf->Cell(190,5,'FORMULIR PENDAFTARAN BEASISWA',0,1,'C');

$pdf->Cell(190,5,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,1,'C');

$pdf->ln(4);

$pdf->SetFont('Arial','',12);

$pdf->Cell(60,7,'Nama',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,$mhs->NMMHSMSMHS,0,1,'L');
$pdf->Cell(60,7,'NPM',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,$mhs->NIMHSMSMHS,0,1,'L');
$pdf->Cell(60,7,'No. NIK/KTP',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,$bio->ktp,0,1,'L');
$pdf->Cell(60,7,'Jenis Kelamin',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,$jk,0,1,'L');
$pdf->Cell(60,7,'Tempat/tanggal lahir',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,$mhs->TPLHRMSMHS.', '.TanggalIndo($mhs->TGLHRMSMHS),0,1,'L');
$pdf->Cell(60,7,'Alamat',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,$bio->alamat,0,1,'L');
$pdf->Cell(60,7,'Fakultas',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,get_fak_byprodi($mhs->KDPSTMSMHS),0,1,'L');
$pdf->Cell(60,7,'Program Studi',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,get_jur($mhs->KDPSTMSMHS),0,1,'L');
$pdf->Cell(60,7,'SKS yang ditempuh',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(25,7,$akt->SKSTTTRAKM,0,0,'L');
$pdf->Cell(20,7,'/ IPK',0,0,'C');
$pdf->Cell(80,7,$akt->NLIPKTRAKM,0,1,'L');
$pdf->Cell(60,7,'Nomor telpon rumah/HP',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,$bio->no_hp,0,1,'L');
$pdf->Cell(60,7,'Prestasi',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,$bea->prestasi,0,1,'L');
$pdf->Cell(60,7,'Pilihan Beasiswa',0,0,'L');
$pdf->Cell(5,7,':',0,0,'C');
$pdf->Cell(125,7,getBeasiswa($bea->beasiswa),0,1,'L');

$pdf->ln(4);

$pdf->SetFont('Arial','',10);

$pdf->Cell(190,5,'*Dokumen persyaratan asli harap diserahkan ke Biro Kemahasiswaan',0,1,'L');



$pdf->Output('Formulir_Beasiswa_'.$mhs->NIMHSMSMHS.'.PDF','I');



?>

