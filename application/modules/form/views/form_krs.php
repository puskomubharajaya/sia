<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Form KRS</h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <b><center>FORM KRS</center></b><br>
          <form id="edit-profile" class="form-horizontal" method="post" action="<?php echo base_url(); ?>auth/mhs_auth">
            <fieldset>
              <div class="control-group">                     
                <label class="control-label">NIM</label>
                <div class="controls">
                  <input type="text" class="span3" name="username" placeholder="NIM" required autofocus>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <div class="control-group">                     
                <label class="control-label">Password</label>
                <div class="controls">
                  <input type="password" class="span3" name="password" placeholder="Password" required>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->
              <div class="control-group">                     
                <label class="control-label">Kelas</label>
                <div class="controls">
                  <select name="kelas" class="span3" required>
                    <option disabled>Pilih Kelas Mahasiswa</option>
                    <option value="PG">A</option>
                    <option value="SR">B</option>
                    <option value="PK">C</option>
                  </select>
                </div> <!-- /controls -->       
              </div> <!-- /control-group -->

              <div class="form-actions">
                <input type="submit" class="btn btn-primary" id="save" value="Submit"/> 
                <input type="reset" class="btn btn-warning" value="Reset"/>
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>