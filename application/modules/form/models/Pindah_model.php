<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pindah_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function getAutocomplete($param)
	{
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa');
		$this->db->like('NIMHSMSMHS', $param, 'BOTH');
		$this->db->or_like('NMMHSMSMHS', $param, 'BOTH');
		$this->db->order_by('NIMHSMSMHS', 'asc');
		return $this->db->get();
	}

	function getSks($nim,$prd)
	{
		$this->db->select('b.sks_matakuliah');
    	$this->db->from('tbl_transaksi_nilai a');
    	$this->db->join('tbl_matakuliah b', 'a.KDKMKTRLNM = b.kd_matakuliah');
    	$this->db->where('a.NIMHSTRLNM', $nim);
    	$this->db->where('b.kd_prodi', $prd);
    	$get = $this->db->get();
    	return $get;
	}

	function getAcademicGuide($nim)
	{
		$this->db->where('npm_mahasiswa', $nim);
    	$this->db->order_by('id_verifikasi', 'desc');
    	return $this->db->get('tbl_verifikasi_krs', 1)->row()->id_pembimbing;
	}

	function getDataMove($npm,$year)
	{
		$this->db->where('npm', $npm);
    	$this->db->where('tahunakademik', $year);
    	return $this->db->get('tbl_pengunduran')->row();
	}

	function getMaxSmtMhs($nim)
	{
		$this->db->select('max(THSMSTRLNM) as khs');
		$this->db->from('tbl_transaksi_nilai');
		$this->db->where('NIMHSTRLNM', $nim);
		return $this->db->get()->row()->khs;
	}

}

/* End of file Pindah_model.php */
/* Location: ./application/models/Pindah_model.php */