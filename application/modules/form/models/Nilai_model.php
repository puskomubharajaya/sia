<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Nilai_model extends CI_Model
{

	public $variable;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_courses($userid, $tahunajar)
	{
		$kelas_ajar = $this->db->query("SELECT 
											jdl.id_jadwal,
											jdl.kd_jadwal,
											jdl.waktu_mulai,
											jdl.waktu_selesai,
											jdl.kelas,
											jdl.hari,
											jdl.kd_matakuliah,
											mk.nama_matakuliah,
											mk.sks_matakuliah
										FROM tbl_jadwal_matkul jdl
										JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
										WHERE jdl.kd_tahunajaran = '$tahunajar'
										AND jdl.kd_dosen = '$userid'
										AND mk.mk_ta IS NULL ")->result();
		return $kelas_ajar;
	}

	function get_matakuliah_by_jadwal($kodeJadwal)
	{
		$matakuliah = $this->db->query("SELECT DISTINCT 
											a.kd_matakuliah,
											b.nama_matakuliah,
											b.semester_matakuliah
	        							FROM tbl_jadwal_matkul a
										JOIN tbl_matakuliah b ON a.kd_matakuliah = b.kd_matakuliah
										WHERE 
											CASE WHEN SUBSTRING_INDEX(a.kd_matakuliah, '-', 1) <> 'MKDU'
											OR SUBSTRING_INDEX(a.kd_matakuliah, '-', 1) <> 'MKWU'
											OR SUBSTRING_INDEX(a.kd_matakuliah, '-', 1) <> 'MKU'
											THEN b.kd_prodi = SUBSTR(a.kd_jadwal,1,5) END
										AND a.kd_jadwal = '$kodeJadwal' ")->row();
		return $matakuliah;
	}

	function get_participant($kodeJadwal)
	{
		$participants 	= $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) AS jumlah FROM tbl_krs 
											WHERE kd_jadwal = '$kodeJadwal'")->row();
		return $participants;
	}

	function get_krs_transaction($kodeKrs)
	{
		$krs = $this->db->query('SELECT * FROM tbl_verifikasi_krs WHERE kd_krs LIKE "' . $kodeKrs . '%"')->row();
		return $krs;
	}

	function get_matakuliah($kodeMatakuliah, $prodi)
	{
		$matakuliah = $this->db->query("SELECT DISTINCT 
											kd_matakuliah, 
											nama_matakuliah, 
											semester_matakuliah 
										FROM tbl_matakuliah
										WHERE kd_matakuliah = '$kodeMatakuliah' 
										AND kd_prodi = SUBSTR('$prodi',1,5)")->row();
		return $matakuliah;
	}

	function participant_validation($npm, $kodeJadwal)
	{
		$participant = $this->db->query("SELECT * FROM tbl_krs WHERE npm_mahasiswa = '" . $npm . "' AND kd_jadwal = '" . $kodeJadwal . "'");
		return $participant;
	}

	function get_detail_participant($kodeJadwal)
	{

		$detail = $this->db->query("SELECT DISTINCT 
										NIMHSMSMHS,
										NMMHSMSMHS,
										
										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 2) AS absen, 

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 5) AS tugas1,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 6) AS tugas2,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 7) AS tugas3,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 8) AS tugas4,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 9) AS tugas5,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 3) AS uts,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 4) AS uas,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 10) AS final

									FROM tbl_mahasiswa mhs
									JOIN tbl_krs krs ON mhs.NIMHSMSMHS = krs.npm_mahasiswa
									WHERE krs.kd_jadwal = '$kodeJadwal' ");
		return $detail;
	}

	function get_detail_participants($kodeJadwal)
	{
		$detail = $this->db->query("SELECT DISTINCT 
										NIMHSMSMHS,
										NMMHSMSMHS,
										
										(
											(
												(SELECT COUNT(*) FROM tbl_absensi_mhs_new_20171
 												WHERE kd_jadwal = '$kodeJadwal'
 												AND npm_mahasiswa = mhs.NIMHSMSMHS
 												AND kehadiran = 'H') / 
 												(SELECT MAX(pertemuan) AS last_meet FROM tbl_absensi_mhs_new_20171
 												WHERE kd_jadwal = '$kodeJadwal')
 											) * 100
										) AS total_absen, 

										(SELECT nilai FROM tbl_nilai_detail
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 1) AS ratarata,

										(SELECT nilai FROM tbl_nilai_detail
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 5) AS tugas1,

										(SELECT nilai FROM tbl_nilai_detail
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 6) AS tugas2,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 7) AS tugas3,

										(SELECT nilai FROM tbl_nilai_detail
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 8) AS tugas4,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 9) AS tugas5,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 3) AS uts,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 4) AS uas,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 10) AS final

									FROM tbl_mahasiswa mhs
									JOIN tbl_krs krs ON mhs.NIMHSMSMHS = krs.npm_mahasiswa
									WHERE krs.kd_jadwal = '$kodeJadwal' ORDER BY mhs.NIMHSMSMHS ASC");
		return $detail;
	}
	function get_detail_participants_prodi($kodeJadwal)
	{
		$detail = $this->db->query("SELECT DISTINCT 
										NIMHSMSMHS,
										NMMHSMSMHS,
										
										(
											(
												(SELECT COUNT(*) FROM tbl_absensi_mhs_new_20171
 												WHERE kd_jadwal = '$kodeJadwal'
 												AND npm_mahasiswa = mhs.NIMHSMSMHS
 												AND kehadiran = 'H') / 
 												(SELECT MAX(pertemuan) AS last_meet FROM tbl_absensi_mhs_new_20171
 												WHERE kd_jadwal = '$kodeJadwal')
 											) * 100
										) AS total_absen, 

										(SELECT nilai FROM tbl_nilai_detail
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 1) AS ratarata,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 3) AS uts,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 4) AS uas,

										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 10) AS final

									FROM tbl_mahasiswa mhs
									JOIN tbl_krs krs ON mhs.NIMHSMSMHS = krs.npm_mahasiswa
									WHERE krs.kd_jadwal = '$kodeJadwal' ORDER BY mhs.NIMHSMSMHS ASC");
		return $detail;
	}

	function get_detail_participants_ta($kodeJadwal)
	{
		$detail = $this->db->query("SELECT DISTINCT 
										NIMHSMSMHS,
										NMMHSMSMHS,
										
										(SELECT nilai FROM tbl_nilai_detail 
										WHERE kd_krs = krs.kd_krs 
										AND kd_jadwal = '$kodeJadwal'
										AND tipe = 10) AS final

									FROM tbl_mahasiswa mhs
									JOIN tbl_krs krs ON mhs.NIMHSMSMHS = krs.npm_mahasiswa
									WHERE krs.kd_jadwal = '$kodeJadwal' ORDER BY mhs.NIMHSMSMHS ASC");
		return $detail;
	}

	function get_score($npm, $tipe, $tahunajar)
	{
		$score = $this->db->query("SELECT nilai FROM tbl_nilai_detail
									WHERE npm_mahasiswa = '$npm'
									AND tipe = '$tipe'
									AND tahun_ajaran = '$tahunajar' ")->row()->nilai;
		return $score;
	}

	function get_last_meeting($kode_jadwal)
	{
		$last_meet = $this->db->query("SELECT MAX(pertemuan) AS satu FROM tbl_absensi_mhs_new_20171 
										WHERE kd_jadwal = '$kode_jadwal'")->row()->satu;
		return $last_meet;
	}

	function get_lecturer($prodi, $tahunajar)
	{
		$fakultas = get_fak_byprodi($prodi);

		$lecturers 	= $this->db->query("SELECT 
											ky.nid,
											ky.nama,
											(SELECT SUM(mk.sks_matakuliah) FROM tbl_matakuliah mk
											JOIN tbl_jadwal_matkul jdl ON  mk.id_matakuliah = jdl.id_matakuliah
											WHERE jdl.kd_dosen = ky.nid
											AND jdl.kd_tahunajaran = '$tahunajar'
											AND mk.nama_matakuliah NOT LIKE '%skripsi%' 
											AND mk.nama_matakuliah NOT LIKE '%tesis%' 
											AND mk.nama_matakuliah NOT LIKE '%thesis%' 
											AND mk.nama_matakuliah NOT LIKE '%magang%' 
											AND mk.nama_matakuliah NOT LIKE '%kerja praktek%' 
											AND mk.nama_matakuliah NOT LIKE '%kuliah kerja%' 
											AND mk.nama_matakuliah NOT LIKE '%seminar%'
											AND mk.nama_matakuliah NOT LIKE '%kkn%') AS sks
										FROM tbl_jadwal_matkul jd 
										JOIN tbl_karyawan ky ON jd.kd_dosen = ky.nid
										WHERE kd_tahunajaran = '$tahunajar'
										AND (jd.kd_jadwal LIKE '$prodi/%' OR jd.kd_jadwal = '$fakultas/%')")->result();
		return $lecturers;
	}

	function get_non_general_courses($prodi, $tahunakademik)
	{
		$fakultas = get_fak_byprodi($prodi);

		$list = $this->db->query("SELECT 
									jm.id_jadwal,
									mk.kd_matakuliah,
									mk.nama_matakuliah,
									jm.kd_dosen,
									kr.nama,
									jm.kelas,
									mk.mk_ta
								FROM tbl_jadwal_matkul jm
								JOIN tbl_matakuliah mk ON jm.id_matakuliah = mk.id_matakuliah
								JOIN tbl_karyawan kr ON jm.kd_dosen = kr.nid
								WHERE jm.kd_tahunajaran = '$tahunakademik'
								AND (jm.kd_jadwal LIKE '$prodi%' OR jm.kd_jadwal LIKE '$fakultas/%')")->result();
		return $list;
	}
}

/* End of file Nilai_model.php */
/* Location: ./application/models/Nilai_model.php */
