            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Form Status Mahasiswa Pada Kelas Perbaikan</h4>

            </div>

            <form class ='form-horizontal' action="<?php echo base_url();?>sp/validasisp/save_open_mhs" method="post" enctype="multipart/form-data">

                <div class="modal-body" style="margin-left: -60px;">

                    <div class="control-group" id="">

                        <label class="control-label">Status</label>

                        <div class="controls">

                            <input type="hidden" name="kd_krs" value="<?php echo $kd_krs;?>" >

                            <select class="form-control" name="open_mhs" id="open_mhs" required>
                                <option>-- Pilih Status --</option>
                                <option value="1">Dikeluarkan </option>
                                <option value="0">Batal Dikeluarkan</option>
                            </select>

                        </div>

                    </div>

                </div> 

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>

                    <input type="submit" class="btn btn-primary" value="Simpan"/>

                </div>

            </form>