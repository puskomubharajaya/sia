<script>
  function app(idk) {
    $('#app_content').load('<?php echo base_url(); ?>sp/validasisp/verifikasi/' + idk);
  }
</script>

<div class="row">
  <div class="span12">
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>FORM SEMESTER PERBAIKAN</h3>
      </div> <!-- /widget-header -->

      <div class="widget-content">
        <div class="span11">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home1">Form Pendaftaran</a></li>
            <li><a data-toggle="tab" href="#home2">Data Pendaftar</a></li>
          </ul>
          <div class="tab-content">
            <div id="home1" class="tab-pane fade in active">
              <center>
                <h4>DATA MATAKULIAH SEMESTER PERBAIKAN - <?php echo $ta; ?> </i></u></h4>
              </center>
              <a href="<?php echo base_url(); ?>sp/validasisp/cetak_list_mk/<?php echo $ta; ?>" target="_blank" data-toggle="tooltip" title="Cetak Data Matakuliah Semester Perbaikan" class="btn btn-success"><i class="btn-icon-only icon-print"> </i> Cetak </a>
              <hr>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>SMTR</th>
                    <th>KODE MK</th>
                    <th>NAMA MATAKULIAH</th>
                    <th>Dosen</th>
                    <th>Waktu</th>
                    <th>PENDAFTAR</th>
                    <th>VERIFIKASI</th>
                    <th width="157">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($rows as $isi) {
                    // if ($isi->id_jadwal != '') {
                    //   $id_jdl = $isi->id_jadwal;
                    //   $dtl = '<a class="btn btn-primary btn-small" data-toggle="tooltip" title="Detail Pendaftar" target="_blank" 
                    //   href="'.base_url().'sp/validasisp/list_mhs/'.$isi->kd_matakuliah.'/'.$isi->id_jadwal.'"><i class="btn-icon-only icon-list-alt"> </i></a>';

                    //   $btn_jdl = '<a class="btn btn-success btn-small" title="Penugasan Dosen" 
                    //   href="#dosen" onclick="edit('.$isi->id_matakuliah.')" data-toggle="modal"><i class="btn-icon-only icon-user"></i></a>';
                    // }else{
                    //   $id_jdl = 0;
                    //   $dtl = '';
                    //   $btn_jdl = '<a class="btn btn-success btn-small" title="Penugasan Dosen" 
                    //   href="#dosen" onclick="edit('.$isi->id_matakuliah.')" data-toggle="modal"><i class="btn-icon-only icon-user"></i></a>';
                    // }

                    $cp = $this->db->query('SELECT COUNT(kd_jadwal) AS jml_mhs FROM tbl_krs_sp WHERE kd_jadwal = "' . $isi->kd_jadwal . '"')->row();

                    if ($isi->open == 0) {
                      $cb = 'btn-default';
                      $note = 'Belum  Terverifikasi';
                      $print = '<a target="_blank" href="' . base_url() . 'sp/validasisp/list_mhs/' . $isi->kd_matakuliah . '/' . $isi->id_jadwal . '" class="btn btn-info btn-small" title="Daftar Mahasiswa"><i class="btn-icon-only icon-list"></i></a> ';
                    } elseif ($isi->open == 1) {
                      $cb = 'btn-success';
                      $note = 'Kelas Dibuka';
                      $print = '<a target="_blank" href="' . base_url() . 'sp/validasisp/list_mhs/' . $isi->kd_matakuliah . '/' . $isi->id_jadwal . '" class="btn btn-info btn-small" title="Daftar Mahasiswa"><i class="btn-icon-only icon-list"></i></a> ';
                      $print .= '<a target="_blank" href="' . base_url() . 'sp/validasisp/cetak_absensi/' . $isi->id_jadwal . '" class="btn btn-warning btn-small" title="Cetak Absensi"><i class="btn-icon-only icon-print"></i></a>';
                    } elseif ($isi->open == 2) {
                      $cb = 'btn-danger';
                      $note = 'Kelas Ditutup';
                      $print = '<a target="_blank" href="' . base_url() . 'sp/validasisp/list_mhs/' . $isi->kd_matakuliah . '/' . $isi->id_jadwal . '" class="btn btn-info btn-small" title="Daftar Mahasiswa"><i class="btn-icon-only icon-list"></i></a> ';
                    }

                  ?>
                    <tr>
                      <td><?php echo $isi->smtr; ?></td>
                      <td><?php echo $isi->kd_matakuliah; ?></td>
                      <td><?php echo $isi->nama_matakuliah; ?></td>
                      <td><?php echo $isi->nama; ?></td>
                      <td><?php echo notohari($isi->hari) . ' / ' . substr($isi->waktu_mulai, 0, -3) . ' - ' . substr($isi->waktu_selesai, 0, -3); ?></td>
                      <td><?php echo $cp->jml_mhs; ?></td>
                      <?php $bayar = $this->app_model->getbayarsp($isi->kd_jadwal); ?>
                      <td><?php echo $bayar; ?></td>
                      <td class="td-actions">
                        <a onclick="app(<?php echo $isi->id_jadwal; ?>)" data-toggle="modal" href="#app" href="<?php echo base_url('sp/validasisp/cetak_absen/' . $isi->kd_matakuliah); ?>" class="btn <?php echo $cb; ?> btn-small" title="<?php echo $note; ?>"><i class="btn-icon-only icon-check"></i></a>
                        <?php echo $print; ?>
                      </td>
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>
            </div>
            <div id="home2" class="tab-pane fade in">
              <b>
                <center>DATA PENDAFTAR SEMESTER PERBAIKAN</center>
              </b><br>
              <br>
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NPM </th>
                    <th>Nama Mahasiswa</th>
                    <th>Semester</th>
                    <th>Total SKS</th>
                    <th>SKS di Setujui</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($mhs as $isi) {

                    $sms = $this->app_model->getdetail('tbl_mahasiswa', 'NIMHSMSMHS', $isi->npm_mahasiswa, 'NIMHSMSMHS', 'asc')->row();
                    $smtr = $this->app_model->get_semester($sms->SMAWLMSMHS);
                  ?>

                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $isi->npm_mahasiswa; ?></td>
                      <td><?php echo get_nm_mhs($isi->npm_mahasiswa); ?></td>
                      <td><?php echo $smtr; ?></td>
                      <td><?php echo $isi->jumlah_sks; ?></td>
                      <td><?php echo $isi->jml_open; ?></td>
                      <td>
                        <a class="btn btn-success btn-small" data-toggle="tooltip" title="Cetak Form" target="_blank" href="<?php echo base_url(); ?>sp/validasisp/print_formulir/<?php echo $isi->kd_krs ?>"><i class="btn-icon-only icon-print"></i></a>
                        <a class="btn btn-info btn-small" data-toggle="tooltip" title="Cetak KRS" target="_blank" href="<?php echo base_url(); ?>sp/validasisp/print_krssp/<?php echo $isi->kd_krs ?>"><i class="btn-icon-only icon-print"></i></a>
                        <a class="btn btn-warning btn-small" data-toggle="tooltip" title="Cetak KHS" target="_blank" href="<?php echo base_url(); ?>form/formnilaisp/cetakkhs/<?php echo $isi->kd_krs ?>"><i class="btn-icon-only icon-print"></i></a>
                      </td>
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="app" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id="app_content">

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->