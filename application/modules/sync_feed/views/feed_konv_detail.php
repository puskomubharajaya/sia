<div class="row">

    <div class="span12">                    

        <div class="widget ">

            <div class="widget-header">

                <i class="icon-list"></i>

                <h3>Data Mahasiswa Konversi - <?php echo date('Y'); ?></h3>

            </div> <!-- /widget-header -->

            

            <div class="widget-content">

<a href="<?php echo base_url(); ?>sync_feed/konversi/sync/<?php echo $npm;?>" title="" class="btn btn-warning"><i class="icon icon-refresh"></i> Sinkron</a>
<hr>
<div class="span11">
  <fieldset>
    <table id="example1" class="table table-striped">
      <thead>
        <tr> 
          <th width="40">No</th>
          <th>Kode MK asal</th>
          <th>Nama MK asal</th>
          <th>SKS MK asal</th>
          <th>Nilai MK asal</th>
          <th>Kode MK</th>
          <th>Nama MK</th>
          <th>SKS MK</th>
          <th>Nilai</th>
          <th>Mutu</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; foreach ($matkul as $row) { ?>
        <tr>
          <?php $forTa = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row(); ?>
          <td><?php echo $no; ?></td>
          <td><?php echo $row->kd_mk_asal; ?></td>
          <td><?php echo $row->nm_mk_asal; ?></td>
          <td><?php echo $row->sks_mk_asal; ?></td>
          <td><?php echo $row->nilai_mk_asal ?></td>

          
          <td><?php echo $row->kd_matakuliah; ?></td>
          <td><?php echo $row->nama_matakuliah; ?></td>
          <td><?php echo $row->sks_matakuliah; ?></td>
          

          <td><?php echo $row->BOBOTTRLNM; ?></td>
          <td><?php echo $row->NLAKHTRLNM; ?></td>
        </tr>
        <?php $no++; } ?>
      </tbody>
    </table>
  </fieldset>
</div>

</div>
</div>
</div>
</div>