<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Data Kartu Hasil Studi</h3>
			</div> <!-- /widget-header -->
			
			
			<div class="widget-content">
				<div class="span11">
					<!-- <a href="<?php //echo base_url(); ?>akademik/khs/transkrip" class="btn btn-primary"><i class="icon icon-print"></i> Transkrip</a> -->
					<?php if (substr($mhs->NIMHSMSMHS, 0,4) > 2014) { ?>
						<a href="<?php echo base_url(); ?>akademik/khs/transkrip/<?php echo $mhs->NIMHSMSMHS; ?>" class="btn btn-primary"><i class="icon icon-print"></i> Transkrip</a>
					<?php } ?>
					<!-- <a href="<?php //echo base_url(); ?>akademik/khs/transkrip" class="btn btn-primary"><i class="icon icon-print"></i> Transkrip</a> -->
					<hr>
					<table>

                        <tr>

                            <td>NPM</td>

                            <td>:</td>

                            <td><?php echo $mhs->NIMHSMSMHS;?></td>

                        </tr>

                        <tr>

                            <td>Nama</td>

                            <td>:</td>

                            <td><?php echo $mhs->NMMHSMSMHS;?></td>

                        </tr>
                        <?php
                        $logged = $this->session->userdata('sess_login');
	                    $pecah = explode(',', $logged['id_user_group']);
						$jmlh = count($pecah);
						for ($i=0; $i < $jmlh; $i++) { 
							$grup[] = $pecah[$i];
						}
	                    if ((in_array(5, $grup))) {
							$prodi = $mhs->KDPSTMSMHS;
						} else {
							$prodi = $logged['userid'];
						}
                        if (substr($mhs->NIMHSMSMHS, 0,4) > 2014) {
						    $ipk = $this->app_model->getipkmhs($mhs->NIMHSMSMHS,$prodi);
						} else {
							$ipk = '-';
						}
                        ?>
                        <tr>

                            <td>IPK</td>

                            <td>:</td>

                            <td><?php echo $ipk; ?></td>

                        </tr>

                    </table>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr> 
	                        	<th>No</th>
	                        	<th>Semester</th>
	                        	<th>Total SKS</th>
	                        	<th>IPS</th>
	                            <th width="40">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<?php $no = 1; foreach ($detail_khs as $row) { ?>
	                        <tr>
	                        	<td><?php echo $no; ?></td>
	                        	<?php $a = $this->app_model->get_semester_khs($row->SMAWLMSMHS,$row->THSMSTRLNM); ?>
	                        	<td><?php echo $a; ?></td>
                                <td><?php echo $row->jum_sks; ?></td>
                                <?php 
                                $logged = $this->session->userdata('sess_login');
			                    $pecah = explode(',', $logged['id_user_group']);
								$jmlh = count($pecah);
								for ($i=0; $i < $jmlh; $i++) { 
									$grup[] = $pecah[$i];
								}
			                    if ((in_array(5, $grup))) {
									$prodi = $mhs->KDPSTMSMHS;
								} else {
									$prodi = $logged['userid'];
								}
			                    $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah` FROM tbl_transaksi_nilai a
			                    JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
			                    WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.$row->NIMHSMSMHS.'" and a.THSMSTRLNM = "'.$row->THSMSTRLNM.'"')->result();

			                    $st=0;
			                    $ht=0;
			                    foreach ($hitung_ips as $iso) {
			                        $h = 0;

			                        $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
			                        $ht=  $ht + $h;

			                        $st=$st+$iso->sks_matakuliah;
			                    }

			                    $ips_nr = $ht/$st;
                                ?><?php //var_dump($row->semester_khs); ?>
                                <td><?php $ips = $this->app_model->get_ips_mahasiswa($row->NIMHSMSMHS, $row->semester_khs)->row()->ips; echo number_format($ips_nr, 2); ?></td>
	                        	<td class="td-actions">
									<a class="btn btn-success btn-small" href="<?php echo base_url(); ?>akademik/khs/detailKhs/<?php echo $row->NIMHSMSMHS; ?>/<?php echo $a; ?>"><i class="btn-icon-only icon-ok"> </i></a>
								</td>
	                        </tr>
							<?php $no++; } ?>
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>