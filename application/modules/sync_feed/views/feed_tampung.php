<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
                <i class="icon-home"></i>
  				<h3>Daya Tampung</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
                    <!-- <a data-toggle="modal" href="#myModal" class="btn btn-success"><i class="btn-icon-only icon-plus"></i>Tambah Data</a>
                    <a href="#" class="btn btn-primary"><i class="btn-icon-only icon-fire"></i>Rekap Data</a>
                    <hr> -->
                    <form class ='form-horizontal' action="<?php echo base_url(); ?>sync_feed/daya_tampung/inputdata" method="post">
                    
            
                        <div class="">   
                            <div class="control-group" id="">
                                <label class="control-label">Target Mahasiswa Baru </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="jml_target" value="<?php echo $row->jml_target;?>" class="form-control span4" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">Pendaftaran Mahasiswa Baru </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="jml_daftar" value="<?php echo $row->jml_daftar;?>" class="form-control span4" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">Peserta Lulus </label>
                                <div class="controls">
                                    <input type="text" id="tgl" value="<?php echo $row->jml_lulus;?>" name="jml_lulus" class="form-control span4" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">Peserta Daftar Ulang </label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="jml_daftar_ulang" value="<?php echo $row->jml_daftar_ulang;?>" class="form-control span4" required>
                                </div>
                            </div>
                            <div class="control-group" id="">
                                <label class="control-label">Peserta Undur Diri</label>
                                <div class="controls">
                                    <input type="text" id="tgl" name="jml_undur" value="<?php echo $row->jml_undur;?>" class="form-control span4" required>
                                </div>
                            </div>
                            <hr>
                            <a class="btn btn-primary" href="<?php echo base_url(); ?>sync_feed/daya_tampung/sync">SYNC</a> 
                            <button class="btn btn-success" type="submit">SUBMIT</button> 
                        </div>
                    </form>
                </div>           
            </div>
        </div>
    </div>
</div>