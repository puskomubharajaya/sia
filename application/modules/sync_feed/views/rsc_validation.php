<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Daftar Penelitian, Publikasi, dan Pengabdian Dosen</h3>
            </div> <!-- /widget-header -->
            
            <div class="widget-content">
                <div class="span11">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>No</th>
                                <th>NID</th>
                                <th>NIDN</th>
                                <th>Nama</th>
                                <th width="40">Lihat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach ($lectures as $lecture) { ?>
                            <tr>
                                <td><?= $no ?></td>
                                <td><?= $lecture->nid; ?></td>
                                <td><?= $lecture->nidn; ?></td>
                                <td><?= $lecture->nama; ?></td>
                                <td class="td-actions">
                                    <a class="btn btn-success btn-small" href="<?= base_url('sync_feed/research_validation/detail/'.$lecture->nid) ?>">
                                        <i class="icon-list"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Keterangan</h4>
            </div>
            <div class="modal-body">    
                <p>Terdapat dua (2) warna yang ditampilkan pada tabel daftar mahasiswa bimbingan anda.</p>
                <ul>
                    <li><b style="color:red">Merah</b> : Mahasiswa yang baru saja melakukan pengisian KRS dan belum ditindak lanjuti.</li>
                    <li><b>Putih</b> : Mahasiswa yang telah melakukan pengisian KRS dan telah ditindak lanjuti.</li>
                </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
