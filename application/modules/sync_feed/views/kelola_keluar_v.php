<div class="row">
    <div class="span12">                    
        <div class="widget ">
            <div class="widget-header">
                <a 
                    data-toggle="tooltip"
                    title="kembali"
                    href="<?= base_url('sync_feed/status_mhs/load_data') ?>" 
                    class="btn btn-default"
                    style="margin-left: 10px;;">
                    <i class="icon-chevron-left" style="margin-left: 0 !important"></i>
                </a>
                <h3>Kelola Mahasiswa Keluar</h3>
            </div>
            
            <div class="widget-content">
                <div class="span11">
                    <form action="<?= base_url('sync_feed/dropout/set_out') ?>" class="form-horizontal" method="post">
                        <button type="submit" class="btn btn-warning pull-right"><i class="icon-upload"></i> Submit</button>
                        <span data-toggle="tooltip" title="petunjuk penggunaan fitur" class="pull-right">
                            <button 
                                class="btn btn-default" 
                                type="button" 
                                data-target="#myModal" 
                                data-toggle="modal" 
                                style="margin-right: 5px">
                                <i class="icon-question"></i>
                            </button>
                        </span>
                        <div class="control-group">
                            <label for="tanggal" class="control-label">Tanggal Keluar</label>
                            <input 
                                type="text" 
                                class="span4 controls" 
                                placeholder="masukan tanggal keluar" 
                                id="tgl" 
                                name="tgl" 
                                required="">
                        </div>
                        <div class="control-group">
                            <label for="tanggal" class="control-label">Tahun Akademik Keluar</label>
                            <select name="tahunakademik" id="" class="span4 controls" required="">
                                <option value="" selected="" disabled=""></option>
                                <?php for ($i = 1995; $i < date('Y'); $i++) : ?>
                                    <?php for ($j = 1; $j < 3; $j++) : ?>
                                        <?php $gg = ($j == 1) ? 'Ganjil' : 'Genap' ?>
                                        <option value="<?= $i.$j ?>"><?= $i.'/'.($i+1).' '.$gg ?></option>
                                    <?php endfor; ?>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label for="angkatan" class="control-label">Pilih Angkatan</label>
                            <select name="angkatan" id="angkatan" class="span4 controls" required="">
                                <option value="" selected="" disabled=""></option>
                                <?php for ($n = 1995; $n < date('Y'); $n++) : ?>
                                    <option value="<?= $n ?>"><?= $n ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div id="append-here">
                            <table id="example3" class="table table-stripped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NPM</th>
                                        <th>Nama</th>
                                        <th>Angkatan</th>
                                        <th>Status</th>
                                        <th>Terakhir Aktif</th>
                                        <th>SKS Total</th>
                                        <th>IPK</th>
                                        <th width="40">Pilih</th>
                                    </tr>    
                                </thead>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Keterangan</h4>
            </div>
            <div class="modal-body">    
                <p>Harap isi kolom tanggal keluar dan tahun akademik keluar terlebih dahulu. Kemudian pilih angkatan mahasiswa yang akan di-<i>set</i> waktu keluarnya. Setelah itu akan muncul daftar mahasiswa berdasarkan angkatan yang dipilih. Klik pada kolom pilih untuk memilih mahasiswa yang akan di-<i>set</i> waktu keluarnya. Setelah itu klik <i>submit</i>. Mahasiswa yang telah di-<i>submit</i> tidak akan dimunculkan kembali di daftar mahasiswa pada halaman ini.</p>
                <p>
                    <b><u>Perhatikan</u></b> status mahasiswa sebelum melakukan <i>submit</i>. Cek apakah status sudah sesuai dengan status keluar yang seharusnya. 
                </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#tgl').datepicker({
            dateFormat: 'yy-mm-dd',
            yearRange: "1995:<?= date('Y') ?>",
            changeMonth: true,
            changeYear: true,
        });

        $('#angkatan').change(function() {
            $.ajax({
                url: '<?= base_url('sync_feed/dropout/get_out_list/') ?>' + $(this).val(),
                method: "post",
                beforeSend: function() {
                    $('#append-here').empty();
                    $('#append-here').append(`<div style="text-align: center"><h4><i>Loading . . .</i></h4><div>`)
                },
                success: function(res) {
                    $('#append-here').html(res)
                }
            });
        });
    })
</script>