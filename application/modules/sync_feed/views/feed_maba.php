<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-list"></i>
  				<h3>Daftar Calon Mahasiswa S1</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
				
					<a href="<?= base_url(); ?>sync_feed/maba/sync_maba_feed" class="btn btn-success">
						<i class="icon-refresh"></i> Sinkronisasi Mahasiswa Baru
					</a>
					<hr>
					<table id="example1" class="table table-bordered table-striped">
	                	<thead>
	                        <tr>
	                        	<th width="25">No</th>
	                        	<th>ID Registrasi</th>
	                        	<th>Tahun Masuk</th>
                                <th>Nama</th>
                                <th>NPM</th>
                                <th>Nomor Handphone</th>
                                <th>Jenis</th>
                                <th>Kelamin</th>
                                <th>Nama Ibu</th>
                                <!-- <th>Status</th> -->
                                <th>Tempat lahir</th>
                                <th width="80">Tanggal Lahir</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                            <?php $no=1; foreach ($look as $row) { ?>
                            <?php
                            	$smawl =  get_smawl($row->npm, $this->user_id);
                            ?>
	                        <tr>
                                <td><?= $no;?></td>
	                        	<td><?= $row->no_reg;?></td>
	                        	<td><?= $smawl;?></td>
	                        	<td><?= $row->nama;?></td>
	                        	<td><?= $row->npm;?></td>
	                        	<td><?= $row->handphone; ?></td>
	                        	
	                        	<?php 
	                        		if ($this->user_id == '74101' || $this->user_id == '61101'){
	                        			
	                        			if (substr(trim($row->npm), 7,1) == 5) {
											$jenis_daftar = "Mahasiswa Baru";
	                        			}elseif (substr(trim($row->npm), 7,1) == 7) {
											$jenis_daftar = "Pindahan";
	                        			}else{
											$jenis_daftar = "Read Misi";
	                        			}
	                        	?>
		                        <?php }else {
		                        		if (substr(trim($row->npm), 8,1) == 5) {
						                    $jenis_daftar = "Mahasiswa Baru";
						                } else {
						                    $jenis_daftar = "Pindahan";
						                }
		                        } ?>
	                        		
	                        	<td><?= $jenis_daftar ?></td>
	                        	<td><?= ($row->jenis_kelamin == 'P') ? "Perempuan" : "Laki-laki"; ?></td>
	                        	<td><?= $row->nama_ibu;?></td>
	                        	
	                        	
	                        	<!-- <td><?= $st; ?></td> -->

	                        	<td><?= $row->tpt_lahir; ?></td>
	                        	<td><?= $row->tgl_lahir; ?></td>
	                        </tr>
                            <?php $no++; } ?>
							
	                    </tbody>
	               	</table>
				</div>
			</div>
		</div>
	</div>
</div>
