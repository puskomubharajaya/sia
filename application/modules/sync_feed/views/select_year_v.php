<div class="row">
	<div class="span12">      		  		
		<div class="widget ">
			<div class="widget-header">
				<i class="icon-calendar"></i>
				<h3>Pilih Tahun Ajaran</h3>
		</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<div class="span11">
  				<form method="post" class="form-horizontal" action="<?= base_url(); ?>sync_feed/status_mhs/set_session">
            <fieldset>
              <div class="control-group">
                <label class="control-label">Tahun Ajaran</label>
                <div class="controls">
                  <select class="form-control span6" name="tahunajaran">
                    <option disabled selected>--Pilih Tahun Akademik--</option>
                    <?php foreach ($tahunajaran as $val) { ?>
                      <option value="<?= $val->kode; ?>"><?= $val->tahun_akademik; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>  
              <div class="form-actions">
                  <input type="submit" class="btn btn-large btn-success" value="Submit"/> 
              </div> <!-- /form-actions -->
            </fieldset>
          </form>
				</div>
			</div>
		</div>
	</div>
</div>
