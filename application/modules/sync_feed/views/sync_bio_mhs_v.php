<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <i class="icon-user"></i>
        <h3>Angkatan Mahasiswa</h3>
      </div>

      <div class="widget-content">
        <div class="span11">
        <form method="post" class="form-horizontal" action="<?= base_url(); ?>sync_feed/sync_bio_mhs/save_sess">
          <fieldset>
            <div class="control-group">
              <label class="control-label">Angkatan</label>
              <div class="controls">
                <select class="form-control span4" name="angkatan" id="angkatan" required>
                  <option disabled selected value="">-- Pilih Angkatan --</option>
                  <?php foreach ($year as $key => $row) { ?>
                  <option value="<?= $row;?>"><?= $row;?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-actions">
                <input type="submit" class="btn btn-large btn-success" value="Cari"/> 
            </div>
          </fieldset>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>

