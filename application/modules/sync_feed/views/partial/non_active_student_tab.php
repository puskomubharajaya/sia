<form action="<?= base_url('sync_feed/status_mhs/sync_non_active') ?>" method="post">
  <fieldset>
  <button type="submit" target="_blank" class="btn btn-primary pull-left" style="margin-right: 5px">
    <i class="icon icon-refresh"></i> Sinkronisasi Non Aktif
  </button>
  <a 
    href="<?= base_url('feeder/status_mahasiswa/nonaktif') ?>" 
    target="_blank" 
    style="margin-right: 5px"
    class="btn btn-default pull-left"
    data-toggle="tooltip"
    title="lihat detail data di feeder">
    <i class="icon-eye-open"></i> Data Nonaktif Feeder
  </a>
  <a 
    data-toggle="tooltip"
    title="Perhatian! Gunakan fitur ini hanya ketika Anda telah melakukan sinkronisasi"
    href="<?= base_url('sync_feed/status_mhs/update_sync/nonaktif') ?>" 
    target="_blank" 
    class="btn btn-info pull-left">
    <i class="icon icon-refresh"></i> Update Sinkronisasi Nonaktif
  </a>
  <table id="example8" class="table table-bordered table-striped">
      <thead>
        <tr> 
          <th width="40">No</th>
          <th>NPM</th>
          <th>NAMA</th>
          <th>IPK</th>
          <th>SKS Total</th>
          <th>Biaya Semester</th>
          <th width="80" style="text-align: center;">Set Keluar</th>
          <th width="80" style="text-align: center;">Set Cuti</th>
        </tr>
      </thead>
      <tbody>
        <?php $numb = 1; foreach ($non as $raw) { ?>
        <tr>
          <td><?= $numb; ?></td>
          <td>
            <?= $raw->NIMHSMSMHS; ?>
            <input type="hidden" name="mhs[<?= $raw->NIMHSMSMHS ?>]" value="<?= $raw->NIMHSMSMHS ?>">
          </td>
          <td><?= $raw->NMMHSMSMHS; ?></td>
          <td>
            <?= !is_null($raw->ipk) ? $raw->ipk : '-'; ?>
            <input type="hidden" name="ipk[<?= $raw->NIMHSMSMHS ?>]" value="<?= $raw->ipk ?>">
          </td>
          <td>
            <?= !is_null($raw->sks) ? $raw->sks : '-'; ?>
            <input type="hidden" name="sks[<?= $raw->NIMHSMSMHS ?>]" value="<?= $raw->sks ?>">
          </td>
          <td>
            <?= 'Rp. 0'; ?>
            <input type="hidden" name="fee[<?= $raw->NIMHSMSMHS ?>]" value="0">
          </td>
          <td style="text-align: center;">
            <a href="<?= base_url('sync_feed/status_mhs/update/'.$raw->NIMHSMSMHS); ?>" data-toggle="tooltip" title="pindahkan ke keluar" class="btn btn-danger">
              <i class="icon icon-signout"></i>
            </a>
          </td>
          <td style="text-align: center;">
            <a href="<?= base_url('sync_feed/status_mhs/set_to_cuti/'.$raw->NIMHSMSMHS); ?>" data-toggle="tooltip" title="pindahkan ke cuti" class="btn btn-warning">
              <i class="icon icon-signout"></i>
            </a>
          </td>
        </tr>
        <?php $numb++; } ?>
      </tbody>
    </table>
  </fieldset>
</form>

<script>
  $("#example8").dataTable({
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bSort": true,
    "bInfo": false,
    "bAutoWidth": true
  });

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>