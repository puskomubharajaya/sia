<fieldset>
  <a 
    href="<?= base_url('sync_feed/dropout/kelola_keluar') ?>" 
    class="btn btn-primary pull-left" 
    style="margin-right: 5px">
    <i class="icon-gear"></i> Kelola Mahasiswa Keluar
  </a>
  <button class="btn btn-primary pull-left" data-toggle="modal" data-target="#myModal" type="button">
    <i class="icon-refresh"></i> Sinkronisasi Data Keluar
  </button>
  <table id="example2" class="table table-bordered table-striped">
    <thead>
      <tr> 
        <th width="40">No</th>
        <th>NPM</th>
        <th>NAMA</th>
        <th>Status</th>
        <th>Tahun Akademik Terakhir</th>
        <th>Tanggal Keluar</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 1; foreach ($keluar as $row) { ?>
      <tr>
        <td><?= $no; ?></td>
        <td><?= $row->npm; ?></td>
        <td><?= $row->NMMHSMSMHS; ?></td>
        <td><?= $row->tipe == 'K' ? 'Mengundurkan diri' : 'Dikeluarkan' ?></td>
        <td><?= get_thnajar($row->tahunakademik) ?></td>
        <td><?= TanggalIndo($row->tanggal) ?></td>
      </tr>
      <?php $no++; } ?>
    </tbody>
  </table>
</fieldset>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Pilih Angkatan</h4>
      </div>
      <form action="<?= base_url('sync_feed/status_mhs/sync_out') ?>" method="post" class="form-horizontal">
        <div class="modal-body">
          <div class="alert alert-warning">
            <strong>Perhatikan!</strong> Status keluar mahasiswa sebelum melakukan sinkronisasi. Cek apakah sesuai atau tidak.
          </div>
          <div class="control-group">
            <label for="angkatan" class="control-label">Angkatan</label>
            <div class="controls">
              <select name="angkatan" class="span3" required="">
                <option value="" selected="" disabled=""></option>
                <?php for ($i = 1995; $i < date('Y'); $i++) : ?>
                  <option value="<?= $i ?>"><?= $i ?></option>
                <?php endfor; ?>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Sinkronisasikan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $("#example2").dataTable({
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bSort": true,
    "bInfo": false,
    "bAutoWidth": true
  });
</script>