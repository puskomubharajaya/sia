<table id="example3" class="table table-bordered table-striped">
    <thead>
        <tr> 
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
            <th>Angkatan</th>
            <th>Status</th>
            <th>Terakhir Aktif</th>
            <th>SKS Total</th>
            <th>IPK</th>
            <th width="40">Pilih</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; foreach ($outs as $out) { ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= $out->NIMHSMSMHS; ?></td>
            <td><?= $out->NMMHSMSMHS; ?></td>
            <td><?= $out->TAHUNMSMHS; ?></td>
            <td>
                <?= $out->STMHSMSMHS == 'K' ? 'Mengundurkan diri' : 'Dikeluarkan' ?>
                <input type="hidden" name="status[<?= $out->NIMHSMSMHS ?>]" value="<?= $out->STMHSMSMHS ?>">
            </td>
            <td><?= substr($out->last_active, 0, 4).'/'.substr($out->last_active, 4,1) ?></td>
            <td>
                <?= $out->sks ?>
                <input type="hidden" name="sks[<?= $out->NIMHSMSMHS ?>]" value="<?= $out->sks ?>">
            </td>
            <td><?= $out->ipk ?></td>
            <td class="td-actions" style="text-align: center;">
                <input type="checkbox" name="npm[<?= $out->NIMHSMSMHS ?>]" value="<?= $out->NIMHSMSMHS ?>">
            </td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>

<script>
    $("#example3").dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true
    });
</script>