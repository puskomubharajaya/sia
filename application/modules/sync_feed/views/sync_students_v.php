<div class="row">
	<div class="span12">      		  		
  		<div class="widget ">
  			<div class="widget-header">
  				<i class="icon-user"></i>
  				<h3>Biodata Mahasiswa</h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				<form action="<?= base_url('sync_feed/sync_bio_mhs/sync') ?>" method="post">
					<div class="span11">
						<a href="<?= base_url('sync_feed/sync_bio_mhs'); ?>" class="btn btn-warning pull-left"> 
							&laquo; Kembali
						</a>
						<button type="submit" class="btn btn-primary pull-left" style="margin-left: 10px">
							<i class="icon-refresh"></i> Sinkronisasi
						</button>
						<button type="button" data-toggle="modal" data-target="#info-modal" class="btn btn-default pull-left" style="margin-left: 10px">
							<i class="icon-info"></i> Petunjuk
						</button>
						<table class="table table-bordered table-striped" id="example0">
		                	<thead>
		                        <tr> 
		                        	<th>No</th>
		                        	<th>NPM</th>
		                        	<th>Nama</th>
	                                <th>Kelurahan</th>
	                                <th>Kab. / Kota</th>
	                                <th>No. HP</th>
	                                <th width="120">E-mail</th>
		                        </tr>
		                    </thead>
		                    <tbody>
	                            <?php $no = 1; foreach ($students as $key => $val) { ?>
		                        <tr>
		                        	<td><?= $no; ?></td>
		                        	<td><?= $val->npm; ?></td>
	                                <td><?= $val->nama; ?></td>
	                                <td><?= $val->kelurahan; ?></td>
	                                <td><?= $val->kota ?></td>
	                                <td>
	                                	<?= $val->no_hp; ?>
	                                	<input type="hidden" name="npm[<?= $val->npm ?>]" value="<?= $val->npm; ?>">
										<input type="hidden" name="nama[<?= $val->npm ?>]" value="<?= $val->nama; ?>">
										<input type="hidden" name="nik[<?= $val->npm ?>]" value="<?= $val->ktp; ?>">
										<input type="hidden" name="nisn[<?= $val->npm ?>]" value="<?= $val->nisn; ?>">
										<input type="hidden" name="kelurahan[<?= $val->npm ?>]" value="<?= empty($val->kelurahan) ? '-' : $val->kelurahan; ?>">
										<input type="hidden" name="dusun[<?= $val->npm ?>]" value="<?= $val->dusun; ?>">
										<input type="hidden" name="kab[<?= $val->npm ?>]" value="<?= $val->id_wil_kota; ?>">
										<input type="hidden" name="rt[<?= $val->npm ?>]" value="<?= $val->rt; ?>">
										<input type="hidden" name="rw[<?= $val->npm ?>]" value="<?= $val->rw; ?>">
										<input type="hidden" name="alamat[<?= $val->npm ?>]" value="<?= $val->alamat; ?>">
										<input type="hidden" name="hp[<?= $val->npm ?>]" value="<?= $val->no_hp; ?>">
										<input type="hidden" name="email[<?= $val->npm ?>]" value="<?= $val->email; ?>">
										<input type="hidden" name="jk[<?= $val->npm ?>]" value="<?= $val->jenis_kelamin; ?>">
										<input type="hidden" name="tpt_lahir[<?= $val->npm ?>]" value="<?= $val->tpt_lahir; ?>">
										<input type="hidden" name="tgl_lahir[<?= $val->npm ?>]" value="<?= $val->tgl_lahir; ?>">
										<input type="hidden" name="ibu[<?= $val->npm ?>]" value="<?= $val->ibu; ?>">
										<input type="hidden" name="ayah[<?= $val->npm ?>]" value="<?= $val->ayah; ?>">
	                                </td>
	                                <td><?= $val->email; ?></td>
		                        </tr>
		                        <?php $no++; } ?>
		                    </tbody>
		               	</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="info-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Informasi</h4>
			</div>
			<div class="modal-body">
				<p>Harap lakukan sikronisasi <b><u>secara bertahap</u></b> dengan melakukan sinkronisasi perhalaman. Per halaman memuat 100 daftar mahasiswa, kemudian lakukan sinkronisasi di halaman selanjutnya setelah dilakukan sinkronisasi pada halaman yang sedang aktif.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>