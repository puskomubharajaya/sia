<div class="row">
  <div class="span12">                
    <div class="widget ">
      <div class="widget-header">
        <a 
          data-toggle="tooltip"
          title="kembali"
          href="<?= base_url('sync_feed/aktifitas_mahasiswa/data_aktifitas') ?>" 
          class="btn btn-default" 
          style="margin-left: 10px">
          <i class="icon-chevron-left" style="margin-left: 0"></i>
        </a>
        <h3>Data Aktifitas Mahasiswa - No. SKEP <?= strtoupper($act->no_sk_tugas); ?></h3>
      </div> 

      <div class="widget-content">
        <div class="tabbable">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#anggota" data-toggle="tab">Anggota</a></li>
            <li><a href="#pembimbing" data-toggle="tab" onclick="load_tab('pbb',<?= $id_aktifitas ?>)">Pembimbing</a></li>
            <li><a href="#penguji" data-toggle="tab" onclick="load_tab('pgj',<?= $id_aktifitas ?>)">Penguji</a></li>
          </ul>      

          <div class="tab-content">
            <div class="tab-pane active" id="anggota">
              <fieldset>
                <button 
                  type="button" 
                  data-toggle="modal"
                  data-target="#addmember" 
                  style="margin-right: 5px" 
                  class="btn btn-success pull-left">
                  <i class="icon icon-plus"></i> Tambah Anggota Aktifitas
                </button>
                
                <?php if ($act->jenis == 7) : ?>
                  <button 
                    type="button" 
                    data-toggle="modal"
                    data-target="#bulkaddmember" 
                    style="margin-right: 5px" 
                    class="btn btn-warning pull-left">
                    <i class="icon icon-plus"></i> Bulk Insert
                  </button>
                <?php endif ?>

                <a 
                  href="<?= base_url('sync_feed/aktifitas_mahasiswa/sync_member/'.$id_aktifitas) ?>" 
                  style="margin-right: 5px" 
                  class="btn btn-primary pull-left">
                  <i class="icon icon-refresh"></i> Sinkronisasi Anggota
                </a>
                <!-- <a 
                  data-toggle="tooltip"
                  title="Perhatian! Gunakan fitur ini hanya ketika Anda telah melakukan sinkronisasi"
                  href="<?= base_url('sync_feed/aktifitas_mahasiswa/') ?>" 
                  target="_blank" 
                  class="btn btn-primary pull-left">
                  <i class="icon icon-refresh"></i> Update Sinkronisasi Anggota
                </a> -->
                
                <table id="example4" class="table table-bordered table-striped">
                  <thead>
                    <tr> 
                      <th width="40">No</th>
                      <th>NPM</th>
                      <th>Nama</th>
                      <th>Jenis</th>
                      <th>Peran</th>
                      <th>Status</th>
                      <th width="40">Hapus</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; foreach ($anggota as $row) { ?>
                    <tr>
                      <td><?= $no; ?></td>
                      <td><?= $row->npm ?></td>
                      <td><?= $row->NMMHSMSMHS; ?></td>
                      <td><?= $row->nama ?></td>
                      <td>
                        <?= $row->peran == 1 ? 'Ketua' : ($row->peran ==  2 ? 'Anggota' : 'Personal') ?>
                      </td>
                      <td>
                        <?php if (!is_null($row->id_anggota_feeder)) : ?>
                          <span class="label label-success">Tersinkronisasi</span>
                        <?php else : ?> 
                          <span class="label label-default">Belum Tersinkronisasi</span>
                        <?php endif; ?>
                      </td>
                      <td>
                        <a 
                          onclick="return confirm('Anda yakin ingin menghapus anggota ini dari daftar?')"
                          href="<?= base_url('sync_feed/aktifitas_mahasiswa/remove_member/'.$row->id_mhs.'/'.$id_aktifitas) ?>"
                          class="btn btn-danger"
                          data-toggle="tooltip"
                          title="hapus anggota">
                          <i class="icon-trash"></i>
                        </a>
                      </td>
                    </tr>
                    <?php $no++; } ?>
                  </tbody>
                </table>
              </fieldset>
            </div>

            <div class="tab-pane" id="pembimbing">
              <div id="pbb">
                <center>
                  <img src="<?= base_url('assets/img/cat_load.gif') ?>" style="width: 30%">
                  <h4><i>Loading . . .</i></h4>
                </center>
              </div>
            </div>

            <div class="tab-pane" id="penguji">
              <div id="pgj">
                <center>
                  <img src="<?= base_url('assets/img/cat_load.gif') ?>" style="width: 30%">
                  <h4><i>Loading . . .</i></h4>
                </center>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="addmember" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Anggota</h4>
      </div>
      <form action="<?=  base_url('sync_feed/aktifitas_mahasiswa/add_member') ?>" method="post" class="form-horizontal">
        <div class="modal-body">
          <div class="control-group">
            <label for="npm" class="control-label">NPM</label>
            <div class="controls">
              <input type="text" class="span3" name="npm" id="npm" placeholder="ketik NPM atau nama mahasiswa" required="">
              <input type="hidden" name="hidden_npm">
              <input type="hidden" name="id_aktifitas" value="<?= $id_aktifitas ?>">
            </div>
          </div>
          <div class="control-group">
            <label for="" class="control-label">Peran</label>
            <div class="controls">
              <div class="radio">
                <label><input type="radio" name="peran" value="1" required="">Ketua</label>
                <label><input type="radio" name="peran" value="2" required="">Anggota</label>
                <label><input type="radio" name="peran" value="3" required="">Personal</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="bulkaddmember" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Anggota Sekaligus</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <small>
            Fitur ini memungkinkan Anda menambahkan mahasiswa untuk kegiatan bimbingan akademis secara sekaligus berdasarkan dosen pembimbing yang telah ditentukan. <b><u>Perhatian!</u></b> Pastikan anda telah menambahkan dosen pembimbing terlebih dahulu sebelum menggunakan fitur ini.
          </small>
        </div>
        <a 
          class="btn btn-warning btn-block" 
          href="<?= base_url('sync_feed/aktifitas_mahasiswa/insert_member_by_guide/'.$id_aktifitas) ?>">
          <i class="icon-refresh"></i> Generate mahasiswa bimbingan
        </a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function load_tab(tab,id_act) {
    $('#' + tab).load('<?= base_url('sync_feed/aktifitas_mahasiswa/get_tab/') ?>' + tab + '/' + id_act)
  }

  $(document).ready(function($) {
    $('input[name^=npm]').autocomplete({
      source: '<?= base_url('sync_feed/aktifitas_mahasiswa/search_mhs');?>',
      minLength: 4,
      select: function (evt, ui) {
          this.form.npm.value = ui.item.value;
          this.form.hidden_npm.value = ui.item.npm;
      }
    });
  });
</script>

<br><br>