<script>
function app(idk){
$('#app_content').load('<?php echo base_url();?>sp/validasisp/verifikasi/'+idk);
}
</script>

<div class="row">
  <div class="span12">                
      <div class="widget ">
        <div class="widget-header">
          <i class="icon-user"></i>
          <h3><?php echo $mhs->NMMHSMSMHS.' ( '.$mhs->NIMHSMSMHS.' )'; ?></h3>
      </div> <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="span11">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#home1">KRS/<?php echo $tahunakademik ?></a></li>
              <li><a data-toggle="tab" href="#home2">KHS/<?php echo $ta_khs ?></a></li>
          </ul>
          <div class="tab-content">
            <div id="home1" class="tab-pane fade in active">
              <center>
                <h4>DATA KARTU RENCANA STUDI - <?php echo $tahunakademik; ?> </i></u></h4><br>
              </center>
          <p>Pembimbing Akademik : <?php echo $pembimbing['nama']; ?> </p>
          <table id="tabel_krs" class="table table-bordered table-striped">
            <thead>
              <tr> 
                <th>No</th>
                <th>Kode MK</th>
                <th>Mata Kuliah</th>
                <th>SKS</th>                
                <th>Dosen</th>
                <th>Kelas</th>
                <!-- <th style="text-align:center">Angka</th>
                <th style="text-align:center">Huruf</th> -->
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; foreach ($detail_krs as $row) { ?>
              <tr>
                <?php
                  switch ($row->hari){
                    case 1 :
                      $hari = 'Senin';
                    break;
                    case 2 :
                      $hari = 'Selasa';
                    break;
                    case 3 :
                      $hari = 'Rabu';
                    break;
                    case 4 :
                      $hari = 'Kamis';
                    break;
                    case 5 :
                      $hari = 'Jumat';
                    break;
                    case 6 :
                      $hari = 'Sabtu';
                    break;
                    case 7 :
                      $hari = 'Minggu';
                    break;
                    default:
                    $hari = '';
                  }
                ?>
                <td><?php echo $no; ?></td>
                <td><?php echo $row->kd_matakuliah; ?></td>
                <td><?php echo $row->nama_matakuliah; ?></td>
                <td><?php echo $row->sks_matakuliah; ?></td>
                <td><?php echo $row->nama; ?></td>
                <td><?php echo $row->kelas; ?></td>    
              </tr>
              <?php $no++; } ?>
            </tbody>
          </table>
            </div>
            <div id="home2" class="tab-pane fade in">
              <center>
                <h4>DATA KARTU HASIL STUDI - <?php echo $ta_khs ?></i></u></h4>
              </center>
              <?php                 
                $logged = $this->session->userdata('sess_login');
                $prodi = $logged['userid'];
                $pecah = explode(',', $logged['id_user_group']);
                
                if ($tahunakademik < '20151') {
                    $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah`
                                                    FROM tbl_transaksi_nilai a JOIN tbl_matakuliah_copy b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                                                    WHERE b.`tahunakademik` = "'.$tahunakademik.'" AND kd_prodi = "'.$prodi.'" 
                                                    AND NIMHSTRLNM = "'.$npm.'" and THSMSTRLNM = "'.$tahunakademik.'" ')->result();
                } else {
                    $hitung_ips = $this->db->query('SELECT distinct a.`NIMHSTRLNM`,a.`KDKMKTRLNM`,a.`NLAKHTRLNM`,a.`BOBOTTRLNM`,b.`sks_matakuliah`
                                                    FROM tbl_transaksi_nilai a JOIN tbl_matakuliah b ON a.`KDKMKTRLNM` = b.`kd_matakuliah`
                                                    WHERE a.`kd_transaksi_nilai` IS NOT NULL AND kd_prodi = "'.$prodi.'" 
                                                    AND NIMHSTRLNM = "'.$npm.'" and THSMSTRLNM = "'.$tahunakademik.'" ')->result();
                }

                $st=0;
                $ht=0;
                foreach ($hitung_ips as $iso) {
                    $h = 0;


                    $h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
                    $ht=  $ht + $h;

                    $st=$st+$iso->sks_matakuliah;
                }

                $ips_nr = $ht/$st;
                //$ipk = $this->app_model->get_ipk_mahasiswa($q->NIMHSMSMHS)->row()->ipk;
                for ($i=0; $i < $jmlh; $i++) { 
                    $grup[] = $pecah[$i];
                }

                $cek = $this->db->query("SELECT * from tbl_sinkronisasi_renkeu where npm_mahasiswa = '".substr($kode->kd_krs, 0,12)."'
                                        and (status >= 1 and status < 8) and tahunajaran = '".substr($kode->kd_krs, 12,5)."'")->result();

                $krsan = $this->app_model->getdetail('tbl_verifikasi_krs','kd_krs',$kode->kd_krs,'kd_krs','asc')->row()->status_verifikasi;
                
                if (($cek == true) and (($krsan != 9) and ($krsan != 10))) {
                //if (($cek == true)) { 
                } ?>
                    
                   
                <table>
                    <tr>
                        <!-- <td>NPM</td>
                        <td>:</td>
                        <td><?php echo $npm;?></td>
                        <td width="100"></td>
                        <td>SEMESTER</td>
                        <td>:</td>
                        <?php $a = $this->app_model->get_semester_khs($mhs->SMAWLMSMHS,$tahunakademik); ?>
                        <td><?php echo $a;?></td> -->
                    </tr>
                    <tr>
                        <td>IPS</td>
                        <td>:</td>
                        <td><?php echo number_format($ips_nr, 2); ?></td>
                    </tr>
                </table>
                <hr>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr> 
                            <th>No</th>
                            <th>Kode MK</th>
                            <th>Mata Kuliah</th>
                            <th>SKS</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; foreach ($detail_khs as $row) { ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->kd_matakuliah; ?></td>
                            <td><?php echo $row->nama_matakuliah; ?></td>
                            <td><?php echo $row->sks_matakuliah ?></td>
                            <?php   
                            $nilai = $this->db->query("SELECT * from tbl_transaksi_nilai where NIMHSTRLNM = '".$mhs->NIMHSMSMHS."' 
                            	                         and KDKMKTRLNM = '".$row->kd_matakuliah."' and THSMSTRLNM = '".$ta_khs."' ")->row();
                            ?>
                            <td><?php echo $nilai->NLAKHTRLNM; ?></td>
                        </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
            </div>
      </div>
    </div>
  </div>
</div>
