<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktifitas_mahasiswa extends CI_Controller {

	private $userid, $usergroup;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth/logout','refresh');
		}

		$this->userid = $this->session->userdata('sess_login')['userid'];
		$group = $this->session->userdata('sess_login')['id_user_group'];
		$this->usergroup = get_group($group);

		if (!in_array(19, $this->usergroup)) {
			redirect('auth','refresh');
		}

		date_default_timezone_set("Asia/Jakarta");
		$this->load->library('pddikti');
		$this->load->model('sync_feed/aktifitas_mhs_model','act');
	}

	public function index()
	{
		$this->session->unset_userdata('tahunakademik_aktifitas');
		$data['userid'] = $this->userid;
		$data['tahunakademik'] = $this->db->get('tbl_tahunakademik')->result();
		$data['page'] = "aktifitas_mhs_v";
		$this->load->view('template/template', $data);
	}

	public function set_session()
	{
		$this->session->set_userdata('tahunakademik_aktifitas', $this->input->post('tahunakademik'));
		redirect('sync_feed/aktifitas_mahasiswa/data_aktifitas');
	}

	public function data_aktifitas()
	{
		$data['tahunakademik']   = $this->session->userdata('tahunakademik_aktifitas');
		$data['jenis_aktifitas'] = $this->db->get('tbl_jenis_aktifitas')->result();
		$data['aktifitas']       = $this->act->get_aktifitas($this->userid, $data['tahunakademik']);
		$data['page']            = "aktifitas_list_v";
		$this->load->view('template/template', $data);
	}

	public function save()
	{
		extract(PopulateForm());

		$this->_is_sk_exist($no_sk);

		$data = [
			'prodi' => $this->userid,
			'tahunakademik' => $this->session->userdata('tahunakademik_aktifitas'),
			'jenis' => $jenis,
			'judul' => $judul,
			'lokasi' => $lokasi,
			'no_sk_tugas' => $no_sk,
			'tgl_sk_tugas' => $tgl_sk,
			'jenis_anggota' => $anggota,
			'note' => $note,
			'created_at' => date('Y-m-d H:i:s')
		];

		$this->db->insert('tbl_aktifitas_mhs', $data);
		echo '<script>alert("Data berhasil disimpan!");
		location.href="'.base_url('sync_feed/aktifitas_mahasiswa/data_aktifitas').'";</script>';
	}

	public function edit($id)
	{
		$data['jenis_aktifitas'] = $this->db->get('tbl_jenis_aktifitas')->result();
		$data['aktifitas'] = $this->act->get_detail_aktifitas($id)->row();
		$this->load->view('partial/edit_aktifitas', $data);
	}

	public function update()
	{
		extract(PopulateForm());

		$this->_is_activity_exist($id, 'Gagal diubah! Data tidak ditemukan!', 'sync_feed/aktifitas_mahasiswa/data_aktifitas');
		
		if ($hidden_no_sk != $no_sk) {
			$this->_is_sk_exist($no_sk);
		}

		$data = [
			'jenis' => $jenis,
			'judul' => $judul,
			'lokasi' => $lokasi,
			'no_sk_tugas' => $no_sk,
			'tgl_sk_tugas' => $tgl_sk,
			'jenis_anggota' => $anggota,
			'note' => $note,
			'updated_at' => date('Y-m-d H:i:s')
		];

		$this->db->update('tbl_aktifitas_mhs', $data, ['id' => $id]);
		echo '<script>alert("Data berhasil diubah!");
		location.href="'.base_url('sync_feed/aktifitas_mahasiswa/data_aktifitas').'";</script>';
	}

	private function _is_sk_exist($sk)
	{
		$check = $this->db->get_where('tbl_aktifitas_mhs', ['no_sk_tugas' => $sk, 'deleted_at IS NULL' => NULL])->num_rows();

		if ($check > 0) {
			echo '<script>alert("Gagal menyimpan/mengubah data! Data aktifitas dengan nomor SK tersebut telah digunakan!");
			location.href="'.base_url('sync_feed/aktifitas_mahasiswa/data_aktifitas').'";</script>';
			exit();
		}

		return;
	}

	private function _is_activity_exist($id, $message, $redirect_to)
	{
		$is_aktifitas_exist = $this->act->get_detail_aktifitas($id)->num_rows();

		if ($is_aktifitas_exist == 0) {
			echo '<script>alert("'.$message.'");
			location.href="'.base_url($redirect_to).'";</script>';
			exit();
		}
		return;
	}

	public function remove($id)
	{
		$this->_is_activity_exist($id, 'Gagal menghapus! Data tidak ditemukan!', 'sync_feed/aktifitas_mahasiswa/data_aktifitas');

		$this->_is_activity_has_content($id);

		$this->db->update('tbl_aktifitas_mhs', ['deleted_at' => date('Y-m-d H:i:s')], ['id' => $id]);
		echo '<script>alert("Data berhasil dihapus!");
		location.href="'.base_url('sync_feed/aktifitas_mahasiswa/data_aktifitas').'";</script>';
	}

	private function _is_activity_has_content($id)
	{
		$param    = ['id_aktifitas' => $id, 'deleted_at IS NULL' => NULL];
		$member   = $this->db->get_where('tbl_anggota_aktifitas', $param)->num_rows();
		$guide    = $this->db->get_where('tbl_pembimbing_aktifitas', $param)->num_rows();
		$examiner = $this->db->get_where('tbl_penguji_aktifitas', $param)->num_rows();

		if ($member > 0 || $guide > 0 || $examiner > 0) {
			echo '<script>alert("Gagal menghapus! Aktifitas tidak kosong!");
			location.href="'.base_url('sync_feed/aktifitas_mahasiswa/data_aktifitas').'";</script>';
			exit();
		}

		return;
	}

	public function sync_activity()
	{
		$token    = $this->pddikti->get_token();
		$id_prodi = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
		$activity = $this->act->get_available_activity($this->userid, $this->session->userdata('tahunakademik_aktifitas'));

		foreach ($activity as $val) {
			$data = [
				'id_prodi' => $id_prodi,
				'id_semester' => $this->session->userdata('tahunakademik_aktifitas'),
				'id_jenis_aktivitas' => $val->jenis,
				'judul' => $val->judul,
				'lokasi' => $val->lokasi,
				'sk_tugas' => $val->no_sk_tugas,
				'tanggal_sk_tugas' => $val->tgl_sk_tugas,
				'jenis_anggota' => $val->jenis_anggota,
				'keterangan' => $val->note
			];

			$payload = [
				"token" => $token,
				"act" => "InsertAktivitasMahasiswa",
				"record" => $data
			];
			$exec = $this->pddikti->runWS($payload);
			$result = json_decode($exec);
			$result->nomor_sk = $val->no_sk_tugas;
			$result->status = $result->error_code == 0 ? "Sinkronisasi berhasil" : "Sinkronisasi gagal";

			if ($result->error_code == 0) {
				$this->db->update('tbl_aktifitas_mhs', ['id_aktifitas_feeder' => $result->data->id_aktivitas], ['id' => $val->id]);
			}

			dd($result); echo '<hr>';
		}
	}

	public function sync_activity_unit($id)
	{
		$token    = $this->pddikti->get_token();
		$id_prodi = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
		$act      = $this->db->get_where('tbl_aktifitas_mhs', ['id' => $id])->row();

		$data = [
			'id_prodi'           => $id_prodi,
			'id_semester'        => $this->session->userdata('tahunakademik_aktifitas'),
			'id_jenis_aktivitas' => $act->jenis,
			'judul'              => $act->judul,
			'lokasi'             => $act->lokasi,
			'sk_tugas'           => $act->no_sk_tugas,
			'tanggal_sk_tugas'   => $act->tgl_sk_tugas,
			'jenis_anggota'      => $act->jenis_anggota,
			'keterangan'         => $act->note
		];

		$payload = [
			"token"  => $token,
			"act"    => "InsertAktivitasMahasiswa",
			"record" => $data
		];

		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec);
		$result->nomor_sk = $act->no_sk_tugas;
		$result->status = $result->error_code == 0 ? "Sinkronisasi berhasil" : "Sinkronisasi gagal";

		if ($result->error_code == 0) {
			$this->db->update('tbl_aktifitas_mhs', ['id_aktifitas_feeder' => $result->data->id_aktivitas], ['id' => $act->id]);
		}

		dd($result,2);
	}

	public function detail($id)
	{
		$this->_is_activity_exist($id, 'Data tidak ditemukan!', 'sync_feed/aktifitas_mahasiswa/data_aktifitas');

		$tahunakademik        = $this->session->userdata('tahunakademik_aktifitas');
		$data['act']          = $this->db->get_where('tbl_aktifitas_mhs', ['id' => $id])->row();
		$data['id_aktifitas'] = $id;
		$data['anggota']      = $this->act->get_anggota($id);
		$data['page']         = "detail_aktifitas_v";
		$this->load->view('template/template', $data);
	}

	public function get_tab($tab, $id_act)
	{
		$data['id_aktifitas'] = $id_act;

		switch ($tab) {
			case 'pbb':
				$data['pembimbing'] = $this->act->get_pembimbing($id_act);
				$data['kegiatan']   = $this->_activity_category();
				$this->load->view('partial/pembimbing_tab_v', $data);
				break;
			
			default:
				$data['penguji']  = $this->act->get_penguji($id_act);
				$data['kegiatan'] = $this->_activity_category();
				$this->load->view('partial/penguji_tab_v', $data);
				break;
		}
	}

	public function sub_category($code)
	{
		$subact = $this->db->get_where('tbl_subactivity_category_feeder', ['parent_code' => $code])->result();

		$sub = '<div class="control-group">';
        $sub .= '<label for="nidn" class="control-label">Subkategori Kegiatan</label>';
        $sub .= '<div class="controls">';
        $sub .= '<select name="subact" class="span3" id="" required="">';
                                
		foreach ($subact as $value) {
			$sub .= '<option value="'.$value->code.'@'.$value->name.'">';
            $sub .= $value->name;
			$sub .= '</option>';
		}

		$sub .= '</select>';
		$sub .= '</div>';
		$sub .= '</div>';

		$response = count($subact) > 0 ? $sub : '';
		echo $response;
	}

	private function _activity_category()
	{
		$token = $this->pddikti->get_token();
		$payload = [
			"act" => "GetKategoriKegiatan",
			"token" => $token
		];
		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec)->data;
		return $result;
	}

	public function search_mhs()
	{
		$mhs = $this->act->get_mhs($_GET['term'], $this->userid);
		foreach ($mhs as $val) {
			$data[] = [
				'npm' => $val->NIMHSMSMHS,
				'value' => $val->NIMHSMSMHS . ' - ' . $val->NMMHSMSMHS
			];
		}
		echo json_encode($data);
	}

	public function add_member()
	{
		extract(PopulateForm());
	
		$this->_is_activity_exist(
					$id_aktifitas, 
					'Gagal menyimpan! Data aktifitas tidak ditemukan!', 
					'sync_feed/aktifitas_mahasiswa/detail/'.$id_aktifitas
				);

		$this->_is_member_has_added($hidden_npm, $id_aktifitas, TRUE);

		$data = [
			'id_aktifitas' => $id_aktifitas,
			'npm' => $hidden_npm,
			'peran' => $peran,
			'created_at' => date('Y-m-d H:i:s')
		];
		$this->db->insert('tbl_anggota_aktifitas', $data);

		echo '<script>alert("Data berhasil disimpan!");
		location.href="'.base_url('sync_feed/aktifitas_mahasiswa/detail/'.$id_aktifitas).'";</script>';
	}

	private function _is_member_has_added($npm, $id, $direct_response)
	{
		$is_exist = $this->act->available_member($id, $npm)->num_rows();
		if ($direct_response) {
			if ($is_exist > 0) {
				echo '<script>alert("Gagal menyimpan! Data mahasiswa ini telah tersedia!");history.go(-1);</script>';
				exit();
			}
			return;
		}
		return $is_exist;
	}

	public function insert_member_by_guide($id_act)
	{
		$get_nid = $this->db->get_where('tbl_pembimbing_aktifitas', ['id_aktifitas' => $id_act])->row();
		if (!isset($get_nid)) {
			echo '<script>alert("Gagal menambahkan anggota! Dosen pembimbing tidak ditemukan!");
			location.href="'.base_url('sync_feed/aktifitas_mahasiswa/detail/'.$id_act).'";</script>';
		}

		$where = ['id_pembimbing' => $get_nid->nid, 'tahunajaran' =>  $this->session->userdata('tahunakademik_aktifitas')];
		$get_member = $this->db->get_where('tbl_verifikasi_krs', $where)->result();

		foreach ($get_member as $nim) {
			$is_npm_exist = $this->_is_member_has_added($nim->npm_mahasiswa, $id_act, FALSE);
			if ($is_npm_exist < 1) {
				$data[] = [
					'id_aktifitas' => $id_act,
					'npm' => $nim->npm_mahasiswa,
					'peran' => 3,
					'created_at' => date('Y-m-d H:i:s')
				];
			}
		}

		if (isset($data)) {
			$this->db->insert_batch('tbl_anggota_aktifitas', $data);
		} else {
			echo '<script>alert("Tidak ada anggota terbaru yang dapat ditambahkan!");
			location.href="'.base_url('sync_feed/aktifitas_mahasiswa/detail/'.$id_act).'";</script>';
		}
		echo '<script>alert("Berhasil menambahkan anggota!");
		location.href="'.base_url('sync_feed/aktifitas_mahasiswa/detail/'.$id_act).'";</script>';
	}

	public function remove_member($id_member, $id_act)
	{
		$this->db->update('tbl_anggota_aktifitas', ['deleted_at' => date('Y-m-d H:i:s')], ['id' => $id_member]);

		echo '<script>alert("Anggota berhasil dihapus!");
		location.href="'.base_url('sync_feed/aktifitas_mahasiswa/detail/'.$id_act).'";</script>';
	}

	public function save_pembimbing()
	{
		extract(PopulateForm());

		$this->_is_activity_exist(
					$id_aktifitas, 
					'Gagal menyimpan! Data aktifitas tidak ditemukan!', 
					'sync_feed/aktifitas_mahasiswa/detail/'.$id_aktifitas
				);

		if (isset($subact)) {
			$id_kegiatan = explode('@', $subact)[0];
			$nama_kegiatan = explode('@', $subact)[1];
		} else {
			$id_kegiatan = explode('@', $kegiatan)[0];
			$nama_kegiatan = explode('@', $kegiatan)[1];
		}

		$data = [
			'nid' => $nid,
			'nidn' => $hidden_nidn,
			'id_aktifitas' => $id_aktifitas,
			'id_kegiatan' => $id_kegiatan,
			'nama_kegiatan' => $nama_kegiatan,
			'no_urut_pembimbing' => $urut,
			'created_at' => date('Y-m-d H:i:s')
		];
		$this->db->insert('tbl_pembimbing_aktifitas', $data);

		echo '<script>alert("Pembimbing berhasil disimpan!");
		location.href="'.base_url('sync_feed/aktifitas_mahasiswa/detail/'.$id_aktifitas).'";</script>';
	}

	public function search_dosen()
	{
		$dsn = $this->act->get_dosen($_GET['term']);
		foreach ($dsn as $val) {
			$id_dosen = empty($val->nidn) || is_null($val->nidn) ? $val->nupn : $val->nidn;
			$data[] = [
				'nid' => $val->nid,
				'nidn' => $id_dosen,
				'value' => $id_dosen . ' - ' . $val->nama
			];
		}
		echo json_encode($data);
	}

	public function remove_guide($id)
	{
		$this->db->update('tbl_pembimbing_aktifitas', ['deleted_at' => date('Y-m-d H:i:s')], ['id' => $id]);

		echo '<script>alert("Pembimbing berhasil dihapus!");history.go(-1);</script>';
	}

	public function save_penguji()
	{
		extract(PopulateForm());

		$this->_is_activity_exist(
					$id_aktifitas, 
					'Gagal menyimpan! Data aktifitas tidak ditemukan!', 
					'sync_feed/aktifitas_mahasiswa/detail/'.$id_aktifitas
				);

		if (isset($subact)) {
			$id_kegiatan = explode('@', $subact)[0];
			$nama_kegiatan = explode('@', $subact)[1];
		} else {
			$id_kegiatan = explode('@', $kegiatan)[0];
			$nama_kegiatan = explode('@', $kegiatan)[1];
		}

		$data = [
			'nid' => $nid,
			'nidn' => $hidden_nidn,
			'id_aktifitas' => $id_aktifitas,
			'id_kegiatan' => $id_kegiatan,
			'nama_kegiatan' => $nama_kegiatan,
			'no_urut_penguji' => $urut,
			'created_at' => date('Y-m-d H:i:s')
		];
		$this->db->insert('tbl_penguji_aktifitas', $data);

		echo '<script>alert("Penguji berhasil disimpan!");
		location.href="'.base_url('sync_feed/aktifitas_mahasiswa/detail/'.$id_aktifitas).'";</script>';
	}

	public function remove_tester($id)
	{
		$this->db->update('tbl_penguji_aktifitas', ['deleted_at' => date('Y-m-d H:i:s')], ['id' => $id]);

		echo '<script>alert("Penguji berhasil dihapus!");history.go(-1);</script>';
	}

	public function sync_member($id)
	{
		$token  = $this->pddikti->get_token();
		$member = $this->act->get_member($id);

		foreach ($member as $val) {
			$id_reg_mhs = $this->_get_mhs($val->npm, $token);

			$data = [
				'id_aktivitas' => $val->id_aktifitas_feeder,
				'id_registrasi_mahasiswa' => $id_reg_mhs,
				'jenis_peran' => $val->peran
			];

			$payload = [
				"act" => "InsertAnggotaAktivitasMahasiswa",
				"token" => $token,
				"record" => $data
			];
			$exec = $this->pddikti->runWS($payload);
			$result = json_decode($exec);
			$result->npm = $val->npm;
			$result->status = $result->error_code == 0 ? "Sinkronisasi berhasil" : "Sinkronisasi gagal";
			if ($result->error_code == 0) {
				$this->db->update(
								'tbl_anggota_aktifitas', 
								['id_anggota_feeder' => str_replace("'", "", $result->data->id_anggota)], 
								['npm' => $val->npm, 'id_aktifitas' => $id]
							);
			}
			dd($result); echo '<hr>';
		}
	}

	private function _get_aktivitas_mhs($id, $token)
	{
		$act      = $this->db->get_where('tbl_aktifitas_mhs', ['id' => $id])->row()->no_sk_tugas;
		$id_prodi = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
		$semester = $this->session->userdata('tahunakademik_aktifitas');

		$payload = [
			"act" => "GetListAktivitasMahasiswa",
			"token" => $token,
			"filter" => "sk_tugas = '$act' and id_prodi = '$id_prodi' and id_semester = '$semester'"
		];
		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec)->data[0]->id_aktivitas;
		return $result;
	}

	private function _get_mhs($npm, $token)
	{
		$payload = [
			"act" => "GetListMahasiswa",
			"token" => $token,
			"filter" => "nim = '$npm'"
		];
		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec)->data[0]->id_registrasi_mahasiswa;
		return $result;
	}

	public function sync_guide($id)
	{
		$token  = $this->pddikti->get_token();
		$guide  = $this->act->get_guide($id);

		foreach ($guide as $val) {
			$id_dosen = $this->_get_dsn($val->nidn, $token);

			$record = [
				'id_aktivitas' => $val->id_aktifitas_feeder,
				'id_kategori_kegiatan' => $val->id_kegiatan,
				'id_dosen' => $id_dosen,
				'pembimbing_ke' => $val->no_urut_pembimbing
			];

			$payload = [
				"act" => "InsertBimbingMahasiswa",
				"token" => $token,
				"record" => $record
			];
			$exec = $this->pddikti->runWS($payload);
			$result = json_decode($exec);
			$result->nidn = $val->nidn;
			$result->status = $result->error_code == 0 ? "Sinkronisasi berhasil" : "Sinkronisasi gagal";
			if ($result->error_code == 0) {
				$this->db->update(
								'tbl_pembimbing_aktifitas', 
								['id_bimbing_feeder' => str_replace("'", "", $result->data->id_bimbing_mahasiswa)], 
								['id' => $val->id]
							);
			}
			dd($result); echo '<hr>';
		}
	}

	private function _get_dsn($nidn, $token)
	{
		$payload = [
			"act" => "GetListDosen",
			"token" => $token,
			"filter" => "nidn = '$nidn'"
		];
		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec)->data[0]->id_dosen;
		return $result;
	}

	public function sync_examiner($id)
	{
		$token  = $this->pddikti->get_token();
		$exam   = $this->act->get_examiner($id);

		foreach ($exam as $val) {
			$id_dosen = $this->_get_dsn($val->nidn, $token);

			$record = [
				'id_aktivitas' => $val->id_aktifitas_feeder,
				'id_kategori_kegiatan' => $val->id_kegiatan,
				'id_dosen' => $id_dosen,
				'penguji_ke' => $val->no_urut_penguji
			];

			$payload = [
				"act" => "InsertUjiMahasiswa",
				"token" => $token,
				"record" => $record
			];
			$exec = $this->pddikti->runWS($payload);
			$result = json_decode($exec);
			$result->nidn = $val->nidn;
			$result->status = $result->error_code == 0 ? "Sinkronisasi berhasil" : "Sinkronisasi gagal";
			if ($result->error_code == 0) {
				$this->db->update(
								'tbl_penguji_aktifitas', 
								['id_penguji_feeder' => str_replace("'", "", $result->data->id_uji)], 
								['id' => $val->id]
							);
			}
			dd($result); echo '<hr>';
		}
	}
}

/* End of file Aktifitas_mahasiswa.php */
/* Location: ./application/modules/sync_feed/controllers/Aktifitas_mahasiswa.php */