<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);

class Krs extends CI_Controller {

	protected $userid, $user_group;

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		ini_set('memory_limit', '2048M');
		$this->load->model('feeder_model');
		if ($this->session->userdata('sess_login') == TRUE) {
			$cekakses = $this->role_model->cekakses(71)->result();
			if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}
		} else {
			redirect('auth','refresh');
		}

		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->user_group = $this->session->userdata('sess_login')['id_user_group'];
	}

	public function index()
	{
		$this->session->unset_userdata('session_akm');
		$data['tahunakademik'] = $this->app_model->getdata('tbl_tahunakademik','kode','asc')->result();
		$data['page'] = 'v_krs_select';
		$this->load->view('template/template', $data); 
	}

	public function change_mode($flag)
	{
		$this->db->where('kd_prodi', $this->userid);
		$this->db->update('panel_switch', ['feedkhs' => $flag]);
		echo "<script>alert('Mode berhasil diubah!');
		document.location.href='".base_url()."sync_feed/krs/akm_list';</script>";
	}

	public function set_filter()
	{
		$array = array(
			'angkatan' => $this->input->post('angkatan'),
			'tahunakademik' => $this->input->post('tahunakademik')
		);
		
		$this->session->set_userdata('session_akm', $array);
		redirect(base_url('sync_feed/krs/akm_list'));
	}

	public function akm_list()
	{
		$this->load->model('feeder_perkuliahan_model','feedkuliah');
		$actyear            = getactyear();
		$yearbefore         = yearBefore();
		$data['lastyear']   = $yearbefore;
		$data['activeyear'] = $actyear;

		$group  = get_group($this->user_group);

		if (in_array(8, $group) || in_array(19, $group)) {
			$prodi = $this->userid;
		} else {
			$prodi = $this->input->post('jurusan', TRUE);
		}

		$angkatan           = $this->session->userdata('session_akm')['angkatan'];
		$tahunakademik      = $this->session->userdata('session_akm')['tahunakademik'];
		$data['chooseyear'] = $tahunakademik;
		$data['angkatan']   = $angkatan;
		
		if ($tahunakademik == $yearbefore) {
			$data['getData'] = $this->feedkuliah->get_akm($tahunakademik, $prodi, $angkatan);
			$data['page'] = 'v_krs_list';
		} else {
			$data['getData'] = $this->feedkuliah->get_krs($angkatan, $this->userid, $tahunakademik);
			$data['page'] = 'v_krs_list2';
		}

		$this->load->view('template/template',$data);
	}

	public function view_krs($id)
	{
		$data['pembimbing']=$this->app_model->get_pembimbing_krs($id)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa($id)->result();
		$data['kd_krs'] = $id;
		$data['page'] = 'akademik/viewkrs_detail';
		$this->load->view('template/template',$data);
	}

	public function getmhs()
    {
        $this->db->select("*");
        $this->db->from('tbl_krs');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.NIMHSMSMHS = tbl_krs.npm_mahasiswa');
        $this->db->where('jabatan_id', 8);
        $this->db->like('nama', $_GET['term'], 'both');
        $sql  = $this->db->get();
        
        $data = [];
        foreach ($sql->result() as $row) {
            $data[] = [
						'nid'   => $row->nid,
						'value' => $row->nama,
                    ];
        }

        echo json_encode($data);
    }

    public function viewkrsmhs($id)
    {
    	if (ctype_digit($id)) {
			$primary = $id;
		} else {
			$idkd = $this->app_model->getdetail('tbl_verifikasi_krs','slug_url',$id,'npm_mahasiswa','asc')->row();
			$primary = $idkd->npm_mahasiswa;
		}
		$data['slugcoy'] = $this->app_model->getdetail('tbl_verifikasi_krs','npm_mahasiswa',$primary,'npm_mahasiswa','asc')->row();
		$data['krs'] = $this->app_model->get_all_krs_mahasiswa($primary)->result();
		$data['npm'] = $primary;
		
		$data['page'] = 'v_krs_detailmhs';
		$this->load->view('template/template',$data);
    }

    public function view_krs_dtl($id)
    {
		$getactyear = getactyear();
		$logged     = $this->session->userdata('sess_login');
		$thn        = substr($id, 12,5);
		$nim        = substr($id, 0,12);

    	$data['npm'] = $nim;

		//tab1
    	$data['pembimbing'] = $this->app_model->get_pembimbing_krs($id)->row_array();
		$data['detail_krs'] = $this->app_model->get_detail_krs_mahasiswa_feeder($id)->result();

		//tab2
		$aa = substr($thn, 0,4);
		$bb = substr($thn, -1);

		if ($bb == 2) {
			$cc = $aa.'1';
		} else{
			$cc = ($aa-1).'2';
		}

		$data['mhs']           = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		$data['semester']      = $this->app_model->get_semester_khs($data['mhs']->SMAWLMSMHS,$id);
		$data['tahunakademik'] = $thn;
		$kode_prodi            = $data['mhs']->KDPSTMSMHS;
		$kode_krs              = $nim.$cc;

		if ($thn < '20151') {
			$data['detail_khs'] = $this->db->query("SELECT DISTINCT 
														b.kd_matakuliah,
														b.nama_matakuliah,
														b.sks_matakuliah 
													FROM tbl_transaksi_nilai a 
													JOIN tbl_matakuliah_copy b ON a.KDKMKTRLNM = b.kd_matakuliah 
													WHERE a.THSMSTRLNM = '$cc' AND b.tahunakademik = '$cc' 
													AND a.NIMHSTRLNM = '$nim' 
													AND b.kd_prodi = '$kode_prodi'")->result();

			$data['kode'] = $this->db->query("SELECT DISTINCT kd_krs FROM tbl_krs WHERE kd_krs LIKE '$kode_krs%'")->row();

		} else {
			$data['detail_khs'] = $this->db->query("SELECT DISTINCT 
														a.*,
														b.nama_matakuliah,
														b.sks_matakuliah 
													FROM tbl_krs a 
													JOIN tbl_matakuliah b ON a.kd_matakuliah = b.kd_matakuliah 
													WHERE a.kd_krs LIKE '$kode_krs%' 
													AND b.kd_prodi = '$kode_prodi'")->result();

			$data['kode'] = $this->db->query("SELECT DISTINCT kd_krs FROM tbl_krs WHERE kd_krs LIKE '$kode_krs%'")->row();
		}

		//view
		if ($thn == $getactyear) {
			$data['page'] = 'sync_feed/v_krs_smtr_detail';
		} else {
			$data['page'] = 'sync_feed/v_krs_smtr_detail2';
		}
		$data['kd_krs'] = $id;
		$data['ta_khs'] = $cc;
		
		$this->load->view('template/template',$data);
    }

    public function get_jadwal()
    {
		$kd_matakuliah = $_POST['kd_matakuliah'];
		$user  = $this->session->userdata('sess_login');
		$prodi = $user['username'];
		$mhs   = $this->app_model->get_jurusan_mhs($npm)->row();
		$data  = $this->app_model->get_pilih_jadwal_feeder($kd_matakuliah,$prodi,$this->session->userdata('ta'))->result();
		$js    = json_encode($data);
		echo $js;
	}

	public function in()
	{
		$total_mhs = count($this->input->post('mhs'));
		$ta        = $this->session->userdata('session_akm')['tahunakademik'];
		$npm       = $this->input->post('mhs',TRUE);
		$this->db->where_in('npm_mahasiswa', $npm);
		$this->db->where('tahunajaran', $ta);
		$this->db->delete('tbl_khs');

		foreach ($_POST["mhs"] as $key => $value) {
			$nim = $value;
			$ips = $_POST["ips"][$key];
			$totsks = $_POST["sks"][$key];
			$ipk = $_POST["ipk"][$key];
			$sks_smt = $_POST["smt"][$key];		

			$datax[] = [
					'npm_mahasiswa' => $nim, 
					'tahunajaran' 	=> $ta, 
					'ips' 			=> $ips, 
					'total_sks' 	=> $totsks, 
					'ipk' 			=> $ipk
				];

			$param = ['THSMSTRAKM' => $ta, 'NIMHSTRAKM' => $nim];
			$is_data_available = $this->db->get_where('tbl_aktifitas_kuliah_mahasiswa', $param)->num_rows();
		
			if ($is_data_available > 0) {
				$datas = [
					'SKSTTTRAKM' => $totsks,
					'NLIPKTRAKM' => $ipk 
				];
				$this->db->where('NIMHSTRAKM', $nim);
				$this->db->where('THSMSTRAKM', $ta);
				$this->db->update('tbl_aktifitas_kuliah_mahasiswa', $datas);
				
			} else {
				$data_akm = [
					'THSMSTRAKM' => $ta,
					'KDPTITRAKM' => '031036',
					'KDJENTRAKM' => 'C',
					'KDPSTTRAKM' => $this->userid,
					'NIMHSTRAKM' => $nim,
					'SKSEMTRAKM' => $sks_smt,
					'NLIPSTRAKM' => $ips,
					'SKSTTTRAKM' => $totsks,
					'NLIPKTRAKM' => $ipk
				];
				$this->db->insert('tbl_aktifitas_kuliah_mahasiswa', $data_akm);
			}
		}

		$this->db->insert_batch('tbl_khs',$datax);
		
		echo "<script>alert('Berhasil!');document.location.href='".base_url()."sync_feed/krs/akm_list';</script>";
	}

	public function cetak_khs()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup))) {
			$prodi = $logged['userid'];
		} else {
			$prodi = $this->input->post('jurusan', TRUE);
		}

		$data['angkatan'] = $tahun;
		$this->db->distinct();
		$this->db->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS,b.kd_krs,b.tahunajaran');
		$this->db->from('tbl_mahasiswa a');
		$this->db->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db->where('a.KDPSTMSMHS', $logged['userid']);
		$this->db->where('a.TAHUNMSMHS >', '2011');
		$this->db->where('a.TAHUNMSMHS <', '2016');
		$this->db->where('b.tahunajaran', $this->session->userdata('ta'));
		$this->db->order_by('a.NIMHSMSMHS', 'asc');
		$data['getData'] =  $this->db->get()->result();

		$this->load->view('feed_excel_khs',$data);
	}

	public function sync_akm()
    {
    	$this->load->library('pddikti');
		$token = $this->pddikti->get_token();

		foreach ($this->input->post('mhs') as $key => $value) {
			$mhs = $this->input->post('mhs['.$value.']');
            $ips = $this->input->post('ips['.$value.']');
            $ipk = $this->input->post('ipk['.$value.']');
            $smt = $this->input->post('smt['.$value.']');
            $sks = $this->input->post('sks['.$value.']');
            $fee = $this->input->post('fee['.$value.']');

            if ($sks != 0) {
                #insert
                $id_reg_pd = $this->_get_mahasiswa_feeder($token, $mhs); 

				$record['id_status_mahasiswa']     = 'A';
				$record['ips']                     = $ips;
				$record['ipk']                     = $ipk;
				$record['sks_semester']            = $smt;
				$record['total_sks']               = $sks;

                $payload = [
					'act'    => 'UpdatePerkuliahanMahasiswa',
					'token'  => $token,
					'key' 	 => [
						'id_registrasi_mahasiswa' => json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa,
						'id_semester' => $this->session->userdata('session_akm')['tahunakademik']
					],
					'record' => $record
                ];

                $result = $this->pddikti->runWS($payload);
                $custom_result = json_decode($result);
                $custom_result->npm = $mhs;
                $custom_result->nama = $this->input->post('nama['.$value.']');
                $custom_result->status = ($custom_result->error_code == 0) ? "Sinkronisasi berhasil!" : "Sinkronisasi gagal!";
                if ($custom_result->error_code == 0) {
                	$this->_update_sync_status($this->session->userdata('session_akm')['tahunakademik'], $mhs);
                }
                dd($custom_result);
                echo "<hr>";
            }
		}
    }

    public function sync_akm_satuan($npm)
    {
    	$this->load->library('pddikti');
    	$token = $this->pddikti->get_token();
    	$id_reg_pd = $this->_get_mahasiswa_feeder($token, $npm);

    	$get_akm = $this->db->get_where('tbl_aktifitas_kuliah_mahasiswa', [
    		'NIMHSTRAKM' => $npm, 
    		'THSMSTRAKM' => $this->session->userdata('session_akm')['tahunakademik']
    	])->row();
		
		$record['id_status_mahasiswa'] = 'A';
		$record['ips']                 = $get_akm->NLIPSTRAKM;
		$record['ipk']                 = $get_akm->NLIPKTRAKM;
		$record['sks_semester']        = $get_akm->SKSEMTRAKM;
		$record['total_sks']           = $get_akm->SKSTTTRAKM;

		$payload = [
			'act'    => 'UpdatePerkuliahanMahasiswa',
			'token'  => $token,
			'key' 	 => [
				'id_registrasi_mahasiswa' => json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa,
				'id_semester' => $this->session->userdata('session_akm')['tahunakademik']
			],
			'record' => $record
        ];

        $result = $this->pddikti->runWS($payload);
        $custom_result = json_decode($result);
        $custom_result->npm = $npm;
        $custom_result->status = ($custom_result->error_code == 0) ? "Sinkronisasi berhasil!" : "Sinkronisasi gagal!";
        if ($custom_result->error_code == 0) {
        	$this->_update_sync_status($this->session->userdata('session_akm')['tahunakademik'], $npm);
        }
        dd($custom_result);
    }

    protected function _update_sync_status($tahun, $npm)
    {
		$data  = [ 'has_sync' => 1 ];
		$where = ['THSMSTRAKM' => $tahun, 'NIMHSTRAKM' => $npm];
		$this->db->update('tbl_aktifitas_kuliah_mahasiswa', $data, $where);
		return;
    }

    private function _get_mahasiswa_feeder($token, $npm)
    {
    	$this->load->library('pddikti');
        $payload2 = [
            'act'    => 'GetListMahasiswa',
            'token'  => $token,
            'filter' => "nim = '$npm'" 
        ];
        $mahasiswa = $this->pddikti->runWS($payload2);
        return $mahasiswa;
    }

    public function sync_status_aktif()
    {
    	extract(PopulateForm());
    	$this->load->library('pddikti');
    	$academic_year = $this->session->userdata('session_akm')['tahunakademik'];
		$token = $this->pddikti->get_token();

        foreach ($npm as $key => $value) {
	        $id_reg_pd = $this->_get_mahasiswa_feeder($token, $npm[$value]);

			$record['id_semester']             = $academic_year;
			$record['id_registrasi_mahasiswa'] = json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa;
			$record['id_status_mahasiswa']     = 'A';
			$record['sks_semester']            = $sks[$value];
			$record['biaya_kuliah_smt']        = (int)$fee[$value];

			$payload = [
				'act'    => 'InsertPerkuliahanMahasiswa',
				'token'  => $token,
				'record' => $record
			];

	        $exec = $this->pddikti->runWS($payload);
	        $result = json_decode($exec);
	        $result->npm = $npm[$value];
	        $result->nama = $nama[$value];
	        if ($result->error_code == 0) {
	        	$result->status = "Sinkronisasi berhasil!";
	        } else {
	        	$result->status = "Sinkronisasi gagal!";
	        }
	        dd($result);
	        echo "<hr>";	
        }
    }

}

/* End of file Krs.php */
/* Location: ./application/controllers/Krs.php */