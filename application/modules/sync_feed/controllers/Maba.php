<?php
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once APPPATH .'third_party/Requests/library/Requests.php';
Requests::register_autoloader();

class Maba extends CI_Controller {
    public $user_id;
    public $max_angkatan;
    public $header = array('Accept' => 'application/json', 'x-feeder-key' => 'yd2smg');

	public function __construct()
	{
		parent::__construct();
       
        ini_set('memory_limit', '512M');
        // $this->db2 = $this->load->database('regis',TRUE);
		if ($this->session->userdata('sess_login') == TRUE) {
			$akses = $this->role_model->cekakses(128)->result();
			if ($akses != TRUE) {
				redirect('home','refresh');
			}
		} else {
			redirect('auth','refresh');
		}

        $this->user_id = $this->session->userdata('sess_login')['userid'];

        if (!$this->cache->memcached->get('max_angkatan')) {
            $this->_set_angkatan();
        }

        $this->max_angkatan = $this->cache->memcached->get('max_angkatan');
	}

    function _set_angkatan()
    {
        $host = API_NEW_SIA_FEEDER ."/tahun_akademik";
        $res = Requests::get($host, $this->header);  
       
        if ($res->status_code == 200) {
            $body = json_decode($res->body, true);
            $max_angkatan = max(array_column($body['data'], 'kode'));
            $this->cache->memcached->save('max_angkatan', $max_angkatan, 3600);
        }
    }

	function index()
	{

        $this->load->model('feeder_model', 'feedermod');

        $maba = [];
        $key = 'maba_'.$this->user_id;
        
        $this->_is_cached_maba_exist($key);

        $maba = $this->cache->memcached->get($key);
        $data['look'] = $maba;
        $data['page'] = "feed_maba";
        
        $this->load->view('template/template', $data);

	}

	function id_maba($id)
	{
		$data['query'] = $this->db->query("SELECT * from tbl_form_camaba where nomor_registrasi = '".$id."'")->row();
		$data['page'] = "feed_idmaba";
		$this->load->view('template/template', $data);
	}

	function inputdata()
	{
		if ($this->input->post('jk') == 'L') {
			$lamin = '1';
		} else {
			$lamin = '2';
		}

		$fak = $this->db->query("SELECT * from tbl_form_camaba where npm_baru = '".$this->input->post('npm')."'")->row();
		$jur = $fak->prodi;
		$haha= $this->db->query("SELECT * from tbl_jurusan_prodi where kd_prodi = '".$jur."'")->row();
		$faks= $haha->kd_fakultas;
		
		$data = array(
			'nomor_pokok' => $this->input->post('npm'),
			'nama' => $this->input->post('nama'),
			'jenis_kelamin' => $lamin,
			'alamat' => $this->input->post('alamat'),
			'rt' => $this->input->post('rt'),
			'rw' => $this->input->post('rw'),
			'kelurahan' => $this->input->post('kel'),
			'kecamatan' => $this->input->post('kec'),
			'provinsi' => $this->input->post('prov'),
			'handphone' => $this->input->post('hp'),
			'email' => $this->input->post('email'),
			'ayah' => $this->input->post('ayah'),
			'ibu' => $this->input->post('ibu'),
			'fakultas' => $faks,
			'jurusan' => $jur
			);
		//var_dump($data);exit();
		$this->db->insert('tbl_biodata', $data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."sync_feed/maba';</script>";
	}

	function sync_maba_feed()
    {
        echo "<script>alert('Fitur sedang dalam perbaikan!!');history.go(-1);</script>";
        // $logged = $this->session->userdata('sess_login');

        // $max_angkatan = $this->db->query("SELECT max(TAHUNMSMHS) as max from tbl_mahasiswa order by id_mhs desc limit 1")->row()->max;


        $maba = [];
        $key = 'maba_'.$this->user_id;
        
        $this->_is_cached_maba_exist($key);

        $maba = $this->cache->memcached->get($key);

        
        // $cari   = $this->db->query("SELECT NIMHSMSMHS as npm from tbl_mahasiswa 
        //                             where KDPSTMSMHS = '".$logged['userid']."' 
        //                             and TAHUNMSMHS = '".$max_angkatan."' ")->result();
        
        $this->load->library("Nusoap_lib");
        // // $url	= 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url 	= 'http://feeder.ubharajaya.ac.id/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        // $url     = 'http://feeder.ubharajaya.ac.id/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $table 	= 'mahasiswa';

        foreach ($maba as $value) {
            // get tahun akademik pertama masuk
            $smawl = get_smawl($value->npm, $this->user_id);

            //1 = islam , 2 = kristen , 3 = katolik , 4 = hindu , 5 = budha , 6 = konghucu, 99 = lainnya
            $agm = $value->agama == 7 ? 99 : $value->agama ;
        	
            $alm  = substr($value->jalan, 0,60);
            $almt = strval($alm);

            $record['id_pd']                     = '';
            $record['nm_pd']                     = $value->nama;
			$record['jk']                        = $value->jenis_kelamin;
			$record['nisn']                      = '0';
			$record['nik']                       = str_replace(' ', '', $value->nik);
			$record['id_agama']                  = $agm;
			$record['id_kk']                     = '0';
			$record['id_wil']                    = '022200'; //kab. bekasi
			//$record['id_sp'] = $result2['result']['id_sp'];
			$record['nm_ayah']                   = $value->nama_ayah;
			$record['nm_ibu_kandung']            = $value->nama_ibu;
			$record['id_kebutuhan_khusus_ibu']   = 0;
			$record['id_kebutuhan_khusus_ayah']  = 0;
			$record['kewarganegaraan']           = 'ID';//ID = indonesia
			$record['a_terima_kps']              = 0;
			$record['ds_kel']                    = $almt;
			$record['tgl_lahir']                 = $value->tgl_lahir;
			$record['tmpt_lahir']                = $value->tpt_lahir;
			$record['no_hp']                     = $value->handphone;
			$record['email']                     = $value->email;

			//var_dump($record);exit();
            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            dd($result1['result']);
            
            $filter2 = "nik ilike '".str_replace(' ', '', $maba->nik)."%'";
            $dbb     = 'mahasiswa_pt';
            $limit   = 5;
            $search  = $proxy->GetRecord($token,'mahasiswa',$filter2);
            $sp      = $proxy->GetRecord($token, 'satuan_pendidikan', "nm_lemb ilike '%bhayangkara jakarta%'");
            $que = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$this->user_id,'prodi','asc')->row();

            $record2['id_pd']            = $search['result']['id_pd'];
            $record2['nipd']             = trim($value->npm);
			$record2['id_sp']            = $sp['result']['id_sp'];
			$record2['id_sms']           = $que->id_sms;
			$record2['tgl_masuk_sp']     = '2020-02-28';
			$record2['a_pernah_paud']    = '0';
			$record2['a_pernah_tk']      = '0';
			$record2['tgl_create']       = date('Y-m-d');
			$record2['mulai_smt']        = $smawl;
            $record2['biaya_masuk_kuliah'] = "8500000";

            if ($logged['userid'] == '74101' or $logged['userid'] == '61101') {
                if (substr(trim($value->npm), 7,1) == 5) {
                    $record2['id_jns_daftar'] = 1;
                } else {
                    $record2['id_jns_daftar'] = 2;
                }
            } else {
                if (substr(trim($maba->npm_baru), 8,1) == 5) {
                    $record2['id_jns_daftar'] = 1;
                } else {
                    $record2['id_jns_daftar'] = 2;
                }
            }

			// $hasil = $proxy->InsertRecord($token,$dbb,json_encode($record2));
			dd($hasil['result']);
            echo "<hr>";

            // $this->db2->query("UPDATE tbl_form_pmb set status_feeder = 1 where npm_baru = '".$maba->npm_baru."'");
        }
    }

    public function _is_cached_maba_exist($key)
    {
        if (!$this->cache->memcached->get($key)) {
            $this->_get_maba($key);
        }
    }

    public function _get_maba($key, $ttl = 3600)
    {
        $this->load->model('Feeder_model', 'feedermod');
        $res = $this->feedermod->get_maba($this->max_angkatan, $this->user_id)->result();
        $this->cache->memcached->save($key, $res, $ttl);
    }
}

/* End of file Maba.php */
/* Location: ./application/modules/sync_feeder/controllers/Maba.php */