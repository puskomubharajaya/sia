<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Khs extends CI_Controller {

function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->load->library('Cfpdf');
		//if ($this->session->userdata('sess_login') == TRUE) {
			//$cekakses = $this->role_model->cekakses(61)->result();
			/*if ($cekakses != TRUE) {
				echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			}*/
		//} else {
			//redirect('auth','refresh');
		//}
	}

	public function index()
	{
		$data['page'] = "feed_khs";
		$this->load->view('template/template', $data);
	}

	function viewkhsprodi()
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(8, $grup))) {
			$prodi = $logged['userid'];
		} else {
			$prodi = $this->input->post('jurusan', TRUE);
		}
		$tahun = $this->input->post('tahun', TRUE);
		$this->session->set_userdata('tahun_ak', $tahun);
		$data['getData']=$this->db->query("SELECT * FROM tbl_mahasiswa WHERE TAHUNMSMHS = '".$tahun."' AND KDPSTMSMHS = '".$prodi."' ")->result();
		$data['page'] = 'feed_list_khs';
		$this->load->view('template/template',$data);
	}

	function viewkhs($id)
	{
		$logged = $this->session->userdata('sess_login');
		$pecah = explode(',', $logged['id_user_group']);
		$jmlh = count($pecah);
		for ($i=0; $i < $jmlh; $i++) { 
			$grup[] = $pecah[$i];
		}
		if ((in_array(5, $grup))) {
			$nim = $logged['userid'];
		} else {
			$nim = $id;
		}
		//nurfan
		$data['krs_by_smtr'] = $this->db->query('SELECT * FROM tbl_verifikasi_krs vkhs
													LEFT JOIN tbl_krs khs ON vkhs.`kd_krs` = khs.`kd_krs`
													WHERE khs.`npm_mahasiswa` = "'.$nim.'"')->result();
		//var_dump($data['krs_by_smtr']);die();
		//$data['ipk'] = $this->app_model->get_ipk_mahasiswa($nim)->row()->ipk;
		$data['mhs'] = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$nim,'NIMHSMSMHS','asc')->row();
		$data['detail_khs'] = $this->app_model->get_all_khs_mahasiswa_ii($nim)->result();
		$data['page'] = 'feed_khs_mhs';
	    $this->load->view('template/template', $data);
	}

}

/* End of file Khs.php */
/* Location: ./application/modules/sync_feed/controllers/Khs.php */