<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

    private $userid;

	public function __construct()
	{
		parent::__construct();
        ini_set('memory_limit', '1024M');
	    if ($this->session->userdata('sess_login') == TRUE) {
            $cekakses = $this->role_model->cekakses(123)->result();
            if ($cekakses != TRUE) {
                echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
            }
        } else {
            redirect('auth','refresh');
        }
        $this->load->model('feeder_perkuliahan_model','feedkuliah');
        $this->load->library('pddikti');
        $this->userid = $this->session->userdata('sess_login')['userid'];
	}

	public function index()
	{
        $this->session->unset_userdata('tahunajaran_kelas');
		$data['tahunajar'] = $this->app_model->getdata('tbl_tahunakademik', 'kode', 'ASC')->result();
		$data['page'] = 'feed_kelas';
		$this->load->view('template/template', $data);
	}

	public function get_jurusan($id)
    {
		$jrs = explode('-',$id);
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $jrs[0], 'id_prodi', 'ASC')->result();
		$out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Program Studi--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."-".$row->prodi."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	public function save_session_prodi()
    {    
        $tahunajaran = $this->input->post('tahunajaran');
        $this->session->set_userdata('tahunajaran_kelas', $tahunajaran);        
        redirect(base_url('sync_feed/kelas/view_matakuliah'));
    }

    public function view_matakuliah()
    {
        $userid = $this->userid;
        $tahunakademik = $this->session->userdata('tahunajaran_kelas');

        if ($userid == '61101' || $userid == '74101') {
            $data['tabs'] = $this->feedkuliah->get_tabs_magister();
            $data['page'] = "v_kelas_list_magister";
        } else {
            $data['tabs']  = $this->feedkuliah->get_tabs();
            $data['page']  ='v_kelas_list';  
        }
        
		$this->load->view('template/template', $data);
	}

    function jmlPeserta($id)
    {
        $kdjdl = get_kd_jdl_feed($id);
        $this->db->select('count(distinct kd_krs) as jml');
        $this->db->from('tbl_krs_feeder');
        $this->db->where('kd_jadwal', $kdjdl);
        $data['jml'] = $this->db->get()->row()->jml;
        $this->load->view('jumlah_peserta_modal', $data);
    }


	function detail_mhs($id)
	{
        $this->db->where('id_jadwal', $id);
        $jadwal = $this->db->get('tbl_jadwal_matkul_feeder')->row();
     
        $this->session->set_userdata('kd_jadwal', $jadwal->kd_jadwal);
        $this->session->set_userdata('kd_matakuliah', $jadwal->kd_matakuliah);
        $this->session->set_userdata('id_jadwal', $id);
       
        $data['th'] = $this->session->userdata('tahunajaran_kelas');

        $this->db->distinct();
		$this->db->select('npm_mahasiswa,kd_jadwal');
		$this->db->from('tbl_krs_feeder');
		$this->db->where('kd_jadwal', $jadwal->kd_jadwal);
        $this->db->order_by('npm_mahasiswa', 'asc');
		$data['peserta'] = $this->db->get()->result();
        
		$data['nama_kelas'] = $jadwal->kelas;
		$data['jdl_mk'] = $jadwal->kd_jadwal;

		$data['page'] = "v_kelas_detail";
		$this->load->view('template/template', $data);
	}

	function load_mhs()
	{
        $this->db->distinct();
        $this->db->select('id_mhs,NIMHSMSMHS,NMMHSMSMHS');
        $this->db->from('tbl_mahasiswa');
        $this->db->like('NIMHSMSMHS', $_GET['term'], 'both');
        $this->db->or_like('NMMHSMSMHS', $_GET['term'], 'both');
        $sql  = $this->db->get();

        $data = array();
        foreach ($sql->result() as $row) {
            $data[] = [
        				'id'	=> $row->id_mhs,
                        'nim'   => $row->NIMHSMSMHS,
                        'value' => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS
                    ];
        }
        echo json_encode($data);
    }

    function tambah_mhs()
    {
        $this->load->view('pindahan_kelas');
    }

    function edit_jadwal($id)
    {
        $data['kode_prodi'] = $this->userid;
        $data['rows'] = $this->db->query('SELECT * FROM tbl_jadwal_matkul_feeder mk
                                LEFT JOIN tbl_matakuliah a ON mk.`id_matakuliah`=a.`id_matakuliah`
                                left JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                WHERE mk.`id_jadwal` = '.$id.'')->row();
    
        $this->load->view('v_kelas_edit', $data);
    }

    function update_kelas()
    {
        $mhs = $this->input->post('student');
        $exp = explode('-', $mhs);
        $npm = trim($exp[0]);

        $q = $this->db->query("SELECT kd_krs FROM tbl_krs_feeder
                                WHERE kd_krs like '".$npm.$this->session->userdata('tahunajaran_kelas')."%'
                                AND npm_mahasiswa = '".$npm."' 
                                AND kd_matakuliah = '".$this->session->userdata('kd_matakuliah')."'
                                limit 1")->row();

        $data = array('kd_jadwal' => $this->session->userdata('kd_jadwal'));

        $this->db->where('kd_krs', $q->kd_krs);
        $this->db->where('kd_matakuliah', $this->session->userdata('kd_matakuliah'));
        $this->db->update('tbl_krs_feeder', $data);

        $this->db->where('kd_krs', $q->kd_krs);
        $this->db->where('kd_matakuliah', $this->session->userdata('kd_matakuliah'));
        $this->db->update('tbl_nilai_detail_feeder', $data);

        echo "<script>history.go(-1);</script>";
    }

    function delete_jadwal($npm)
    {
        $object = array('kd_jadwal' => NULL );

        $this->db->where('kd_jadwal',$this->session->userdata('kd_jadwal'));
        $this->db->where('npm_mahasiswa',$npm);
        $this->db->update('tbl_krs_feeder', $object);

        redirect('sync_feed/kelas/detail_mhs/'.$this->session->userdata('id_jadwal'),'refresh');

    }

    public function load_dosen($idk)
    {
        $data['jadwal'] = $this->db->where('id_jadwal',$idk)->get('tbl_jadwal_matkul_feeder')->row();
        $data['dosen']  = $this->db->get('tbl_karyawan')->result();
		$this->load->view('penugasan_dosen',$data);
	}

	public function update_dosen()
    {
        $dosen = $this->input->post('kd_dosen');
        $id    = $this->input->post('id_jadwal');
        $data  = ['kd_dosen' => $dosen];

        $sks = $this->db->query("SELECT mk.`sks_matakuliah` FROM tbl_jadwal_matkul_feeder jd
                                JOIN tbl_matakuliah mk ON jd.`id_matakuliah` = mk.`id_matakuliah`
                                WHERE jd.`id_jadwal` = '{$id}'
                                AND mk.`id_matakuliah` = jd.`id_matakuliah`")->row()->sks_matakuliah;

        $isCreditEnough = $this->db->query("SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul_feeder jd
                                            JOIN tbl_matakuliah mk ON jd.`id_matakuliah` = mk.`id_matakuliah`
                                            WHERE mk.`mk_ta` IS NULL
                                            AND jd.`kd_tahunajaran` = '{$this->session->userdata('tahunajaran_kelas')}'
                                            AND jd.`kd_dosen` = '{$dosen}'")->row()->sks;

        if (($isCreditEnough+$sks) > 18) {
            echo "<script>alert('SKS dosen melebihi ketentuan!');location.href='".base_url('sync_feed/kelas/view_matakuliah')."';</script>";
            exit();
        }

        $this->db->where('id_jadwal',$id)->update('tbl_jadwal_matkul_feeder', $data);
        redirect('sync_feed/kelas/view_matakuliah','refresh');
    }

    public function update_data_matakuliah()
    {
        date_default_timezone_set("Asia/Jakarta"); 
        extract(PopulateForm());

        $data = [
                'kelas'         => $kelas,
                'id_matakuliah' => $id_matakuliah,
                'kd_matakuliah' => $kd_matakuliah,
                'waktu_kelas'   => $st_kelas,
            ];
        $this->db->where('id_jadwal', $id_jadwal)->update('tbl_jadwal_matkul_feeder',$data);

        redirect(base_url('sync_feed/kelas/view_matakuliah'));
    }

    public function save_data()
    {
        date_default_timezone_set("Asia/Jakarta"); 
        extract(PopulateForm());      

        $kode_prodi      = $this->userid;
        $kode_jadwal     = $this->_generate_schedule_code($this->userid);
        $session         = $this->session->userdata('sess_login');
        $user            = $session['username'];

        $data = array(
            'kelas'         => $kelas,
            'waktu_kelas'   => $st_kelas,
            'kd_jadwal'     => $kode_jadwal,
            'id_matakuliah' => $id_matakuliah,
            'kd_matakuliah' => $kd_matakuliah,
            'kd_tahunajaran'=> $this->session->userdata('tahunajaran_kelas'),
            'audit_date'    => date('Y-m-d H:i:s'),
            'audit_user'    => $user
            );
        $this->db->insert('tbl_jadwal_matkul_feeder', $data);

        redirect('sync_feed/kelas/view_matakuliah','refresh');
    }

    private function _generate_schedule_code($kode_prodi)
    {
        $gpass = NULL;
        $n = 6; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";

        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $gpass .=substr($chr, $rIdx, 1);
        }
         
        $list_dept_schedule = $this->db->like('kd_jadwal',$kode_prodi)->get('tbl_jadwal_matkul_feeder')->result();
        $total_schedule     = count($list_dept_schedule);

        if($total_schedule == 0) {
            $schedule_code="".$kode_prodi."/".$gpass."/001";
        } elseif($total_schedule < 10) {
            $schedule_code="".$kode_prodi."/".$gpass."/00".($total_schedule+1);
        } elseif($total_schedule < 100) {
            $schedule_code="".$kode_prodi."/".$gpass."/0".($total_schedule+1);
        } elseif ($total_schedule < 1000) {
            $schedule_code="".$kode_prodi."/".$gpass."/".($total_schedule+1);
        } elseif ($total_schedule >= 1000){
            $schedule_code="".$kode_prodi."/".$gpass."/".($total_schedule+1);
        }

        return $schedule_code;
    }

    public function cetak_jadwal_all()
    {
        $data['rows'] = $this->feedkuliah->print_schedule($this->userid, $this->session->userdata('tahunajaran_kelas'));
        $data['duplicate'] = $data['rows']->row();
        $this->load->view('feed_cetak_jdl', $data);
    }

    function sync_kelas_nilai_soap($id)
    {
        $this->load->model('sync_feed/kelas_model', 'kelas');

        $logged = $this->session->userdata('sess_login');
        $years 	= $this->session->userdata('tahunajaran_kelas');

        $kdProdiFeeder = $this->app_model->getdetail('tbl_jurusan_prodi','kd_prodi',$logged['userid'],'kd_prodi','asc')->row();
        
        $jadwal 	= $this->app_model->getdetail('tbl_jadwal_matkul_feeder','id_jadwal',$id,'id_jadwal','asc')->row();   
        
        $getnilai 	= $this->kelas->getNilaiFeeder($jadwal->kd_jadwal)->result();
  
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url    = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy  = $client->getProxy();
        $gettok = $proxy->GetToken(userfeeder, passwordfeeder);
        $token  = $gettok;
        
        $kodemk = str_replace('-', '', $jadwal->kd_matakuliah);
        $kelas  = str_replace(' ', '', $jadwal->kelas);

        $table1 = 'kelas_kuliah';
        $filter = "kode_mk = '".$kodemk."' and nm_kls = '".$kelas."'";
        $limit  = 100; // jumlah data yang diambill
        $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $result = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
        
        $before = yearBefore();

        foreach ($result['result'] as $value1) {
            if (($value1['id_smt'] == $before) && ($value1['id_sms'] == $kdProdiFeeder->id_sms)) {
                $id_kls = $value1['id_kls'];
            }
        }

        $table   = 'nilai';
        $records = [];
        foreach ($getnilai as $val) {
            
            $nilai_huruf = $this->kelas->getNilaiTransaksi($val->npm_mahasiswa,$jadwal->kd_matakuliah,$years)->row();    
                        
            $sp  = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$val->npm_mahasiswa."%'");
            $key = ['id_reg_pd' => $sp['result']['id_reg_pd'], 'id_kls' => $id_kls];
            
            if ((is_null($nilai_huruf->nilai_akhir)) or (empty($nilai_huruf->nilai_akhir)) or ($nilai_huruf->nilai_akhir == 0) ) {
            	$data = array(
                    'nilai_angka'=>'0.00',
                    'nilai_huruf'=>'E',
                    'nilai_indeks'=>'0.00'
                );

            } elseif ($nilai_huruf->nilai_akhir > 100) {
                $data = array(
                    'nilai_angka'=>'0.00',
                    'nilai_huruf'=>'T',
                    'nilai_indeks'=>'0.00'
                );

            } else {
            	$data = array(
                    'nilai_angka'=>$nilai_huruf->nilai_akhir,
                    'nilai_huruf'=>$nilai_huruf->NLAKHTRLNM,
                    'nilai_indeks'=>$nilai_huruf->BOBOTTRLNM
                );
            }

            $records[] = array('key'=>$key, 'data'=>$data);
        }

        foreach ($records as $record) {
            $result = $proxy->UpdateRecord($token, $table, json_encode($record));
            var_dump($result);echo "<hr>";
        }
        $this->db->query("UPDATE tbl_jadwal_matkul_feeder set status_feeder = 1 where id_jadwal = ".$id."");
        $this->db->query("UPDATE tbl_krs_feeder set status_feeder = 1 where kd_jadwal = '".$jadwal->kd_jadwal."'");
    }

    public function sync_kelas_nilai($id_jadwal)
    {
        $token         = $this->pddikti->get_token();
        $jadwal        = $this->db->get_where('tbl_jadwal_matkul_feeder', ['id_jadwal' => $id_jadwal])->row();
        $tahunakademik = $this->session->userdata('tahunajaran_kelas');
        $nilai         = $this->feedkuliah->get_nilai_kuliah($jadwal->kd_jadwal, $tahunakademik);
        $id_prodi      = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
        $kelas         = $this->_get_kelas_kuliah($jadwal->kd_matakuliah, $jadwal->kelas, $tahunakademik, $token, $id_prodi);

        foreach ($nilai as $val) {
            $get_id_mhs = $this->_get_mahasiswa_feeder($token, $val->npm_mahasiswa);
            $id_reg_mhs = json_decode($get_id_mhs)->data[0]->id_registrasi_mahasiswa;

            $payload = [
                "act" => "UpdateNilaiPerkuliahanKelas",
                "token" => $token,
                "key" => [
                    "id_registrasi_mahasiswa" => $id_reg_mhs,
                    "id_kelas_kuliah" => $kelas
                ],
                "record" => [
                    "nilai_angka" => $val->nilai_akhir > 100 ? 0 : $val->nilai_akhir,
                    "nilai_huruf" => $val->NLAKHTRLNM,
                    "nilai_indeks" => $val->BOBOTTRLNM
                ]
            ];

            $exec = $this->pddikti->runWS($payload);
            $result = json_decode($exec);
            $result->npm = $val->npm_mahasiswa;
            dd($result);
            echo '<hr>';
        }
    }

    function sync_kelas_add_soap()
    {
        $logged 		= $this->session->userdata('sess_login');
        $kdProdiFeeder 	= $this->db->query('SELECT * FROM tbl_jurusan_prodi WHERE `kd_prodi`="'.$logged['userid'].'"')->row();
        $jadwal 		= $this->db->query("SELECT distinct kd_jadwal,kelas,kd_matakuliah from tbl_jadwal_matkul_feeder 
        									where kd_jadwal like '".$logged['userid']."%' 
        									and kd_tahunajaran = '".$this->session->userdata('tahunajaran_kelas')."'")->result();
        //var_dump($jadwal);exit();
        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url 	= 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;
        $table 	= 'kelas_kuliah';

        foreach ($jadwal as $value) {
            $mk = $this->db->query("SELECT mk.sks_matakuliah from tbl_matakuliah mk
                                    JOIN tbl_kurikulum_matkul_new km ON mk.kd_matakuliah = km.kd_matakuliah
                                    JOIN tbl_kurikulum_new kr ON kr.kd_kurikulum = km.kd_kurikulum
            						where mk.kd_matakuliah = '".$value->kd_matakuliah."' 
            						and mk.kd_prodi = '".$logged['userid']."'
                                    and kr.status = 1")->row();

            $table1 = 'mata_kuliah';
            $filter = "kode_mk = '".str_replace('-', '', trim($value->kd_matakuliah))."'";
            $limit 	= 150; // jumlah data yang diambill
            $offset = 0; // offset dipakai untuk paging, contoh: bila $limit=20
            $result2 = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);

            foreach ($result2['result'] as $value1) {
                if ($value1['id_sms'] == $kdProdiFeeder->id_sms) {
                    $id_mk = $value1['id_mk'];
                }
            }
            
            $record['id_sms'] = $kdProdiFeeder->id_sms; // id prodi
            $record['id_smt'] = $this->session->userdata('tahunajaran_kelas');
            $record['nm_kls'] = substr(str_replace(' ', '', $value->kelas), 0,5); //nama kelas
            $record['sks_mk'] = $mk->sks_matakuliah; //sks mk
            $record['sks_tm'] = $mk->sks_matakuliah; //sks tatap muka
            $record['a_selenggara_pditt'] = '0';
            $record['a_pengguna_pditt'] = '0';
            $record['kuota_pditt'] = '0';
            $record['id_mk'] = $id_mk;

            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            echo "<pre>";
            print_r ($result1['result']);
            echo "</pre><hr>";
        }
        //$result = $proxy->InsertRecordset($token, $table,json_encode($records));
    }

    public function sync_kelas()
    {
        $token           = $this->pddikti->get_token();
        $tahunakademik   = $this->session->userdata('tahunajaran_kelas');
        $get_jadwal      = $this->feedkuliah->get_jadwal($tahunakademik, $this->userid);
        $id_prodi_feeder = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;

        foreach ($get_jadwal as $jadwal) {
            $kode_mk         = str_replace('-', '', $jadwal->kd_matakuliah);
            $id_matakuliah   = $this->_get_matakuliah_feeder($token, $kode_mk, $id_prodi_feeder); 
            $record = [
                'id_prodi'              => $id_prodi_feeder,
                'id_semester'           => $tahunakademik,
                'id_matkul'             => $id_matakuliah,
                'nama_kelas_kuliah'     => substr(str_replace(' ', '', $jadwal->kelas), 0,5),
                'bahasan'               => '',
                'tanggal_mulai_efektif' => '',
                'tanggal_akhir_efektif' => ''
            ];

            $payload = [
                "act"    => "InsertKelasKuliah",
                "token"  => $token,
                "record" => $record
            ];

            $exec = $this->pddikti->runWS($payload);
            dd(json_decode($exec));
            echo '<hr>';
        }
    }

    private function _get_matakuliah_feeder($token, $kd_matakuliah, $prodi)
    {
        $payload = [
            "act" => "GetListMataKuliah",
            "token" => $token,
            "filter" => "kode_mata_kuliah = '$kd_matakuliah' and id_prodi = '$prodi'"
        ];
        $exec = $this->pddikti->runWS($payload);
        $result = json_decode($exec);
        return $result->data[0]->id_matkul;
    }

    function sync_kelas_krs_soap($id)
    {
        $thajar   = getactyear();
        $logged   = $this->session->userdata('sess_login');
        $jadwal   = $this->app_model->getdetail('tbl_jadwal_matkul_feeder','id_jadwal',$id,'id_jadwal','asc')->row();
        $getnilai = $this->db->query("SELECT DISTINCT a.npm_mahasiswa FROM tbl_krs_feeder a 
                                    JOIN tbl_verifikasi_krs c ON a.kd_krs = c.kd_krs
                                    WHERE a.kd_matakuliah = '$jadwal->kd_matakuliah' 
                                    AND c.tahunajaran = '$jadwal->kd_tahunajaran' 
                                    AND a.kd_jadwal = '$jadwal->kd_jadwal'")->result();


        $this->load->library("Nusoap_lib");
        //$url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url 	= 'http://feeder.ubharajaya.ac.id/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy 	= $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token 	= $result;

        $table1 	= 'kelas_kuliah';
        $filter 	= "kode_mk = '".str_replace('-', '', $jadwal->kd_matakuliah)."' and nm_kls = '".substr(str_replace(' ', '', $jadwal->kelas), 0,5)."'";
        $limit 		= 50; // jumlah data yang diambill
        $offset 	= 0; // offset dipakai untuk paging, contoh: bila $limit=20
        $order 		= 'id_smt desc';
        $result2 	= $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);

        // dd($result2, 2);

        foreach ($result2['result'] as $value1) {
            if (($value1['id_smt'] == $thajar) && ($value1['id_sms'] == $q->id_sms)) {
                $id_kls = $value1['id_kls'];
            }
        }
        
        $table = 'nilai';
        $records = array();
        foreach ($getnilai as $value) {
            $sp2                 = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$value->npm_mahasiswa."%'");
            $record['id_reg_pd'] = $sp2['result']['id_reg_pd'];
            $record['id_kls']    = $id_kls;
            $result1             = $proxy->InsertRecord($token, $table, json_encode($record));
            dd($result1);
            echo "<hr>";
        }
    }

    function sync_kelas_krs($id)
    {
        $sesi_tahunajar = $this->session->userdata('tahunajaran_kelas');
        $thajar         = getactyear();
        $jadwal         = $this->db->get_where('tbl_jadwal_matkul_feeder', ['id_jadwal' => $id])->row();
        $getnilai       = $this->feedkuliah->get_class_participant(
                                                $jadwal->kd_matakuliah, 
                                                $jadwal->kd_tahunajaran, 
                                                $jadwal->kd_jadwal);

        $token = $this->pddikti->get_token();

        $kode_matakuliah = str_replace('-', '', $jadwal->kd_matakuliah);
        $nama_kelas      = substr(str_replace(' ', '', $jadwal->kelas), 0,5);
        $id_prodi_feeder = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;
        // get kelas feeder
        $kelas = $this->_get_kelas_feeder($token, $kode_matakuliah, $nama_kelas, $sesi_tahunajar, $id_prodi_feeder);
        // dd($kelas, 2);
        
        foreach ($getnilai as $value) {
            // get id registrasi mahasiswa feeder
            $mahasiswa = $this->_get_mahasiswa_feeder($token, $value->npm_mahasiswa);
            // dd($mahasiswa, 2);

            $payload3 = [
                'act'    => 'InsertPesertaKelasKuliah',
                'token'  => $token,
                'record' => [
                    'id_kelas_kuliah'         => json_decode($kelas)->data[0]->id_kelas_kuliah,
                    'id_registrasi_mahasiswa' => json_decode($mahasiswa)->data[0]->id_registrasi_mahasiswa
                ]
            ];
            $result = $this->pddikti->runWS($payload3);
            dd(json_decode($result));
            echo '<hr>';
        }
    }

    private function _get_kelas_feeder($token, $kd_mk, $kelas, $tahunajaran, $id_prodi_feeder)
    {
        $payload1 = [
            'act'    => 'GetListKelasKuliah',
            'token'  => $token,
            'filter' => "kode_mata_kuliah = '$kd_mk' and nama_kelas_kuliah = '$kelas' and id_semester = '$tahunajaran' and id_prodi = '$id_prodi_feeder'",
            'limit'  => 0,
            'offset' => 0
        ];
        $kelas = $this->pddikti->runWS($payload1);
        return $kelas;
    }

    private function _get_mahasiswa_feeder($token, $npm)
    {
        $payload2 = [
            'act'    => 'GetListMahasiswa',
            'token'  => $token,
            'filter' => "nim = '$npm'" 
        ];
        $mahasiswa = $this->pddikti->runWS($payload2);
        return $mahasiswa;
    }

    function sync_ajar_dosen_soap()
    {
        $yearBefore = yearBefore();
        
        $loginSession = $this->session->userdata('sess_login');

        $prodi = $this->db->query('SELECT * FROM tbl_jurusan_prodi WHERE kd_prodi="'.$loginSession['userid'].'"')->row();
        
        $jadwal = $this->db->query("SELECT kelas,kd_matakuliah,nidn,nupn,nid from tbl_jadwal_matkul_feeder a
                                    JOIN tbl_karyawan b ON a.`kd_dosen` = b.`nid`
                                    where kd_jadwal like '".$loginSession['userid']."%' 
                                    AND kd_tahunajaran = '".$this->session->userdata('tahunajaran_kelas')."' 
                                    AND (
                                            (b.`nidn` IS NOT NULL OR b.`nidn` != '') 
                                            OR (b.`nupn` IS NOT NULL OR b.`nupn` != '')
                                        )
                                    ORDER BY nidn ASC")->result();

        $this->load->library("Nusoap_lib");
        // $url = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
        $url = 'http://172.16.0.58:8082/ws/live.php?wsdl'; // gunakan live bila sudah yakin
        $client = new nusoap_client($url, true);
        $proxy = $client->getProxy();
        $result = $proxy->GetToken(userfeeder, passwordfeeder);
        $token = $result;
        $table = 'ajar_dosen';

        foreach ($jadwal as $value) {
            $mk = $this->db->query("SELECT sks_matakuliah from tbl_matakuliah 
                                    where kd_matakuliah = '".$value->kd_matakuliah."' 
                                    AND kd_prodi = '".$loginSession['userid']."'")->row();

            if (!empty($value->nidn) && !is_null($value->nidn)) {
                $id_dosen = $value->nidn;
            } else {
                $id_dosen = $value->nupn;
            }
            // var_dump($id_dosen);exit();

            $record['id_jns_eval'] = '1';
           
            $id_sdm = $proxy->GetRecord($token, 'dosen', "nidn = '".trim($id_dosen)."'");
            
            $id_reg_ptk = $proxy->GetRecordSet($token, 'dosen_pt', "p.id_sdm = '".$id_sdm['result']['id_sdm']."'");;

            $record['id_reg_ptk'] = $id_reg_ptk['result'][0]['id_reg_ptk'];

            if ($loginSession['userid'] == '74101') {
                $kelas = substr(trim(str_replace(' ', '', $value->kelas)), 0,5);
            } else {
                $kelas = trim(str_replace(' ', '', $value->kelas));
            }

            $table1     = 'kelas_kuliah';
            $filter     = "kode_mk = '".str_replace('-', '', $value->kd_matakuliah)."' and nm_kls = '".$kelas."'";
            $limit      = 100; // jumlah data yang diambill
            $offset     = 0; // offset dipakai untuk paging, contoh: bila $limit=20
            $result2    = $proxy->GetRecordset($token, $table1, $filter, $order, $limit,$offset);
            // var_dump($result2);exit();

            foreach ($result2['result'] as $value1) {
                if (($value1['id_smt'] == $yearBefore) && ($value1['id_sms'] == $prodi->id_sms)) {
                    $id_kls = $value1['id_kls'];
                }
            }

            $record['id_kls']               = $id_kls;
            $record['sks_subst_tot']        = $mk->sks_matakuliah;
            $record['sks_tm_subst']         = $mk->sks_matakuliah;
            $record['sks_prak_subst']       = '0';
            $record['sks_prak_lap_subst']   = '0';
            $record['sks_sim_subst']        = '0';
            $record['jml_tm_renc']          = '16';
            $record['jml_tm_real']          = '16';

            $result1 = $proxy->InsertRecord($token, $table, json_encode($record));
            dd($result1);echo "<hr>";
        }
    }

    public function sync_ajar_dosen()
    {
        $token          = $this->pddikti->get_token();
        $sesi_tahunajar = $this->session->userdata('tahunajaran_kelas');
        $get_schedule   = $this->feedkuliah->jadwal_dosen($this->userid, $sesi_tahunajar);
        $get_eval       = $this->_get_eval($token);
        $id_prodi       = $this->db->get_where('tbl_jurusan_prodi', ['kd_prodi' => $this->userid])->row()->id_sms;

        foreach ($get_schedule as $schedule) {
            $kelas     = substr(trim(str_replace(' ', '', $schedule->kelas)), 0,5);
            $id_dosen  = ($schedule->nidn == '' || empty($schedule->nidn)) ? $schedule->nupn : $schedule->nidn;
            $reg_dosen = $this->_get_reg_dosen($id_dosen, $token);
            $kode_mk   = str_replace('-', '', $schedule->kd_matakuliah);
            $id_kuliah = $this->_get_kelas_feeder($token, $kode_mk, $kelas, $schedule->kd_tahunajaran, $id_prodi);
            $id_kelas  = json_decode($id_kuliah)->data[0]->id_kelas_kuliah;

            $payload = [
                "act" => "InsertDosenPengajarKelasKuliah",
                "token" => $token,
                "record" => [
                    "id_registrasi_dosen"  => $reg_dosen,
                    "id_kelas_kuliah"      => $id_kelas,
                    "id_substansi"         => "",
                    "sks_substansi_total"  => $schedule->sks_matakuliah,
                    "rencana_tatap_muka"   => "16",
                    "realisasi_tatap_muka" => "16",
                    "id_jenis_evaluasi"    => $get_eval
                ]
            ];
            $exec = $this->pddikti->runWS($payload);
            $result = json_decode($exec);
            $result->data = "nama: $schedule->nama, nidn/nupn: $id_dosen, matakuliah: $schedule->kd_matakuliah, kelas: $schedule->kelas";
            $result->status = $result->error_code == 0 ? 'Sinkronisasi berhasil' : 'Sinkronisasi gagal';
            dd($result); echo '<hr>';
        }
    }

    private function _get_reg_dosen($nidn, $token)
    {
        $payload = [
            "act" => "GetListPenugasanDosen",
            "token" => $token,
            "filter" => "nidn = '$nidn'"
        ];
        $exec = $this->pddikti->runWS($payload);
        $result = json_decode($exec)->data[0]->id_registrasi_dosen;
        return $result;
    }

    private function _get_kelas_kuliah($kd_mk, $kelas, $tahunakademik, $token, $id_prodi)
    {
        $kd_matakuliah = trim(str_replace('-', '', $kd_mk));
        $filter = "kode_mata_kuliah = '$kd_matakuliah' and nama_kelas_kuliah = '$kelas' and id_semester = '$tahunakademik' and id_prodi = '$id_prodi'";
        $payload = [
            "act" => "GetListKelasKuliah",
            "token" => $token,
            "filter" => $filter
        ];
        $exec = $this->pddikti->runWS($payload);
        $result = json_decode($exec)->data[0]->id_kelas_kuliah;
        return $result;
    }

    private function _get_eval($token)
    {
        $payload = [
            "act" => "GetJenisEvaluasi",
            "token" => $token
        ];
        $exec = $this->pddikti->runWS($payload);
        $result = json_decode($exec)->data[0]->id_jenis_evaluasi;
        return $result;
    }

    public function get_detail_matakuliah_by_semester_feeder($id)
    {
        $user = $this->session->userdata('sess_login');
        $kode = $user['userid'];

        if ($this->session->userdata('tahunajaran_kelas') > 20162) {
            $data   = $this->db->query('SELECT DISTINCT a.kd_matakuliah, a.nama_matakuliah, a.id_matakuliah FROM tbl_matakuliah a
                                        join tbl_kurikulum_matkul_new b on a.kd_matakuliah = b.kd_matakuliah
                                        WHERE a.kd_prodi = '.$kode.' 
                                        AND b.semester_kd_matakuliah = '.$id.' 
                                        AND b.`kd_kurikulum` LIKE "%'.$kode.'%"
                                        order by nama_matakuliah asc')->result();

        } else {
            $data   = $this->db->query('SELECT DISTINCT a.kd_matakuliah, a.nama_matakuliah, a.id_matakuliah FROM tbl_matakuliah a
                                        join tbl_kurikulum_matkul b on a.kd_matakuliah = b.kd_matakuliah
                                        WHERE a.kd_prodi = '.$kode.' 
                                        AND b.semester_kd_matakuliah = '.$id.' 
                                        AND b.`kd_kurikulum` LIKE "%'.$kode.'%"
                                        order by nama_matakuliah asc')->result();
        }
        
        $list = "<option> -- </option>";

        foreach($data as $row){
            $list .= "<option value='".$row->id_matakuliah."'>".$row->kd_matakuliah." - ".$row->nama_matakuliah."</option>";
        }

        die($list);

    }

}

/* End of file Kelas.php */
/* Location: ./application/modules/sync_feed/controllers/Kelas.php */