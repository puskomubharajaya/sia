<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Biodata extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		ini_set('memory_limit', '512M');
		if (!$this->session->userdata('sess_login')) {
			// $cekakses = $this->role_model->cekakses(40)->result();
			// if ($cekakses != TRUE) {
			// 	echo "<script>alert('Anda Tidak Berhak Mengakses !!');history.go(-1);</script>";
			// }
			redirect('auth','refresh');
		}
	}

	public function index()
	{
		$logged = $this->session->userdata('sess_login');
		$grup   = $logged['id_user_group'];
		$pecah  = explode(',', $grup);
		$jmlh   = count($pecah);

		for ($i=0; $i < $jmlh; $i++) { 
			$grups[] = $pecah[$i];
		}

		if ((in_array(7, $grups)) || (in_array(6, $grups)) ) {
			redirect(base_url('sync_feed/biodata/bio_dosen'));
		} else {
			$data['mhs']       = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$logged['userid'],'NIMHSMSMHS','asc')->row();
			$data['mhs2']      = $this->app_model->getdetail('tbl_bio_mhs','npm',$logged['userid'],'npm','asc')->row();
			$data['provinces'] =$this->app_model->getdata('tbl_provinsis','id', 'ASC')->result();
			$data['page']      = 'v_biodata';
		}
		
        $this->load->view('template/template', $data);
	}

	function get_kota($id)
	{
        $kota = $this->app_model->getdetail('tbl_kokab', 'province_id', $id, 'id', 'ASC')->result();
		$out = "<select class='form-control' name='kota' id='kota'>";
        foreach ($kota as $row) {
            $out .= "<option value='".$row->id."'>".$row->name. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function get_kec($id)
	{
        $kota = $this->app_model->getdetail('tbl_kecamatans', 'regency_id', $id, 'id', 'ASC')->result();
		$out = "<select class='form-control' name='kecamatan' id='kec'>";
        foreach ($kota as $row) {
            $out .= "<option value='".$row->id."'>".$row->name. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function get_kel($id)
	{
        $kota = $this->app_model->getdetail('tbl_kelurahans', 'district_id', $id, 'id', 'ASC')->result();
		$out = "<select class='form-control' name='kelurahan' id='kel'>";
        foreach ($kota as $row) {
            $out .= "<option value='".$row->id."'>".$row->name. "</option>";
        }
        $out .= "</select>";
        echo $out;
	}

	function updatedata()
	{
		if ($this->input->post('prov') == '0000') {
			$provinsi = $this->input->post('provhidden');
		} else {
			$provinsi = $this->input->post('prov');
		}

		if ($this->input->post('kecamatan') == '0000') {
			$kecamatan = $this->input->post('kecamatanhidden');
		} else {
			$kecamatan = $this->input->post('kecamatan');
		}
		
		if ($this->input->post('kota') == '0000') {
			$kota = $this->input->post('kotahidden');
		} else {
			$kota = $this->input->post('kota');
		}

		if ($this->input->post('kelurahan') == '0000') {
			$kelurahan = $this->input->post('kelurahanhidden');
		} else {
			$kelurahan = $this->input->post('kelurahan');
		}		

		$logged = $this->session->userdata('sess_login');
		$mhs    = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$logged['userid'],'NIMHSMSMHS','asc')->row();
		$cek1   = $this->app_model->getdetail('tbl_bio_mhs','npm',$logged['userid'],'npm','asc')->num_rows();

		$data['npm']        = $logged['userid'];
		$data['no_hp']      = $this->input->post('tlpn', TRUE);
		$data['email']      = $this->input->post('email', TRUE);
		$data['nama_ibu']   = strtoupper($this->input->post('ibu', TRUE));
		$data['nama_ayah']  = $this->input->post('ayah', TRUE);
		$data['tgl_update'] = date('Y-m-d H:i:s');
		$data['provinsi']   = $provinsi;
		$data['kecamatan']  = $kecamatan;
		$data['kota']       = $kota;
		$data['kelurahan']  = $kelurahan;
		$data['alamat']     = trim($this->input->post('alamat', TRUE));

		if ($cek1 > 0) {
			$this->db->where('npm', $logged['userid'])->update('tbl_bio_mhs',$data);
		} else {
			$data = $this->security->xss_clean($data);
			$this->db->insert('tbl_bio_mhs', $data);
		}

		$data1['NIKMSMHS']   = $this->input->post('nik', TRUE);
		$data1['NMIBUMSMHS'] = strtoupper($this->input->post('ibu', TRUE));
		$data1['NISNMSMHS']  = $this->input->post('nisn', TRUE);
		$data1['KDJEKMSMHS'] = $this->input->post('jk');
		$data1               = $this->security->xss_clean($data1);
		$this->db->where('NIMHSMSMHS', $logged['userid'])->update('tbl_mahasiswa',$data1);
		redirect('sync_feed/biodata','refresh');
	}

	function bio_dosen()
	{
		$this->load->model('temph_model');
		$session     = $this->session->userdata('sess_login');
		$log         = $session['userid'];
		$tahunajaran = getactyear();

		$data['yearAcademic'] = $this->db->get('tbl_tahunakademik')->result();
		// $data['beban'] = $this->temph_model->getBebanDosen($log,$tahunajaran)->row();
		$data['detil'] = $this->db->query("SELECT * from tbl_biodata_dosen  where nid = '".$log."'")->row();
		$data['dets']  = $this->db->query("SELECT * from tbl_karyawan where nid = '".$log."'")->row();
		$data['rsc']   = $this->db->query("SELECT * from tbl_penelitian_dosen where userid = '".$log."'")->result();
		$data['dev']   = $this->db->query("SELECT * from tbl_pengabdian_dosen where userid = '".$log."'")->result();
		$data['pub']   = $this->db->query("SELECT * from tbl_publikasi_dosen where userid = '".$log."'")->result();
		$data['pdd']   = $this->db->query("SELECT * from tbl_pendidikan_dosen where userid = '".$log."'")->result();
		$data['page']  = "v_bio_dosen";

		$this->load->view('template/template', $data);
	}

	function countCreditsTotal()
	{
		$this->load->model('temph_model');
		$session     = $this->session->userdata('sess_login');
		$log         = $session['userid'];
		$tahunajaran = getactyear();
		$credits = $this->temph_model->getBebanDosen($log,$tahunajaran)->row();
		echo $credits->jums.' SKS';
	}

	function detil_beban()
	{
		$q = $this->db->query("SELECT kode from tbl_tahunakademik where status = '1'")->row();
		$data['quer'] = $this->db->query("SELECT * from tbl_tahunakademik where kode <= '".$q->kode."'")->result();
		$data['page'] = "v_wg_sks";
		$this->load->view('template/template', $data);
	}

	function tambah_biodosen()
	{
		$logs = $this->session->userdata('sess_login');
		$hitungkerja = count($this->input->post('work', TRUE));
		$hitungdidik = count($this->input->post('didik', TRUE));
		$kemon = '';
		for ($i=0; $i < $hitungkerja ; $i++) { 
			if ($kemon == '') {
			 	$kemon = $this->input->post('work['.$i.']', TRUE).',';
			 } else {
			 	$kemon = $kemon.$this->input->post('work['.$i.']', TRUE).',';
			 }
		}
		$markemon = '';
		for ($i=0; $i < $hitungdidik ; $i++) { 
			if ($markemon == '') {
			 	$markemon = $this->input->post('didik['.$i.']', TRUE).',';
			 } else {
			 	$markemon = $markemon.$this->input->post('didik['.$i.']', TRUE).',';
			 }
		}
		$petjah = explode('/', $this->input->post('tgl'));
		$a = $petjah[0];
		$b = $petjah[1];
		$c = $petjah[2];
		$d = $c.'/'.$a.'/'.$b;

		$data = array(
			'nid'			=> $this->input->post('nid'),
			'nidn'			=> $this->input->post('nidn'),
			'nama'			=> $this->input->post('nama'),
			'alamat'		=> $this->input->post('alamat'),
			'tlp'			=> $this->input->post('tlp'),
			'email'			=> $this->input->post('email'),
			'tpt_lahir'		=> $this->input->post('tempatlahir'),
			'tgl_lahir'		=> $d,
			'jabfung'		=> $this->input->post('jabfung'),
			'thnj'			=> $this->input->post('thnj'),
			'pekerjaan'		=> $kemon,
			'date_input'	=> date('Y-m-d')
			// 'foto'			=> $config['upload_path'].$config['file_name']
		);

		$sql 	= "SELECT * from tbl_biodata_dosen where nid = '".$logs['userid']."'";
		$lookup = $this->db->query($sql)->num_rows();

		if ($lookup > 0) {
		$this->app_model->updatedata('tbl_biodata_dosen','nid',$logs['userid'],$data);
		} else {
		$this->app_model->insertdata('tbl_biodata_dosen',$data);
		}

		$kd = $this->input->post('inst');


		if (!is_null($kd)) {
		for ($i = 0; $i < count($kd); $i++) {
			$dt[] = array(
					'userid'			=> $logs['userid'],
					'jurusan'			=> $this->input->post('konst', TRUE)[$i],
					'instansi'			=> $this->input->post('inst', TRUE)[$i],
					'tahun_masuk'		=> $this->input->post('str', TRUE)[$i],
					'tahun_keluar'		=> $this->input->post('end', TRUE)[$i]
				);
		}

		$this->db->insert_batch('tbl_pendidikan_dosen',$dt);
		}

		echo "<script>alert('berhasil');document.location.href='".base_url()."sync_feed/biodata/bio_dosen';</script>";
	}

	function profil()
	{
		$logg = $this->session->userdata('sess_login');

		if ($this->session->userdata('sess_biokaryawan') == TRUE) {
			$logx = $this->session->userdata('sess_biokaryawan');
		} else {
			$logx = $logg['userid'];
		}

		$data['quo'] = $this->db->query("SELECT * from tbl_karyawan where nid = '".$logx."'")->row();
		$data['que'] = $this->db->query("SELECT * from tbl_biodata_dosen where nid = '".$logx."'")->row();
		$data['rsc'] = $this->db->query("SELECT * from tbl_penelitian_dosen where userid = '".$logx."'")->result();
		$data['dev'] = $this->db->query("SELECT * from tbl_pengabdian_dosen where userid = '".$logx."'")->result();
		$data['pub'] = $this->db->query("SELECT * from tbl_publikasi_dosen where userid = '".$logx."'")->result();
		$data['pdd'] = $this->db->query("SELECT * from tbl_pendidikan_dosen where userid = '".$logx."'")->result();
		$data['page'] = "v_profile";
		$this->load->view('template/template', $data);
	}

	function pdf()
	{
		$this->load->library('Cfpdf');
		$logg = $this->session->userdata('sess_login');

		if ($this->session->userdata('sess_biokaryawan') == TRUE) {
			$logs = $this->session->userdata('sess_biokaryawan');
		} else {
			$logs = $logg['userid'];
		}
		$data['quo'] = $this->db->query("SELECT * from tbl_karyawan where nid = '".$logs."'")->row();
		$data['que'] = $this->db->query("SELECT * from tbl_biodata_dosen where nid = '".$logs."'")->row();
		$data['rsc'] = $this->db->query("SELECT * from tbl_penelitian_dosen where userid = '".$logs."'")->result();
		$data['dev'] = $this->db->query("SELECT * from tbl_pengabdian_dosen where userid = '".$logs."'")->result();
		$data['pub'] = $this->db->query("SELECT * from tbl_publikasi_dosen where userid = '".$logs."'")->result();
		$data['pdd'] = $this->db->query("SELECT * from tbl_pendidikan_dosen where userid = '".$logs."'")->result();
		
		$this->load->view('pdf_profile', $data);
	}

	
	function addresearch()
	{
		extract(PopulateForm());

		$usr = $this->session->userdata('sess_login');

		$hit = count($titls);

		for ($i = 0; $i < $hit; $i++) {
			$data[] = array(
				'userid'	=> $usr['userid'],
				'judul'		=> $titls[$i],
				'tahun'		=> $years[$i],
				'biaya'		=> $fees[$i],
				'posisi'	=> $posy[$i],
				'jenis'		=> $types[$i]
			);
		}
		// var_dump($data);exit();
		$this->db->insert_batch('tbl_penelitian_dosen',$data);
		echo '<script>alert("Berhasil!");history.go(-1);</script>';
	}

	function addDevotion()
	{
		extract(PopulateForm());

		$usr = $this->session->userdata('sess_login');

		$hit = count($titls);

		for ($i = 0; $i < $hit; $i++) {
			$data[] = array(
				'userid'	=> $usr['userid'],
				'judul'		=> $titls[$i],
				'tahun'		=> $years[$i],
				'dana'		=> $fees[$i],
				'posisi'	=> $posy[$i],
				'jenis'		=> $types[$i]
			);
		}
		// var_dump($data);exit();
		$this->db->insert_batch('tbl_pengabdian_dosen',$data);
		echo '<script>alert("Berhasil!");history.go(-1);</script>';
	}

	function addPublication()
	{
		extract(PopulateForm());

		$usr = $this->session->userdata('sess_login');

		$hit = count($titls);

		for ($i = 0; $i < $hit; $i++) {
			$data[] = array(
				'userid'	=> $usr['userid'],
				'judul'		=> $titls[$i],
				'tahun'		=> $years[$i],
				'jurnal'	=> $jurnal[$i],
				'tingkat'	=> $tgkt[$i],
				'tipe'		=> $jens[$i]
			);
		}
		// var_dump($data);exit();
		$this->db->insert_batch('tbl_publikasi_dosen',$data);
		echo '<script>alert("Berhasil!");history.go(-1);</script>';
	}
	
	
	public function delete($id)
	{
		$this->app_model->deletedata('tbl_pendidikan_dosen','id_pddkn',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."sync_feed/biodata/bio_dosen';</script>";
	}

	function delRows($id)
	{
		$this->app_model->deletedata('tbl_pendidikan_dosen','id_pddkn',$id);
	}

	function upload($id)
	{
		$data['npm']= $id;
        $this->load->view('image_upload', $data);
	}

	public function edit_foto()
	{
		$npm   = $this->input->post('npm');
		$mhs   = $this->app_model->getdetail('tbl_mahasiswa','NIMHSMSMHS',$npm,'NIMHSMSMHS','asc')->row();
		$prodi = $mhs->KDPSTMSMHS;
		
		$datetime = date('Ymd_His');
		$tem      = $_FILES['files']['name'];
		$depan    = pathinfo($tem,PATHINFO_FILENAME);
		$depan1   = str_replace(' ','_',$depan);
		$depan2   = str_replace('.','_',$depan1);
		$ext      = pathinfo($tem,PATHINFO_EXTENSION);
		$filename = $npm.'-'.$depan2.'-'.$datetime.'.'.$ext;
		
		$config['upload_path']   = './image/biodata/'.$prodi.'/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name']     = $filename;
		$config['max_size']      = '512';
		$config['max_width']     = '102400';
		$config['max_height']    = '102400';

		if (!is_dir('image/biodata/'.$prodi)) {
			mkdir('./image/biodata/' . $prodi, 0777, TRUE);
		}
			
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload('files')) {
			$error = strip_tags($this->upload->display_errors());
			echo "<script>alert('".$error."');history.go(-1);</script>";
		} else {

			$data = array(
				'image' 		=> $filename
			);
			
			$row = $this->db->where('npm',$npm)->get('tbl_bio_mhs')->row();
			if (!empty($row->image)){
				unlink('./image/biodata/'.$prodi.'/'.$row->image);
			}

			$this->app_model->updatedata('tbl_bio_mhs','npm',$npm, $data);
			echo "<script>alert('Sukses');history.go(-1);</script>";
		}
	}

	public function print_bkd()
	{
		$this->load->library('Cfpdf');
		$loginSession = $this->session->userdata('sess_login')['userid'];
		$tahunAjar = $this->input->post('tahunajar');
		$data['semester'] = $this->db->get_where('tbl_tahunakademik', ['kode' => $tahunAjar])->row()->tahun_akademik;
		$data['data'] = $this->db->query("SELECT 
											kry.nama,
											kry.nidn,
											kry.jabfung,
											prd.prodi,
											fak.fakultas
										FROM tbl_karyawan kry
										LEFT JOIN tbl_jurusan_prodi prd ON kry.jabatan_id = prd.kd_prodi
										JOIN tbl_fakultas fak ON prd.kd_fakultas = fak.kd_fakultas
										WHERE kry.nid = '$loginSession'")->row();

		$data['courses'] = $this->db->query("SELECT 
												mk.kd_matakuliah, 
												mk.nama_matakuliah,
												mk.sks_matakuliah,
												jdl.waktu_mulai,
												jdl.waktu_selesai,
												jdl.hari
											FROM tbl_jadwal_matkul jdl
											JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
											WHERE jdl.kd_dosen = '$loginSession'
											AND jdl.kd_tahunajaran = '$tahunAjar'
											AND (mk.nama_matakuliah NOT LIKE 'skripsi%' 
												AND mk.nama_matakuliah NOT LIKE 'kerja praktek%' 
												AND mk.nama_matakuliah NOT LIKE 'tesis%'
												AND mk.nama_matakuliah NOT LIKE 'thesis%' 
												AND mk.nama_matakuliah NOT LIKE 'magang%'
												AND mk.nama_matakuliah NOT LIKE 'kuliah kerja%') ")->result();

		$data['research'] = $this->db->query("SELECT * FROM tbl_penelitian_dosen WHERE userid = '$loginSession'")->result();
		$data['devotion'] = $this->db->query("SELECT * FROM tbl_pengabdian_dosen WHERE userid = '$loginSession'")->result();
		$data['publication'] = $this->db->query("SELECT * FROM tbl_pengabdian_dosen WHERE userid = '$loginSession'")->result();
		$this->load->view('bkd_printout', $data);
	}
}

/* End of file biodata.php */
/* Location: ./application/controllers/biodata.php */