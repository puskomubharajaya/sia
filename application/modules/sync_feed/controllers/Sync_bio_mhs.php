<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sync_bio_mhs extends CI_Controller {

	protected $userid, $token;

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('sess_login')) {
			redirect('auth/logout','refresh');
		}
		$this->userid = $this->session->userdata('sess_login')['userid'];
		$this->load->driver('cache', ['adapter' => 'memcached']);
		$this->load->library('pddikti');
		$this->token = $this->pddikti->get_token();
	}

	public function index()
	{
		if ($this->session->userdata('angkatan_sess')) {
			$this->session->unset_userdata('angkatan_sess');
		}

		for ($i = 1995; $i <= date('Y'); $i++) {
			$angkatan[] = $i;
		}
		$data['page'] = 'sync_bio_mhs_v';
		$data['year'] = $angkatan;
		$this->load->view('template/template', $data);
	}

	public function save_sess()
	{
		$this->session->set_userdata('angkatan_sess', $this->input->post('angkatan'));
		redirect('sync_feed/sync_bio_mhs/get_students');
	}

	public function get_students()
	{
		$cache_name = 'bio_mahasiswa_'.$this->userid.'_'.$this->session->userdata('angkatan_sess');
		if (!$this->cache->get($cache_name)) {
			$get_students = $this->_get_students($this->userid, $this->session->userdata('angkatan_sess'));
			$students = json_decode($get_students)->data;
			$this->cache->save($cache_name, $students, 300);
		}
		$data['students'] = $this->cache->get($cache_name);
		$data['page'] = 'sync_students_v';
		$this->load->view('template/template', $data);
	}

	protected function _get_students($userid, $angkatan)
	{
		$ch = curl_init('https://sia.ubharajaya.ac.id/api?api_auth_key=Ubharaj4y42019&kd_prodi='.$userid.'&kd_thn='.$angkatan);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$exec = curl_exec($ch);
		curl_close($ch);
		return $exec;
	}

	public function sync()
	{
		extract(PopulateForm());

		foreach ($npm as $key => $value) {
			$filterHp = str_replace(['+62', ' ', '-'], ['0', '', ''], $hp[$value]);
			$id_mahasiswa = $this->_get_feeder_id_mhs($npm[$value]);
			$data = [
				'jenis_kelamin' => $this->_set_gender($jk[$value]),
				'jalan' => substr($alamat[$value], 0, 80),
				'dusun' => $dusun[$value],
				'rt' => $rt[$value],
				'rw' => $rw[$value],
				'kelurahan' => $kelurahan[$value],
				'id_wilayah' => $kab[$value],
				'handphone' => $filterHp,
				'email' => $email[$value],
			];

			$payload = [
				"act" => 'UpdateBiodataMahasiswa',
				"token" => $this->token,
				"key" => ['id_mahasiswa' => $id_mahasiswa],
				"record" => $data
			];

			$exec = $this->pddikti->runWS($payload);
			$result = json_decode($exec);

			$response = new stdClass;
			$response->nama = $nama[$value];
			$response->npm = $npm[$value];
			$response->status = $result->error_code != 0 ? 'Sinkronisasi gagal. Pesan: '.$result->error_desc : 'Sinkronisasi berhasil';
			dd($response);
		}
	}

	protected function _get_feeder_id_mhs($nim)
	{
		$payload = [
			"act" => "GetListMahasiswa",
			"token" =>  $this->token,
			"filter" => "nim = '$nim'"
		];

		$exec = $this->pddikti->runWS($payload);
		$result = json_decode($exec)->data;
		return $result[0]->id_mahasiswa;
	}

	protected function _set_gender($gender)
	{
		switch ($gender) {
			case 1:
				return "L";
				break;

			case 2:
				return "P";
				break;
			
			default:
				return $gender;
				break;
		}
	}

}

/* End of file Sync_bio_mhs.php */
/* Location: ./application/modules/sync_feed/controllers/Sync_bio_mhs.php */