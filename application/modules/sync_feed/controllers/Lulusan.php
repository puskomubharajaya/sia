<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lulusan extends CI_Controller {

    private $userid, $usergroup;

	public function __construct()
	{
		parent::__construct();

        if (!$this->session->userdata('sess_login')) {
            redirect('auth/logout','refresh');
        }

        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('setting_model');
        $this->load->library("pddikti");
        $this->userid = $this->session->userdata('sess_login')['userid'];
        $this->usergroup = $this->session->userdata('sess_login')['id_user_group'];
	}

    public function index()
    {
        $this->session->unset_userdata('tahun');
        $this->session->unset_userdata('jurusan');

        $group = get_group($this->usergroup);
        $data['userid'] = $this->userid;

        if (in_array(10, $group)) { //baa
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
            $data['page']     = 'sync_feed/v_lulusan_select_baa';

        } elseif (in_array(9, $group)) { // fakultas
            $data['jurusan']  = $this->app_model->getdetail(
                                                    'tbl_jurusan_prodi',
                                                    'kd_fakultas',
                                                    $this->userid,
                                                    'prodi',
                                                    'asc')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
            $data['page']     = 'sync_feed/v_lulusan_select_fak';

        } elseif (in_array(8, $group) || in_array(19, $group)) { // prodi/operator
            $data['jurusan']  = $this->db->where('kd_prodi',$this->userid)->get('tbl_jurusan_prodi')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
            $data['page']     = 'sync_feed/v_lulusan_select_prodi';

        } elseif (in_array(1, $group)) { // admin
            $data['fakultas'] = $this->app_model->getdata('tbl_fakultas', 'id_fakultas', 'ASC')->result();
            $data['akademik'] = $this->db->get('tbl_tahunakademik')->result();
            $data['page']     = 'sync_feed/v_lulusan_select_baa';
        }

        $this->load->view('template/template', $data);   
    }

    public function form()
    {
        $data['page'] = 'v_lulusan_form';
        $this->load->view('template/template', $data);
    }

    public function set_search()
    {
        $jurusan = $this->input->post('jurusan');
        $tahun   = $this->input->post('tahun');

        $this->session->set_userdata('tahun',$tahun);
        $this->session->set_userdata('jurusan',$jurusan);

        redirect(base_url('sync_feed/lulusan/view_lulusan'),'refresh');
    }

    public function view_lulusan()
    {
        if ($this->session->userdata('jurusan')) {
            $data['rows'] = $this->db->query("SELECT * FROM tbl_lulusan lls 
                                            JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = lls.`npm_mahasiswa`
                                            WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' 
                                            AND lls.`ta_lulus` = ".$this->session->userdata('tahun')."")->result();

            $data['page'] = 'v_lulusan_view';
            $this->load->view('template/template', $data);    
        }else{
            redirect(base_url().'sync_feed/lulusan','refresh');
        }        
    }

    public function save()
    {
    	$data = array(
                    'npm_mahasiswa' => $this->input->post('npm'), 
                    'sk_yudisium'   => $this->input->post('sk_yudi'),
                    'tgl_yudisium'  => $this->input->post('tgl_yudi'),
                    'tgl_lulus'     => $this->input->post('tgl_lulus'),
                    'ipk'           => $this->input->post('ipk'),
                    'no_ijazah'     => $this->input->post('no_ijazah'),
                    'jdl_skripsi'   => $this->input->post('jdl_skripsi'),
                    'dospem1'       => $this->input->post('nidn1'),
                    'dospem2'       => $this->input->post('nidn2'),
                    'mulai_bim'     => $this->input->post('mulai_bim'),
                    'ahir_bim'      => $this->input->post('ahir_bim'),
                    'ta_lulus'      => $this->input->post('ta'),
                    'sks'           => $this->input->post('sks'),
                    'flag_feeder'   => 1
				);

        $isStudentExist = $this->db->where('npm_mahasiswa', $this->input->post('npm'))
                                    ->get('tbl_lulusan')
                                    ->num_rows();

        if ($isStudentExist > 0) {
            $this->db->where('npm_mahasiswa', $this->input->post('npm'))->update('tbl_lulusan', $data);
        } else {
            $this->db->insert('tbl_lulusan', $data);
        }

        $updt = [
            'STMHSMSMHS' => 'L',
            'TGLLSMSMHS' => $this->input->post('tgl_lulus')
        ];
        $this->db->where('NIMHSMSMHS', $this->input->post('npm'));
        $this->db->update('tbl_mahasiswa', $updt);

        // after added to lulusan table, student cannot login
        $this->db->where('userid', $this->input->post('npm'));
        $this->db->update('tbl_user_login', ['status' => 0]);

        echo "<script>alert('Data berhasil disimpan!');
        document.location.href='".base_url()."sync_feed/lulusan/view_lulusan';</script>";
    }

    public function edit($id)
    {
        $data['row'] = $this->db->select('lls.*,mhs.NMMHSMSMHS')
                                ->from('tbl_lulusan lls')
                                ->join('tbl_mahasiswa mhs','lls.npm_mahasiswa = mhs.NIMHSMSMHS')
                                ->where('id_lulusan',$id)
                                ->get()->row();

        $data['page'] = 'v_lulusan_form_edit';
        $this->load->view('template/template', $data);
    }

    public function save_edit()
    {
        $id = $this->input->post('id');

        $data = [
                'sk_yudisium'  => $this->input->post('sk_yudi'),
                'tgl_yudisium' => $this->input->post('tgl_yudi'),
                'tgl_lulus'    => $this->input->post('tgl_lulus'),
                'ipk'          => $this->input->post('ipk'),
                'no_ijazah'    => $this->input->post('no_ijazah'),
                'jdl_skripsi'  => $this->input->post('jdl_skripsi'),
                'mulai_bim'    => $this->input->post('mulai_bim'),
                'ahir_bim'     => $this->input->post('ahir_bim'),
                'sks'          => $this->input->post('sks'),
                'dospem1'      => $this->input->post('nidn1'),
                'dospem2'      => $this->input->post('nidn2')
            ];

        $this->db->where('id_lulusan', $id);
        $this->db->update('tbl_lulusan', $data);

        echo "<script>alert('Update sukses!');
        document.location.href='".base_url()."sync_feed/lulusan/view_lulusan';</script>";
    }

    public function delete_data($id)
    {
        $this->db->where('id_lulusan', $id)->delete('tbl_lulusan');

        echo "<script>alert('Sukses');
        document.location.href='".base_url()."sync_feed/lulusan/view_lulusan';</script>";
    }

    public function get_jurusan($id)
    {
        $jurusan = $this->app_model->getdetail('tbl_jurusan_prodi', 'kd_fakultas', $id, 'id_prodi', 'ASC')->result();
        $out = "<select class='form-control' name='jurusan' id='jurs'><option>--Pilih Jurusan--</option>";
        foreach ($jurusan as $row) {
            $out .= "<option value='".$row->kd_prodi."_".$id."'>".$row->prodi. "</option>";
        }
        $out .= "</select>";

        echo $out;
    }

    public function load_mhs_autocomplete()
    {
        $or_stm = "(a.NMMHSMSMHS like '%".$_GET['term']."%' OR a.NIMHSMSMHS like '%".$_GET['term']."%')"; 
        $st_mhs = array('C','K');
        $prodi = $this->session->userdata('jurusan');
        $this->db->distinct();
        $this->db->select("a.NIMHSMSMHS,a.NMMHSMSMHS");
        $this->db->from('tbl_mahasiswa a');
        $this->db->where('a.KDPSTMSMHS', $prodi);
        $this->db->where($or_stm, NULL, FALSE);
        $sql  = $this->db->get();

        $data = array();
        foreach ($sql->result() as $row) {
            $data[] = [
                        'nama'  => $row->NMMHSMSMHS,
                        'npm'   => $row->NIMHSMSMHS,
                        'value' => $row->NIMHSMSMHS.' - '.$row->NMMHSMSMHS
                    ];
        }

        echo json_encode($data);
    }

    public function load_kry_autocomplete()
    {
        $prodi  = $this->session->userdata('jurusan');
        $term = $_GET['term'];

        $sql = $this->db->query("SELECT nidn, nupn, nama FROM tbl_karyawan 
                                WHERE (nidn <> '' OR nupn <> '') 
                                AND nama LIKE '%$term%'");

        $data = array();
        foreach ($sql->result() as $row) {
            if ($row->nidn != '') {
                $iddos = $row->nidn;
            } else {
                $iddos = $row->nupn;
            }
            $data[] = [
                        'nama'  => $row->nama,
                        'nidn'  => $iddos,
                        'value' => $iddos.' - '.$row->nama
                    ];
        }
        echo json_encode($data);
    }

    public function sync_mhs_soap()
    {
        if ($this->session->userdata('jurusan')) {
        
            $this->load->library("Nusoap_lib");
            // $url   = 'http://172.16.0.58:8082/ws/sandbox.php?wsdl'; // gunakan sandbox untuk coba-coba
            $url      = 'http://feeder.ubharajaya.ac.id/ws/live.php?wsdl'; // gunakan live bila sudah yakin
            $client   = new nusoap_client($url, true);
            $proxy    = $client->getProxy();
            $username = userfeeder;
            $password = passwordfeeder;
            $result   = $proxy->GetToken($username, $password);
            $token    = $result;
            $table    = 'mahasiswa_pt';

            // get mahasiswa lulusan
            $mhs_data = $this->db->query("SELECT lls.* FROM tbl_lulusan lls 
                                        JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = lls.`npm_mahasiswa`
                                        WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' 
                                        AND lls.`ta_lulus` = ".$this->session->userdata('tahun')."")->result();

            foreach ($mhs_data as $value) {     
                // status kelulusan 
                // 1 = lulus , 2 = mutasi, 3 = dikeluarkan, 4 = mengundurkan diri, 6 = wafat, 7 = hilang
                $data = [
                        'bln_awal_bimbingan'  => $value->mulai_bim, 
                        'bln_akhir_bimbingan' => $value->ahir_bim,
                        'judul_skripsi'       => $value->jdl_skripsi,
                        'jalur_skripsi'       => 1,
                        'no_seri_ijazah'      => $value->no_ijazah,
                        'ipk'                 => $value->ipk,
                        'tgl_sk_yudisium'     => $value->tgl_yudisium,
                        'tgl_keluar'          => $value->tgl_lulus,
                        'sk_yudisium'         => $value->sk_yudisium,
                        'id_jns_keluar'       => '1'
                        //'sks_diakui' => 144                                
                    ];

                // make array of list student to update student's status
                $sp        = $proxy->GetRecord($token, 'mahasiswa_pt', "nipd ilike '%".$value->npm_mahasiswa."%'");
                $key       = ['id_reg_pd' => $sp['result']['id_reg_pd']];
                $records[] = ['key' => $key, 'data' => $data];
                // get pembimbing 1
                $id_sdm                     = $proxy->GetRecord($token, 'dosen' , "nidn = '".$value->dospem1."'");
                $dospem1['id_sdm']          = $id_sdm['result']['id_sdm'];
                $dospem1['id_reg_pd']       = $sp['result']['id_reg_pd'];
                $dospem1['urutan_promotor'] = 1;

                // get pembimbing 2
                $id_sdm2                    = $proxy->GetRecord($token, 'dosen' , "nidn = '".$value->dospem2."'");
                $dospem2['id_sdm']          = $id_sdm2['result']['id_sdm'];
                $dospem2['id_reg_pd']       = $sp['result']['id_reg_pd'];
                $dospem2['urutan_promotor'] = 2;
                
                // sync pembinmbing 1
                $result10 = $proxy->InsertRecord($token, 'dosen_pembimbing', json_encode($dospem1));
                dd($result10['result']);
                echo '<hr>';

                // sync pembinmbing 2
                $result20 = $proxy->InsertRecord($token, 'dosen_pembimbing', json_encode($dospem2));
                dd($result10['result']);
                echo '<hr>';
            }

            // update status to lulus
            foreach ($records as $record) {
                $result = $proxy->UpdateRecord($token, $table, json_encode($record, JSON_FORCE_OBJECT));
                dd($result,2);
                dd($result);
                echo '<hr>';
            }

        } else {
            redirect(base_url().'sync_feed/lulusan','refresh');
        }
    }

    public function sync_mhs()
    {
        if ($this->session->userdata('jurusan')) {
            // get token
            $token = $this->pddikti->get_token();
            // get mahasiswa lulusan
            $mhs_data = $this->db->query("SELECT lls.* FROM tbl_lulusan lls 
                                        JOIN tbl_mahasiswa mhs ON mhs.`NIMHSMSMHS` = lls.`npm_mahasiswa`
                                        WHERE mhs.`KDPSTMSMHS` = '".$this->session->userdata('jurusan')."' 
                                        AND lls.`ta_lulus` = ".$this->session->userdata('tahun')."  ")->result();
            foreach ($mhs_data as $value) { 
                // get id pd (peserta didik)
                $id_reg_pd = $this->_get_id_pd($token, $value->npm_mahasiswa);

                // status kelulusan 
                // 1 = lulus , 2 = mutasi, 3 = dikeluarkan, 4 = mengundurkan diri, 6 = wafat, 7 = hilang
                $data = [
                        'id_registrasi_mahasiswa' => $id_reg_pd,
                        'id_jenis_keluar'         => '1',                     
                        'id_periode_keluar'       => $value->ta_lulus,
                        'tanggal_keluar'          => $value->tgl_lulus,
                        'keterangan'              => '',
                        'nomor_sk_yudisium'       => $value->sk_yudisium,
                        'tanggal_sk_yudisium'     => $value->tgl_yudisium,
                        'ipk'                     => $value->ipk,
                        'nomor_ijazah'            => $value->no_ijazah,
                        'jalur_skripsi'           => 1,
                        // 'judul_skripsi'           => $value->jdl_skripsi,
                        'bulan_awal_bimbingan'    => $value->mulai_bim, 
                        'bulan_akhir_bimbingan'   => $value->ahir_bim
                    ];

                // update graduation data of student
                $key    = ['id_registrasi_mahasiswa' => $id_reg_pd];
                $record = ['act' => "InsertMahasiswaLulusDO", 'token' => $token, 'record' => $data];
                $exec = $this->pddikti->runWS($record);
                $result = json_encode($exec);

                $response = new stdClass;
                $response->npm = $value->npm_mahasiswa;
                $response->status = $result->error_code == 0 ? 'Sinkronisasi berhasil!' : 'Sinkronisasi gagal!';
                $response->message = $result->error_desc;
                dd($response);
                echo '<hr>';

                /** sikronisasi pembimbing dilakukan pada menu aktifitas mahasiswa - change on Sept, 22 2020.
                // get pembimbing 1 and dispatch it
                $id_dosen1          = $this->_get_id_dosen($token, $value->dospem1);
                $insert_pembimbing1 = $this->_insert_dosen_pembimbing($token, $id_dosen1, $id_reg_pd, 1);
                dd($insert_pembimbing1);

                // get pembimbing 2 and dispatch it
                if (!is_null($value->dospem2)) {
                    $id_dosen2          = $this->_get_id_dosen($token, $value->dospem2);
                    $insert_pembimbing2 = $this->_insert_dosen_pembimbing($token, $id_dosen2, $id_reg_pd, 2);
                    dd($insert_pembimbing2); 
                    echo '<hr>';   
                } */            
            }

        } else {
            redirect(base_url().'sync_feed/lulusan','refresh');
        }
    }

    private function _get_id_pd($token, $nim)
    {
        $payload = [
            'act'    => "GetListRiwayatPendidikanMahasiswa",
            'token'  => $token,
            'filter' => "nim like '%$nim%'",
            'limit'  => 1,
            'offset' => 0
        ];
        $id_reg_pd = $this->pddikti->runWS($payload);
        $id_reg_pd = json_decode($id_reg_pd)->data[0]->id_registrasi_mahasiswa;
        return $id_reg_pd;
    }

    private function _get_id_dosen($token, $nidn)
    {
        $get_pembimbing = ['act' => "GetListDosen", 'token' => $token, 'filter' => "nidn = '$nidn'"];
        $pembimbing     = $this->pddikti->runWS($get_pembimbing);
        $id_dosen       = json_decode($pembimbing)->data[0]->id_dosen;
        return $id_dosen;
    }

    private function _insert_dosen_pembimbing($token, $id_dosen, $id_mahasiswa, $urutan_promotor)
    {
        // prepare payload data
        $data_bimbingan = [
            'id_dosen'                => $id_dosen,
            'id_registrasi_mahasiswa' => $id_mahasiswa,
            'pembimbing_ke'           => $urutan_promotor
        ];
        // dispatch data
        $insert_pembimbing = $this->pddikti->runWS([
            'act'    => "InsertDosenPembimbing", 
            'token'  => $token, 
            'record' => $data_bimbingan
        ]);

        return $insert_pembimbing;
    }

}

/* End of file Lulusan.php */
/* Location: ./application/controllers/Lulusan.php */