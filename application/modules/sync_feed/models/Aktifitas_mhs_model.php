<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktifitas_mhs_model extends CI_Model {

	public function get_aktifitas($prodi, $tahunakademik)
	{
		$act = $this->db->query("SELECT act.*, type.nama FROM tbl_aktifitas_mhs act
								JOIN tbl_jenis_aktifitas type ON act.jenis = type.kode
								WHERE act.deleted_at IS NULL
								AND act.prodi = '$prodi'
								AND act.tahunakademik = '$tahunakademik' ")->result();
		return $act;
	}

	public function get_detail_aktifitas($id)
	{
		$data = $this->db->get_where('tbl_aktifitas_mhs', ['id' => $id]);
		return $data;
	}

	public function get_available_activity($prodi, $tahunakademik)
	{
		$param = ['deleted_at IS NULL' => NULL, 'prodi' => $prodi, 'tahunakademik' => $tahunakademik];
		$act   = $this->db->get_where('tbl_aktifitas_mhs', $param)->result();
		return $act;
	}

	public function get_anggota($id)
	{
		$data = $this->db->query("SELECT 
									jns.nama, 
									mhs.NMMHSMSMHS, 
									mbr.id_anggota_feeder,
									mbr.npm, 
									mbr.peran,
									mbr.id AS id_mhs, 
									act.id, 
									act.id_aktifitas_feeder 
								FROM tbl_aktifitas_mhs act
								JOIN tbl_anggota_aktifitas mbr ON mbr.id_aktifitas = act.id
								JOIN tbl_mahasiswa mhs ON mbr.npm = mhs.NIMHSMSMHS
								JOIN tbl_jenis_aktifitas jns ON jns.id = act.jenis
								WHERE act.id = '$id'
								AND mbr.deleted_at IS NULL ")->result();
		return $data;
	}

	public function get_mhs($term, $prodi)
	{
		$mahasiswa = $this->db->query("SELECT NIMHSMSMHS, NMMHSMSMHS FROM tbl_mahasiswa 
										WHERE (NMMHSMSMHS LIKE '%$term%' OR NIMHSMSMHS LIKE '%$term%')
										AND KDPSTMSMHS = '$prodi' ")->result();
		return $mahasiswa;
	}

	public function get_pembimbing($id)
	{
		$dosen 	= $this->db->query("SELECT 
										jns.nama, 
										kry.nidn,
										kry.nupn, 
										kry.nama, 
										pbb.id, 
										pbb.nama_kegiatan, 
										pbb.no_urut_pembimbing,
										pbb.id_bimbing_feeder 
									FROM tbl_pembimbing_aktifitas pbb
									JOIN tbl_karyawan kry ON pbb.nid = kry.nid
									JOIN tbl_aktifitas_mhs act ON act.id = pbb.id_aktifitas
									JOIN tbl_jenis_aktifitas jns ON jns.kode = act.jenis
									WHERE act.id = $id
									AND pbb.deleted_at IS NULL")->result();
		return $dosen;
	}

	public function get_penguji($id)
	{
		$dosen 	= $this->db->query("SELECT 
										jns.nama, 
										kry.nidn,
										kry.nupn, 
										kry.nama, 
										pgj.id, 
										pgj.nama_kegiatan, 
										pgj.no_urut_penguji,
										pgj.id_penguji_feeder 
									FROM tbl_penguji_aktifitas pgj
									JOIN tbl_karyawan kry ON pgj.nid = kry.nid
									JOIN tbl_aktifitas_mhs act ON act.id = pgj.id_aktifitas
									JOIN tbl_jenis_aktifitas jns ON jns.id = act.jenis
									WHERE act.id = $id
									AND pgj.deleted_at IS NULL")->result();
		return $dosen;
	}

	public function get_dosen($term)
	{
		$dsn = $this->db->query("SELECT nid, nidn, nupn, nama FROM tbl_karyawan
								WHERE (nidn LIKE '%$term%' OR nama LIKE '%$term%')
								AND (nidn <> '' OR nupn <> '')")->result();
		return $dsn;
	}

	public function get_member($id)
	{
		$member = $this->db->query("SELECT act.id_aktifitas_feeder, mbr.npm, mbr.peran FROM tbl_anggota_aktifitas mbr
									JOIN tbl_aktifitas_mhs act ON mbr.id_aktifitas = act.id
									WHERE mbr.id_aktifitas = $id ")->result();
		return $member;
	}

	public function get_guide($id)
	{
		$guide = $this->db->query("SELECT 
										gd.id,
										gd.nidn,
										gd.no_urut_pembimbing, 
										gd.id_kegiatan, 
										act.no_sk_tugas,
										act.id_aktifitas_feeder 
									FROM tbl_pembimbing_aktifitas gd
									JOIN tbl_aktifitas_mhs act ON gd.id_aktifitas = act.id
									WHERE gd.id_aktifitas = $id
									AND gd.deleted_at IS NULL ")->result();
		return $guide;
	}

	public function get_examiner($id)
	{
		$guide = $this->db->query("SELECT 
										exm.id,
										exm.nidn,
										exm.no_urut_penguji, 
										exm.id_kegiatan, 
										act.no_sk_tugas,
										act.id_aktifitas_feeder 
									FROM tbl_penguji_aktifitas exm
									JOIN tbl_aktifitas_mhs act ON exm.id_aktifitas = act.id
									WHERE exm.id_aktifitas = $id
									AND exm.deleted_at IS NULL ")->result();
		return $guide;
	}

	public function available_member($id_act, $npm)
	{
		$member = $this->db->query("SELECT * FROM tbl_anggota_aktifitas 
									WHERE id_aktifitas = $id_act 
									AND npm = '$npm' 
									AND deleted_at IS NULL");
		return $member;
	}

}

/* End of file Aktifitas_mhs_model.php */
/* Location: ./application/modules/sync_feed/models/Aktifitas_mhs_model.php */