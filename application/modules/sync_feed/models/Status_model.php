<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_model extends CI_Model {

	function get_do($prodi, $tahunakademik)
	{
		$this->db->select('skep,tgl_skep,npm_mahasiswa,alasan,tahunajaran');
        $this->db->from('tbl_dropout');
        $this->db->where('tahunajaran', $tahunakademik);
        $this->db->where('audit_user', $prodi);
        $data = $this->db->get()->result();	
        return $data;
	}

	function get_out_student($prodi, $angkatan)
	{
		$out = $this->db->query("SELECT *, 

									(SELECT MAX(THSMSTRAKM) FROM tbl_aktifitas_kuliah_mahasiswa 
									WHERE NIMHSTRAKM = NIMHSMSMHS) AS last_active,

									(SELECT SKSTTTRAKM FROM tbl_aktifitas_kuliah_mahasiswa
									WHERE THSMSTRAKM = last_active
									AND NIMHSTRAKM = NIMHSMSMHS) AS sks,

									(SELECT NLIPKTRAKM FROM tbl_aktifitas_kuliah_mahasiswa
									WHERE THSMSTRAKM = last_active
									AND NIMHSTRAKM = NIMHSMSMHS) AS ipk

								FROM tbl_mahasiswa 
								WHERE (STMHSMSMHS = 'K' OR STMHSMSMHS = 'D')
								AND KDPSTMSMHS = '$prodi'
								AND NIMHSMSMHS NOT IN 
									(SELECT npm FROM tbl_pengunduran
									WHERE tipe = 'K')
								AND TAHUNMSMHS = '$angkatan'")->result();
		return $out;
	}

	function get_out_list($prodi, $angkatan)
	{
		$out = $this->db->query("SELECT * FROM tbl_pengunduran WHERE prodi = '$prodi' AND npm LIKE '$angkatan%'")->result();
		return $out;
	}

}

/* End of file Status_model.php */
/* Location: ./application/modules/sync_feed/models/Status_model.php */