<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('sess_jadwal') == '') {

			//var_dump($this->session->userdata('sess_login'));

			redirect('absensi/login','refresh');

			//die('asddsasdasad');

		} else {
			$user = $this->session->userdata('sess_jadwal');
			// $res = explode('/', $user['kd_jdl']);
			$data['kelas'] = $this->app_model->get_kelas_mahasiswa($user['id_jdl'])->result();
			$tanggal = $this->db->query("select distinct tanggal,max(pertemuan) AS pertemuan from tbl_absensi_mhs where kd_jadwal = '".$user['kd_jdl']."' order by id_absen desc limit 1")->row();
			$data['tang'] = $tanggal->pertemuan+1;
			$this->db->select('a.nid,a.nama,b.kelas,c.nama_matakuliah')->from('tbl_karyawan a')
			->join('tbl_jadwal_matkul b','a.nid = b.kd_dosen')->join('tbl_matakuliah c','c.kd_matakuliah = b.kd_matakuliah')
			->where('b.kd_jadwal',$user['kd_jdl']);
			$data['nm'] = $this->db->get()->row();
			// $data['kelas'] = $this->app_model->get_data_jadwal($user['id_jdl'],$res[0])->row();
			$data['max'] = $this->db->query("select count(npm_mahasiswa) as mhs from tbl_krs where kd_jadwal = '".$user['kelas']."'")->row()->mhs;
			$data['id'] = $user['id_jdl'];
			$data['page'] = "v_abs_dsn";
			$this->load->view('template/template', $data);

		}
	}

	function login()
	{
		$this->load->view('v_login_jdl');
	}

	function verif_kode()
	{
		$user = $this->input->post('kd', TRUE);

		$cek = $this->login_model->cek_kode($user)->result();

			if (count($cek) > 0) {

				foreach ($cek as $key) {
					//die('tolol');

					$session['userid'] = $key->kd_dosen;

					$session['kd_jdl'] = $key->kd_jadwal;

					$session['kd_ta'] = $key->kd_tahunajaran;

					$session['kelas'] = $key->kelas;

					$session['kd_mk'] = $key->kd_matakuliah;

					$session['id_jdl'] = $key->id_jadwal;

					$this->session->set_userdata('sess_jadwal',$session);

					$this->index();

				}

			} else {

				//echo "Gagal Login , <a href=".base_url()."auth> Back >> </a>";

				echo "<script>alert('Akses Ditolak !');history.go(-1);</script>";

			}
	}

	function list_abs()
	{
		$sesi = $this->session->userdata('sess_jadwal');
		$rt = $sesi['kd_jdl'];
		$data['id_jadwal'] = $sesi['id_jdl'];
		// var_dump($rt);exit();
		$data['pertemuan'] = $this->db->query('SELECT DISTINCT pertemuan,tanggal,bahasan FROM tbl_absensi_mhs WHERE kd_jadwal = "'.$rt.'" GROUP BY pertemuan,tanggal')->result();
		$data['page'] = "v_list_abs";
		$this->load->view('template/template', $data);
	}

	function out()
	{
		$this->session->unset_userdata('sess_jadwal');

		//$this->session->sess_destroy();

        redirect('absensi/absen', 'refresh');
	}

	function kelas_absen_copy($id)
	{
		// var_dump($id);exit();
		$jumlahabsen=$this->input->post('jumlah', TRUE);
		$kode = $this->app_model->getdetail('tbl_jadwal_matkul','id_jadwal',$id,'id_jadwal','asc')->row();
		$cek = $this->db->query("select count(distinct tanggal) as jml from tbl_absensi_mhs where kd_jadwal = '".$kode->kd_jadwal."'")->row()->jml;
		$tanggal = $this->db->query("select distinct tanggal,max(pertemuan) AS pertemuan from tbl_absensi_mhs where kd_jadwal = '".$kode->kd_jadwal."' order by id_absen desc limit 1")->row();
		$data['tang'] = $tanggal->pertemuan+1;
		//var_dump($cek);exit();
		if ($cek > 0) {
			if ($tanggal->tanggal == $this->input->post('tgl',TRUE)) {
				//$pertemuan =  1;
				echo "<script>alert('Absensi Sudah Dilakukan Pada Tanggal Tersebut');
				history.go(-1);</script>";
			} else {
				$a = 1;
				for ($i=0; $i < $jumlahabsen-1; $i++) { 
					$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
					//echo 'absen'.$a.'['.$i.']';
					$pecah = explode('-', $hasil);
					//var_dump($hasil);
					$datax[] = array(
						'npm_mahasiswa' => $pecah[1],
						'kd_jadwal' => $kode->kd_jadwal,
						'tanggal' => $this->input->post('tgl',TRUE),
						'pertemuan' => $tanggal->pertemuan+1,
						'kehadiran' => $pecah[0],
						'bahasan'	=> $this->input->post('bahas')
						);
					// $data[]['npm_mahasiswa'] = $pecah[1];
					// $data[]['kd_jadwal'] = $kode->kd_jadwal;
					// $data[]['tanggal'] = $this->input->post('tgl',TRUE);
					// $data[]['pertemuan'] = $tanggal->pertemuan+1;
					// $data[]['kehadiran'] = $pecah[0];
					//var_dump($data);exit();
					if (($tanggal->pertemuan+1) > 16) {
						echo "<script>alert('Absen Melebihi Jumlah 16 Pertemuan');
						document.location.href='".base_url()."akademik/ajar/load_list_dosen/';</script>";
						exit();
					}
					$a++;
				}
				// var_dump($datax);exit();
				$this->db->insert_batch('tbl_absensi_mhs',$datax);
				
				echo "<script>alert('Sukses');
				document.location.href='".base_url()."absensi/absen/list_abs/';</script>";
				
			}
		} else {
			$a = 1;
			for ($i=0; $i < $jumlahabsen-1; $i++) { 
				$hasil = $this->input->post('absen'.$a.'[0]', TRUE);
				//echo 'absen'.$a.'['.$i.']';
				$pecah = explode('-', $hasil);
				//var_dump($hasil);
				$datax[] = array(
					'npm_mahasiswa' => $pecah[1],
					'kd_jadwal' => $kode->kd_jadwal,
					'tanggal' => $this->input->post('tgl',TRUE),
					'pertemuan' => 1,
					'kehadiran' => $pecah[0],
					'bahasan'	=> $this->input->post('bahas')
					);
				//$data[]['npm_mahasiswa'] = $pecah[1];
				//$data[]['kd_jadwal'] = $kode->kd_jadwal;
				//$data[]['tanggal'] = $this->input->post('tgl',TRUE);
				//$data[]['pertemuan'] = 1;
				//$data[]['kehadiran'] = $pecah[0];
				//var_dump($data);exit();
				//$q = $this->app_model->insertdata('tbl_absensi_mhs',$data);
				$a++;
			}
			// var_dump($datax);exit();
			$this->db->insert_batch('tbl_absensi_mhs',$datax);
			
			echo "<script>alert('Sukses');
			document.location.href='".base_url()."absensi/absen/list_abs/';</script>";
		}
		
	}

	function modal_edit_absen_copy($id,$pertemuan)
	{
		clearstatcache();
		$q = $this->db->query('select kd_jadwal from tbl_jadwal_matkul where id_jadwal = '.$id.'')->row()->kd_jadwal;

		$data['pertemuan'] = $pertemuan;
		$data['kode_jadwal'] = $q;
		$data['tanggal'] = $this->db->query('SELECT DISTINCT tanggal FROM tbl_absensi_mhs WHERE pertemuan = '.$pertemuan.' AND kd_jadwal = "'.$q.'"')->row()->tanggal;
		$data['kelas'] = $this->db->query('SELECT DISTINCT krs.`npm_mahasiswa`,mhs.`NMMHSMSMHS`,
											(SELECT kehadiran FROM tbl_absensi_mhs WHERE kd_jadwal = "'.$q.'" AND pertemuan = '.$pertemuan.' AND npm_mahasiswa = krs.`npm_mahasiswa`) as kehadiran
											FROM tbl_krs krs 
											JOIN tbl_mahasiswa mhs ON krs.`npm_mahasiswa` = mhs.`NIMHSMSMHS`
											WHERE krs.`kd_jadwal` = "'.$q.'" ORDER BY mhs.NIMHSMSMHS ASC')->result();
		$data['id'] = $id;
		$this->load->view('v_list', $data);
	}

}

/* End of file Absen.php */
/* Location: ./application/modules/absensi/controllers/Absen.php */