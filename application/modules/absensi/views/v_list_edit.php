<script type="text/javascript">
    $(function() {
        $("#example4").dataTable();
    });
</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> Daftar Hadir Mahasiswa - Pertemuan Ke-<?php echo $pertemuan ?></h4>
</div>
<form class='form-horizontal' action="<?php echo base_url(); ?>absensi/edit_kelas_absen/<?php echo $id; ?>" method="post">
    <div class="modal-body">
        <div class="control-group" id="">
            <fieldset>
                <div class="control-group" style="margin-left: -20px;">
                    <label class="control-label">Tanggal Pertemuan</label>
                    <div class="controls">
                        <input class="span2" type="text" name="tanggal" value="<?php echo $tanggal; ?>" id="tgl" required />
                    </div>
                </div>
                <div class="control-group" style="margin-left: -20px;">
                    <label for="" class="control-label">Bahasan</label>
                    <div class="controls">
                        <textarea name="materi" rows="2" cols="50"><?php echo $bahasan->materi; ?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <div class="alert-danger">
                        Jumlah Kehadiran : <b>Hadir(H) = <?php echo $h->jums; ?></b> | <b>Sakit(S) = <?php echo $s->jums; ?></b> | <b>Izin(I) = <?php echo $i->jums; ?></b> |
                        <b>Alfa(A) = <?php echo $a->jums; ?></b>
                    </div>
                </div>
            </fieldset>
            <input type="hidden" name="pertemuan">
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th width="90">NIM</th>
                        <th>NAMA</th>
                        <th width="20">HADIR</th>
                        <th width="20">SAKIT</th>
                        <th width="20">IZIN</th>
                        <th width="20">ALPHA</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($kelas as $value) { ?>
                        <!--input type="hidden" name="jadwal" value="<?php //echo $value->kd_jadwal; 
                                                                        ?>"/-->
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $value->npm_mahasiswa; ?></td>
                            <td><?php echo $value->NMMHSMSMHS; ?></td>
                            <td><input type="radio" class="idabsen1" name="absen<?php echo $no; ?>[]" value="H-<?php echo $value->npm_mahasiswa . '-' . $value->kd_jadwal; ?>" <?php if ($value->kehadiran == "H") {
                                                                                                                                                                                    echo "checked";
                                                                                                                                                                                } ?> required /></td>
                            <td><input type="radio" name="absen<?php echo $no; ?>[]" value="S-<?php echo $value->npm_mahasiswa . '-' . $value->kd_jadwal; ?>" <?php if ($value->kehadiran == "S") {
                                                                                                                                                                    echo "checked";
                                                                                                                                                                } ?> /></td>
                            <td><input type="radio" name="absen<?php echo $no; ?>[]" value="I-<?php echo $value->npm_mahasiswa . '-' . $value->kd_jadwal; ?>" <?php if ($value->kehadiran == "I") {
                                                                                                                                                                    echo "checked";
                                                                                                                                                                } ?> /></td>
                            <td><input type="radio" name="absen<?php echo $no; ?>[]" value="A-<?php echo $value->npm_mahasiswa . '-' . $value->kd_jadwal; ?>" <?php if ($value->kehadiran == "A") {
                                                                                                                                                                    echo "checked";
                                                                                                                                                                } ?> /></td>

                        </tr>
                        <input type="hidden" name="kd_jadwal[]" value="<?php echo $value->kd_jadwal ?>">
                    <?php $no++;
                    } ?>
                    <input type="hidden" name="jumlah" value="<?php echo $no - 1; ?>">
                    <input type="hidden" name="pertemuan" value="<?php echo $pertemuan; ?>" />
                    <input type="hidden" name="tanggal_sebelumnya" value="<?php echo $tanggal; ?>" id="tanggal_sebelumnya" />
                    <input type="hidden" name="id_jadwal" value="<?php echo $kode_jadwal; ?>" />
                </tbody>
            </table>
            <script>
                $(function() {
                    $("#tgl").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "yy-mm-dd",
                        minDate: -150,
                        maxDate: 0,
                    });
                    $("#tgl").keydown(function(event) {
                        event.preventDefault();
                    });
                });
            </script>

        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Ubah Absen" />
    </div>
</form>