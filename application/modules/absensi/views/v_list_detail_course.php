<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Rincian materi kuliah</h4>
</div>
<div class="modal-content">
	<table class="table table-bordered table-hovered">
		<thead>
			<tr>
				<th>No</th>
				<th>Pertemuan</th>
				<th>Materi</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 1; foreach ($detail as $key => $value): ?>
				<tr>
					<td><?php echo $i++; ?></td>
					<td>Pertemuan ke <?php echo $value->pertemuan; ?></td>
					<td><?php echo $value->materi; ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<div class="modal-footer">
	<button class="btn btn-default" data-dismiss="modal" >Close</button>
</div>