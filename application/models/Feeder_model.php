<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feeder_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function isthisngulang($code,$uid,$own,$year)
	{
		$loo = $this->db->where('kd_lama', $code)
						->where('kd_prodi', $own)
						->where('flag', '1')
						->get('tbl_konversi_matkul_temp');

		if ($loo->num_rows() > 0) {

			$this->db->where('KDKMKTRLNM', $loo->row()->kd_baru);
			$this->db->where('NIMHSTRLNM', $uid);
			$this->db->where('THSMSTRLNM <', $year);
			return $this->db->get('tbl_transaksi_nilai');

		} else {

			$this->db->where('KDKMKTRLNM', $code);
			$this->db->where('NIMHSTRLNM', $uid);
			$this->db->where('THSMSTRLNM <', $year);
			return $this->db->get('tbl_transaksi_nilai');

		}
	}

	/*
    /
    / Mengambil data maba berdasarkan tahun masuk dan prodi
    /
    */
    public function get_maba($angkatan, $prodi){
    	$siabaru = $this->load->database('siabaru', TRUE);
        $sql = "select
                    c.no_ujian as no_reg,
                    c.kampus_pilihan,
                    d.KDPSTMSMHS as kode_prodi,
                    d.SMAWLMSMHS,
                    a.npm,
                    c.nama,
                    (select nama from peembe.gelombang where id=c.id_gelombang) as jenis_daftar,
					if((select id_jalur from peembe.gelombang where id=c.id_gelombang)=9,'Konversi','Reguler') as status_pendaftaran,
                    c.sex as jk,
                    if(c.sex = '1','L','P') as jenis_kelamin,
                    c.no_induk as nisn,
                    c.no_identitas as nik, 
                    c.agama,
                    c.no_kps,
                    c.nama_ayah,
                    c.nama_ibu,
                    c.kewarganegaraan,
                    c.alamat_jalan as jalan,
                    c.alamat_rt as rt,
                    c.alamat_rw as rw,
                    c.alamat_desa as desa,
                    c.pob as tpt_lahir,
                    c.dob as tgl_lahir, 
                    c.email,
                    c.no_hp as handphone
                from 
                    siakadonline.tbl_bio_mhs as a 
                join 
                    siakadonline.tbl_register as b 
                on 
                    a.npm = b.npm 
                join 
                    peembe.pendaftar as c 
                on 
                    b.no_ujian = c.no_ujian 
                join 
                    siakadonline.tbl_mahasiswa as d 
                on 
                    a.npm = d.NIMHSMSMHS 
                where 
                    substr(a.npm,1,5) = '$angkatan'
                and 
                    d.KDPSTMSMHS = '$prodi'";
        
        $sql .= " LIMIT 1";

        return $siabaru->query($sql);
    }
}

/* End of file Feeder_model.php */
/* Location: ./application/models/Feeder_model.php */