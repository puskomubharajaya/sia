<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sp_model extends CI_Model {

	function jdl_matkul_by_semester_sp($smtr){
		$q = $this->db->query('SELECT distinct mk.audit_user,mk.kd_jadwal,mk.id_jadwal,mk.kelas,mk.kd_matakuliah,mk.hari,mk.waktu_mulai,mk.waktu_selesai,a.nama_matakuliah,a.sks_matakuliah,r.*,kry.nama FROM tbl_jadwal_matkul mk
                                JOIN tbl_matakuliah a ON mk.`kd_matakuliah`=a.`kd_matakuliah`
                                left JOIN tbl_ruangan r ON r.`id_ruangan` = mk.`kd_ruangan`
                                JOIN tbl_fakultas b ON a.`kd_fakultas` = b.`kd_fakultas`
                                LEFT JOIN tbl_karyawan kry ON kry.`nid` = mk.`kd_dosen`
                                JOIN tbl_kurikulum_matkul km ON mk.`kd_matakuliah` = km.`kd_matakuliah`
                                WHERE mk.`kd_jadwal` LIKE "'.$this->session->userdata('id_jurusan_prasyarat').'%"
                                AND a.`kd_prodi` = "'.$this->session->userdata('id_jurusan_prasyarat').'"
                                AND km.`kd_kurikulum` LIKE "'.$this->session->userdata('id_jurusan_prasyarat').'%"
                                AND mk.`kd_tahunajaran` = "'.$this->session->userdata('tahunajaran').'"
                                AND km.`semester_kd_matakuliah` = '.$smtr.'
                                order by mk.kd_matakuliah ASC')->result();
		return $q;

	}

}

/* End of file sp_model.php */
/* Location: ./application/models/sp_model.php */