<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class App_model2 extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('sqlserver', TRUE);
	}

	function getdata($table,$key,$order)

	{

		$this->db2->order_by($key, $order);

		$q = $this->db2->get($table);

		return $q;

	}



	function insertdata($table,$data)

	{

		$q = $this->db2->insert($table, $data);

		return $q;

	}

	

	function get_lembaga(){

		return $this->db2->get('tbl_lembaga')->result();

	}

	function get_KDPSTMSMHS($mhs){
		$this->db2->select('KDPSTMSMHS');
		$this->db2->from('tbl_mahasiswa');
		$this->db2->where('NIMHSMSMHS', $mhs);
		return $this->db2->get();

	}

	

	function get_jabatan(){

		$this->db2->select('a.*,b.divisi');

		$this->db2->from('tbl_jabatan a');

		$this->db2->join('tbl_divisi b','a.kd_divisi = b.kd_divisi');

		return $this->db2->get()->result();

	}



	function getdetail($table,$pk,$value,$key,$order)

	{

		$this->db2->where($pk,$value);

		$this->db2->order_by($key, $order);

		$q = $this->db2->get($table);

		return $q;

	}



	function updatedata($table,$pk,$value,$data)

	{

		$this->db2->where($pk,$value);

		$q = $this->db2->update($table, $data);

		return $q;

	}

	function updatedata_by_smt($table,$pk,$value,$data,$tahunajaran)

	{

		$this->db2->where($pk,$value);

		$this->db2->where("tahunajaran",$tahunajaran);


		$q = $this->db2->update($table, $data);

		return $q;

	}


	function updatedatamk($table,$pk1,$value1,$pk2,$value2,$data)

	{

		$this->db2->where($pk2,$value2);

		$this->db2->where($pk1,$value1);

		$q = $this->db2->update($table, $data);

		return $q;

	}



	function deletedata($table,$pk,$value)

	{

		$this->db2->where($pk,$value);

		$q = $this->db2->delete($table);

		return $q;

	}

	

	function getlistjab($id){

		$this->db2->where('lembaga_id',$id);

		return $this->db2->get('tbl_jabatan')->result();

	}

	

	function get_karyawan(){

		$this->db2->select('a.*,b.jabatan,c.divisi');

		$this->db2->from('tbl_karyawan a');

		$this->db2->join('tbl_jabatan b','a.jabatan_id = b.id_jabatan');

		$this->db2->join('tbl_divisi c','b.kd_divisi = c.kd_divisi','left');

		$this->db2->where('status', 1);

		$this->db2->order_by('nama', 'asc');

		return $this->db2->get()->result();

	}

	

	function get_jabatan_user($id){

		$this->db2->select('a.*,b.jabatan,c.kd_divisi,c.divisi');

		$this->db2->from('tbl_karyawan a');

		$this->db2->join('tbl_jabatan b','a.jabatan_id = b.id_jabatan');

		$this->db2->join('tbl_divisi c','b.kd_divisi = c.kd_divisi');

		$this->db2->where('a.nid',$id);

		return $this->db2->get();

	}



	function get_matkul_by_kurikulum(){

		$this->db2->select('a.kd_fakultas,a.kd_prodi,b.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah,d.tahunajaran');

		$this->db2->from('tbl_kurikulum a');

		$this->db2->join('tbl_kurikulum_matkul b', 'a.kd_kurikulum = b.kd_kurikulum', 'left');

		$this->db2->join('tbl_matakuliah c', 'b.kd_matakuliah = c.kd_matakuliah', 'left');

		$this->db2->join('tbl_tahunajaran d', 'a.tahun_ajaran_kurikulum = d.id_tahunajaran', 'left');

		$this->db2->where('a.kd_prodi',$prodi);

		$this->db2->where('d.tahunajaran',$tahunajaran);

		return $this->db2->get();

	}

	

	function get_detail_matakuliah($fakultas, $jurusan){

		$this->db2->select('a.*,b.fakultas,c.prodi');

		$this->db2->from('tbl_matakuliah a');

		$this->db2->join('tbl_fakultas b','a.kd_fakultas = b.kd_fakultas');

		$this->db2->join('tbl_jurusan_prodi c','a.kd_prodi = c.kd_prodi');

		$this->db2->where('a.kd_fakultas',$fakultas);

		$this->db2->like('a.kd_prodi',$jurusan,'both');

		$this->db2->order_by('kd_matakuliah', 'asc');

		return $this->db2->get()->result();

	}



	function get_detail_jdl_matakuliah($fakultas, $jurusan){

		$this->db2->select('a.*,b.fakultas,c.prodi,mk.*,r.*,kry.*');

		$this->db2->from('tbl_jadwal_matkul mk');

		$this->db2->join('tbl_matakuliah a','a.kd_matakuliah = mk.kd_matakuliah','left');

		$this->db2->join('tbl_karyawan kry', 'kry.nid = mk.kd_dosen', 'left');

		$this->db2->join('tbl_ruangan r', 'r.id_ruangan = mk.kd_ruangan', 'left');

		

		//$this->db2->join('tbl_fakultas b','a.kd_fakultas = b.kd_fakultas');

		//$this->db2->join('tbl_jurusan_prodi c','a.kd_prodi = c.kd_prodi');

		$this->db2->where('a.kd_fakultas',$fakultas);

		$this->db2->where('a.kd_prodi',$jurusan);

		return $this->db2->get()->result();

	}



	function get_detail_matakuliah_by_semester($fakultas, $jurusan, $semester){

		$this->db2->select('a.*,b.fakultas,c.prodi');

		$this->db2->from('tbl_matakuliah a');

		$this->db2->join('tbl_fakultas b','a.kd_fakultas = b.kd_fakultas');

		$this->db2->join('tbl_jurusan_prodi c','b.kd_fakultas = c.kd_fakultas');

		//$this->db2->where('b.kd_fakultas',$fakultas);

		$this->db2->where('c.kd_prodi',$jurusan);

		$this->db2->where('a.semester_matakuliah',$semester);

		return $this->db2->get()->result();

	}

	

	function get_detail_kurikulum(){

		$this->db2->select('a.*,b.fakultas,c.prodi,d.*');

		$this->db2->from('tbl_kurikulum a');

		$this->db2->join('tbl_fakultas b','a.kd_fakultas = b.kd_fakultas');

		$this->db2->join('tbl_jurusan_prodi c','a.kd_prodi = c.kd_prodi');

		$this->db2->join('tbl_tahunajaran d','d.id_tahunajaran = a.tahun_ajaran_kurikulum');

		return $this->db2->get()->result();

	}

	function get_detail_kurikulum_prodi($prodi){

		$this->db2->select('a.*,b.fakultas,c.prodi,d.*');

		$this->db2->from('tbl_kurikulum a');

		$this->db2->join('tbl_fakultas b','a.kd_fakultas = b.kd_fakultas');

		$this->db2->join('tbl_jurusan_prodi c','a.kd_prodi = c.kd_prodi');

		$this->db2->join('tbl_tahunajaran d','d.id_tahunajaran = a.tahun_ajaran_kurikulum');

		$this->db2->where('c.kd_prodi', $prodi);

		return $this->db2->get()->result();

	}

	function get_detail_kurikulum_fakultas($fak)
	{
		$this->db2->select('a.*,b.fakultas,c.prodi,d.*');

		$this->db2->from('tbl_kurikulum a');

		$this->db2->join('tbl_fakultas b','a.kd_fakultas = b.kd_fakultas');

		$this->db2->join('tbl_jurusan_prodi c','a.kd_prodi = c.kd_prodi');

		$this->db2->join('tbl_tahunajaran d','d.id_tahunajaran = a.tahun_ajaran_kurikulum');

		$this->db2->where('b.kd_fakultas', $fak);

		return $this->db2->get()->result();
	}

	function get_matkul_krs($semester, $prodi, $npm){
		
		$mk = $this->db2->query('select KDKMKTRLNM from tbl_transaksi_nilai where NIMHSTRLNM="'.$npm.'"')->row_array();
		$this->db2->distinct();
		$this->db2->select('a.*,b.*,c.*,d.*');
		$this->db2->from('tbl_kurikulum_matkul a');
		$this->db2->join('tbl_matakuliah b','a.kd_matakuliah = b.kd_matakuliah');
		$this->db2->join('tbl_kurikulum c','a.kd_kurikulum = c.kd_kurikulum');
		$this->db2->join('tbl_tahunajaran d','d.id_tahunajaran = c.tahun_ajaran_kurikulum');

		$thn = $this->db2->query('SELECT TAHUNMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS="'.$npm.'"')->row();
		$ajaran = $this->db2->query('SELECT a.kd_kurikulum, b.id_tahunajaran FROM tbl_kurikulum a JOIN tbl_tahunajaran b ON ((b.id_tahunajaran = a.tahun_ajaran_kurikulum)) where a.kd_prodi="'.$prodi.'" AND SUBSTR(b.tahunajaran, 1, 4) >= "'.$thn->TAHUNMSMHS.'" ORDER BY id_tahunajaran ASC LIMIT 1')->row();
		if (count($ajaran) > 0) {
			$tahun = $ajaran->id_tahunajaran;
		} else {
			$ajaran = $this->db2->query('SELECT a.kd_kurikulum, b.id_tahunajaran FROM tbl_kurikulum a JOIN tbl_tahunajaran b ON ((b.id_tahunajaran = a.tahun_ajaran_kurikulum)) where a.kd_prodi="'.$prodi.'" AND SUBSTR(b.tahunajaran, 1, 4) <= "'.$thn->TAHUNMSMHS.'" ORDER BY id_tahunajaran DESC LIMIT 1')->row();
			$tahun = $ajaran->id_tahunajaran;
		}
		
		
		
		$this->db2->where('d.id_tahunajaran',$tahun);
		$this->db2->where('a.semester_kd_matakuliah',$semester);
		$this->db2->where('b.kd_prodi',$prodi);
		$this->db2->where('c.kd_prodi',$prodi);
		$this->db2->where('c.status','1');
		
		//$this->db2->where_not_in('a.kd_matakuliah',$mk);

		return $this->db2->get();

	}

	

	function get_matkul_krs_rekam($semester, $npm){

		$this->db2->distinct();

		$this->db2->select('a.*, b.nama_matakuliah,b.sks_matakuliah');

		$this->db2->from('tbl_krs a');

		$this->db2->join('tbl_matakuliah b','a.kd_matakuliah = b.kd_matakuliah');

		$this->db2->where('a.npm_mahasiswa',$npm);

		$this->db2->where('a.semester_krs',$semester);

		return $this->db2->get();

	}

	

	function get_prasyarat($prasyarat){

		$this->db2->select('a.*');

		$this->db2->from('tbl_transaksi_nilai a');

		$this->db2->where_in('a.KDKMKTRLNM', $prasyarat);

		return $this->db2->get();

	}

	

	function get_matakuliah_kurikulum($id,$semester){

		/*$this->db2->distinct();

		$this->db2->select('a.*,b.*,c.*');

		$this->db2->from('tbl_kurikulum_matkul a');

		$this->db2->join('tbl_matakuliah b','a.kd_matakuliah = b.kd_matakuliah');

		$this->db2->join('tbl_kurikulum c','a.kd_kurikulum = c.kd_kurikulum');

		$this->db2->join('tbl_jurusan_prodi d',' REPLACE(REPLACE(d.kd_prodi, "\r", ""), "\n", "") = REPLACE(REPLACE(c.kd_prodi, "\r", ""), "\n", "")');

		$this->db2->where('c.kd_kurikulum',$id);

		$this->db2->where('b.kd_prodi','c.kd_prodi');

		$this->db2->where('b.semester_matakuliah',$semester);

		return $this->db2->get()->result();*/

		return $this->db2->query("SELECT a.*,b.*,c.* FROM tbl_kurikulum_matkul a 
				JOIN tbl_matakuliah b ON((a.kd_matakuliah = b.kd_matakuliah)) 
				JOIN tbl_kurikulum c ON((a.kd_kurikulum = c.kd_kurikulum))
				WHERE c.kd_kurikulum = '".$id."' 
				AND b.kd_prodi = REPLACE(REPLACE(c.kd_prodi, '\r', ''), '\n', '') 
				AND a.semester_kd_matakuliah='".$semester."' 
				ORDER BY b.kd_matakuliah ASC")->result();

	}

	

	function get_matakuliah_baru($id){

		$this->db2->select('a.*,b.*');

		$this->db2->from('tbl_kurikulum a');		

		$this->db2->join('tbl_matakuliah b','b.kd_prodi = REPLACE(REPLACE(a.kd_prodi, "\r", ""), "\n", "")');

		$this->db2->where('a.kd_kurikulum',$id);
		
		//$kd = $this->db2->query('select kd_prodi from tbl_kurikulum where kd_kurikulum="'.$id.'"')->row()->kd_prodi;
		//if(trim($kd) == '62201' || trim($kd) == '70201' || trim($kd) == '55201'){
			$this->db2->like('kd_matakuliah','-','both');
		//}

		//$this->db2->join('tbl_kurikulum_matkul c',' c.kd_kurikulum ="'.$id.'"');

		$mk = $this->db2->query('select kd_matakuliah from tbl_kurikulum_matkul where kd_kurikulum="'.$id.'"');

		if($mk->num_rows() > 0){

			foreach ($mk->result() as $row) {

				$mkt[] = $row->kd_matakuliah;

			}		

			$this->db2->where_not_in('b.kd_matakuliah',$mkt);

		}

		$this->db2->order_by('b.kd_matakuliah', 'asc');

		return $this->db2->get()->result();

	}

	

	function get_all_krs($npm){		

		return $this->db2->query('SELECT SUM(sks_matakuliah) AS jum_sks, (SUBSTR(b.THSMSTRLNM - c.SMAWLMSMHS, 0,1) * 2) + (SUBSTR(b.THSMSTRLNM - c.SMAWLMSMHS, 1,2) + 1) AS semester_krs, a.*, b.*, c.NIMHSMSMHS FROM tbl_matakuliah a JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="'.$npm.'" GROUP BY b.THSMSTRLNM');

	}

	
	function get_all_krs_mahasiswa($npm){		
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="'.$npm.'"')->row();
				
		return $this->db2->query('SELECT SUM(sks_matakuliah) AS jum_sks, (SUBSTR(SUBSTR(b.kd_krs, 13, 5) - c.SMAWLMSMHS, 0,1) * 2) + (SUBSTR(SUBSTR(b.kd_krs, 13, 5) - c.SMAWLMSMHS, 1,2) + 1) AS semester_krs, a.*, b.*, c.NIMHSMSMHS 

				FROM tbl_matakuliah a 

				JOIN tbl_krs b ON ((a.kd_matakuliah=b.kd_matakuliah)) 

				JOIN tbl_mahasiswa c ON ((b.npm_mahasiswa=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  

				WHERE b.npm_mahasiswa="'.$npm.'" AND a.kd_prodi="'.$kd_prodi->KDPSTMSMHS.'"

				GROUP BY b.kd_krs');

	}

	

	function get_detail_krs_mahasiswa($id){
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="'.substr($id, 0, 12).'"')->row();
		$this->db2->distinct();
		$this->db2->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.kelas,
		c.kd_jadwal,c.hari,c.waktu_mulai,c.waktu_selesai,d.*,e.*');
		$this->db2->from('tbl_krs a');
		$this->db2->join('tbl_matakuliah b','a.kd_matakuliah=b.kd_matakuliah');
		$this->db2->join('tbl_jadwal_matkul c','a.kd_jadwal=c.kd_jadwal', 'left');
		$this->db2->join('tbl_karyawan d','c.kd_dosen=d.nid', 'left');
		$this->db2->join('tbl_ruangan e','c.kd_ruangan=e.id_ruangan', 'left');
		$this->db2->where('b.kd_prodi',$kd_prodi->KDPSTMSMHS);
		$this->db2->where('a.kd_krs',$id);
		return $this->db2->get();

	}

	function get_detail_krs_mahasiswa_feeder($id){
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="'.substr($id, 0, 12).'"')->row();
		$this->db2->distinct();
		$this->db2->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.kelas,
		c.kd_jadwal,d.*,e.*');
		$this->db2->from('tbl_krs_feeder a');
		$this->db2->join('tbl_matakuliah b','a.kd_matakuliah=b.kd_matakuliah');
		$this->db2->join('tbl_jadwal_matkul_feeder c','a.kd_jadwal=c.kd_jadwal', 'left');
		$this->db2->join('tbl_karyawan d','c.kd_dosen=d.nid', 'left');
		$this->db2->join('tbl_ruangan e','c.kd_ruangan=e.id_ruangan', 'left');
		$this->db2->where('b.kd_prodi',$kd_prodi->KDPSTMSMHS);
		$this->db2->where('a.kd_krs',$id);
		return $this->db2->get();

	}

	function get_detail_print_krs_mahasiswa($id){
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = "'.substr($id, 0, 12).'" OR NIMHSMSMHS LIKE "'.substr($id, 0, 12).'%" ')->row();
		
		$this->db2->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.NIMHSMSMHS,e.nama,f.kode_ruangan,d.hari,d.waktu_mulai,d.waktu_selesai');
		$this->db2->from('tbl_krs a');
		$this->db2->join('tbl_matakuliah b','a.kd_matakuliah=b.kd_matakuliah');
		$this->db2->join('tbl_mahasiswa c','c.NIMHSMSMHS=a.npm_mahasiswa');
		$this->db2->join('tbl_jadwal_matkul d', 'd.kd_jadwal = a.kd_jadwal', 'left');
		$this->db2->join('tbl_karyawan e', 'e.nid = d.kd_dosen', 'left');
		$this->db2->join('tbl_ruangan f', 'f.id_ruangan = d.kd_ruangan', 'left');
		$this->db2->where('b.kd_prodi',$kd_prodi->KDPSTMSMHS);
		$this->db2->where('a.kd_krs',$id);
		return $this->db2->get();

	}

	function get_detail_print_krs_mahasiswa_feeder($id){
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = "'.substr($id, 0, 12).'" OR NIMHSMSMHS LIKE "'.substr($id, 0, 12).'%" ')->row();
		
		$this->db2->select('a.*,b.nama_matakuliah,b.sks_matakuliah,c.NIMHSMSMHS,e.nama,f.kode_ruangan,d.hari,d.waktu_mulai,d.waktu_selesai');
		$this->db2->from('tbl_krs_feeder a');
		$this->db2->join('tbl_matakuliah b','a.kd_matakuliah=b.kd_matakuliah');
		$this->db2->join('tbl_mahasiswa c','c.NIMHSMSMHS=a.npm_mahasiswa');
		$this->db2->join('tbl_jadwal_matkul_feeder d', 'd.kd_jadwal = a.kd_jadwal', 'left');
		$this->db2->join('tbl_karyawan e', 'e.nid = d.kd_dosen', 'left');
		$this->db2->join('tbl_ruangan f', 'f.id_ruangan = d.kd_ruangan', 'left');
		$this->db2->where('b.kd_prodi',$kd_prodi->KDPSTMSMHS);
		$this->db2->where('a.kd_krs',$id);
		return $this->db2->get();

	}

	function get_matakuliah_sp($kd_krs,$prodi){
		$this->db2->select('*');
		$this->db2->from('tbl_krs b');
		$this->db2->join('tbl_verifikasi_krs a', 'a.kd_krs = b.kd_krs');
		$this->db2->join('tbl_matakuliah c', 'c.kd_matakuliah = b.kd_matakuliah');
		$this->db2->where('b.kd_krs', $kd_krs);
		$this->db2->where('a.status_verifikasi', 2);
		//$this->db2->where('c.kd_prodi', $prodi);
		return $this->db2->get();
	}

	function get_all_khs_mahasiswa($npm){		
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="'.$npm.'"')->row();
		
		//ditambahin like -
		return $this->db2->query('SELECT b.THSMSTRLNM,c.SMAWLMSMHS,SUM(sks_matakuliah) AS jum_sks, a.*, b.*, c.NIMHSMSMHS 
		,SUM(a.sks_matakuliah*b.BOBOTTRLNM)/SUM(a.sks_matakuliah) AS ips
		FROM tbl_matakuliah a 
		JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) 
		JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")=REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")))  
		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="'.$npm.'"
		AND a.kd_prodi="'.$kd_prodi->KDPSTMSMHS.'" AND kd_matakuliah LIKE "%-%" GROUP BY b.THSMSTRLNM');

	}

	function get_all_khs_mahasiswa_ii($npm){		
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="'.$npm.'"')->row();
		
		//ditambahin like -
		return $this->db2->query('SELECT b.THSMSTRLNM,c.SMAWLMSMHS,SUM(sks_matakuliah) AS jum_sks, a.*, b.*, c.NIMHSMSMHS 
		,SUM(a.sks_matakuliah*b.BOBOTTRLNM)/SUM(a.sks_matakuliah) AS ips
		FROM tbl_matakuliah a 
		JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) 
		JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")=REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")))  
		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="'.$npm.'"
		AND a.kd_prodi="'.$kd_prodi->KDPSTMSMHS.'" AND kd_matakuliah LIKE "%-%" and b.THSMSTRLNM = "20152" GROUP BY b.THSMSTRLNM');

	}


	function get_ips_mahasiswa($npm, $id){		

		return $this->db2->query('

		SELECT SUM(a.sks_matakuliah*b.BOBOTTRLNM)/SUM(a.sks_matakuliah) AS ips

		FROM tbl_matakuliah a 

		JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) 

		JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  

		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="'.$npm.'" AND kd_matakuliah LIKE "%-%"

		AND (SUBSTR(LPAD(b.THSMSTRLNM - c.SMAWLMSMHS, 2, "0"), 1,1) * 2) + (SUBSTR(LPAD(b.THSMSTRLNM - c.SMAWLMSMHS, 2, "0"), 2,1) + 1)="'.$id.'"

		GROUP BY b.THSMSTRLNM');

	}

	function get_ipk_mahasiswa($npm){		

		return $this->db2->query('

		SELECT SUM(a.sks_matakuliah*b.BOBOTTRLNM)/SUM(a.sks_matakuliah) AS ipk

		FROM tbl_matakuliah a 

		JOIN tbl_transaksi_nilai b ON ((a.kd_matakuliah=REPLACE(REPLACE(b.KDKMKTRLNM, "\r", ""), "\n", ""))) 

		JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  

		WHERE REPLACE(REPLACE(b.NIMHSTRLNM, "\r", ""), "\n", "")="'.$npm.'" AND kd_matakuliah LIKE "%-%"

		GROUP BY b.THSMSTRLNM');

	}

	

	function get_detail_khs_mahasiswa($npm, $id){
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS = "'.$npm.'"')->row();
		return $this->db2->query('SELECT distinct a.id,a.NLAKHTRLNM,a.kd_transaksi_nilai, b.*, c.NIMHSMSMHS FROM tbl_transaksi_nilai a 
	JOIN tbl_matakuliah b ON ((b.kd_matakuliah=REPLACE(REPLACE(a.KDKMKTRLNM, "\r", ""), "\n", ""))) 
	JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")=REPLACE(REPLACE(a.NIMHSTRLNM, "\r", ""), "\n", ""))) 
	WHERE (SUBSTR(LPAD(a.THSMSTRLNM - c.SMAWLMSMHS, 2, "0"), 1,1) * 2) + (SUBSTR(LPAD(a.THSMSTRLNM - c.SMAWLMSMHS, 2, "0"), 2,1) + 1)="'.$id.'" 
	AND b.kd_prodi="'.$kd_prodi->KDPSTMSMHS.'" AND kd_matakuliah LIKE "%-%"
	AND c.NIMHSMSMHS="'.$npm.'"'); 

	}


	function getpembimbingall($id)

	{

		$this->db2->select('a.* , b.nama, c.perihal as perihalan');

		$this->db2->from('tbl_penugasan_dosen a');

		$this->db2->join('tbl_karyawan b', 'a.nik = b.nid');

		$this->db2->join('tbl_perihal c', 'a.perihal = c.kd_perihal');

		$this->db2->like('a.penugasan', $id, 'both');

		return $this->db2->get();

	}


	function getpembimbingdsn($id)

	{
		$this->db2->distinct();

		$this->db2->select('a.nik,a.perihal , b.nama, c.perihal as perihalan');

		$this->db2->from('tbl_penugasan_dosen a');

		$this->db2->join('tbl_karyawan b', 'a.nik = b.nid');

		$this->db2->join('tbl_perihal c', 'a.perihal = c.kd_perihal');

		$this->db2->where('a.nik', $id);

		return $this->db2->get();

	}

	function getpembimbingdsnall($id,$dsn)

	{

		$this->db2->select('a.* , b.nama, c.perihal as perihalan');

		$this->db2->from('tbl_penugasan_dosen a');

		$this->db2->join('tbl_karyawan b', 'a.nik = b.nid');

		$this->db2->join('tbl_perihal c', 'a.perihal = c.kd_perihal');

		$this->db2->where('a.nik', $dsn);

		$this->db2->where('a.perihal', $id);

		return $this->db2->get();

	}


	function get_detail_krs($id){

		return $this->db2->query('SELECT a.*, b.*, c.NIMHSMSMHS from tbl_transaksi_nilai a 

			join tbl_matakuliah b on ((b.kd_matakuliah=REPLACE(REPLACE(a.KDKMKTRLNM, "\r", ""), "\n", ""))) 

			JOIN tbl_mahasiswa c ON ((REPLACE(REPLACE(a.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")))  

			WHERE (SUBSTR(a.THSMSTRLNM - c.SMAWLMSMHS, 0,1) * 2) + (SUBSTR(a.THSMSTRLNM - c.SMAWLMSMHS, 1,2) + 1)="'.$id.'"');

		//$this->db2->select(',a.*,b.*,c.NIMHSMSMHS');

		//$this->db2->from('tbl_transaksi_nilai a');		

		//$this->db2->join('tbl_matakuliah b','b.kd_matakuliah=REPLACE(REPLACE(a.KDKMKTRLNM, "\r", ""), "\n", "")');

		//$this->db2->join('tbl_mahasiswa c','REPLACE(REPLACE(a.NIMHSTRLNM, "\r", ""), "\n", "")=REPLACE(REPLACE(c.NIMHSMSMHS, "\r", ""), "\n", "")');

		//$this->db2->where('(a.THSMSTRLNM-c.SMAWLMSMHS)+1',$id);

		//return $this->db2->get();

	}

	

	function get_pembimbing_krs($kd){

		$this->db2->select('a.*,b.*');

		$this->db2->from('tbl_verifikasi_krs a');		

		$this->db2->join('tbl_karyawan b','b.nid = a.id_pembimbing');

		$this->db2->where('a.kd_krs',$kd);

		return $this->db2->get();

	}

	

	function get_fakultas_karyawan($id){

		$this->db2->select('a.*,b.*,c.*,d.*');

		$this->db2->from('tbl_karyawan a');		

		$this->db2->join('tbl_jabatan b','a.jabatan_id = b.id_jabatan');

		$this->db2->join('tbl_divisi c','b.kd_divisi = c.kd_divisi');

		$this->db2->join('tbl_fakultas d','d.kd_fakultas = c.kd_divisi');

		$this->db2->where('a.nid',$id);

		return $this->db2->get();

	}

	

	function get_prodi_karyawan($id){

		$this->db2->select('a.*,b.*,c.*,d.*');

		$this->db2->from('tbl_karyawan a');		

		$this->db2->join('tbl_jabatan b','a.jabatan_id = b.id_jabatan');

		$this->db2->join('tbl_divisi c','b.kd_divisi = c.kd_divisi');

		$this->db2->join('tbl_jurusan_prodi d','d.kd_prodi = c.kd_divisi');

		$this->db2->where('a.nid',$id);

		return $this->db2->get();

	}

	

	function getlistmenu($id){

		$this->db2->where('user_group_id', $id);

		$getmenu = $this->db2->get('tbl_role_access')->result();

		foreach ($getmenu as $row) {

			$menu[] = $row->menu_id;

		}

		$this->db2->where_not_in('id_menu', $menu);

		$q = $this->db2->get('tbl_menu');

		return $q;

	}



	function getfakultas()

	{

		$this->db2->select('*');

		$this->db2->from('tbl_fakultas');

		return $this->db2->get()->result();

	}



	function get_jurusan()

	{

		$this->db2->select('*');

		$this->db2->from('tbl_jurusan_prodi a');

		$this->db2->join('tbl_fakultas b', 'a.kd_fakultas = b.kd_fakultas');

		return $this->db2->get()->result();

	}



	function get_lokasi()

	{

		$this->db2->select('*');

		$this->db2->from('tbl_gedung a');

		$this->db2->join('tbl_lembaga b', 'a.kode_lembaga = b.kode_lembaga');

		return $this->db2->get()->result();

	}



	function get_lantai($id)

	{

		$this->db2->select('*');

		$this->db2->from('tbl_lantai a');

		$this->db2->join('tbl_gedung b', 'a.id_gedung = b.id_gedung');

		$this->db2->where('a.id_gedung', $id);

		return $this->db2->get()->result();

	}



	function get_ruang($id)

	{

		$this->db2->select('*');

		$this->db2->from('tbl_ruangan a');

		$this->db2->join('tbl_lantai b', 'a.id_lantai = b.id_lantai');

		$this->db2->where('a.id_lantai', $id);

		return $this->db2->get()->result();

	}



	function get_sync()

	{

		$this->db2->select('*');

		$this->db2->from('tbl_sync');

		return $this->db2->get()->result();

	}



	function getIndexPembayaran($jur,$angk)

	{

		$this->db2->select('a.*, b.jenis_pembayaran');

		$this->db2->from('tbl_index_pembayaran a');

		$this->db2->join('tbl_jenis_pembayaran b', 'a.kd_jenis = b.kd_jenis');

		$this->db2->where('a.kd_prodi', $jur);

		$this->db2->where('a.tahunajaran', $angk);

		return $this->db2->get()->result();

	}

	

	function get_jadwal($jurusan, $tahun){

		$this->db2->select('a.*, b.*');

		$this->db2->from('tbl_jadwal_matkul a');

		$this->db2->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');

		$this->db2->where('a.kd_tahunajaran', $tahun);

		$this->db2->where('b.kd_prodi', $jurusan);

		return $this->db2->get();

	}


	function getdatadosen($value)

	{

		$this->db2->select('*');

		$this->db2->from('tbl_karyawan a');

		$this->db2->join('tbl_jabatan b', 'a.jabatan_id = b.id_jabatan');

		$this->db2->where('a.status', '1');

		$this->db2->where('b.kd_divisi', $value);

		$this->db2->like('b.jabatan', 'Pendidik', 'both');

		return $this->db2->get();

	}



	function getdatathn()

	{

		$this->db2->select('*');

		$this->db2->from('tbl_tahunakademik');

		//$this->db2->join('tbl_penugasan_dosen b', 'a.kode = b.penugasan','left');

		//$this->db2->where('b.penugasan is null', null, false);

		return $this->db2->get();

	}



	function getdivisi($value){

		$this->db2->select('*');

		$this->db2->from('tbl_karyawan a');

		$this->db2->join('tbl_jabatan b', 'b.jabatan_id = ','left');

		$this->db2->where('a.nid',$value);

		return $this->db2->get();

	}



	function join_sync_detail()

	{

		$this->db2->select('id_sync_detail, COUNT(id_sync_detail) as total');

		$this->db2->from('tbl_sync_detail a');

		$this->db2->join('tbl_sync b', 'a.date_sync = b.tanggal_sinkron');

		$this->db2->group_by('tanggal_sinkron');

		$this->db2->order_by('total', 'desc');

		return $this->db2->get();

	}



	function ambil_dt_mhs($id)

	{

		$this->db2->select('*');

		$this->db2->from('tbl_mahasiswa');

		$this->db2->where('id_mhs', $id);

		return $this->db2->get()->row();

	}

	

	function get_jadwalujian(){

		$this->db2->select('a.*, b.*, c.*, d.*');

		$this->db2->from('tbl_jadwal_ujian a');

		$this->db2->join('tbl_ruangan b', 'a.kode_ruangan = b.id_ruangan');

		$this->db2->join('tbl_jadwal_matkul c', 'a.kd_jadwal_matkul = c.kd_jadwal');

		$this->db2->join('tbl_matakuliah d', 'c.kd_matakuliah = d.kd_matakuliah');

		$this->db2->order_by('tanggal_jadwal_ujian', 'asc');

		$this->db2->order_by('mulai_jadwal_ujian', 'asc');

		return $this->db2->get();

	}

	

	function get_semester($tahun){

		$this->db2->order_by('kode', 'desc');

		$this->db2->where('status', '1');

		$tahunakademik = $this->db2->get('tbl_tahunakademik', 1)->row();
		
		if($tahun%2 == 0){
			$awal_masuk = $tahun - 1;
		}else{
			$awal_masuk = $tahun;
		}
		
		$selisih =   ($tahunakademik->kode) - ($awal_masuk);

		if ($selisih == 1) {
			$sls = '01';
		} else {
			$sls = $selisih;
		}
		

		$semawal = (substr($sls, 0,1)) * 2;

		$semtambah = substr($sls, 1,2) + 1;

		$semester = $semawal + $semtambah;

		return $semester;

	}

	function get_semester_new($tahun){

		$this->db2->order_by('kode', 'desc');

		$this->db2->where('status', '1');

		$tahunakademik = $this->db2->get('tbl_tahunakademik', 1)->row();
		
		if($tahun%2 == 0){
			$awal_masuk = $tahun - 1;
		}else{
			$awal_masuk = $tahun;
		}
		
		$selisih =   ($tahunakademik->kode) - ($awal_masuk);

		if ($selisih == 1) {
			$sls = '01';
		} else {
			$sls = $selisih;
		}
		

		$semawal = (substr($sls, 0,1)) * 2;

		$semtambah = substr($sls, 1,2) + 1;

		$semester = $semawal + $semtambah;

		return $semester;

	}

	

	function get_matakuliah_jadwal($prodi,$semester){

		$this->db2->select('a.*,b.*,c.*');

		$this->db2->from('tbl_kurikulum_matkul a');

		$this->db2->join('tbl_matakuliah b','a.kd_matakuliah = b.kd_matakuliah');

		$this->db2->join('tbl_kurikulum c','a.kd_kurikulum = c.kd_kurikulum');

		$this->db2->where('c.kd_prodi',$prodi);

		$this->db2->where('c.status','1');

		$this->db2->where('b.semester_matakuliah',$semester);

		return $this->db2->get()->result();

	}

	
	//ingetin harus dirubah
	function get_pilih_jadwal($kd,$prodi,$ta){

		/*$this->db2->select('a.*, b.*,c.*');

		$this->db2->from('tbl_jadwal_matkul a');

		$this->db2->join('tbl_ruangan b','a.kd_ruangan = b.id_ruangan', 'left');

		$this->db2->join('tbl_karyawan c','a.kd_dosen = c.nid');

		$this->db2->join('tbl_matakuliah d', 'a.kd_matakuliah = d.kd_matakuliah');

		$this->db2->like('a.kd_jadwal', $prodi);

		$this->db2->where('a.kd_matakuliah', $kd);

		$this->db2->like('d.kd_prodi', $prodi);*/

		$q = $this->db2->query("SELECT a.*,b.kuota,b.ruangan,c.nama,b.kode_ruangan,(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah FROM tbl_jadwal_matkul a
								left JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								JOIN tbl_karyawan c ON a.kd_dosen = c.nid
								JOIN tbl_matakuliah d ON a.kd_matakuliah = d.kd_matakuliah
								WHERE d.kd_prodi = '".$prodi."' AND a.kd_matakuliah = '".$kd."' AND a.kd_jadwal LIKE '".$prodi."%' and a.kd_tahunajaran = '".$ta."'
								");
		
		// $q = $this->db2->query("SELECT a.*,b.kuota,b.ruangan,c.nama,b.kode_ruangan,(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah FROM tbl_jadwal_matkul a
		// 						JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
		// 						JOIN tbl_karyawan c ON a.kd_dosen = c.nid
		// 						JOIN tbl_matakuliah d ON a.kd_matakuliah = d.kd_matakuliah
		// 						WHERE d.kd_prodi = '".$prodi."' AND a.kd_matakuliah = '".$kd."' AND a.kd_jadwal LIKE '".$prodi."%' AND kd_tahunajaran = '".$ta."'
		// 						AND (SUBSTR(b.kuota,1,2) - (SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal)) >= -5
		// 						");

		//return $this->db2->get();

		return $q;

	}

	function get_pilih_jadwal_feeder($kd,$prodi,$ta){


		$q = $this->db2->query("SELECT a.*,b.kuota,b.ruangan,c.nama,b.kode_ruangan,(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs_feeder z WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah FROM tbl_jadwal_matkul_feeder a
								left JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								JOIN tbl_karyawan c ON a.kd_dosen = c.nid
								JOIN tbl_matakuliah d ON a.kd_matakuliah = d.kd_matakuliah
								WHERE d.kd_prodi = '".$prodi."' AND a.kd_matakuliah = '".$kd."' AND a.kd_jadwal LIKE '".$prodi."%' and a.kd_tahunajaran = '".$ta."'
								");

		return $q;

	}

	function get_pilih_jadwal_krs($kd,$prodi,$ta,$kelas){

		/*$this->db2->select('a.*, b.*,c.*');

		$this->db2->from('tbl_jadwal_matkul a');

		$this->db2->join('tbl_ruangan b','a.kd_ruangan = b.id_ruangan', 'left');

		$this->db2->join('tbl_karyawan c','a.kd_dosen = c.nid');

		$this->db2->join('tbl_matakuliah d', 'a.kd_matakuliah = d.kd_matakuliah');

		$this->db2->like('a.kd_jadwal', $prodi);

		$this->db2->where('a.kd_matakuliah', $kd);

		$this->db2->like('d.kd_prodi', $prodi);*/

		if ( (($kd == 'MKU-1001') or ($kd == 'MKU-3008')) and (($prodi == '62201') or ($prodi == '61201') or ($prodi == '70201') or ($prodi == '73201')) ) {
			$q = $this->db2->query("SELECT a.*,b.kuota,b.ruangan,c.nama,b.kode_ruangan,(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah FROM 				  tbl_jadwal_matkul a
								left JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								JOIN tbl_karyawan c ON a.kd_dosen = c.nid
								JOIN tbl_matakuliah d ON a.kd_matakuliah = d.kd_matakuliah
								WHERE d.kd_prodi = '".$prodi."' AND a.kd_matakuliah = '".$kd."' AND a.kd_jadwal LIKE '".$prodi."%' AND kd_tahunajaran = '".$ta."' ");
		} else {
			$q = $this->db2->query("SELECT a.*,b.kuota,b.ruangan,c.nama,b.kode_ruangan,(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah FROM 				  tbl_jadwal_matkul a
								left JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								JOIN tbl_karyawan c ON a.kd_dosen = c.nid
								JOIN tbl_matakuliah d ON a.kd_matakuliah = d.kd_matakuliah
								WHERE d.kd_prodi = '".$prodi."' AND a.kd_matakuliah = '".$kd."' AND a.kd_jadwal LIKE '".$prodi."%' AND kd_tahunajaran = '".$ta."' and a.waktu_kelas = '".$kelas."'
								");
		}
		
		// $q = $this->db2->query("SELECT a.*,b.kuota,b.ruangan,c.nama,b.kode_ruangan,(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah FROM tbl_jadwal_matkul a
		// 						JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
		// 						JOIN tbl_karyawan c ON a.kd_dosen = c.nid
		// 						JOIN tbl_matakuliah d ON a.kd_matakuliah = d.kd_matakuliah
		// 						WHERE d.kd_prodi = '".$prodi."' AND a.kd_matakuliah = '".$kd."' AND a.kd_jadwal LIKE '".$prodi."%' AND kd_tahunajaran = '".$ta."'
		// 						AND (SUBSTR(b.kuota,1,2) - (SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs z WHERE z.kd_jadwal = a.kd_jadwal)) >= -10
		// 						");

		//return $this->db2->get();

		return $q;

	}

	function get_pilih_jadwal_krs_feeder($kd,$prodi,$ta,$kelas){

		if ( (($kd == 'MKU-1001') or ($kd == 'MKU-3008')) and (($prodi == '62201') or ($prodi == '61201') or ($prodi == '70201') or ($prodi == '73201')) ) {
			$q = $this->db2->query("SELECT a.*,b.kuota,b.ruangan,c.nama,b.kode_ruangan,(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs_feeder z WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah FROM 				  tbl_jadwal_matkul_feeder a
								left JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								JOIN tbl_karyawan c ON a.kd_dosen = c.nid
								JOIN tbl_matakuliah d ON a.kd_matakuliah = d.kd_matakuliah
								WHERE d.kd_prodi = '".$prodi."' AND a.kd_matakuliah = '".$kd."' AND a.kd_jadwal LIKE '".$prodi."%' AND kd_tahunajaran = '".$ta."' ");
		} else {
			$q = $this->db2->query("SELECT a.*,b.kuota,b.ruangan,c.nama,b.kode_ruangan,(SELECT COUNT(z.npm_mahasiswa) FROM tbl_krs_feeder z WHERE z.kd_jadwal = a.kd_jadwal) AS jumlah FROM 				  tbl_jadwal_matkul_feeder a
								left JOIN tbl_ruangan b ON a.kd_ruangan = b.id_ruangan
								JOIN tbl_karyawan c ON a.kd_dosen = c.nid
								JOIN tbl_matakuliah d ON a.kd_matakuliah = d.kd_matakuliah
								WHERE d.kd_prodi = '".$prodi."' AND a.kd_matakuliah = '".$kd."' AND a.kd_jadwal LIKE '".$prodi."%' AND kd_tahunajaran = '".$ta."' and a.waktu_kelas = '".$kelas."'
								");
		}

		return $q;

	}



	function updatedata_krs($kd, $krs, $data)

	{

		$this->db2->where('kd_matakuliah', $kd);

		$this->db2->where('kd_krs', $krs);

		$q = $this->db2->update('tbl_krs', $data);

		return $q;

	}

	function updatedata_krs_feeder($kd, $krs, $data)

	{

		$this->db2->where('kd_matakuliah', $kd);

		$this->db2->where('kd_krs', $krs);

		$q = $this->db2->update('tbl_krs_feeder', $data);

		return $q;

	}



	function getmhsbimbingan($id)

	{

		$this->db2->distinct();

		$this->db2->select('b.NIMHSMSMHS, b.NMMHSMSMHS, b.TAHUNMSMHS');

		$this->db2->from('tbl_verifikasi_krs a');

		$this->db2->join('tbl_krs c', 'a.kd_krs = c.kd_krs');

		$this->db2->join('tbl_mahasiswa b', 'b.NIMHSMSMHS = c.npm_mahasiswa');

		$this->db2->where('id_pembimbing', $id);

		//$this->db2->where('TAHUNMSMHS', $tahun);

		return $this->db2->get();

	}

	

	function get_krs_mahasiswa($npm, $semester){
		
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="'.$npm.'"')->row();
		
		$this->db2->distinct();

		$this->db2->select('a.*, b.nama_matakuliah,b.sks_matakuliah,c.semester_kd_matakuliah');

		$this->db2->from('tbl_krs a');

		$this->db2->join('tbl_matakuliah b', 'b.kd_matakuliah=a.kd_matakuliah');

		$this->db2->join('tbl_kurikulum_matkul c', 'b.kd_matakuliah = c.kd_matakuliah');

		$this->db2->where('npm_mahasiswa', $npm);
		
		$this->db2->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);

		$this->db2->where('SUBSTR(c.kd_kurikulum,1,5)', $kd_prodi->KDPSTMSMHS);

		$this->db2->where('semester_krs', $semester);

		return $this->db2->get();

	}

	function jdl_matkul_by_semester($smtr){
		$q = $this->db2->query('SELECT * FROM tbl_jadwal_matkul mk
                                left JOIN tbl_matakuliah a ON mk.kd_matakuliah=a.kd_matakuliah
                                left JOIN tbl_ruangan r ON r.id_ruangan = mk.kd_ruangan
                                JOIN tbl_fakultas b ON a.kd_fakultas = b.kd_fakultas
                                LEFT JOIN tbl_karyawan kry ON kry.nid = mk.kd_dosen
                                JOIN tbl_kurikulum_matkul km ON mk.kd_matakuliah = km.kd_matakuliah
                                JOIN tbl_kurikulum kur ON kur.kd_kurikulum = km.kd_kurikulum 
                                WHERE mk.kd_jadwal LIKE "%'.$this->session->userdata('id_jurusan_prasyarat').'%"
                                AND a.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'"
                                AND kur.kd_prodi = "'.$this->session->userdata('id_jurusan_prasyarat').'"
                                AND mk.kd_tahunajaran like "'.$this->session->userdata('tahunajaran').'%"
                                AND km.semester_kd_matakuliah = '.$smtr.'
                                GROUP BY mk.kd_jadwal
                                order by mk.kelas')->result();
		return $q;

	}

	function jdl_matkul_by_semester_feed($smtr){
		$te = $this->session->userdata('sess_login');
		$as = $te['userid'];
		$q = $this->db2->query('SELECT distinct mk.id_jadwal,mk.kd_matakuliah,mk.kelas,mk.kd_jadwal, a.semester_matakuliah,a.nama_matakuliah,a.sks_matakuliah, kry.nupn,kry.nidn,kry.nid,kry.nama FROM tbl_jadwal_matkul_feeder mk
                                left JOIN tbl_matakuliah a ON mk.kd_matakuliah=a.kd_matakuliah
                                LEFT JOIN tbl_karyawan kry ON kry.nid = mk.kd_dosen
                                JOIN tbl_kurikulum_matkul km ON mk.kd_matakuliah = km.kd_matakuliah
                                JOIN tbl_kurikulum kur ON kur.kd_kurikulum = km.kd_kurikulum 
                                WHERE mk.kd_jadwal LIKE "%'.$as.'%"
                                AND a.kd_prodi = "'.$as.'"
                                AND kur.kd_prodi = "'.$as.'"
                                AND mk.kd_tahunajaran = "'.$this->session->userdata('tahunajaran').'"
                                AND km.semester_kd_matakuliah = '.$smtr.'
                                order by mk.kd_matakuliah asc')->result();
		//$q = $this->db2->query("CALL sp_jadwal(".$smtr.",'".$this->session->userdata('tahunajaran')."','".$as."')");
		return $q;

	}

	function dosen_ajar_feed($sesi)
	{
		$q = $this->db2->query("SELECT DISTINCT jdl.kd_jadwal,mk.sks_matakuliah,mk.kd_prodi,jdl.kd_tahunajaran,mk.semester_matakuliah, jdl.kelas,mk.nama_matakuliah,mk.kd_matakuliah,jdl.kd_dosen ,kry.nama,kry.nid,kry.nidn,kry.nupn,
								(SELECT SUM(mk.sks_matakuliah) AS sks FROM tbl_jadwal_matkul_feeder jdl
								JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
								WHERE jdl.kd_dosen = kry.nid AND
								jdl.kd_tahunajaran = '20152' AND mk.kd_prodi = '73201'
								AND jdl.kd_jadwal LIKE '73201%') AS sks
								FROM tbl_jadwal_matkul_feeder jdl
								JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
								JOIN tbl_karyawan kry ON jdl.kd_dosen = kry.nid
								WHERE jdl.kd_tahunajaran = '20152' AND mk.kd_prodi = '73201'
								AND jdl.kd_jadwal LIKE '73201%'
								ORDER BY kry.nama ASC")->result();
		return $q;
	}

	function getkrsmhskp($prodi)
	{
		$this->db2->select('a.NMMHSMSMHS , a.NIMHSMSMHS');
		$this->db2->from('tbl_mahasiswa a');
		$this->db2->join('tbl_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db2->join('tbl_matakuliah c', 'b.kd_matakuliah = c.kd_matakuliah');
		$this->db2->where('a.KDPSTMSMHS', $prodi);
		$this->db2->like('c.nama_matakuliah','kerja praktek','both');
		return $this->db2->get();

	}

	function getkrsmhsskripsi($prodi)
	{
		$this->db2->select('a.NMMHSMSMHS , a.NIMHSMSMHS');
		$this->db2->from('tbl_mahasiswa a');
		$this->db2->join('tbl_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db2->join('tbl_matakuliah c', 'b.kd_matakuliah = c.kd_matakuliah');
		$this->db2->like('c.nama_matakuliah','skripsi','both');
		$this->db2->where('a.KDPSTMSMHS', $prodi);
		return $this->db2->get();	
	}

	function get_jurusan_mhs($npm)
	{
		$this->db2->select('NIMHSMSMHS,KDPSTMSMHS');
		$this->db2->from('tbl_mahasiswa');
		$this->db2->where('NIMHSMSMHS', $npm);
		return $this->db2->get();
	}

	function flag($isi){
		$query = $this->db2->query('select * from tbl_nilai where kd_jadwal = "'.$isi.'"');
		return $query;
	}

	function get_kelas_mahasiswa($id)
	{
		$this->db2->select('a.kd_jadwal,b.NIMHSMSMHS,b.NMMHSMSMHS');
		$this->db2->from('tbl_krs a');
		$this->db2->join('tbl_mahasiswa b', 'a.npm_mahasiswa = b.NIMHSMSMHS');
		$this->db2->join('tbl_jadwal_matkul c', 'a.kd_jadwal = c.kd_jadwal');
		$this->db2->where('c.id_jadwal', $id);
		$this->db2->order_by('b.NIMHSMSMHS', 'asc');
		return $this->db2->get();
	}

	function get_kelas_mahasiswa_nilai($id,$tipe)
	{
		// $this->db2->select('a.kd_jadwal,c.id_jadwal,b.NIMHSMSMHS,b.NMMHSMSMHS,d.nilai,d.flag_publikasi');
		// $this->db2->from('tbl_krs a');
		// $this->db2->join('tbl_mahasiswa b', 'a.npm_mahasiswa = b.NIMHSMSMHS');
		// $this->db2->join('tbl_jadwal_matkul c', 'a.kd_jadwal = c.kd_jadwal');
		// $this->db2->join('tbl_nilai_detail d', 'd.npm_mahasiswa = a.npm_mahasiswa', 'left');
		// $this->db2->where('c.id_jadwal', $id);
		// $this->db2->where('d.tipe', $tipe);
		// $this->db2->where('a.kd_matakuliah', 'd.kd_matakuliah');
		// $this->db2->order_by('b.NIMHSMSMHS', 'asc');
		// return $this->db2->get();

		$query=$this->db2->query('
				SELECT mhs.NIMHSMSMHS,mhs.NMMHSMSMHS,nd.nilai,nd.flag_publikasi FROM tbl_krs krs
					JOIN tbl_mahasiswa mhs ON krs.npm_mahasiswa = mhs.NIMHSMSMHS
					JOIN tbl_jadwal_matkul jdl ON krs.kd_jadwal = jdl.kd_jadwal
					LEFT JOIN tbl_nilai_detail nd ON nd.npm_mahasiswa = krs.npm_mahasiswa
					WHERE jdl.id_jadwal = '.$id.'
					AND nd.tipe = '.$tipe.'
					AND krs.kd_matakuliah = nd.kd_matakuliah
			');

		return $query;
	}

	function get_kelas_mahasiswa_ujian($id)
	{
		$this->db2->select('a.kd_jadwal,b.NIMHSMSMHS,b.NMMHSMSMHS');
		$this->db2->from('tbl_krs a');
		$this->db2->join('tbl_mahasiswa b', 'a.npm_mahasiswa = b.NIMHSMSMHS');
		$this->db2->join('tbl_jadwal_matkul c', 'a.kd_jadwal = c.kd_jadwal');
		$this->db2->join('tbl_sinkronisasi_renkeu d', 'b.NIMHSMSMHS = d.npm_mahasiswa');
		$this->db2->where('c.id_jadwal', $id);
		$this->db2->where('d.status >=', '2');
		$this->db2->order_by('b.NIMHSMSMHS', 'asc');
		return $this->db2->get();
	}

	function get_kelas_mahasiswa_ujian2($type1,$jadwal,$ta)
	{
		return $this->db2->query('SELECT DISTINCT a.npm_mahasiswa FROM tbl_krs a
										JOIN tbl_sinkronisasi_renkeu b ON a.npm_mahasiswa = b.npm_mahasiswa
										WHERE (b.status >= '.$type1.') AND a.kd_jadwal = "'.$jadwal.'" AND b.tahunajaran = "'.$ta.'" order by npm_mahasiswa asc');
	}

	function get_matkul_ajar($ta,$jurusan,$semester)
	{
		$this->db2->distinct();
		$this->db2->select('a.kd_matakuliah,b.nama_matakuliah,d.kd_jadwal,d.hari,d.kelas,c.nama,d.id_jadwal');
		$this->db2->from('tbl_kurikulum_matkul a');
		//$this->db2->join('tbl_kurikulum e', 'a.kd_kurikulum = e.kd_kurikulum');
		$this->db2->join('tbl_matakuliah b', 'a.kd_matakuliah = b.kd_matakuliah');
		$this->db2->join('tbl_jadwal_matkul d', 'a.kd_matakuliah = d.kd_matakuliah');
		$this->db2->join('tbl_karyawan c', 'd.kd_dosen = c.nid');
		//$this->db2->where('e.kd_prodi', $jurusan);
		$this->db2->where('b.kd_prodi', $jurusan);
		$this->db2->where('b.semester_matakuliah', $semester);
		$this->db2->like('d.kd_jadwal', $jurusan,'after');
		$this->db2->where('d.kd_tahunajaran', $ta);
		return $this->db2->get();
	}

	function getnilai($id, $npm, $tipe,$mk)
	{
		$this->db2->select('a.*');
		$this->db2->from('tbl_nilai_detail a');
		$this->db2->join('tbl_jadwal_matkul b', 'a.kd_jadwal=b.kd_jadwal');
		$this->db2->where('a.kd_jadwal', $id);
		$this->db2->where('a.kd_matakuliah', $mk);
		$this->db2->where('a.npm_mahasiswa', $npm);
		$this->db2->where('a.tipe', $tipe);
		return $this->db2->get();
	}

	function get_dosen($dosen){
		$this->db2->select('nama');
		$this->db2->from('tbl_karyawan');
		$this->db2->where('nid', $dosen);
		return $this->db2->get();
	}

	function get_mk_by_jadwal($id)
	{
		$this->db2->select('tbl_jadwal_matkul.kd_matakuliah,nama_matakuliah');
		$this->db2->from('tbl_jadwal_matkul');
		$this->db2->join('tbl_matakuliah', 'tbl_jadwal_matkul.kd_matakuliah = tbl_matakuliah.kd_matakuliah');
		$this->db2->where('kd_jadwal', $id);
		return $this->db2->get();
	}

	function get_krs_mahasiswa_sp($npm, $semester){
		$kd_prodi = $this->db2->query('select KDPSTMSMHS from tbl_mahasiswa where NIMHSMSMHS="'.$npm.'"')->row();
		$this->db2->select('a.*, b.*,c.semester_kd_matakuliah');
		$this->db2->from('tbl_krs a');
		$this->db2->join('tbl_matakuliah b', 'b.kd_matakuliah=a.kd_matakuliah');
		$this->db2->join('tbl_verifikasi_krs d', 'a.kd_krs = d.kd_krs');
		$this->db2->join('tbl_kurikulum_matkul c', 'b.kd_matakuliah = c.kd_matakuliah');
		$this->db2->where('d.npm_mahasiswa', $npm);
		$this->db2->where('b.kd_prodi', $kd_prodi->KDPSTMSMHS);
		$this->db2->where('SUBSTR(c.kd_kurikulum,1,5)', $kd_prodi->KDPSTMSMHS);
		$this->db2->where('semester_krs', $semester);
		$this->db2->where('d.status_verifikasi', '2');
		return $this->db2->get();
	}

	function get_matkul_sp($prodi){
		$this->db2->distinct();
		$this->db2->select('a.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah');
		$this->db2->from('tbl_krs a');
		$this->db2->join('tbl_verifikasi_krs b', 'a.kd_krs = b.kd_krs');
		$this->db2->join('tbl_matakuliah c', 'a.kd_matakuliah = c.kd_matakuliah');
		$this->db2->where('b.status_verifikasi', '2');
		$this->db2->where('b.kd_jurusan', $prodi);
		$this->db2->where('c.kd_prodi', $prodi);
		return $this->db2->get();
	}

	function getkrsmhsbyprodi($prodi,$tahun)
	{
		$this->db2->distinct();
		$this->db2->select('a.NIMHSMSMHS,a.NMMHSMSMHS,a.TAHUNMSMHS');
		$this->db2->from('tbl_mahasiswa a');
		$this->db2->join('tbl_verifikasi_krs b', 'a.NIMHSMSMHS = b.npm_mahasiswa');
		$this->db2->where('a.KDPSTMSMHS', $prodi);
		$this->db2->where('a.TAHUNMSMHS', $tahun);
		$this->db2->order_by('a.NIMHSMSMHS', 'asc');
		return $this->db2->get();
	}

	function getdetailslug($npm,$slug)
	{
		$this->db2->select('*');
		$this->db2->from('tbl_verifikasi_krs');
		$this->db2->where('npm_mahasiswa', $npm);
		$this->db2->where('slug_url', $slug);
		return $this->db2->get();
	}

	function getkartumhs($kodefakultas,$atas,$bawah)
	{
		return $this->db2->query("select a.NMMHSMSMHS,a.NIMHSMSMHS,a.TAHUNMSMHS from tbl_mahasiswa a 
								join tbl_jurusan_prodi b on b.kd_prodi = a.KDPSTMSMHS
								join tbl_fakultas c on c.kd_fakultas = b.kd_fakultas
								where FLAG_RENKEU = 5 and c.kd_fakultas = ".$kodefakultas."
								and TAHUNMSMHS BETWEEN '".$atas."' AND '".$bawah."'
								order by NIMHSMSMHS")->result();
	}

	function get_peserta_sp($kode,$prodi)
	{
		$this->db2->select('b.kd_krs,a.kd_matakuliah,c.nama_matakuliah,c.sks_matakuliah,b.npm_mahasiswa,d.NMMHSMSMHS,d.TAHUNMSMHS');
		$this->db2->from('tbl_krs a');
		$this->db2->join('tbl_verifikasi_krs b', 'a.kd_krs = b.kd_krs');
		$this->db2->join('tbl_mahasiswa d', 'b.npm_mahasiswa = d.NIMHSMSMHS');
		$this->db2->join('tbl_matakuliah c', 'a.kd_matakuliah = c.kd_matakuliah');
		$this->db2->where('b.status_verifikasi', '2');
		$this->db2->where('b.kd_jurusan', $prodi);
		$this->db2->where('c.kd_prodi', $prodi);
		$this->db2->where('a.kd_matakuliah', $kode);
		return $this->db2->get();
	}

	function getmhslulus($tahun)
	{
		$this->db2->select('a.*,b.*,c.prodi,d.kd_fakultas,d.fakultas');
		$this->db2->from('tbl_mahasiswa a');
		$this->db2->join('tbl_sinkronisasi_renkeu b', 'a.NIMHSMSMHS = b.npm_mahasiswa', 'left');
		$this->db2->join('tbl_jurusan_prodi c', 'a.KDPSTMSMHS = c.kd_prodi');
		$this->db2->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
		$this->db2->where('a.STMHSMSMHS', 'L');
		$this->db2->where('YEAR(TGLLSMSMHS)', $tahun);
		return $this->db2->get();
	}

	function getkrsanmhs($npm,$ta)
	{
		return $this->db2->query("select * from tbl_sinkronisasi_renkeu where npm_mahasiswa = '".$npm."' and tahunajaran = '".$ta."'");
	}

	function validjadwal($kodekrs)
	{
		$mhs  = $this->app_model->get_jurusan_mhs(substr($kodekrs, 0,12))->row();
		return $this->db2->query("SELECT a.kd_jadwal,b.nama_matakuliah FROM tbl_krs a
								JOIN tbl_matakuliah b ON a.kd_matakuliah = b.kd_matakuliah
								WHERE (b.nama_matakuliah NOT LIKE 'skripsi%' AND b.nama_matakuliah NOT LIKE '%ahir%' AND b.nama_matakuliah NOT LIKE '%kerja praktek%' AND b.nama_matakuliah NOT LIKE '%kerja mahasiswa%' AND b.nama_matakuliah NOT LIKE '%magang%' AND b.nama_matakuliah NOT LIKE '%tugas%' AND b.nama_matakuliah NOT LIKE '%seminar%' AND b.nama_matakuliah NOT LIKE '%tesis%')
								AND a.kd_krs = '".$kodekrs."' AND b.kd_prodi = '".$mhs->KDPSTMSMHS."' AND a.kd_jadwal IS NULL");
	}

	function ketentuan_sks($ipss,$sks)
	{
		$ips = number_format($ipss,2);
		if ($sks <= 24) {
			if ((($ips >= 0.00) and ($ips < 1.40)) and ($sks <= 12)) {
				$boleh = 1;
			} elseif ( (($ips >= 1.40) and ($ips <= 1.99)) and ($sks <= 15) ) {
				$boleh = 1;
			} elseif ( (($ips >= 2.00) and ($ips <= 2.49)) and ($sks <= 18) ) {
				$boleh = 1;
			} elseif ( (($ips >= 2.50) and ($ips <= 2.99)) and ($sks <= 21) ) {
				$boleh = 1;
			} elseif ( (($ips >= 3.00) and ($ips <= 4.00)) and ($sks <= 24 ) ) {
				$boleh = 1;
			//} elseif ($ips == 0.00) {
				//$boleh = 0;
			} else {
				$boleh = 0;
			}
			return $boleh;
		} else {
			$boleh = 0;
			return $boleh;
		}
		
		
		
	}

	function getmabas1($tahun)
	{

		$this->db2->select('*');

		$this->db2->from('tbl_form_camaba a');

		$this->db2->join('tbl_mahasiswa b', 'b.NIMHSMSMHS = a.npm_baru');

		$this->db2->join('tbl_jurusan_prodi c', 'b.KDPSTMSMHS = c.kd_prodi');

		$this->db2->where('a.status >=', 1);

		// $this->db2->where('a.gelombang', 2);

		$names = array('3','4','5');
		$this->db2->where_in('a.gelombang', $names);

		$this->db2->like('b.NIMHSMSMHS', $tahun, 'after');

		$this->db2->order_by('b.NIMHSMSMHS', 'asc');

		return $this->db2->get();
	}

	function getmabas2($tahun)
	{

		$this->db2->select('*');

		$this->db2->from('tbl_pmb_s2 a');

		$this->db2->join('tbl_mahasiswa b', 'b.NIMHSMSMHS = a.npm_baru');

		$this->db2->join('tbl_jurusan_prodi c', 'b.KDPSTMSMHS = c.kd_prodi');

		$this->db2->where('a.status >=', 1);

		$this->db2->where('a.gelombang >=',4);

		$this->db2->like('b.NIMHSMSMHS', $tahun, 'after');

		//$this->db2->or_like('b.NIMHSMSMHS', '2015', 'after');

		$this->db2->order_by('b.NIMHSMSMHS', 'asc');

		return $this->db2->get();
	}

	function get_semester_khs($smsawl,$smsnilai)
	{
		if($smsawl%2 == 0){
			$awal_masuk = $smsawl - 1;
		}else{
			$awal_masuk = $smsawl;
		}
		
		$selisih =   ($smsnilai) - ($awal_masuk);

		if ($selisih == 1) {
			$sls = '01';
		} else {
			$sls = $selisih;
		}
		

		$semawal = (substr($sls, 0,1)) * 2;

		$semtambah = substr($sls, 1,2) + 1;

		$semester = $semawal + $semtambah;

		return $semester;
	}

	function getdatapublish($prodi,$ta,$semester)
	{
		$q = $this->db2->query("select distinct a.kd_matakuliah,a.nid,a.audit_date,a.publis_date,b.kelas,c.nama,d.nama_matakuliah from tbl_nilai_detail a 
								join tbl_jadwal_matkul b on a.kd_jadwal = b.kd_jadwal
								join tbl_karyawan c on a.nid = c.nid
								join tbl_matakuliah d on a.kd_matakuliah = d.kd_matakuliah
								where d.kd_prodi = '".$prodi."' and d.semester_matakuliah = ".$semester." and a.kd_tahunakademik = '".$ta."'
								group by a.kd_jadwal order by a.kd_matakuliah asc ");
		return $q;
	}

	function get_data_jadwal($id,$prodi)
	{
		$query = $this->db2->query("select distinct kd_jadwal,nama_matakuliah,kelas,nama from tbl_jadwal_matkul a
									join tbl_karyawan b on a.kd_dosen = b.nid
									join tbl_matakuliah c on a.kd_matakuliah = c.kd_matakuliah
									where a.id_jadwal = ".$id." and c.kd_prodi = '".$prodi."'");
		return $query;
	}

	function getkrsbefore($kode,$kdmatukul)
	{
		$this->db2->select('*');
		$this->db2->from('tbl_krs');
		$this->db2->where('kd_krs', $kode);
		$this->db2->where('kd_matakuliah', $kdmatukul);
		return $this->db2->get();
	}

	function cekmkbarukrs($kode,$mkbaru)
	{
		$this->db2->select('id_krs');
		$this->db2->from('tbl_krs');
		$this->db2->where('kd_krs', $kode);
		$this->db2->where_not_in('kd_matakuliah', $mkbaru);
		return $this->db2->get();
		//var_dump($q);exit();
	}

	function getstatusmahasiswa($fak,$prodi,$ta)
	{
		if (($fak == 0) and ($prodi == 0)) {
			$this->db2->distinct();
			$this->db2->select('a.*,b.NMMHSMSMHS,c.prodi,d.fakultas');
			$this->db2->from('tbl_status_mahasiswa a');
			$this->db2->join('tbl_mahasiswa b', 'a.npm = b.NIMHSMSMHS');
			$this->db2->join('tbl_jurusan_prodi c', 'b.KDPSTMSMHS = c.kd_prodi');
			$this->db2->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
			$this->db2->where('a.tahunajaran', $ta);
		} elseif (($fak != 0) and ($prodi == 0)) {
			$this->db2->distinct();
			$this->db2->select('a.*,b.NMMHSMSMHS,c.prodi,d.fakultas');
			$this->db2->from('tbl_status_mahasiswa a');
			$this->db2->join('tbl_mahasiswa b', 'a.npm = b.NIMHSMSMHS');
			$this->db2->join('tbl_jurusan_prodi c', 'b.KDPSTMSMHS = c.kd_prodi');
			$this->db2->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
			$this->db2->where('d.kd_fakultas', $fak);
			$this->db2->where('a.tahunajaran', $ta);
		} else {
			//var_dump('aaaa');exit();
			$this->db2->distinct();
			$this->db2->select('a.*,b.NMMHSMSMHS,c.prodi,d.fakultas');
			$this->db2->from('tbl_status_mahasiswa a');
			$this->db2->join('tbl_mahasiswa b', 'a.npm = b.NIMHSMSMHS');
			$this->db2->join('tbl_jurusan_prodi c', 'b.KDPSTMSMHS = c.kd_prodi');
			$this->db2->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
			$this->db2->where('d.kd_fakultas', $fak);
			$this->db2->where('b.KDPSTMSMHS', $prodi);
			$this->db2->where('a.tahunajaran', $ta);
		}
		$this->db2->order_by('npm', 'asc');
		return $this->db2->get();
	}

	function getmhs($prodi,$tahun)
	{
		$this->db2->distinct();
		$this->db2->select('NIMHSMSMHS,NMMHSMSMHS,prodi,fakultas');
		$this->db2->from('tbl_mahasiswa a');
		$this->db2->join('tbl_jurusan_prodi c', 'a.KDPSTMSMHS = c.kd_prodi');
		$this->db2->join('tbl_fakultas d', 'c.kd_fakultas = d.kd_fakultas');
		$this->db2->where('KDPSTMSMHS', $prodi);
		$this->db2->where('TAHUNMSMHS', $tahun);
		return $this->db2->get();
	}

	function getsmsmhs($npm,$smstr)
	{
		$query = $this->db2->query("SELECT DISTINCT semester_krs,kd_krs FROM tbl_krs WHERE npm_mahasiswa = '".$npm."' AND semester_krs = ".$smstr."");
		return $query;
	}

	function getsmsmhsnon($npm,$smstr)
	{
		$query = $this->db2->query("SELECT DISTINCT semester,status FROM tbl_status_mahasiswa WHERE npm = '".$npm."' AND semester = ".$smstr."");
		return $query;
	}

	function getmhsrenkeu($npm,$ta)
	{
		$this->db2->select('status');
		$this->db2->from('tbl_sinkronisasi_renkeu');
		$this->db2->where('npm_mahasiswa', $npm);
		$this->db2->where('tahunajaran', $ta);
		return $this->db2->get();
	}

	function getjumlahkrsperprodi($prodi,$ta)
	{
		return $this->db2->query("SELECT DISTINCT TAHUNMSMHS,COUNT(DISTINCT npm_mahasiswa) as total FROM tbl_verifikasi_krs a
								JOIN tbl_mahasiswa b ON a.npm_mahasiswa = b.NIMHSMSMHS 
								WHERE a.tahunajaran = '".$ta."' AND b.KDPSTMSMHS = '".$prodi."'
								GROUP BY b.TAHUNMSMHS ORDER BY b.TAHUNMSMHS ASC")->result();	
	}

	function gettotalkrsperprodi($prodi,$ta)
	{
		return $this->db2->query("SELECT COUNT(DISTINCT npm_mahasiswa) as total FROM tbl_verifikasi_krs a
								JOIN tbl_mahasiswa b ON a.npm_mahasiswa = b.NIMHSMSMHS 
								WHERE a.tahunajaran = '".$ta."' AND b.KDPSTMSMHS = '".$prodi."'")->row();	
	}

	function getabcdeperprodi($prodi,$ta)
	{
		return $this->db2->query("SELECT DISTINCT NLAKHTRLNM,COUNT(DISTINCT NIMHSMSMHS) as total FROM tbl_transaksi_nilai a
								JOIN tbl_mahasiswa b ON a.NIMHSTRLNM = b.NIMHSMSMHS 
								WHERE a.THSMSTRLNM = '".$ta."' AND b.KDPSTMSMHS = '".$prodi."' AND a.NLAKHTRLNM != ''
								GROUP BY a.NLAKHTRLNM ORDER BY a.NLAKHTRLNM ASC")->result();
	}

	function getipsperprodi1($prodi,$ta)
	{
		return $this->db2->query("SELECT COUNT(DISTINCT NIMHSMSMHS) as total FROM tbl_ips a
								JOIN tbl_mahasiswa b ON a.npm_mahasiswa = b.NIMHSMSMHS 
								WHERE a.tahunajaran = '".$ta."' AND b.KDPSTMSMHS = '".$prodi."' AND ips < 2.500")->row();
	}

	function getipsperprodi2($prodi,$ta)
	{
		return $this->db2->query("SELECT COUNT(DISTINCT NIMHSMSMHS) as total FROM tbl_ips a
								JOIN tbl_mahasiswa b ON a.npm_mahasiswa = b.NIMHSMSMHS 
								WHERE a.tahunajaran = '".$ta."' AND b.KDPSTMSMHS = '".$prodi."' AND (ips > 2.499 AND ips < 3.000)")->row();
	}

	function getipsperprodi3($prodi,$ta)
	{
		return $this->db2->query("SELECT COUNT(DISTINCT NIMHSMSMHS) as total FROM tbl_ips a
								JOIN tbl_mahasiswa b ON a.npm_mahasiswa = b.NIMHSMSMHS 
								WHERE a.tahunajaran = '".$ta."' AND b.KDPSTMSMHS = '".$prodi."' AND ips > 2.999")->row();
	}

	function get_laporan_cuti($prodi,$thn)
	{
		return $this->db2->query('SELECT DISTINCT b.prodi,COUNT(npm) AS jml FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa g ON a.npm=g.NIMHSMSMHS
											JOIN tbl_jurusan_prodi b ON b.kd_prodi=g.KDPSTMSMHS WHERE STATUS = "C"
											AND b.kd_prodi = "'.$prodi.'" AND a.tahunajaran = "'.$thn.'"')->row();
	}

	function get_dostitap($prodi,$th)
	{
		return $this->db2->query("SELECT COUNT(DISTINCT nid) AS juml_dsn FROM tbl_karyawan a
									JOIN tbl_jabatan b ON a.jabatan_id = b.id_jabatan
									JOIN tbl_jadwal_matkul c ON a.nid = c.kd_dosen
									WHERE SUBSTRING(a.nid,1,2) = '00' AND c.kd_tahunajaran = '".$th."' AND c.kd_jadwal like '".$prodi."%'")->row();
	}

	function get_dostap($prodi,$th)
	{
		return $this->db2->query("SELECT COUNT(DISTINCT nid) AS juml_dsn FROM tbl_karyawan a
									JOIN tbl_jabatan b ON a.jabatan_id = b.id_jabatan
									JOIN tbl_jadwal_matkul c ON a.nid = c.kd_dosen
									WHERE SUBSTRING(a.nid,1,2) != '00' AND SUBSTRING(a.nid,2,2) != '0' AND c.kd_tahunajaran = '".$th."'
									AND b.kd_divisi = '".$prodi."'")->row();
	}

	function getipkmhs($npm,$prodi)
	{
		$hitung_ips = $this->db2->query('SELECT distinct a.NIMHSTRLNM,a.KDKMKTRLNM,a.NLAKHTRLNM,a.BOBOTTRLNM,b.sks_matakuliah FROM tbl_transaksi_nilai a
										JOIN tbl_matakuliah b ON a.KDKMKTRLNM = b.kd_matakuliah
										WHERE a.kd_transaksi_nilai IS NOT NULL AND kd_prodi = "'.$prodi.'" AND NIMHSTRLNM = "'.$npm.'"')->result();
		//var_dump($hitung_ips);exit();

		$st=0;
		$ht=0;
		foreach ($hitung_ips as $iso) {
			$h = 0;


			$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
			$ht=  $ht + $h;
			//echo $iso->NLAKHTRLNM.' - '.$iso->BOBOTTRLNM.' - '.$iso->sks_matakuliah.'<br>';
			$st=$st+$iso->sks_matakuliah;
		}
		//echo $st.'<br>';
		//echo $ht.'<br>';
		$ipk_nr = $ht/$st;
		return number_format($ipk_nr,2);
	}

	function getipsmhs($prodi,$npm,$ta)
	{
		$hitung_ips = $this->db2->query('SELECT a.NIMHSTRLNM,a.KDKMKTRLNM,a.NLAKHTRLNM,a.BOBOTTRLNM,b.sks_matakuliah FROM tbl_transaksi_nilai a
										JOIN tbl_matakuliah b ON a.KDKMKTRLNM = b.kd_matakuliah
										WHERE a.kd_transaksi_nilai IS NOT NULL AND kd_prodi = '.$prodi.' AND NIMHSTRLNM = "'.$npm.'" and THSMSTRLNM = "'.$ta.'"')->result();

		$st=0;
		$ht=0;
		foreach ($hitung_ips as $iso) {
			$h = 0;


			$h = $iso->BOBOTTRLNM * $iso->sks_matakuliah;
			$ht=  $ht + $h;

			$st=$st+$iso->sks_matakuliah;
		}

		$ips_nr = $ht/$st;
		return number_format($ips_nr,2);
	}

	function get_jml_sts($tahunajar)
	{
		return $this->db2->query('SELECT COUNT(DISTINCT kd_krs) AS jml, b.prodi, b.kd_prodi, c.fakultas, a.tahunajaran FROM tbl_verifikasi_krs a join tbl_jurusan_prodi b on a.kd_jurusan=b.kd_prodi
								join tbl_fakultas c on b.kd_fakultas=c.kd_fakultas WHERE (npm_mahasiswa IS NOT NULL OR npm_mahasiswa != "" OR npm_mahasiswa != 0)
								AND a.tahunajaran = "'.$tahunajar.'"  group by b.kd_prodi order by b.kd_prodi asc')->result();
	}

	function get_sts_cuti($th)
	{
		return $this->db2->query('SELECT COUNT(DISTINCT npm) as jum FROM tbl_status_mahasiswa a JOIN tbl_mahasiswa b ON a.npm=b.NIMHSMSMHS
								WHERE a.status="C" AND a.tahunajaran = "'.$th.'" GROUP BY b.KDPSTMSMHS order by b.KDPSTMSMHS asc')->result();
	}

	function jml_detil($prodi,$th)
	{
		return $this->db2->query('SELECT distinct a.NIMHSMSMHS, b.prodi, a.NMMHSMSMHS FROM tbl_mahasiswa a JOIN tbl_jurusan_prodi b
								ON a.KDPSTMSMHS=b.kd_prodi JOIN tbl_verifikasi_krs c ON c.npm_mahasiswa=a.NIMHSMSMHS
								WHERE c.tahunajaran = "'.$th.'" AND a.KDPSTMSMHS = "'.$prodi.'" and (npm_mahasiswa IS NOT NULL OR npm_mahasiswa != "" OR npm_mahasiswa != 0)')->result();
	}

	function jml_cuti($prd,$tah)
	{
		return $this->db2->query('SELECT distinct a.NIMHSMSMHS,a.NMMHSMSMHS FROM tbl_mahasiswa a LEFT JOIN tbl_status_mahasiswa b ON a.NIMHSMSMHS = b.npm
								WHERE b.status = "C" AND a.KDPSTMSMHS = "'.$prd.'" AND b.tahunajaran = "'.$tah.'" and validate >= 1')->result();
	}

	function list_dosen($sess,$smt,$ta)
	{
		return $this->db2->query("SELECT distinct b.id_jadwal,b.kd_jadwal,b.kd_matakuliah,b.kd_dosen,b.kelas,c.nama,d.nama_matakuliah from tbl_jadwal_matkul b 
								join tbl_karyawan c on b.kd_dosen = c.nid
								join tbl_matakuliah d on b.kd_matakuliah = d.kd_matakuliah
								join tbl_kurikulum_matkul e on d.kd_matakuliah = e.kd_matakuliah
								where e.semester_kd_matakuliah = ".$smt."
								and b.kd_tahunajaran = ".$ta." AND NOT (b.kd_dosen <=> NULL or b.kd_dosen <=> '')
								and b.kd_jadwal like '".$sess."%'
								group by b.kd_jadwal order by d.kd_matakuliah asc ")->result();
	}

	function list_dosen_fak($ses)
	{
		return $this->db2->query("SELECT DISTINCT jdl.kd_dosen ,kry.nama,kry.nid,(SELECT SUM(mk.sks_matakuliah) AS sks FROM tbl_jadwal_matkul jdl
                                JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
                                WHERE jdl.kd_dosen = kry.nid AND mk.kd_prodi = ".$ses.") AS sks 
								FROM tbl_jadwal_matkul jdl
								JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
								JOIN tbl_karyawan kry ON jdl.kd_dosen = kry.nid
								WHERE mk.kd_prodi = ".$ses." AND NOT (jdl.kd_dosen <=> NULL) AND NOT (jdl.kd_dosen <=> '')")->result();
	}

	function list_dosen_baa()
	{
		return $this->db2->query("SELECT DISTINCT jdl.kd_dosen ,kry.nama,kry.nid,(SELECT SUM(mk.sks_matakuliah) AS sks FROM tbl_jadwal_matkul jdl
                                LEFT JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
                                WHERE jdl.kd_dosen = kry.nid) AS sks 
								FROM tbl_jadwal_matkul jdl
								LEFT JOIN tbl_matakuliah mk ON jdl.id_matakuliah = mk.id_matakuliah
								RIGHT JOIN tbl_karyawan kry ON jdl.kd_dosen = kry.nid
								WHERE NOT (jdl.kd_dosen <=> NULL) AND NOT (jdl.kd_dosen <=> '')")->result();
	}

}



/* End of file app_model.php */

/* Location: ./application/models/app_model.php */