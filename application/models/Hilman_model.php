<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hilman_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function moreLike($arr, $tbl, $poss)
	{
		$jum = count($arr);

		for ($i = 0; $i < $jum; $i++) {
			$this->db->like(array_keys($arr)[$i], array_values($arr)[$i], $poss);
		}

		$get = $this->db->get($tbl);

		return $get;
	}

	function updateMoreParams($arr, $tbl, $data)
	{
		$jum = count($arr);

		for ($i = 0; $i < $jum; $i++) {
			$this->db->where(array_keys($arr)[$i], array_values($arr)[$i]);
		}

		$this->db->update($tbl, $data);
	}

	function moreWhere($arr, $tbl)
	{
		$jum = count($arr);

		for ($i = 0; $i < $jum; $i++) {
			$this->db->where(array_keys($arr)[$i], array_values($arr)[$i]);
		}

		$get = $this->db->get($tbl);

		return $get;
	}

}

/* End of file Hilman_model.php */
/* Location: ./application/models/Hilman_model.php */