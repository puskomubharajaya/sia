<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ujian_model extends CI_Model
{

	public $variable;

	public function __construct()
	{
		parent::__construct();
	}

	function loaduji($year, $uid)
	{
		$fakultas = get_kdfak_byprodi($uid) . '/';

		$this->db->select('
			a.id_jadwal,
			a.kd_jadwal,
			a.id_matakuliah,
			a.kd_matakuliah,
			a.kelas,
			(SELECT tipe_uji FROM tbl_jadwaluji WHERE kd_jadwal = a.kd_jadwal AND tipe_uji = 1) AS uts,
			(SELECT tipe_uji FROM tbl_jadwaluji WHERE kd_jadwal = a.kd_jadwal AND tipe_uji = 2) AS uas, 
			b.nama_matakuliah, 
			c.nama
		');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->join('tbl_matakuliah b', 'a.id_matakuliah = b.id_matakuliah');
		$this->db->join('tbl_karyawan c', 'c.nid = a.kd_dosen');
		$this->db->group_start();
		$this->db->like('kd_jadwal', $uid, 'after');
		$this->db->or_like('kd_jadwal', $fakultas, 'after');
		$this->db->group_end();
		$this->db->where('kd_tahunajaran', $year);
		$get = $this->db->get();
		return $get;
	}

	public function exam_deadline()
	{
		$list = $this->db->query("SELECT 
									a.id, 
									a.tahunajaran, 
									a.tipe_uji, 
									a.deadline_uji, 
									a.deadline_scoring, 
									a.deadline_interval, 
									b.tahun_akademik 
								FROM tbl_deadline_uji a
								JOIN tbl_tahunakademik b ON a.tahunajaran = b.kode
								WHERE deleted_at IS NULL")->result();
		return $list;
	}

	function loadjadwal($id, $year, $tipe)
	{
		$this->db->where('id_jadwal', $id);
		$this->db->where('tahunakademik', $year);
		$this->db->where('tipe_uji', $tipe);
		$get = $this->db->get('tbl_jadwaluji');
		return $get;
	}

	function getDetailJdl($id, $typ)
	{
		$this->db->select('a.*, b.kd_jadwal,b.kd_matakuliah,b.id_jadwal,b.kelas,b.kd_dosen, a.tahunakademik');
		$this->db->from('tbl_jadwaluji a');
		$this->db->join('tbl_jadwal_matkul b', 'a.id_jadwal = b.id_jadwal');
		$this->db->where('a.id_jadwal', $id);
		$this->db->where('a.tipe_uji', $typ);
		$res = $this->db->get();
		return $res;
	}

	function search($year, $uid, $start, $length, $search)
	{
		$this->db->select('*');
		$this->db->from('tbl_jadwal_matkul');
		$this->db->like('kd_jadwal', $uid, 'after');
		$this->db->or_like('kd_dosen', $search, 'BOTH');
		$this->db->or_like('kd_matakuliah', $search, 'BOTH');
		$this->db->or_like('kelas', $search, 'BOTH');
		$this->db->where('kd_tahunajaran', $year);
		$this->db->limit($length, $start);
		// $this->db->where('b.tipe_uji', $typ);
		$get = $this->db->get();
		return $get;
	}

	function posts_search_count($search)
	{
		$query = $this
			->db
			->like('kelas', $search)
			->or_like('kd_dosen', $search)
			->or_like('kd_matakuliah', $search)
			->get('tbl_jadwal_matkul');

		return $query->num_rows();
	}

	function export($year, $user, $type)
	{
		$this->db->select('*');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->join('tbl_jadwaluji b', 'a.id_jadwal = b.id_jadwal', 'left');
		$this->db->where('b.tipe_uji', $type);
		$this->db->like('a.kd_jadwal', $user, 'after');
		$this->db->where('a.kd_tahunajaran', $year);
		$get = $this->db->get();
		return $get;
	}

	function export_byDosen($year, $user, $type)
	{
		$this->db->select('*');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->join('tbl_jadwaluji b', 'a.id_jadwal = b.id_jadwal', 'left');
		$this->db->where('b.tipe_uji', $type);
		$this->db->like('a.kd_dosen', $user, 'after');
		$this->db->where('a.kd_tahunajaran', $year);
		$get = $this->db->get();
		return $get;
	}

	function loadByDosen($year, $uid)
	{
		$this->db->select('
			a.id_jadwal,
			a.kd_jadwal,
			a.id_matakuliah,
			a.kd_matakuliah,
			a.kelas,
			(SELECT tipe_uji FROM tbl_jadwaluji WHERE kd_jadwal = a.kd_jadwal AND tipe_uji = 1) AS uts,
			(SELECT tipe_uji FROM tbl_jadwaluji WHERE kd_jadwal = a.kd_jadwal AND tipe_uji = 2) AS uas, 
			b.nama_matakuliah, 
			c.nama
		');
		$this->db->from('tbl_jadwal_matkul a');
		$this->db->join('tbl_matakuliah b', 'a.id_matakuliah = b.id_matakuliah');
		$this->db->join('tbl_karyawan c', 'c.nid = a.kd_dosen');
		$this->db->where('a.kd_dosen', $uid);
		$this->db->where('kd_tahunajaran', $year);
		return $this->db->get();
	}
}

/* End of file Ujian_model.php */
/* Location: ./application/models/Ujian_model.php */
