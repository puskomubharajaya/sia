$(document).ready(function(){
  $('#program').change(function(){
    $.post(location.origin+'/akademik/beban_kinerja_dosen/get_program_activity/'+$(this).val(),{},function(get){
      $('#activity').html(get);
    });
  });
})

function getTable() {
  $.post('<?php echo base_url(); ?>data/dropout/loadTable/', function(data) {
      $('#appearHere').html(data);
    });
  }

$(function () {
  $('#throw').click(function (e) {
    $.ajax({
      type: 'POST',
      url: '<?= base_url('data/dropout/outtemporary'); ?>',
      data: $('#temporaryform').serialize(),
      error: function (xhr, ajaxOption, thrownError) {
        return false;
      },
      success: function () {
        getTable();
        $('#toRemove').hide();
        $('#npm,#awl,#reason').val('');
      }
    });
    e.preventDefault();
  });
});

function rmData(id) {
    //alert(id);
    $.ajax({
        type: 'POST',
        url: '<?= base_url('data/dropout/deleteList/');?>'+id,
        error: function (xhr, ajaxOptions, thrownError) {
            return false;           
        },
        success: function () {
            getTable();
        }
    });
}